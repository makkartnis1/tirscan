'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var postcss    = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');

// 
//gulp.task('sass', function () {
//  return gulp.src('./frontend/web/css/scss/main.scss')
//    .pipe(sass().on('error', sass.logError))
//    .pipe(gulp.dest('./frontend/web/css'));
//});
// 
//gulp.task('sass:watch', function () {
//  gulp.watch('./frontend/web/css/scss/**/*.scss', ['sass']);
//});
//
//gulp.task

gulp.task('sass', function () {
    
    
    
    return gulp.src('./assets/css/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe( sourcemaps.init() )
        .pipe( postcss([ require('autoprefixer'), require('precss') ]) )
        .pipe( sourcemaps.write('.') )
        .pipe(gulp.dest('./assets/css'))
        .pipe(livereload());
});

gulp.task('default', function () {
    livereload.listen()
    gulp.watch('./assets/css/scss/**/*.scss', ['sass']);
});