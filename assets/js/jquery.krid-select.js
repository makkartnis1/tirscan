
$.fn.kridSelect = function ( options ) {
    var self_options = $.extend({
            defaultOptionText: '(не обрано)'
        },
        options);

    var result = this.each(function () {
        var name = $(this).attr('ks-name'),
            placeholder = $(this).attr('ks-placeholder');

        var ul = $(this).find('ul.dropdown-menu'),
            select_title = $(this).find('span.dropdown-select');

        if (typeof placeholder !== 'undefined') {
            var li_default = $('<li><a href="javascript:;">'+self_options.defaultOptionText+'</a></li>')
                .attr('ks-is-placeholder', 'yes')
                .attr('ks-value', placeholder);

            ul.prepend(li_default);
        }

        ul.find('li')
            .addClass('ks-option');

        var input = $('<input type="hidden" name="'+name+'">');
        $(this).append(input);
    });

    $('.ks-option').click(function () {
        var root = $(this).closest('.dropdown'),
            title_el = root.find('span.dropdown-select'),
            input_el = root.find('input[type="hidden"]');

        if ( $(this).attr('ks-is-placeholder') === 'yes' ) {
            input_el.remove();
            title_el.text( $(this).attr('ks-value') );
        } else {
            if (input_el.length == 0) {
                input_el = $('<input type="hidden" name="'+root.attr('ks-name')+'">')
                    .val( $(this).attr('ks-value') );

                root.append(input_el);
            } else {
                input_el.val( $(this).attr('ks-value') );
            }

            title_el.text( $(this).find('a').text() );
        }
    });

    this.each(function () {
        var default_value = $(this).attr('ks-default-value');

        $(this).find('li.ks-option[ks-value="'+default_value+'"]').click();
    });

    return result;
};