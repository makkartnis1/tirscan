$(function(){
    $('.selectpicker').selectpicker();
    $('.dropdown-toggle').dropdown();

    $('.profile-f-del, .profile-f-remove').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.profile-f').remove();
        $(this).closest('.info-profile').remove();
    });

    $('.profile-f, .info-profile').mouseover(function () {
        $(this).find('.profile-f-del, .profile-f-remove').show();
    });
    $('.profile-f, .info-profile').mouseout(function () {
        $(this).find('.profile-f-del, .profile-f-remove').hide();
    });

    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    });

    $('.q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden-q') // Use the "div" element next to this for the content
            },
            show: {
                event: 'click'
            },
            hide: {
                event: 'mouseover'
            }
        });
    });

});