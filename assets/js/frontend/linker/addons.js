/*
 * LINKeR
 */

/**
 * Created by LINKeR on 06.06.16.
 */
function table__build_LINKeRxUA( db_result ){

    var custom_options = {};

    if(typeof arguments[1] === typeof {}){
        $.extend(true, custom_options, arguments[1])
    }

    var default_options    = {

        table:{
            css_class: ['table', 'table-striped']
        },

        head: {

            visible:      [],      // колонки які будуть відображатись, якщо пустий массив, то показувати всі!
            render: false,         // чи показувати THEAD
            translate: {},         // колонки які перекладати
            enumerate: false,      // чи нумерувати рядки, якщо ІНТ > 0, то з якого починати нумерацію
            function: null         // функція, яка буде перекладати колонки

        },

        row_callback: null         // обробка значень ряду (наприклад , якщо 1,0 - статуси, а користувачу потрібно показати "Активний", "Неактивний")

    };

    var options = {};

    $.extend(true, options, default_options);
    $.extend(true, options, custom_options);

    var head = '';

    if(options.head.render){

        head = $('<tr></tr>').clone();

        if(options.head.enumerate !== false){

            head.append( $('<th>#</th>').clone() );

        }

        if(options.head.visible.length){

            $.each(options.head.visible, function(i, column){

                var col_name = options.head.translate[column];

                if( $.isFunction( options.head.function ) ){

                    switch (true){

                        case( typeof options.head.translate[column] === typeof ''):
                            col_name = options.head.function.apply(null, [options.head.translate[column]]);
                            break;

                        case( typeof options.head.translate[column] === typeof []):
                            col_name = options.head.function.apply( null, options.head.translate[column] );
                            break;

                        default: col_name = options.head.translate[column];
                    }

                }

                head.append( $('<th>' + col_name + '</th>').clone() );

            });

        }else{

            $.each(db_result, function(i, row){

                $.each(row, function(col, val){

                    head.append( $('<th>' + col + '</th>').clone() );

                });

                return false;

            });

        }

    }

    var table = $('<table></table>').clone();
    var thead = $('<thead></thead>').clone();
    var tbody = $('<tbody></tbody>').clone();

    $.each(options.table.css_class, function (i, class_val) {

        table.addClass(class_val);

    });

    $.each(db_result, function(i, row){

        var spec = '';
        if($.isFunction(options.row_callback)){
            spec = options.row_callback(row);

            if(typeof spec !== typeof '')
                spec = '';

        }

        var tr = $('<tr ' + spec + '></tr>').clone();

        if(options.head.enumerate !== false){

            var row_num = options.head.enumerate + i;

            tr.append( $('<td>' + row_num + '</td>').clone() );

        }

        if(options.head.visible.length){

            $.each(options.head.visible, function(j, column){

                tr.append( $('<td>' + row[column] + '</td>').clone() );

            });

        }else{

            $.each(row, function(column, value){

                tr.append( $('<td>' + value + '</td>').clone() );

            });

        }

        tbody.append( tr );

    });

    thead.append(head);
    table.append(thead);
    table.append(tbody);

    return table;

}