var emptyCargoForm = 'load=&load_geo=&unload=&unload_geo=&car_type=&load_radius=&unload_radius=&weight=&volume=&size_z=&size_x=&size_y=&cond_t_value=&cond_pallets_value=&cond_ADR_value=&price_value=&price_currency=1&price_before_amount=&pay_form=&date_from=&date_to=';

function formIsEmpty() {
    return ($('#filter-form').serialize() == emptyCargoForm);
}

function activateSemafor(tid, ttype) {
    $.konjax('send', window.ssi.api.filter.templates.activate_semafor, {
        id: tid,
        type: ttype
    }, {
        data_processor: {
            success: function (response) {
                getTemplatesList("cargo");
            }
        }
    });
}

function getTemplatesList(request_type) {
    $.konjax('send', window.ssi.api.filter.templates.get, {type: request_type}, {
        data_processor: {
            success: function (response) {

                var templates = [],
                    current_query = $('#filter-form').serialize();

                $.each(response.data, function () {
                    var obj = this;
                    obj.active = this.query == current_query;
                    templates.push(obj);
                });

                var template = Handlebars.compile($('#template_filterTemplateInModal').html());
                $('#template-modal .modal-body .template-fixed-box .inner-holder').html(template({items: templates}));
            }
        }
    });
}

function updateTemplate(new_name, new_query, id, type) {
    var new_fields = {};

    if (id != undefined)
        new_fields.id = id;
    if (new_name != undefined)
        new_fields.name = new_name;
    if (new_query != undefined)
        new_fields.query = new_query;
    if (type != undefined && id == undefined)
        new_fields.type = type;

    $.konjax('send', window.ssi.api.filter.templates.update, new_fields, {
        data_processor: {
            success: function (response) {
                getTemplatesList("cargo");
            }
        }
    });
}

function deleteTemplate(id) {
    $.konjax('send', window.ssi.api.filter.templates.remove, {itemID: id}, {
        data_processor: {
            success: function (response) {
                getTemplatesList("cargo");

                console.log(response.data);
            }
        }
    });
}

function checkNew(ttype) {
    if (window.ssi.authorized == 0) {
        return;
    }

    $.konjax('send', window.ssi.api.filter.templates.check_new, {
        type: ttype,
        locale_id: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {
                var templates = response.data.templates,
                    tmp = '';

                if (templates.length == 0) {
                    $('.template-semafor-block').hide();
                }
                else {
                    $('.template-items').empty();

                    $.each(templates, function () {
                        tmp = '<li class="template-item active">'+
                            '<div class="first-holder text-center">'+
                            '<span class="items-numb">'+this.count+'</span>'+
                            '</div>'+
                            '<div class="center-holder text-left">'+
                            '<a href="?t='+this.raw.id+'">'+
                            this.raw.name+
                            '</a>'+
                            '</div>'+
                            '</li>';

                        $('.template-items').append(tmp);
                    });

                    $('.template-semafor-block').show();
                }
            }
        }
    });
}

$(function () {
    window.setInterval(function () {
        checkNew('cargo');
    }, 10000);

    $('.q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden') // Use the "div" element next to this for the content
            },
            show: {
                event: 'mouseover'
            },
            hide: {
                target: $('.close-q'),
                event: 'click'
            },
            position: {
                my: 'top left', // Position my top left...
                at: 'bottom center' // at the bottom right of...
            }
        });
    });

    $('.sticker-close').click(function () {
        var sticker = $(this).parents('.sticker'),
            selector = sticker.attr('clear-target');

        $(selector).each(function () {
            if ($(this).attr('type') == 'checkbox') {
                $(this).prop('checked', false);
            }
            else {
                $(this).val('');
            }
        });

        sticker.remove();

        $('form#filter-form').submit();
    });

    $('body').on('keypress', '.input-edit-template-name', function (e) {
        if (e.which == 13) {
            var item_id = $(this).attr('data-id');

            updateTemplate($(this).val(), undefined, item_id, 'cargo');
            return false;
        }
    });

    $('body').on('click', '.template-delete', function () {
        var item_id = $(this).parents('li.template-item').attr('data-id');

        deleteTemplate(item_id)
    });

    $('body').on('click', '.template-update-query', function () {
        var item_id = $(this).parents('li.template-item').attr('data-id'),
            query = $('#filter-form').serialize();

        updateTemplate(undefined, query, item_id, 'cargo');
    });

    $('body').on('click', '.template-semafor', function () {
        var item_id = $(this).parents('li.template-item').attr('data-id');

        activateSemafor(item_id, 'cargo');
    });


    $('body').on('click', '.template-edit-name', function () {
        var item_id = $(this).parents('li.template-item').attr('data-id'),
            name = $(this).parents('li.template-item').attr('data-name');

        $('.link-apply-template').show();
        $(this).parents('li.template-item').find('.link-apply-template:first').hide();
        $('.input-edit-template-name').hide();
        $(this).parents('li.template-item').find('.input-edit-template-name:first').show();
    });

    $('#template-modal').on('shown.bs.modal', function () {
        getTemplatesList("cargo");
    });

    $('.button-add-template').click(function () {
        var query = $('#filter-form').serialize();

        if (formIsEmpty()) {
            toastr.error('Будь ласка, заповніть хоча б одне поле у формі фільтрів для створення шаблону');
        }
        else {
            updateTemplate('%force_default_name%', query, undefined, 'cargo');
        }
    });

    $( ".datepicker" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $('#getFixed2').on('click', function () {
        $('#getFixed').toggle('show');
    });

    $('#close-x').on('click', function (event) {
        event.preventDefault();
        $(this).closest('#getFixed').hide();
    });

    $('#swag-button-places').click(function () {
        var load_val = $('[name="load"]').val(),
            load_geo_val = $('[name="load_geo"]').val(),
            load_radius = $('[name="load_radius"]').val();

        $('[name="load"]').val( $('[name="unload"]').val() );
        $('[name="load_geo"]').val( $('[name="unload_geo"]').val() );
        $('[name="load_radius"]').val( $('[name="unload_radius"]').val() );

        $('[name="unload"]').val( load_val );
        $('[name="unload_geo"]').val( load_geo_val );
        $('[name="unload_radius"]').val( load_radius );

        $('form#filter-form').submit();
    });
});

//      -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//      PAGE MANAGE :   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
//      -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -

function get_transport_application(request_id) {
    $.konjax('send', window.ssi.api.applications.get, {
        type: 'cargo',
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile( $('#template_modalRequestApply').html() ) ;
                $('#modal-apply-area').html( template(response.data) );

                $('#cargoOrderModal').modal('show');
            }
        }
    });
}

function send_transport_application(request_id) {
    $.konjax('send', window.ssi.api.applications.send, {
        type: 'cargo',
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                $('#cargoOrderModal').modal('hide');
            }
        }
    });
}

$(function(){

    $(document)
        .on('click', '#btn-send-application', function () {
            send_transport_application($(this).attr('data-request-id'));
        });

    $(document)
        .on('click', '[data-click="showCargoOrderModal"]', function(){

            if (!window.ssi.authorized) {
                $('#modal-login').modal('show');
                return;
            }

            var row_info = $(this).closest('table').clone();

            $('#cargoOrderModal [data-table="here"]').html($(row_info));

            var garbage = '#cargoOrderModal [data-table="here"] tbody tr:not([data-request-id="' + $(this).attr('data-request-id') + '"])';

            $('#cargoOrderModal [data-table="here"] tbody tr').prop('data-click', false);

            $(garbage).remove();

            $('#cargoOrderModal .modal-title span').text('Заяка №' + $(this).attr('data-request-id')); // todo READ LOCALED STRING

            $('#cargoOrderModal').attr('data-from-placeid', $(this).attr('data-from-placeid'));
            $('#cargoOrderModal').attr('data-to-placeid', $(this).attr('data-to-placeid'));

            $('#btn-send-application').attr('data-request-id', $(this).attr('data-request-id'));

            $('#cargoOrderModal .modal-body i.fa-envelope').remove();
            $('#cargoOrderModal .col-additional-info').remove();

            get_transport_application($(this).attr('data-request-id'));
            
            modal_cargo_set_details();

            addView($(this).attr('data-request-id'), 'cargo');
        });

    $('#cargoOrderModal').on('shown.bs.modal', function () {
        $(this).find('iframe').attr('src','' +
            'https://www.google.com/maps/embed/v1/directions' +
            '?key=' +
            window.tscan.google.key +
            '&origin=place_id:' + $(this).attr('data-from-placeid') +
            '&destination=place_id:' + $(this).attr('data-to-placeid') +
            '&avoid=tolls|highways' +
            '');
    });


    $('.contact').on('click', function(e){
        
        if (!window.ssi.authorized) {
                $('#modal-login').modal('show');
                return;
            }

        var self = $(this).parent();

        e.stopPropagation();

        $('#modal-message').modal('show');

        var theme = self.attr('data-id')+' - '
                +self.parent().find('.from-country').text()+' -> '
                +self.parent().find('.to-country').text(),
            user_to = self.attr('data-user-id');

        $('#modal-message [name="theme"]').val(theme);
        $('#send-message').attr('data-user-id', user_to);

    });


    $('#send-message').click(function(){
        var user_to = $(this).attr('data-user-id'),
            theme = $('#modal-message [name="theme"]').val();

        $.konjax('send',ssi.api.messenger.createNewDialogSendMessage,{
            user_from:$('#user_from').val(),
            user_to: user_to,
            message: $('#message-textarea').val(),
            theme: theme
        },{
            data_processor:{
                success: function(){
                    $('#modal-message').modal('hide');
                    $('#form-messages').trigger('reset');
                },
                error: function(){
                    console.log('error');
                }
            }
        })

    });

});



function modal_cargo_set_details(){

    var modal   = $('#cargoOrderModal');
    var requestID = modal.find('[data-request-id]').attr('data-request-id');
    var _list   = JSON.parse( $('tr[data-id="' + requestID + '"] script[type="application/json"]').html() );

    var list    = [];

    window.ssi.log('READING ADDITOINAL INFO: CARGO');

    window.i18n_sss = [];

    $.each(_list, function(prop, value){

        window.i18n_sss.push( '__db("transport.option.'+prop+'");');

        window.ssi.log(prop, value);

        if( value == 'no' || value == '' || value == null ){
            return true; // пропускаємо поточну ітерацію
        }

        var unit = '';

        switch(true){

            case (value == 'yes'):
                value   = '';
                unit    = '<i class="fa fa-check text-success"></i>';
                break;

            case (prop == 'paymentType'):
                value = i18n(value);
                break;

            case (prop == 'info'):
                name = '';
                unit = '';
                break;

            case (prop == 'price'):
                value = value.split('|', 2);
                unit  = value[1]; value = value[0];
                break;

            case (prop == 'condADR'):
                console.log('here>>ADR');
                unit = '<i class="fa fa-warning text-warning"></i>';
                break;

            case (prop == 'advancedPayment'):
            case (prop == 'PDV'):
                unit    = '%';
                break;

            case (prop == 'condTemperature'):
                unit    = i18n('unit.degree');
                break;

        }



        var row = {
            name:   i18n('transport.option.' + prop),
            value:  value,
            unit:   unit
        };

        list.push( row );

    });

    var source      = $('#modal-cargo-or-transport-request-additional-info').html();
    var template    = Handlebars.compile(source);
    var block       = template({list: list});

    // console.log(list, block, _list);

    modal.find('[data-info]').html( block );



}