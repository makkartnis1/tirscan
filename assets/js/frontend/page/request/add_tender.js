$(function(){

    $(document).on( "click", "[name=filterPartners]", function(){
        var check = $(this).prop('checked');
        if (check)
        {
            $("[name=filterForwarder]").prop('checked',false).attr("disabled", true);
            $("[for=filterForwarder]").css('color','#BBBBBB');

            $("[name=filterCompanyYears]").prop('checked',false).attr("disabled", true);
            $("[for=filterCompanyYears]").css('color','#BBBBBB');

            $("[name=companyYears]").val("").css('color','#BBBBBB');
            $("[for=companyYears]").css('color','#BBBBBB');
        }
        else
        {
            $("[name=filterForwarder]").removeAttr("disabled");
            $("[for=filterForwarder]").css('color','black');

            $("[name=filterCompanyYears]").removeAttr("disabled");
            $("[for=filterCompanyYears]").css('color','black');

            $("[name=companyYears]").css('color','black');
            $("[for=companyYears]").css('color','black');
        }
    });

    $('.additionalInfoButton').on('click', function(){
        set_additional_text();
        $("#additionalInfoModal").modal('hide');
    });



    $( ".datepicker" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });


    // google maps
//    google.maps.event.addDomListener(window, 'load', initialize);

//    $(document).on('change', '.startPointTender, .endPointTender', function () {
//        var self = $(this);
//
//        window.setTimeout(function () {
//            googleMapsLoaded(self.attr('name'));
//        }, 300);
//    });
//


    $(document).on('click', '.deletePlace', function () {
        $(this).parent().remove();
    });

    var load_count = 1;
    var unload_count = 1;

    $('#addLoadPlace').click(function () {
        load_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control startPointTender" id="startPointTender'+load_count+'" name="load_'+load_count+'" placeholder="'+i18n('tenders.page.load_point')+'">'+
                '<input type="hidden" name="load_place[]">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
                '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="load_'+load_count+'"]').each(function () {
//            autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
            addAutocompleteTender(this);
        });
    });

    $('#addUnloadPlace').click(function () {
        unload_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control endPointTender" id="endPointTender'+unload_count+'" name="unload_'+unload_count+'" placeholder="'+i18n('tenders.page.unload_point')+'">'+
                '<input type="hidden" name="unload_place[]">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
                '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="unload_'+unload_count+'"]').each(function () {
//            autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
            addAutocompleteTender(this);
        });
    });

});

//var autocomplete = {},
//    ,
//    options = {
//        types: ['(regions)']
//    };

//function initialize() {
//    $('.startPointTender, .endPointTender').each(function () {
//        autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
//    });
//}

//function googleMapsLoaded(name) {
//    var place = autocomplete[name].getPlace(),
//        place_input = $('[name="'+name+'"]'),
//        place_data_input = place_input.next();
//
//
//    var post = new Object();
//    post.place_id = place.place_id;
//    post.latitude = place.geometry.location.lat;
//    post.longitude = place.geometry.location.lng;
//    post.name = place.name;
//    post.full_name = place.formatted_address;
//    post.places = [];
//    post.country = '';
//
//    $.each(place.address_components, function(key, value){
//        if (key > 0)
//        {
//            post.places.push(value.long_name);
//        }
//        post.country = value.short_name;
//    });
//
//    window.lang = 'uk';
//
//    $.ajax({
//        method: "POST",
//        url: "/cms/geo/"+window.lang+"/add/",
//        data: post
//    })
//        .done(function( response ) {
//            place_data_input.val(response);
//        });
//}

function show_hidden_fields(name_selector,value)
{
    if (value)
    {
        $("[name="+name_selector+"]").show().val(value);
        return true;
    }
    return false;
}

function set_value(name_selector,value)
{
    if (value)
    {
        $("[name="+name_selector+"]").val(value);
        return true;
    }
    return false;
}



function set_load(action,name,loads_arr)
{
    if (loads_arr.length == 0) return null;
    var del_btn = '';

    $("#"+action).html('');

    console.log(loads_arr);
    for (var i=0; i<loads_arr.length; i++)
    {
        del_btn = '';
        if (i > 0) del_btn = '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>';

        var new_child =
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control '+action+'PointTender" id="'+action+'PointTender'+i+'" name="'+name+'_'+i+'" value="'+loads_arr[i].name+'" placeholder="'+i18n('tenders.page.load_point')+'">'+
                '<input type="hidden" name="'+name+'_place[]" value="'+loads_arr[i].value+'">'+
                del_btn+
                '</div>';

        $('#'+action).append(new_child);
    }

//    initialize();
}

function set_checked(name_selector,val)
{
    if (val)
    {
        $('[name='+name_selector+']').prop('checked', true);

        if (name_selector == 'filterPartners')
        {
            $("[name=filterForwarder]").prop('checked',false).attr("disabled", true);
            $("[for=filterForwarder]").css('color','#BBBBBB');

            $("[name=filterCompanyYears]").prop('checked',false).attr("disabled", true);
            $("[for=filterCompanyYears]").css('color','#BBBBBB');

            $("[name=companyYears]").val("").css('color','#BBBBBB');
            $("[for=companyYears]").css('color','#BBBBBB');
        }

        return true;
    }
    return false;
}


function set_additional_text()
{
    var checked = [];

    $('#additionalInfoModal .checkbox').each(function(i, el){

        if( $(el).find('input[type="checkbox"]:checked').length ){

            if( $(el).hasClass('checkboxOnly') ){

                checked.push( $(el).text().trim() )

            }else if( $(el).hasClass('checkboxWithInput') ){

                checked.push( $(el).text().trim() + ': ' + $(el).find('.additionalTextInfo').val() )

            }

        }

    });

    var textarea = $('.modal textarea').val().trim();

    if(textarea.length)
        checked.push(i18n('tenders.page.mark')+': ' + textarea);

    $('.additionalInfoResult').text( checked.join(', ') );
    $(this).parents('.modal').modal('hide');
}

function set_errors(errors)
{
    if (errors.length == 0) return false;

    for(key in errors)
    {
        $("[for="+key+"]").css("color","rgb(255, 0, 0)").css("display","block").html(errors[key]).show();
    }

}