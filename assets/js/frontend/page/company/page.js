$(function() {
    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('.q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden') // Use the "div" element next to this for the content
            },
            show: {
                event: 'click'
            },
            hide: {
                event: 'mouseover'
            }
        });
    });

    $('.dropdown-toggle').dropdown();

    for (var i=1; i<5; i++)
    {
        $('#companyRating'+i).barrating({
            theme: 'fontawesome-stars',
            readonly: true
        });
    }

    fakewaffle.responsiveTabs(['xs']);

    $('#complaint').on('click', function(){
        $('#modal-complaint').modal('show');
    });

    $('#create-message').on('click', function(){
        $('#modal-message').modal('show');
    });

    $(document).on('submit', '#form-messages', function(e) {

        e.preventDefault();

        var user_to     = $('#form-messages [name="user_to"]').val(),
            user_from   = $('#form-messages [name="user_from"]').val(),
            theme       = $('#form-messages [name="theme"]').val(),
            message     = $('#form-messages [name="message"]').val();

        $.konjax('send',ssi.api.messenger.createNewDialogSendMessage,{
            user_to: user_to,
            user_from: user_from,
            theme: theme,
            message: message
        },{
            data_processor:{
                success: function(){
                    console.log('success');

                    $('#modal-message').modal('hide');
                    $('#form-messages').trigger('reset');
                },
                error: function(){
                    console.log('error');
                }
            }
        });

    });

    var userID    = $('#form-complaint [name="user-id"]').val(),
        toCompany = $('#form-complaint [name="complain-company-id"]').val();

    $(document).on('submit', '#form-complaint', function(e) {

        e.preventDefault();

        var  message = $('#form-complaint #message-textarea').val();

        $.konjax('send',ssi.api.complaints.sendComplaint,{
            message: message,
            userID: userID,
            toCompany: toCompany
        },{
            data_processor:{
                success: function(){
                    console.log('success');

                    $('#modal-complaint').modal('hide');
                    $('#form-complaint').trigger('reset');
                },
                error: function(){
                    console.log('error');
                }
            }
        });

    })

    .on('click', '#add-black-list', function(e){

        e.preventDefault();

        $.konjax('send',ssi.api.complaints.addToBlackList,{
            userID: userID,
            toCompany: toCompany
        },{
            data_processor:{
                success: function(){
                    console.log('success');
                },
                error: function(){
                    console.log('error');
                }
            }
        });
    });

    // модалки
    $(document)
        .on('click', '#btn-send-application', function () {
            send_application($(this).attr('data-request-id'), $(this).attr('data-request-type'));
        });


    $(document)
        .on('click', '[data-click="showTransportModal"]', function () {

            if (!window.ssi.authorized) {
                $('#modal-login').modal('show');
                return;
            }

            var row_info = $(this).closest('table').clone();

            // $(row_info).find('[data-click="showTransportModal"]').removeAttr('data-click');

            $('#transportOrderModal [data-table="here"]').html($(row_info));
            var garbage = '#transportOrderModal [data-table="here"] tbody tr:not([data-request-id="' + $(this).attr('data-request-id') + '"])';

            $('#transportOrderModal [data-table="here"] tbody tr').prop('data-click', false);

            $(garbage).remove();

            $('#transportOrderModal .modal-title span').text('Заяка №' + $(this).attr('data-request-id')); // todo READ LOCALED STRING

            $('#transportOrderModal').attr('data-from-placeid', $(this).attr('data-from-placeid'));
            $('#transportOrderModal').attr('data-to-placeid', $(this).attr('data-to-placeid'));

            $('#transportOrderModal .modal-body i.fa-envelope').remove();

            modal_transport_set_details();

            get_application($(this).attr('data-request-id'), $(this).attr('data-request-type'));

            addView($(this).attr('data-request-id'), $(this).attr('data-request-type'));
        });

    $('#transportOrderModal').on('shown.bs.modal', function () {
        $(this).find('iframe').attr('src', '' +
            'https://www.google.com/maps/embed/v1/directions' +
            '?key=' +
            window.tscan.google.key +
            '&origin=place_id:' + $(this).attr('data-from-placeid') +
            '&destination=place_id:' + $(this).attr('data-to-placeid') +
            '&avoid=tolls|highways' +
            '');
    });

});

function modal_transport_set_details(){

    var modal   = $('#transportOrderModal');
    var requestID = modal.find('[data-request-id]').attr('data-request-id');
    var _list   = JSON.parse( $('tr[data-id="' + requestID + '"] script[type="application/json"]').html() );

    var list    = [];
    window.ssi.log('READING ADDITOINAL INFO: TRANSPORT');
    $.each(_list, function(prop, value){

        window.ssi.log(prop, value);

        if( value == 'no' || value == '' || value == null ){
            return true; // пропускаємо поточну ітерацію
        }

        var unit = '';


        switch(true){

            case (value == 'yes'):
                value   = '';
                unit    = '<i class="fa fa-check text-success"></i>';
                break;

            case (prop == 'paymentType'):
                value = i18n(value);
                break;

            case (prop == 'info'):
                name = '';
                unit = '';
                break;

            case (prop == 'price'):
                value = value.split('|', 2);
                unit  = value[1]; value = value[0];
                break;

            case (prop == 'condADR'):
                console.log('here>>ADR');
                unit = '<i class="fa fa-warning text-warning"></i>';
                break;

            case (prop == 'advancedPayment'):
            case (prop == 'PDV'):
                unit    = '%';
                break;

            case (prop == 'condTemperature'):
                unit    = i18n('unit.degree');
                break;

        }



        var row = {
            name:   i18n('transport.option.' + prop),
            value:  value,
            unit:   unit
        };

        list.push( row );

    });

    var source      = $('#modal-cargo-or-transport-request-additional-info').html();
    var template    = Handlebars.compile(source);
    var block       = template({list: list});

    // console.log(list, block, _list);

    modal.find('[data-info]').html( block );



}

function get_application(request_id, _type) {
    $.konjax('send', window.ssi.api.applications.get, {
        type: _type,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_modalRequestApply').html());
                $('#modal-apply-area').html(template(response.data));

                $('#transportOrderModal').modal('show');
            }
        }
    });
}

function send_application(request_id, _type) {
    $.konjax('send', window.ssi.api.applications.send, {
        type: _type,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                $('#transportOrderModal').modal('hide');
            }
        }
    });
}
