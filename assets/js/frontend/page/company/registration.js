$(function () {
    $('#registration-submit').click(function () {
        if ( $('[name="terms_accepted"]').prop('checked') == false )
            toastr.error('Ознайомтесь та погодьтесь з правилами на сайті !');
        else
            $('#registration-form').submit();
    });

    $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});