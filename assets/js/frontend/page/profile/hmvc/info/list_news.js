function get_list() {
    $.konjax('send', window.ssi.api.profile.listNews, {
        lang_id: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {
                var source = $('#info-block-list-news').html(),
                    template = Handlebars.compile(source),
                    html = template({list_news: response.data.list_news});

                $('#list').html(html);
            }
        }
    });
}

$(document).on('click', 'a[href="#tab-infoblock-news"]', function (){
    get_list();
});

$(document).on('click', '.edit-news', function(){

    var id = $(this).attr('data-id');

    $.konjax('send', window.ssi.api.profile.oneNews, {
        lang_id: window.ssi.locale.id,
        news_id: id
    }, {
        data_processor: {
            success: function(response){
                var source = $('#info-block-one-news').html(),
                    template = Handlebars.compile(source),
                    html = template({
                        id: response.data.one_news.ID,
                        content: response.data.one_news.content,
                        createDate: response.data.one_news.createDate,
                        preview: response.data.one_news.preview,
                        title: response.data.one_news.title
                    });

                $('#list').html(html);
            },
            error: function () {}
        }
    });

});

$(function() {

    var form = '#update_news';

    $(document).on('submit', form, function (e) {

        e.preventDefault();

        $(form).find('input, select, textarea').inputError('remove');
        var formData = $(form).serializeJSON();

        $.konjax('send', window.ssi.api.partner.news_update, {
            lang_id: window.ssi.locale.id,
            post: formData
        }, {
            data_processor: {
                success: function() {
                    get_list();
                }
            }
        });

    });

});