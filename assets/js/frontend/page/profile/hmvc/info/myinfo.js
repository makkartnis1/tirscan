/**
 * Created by LINKeR on 29.04.16.
 */

$(function(){


    var form = '#tab-infoblock-myinfo form';

    $(form).find('input[name="Frontend_Users[email]"]').prop('disabled', true);

    $(document).on('submit', form, function(e){

        e.preventDefault();

        $(form).find('input, select').inputError('remove');

        var formData = $(form).serializeJSON();


        console.log(formData);
        $.konjax('send', ssi.api.profile.saveInfo, {post: formData}, {
            
            data_processor: {

                success: function (resp) {

                    // func from /assets/js/frontend/page/profile/hmvc/info.js
                    func_assign_data_profile_info(resp.data);

                },
                error: function(resp){

                    $.each(resp.data.validation, function(field, validation_row){

                        console.log(field, validation_row);

                        switch (field){

                            case ('name'):

                                if(validation_row[0] == 'not_empty'){

                                    var primaryLocaleID = $(form).find('input[name="primaryLocaleID"]').val();

                                    var input = 'input[name="Frontend_Users_Locale[' + primaryLocaleID + '][name]"]';

                                }

                                break;

                            case ('icq'): var input = 'input[name="Frontend_Users[icq]"]'; break;
                            case ('skype'): var input = 'input[name="Frontend_Users[skype]"]'; break;
                            case ('email'): var input = 'input[name="Frontend_Users[email]"]'; break;
                            case ('phone'): var input = 'input[name="Frontend_Users[phone]"]'; break;
                            case ('phoneStationary'): var input = 'input[name="Frontend_Users[phoneStationary]"]'; break;
                            case ('icq'): var input = 'input[name=""]'; break;

                        }

                        $(form)
                            .find(input)
                            .inputError('add', i18n_validation(validation_row));

                        // switch (field){
                        //     case ('name'):
                        // }

                    });

                }

            }


        });

    })

});