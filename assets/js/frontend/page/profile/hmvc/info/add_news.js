$(function() {

    var files,
        form = '#tab-infoblock-newsadd form';

    // Вешаем функцию на событие
    // Получим данные файлов и добавим их в переменную

    $('input[type=file]').change(function () {
        files = this.files;

        // Создадим данные формы и добавим в них данные файлов из files
        var data = new FormData();

        $.each( files, function( key, value ){
            data.append( key, value );
        });

        // Отправляем запрос
        $.ajax({
            url: ssi.api.profile.uploadImgNews,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Не обрабатываем файлы (Don't process the files)
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function( respond, textStatus, jqXHR ){

                // Если все ОК

                if( typeof respond.success === 'undefined' ){
                    // Файлы успешно загружены, делаем что нибудь здесь

                    // выведем пути к загруженным файлам в блок '.ajax-respond'
                    var files_path = respond.data.files,
                        html = '';

                    $.each( files_path, function( key, val ){
                        html = val;
                    });

                    $('.ajax-respond').attr( 'value', html );
                }
                else{
                    console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log('ОШИБКИ AJAX запроса: ' + textStatus );
            }
        });
    });


    $(document).on('submit', form, function(e) {

        e.preventDefault();

        $(form).find('input, select, textarea').inputError('remove');

        var formData = $(form).serializeJSON(),
            status   = $('#subscribe').prop('checked'),
            url      = $('#url').val(),
            user_company_id = $('#company_id').val();

        $.konjax('send', ssi.api.profile.addNews, {
            post: formData,
            company_id: user_company_id,
            send_mail: status,
            url: url,
            image: $('.ajax-respond').val()
        }, {
            data_processor: {
                success: function (){},
                error: function (response) {

                    $.each(response.data.validation, function(field, validation_row){

                        var primaryLocaleID = response.data.user.primaryLocaleID;

                        switch (field){
                            case ('title'): if(validation_row[0] == 'not_empty'){
                                var input = 'input[name="Frontend_News_Locales[' + primaryLocaleID + '][title]"]';
                            } break;

                            case ('preview'): if(validation_row[0] == 'not_empty'){
                                var input = 'textarea[name="Frontend_News_Locales[' + primaryLocaleID + '][preview]"]';
                            } break;

                            case ('content'): if(validation_row[0] == 'not_empty'){
                                var input = 'textarea[name="Frontend_News_Locales[' + primaryLocaleID + '][content]"]';
                            } break;
                        }

                        $(form).find(input).inputError('add', i18n_validation(validation_row));

                    });

                }

            }
        });

    });

});