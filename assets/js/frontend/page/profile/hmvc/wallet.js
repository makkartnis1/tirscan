function updateLiqpayForm(_amount) {
    $.konjax('send', window.ssi.api.wallet.get_liqpay_form, {
        amount: _amount
    }, {
        data_processor: {
            success: function (response) {
                $('#walletPurchaseHiddenForm').html(response.data.form);
                $('#walletPurchaseHiddenForm form').submit();
            }
        }
    });
}

function getHistory() {
    $.konjax('send', window.ssi.api.wallet.get_history, {
        localeID: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {
                console.log(response.data);

                var template = Handlebars.compile($('#template_walletMyHistory').html());
                $('#table-wallet-history').html(template(response.data));
            }
        }
    });
}

$(function () {
    $('#walletPurchaseMoneyBtn').click(function () {
        updateLiqpayForm($('[name="purchase_amount"]').val());
    });

    $('a[href="#wallet-history"]').on('shown.bs.tab', function (e) {
        if(typeof  e.relatedTarget !== 'undefined'){
            getHistory();
        }
    });
});