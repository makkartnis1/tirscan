


$(document).on('click','a[href="#subscription"]', function(e){

    e.preventDefault();

    window.linker_request_subscription_list.loading(1);
});

$(document).on('click','#profile ul.pagination li:not(.active) a', function (e){

    e.preventDefault();

    window.linker_request_subscription_list.loading($(this).attr('data-page'));
});

window.linker_request_subscription_list = {

    loading: function(page){
        window.linker_request_subscription_list._sys.list(window.linker_request_subscription_list._sys.assign, page);
    },

    _sys: {

        assign: function (data) {

            var source = $('#profile-company-list').html(),
                template = Handlebars.compile(source),
                html = template({company_list: data.data.company_list});

            $('#subscription .table-responsive').html(html);

            if (data.data.pages != 0) {

                var pagination = $('#hb-bs-pagination').html(),
                    pagination_template = Handlebars.compile(pagination),
                    pagination_html = pagination_template({
                        currentPage: data.data.current,
                        totalPage: data.data.pages,
                        size: data.data.step,
                        options: {}
                    });

            } else {
                pagination_html = '';
            }

            $('#subscription ul.pagination').html(pagination_html);

        },

        list: function (callback, page) {

            $.konjax('send', window.ssi.api.subscribe.getCompany, {
                page: page,
                lang_id: window.ssi.locale.id
            }, {
                data_processor: {
                    success: function(response){
                        callback(response);
                    }
                }
            });

        }

    }

};

$(document).on('click', '.del-sub-list', function(){

    var companyID = $(this).attr('data-company');

    $.konjax('send', window.ssi.api.subscribe.unSubscribe, {
        companyID: companyID
    }, {
        data_processor: {
            success: function(){
                console.log('success');
            }
        }
    });

    window.linker_request_subscription_list.loading(1);

});