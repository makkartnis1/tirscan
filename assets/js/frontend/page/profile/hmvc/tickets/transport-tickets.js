function loadTickets() {
    $.konjax('send', window.ssi.api.tickets.get_transports, {
        localeID: window.ssi.locale.id,
        type: $('[name="filter-my-transport-requests-status"]').val()
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_transportMyRequests').html());
                $('.table-transport-my-requests').html(template(response.data));

                $('.transport-ticket-count').text( $('.transport-ticket-row').length );
            }
        }
    });
}

function loadOrderCompanies(request_id) {
    $.konjax('send', window.ssi.api.tickets.get_transport_ticket_companies, {
        localeID: window.ssi.locale.id,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_transportCompanyProps').html());
                $('#orderCompaniesProps').html(template(response.data));

                if (response.data.companies.length == 0) {
                    $('#orderInProcessModalWindow').modal('hide');
                }
            }
        }
    });
}

function changeOrderProp(status, app_id) {
    $.konjax('send', window.ssi.api.tickets.set_transport_proposition, {
        action: status,
        appID: app_id
    }, {
        data_processor: {
            success: function (response) {
                loadOrderCompanies($('#orderInProcessModalWindow').attr('data-id'))
                loadTickets();
            }
        }
    });
}

function closeTicket(request_id, _rating, text) {
    $.konjax('send', window.ssi.api.tickets.close_transport_ticket, {
        rating: _rating,
        feedback: text,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                $('#RatingModal').modal('hide');
                loadTickets();
            }
        }
    });
}

$(function () {
    
    $('#submitCloseTicket').click(function () {
        var text = $('[name="closeTicketText"]').val();
        var rating = $('[name="closeTicketRating"]:checked').val();
        var request_id = $('#RatingModal').attr('data-id');

        if (text.length < 10) {
            toastr.error('Будь ласка, залиште змістовний коментар щодо роботи компанії')
        }
        
        closeTicket(request_id, rating, text);
    });

    $(document).on('click', '.transport-ticket-delete', function (event) {
        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        window.ssi.dialog.confirmation('transport-ticket-delete-yes', 'Ви дійсно бажаєте видалити заявку №' + request_id + '?', request_id);
    });

    $(document).on('click', '#transport-ticket-delete-yes [name="onConfirm"]', function () {
        $('#transportOrderModal').modal('hide');

        var request_id = $(this).attr('value');

        $.konjax('send', window.ssi.api.tickets.delete_transport_request, {requestID: request_id}, {
            data_processor: {
                success: function (response) {
                    loadTickets();
                }
            }
        });
    });

    $(document).on('click', '.accept-transport-company-prop', function () {
        changeOrderProp('accept', $(this).attr('data-id'));
    });

    $(document).on('click', '.reject-transport-company-prop', function () {
        changeOrderProp('reject', $(this).attr('data-id'));
    });

    $(document).on('click', '.transport-ticket-props', function (event) {
        $('#transportOrderModal').modal('hide');

        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        $('.transport-ticket-apps-number').text(request_id);
        $('#orderInProcessModalWindow')
            .attr('data-id', request_id)
            .modal('show');
    });

    $(document).on('click', '.transport-ticket-close', function (event) {
        $('#transportOrderModal').modal('hide');

        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        $('.transport-ticket-close-number').text(request_id);
        $('#RatingModal')
            .attr('data-id', request_id)
            .modal('show');
    });

    $('#orderInProcessModalWindow').on('shown.bs.modal', function () {
        var request_id = $(this).attr('data-id');
        
        loadOrderCompanies(request_id);
    });
    
    $('[name="filter-my-transport-requests-status"]').change(function () {
        loadTickets();
    });

    $('a[href="#ticket-transport"], a[href="#requests-transport-my-requests"]').on('shown.bs.tab', function (e) {
        if(typeof  e.relatedTarget !== 'undefined'){
            loadTickets();
        }
    });

    var hash = window.location.hash;
    if (hash == "#ticket-transport") {
        loadTickets();
    }
    
});