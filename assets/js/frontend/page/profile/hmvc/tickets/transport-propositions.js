function loadProps() {
    $.konjax('send', window.ssi.api.tickets.get_transport_propositions, {localeID: window.ssi.locale.id}, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_transportMyPropositions').html());
                $('.table-transport-my-propositions').html(template(response.data));

                $('.transport-prop-count').text( $('.transport-prop-row').length );
            }
        }
    });
}

$(function () {

    $('a[href="#ticket-transport"], a[href="#requests-transport-my-applications"]').on('shown.bs.tab', function (e) {
        if(typeof  e.relatedTarget !== 'undefined'){
            loadProps();
        }
    });

    $(document).on('click', '.cargo-prop-delete', function () {
        var request_id = $(this).attr('data-id');

        window.ssi.dialog.confirmation('transport-prop-delete', 'Ви дійсно бажаєте відмовитись від вашої пропозиції на заявку №' + request_id + '?', request_id);
    });

    $(document).on('click', '#transport-prop-delete [name="onConfirm"]', function () {
        var request_id = $(this).attr('value');

        $.konjax('send', window.ssi.api.tickets.delete_transport_proposition, {requestID: request_id}, {
            data_processor: {
                success: function (response) {
                    loadProps();
                }
            }
        });

        $('#transportOrderModal').modal('hide');
    });


});