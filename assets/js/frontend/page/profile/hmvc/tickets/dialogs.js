var yet = true;

function showDialogModal(user, user2, request_type, sender_type, request) {
    $('#appDM_requestID').text(request);

    var modal = $('#appDialogModal');

    modal.attr('data-request-type', request_type);
    modal.attr('data-request-id', request);
    modal.attr('data-sender-type', sender_type);
    modal.attr('data-user', user);
    modal.attr('data-user2', user2);

    updateMessages();
    modal.modal();

    if (yet) {
        window.setInterval(function () {
            updateMessages();
        }, 5000);
        yet = false;
    }
}

function getCurrentDialogInfo() {
    var modal = $('#appDialogModal');

    return {
        request_type: modal.attr('data-request-type'),
        request_id: modal.attr('data-request-id'),
        sender_type: modal.attr('data-sender-type'),
        user: modal.attr('data-user'),
        user2: modal.attr('data-user2'),
        message: $('#appDM_message').val()
    }
}

function sendMessage() {
    var info = getCurrentDialogInfo();

    if (info.message.length == 0) {
        toastr.error('Сообщение не должно быть пустым!')
        return;
    }

    $.konjax('send', window.ssi.api.messaging.send_message, info, {
        data_processor: {
            success: function (response) {
                updateMessages();

                $('#appDM_message').val('');
            }
        }
    });
}

function updateMessages() {
    var info = getCurrentDialogInfo();

    $.konjax('send', window.ssi.api.messaging.get_messages, info, {
        data_processor: {
            success: function (response) {
                $('#appDM_dialogMessages').empty();

                $.each(response.data.messages, function () {
                    var my_name = '',
                        owner_class = '';
                    if (this.owner == 'user') {
                        my_name = this.userName;
                    }
                    else {
                        my_name = this.ownerName;
                    }

                    if (((info.sender_type == 'prop') && (this.owner == 'user'))
                        || ((info.sender_type == 'ticket') && (this.owner == 'owner'))) {
                        owner_class = 'myMessage';
                    }
                    else {
                        owner_class = 'guestMessage';
                    }

                    addMessage(this.msg, this.avatar, my_name, this.created, owner_class);
                });
            }
        }
    });
}

function addMessage(text, avatar, name, created, class_type) {
    $('#appDM_dialogMessages').append('<div class="dialogueHistoryItem '+class_type+'">'+
        '<div class="clearfix">'+
        '<div class="userInfo">'+
        '<div class="imgHolder">'+
        '<div class="img" style="background-image: url('+avatar+')"></div>'+
        '<div class="status online"></div>'+
        '</div>'+
        '</div>'+
        '<div class="message">'+
        '<p class="name"><a href="javascript:;">'+name+'</a></p>'+
    '<p class="date">'+created+'</p>'+
    '<p class="text">'+text+'</p>'+
    '</div>'+
    '</div>'+
    '</div>');
}

function addDialog(last_msg, avatar, name, updated, user_id) {
    $('#appSDM_dialogs').append('<div class="info-profile sdm-dialog" data-user-id="'+user_id+'">'+
        '<div class="title">'+
        '<div class="row inner-tab">'+
        '<div class="message-profile ">'+
        '<div class="col-xs-12 col-ms-12 col-sm-6 col-md-4">'+
        '<div class="profile-f">'+
        '<div class="row">'+
        '<div class="col-xs-3">'+
        '<div class="profile-f-img message">'+
        '<img src="'+avatar+'">'+
        '</div>'+
        '</div>'+
        '<div class="col-xs-8">'+
        '<div class="profile-f-p">'+
        '<span class="profile-f-top"><p>'+
        '<a href="javascript:;">'+
        name+
        '</a></p>'+
    '</span>'+
    '<span class="profile-f-bottom message"><p>'+updated+'</p></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-xs-12 col-ms-12 col-sm-5 col-md-7">'+
    '<div class="profile-f-p">'+
    '<span class="profile-f-center message">'+
    '<p>'+last_msg+'</p>'+
    '</span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>');
}

function showAllDialogs(user, type, request) {
    $.konjax('send', window.ssi.api.messaging.get_all_dialogs, {
        request_type: type,
        request_id: request
    }, {
        data_processor: {
            success: function (response) {
                if (response.data.dialogs.length == 0) {
                    toastr.info('По заявке нет предложений для диалога');
                }
                else {
                    $('#appSDM_dialogs').empty();

                    var modal = $('#appSelectDialogModal');
                    modal.attr('data-request-user', user);
                    modal.attr('data-request-type', type);
                    modal.attr('data-request-id', request);

                    $.each(response.data.dialogs, function () {
                        addDialog(this.msg, this.avatar, this.userName, this.created, this.userID);
                    });

                    modal.modal();
                }
            }
        }
    });
}

$(function () {
    $(document).on('click', '.transport-ticket-message', function () {
        var user = $(this).closest('.transport-ticket-row').attr('data-request-user-id');

        showAllDialogs(user, 'transport', $(this).attr('data-id'));
    });

    $(document).on('click', '.cargo-ticket-message', function () {
        var user = $(this).closest('.cargo-ticket-row').attr('data-request-user-id');

        showAllDialogs(user, 'cargo', $(this).attr('data-id'));
    });

    $(document).on('click', '.sdm-dialog', function () {
        var user = $('#appSelectDialogModal').attr('data-request-user'),
            user2 = $(this).attr('data-user-id'),
            type = $('#appSelectDialogModal').attr('data-request-type'),
            request = $('#appSelectDialogModal').attr('data-request-id');

        $('#appSelectDialogModal').modal('hide');
        showDialogModal(user, user2, type, 'ticket', request);
    });

    $(document).on('click', '.transport-prop-message', function () {
        var user = $(this).closest('.cargo-prop-row').attr('data-request-user-id'),
            user2 = $(this).closest('.cargo-prop-row').attr('data-request-user2-id');

        showDialogModal(user, user2, 'transport', 'prop', $(this).attr('data-id'));
    });
    $(document).on('click', '.cargo-prop-message', function () {
        var user = $(this).closest('.transport-prop-row').attr('data-request-user-id'),
            user2 = $(this).closest('.transport-prop-row').attr('data-request-user2-id');

        showDialogModal(user, user2, 'cargo', 'prop', $(this).attr('data-id'));
    });

    $(document).on('click', '#appDM_send', function () {
        sendMessage();
    });
});