function loadSubscriptions(type) {
    $.konjax('send', window.ssi.api.tickets.get_subscriptions, {
        type: type
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_'+type+'MySubscriptions').html());
                $('.table-'+type+'-my-subscriptions').html(template(response.data));

                $('.'+type+'-subscriptions-count').text( response.data.requests.length );
            }
        }
    });
}

$(function () {
    $('a[href="#requests-transport-my-subscriptions"]').on('shown.bs.tab', function (e) {
        if(typeof e.relatedTarget !== 'undefined'){
            loadSubscriptions('transport');
        }
    });

    $('a[href="#requests-cargo-my-subscriptions"]').on('shown.bs.tab', function (e) {
        if(typeof e.relatedTarget !== 'undefined'){
            loadSubscriptions('cargo');
        }
    });
});