/*
 * LINKeR
 */

/**
 * Created by LINKeR on 24.05.16.
 */

console.log('partner here');
$(document).on('click','#feedback-request-based-partners button[name="refresh"]',
    function(e){ e.preventDefault(); window.linker_profile_request_partners.load( 1 ); });

$(document).on('change','#feedback-request-based-partners select[name="sort"]',
    function(e){ e.preventDefault(); window.linker_profile_request_partners.load( 1 ); });

$(document).on('click','#feedback-request-based-partners button[name="partner-request"]',
    function(e){ e.preventDefault(); $('#feedback-request-based-partners select[name="sort"]').val('i-must-confirm').change() });

$(document).on('click','#feedback-request-based-partners ul.pagination li:not(.active) a',function (e) {
    e.preventDefault(); window.linker_profile_request_partners.load( $(this).attr('data-page') ); });

$(document).on('click','' + '#partner-request-decline button[name="onConfirm"]', function(e){ e.preventDefault();

    var companyID = $(this).val().split('|');
    var type        = companyID[1];
    var page        = companyID[2];
    companyID       = companyID[0];

    window.linker_profile_request_partners._sys.confirm(companyID, type, page);

});



$(document).on('click', '.partner .manage-linker [data-confirmation]', function(e){

    e.preventDefault();

    var confirm = !!($(this).attr('data-confirmation') == 'on');

    var type    = $(this).closest('.manage-linker');

    var companyID = $(this).attr('data-id');

    var companyName = $(this).closest('.partner').find('.company-name-linker').text();

    var page = $('#feedback-request-based-partners ul.pagination li.active a');

    if(page.length){
        page = page.attr('data-page');
    }else{
        page = 1;
    }

    switch(true){

        case (type.hasClass('wait')):           type = 'wait'; break;
        case (type.hasClass('not-confirmed')):  type = 'confirm'; break;
        case (type.hasClass('confirmed')):      type = 'unlink'; break;

        default: window.ssi.error('undefined type of request in ', $(this));

    }

    if(confirm){

        if(type == 'confirm'){

            window.linker_profile_request_partners._sys.accept(companyID, type, page)

        }

    }else{

        var replacement = {"[:companyName:]": companyName};

        var message = i18n('do.confirm');

        switch (type){

            case ('wait'):      message = i18n('partner.request.confirm.declining.of.your.request[:companyName:]', replacement); break;
            case ('confirm'):   message = i18n('partner.request.confirm.declining.of.request.from[:companyName:]', replacement); break;
            case ('unlink'):    message = i18n('partner.request.confirm.breaking.partnership.from[:companyName:]', replacement); break;

        }

        window.ssi.dialog.confirmation('partner-request-decline', message, companyID + '|' + type + '|' + page);

    }

});

window.linker_profile_request_partners = {

    load: function(page){
        window.linker_profile_request_partners._sys.load( window.linker_profile_request_partners._sys.assign, page );
    },

    _sys: {

        assign: function(data){

            var source = $('#profile-partners-list-blocks').html();
            var template = Handlebars.compile(source);
            var html = template({
                companies: data.data.partners
            });

            $('#feedback-request-based-partners .reviews .oneCompanyMainFigureContent .row').html( html );

            $('#feedback-request-based-partners button[name="partner-request"]').val(data.data.count_i_must_confirm);
            $('#feedback-request-based-partners button[name="partner-request"] span').text(data.data.count_i_must_confirm);

            if(data.data.pages != 0){

                var pagination          = $('#hb-bs-pagination').html();
                var pagination_template = Handlebars.compile(pagination);
                var pagination_html     = pagination_template({
                    currentPage:    data.data.current,
                    totalPage:      data.data.pages,
                    size:           data.data.step,
                    options:        {}
                });
            }else{
                var pagination_html = '';
            }

            $('#feedback-request-based-partners ul.pagination').html(pagination_html);

        },

        load:   function(callback, page){

            $.konjax('send',window.ssi.api.partner.list, {
                page:page,
                type: $('#feedback-request-based-partners select[name="sort"] option:selected').val(),
                lang_id: window.ssi.locale.id,
                lang: window.ssi.locale.uri

            },{

                data_processor: {

                    success: function(response){

                        callback(response);

                    }

                }

            });

        },

        confirm: function( companyID, type, page ){

            $.konjax('send', window.ssi.api.partner.decline, {
                companyID:  companyID,
                type:       type,
                page:       page,
                lang_id: window.ssi.locale.id,
                lang: window.ssi.locale.uri
            },{
                data_processor:{
                    success: function (r) {
                        window.linker_profile_request_partners._sys.assign(r);
                    }
                }
            });

        },

        accept: function ( companyID, type, page ) {

            $.konjax('send', window.ssi.api.partner.accept, {
                companyID:  companyID,
                type:       type,
                page:       page,
                lang_id: window.ssi.locale.id,
                lang: window.ssi.locale.uri
            },{
                data_processor:{
                    success: function (r) {
                        window.linker_profile_request_partners._sys.assign(r);
                    }
                }
            });

        },

        _:''

    }


};