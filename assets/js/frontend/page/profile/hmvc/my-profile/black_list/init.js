


$(document).on('click','#infoblock a[href="#black_list"]', function(e){

    e.preventDefault();

    window.linker_profile_request_black_list.loading(1);
});

$(document).on('click','#infoblock ul.pagination li:not(.active) a', function (e){

    e.preventDefault();

    window.linker_profile_request_black_list.loading($(this).attr('data-page'));
});

window.linker_profile_request_black_list = {

    loading: function(page){
        window.linker_profile_request_black_list._sys.list(window.linker_profile_request_black_list._sys.assign, page);
    },

    _sys: {

        assign: function (data) {

            var source = $('#profile-partners-black-list').html(),
                template = Handlebars.compile(source),
                html = template({black_list: data.data.black_list});

            $('#black_list .table-responsive').html(html);

            if (data.data.pages != 0) {

                var pagination = $('#hb-bs-pagination').html(),
                    pagination_template = Handlebars.compile(pagination),
                    pagination_html = pagination_template({
                        currentPage: data.data.current,
                        totalPage: data.data.pages,
                        size: data.data.step,
                        options: {}
                    });

            } else {
                pagination_html = '';
            }

            $('#black_list ul.pagination').html(pagination_html);

        },

        list: function (callback, page) {

            $.konjax('send', window.ssi.api.complaints.getBlackList, {
                page: page,
                lang_id: window.ssi.locale.id
            }, {
                data_processor: {
                    success: function(response){
                        callback(response);
                    }
                }
            });

        }

    }

};

$(document).on('click', '.del-with-list', function(){

    var companyID = $(this).attr('data-company');

    $.konjax('send', window.ssi.api.complaints.delWithBlackList, {
        companyID: companyID
    }, {
        data_processor: {
            success: function(){
                console.log('success');
            }
        }
    });

    window.linker_profile_request_black_list.loading(1);

});