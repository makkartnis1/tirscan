/**
 * Created by LINKeR on 18.05.16.
 */
$(document).on('click','#feedback-of-requests button[name="refresh"]', function(){

    window.linker_profile_request_feedback.all.read( window.linker_profile_request_feedback.all.assign );

});

$(document).on('click', '#feedback-of-requests .pagination.center li a', function (e) {

    e.preventDefault();

    if($(this).parent().is(':not(.active)')){

        window.linker_profile_request_feedback.all.read(
            window.linker_profile_request_feedback.all.assign,
            $(this).attr('data-page')
        );

    }


});

$(document).on('change', '#feedback-of-requests select[name="sort"]', function (e) {

    e.preventDefault();

    window.linker_profile_request_feedback.all.read(
        window.linker_profile_request_feedback.all.assign,
        1
    );

});

window.linker_profile_request_feedback = {

    all: {

        read: function (callback) {

            var sort = $('#feedback-of-requests select[name="sort"] option:selected').val();

            var page = ( typeof arguments[1] === 'undefined' ) ? 1 : arguments[1];

            $.konjax('send',window.ssi.api.requestFeedback.get,{
                sort:           sort,
                page:           page,
                locale_id:      window.ssi.locale.id,
                locale_uri:     window.ssi.locale.uri
            },{

                data_processor: {

                    success: function (response) {

                        callback(response);

                    }

                }

            })

        },

        assign: function(response){

            window.ssi.log('request feedback list response >> ', response);

            var source = $('#hb-profile-feedback-review').html();
            var template = Handlebars.compile(source);
            var html = template({elements:response.data.result});

            $('#feedback-of-requests .info-profile.reviews').html(html);

            if(response.data.pages != 0){

                var pagination          = $('#hb-bs-pagination').html();
                var pagination_template = Handlebars.compile(pagination);
                var pagination_html     = pagination_template({
                    currentPage:    response.data.current,
                    totalPage:      response.data.pages,
                    size:           response.data.step,
                    options:        {}
                });
                
            }else{
                var pagination_html = '';
            }

            $('#feedback-of-requests .pagination').html(pagination_html);
            
            


        }


    }

};