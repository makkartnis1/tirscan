/**
 * Created by LINKeR on 28.04.16.
 */

window.form_profile_info = '#tab-infoblock-myinfo form';

$(function () {
    if ($('a[href="#profile"]').parent().hasClass('active')) {
        $.konjax('send', ssi.api.profile.getInfo, {}, {

            data_processor:{

                success: function(resp){

                    func_assign_data_profile_info(resp.data);
                    func_assign_comapny_info(resp.data);


                    var hidden = '<input type="hidden" name="primaryLocaleID" value="' + resp.data.user.hidden['primaryLocaleID'] + '"/>';

                    $(window.form_profile_info).append(hidden);

                }

            }

        })
    }
});

function func_assign_data_profile_info(data) {

    $.each(data.user.form, function(name, value){

        if( name.indexOf('CodeID') > -1){

            var option = 'select[name="' + name + '"] option[value="' + value + '"]';

            $(window.form_profile_info).find(option).prop('selected', true);

        }else{

            var input = 'input[name="' + name + '"]';

            $(window.form_profile_info).find(input).val(value);

        }


    });

}

function func_assign_comapny_info(response){

    var disabled = !(response.company.isOwner == 'yes');

    $('input, select').inputError('remove');

    $.each(response.company.form, function(name, value){

        var element = $('#tab-infoblock-companyinfo form [name="' + name + '"]');

        switch (true){

            case (element.is('input') || element.is('textarea')):
                element.val(value);
                break;

            case (element.is('select')):
                element.find('option[value="' + value + '"]').prop('selected', true);
                element = element.closest('select');
                break;

            default: window.ssi.log(name, value,' << undefined company info form elemnt');
        }

        element.prop('disabled', disabled);

    });

    $('select[name="Frontend_Companies[typeID]"]').prop('disabled',true);

    // ------------------------------
    // +++ Заповнення нової таби
    // ------------------------------

    var primaryLocaleID = response.company.form['Frontend_Companies[primaryLocaleID]'];

    $('#tab-infoblock-docs .location [data-phone]').text( response.company.form['Frontend_Companies[phone]'] );
    $('#tab-infoblock-docs .location [data-adress]').text( response.company.form['Frontend_Company_Locales[' + primaryLocaleID + '][address]'] );
    $('#tab-infoblock-docs .location [data-email]').text( response.company.ownerEmail );

}

    $('a[href="#profile"]').on('shown.bs.tab', function (e) {

        // TODO: вродь пофіксили, але вродь хз
        if(typeof  e.relatedTarget !== 'undefined'){
            
            console.log('>> tab open infoblock');

            if( $(window.form_profile_info).find('input[name="primaryLocaleID"]').length == 0 ){

                $.konjax('send', ssi.api.profile.getInfo, {}, {

                    data_processor:{

                        success: function(resp){

                            func_assign_data_profile_info(resp.data);
                            func_assign_comapny_info(resp.data);


                            var hidden = '<input type="hidden" name="primaryLocaleID" value="' + resp.data.user.hidden['primaryLocaleID'] + '"/>';

                            $(window.form_profile_info).append(hidden);

                        }

                    }
                    
                })

            }


        }


    });

$(document).on('submit','#tab-infoblock-companyinfo form', function(e){

    e.preventDefault();

    var form = $(this);

    var formData = $(form).serializeJSON();

    $(form).find('input, select').inputError('remove');

    $.konjax('send', window.ssi.api.company.saveInfo, {post: formData },

    {

        data_processor: {

            success: function (resp) {
                window.ssi.log('save comapny info SUCCESS >> ', resp);
                // func from /assets/js/frontend/page/profile/hmvc/info.js
                // func_assign_data_profile_info(resp.data);

            },
            error: function(resp){

                window.ssi.log('save comapny info ERROR >> ', resp);

                if(typeof  resp.data.orm !== 'undefined'){

                    $.each(resp.data.orm, function(field, validation_row){

                        switch(field){

                            case ('name'):
                            case ('address'):
                            case ('realAddress'):
                                var input = $('#tab-infoblock-companyinfo [name="Frontend_Company_Locales[' + primaryLocaleID + '][' + field +']"]');
                                break;

                            case ('typeID'):
                            case ('ipn'):
                            case ('ownershipTypeID'):
                            case ('phone'):
                            case ('phoneStationary'):

                            case ('phoneCodeID'):
                            case ('phoneStationaryCodeID'):
                                var input = $('#tab-infoblock-companyinfo [name="Frontend_Companies[' + field +']"]');
                                break;

                            default: return true;

                        }

                        input.inputError('add', i18n_validation(validation_row));


                    });

                }

            }

        }


    });



});
