/**
 * Created by LINKeR on 28.04.16.
 */
// $(function(){
//
//     if(window.location.hash.length){ //якщо задана інформація про #хеш
//
//         $('a[data-toggle="tab"][href="' + window.location.hash + '"]').tab('show'); // відкриваємо поточну табу, всі залежносні описуємо в середнні
//         console.log('here');
//     }
//
// });

$(function(){
    //$('')
    $(document).on('click', '.tabs-left li:not(.active) a', function(){
        history.pushState({anchor: $(this).attr('href')}, '', $(this).attr('href'));
    });

    window.onpopstate = function(event) {
        $('a[href="'+event.state.anchor+'"]').click();
    };

    $(document).on('click', '.go-to-infoediting', function () {
        $('[href="#profile"]').click();
        $('[href="#tab-infoblock-companyinfo"]').click();
    });

    $(document).on('click', '.go-to-wallet', function () {
        $('[href="#wallet"]').click();
        $('[href="#wallet-purchase"]').click();
    });

});