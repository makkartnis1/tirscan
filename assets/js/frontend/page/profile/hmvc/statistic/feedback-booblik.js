/*
 * LINKeR
 */

/**
 * Created by LINKeR on 30.05.16.
 */

function init_feedback_statistic_booblik() {

    var init = $('#feedback-statistic'); $('#feedback-statistic-preview').remove();

    $('#feedback-statistic').parent().append( $('<canvas id="feedback-statistic-preview"></canvas>') );

    var canvas = $('#feedback-statistic-preview')[0].getContext("2d");

    init = JSON.parse(init.html());

    window.ssi.log(init);

    var labels = [];

    $.each(init.labels, function(i, name){
        labels.push( i18n(name) );
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: init.data,
                backgroundColor: [
                    "rgba(21,   163,    51, 1)",
                    "rgba(223,  79,     55, 1)"

                ],
                hoverBackgroundColor: [
                    "rgba(21,   163,    51, 0.8)",
                    "rgba(223,  79,     55, 0.8)"
                ]
            }]
    };

    var myDoughnutChart = new Chart(canvas, {
        type: 'doughnut',
        data: data,
        options: {
            responsive: true,
            legend: {
                display: false
            }
        }
    });

}