/*
 * LINKeR
 */

/**
 * Created by LINKeR on 30.05.16.
 */


function init_feedback_statistic_line() {
    
    var init = $('#feedback-statistic-line'); $('#feedback-statistic-line-preview').remove();
    
    $('#feedback-statistic-line').parent().append( $('<canvas id="feedback-statistic-line-preview"></canvas>') );

    var canvas = $('#feedback-statistic-line-preview')[0].getContext("2d");

    init = JSON.parse(init.html());

    window.ssi.log('init feedback stat chart', init);

    var labels = init.labels.list;

    var before = init.hint.before;
    var from = (init.hint.i18n)     ? i18n(init.hint.from):     init.hint.from;
    var to = (init.hint.i18n)       ? i18n(init.hint.to):       init.hint.to;

    $('#feedback-statistic-line-preview').closest('.panel').find('.panel-title [data-from]').text(before + ' ' + from);
    $('#feedback-statistic-line-preview').closest('.panel').find('.panel-title [data-to]').text(before + ' ' + to);

    if(init.labels.i18n){

        labels = [];

        $.each(init.labels.list, function(i, name){

            labels.push( i18n(name) );

        });

    }

    var dataset     = $('#statistic form [name="show"]:checked').val();
    var data_negative = init[dataset].negative.data;
    var data_positive = init[dataset].positive.data;

    var data = {
        labels: labels,

        datasets: [
            {
                label: i18n(init.dataset.negative.i18n_name),
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(223,79,55,0.4)",
                borderColor: "rgba(223,79,55,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(223,79,55,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(246,211,205,1)",
                pointHoverBorderColor: "rgba(223,79,55,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: data_negative
            },

            {
                label: i18n(init.dataset.positive.i18n_name),
                fill: false,
                lineTension: 0.1,
                backgroundColor: "rgba(21,163,51,0.4)",
                borderColor: "rgba(21,163,51,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(21,163,51,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(197,231,204,1)",
                pointHoverBorderColor: "rgba(21,163,51,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: data_positive
            }
        ]
    };

    var max_value = 0;

    $.each(data_negative, function(i, val){
        if(val > max_value)
            max_value = val;
    });


    $.each(data_positive, function(i, val){
        if(val > max_value)
            max_value = val;
    });

    max_value = Math.ceil( max_value* 1.2 ); // збільшуємо градацію по осі Y на 20%, щоб графік виглядав більш комфортно
    max_value = (max_value > 0) ? max_value : 10;

    var myDoughnutChart = new Chart(canvas, {
        type: 'line',
        data: data,
        options: {
            responsive: true,
            legend: {
                display: true
            },
            scales: {
                yAxes: [{
                    ticks: {
                        max: max_value,
                        min: 0,
                        stepSize: Math.ceil( max_value / 500 ) * 100    // заокруглюємо до 100 кожен крок градації, (максимум кроків = 5)
                    }
                }]
            }
        }
    });

}