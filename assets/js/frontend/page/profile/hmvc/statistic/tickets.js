/*
 * LINKeR
 */

/**
 * Created by LINKeR on 30.05.16.
 */

function init_ticket_statistic_booblik(type){

    var chart_data_id   = '';
    var chart_canvas_id = '';

    switch (type){

        case('transport'):
            chart_data_id   = 'transport-request-booblik';
            chart_canvas_id = chart_data_id + '-preview';
            break;

        case('cargo'):
            chart_data_id   = 'cargo-request-booblik';
            chart_canvas_id = chart_data_id + '-preview';
            break;

        default: window.ssi.error('undefined type of statistic');   return;
    }


    var init = $('#' + chart_data_id); $('#' + chart_canvas_id).remove();

    $('#' + chart_data_id).parent().append( $('<canvas id="' + chart_canvas_id + '"></canvas>') );

    var canvas = $('#' + chart_canvas_id)[0].getContext("2d");

    init = JSON.parse(init.html());

    window.ssi.log(init);

    var labels = [];

    $.each(init.labels, function(i, name){
        labels.push( i18n(name) );
    });

    var data = {
        labels: labels,
        datasets: [
            {
                data: init.data,
                backgroundColor: [
                    // 'rgba(127,127,127,  0.8)',
                    // 'rgba(21,163,51,    0.8)',
                    // 'rgba(70,107,232,   0.8)',
                    // 'rgba(139,107,232,  0.8)',
                    // 'rgba(255,204,0,    0.8)'

                    'rgba(127,127,127,  1)',
                    'rgba(21,163,51,    1)',
                    'rgba(70,107,232,   1)',
                    'rgba(139,107,232,  1)',
                    'rgba(255,204,0,    1)'
                ],
                hoverBackgroundColor: [
                    'rgba(127,127,127,  0.8)',
                    'rgba(21,163,51,    0.8)',
                    'rgba(70,107,232,   0.8)',
                    'rgba(139,107,232,  0.8)',
                    'rgba(255,204,0,    0.8)'
                ]
            }]
    };

    var myDoughnutChart = new Chart(canvas, {
        type: 'doughnut',
        data: data,
        options: {
            responsive: true,
            legend: {
                display: false
            }
        }
    });


}

function init_ticket_statistic_chart(type) {

    var chart_data_id   = '';
    var chart_canvas_id = '';

    switch (type){

        case('transport'):
            chart_data_id   = 'transport-request-chart';
            chart_canvas_id = chart_data_id + '-preview';
            break;

        case('cargo'):
            chart_data_id   = 'cargo-request-chart';
            chart_canvas_id = chart_data_id + '-preview';
            break;

        default: window.ssi.error('undefined type of statistic');   return;
    }

    var init = $('#' + chart_data_id); $('#' + chart_canvas_id).remove();

    $('#' + chart_data_id).parent().append( $('<canvas id="'+ chart_canvas_id+'"></canvas>') );

    var canvas = $('#'+ chart_canvas_id)[0].getContext("2d");

    init = JSON.parse(init.html());

    window.ssi.log('init transport stat chart', init);

    var labels = init.labels.list;

    var before = init.hint.before;
    var from = (init.hint.i18n)     ? i18n(init.hint.from):     init.hint.from;
    var to = (init.hint.i18n)       ? i18n(init.hint.to):       init.hint.to;

    $('#' + chart_canvas_id).closest('.panel').find('.panel-title [data-from]').text(before  + from);
    $('#' + chart_canvas_id).closest('.panel').find('.panel-title [data-to]').text(before + to);

    if(init.labels.i18n){

        labels = [];

        $.each(init.labels.list, function(i, name){

            labels.push( i18n(name) );

        });

    }



    var init_dataset = function (i18n_const, R,G,B, data) {
        return{
            label: i18n(i18n_const),
            backgroundColor: "rgba("+R+","+G+","+B+",0.5)",
            borderColor: "rgba("+R+","+G+","+B+",1)",
            // borderWidth: 0,
            // pointRadius: 1,
            tension: 0,
            hoverBackgroundColor: "rgba("+R+","+G+","+B+",0.7)",
            hoverBorderColor: "rgba("+R+","+G+","+B+",1)",
            data: data
        };
    };

    var chart_name = init.dataset[0].i18n_name;
    var dataset = init[ $('#statistic form [name="show"]:checked').val() ][0].data;

    var data = {
        labels: labels,
        datasets: [

            // init_dataset(
            //     init.dataset.outdated.i18n_name
            //     ,127,127,127,
            //     init.dataset.outdated.data
            // ),
            init_dataset(
                chart_name
                ,21,163,51,
                dataset
            )
            // init_dataset(
            //     init.dataset.confirmed.i18n_name
            //     ,70,107,232,
            //     init.dataset.confirmed.data
            // ),
            // init_dataset(
            //     init.dataset.not_confirmed.i18n_name
            //     ,139,107,232,
            //     init.dataset.not_confirmed.data
            // ),
            // init_dataset(
            //     init.dataset.no_proposition.i18n_name
            //     ,255,204,0,
            //     init.dataset.no_proposition.data
            // )

        ]
    };

    console.log(init);

    var max_value = 0;

    $.each(dataset, function(i, val){
        if(val > max_value)
            max_value = val;
    });

    max_value = Math.ceil( max_value* 1.2 ); // збільшуємо градацію по осі Y на 20%, щоб графік виглядав більш комфортно
    max_value = (max_value > 0) ? max_value : 10;

    new Chart(canvas, {
        type: "line",
        data: data,
        options: {
            legend: {
                display: false
            },
            responsive: true,
            scales: {
                // xAxes: [{
                //     stacked: true
                // }],
                yAxes: [{
                    // stacked: true
                    ticks: {
                        max: max_value,
                        min: 0,
                        stepSize: Math.ceil( max_value / 500 ) * 100    // заокруглюємо до 100 кожен крок градації, (максимум кроків = 5)
                    }
                }]
            }
        }
    });

}