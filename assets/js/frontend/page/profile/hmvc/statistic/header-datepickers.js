/*
 * LINKeR
 */

/**
 * Created by LINKeR on 31.05.16.
 */
$(function(){


    var curYear = new Date().getFullYear();
    var curMonth = new Date().getMonth() + 1;

    var minYear = new Date().getFullYear(); // TODO company registration date
    var minMonth = 2;                       // TODO company registration date, нумерація починається із 0 (0-11)

    $('#statistic [data-this-is="datepicker"][name="year"]').datetimepicker({

        format: 'YYYY',
        minDate: new Date(minYear, 1 - 1, 1),
        maxDate: new Date(curYear, 1 - 1, 1)

    });

    $('#statistic [data-this-is="datepicker"][name="month"]').datetimepicker({

        format: 'YYYY/MM',
        minDate: new Date(minYear, minMonth - 1, 1),
        maxDate: new Date(curYear, curMonth - 1, 1)

    });

    $(document)
        .on('focus', '#statistic [data-this-is="datepicker"][name="month"], #statistic [data-this-is="datepicker"][name="year"]',function (e) {

            $(this).closest('.panel').find('input[type="radio"]').prop('checked', true);
            // тут event onChange не зпрацює

        })
        .on('change', '#statistic input[name="group"]', function(e){

            window.profile_stat_linker.load.if_form_was_changed();

        })
        .on('change', '#statistic input[name="show"]', function(e){

            switch ($('#statistic .nav-tabs li.active a').attr('href')){

                case ('#stat-feedback-linker'):             init_feedback_statistic_line(); break;
                case ('#stat-transport-requests-linker'):   init_ticket_statistic_chart('transport'); break;

            }

        })
        .on('dp.hide', '#statistic [data-this-is="datepicker"][name="month"], #statistic [data-this-is="datepicker"][name="year"]',function (e) {

            window.profile_stat_linker.load.if_form_was_changed();
            
            $('#statistic [data-this-is="datepicker"][name="month"], #statistic [data-this-is="datepicker"][name="year"]').blur();

        })
        .on('show.bs.tab', '#statistic .nav-tabs li a[data-toggle="tab"]',function (e) {

            window.profile_stat_linker.load.immediately($(e.target).attr('href'));

        });

    window.profile_stat_linker.load.if_form_was_changed();

});


window.profile_stat_linker = {

    load: {

        if_form_was_changed: function(){

            var form    = $('#statistic form');
            var refresh = false;

            var group_type          = form.find('[name="group"]:checked').val();

            var group_range_year    = form.find('[name="year"]').val();
            var group_range_month   = form.find('[name="month"]').val();

            var group_changed = !!( group_type !== form.find('[name="curGroup"]').val() );
            var range_changed = false;

            form.find('[name="curGroup"]').val(group_type);

            switch (group_type){

                case('year'):

                    range_changed = !!( group_range_year !== form.find('[name="curYear"]').val() )

                    break;

                case('month'):

                    range_changed =  !!( group_range_month !== form.find('[name="curMonth"]').val() )

                    break;

            }

            form.find('[name="curYear"]').val( form.find('[name="year"]').val() );
            form.find('[name="curMonth"]').val( form.find('[name="month"]').val() );

            refresh = ( range_changed || group_changed );

            if(refresh){

                window.profile_stat_linker.load.immediately();

            }

        },

        immediately: function(){
            var form = $('#statistic form');
            var inputs    = $('#statistic form input[type=hidden]');

            var data = {};

            $.each(inputs, function(i, input){

                if( $(input).val() == ''){

                    var value = '';

                    switch ($(input).attr('name')){
                        case ('curMonth'):      value = form.find('[name="month"]').val();          break;
                        case ('curYear'):       value = form.find('[name="year"]').val();           break;
                        case ('curGroup'):      value = form.find('[name="group"]:checked').val();  break;

                        default: window.ssi.error('undefined input', $(input)); return;
                    }

                }

                data[$(input).attr('name')] = $(input).val();

            });

            var url = false;
            var tab = false;

            if(typeof arguments[0] === typeof ''){

                tab = arguments[0];

            }else{

                tab = $('#statistic .nav-tabs li.active a').attr('href');

            }

            switch ( tab ){

                case('#stat-feedback-linker'):              url = window.ssi.api.statistic.feedback;            break;
                case('#stat-transport-requests-linker'):    url = window.ssi.api.statistic.request.transport;   break;
                case('#stat-cargo-requests-linker'):        url = window.ssi.api.statistic.request.cargo;       break;

                default: window.ssi.error('unknown statistic tab: '+ tab ); return;

            }

            $.konjax('send', url, data, {
                data_processor: {
                    success: function (response) {
                        window.profile_stat_linker.assign[response.data.callback]( response.data );
                    }
                }
            })

        }

    },

    assign:{

        feedback: function (data) {

            $('script#feedback-statistic').html(        JSON.stringify(data.proportion) );
            $('script#feedback-statistic-line').html(   JSON.stringify(data.chart) );
            init_feedback_statistic_booblik();
            init_feedback_statistic_line();

            

        },

        transport: function (data) {

            $('script#transport-request-booblik').html(        JSON.stringify(data.proportion) );
            $('script#transport-request-chart').html(   JSON.stringify(data.chart) );
            init_ticket_statistic_booblik('transport');
            init_ticket_statistic_chart('transport');

        },

        cargo: function (data) {

            $('script#cargo-request-booblik').html(        JSON.stringify(data.proportion) );
            $('script#cargo-request-chart').html(   JSON.stringify(data.chart) );
            init_ticket_statistic_booblik('cargo');
            init_ticket_statistic_chart('cargo');

        }

    }

};