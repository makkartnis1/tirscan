/*
 * LINKeR
 */

/**
 * Created by LINKeR on 07.06.16.
 */


$(function(){ linker_my_comp.managers._init(); });

$(document).on('click', '[data-compnay-managers-linker-x-ua] [data-pagination] li a', function (e) {

    e.preventDefault();

    if( $(this).parent().is(':not(.active)') ){
        window.linker_my_comp.managers.read( $(this).attr('data-page'), window.linker_my_comp.managers.assign );
    }




});

$(document).on('click', '[data-compnay-managers-linker-x-ua] [data-table] button[name="delete-manager"]', function () {
    
    window.linker_my_comp.managers.remove( $(this).val() );
    
});

window.linker_my_comp = {
    
    managers: {

        _init: function () {

            var is_active_my_company_tab    = !!(

                $( 'ul.nav li a[href="#my-company"]' )
                    .parent()
                    .hasClass('active')

            );

            var is_active_managers_tab      = !!(

                $( '#my-company ul.nav li a[href="#my-company-managers"]' )
                    .parent()
                    .hasClass('active')

            );

            var require_init                = (

                $('#my-company-managers [data-compnay-managers-linker-x-ua]').attr('data-compnay-managers-linker-x-ua') == ''

            );

            if(require_init){

                if(is_active_managers_tab && is_active_my_company_tab){

                    window.linker_my_comp.managers.read( 1, window.linker_my_comp.managers.assign );

                }else{

                    // ця умова спрацює лише раз! тому й івент буде задекларований 1 раз!

                    $(document).on('shown.bs.tab', '' +
                        'ul.nav li a[href="#my-company"], ' +
                        '#my-company ul.nav li a[href="#my-company-managers"]',
                        function () {

                            linker_my_comp.managers._init();

                        }
                    );

                }

            }

        },
        
        read: function( page, assign_callback ){

            $('#my-company-managers [data-compnay-managers-linker-x-ua]').attr('data-compnay-managers-linker-x-ua', 'ok');
            
            $.konjax('send', window.ssi.api.company.managers.read,{
                page:page,
                loc_id: window.ssi.locale.id
            }, {

                data_processor: {

                    success: function(response){

                        assign_callback( response.data );

                    }

                }

            });
            
        },
        
        assign: function(data){

            if(data.pages != 0){

                var pagination          = $('#hb-bs-pagination').html();
                var pagination_template = Handlebars.compile(pagination);
                var pagination_html     = pagination_template({
                    currentPage:    data.current,
                    totalPage:      data.pages,
                    size:           data.step,
                    options:        {}
                });

                var table = table__build_LINKeRxUA(data.table,{

                    head: {
                        render: true,
                        visible:      ['username', 'manage'],
                        translate: {
                            username: 'company.manager.name',
                            manage: 'manage.company.manager'
                        },
                        enumerate: ( data.current - 1 ) * data.limit + 1,
                        function: i18n

                    },
                    row_callback : function (row) {

                        row.manage = '' +
                            '<button type="button" style="max-width: 180px;"  class="btn btn-primary" name="delete-manager" value="'+ row.userID +'">' +
                                i18n('delete.my.manager') +
                            '</button>'
                        ;

                    }

                });

                $('[data-compnay-managers-linker-x-ua] [data-table]').html( table );

            }else{

                $('[data-compnay-managers-linker-x-ua] [data-table]').html('');

                var pagination_html = '';

            }


            $('[data-compnay-managers-linker-x-ua] [data-pagination]').html(pagination_html);

        },
        
        remove: function (userID) {

            var page = $('[data-compnay-managers-linker-x-ua] [data-pagination] li.active a');

            if( page.length ){

                page = page.attr('data-page');

            }else{

                page = 1;

            }
            
            $.konjax('send', window.ssi.api.company.managers.delete, {userID: userID, page: page, loc_id: window.ssi.locale.id},{

                data_processor:{

                    success: function (response) {

                        window.linker_my_comp.managers.assign( response.data );

                    }

                }

            });
            
        }
        
    } 
    
}