$(function () {
    $('a[href="' + window.location.hash + '"]').click();

    $('#buttonAddMyTransport').click(function () {
        $('#addMyTransport').modal('show');
    });

    $('.transportEditCar_link').click(function () {
        var parent = $(this).parents('.transport-box');

        $('[name="techpassport_car_id"]').val( parent.attr('car_id') );
        $('[name="photo_car_id"]').val( parent.attr('car_id') );

        $('#transportEditCar_documents').html( parent.find('.car-documents').html() );
        $('#transportEditCar_photos').html( parent.find('.car-photos').html() );

        $('#transportEditCar_title').text(parent.find('.car-title').text());
        $('#transportEditCar_size').text(parent.find('.car-size').text());
        $('#transportEditCar_volumeWeight').text(parent.find('.car-volume-weight').text());
    });

    $('[name="car_techpassport_file"]').change(function() {
        $(this).parent().submit();
    });

    $('[name="car_photo_file"]').change(function() {
        $(this).parent().submit();
    });

    $('.transportDeleteCar_link').click(function () {
        var form = $(this).find('.form-delete-car');

        form.submit();
    });
});