/**
 * Created by LINKeR on 26.04.16.
 */
(function( $ ){

    $.fn.inputError = function(action) {

        switch (action){

            case('add'):

                var message = (typeof arguments[1] == 'undefined') ? [] : arguments[1];

                if(typeof message !== typeof []){
                    message = [message];
                }

                return this.each(function (i, node) {

                    var parent = $(node).closest('.form-group');

                    parent.addClass('errorForm');

                    if(!parent.find('ul.errorText').length){

                        parent.append(
                            '<ul class="errorText">' +
                            '</ul>'
                        );

                    }

                    var errorBlock = parent.find('ul.errorText');

                    $.each(message, function (i, msg) {

                        errorBlock.append('<li>'+ msg + '</li>');

                    });
                    
                });

                break;
            case('remove'):

                return this.each(function (i, node) {

                    var parent = $(node).closest('.form-group');

                    parent.removeClass('errorForm');

                    parent.find('ul.errorText').remove();
                });

                break;

            default: console.warn('LINKeRxUA do not think about this method.. Contact with him using skype: linker_ua ');


        }



    };


})( jQuery );