$(function () {


    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $('.q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden') // Use the "div" element next to this for the content
            },
            show: {
                event: 'click'
            },
            hide: {
                event: 'mouseover'
            }
        });
    });
    $('.dropdown-toggle').dropdown();

    $( ".datepicker" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

    // google maps
    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).on('change', '.startPointCargo, .endPointCargo', function () {
        var self = $(this);

        window.setTimeout(function () {
            googleMapsLoaded(self.attr('name'));
        }, 300);
    });

    $(document).on('click', '.deletePlace', function () {
        $(this).parent().remove();
    });

    $('#addLoadPlace').click(function () {
        load_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control startPointCargo" id="startPointCargo'+load_count+'" name="load_'+load_count+'" placeholder="'+i18n('tenders.page.load_point')+'">'+
                '<input type="hidden" name="load_places[]">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
                '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="load_'+load_count+'"]').each(function () {
            autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
        });
    });

    $('#addUnloadPlace').click(function () {
        unload_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control endPointCargo" id="endPointCargo'+unload_count+'" name="unload_'+unload_count+'" placeholder="'+i18n('tenders.page.unload_point')+'">'+
                '<input type="hidden" name="unload_places[]">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
                '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="unload_'+unload_count+'"]').each(function () {
            autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
        });
    });
});

var autocomplete = {},
    load_count = 1,
    unload_count = 1,
    options = {
        types: ['(regions)']
    };

function initialize() {
    $('.startPointCargo, .endPointCargo').each(function () {
        autocomplete[$(this).attr('name')] = new google.maps.places.Autocomplete(this, options, googleMapsLoaded);
    });
}

function googleMapsLoaded(name) {
    var place = autocomplete[name].getPlace(),
        place_input = $('[name="'+name+'"]'),
        place_data_input = place_input.next();

    var result = {
        place_id: place['place_id'],
        name: place['name'],
        full_address: place['formatted_address'],
        lat: place['geometry']['location'].lat(),
        lng: place['geometry']['location'].lng()
    };

    place_input.val(place['formatted_address']);
    place_data_input.val(JSON.stringify(result));
}