Tirscan = {
    ajax: function (action, params, done) {
        $.ajax({
                method: "POST"
                , url: "/ajax/" + action
                , data: params
                , dataType: "json"
            })
            .done(function (data) {
                if (DEBUG_MODE) {
                    console.log('----- ACTION -----');
                    console.table(action);
                    console.log('----- DATA -----');
                    console.table(params);
                    console.log('----- RESPONSE -----');
                    console.table(data);
                }

                if (typeof (done) !== "undefined")
                    done();
            });
    }
};

function addView(tid, ttype) {
    $.konjax('send', window.ssi.api.tickets.request_add_view, {
        id: tid,
        type: ttype
    }, {
        data_processor: {
            success: function (response) {
            }
        }
    });
}

// при клікові на модальні кнопки соцмереж емулюєм клік по відповідній прихованій кнопці uLogin
$(function () {
    $('.social-link').click(function () {
        $($(this).attr('data-target')).click();
    });
    $('#collapseExample').on('show.bs.collapse', function () {
        $(".crossRotate > i").css("transform", "rotate(180deg)");
    });
    $('#collapseExample').on('hide.bs.collapse', function () {
        $(".crossRotate > i").css("transform", "");
    });

    $(document).on('click', 'td a', function (e) {
        e.stopPropagation();
    });
});

// seo
// $(function () {
//     $('.seo_block_list').click(function () {
//         $(this).css('text-overflow', 'inherit');
//         $(this).css('white-space', 'inherit');
//         $(this).css('overflow', 'auto');
//     });
// });