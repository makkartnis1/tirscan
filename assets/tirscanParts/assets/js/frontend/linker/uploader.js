

/*
 * LINKeR
 */

$(document)
    .on('click', '[data-widget-type="manage"] [data-widget="attach"]', function(){

        window.__upload_registeredCallbacks__ = [];

        // Відкриваємо вікно прикріплення файлу

        var config = linker_file_uploader.readConfig( this );

        linker_file_uploader.resetFile();

        linker_file_uploader.assignConfig(config, $(this).closest('[data-widget-type="manage"]'));


        $('[data-this-is="uploader"] form input[type="file"]').click();

    })
    .on('change', '[data-this-is="uploader"] form[data-this-is="upload"] input[type="file"]', function(){

        if($(this)[0].files.length){

            var valid = linker_file_uploader.checkFile( $(this)[0].files[0].name, $(this)[0].files[0].size, $(this)[0].files[0].type, $('[data-this-is="uploader"] form [name="config"]').val());

            if( valid ){

                toastr.success('FILE OK ');

                $('[data-this-is="uploader"] form[data-this-is="upload"] button[type="submit"]').click();

            }else{

                toastr.error('INVALID FILE');

            }

        }

    })
    .on('click', '[data-widget="files"] [data-action="delete"]', function(e){
        e.preventDefault();
        window.linker_file_uploader.delete.do_confirm(this);
    })

    .on('click', '.toast button[name="confirm"]', function(e){
        e.preventDefault();
        window.linker_file_uploader.delete.do_delete(this);
    })

;



window.linker_file_uploader = {

    checkFile: function (name, size, mime, config) {

        config = window.ssi.file_uploader_profiles[config];
        
        // перевірка типів перед відправленням
        
        mime_ok = true;

        if( typeof config.mime == typeof [] && config.mime.length ){

            var mime_ok = false;

            var enabled_mime_type = [];

            $.each(config.mime, function(i, m){

                m       = m.toLowerCase().trim();
                mime    = mime.toLowerCase().trim();

                enabled_mime_type.push( i18n('file.mime.' + m) );

                if(m == mime){

                    mime_ok = true;

                    return false;

                }


            });

            if(!mime_ok){

                enabled_mime_type = enabled_mime_type.join(', ');

                toastr.warning(
                    i18n('file.mime.invalid.type.enabled.[:enabled:]', { "[:enabled:]": enabled_mime_type })
                );

            }
            
        }
        
        // перевірка об"му перед відправленням

        var size_ok = true;
        if(typeof config.size === typeof 777 && config.size > 0){

            size_ok = !!(size <= config.size);

            if( !size_ok ){

                var curSize = humanFileSize(size);
                var limSize = humanFileSize(config.size);

                toastr.warning(
                    i18n('file.size.limit.[:lim-size:]([:lim-unit:]).current.[:cur-size:]([:cur-unit:])',
                        {   "[:cur-size:]": curSize[0],
                            '[:cur-unit:]': curSize[1],
                            "[:lim-size:]": limSize[0],
                            '[:lim-unit:]': limSize[1],
                        })
                );

            }

        }

        return !!(size_ok && mime_ok);

    },

    assignConfig: function(config, widget){

        var form = $('[data-this-is="uploader"] form[data-this-is="upload"]');

        form.find('input[name="token"]'              ).val(config.u_tkn          );
        form.find('input[name="id"]'                 ).val(config.u_id           );
        form.find('input[name="location"]'           ).val(config.u_loc          );
        form.find('input[name="dir"]'                ).val(config.u_dir          );
        form.find('input[name="config"]'             ).val(config.u_conf_name    );
        form.find('input[name="config-token"]'       ).val(config.u_conf_tkn     );
        form.find('input[name="init"]'               ).val(config.init           );

        var UID = window.linker_file_uploader.helper.widgetUID(widget);

        form.find('input[name="callback"]').val('linker_uploader_assign_after_upload|' + UID);

    },

    resetFile: function(){
        $('[data-this-is="uploader"] form input[type="file"]').replaceWith( $('[data-this-is="uploader"] form input[type="file"]').clone(true) );
    },

    readConfig: function(that){

        var element = $(that).find('[data-uploader="info"]');
        
        var widget = $(that).closest('[data-widget="files"][data-init]');

        return {
            u_tkn:          $(element).attr('data-upload-token'),
            u_id:           $(element).attr('data-upload-id'),
            u_loc:          $(element).attr('data-upload-location'),
            u_dir:          $(element).attr('data-upload-dir'),
            u_conf_name:    $(element).attr('data-upload-config'),
            u_conf_tkn:     $(element).attr('data-upload-config-token'),
            init:           widget.attr('data-init')
        };

    },
    delete:{

        do_confirm: function(that){

            var file = {"[:filename:]": $(that).attr('data-file')};

            var content = $('<div>' +
                i18n( "uploader.confirm.message.delete [:filename:]", file ) +
                '</br>' +
                '<button class="btn btn-danger pull-left"   name="confirm" type="button">'      + i18n('btn.yes')   +    '</button>' +
                '<button class="btn btn-default pull-right" name="decline" type="button">'      + i18n('btn.no')    +    '</button>' +
                '</div>');

            var data_attrs = ['url', 'location', 'id', 'dir', 'file', 'token'];

            $.each(data_attrs, function(i, attr){

                content.find('[name="confirm"]').attr('data-'+ attr , $(that).attr('data-' + attr) );

            });

            toastr.info( content.html(), i18n('uploader.confirm.title.delete [:filename:]',file) );

        },
        do_delete: function(that){

            var form = $('[data-this-is="uploader"] form[data-this-is="delete"]');

            form.find('input').val('');

            var widget = $('[data-widget="files"][data-init]' +
                ' ' +
                '[data-location="'          +   $(that).attr('data-location')   +  '"]' +
                '[data-id="'                +   $(that).attr('data-id')         +  '"]' +
                '[data-dir="'               +   $(that).attr('data-dir')        +  '"]' +
                '[data-file="'              +   $(that).attr('data-file')       +  '"]' +
                '[data-token="'             +   $(that).attr('data-token')      +  '"]' +
            '').closest('[data-init]');

            var UID = window.linker_file_uploader.helper.widgetUID(widget);

            var params = widget.attr('data-init');

            form.find('input[name="token"]').val(       $(that).attr('data-token') );
            form.find('input[name="file"]').val(        $(that).attr('data-file') );
            form.find('input[name="location"]').val(    $(that).attr('data-location') );
            form.find('input[name="id"]').val(          $(that).attr('data-id') );
            form.find('input[name="dir"]').val(         $(that).attr('data-dir') );
            form.find('input[name="init"]').val(        params );
            form.find('input[name="callback"]').val('linker_uploader_assign_after_delete|' + UID);

            var widget = $(that).closest('[data-widget="files"]');

            if( widget.find('[data-widget="attach"]').length ){
                // є блок управління файлами, пеедаємо token на модифікацію,
                // щоб в результат попали токени на мапіпуляцію файлами

                form.find('input[name="token/upload"]').val( widget.find('[data-widget="attach"] [data-upload-token]').attr('data-upload-token') );

            }

            form.find('[type="submit"]').click();

        }


    },
    
    helper: {
        
        widgetUID: function (widget) {

            var UID = $(widget).attr('data-uid');

            if( typeof UID !== 'undefined'){

                return UID;

            }

            var UID = window.linker_file_uploader.helper._randUID();

            while($('[data-widget="files"][data-uid="' + UID + '"]').length){

                UID = window.linker_file_uploader.helper._randUID();

            }

            $(widget).attr('data-uid', UID);

            return UID;
            
        },
        _randUID: function () {

            return '_' + Math.random().toString(36).substr(2, 9);
            
        }
        
    }


};

window.linker_uploader_assign_after_delete = function (uid, response) {

    if(response.delete){

        toastr.success( i18n('uploader.file.delete.success') );

    }else{

        toastr.error( i18n('uploader.file.delete.failed') );

    }

    $('[data-widget="files"][data-uid="' + uid + '"] .companyDocsHolder > .row').html('');

    $('[data-widget="files"][data-uid="' + uid + '"] .companyDocsHolder > .row')
        .replaceWith( $(response.html).find('.companyDocsHolder > .row') );

};

window.linker_register_upload_callback = function(myTempFunction) {
    window.__upload_registeredCallbacks__.push(myTempFunction);
};

window.__upload_registeredCallbacks__ = [];



window.linker_uploader_assign_after_upload = function (uid, response) {

    $.each(response.messages, function (type, messages) {

        $.each(messages, function (i, message) {

            if(typeof message === typeof ''){

                toastr[type](message)

            }else{

                if(typeof message.sizeErr === typeof ''){

                    var curSize = humanFileSize(message.size.cur);
                    var limSize = humanFileSize(message.size.lim);
                    
                    

                    toastr[type](
                        i18n('file.size.limit.[:lim-size:]([:lim-unit:]).current.[:cur-size:]([:cur-unit:])',
                            {   "[:cur-size:]": curSize[0],
                                '[:cur-unit:]': curSize[1],
                                "[:lim-size:]": limSize[0],
                                '[:lim-unit:]': limSize[1],
                            })
                    );
                    
                }

                if( typeof message.mime === typeof ''){

                    toastr[type]( i18n('file.mime.invalid.type.enabled.[:enabled:]', message.replace) );

                }

            }

        });

    });

    $('[data-widget="files"][data-uid="' + uid + '"] .companyDocsHolder > .row').html('');

    $('[data-widget="files"][data-uid="' + uid + '"] .companyDocsHolder > .row')
        .replaceWith( $(response.html).find('.companyDocsHolder > .row') );


    $.each(window.__upload_registeredCallbacks__, function(i, callback){

        window.ssi.log('callback    >>  ', callback);
        
        callback(uid, response);

    });

    window.__upload_registeredCallbacks__ = [];

};

window.humanFileSize = function (bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);

    return [bytes.toFixed(1), units[u]];
};