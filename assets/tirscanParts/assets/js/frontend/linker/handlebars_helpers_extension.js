/**
 * Created by LINKeR on 13.05.16.
 */
Handlebars.registerHelper('if_equal_hard', function(a, b, opts) {
    if(a === b) // Or === depending on your needs
        return opts.fn(this);
    else
        return opts.inverse(this);
});
Handlebars.registerHelper('if_equal_soft', function(a, b, opts) {
    if(a == b) // Or === depending on your needs
        return opts.fn(this);
    else
        return opts.inverse(this);
});

Handlebars.registerHelper('pagination', function(currentPage, totalPage, size, options) {

    totalPage = options.data.root.totalPage;

    var startPage, endPage, context;

    if (arguments.length === 3) {
        options = size;
        size = 5;
    }

    startPage = currentPage - Math.floor(size / 2);
    endPage = currentPage + Math.floor(size / 2);

    if (startPage <= 0) {
        endPage -= (startPage - 1);
        startPage = 1;
    }

    if (endPage > totalPage) {
        endPage = totalPage;
        if (endPage - size + 1 > 0) {
            startPage = endPage - size + 1;
        } else {
            startPage = 1;
        }
    }

    context = {
        startFromFirstPage: false,
        pages: [],
        endAtLastPage: false,
    };

    if (startPage === 1) {
        context.startFromFirstPage = true;
    }

    for (var i = startPage; i <= endPage; i++) {
        context.pages.push({
            page: i,
            isCurrent: i === currentPage,
        });
    }

    if (endPage === totalPage) {
        context.endAtLastPage = true;
    }

    context.lastPage = totalPage;

    return options.fn(context);
    
});

Handlebars.registerHelper('car_avatar', function(avatar, opts) {

    if( (avatar === null) || (avatar == '') )
        return opts.inverse(this);
    else
        return opts.fn(this);

});

Handlebars.registerHelper('thead_tr', function (_columns) {

    // columns = {
    //     enumerate_starts: false| INT > 0, // чи показувати нумерацію рядків
    //     enumerate_caption: '#'
    //     list:[
    //         column1: 'i18n.column.key',
    //     ]
    // }

    var enumerate   = '';
    var caption     = '';
    var columns     = '';

    if(typeof _columns.enumerate_starts === typeof 0){

        if(typeof _columns.enumerate_caption === typeof ''){

            caption = '#';

        }else{

            caption = _columns.enumerate_caption;

        }

        enumerate = '<th>' + caption + '</th>';

    }

    $.each(_columns.list, function(column, translation){
        columns = columns + '<th>' + i18n(translation) + '</th>';
    });

    return '<tr>' + enumerate + columns + '</tr>';

});
