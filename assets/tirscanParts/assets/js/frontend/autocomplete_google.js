$(function () {
    // google maps
    google.maps.event.addDomListener(window, 'load', initialize);

    // $(document).keydown(function (e) {
    //     if (e.which == 9) {
    //         var obj = $($(".pac-container .pac-item:first").html());
    //
    //         console.log($(".pac-container .pac-item:first"));
    //     }
    //
    // });
});

var autocomplete = {},
    options = {
        types: ['(regions)']
    },
    options_cities = {
        types: ['(cities)']
    };

function addAutocomplete(self) {
    var name = $(self).attr('name');

    autocomplete[name] = new google.maps.places.Autocomplete(self, options);
    autocomplete[name].addListener('place_changed', googleMapsLoaded);
    autocomplete[name].name = name;
}

function addAutocompleteCities(self) {
    var name = $(self).attr('name');

    autocomplete[name] = new google.maps.places.Autocomplete(self, options_cities);
    autocomplete[name].addListener('place_changed', googleMapsLoaded);
    autocomplete[name].name = name;
}

function addAutocompleteTender(self) {
    var name = $(self).attr('name');

    autocomplete[name] = new google.maps.places.Autocomplete(self, options);
    autocomplete[name].addListener('place_changed', googleMapsLoadedTender);
    autocomplete[name].name = name;
}

function initialize() {
    $('.searchGeo').each(function () {
        if ($(this).attr('data-only-cities') == 'yes') {
            addAutocompleteCities(this);
        }
        else {
            addAutocomplete(this);
        }
    });

    $('.searchGeoTender').each(function () {
        addAutocompleteTender(this);
    });
}

function googleMapsLoaded() {
    var place = this.getPlace(),
        place_input = $('[name="' + this.name + '"]'),
        place_data_input = place_input.next();

    var result = {
        place_id: place['place_id'],
        name: place['name'],
        full_name: place['formatted_address'],
        full_address: place['formatted_address'],
        latitude: place['geometry']['location'].lat(),
        longitude: place['geometry']['location'].lng(),
        places: [],
        country: ''
    };

    $.each(place['address_components'], function (key, value) {
        if (key > 0) {
            result.places.push(value['long_name']);
        }
        result.country = value['short_name'];
    });

    place_input.val(place['formatted_address']);
    place_data_input.val(JSON.stringify(result));
}

function googleMapsLoadedTender() {
    var name = this.name,
        place = this.getPlace(),
        place_input = $('[name="'+name+'"]'),
        place_data_input = place_input.next();

    var post = new Object();
    post.place_id = place.place_id;
    post.latitude = place.geometry.location.lat;
    post.longitude = place.geometry.location.lng;
    post.name = place.name;
    post.full_name = place.formatted_address;
    post.places = [];
    post.country = '';

    $.each(place.address_components, function(key, value){
        if (key > 0)
        {
            post.places.push(value.long_name);
        }
        post.country = value.short_name;
    });

    window.lang = 'uk';

    $.ajax({
            method: "POST",
            url: "/cms/geo/"+window.lang+"/add/",
            data: post
        })
        .done(function( response ) {
            place_data_input.val(response);
        });
}
