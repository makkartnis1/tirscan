/*      KONJAX. v0.85        [KO]hana co[N]troller a[JAX] = [KONJAX]
 *      --------------
 *      |   КОНЬЯКс  | -    плагин отправки ПОСТ запроса на
 *      --------------      KOHANA AJAX CONTROLLER;
 *
 *      https://jsfiddle.net/6kho8xm6/      HOW TO EXTENDS OBJECT!!!
 *
 *      v0.8 -> v0.85 = добавлена змога мультимвоності
 *
 * */
(function(){
    jQuery.konjax = function(func){

        var about = '' +
            '   developed by [SKYPE::LINKER_UA]' + "\r\n" +
            '       Free to use' + "\r\n" +
            '       if posted reference to profile' + "\r\n" +
            '' + "\r\n" +
            '--      --' + "\r\n" +
            '      |   ' + "\r\n" +
            '[________]';


        switch(func){
            // -----------------------------------------------------------------------------------------
            //      INIT        ------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------------
            case('init'):   //$.konjax('init', { url:'/ajax', success: function(){alert(ok)} } )

                var defaults    = {
                    do_debug: false,
                    message_processor:{
                        error:      function(data, xhr){},              //count: errors>0, success = 0
                        warning:    function(data, xhr){},              //count: errors>0, success > 0
                        success:    function(data, xhr){}               //count: errors=0, success >= 0
                    },
                    data_processor:{
                        success:    function(data, xhr){},              //success and warnings
                        error:    function(data, xhr){}                 //errors only
                    },
                    // i18n:    'eng' - файл локалізації для адмінки (v0.85 зміна)

                    before:     function(){},           //use it for freezing page, till waiting response
                    after:      function(){},           //use it for unfreezing page, after response processed
                    url:        '#',
                    _nothing:     ''
                };


                var init = $.extend(true, defaults, arguments[1]);
                window.konjax = { init: init };

                break;
            // -----------------------------------------------------------------------------------------
            //      SEND        ------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------------
            case('send'):   //$.konjax('send', action, obj_to_post{}, spec_init{})
                //$.konjax('send', action, obj_to_post})
                //$.konjax('send', action)

                if(typeof window.konjax !== 'object'){
                    jQuery.konjax('init',{});
                }

                var custom_options      = (typeof arguments[3] === typeof {}) ? $.extend(true, {}, arguments[3]) : {};
                var defaults            = $.extend(true, {}, window.konjax.init);
                var options = {
                    message_processor:  {},
                    data_processor:     {},
                    url:                '#'
                };
                //http://jquerybook.ru/api/jQuery.extend()-fn86.html
                $.extend(true, options, defaults, custom_options);

                // var custom_options      = (typeof arguments[3] === typeof {}) ? $.extend(true, {}, arguments[3]) : {};
                //
                // var defaults            = $.extend(true, {}, window.konjax.init);
                //
                // var options = {
                //     message_processor:  {},
                //     data_processor:     {},
                //     url:                '#'
                // };
                //
                // $.each(defaults, function(k1,p1){
                //
                //     if(typeof p1 == 'object'){
                //
                //         $.each(p1, function(k2, p2){
                //
                //             if(typeof options[k1] != 'object'){
                //                 options[k1] = {};
                //             }
                //
                //             if(typeof custom_options[k1] !== 'undefined'){
                //
                //                 if( typeof custom_options[k1][k2] !== 'undefined'){
                //                     options[k1][k2] = custom_options[k1][k2];
                //                 }else{
                //                     options[k1][k2] = defaults[k1][k2];
                //                 }
                //
                //             }else{
                //
                //                 options[k1][k2] = defaults[k1][k2];
                //
                //             }
                //
                //         });
                //
                //     }else{
                //
                //         if( typeof custom_options[k1] !== 'undefined'){
                //
                //             options[k1] = custom_options[k1];
                //
                //         }else{
                //
                //             options[k1] = defaults[k1];
                //
                //         }
                //
                //     }
                //
                // });

                //var options             = $.extend(true, defaults, custom_options);
                var data                = (typeof arguments[2] === 'object') ? $.parseJSON(JSON.stringify(arguments[2])) : {};
                var action              = arguments[1];

                //console.log(options);

            function msg_success(data, xhr){
                options.message_processor.success(data, xhr);
            };

            function msg_warning(data, xhr){
                options.message_processor.warning(data, xhr);
            };

            function msg_error(data, xhr){
                options.message_processor.error(data, xhr);
            };

            function data_ok(data, xhr){
                options.data_processor.success(data, xhr);
            };

            function data_fail(data, xhr){
                options.data_processor.error(data, xhr);
            };



                options.before();
                var init_url = options.url;
                var urlToPost = ( action.charAt(0) == '/') ? action : init_url + '/' + action;

                var post = {data:data};
                if(typeof options.i18n == 'string'){
                    post.i18n = options.i18n;
                }

                $.post(urlToPost, post,'json').complete(
                    function (xhr) {

                        options.after();

                        var ok = true; var json_data;

                        try{

                            json_data = $.parseJSON(xhr.responseText);

                        }catch(e){

                            ok = false;

                            if(e.name == 'SyntaxError'){

                                alert('SERVER RESPONSE IS INCORRECT');

                            }else{

                                alert('UNDEFINED ERROR');
                            }

                        }

                        if(ok){

                            if (xhr.status !== 200) {

                                alert(json_data.msg.error.join("\r\n"));

                            } else {
                                var _e = json_data.msg.error.length; var _s = json_data.msg.success.length;

                                if(_e == 0 && _s >= 0){

                                    data_ok(json_data, xhr);
                                    msg_success(json_data, xhr);
                                    if(options.do_debug == true){
                                        console.log('data ok','msg_success');
                                    }

                                }else if(_e >= 0 && _s == 0){

                                    data_fail(json_data, xhr);
                                    msg_error(json_data, xhr);
                                    if(options.do_debug == true){
                                        console.log('data fail','msg_error');
                                    }

                                }else if(_e > 0 && _s > 0){

                                    data_ok(json_data, xhr);
                                    msg_success(json_data, xhr);
                                    msg_warning(json_data, xhr);

                                    if(options.do_debug == true){
                                        console.log('data_fail','msg_error && msg_warning');
                                    }

                                }
                                if(options.do_debug == true){
                                    console.log(json_data, xhr);
                                }


                            }

                        }

                    }

                );
                break;
            case ('about'):
                alert(about);
                break;
            // -----------------------------------------------------------------------------------------
            //      DEFAULT     ------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------------
            default: alert('KONJAX UNKNOWN METHOD IS CALLED');
        };

    };


})(window);
