/**
 * Created by LINKeR on 21.04.16.
 */
window.linker_konjax_freezer = '' +
    // '' +
    // '<div style="' +
    // '   background-color: rgba(0,0,0,0.5);' +
    // '   display: none;' +
    // '   width: 100%;' +
    // '   height: 100%;' +
    // '   position: fixed;' +
    // '   left: 0;' +
    // '   top: 0;' +
    // '   z-index: 999999999999999999;' +
    // '" ' +
    // '   class="konjax_request_freezer"' +
    // '>' +
    // '   <div style="">' +
    //'       <i class="fa fa-spinner fa-spin" style="font-size: 2000%; color: rgba(255,255,255,0.5); position: fixed; left:40%; top: 20%;"></i>' +
    // '   </div>' +
    // '</div>' +
    '';

function konjax_freeze(){

    var fr_selector = arguments;
    //
    // if(fr_selector.length == 0){

        fr_selector = ['body'];

    // }

    $.each(fr_selector, function (i, curNodeSelector) {

        $(curNodeSelector).prepend( $(window.linker_konjax_freezer).clone() );
        $(curNodeSelector + ' .konjax_request_freezer').show();
        // console.log('f -',curNodeSelector);
        // $(curNodeSelector).overlay();

    });


}
function konjax_unfreeze(){

    // if(arguments.length > 0){
    // $.each(arguments, function (i, curNodeSelector) {
    //     $( curNodeSelector + ' .konjax_request_freezer' ).remove();
    //
    // });
    // }else{
        $( '.konjax_request_freezer' ).remove();
    // }



}


$(function(){           //  INITIALIZATION

    $.konjax('init',{
        message_processor:{
            success:    function(data){         //count: errors=0, success >= 0
                $(data['msg']['success']).each(function(i,el){
                    Command: toastr["success"](i18n(el));
                });
            },
            error:      function(data){         //count: errors>0, success = 0
                $(data['msg']['error']).each(function(i,el){
                    Command: toastr["error"](i18n(el));
                });
            },
            warning:    function(data){         //count: errors>0, success > 0
                $(data['msg']['error']).each(function(i,el){
                    Command: toastr["warning"](i18n(el));
                });
            }
        },

        data_processor:{
            success: function(data, xhr){},
            error: function(data, xhr){}

        },


        before:     function(){konjax_freeze();},           //use it for freezing page, till waiting response
        after:      function(){konjax_unfreeze();},           //use it for unfreezing page, after response processed

        url:        '#'//controllerAjax.0.85.js
    });

});