$(function () {

    $('#button-add-extisting-transport').click(function () {
        $('#addExistingTransport').modal('hide');

        console.log($('#select-existing-car').val());
        $('[name="existing_car_id"]').val( $('#select-existing-car').val() );

        $('#textSelectedCar').text( $('#select-existing-car option:selected').text() );
        $('#formAddCar').hide();
        $('#formSelectedCar').show();
    });

    $('#linkRemoveSelectedCar').click(function () {
        $('#formAddCar').show();
        $('#formSelectedCar').hide();

        $('[name="existing_car_id"]').val('');
    });

    $('#add-request-submit').click(function () {
        if ( $('[name="terms_accepted"]').prop('checked') == false )
            toastr.error('Ознайомтесь та погодьтесь з правилами на сайті !');
        else
            $('#add-request-form').submit();
    });

    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('.q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden') // Use the "div" element next to this for the content
            },
            show: {
                event: 'click'
            },
            hide: {
                event: 'mouseover'
            }
        });
    });
    $('.dropdown-toggle').dropdown();

    $( ".datepicker" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $(document).on('click', '.deletePlace', function () {
        $(this).parent().remove();
    });

    $('#addLoadPlace').click(function () {
        load_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control startPointCargo" data-only-cities="yes" id="startPointCargo'+load_count+'" name="load_'+load_count+'" placeholder="Пункт (країна) завантаження">'+
                '<input type="hidden" name="load_places['+load_count+']">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
            '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="load_'+load_count+'"]').each(function () {
            addAutocompleteCities(this);
        });
    });

    $('#addUnloadPlace').click(function () {
        unload_count++;

        var new_child = $(
            '<div class="uploadPlaceInputHolder">'+
                '<input type="text" class="form-control endPointCargo" data-only-cities="yes" id="endPointCargo'+unload_count+'" name="unload_'+unload_count+'" placeholder="Пункт (країна) розвантаження">'+
                '<input type="hidden" name="unload_places['+unload_count+']">'+
                '<a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>'+
            '</div>');

        new_child.insertAfter( $(this).parents('.form-group').find('.uploadPlaceInputHolder').last() );
        $('[name="unload_'+unload_count+'"]').each(function () {
            addAutocompleteCities(this);
        });
    });
});

var load_count = $('.startPointCargo').length,
    unload_count = $('.endPointCargo').length;