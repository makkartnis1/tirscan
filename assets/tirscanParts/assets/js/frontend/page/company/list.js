$(function() {
    // barrating плагін для рейтингу компаній
    $('.companyRating').barrating({
        theme: 'fontawesome-stars',
        readonly: true
    });

    $('.krid-select').kridSelect();

    $('[name="f_place"]').change(function () {
        var place_input = $('input[name="f_place"]'),
            place_id_input = $('input[name="f_place_id"]');

        if ( place_input.val() == '' )
            place_id_input.val('');
    });
});