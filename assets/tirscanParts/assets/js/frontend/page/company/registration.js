$(function () {
    $('#registration-submit').click(function () {
        if ( $('[name="terms_accepted"]').prop('checked') == false )
            toastr.error('Ознайомтесь та погодьтесь з правилами на сайті !');
        else
            $('#registration-form').submit();
    });
});