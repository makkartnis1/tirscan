$('a[href="#transport"]').on('shown.bs.tab', function (e) {


    if(typeof  e.relatedTarget !== 'undefined'){

        window.ssi.log('>> tab open transport');

        var content = $('[data-this-is="transport-layout"]');

        if( content.attr('data-require-init') == 'true' ){

            window.linker_transport.load( window.linker_transport.assign, 1 );

        }

    }

});

$(document).on('click', '#modal_edit_my_transport button[name="action"][value="save"]', function(){

    $('#modal_edit_my_transport input[type=text]').inputError('remove');

    $.konjax('send',window.ssi.api.transport.updateTransport, {
        query: $(this).closest('form').serialize(),
        id: $('#modal_edit_my_transport').attr('data-row-id')

    },{

        data_processor:{

            success: function (response) {
                $(this).closest('.modal').attr('data-do-refresh', 'true');
            },

            error: function (response) {
                
                var errors = response.data.errors;

                $.each(errors, function(column, rule){

                    var message = i18n_validation(rule);

                    $('#modal_edit_my_transport [name="' + column + '"] ').inputError('add',message);

                    window.ssi.log(message, column, rule);

                });
            }

        }

    });


});

$(document).on('click', '[data-update-main-car-gallery-image]', function(e){

    $(this).closest('.modal').attr('data-do-refresh', 'true');

    e.preventDefault();

    window.linker_transport.edit_modal.updateImage( $(this).attr('href') );

});

$(document).on('click', '[data-trasnsport="pagination"] li:not(.active) > a',function(e){

    e.preventDefault();

    window.linker_transport.load( window.linker_transport.assign, $(this).attr('data-page') );

});

$(document).on('click','[data-this-is="transport-layout"] [data-action="delete"]', function(){

    var invoker = $(this).closest('.transport-box');
    var id      = invoker.attr('data-id');
    var name    = invoker.find('p.top-inner').text();


    var content = $('<div>' +
        i18n( "transport.my.car.confirm [:carname:]", {"[:carname:]": '<b>' + name + '</b>'}) +
        '</br>' +
        '<button class="btn btn-danger pull-left"   name="confirm-delete-transport" value="' + id + '" type="button">'      + i18n('btn.yes')   +    '</button>' +
        '<button class="btn btn-default pull-right" name="decline" type="button">'      + i18n('btn.no')    +    '</button>' +
        '</div>');

    toastr.info(content.html());


});

$(document).on('click','[name="confirm-delete-transport"]', function(){

    $.konjax('send',window.ssi.api.transport.deleteTransport,{id: $(this).val()},{
        data_processor:{

            success: function(){

                setTimeout(function() {

                    window.linker_transport.load( window.linker_transport.assign, 1 ); // todo read page

                },200)

            }

        }
    })

});


window.linker_transport = {

    load:   function(callback, page){

        $.konjax('send', window.ssi.api.transport.listMyTransport,{page:page},{
            data_processor:{
                
                success:    function (response) {
                    $.each(response.data.result, function(i, row){

                        var title = response.data.result[i].title;

                        $.each(title, function (i, constant) {
                            title[i] = i18n(constant);
                        });

                        response.data.result[i].title = title.join(', ');

                    });

                    callback(response);
                },
                
                error:      function (response) {
                    if(typeof arguments[2] !== 'undefined'){
                        arguments[2](response);
                    }
                }
                
            }
        })

    },

    assign: function(response){

        var source      = $("#hb-template-my-transport-items").html();
        var template    = Handlebars.compile(source);
        var html        = template({cars: response.data.result});

        $('[data-this-is="transport-layout"] [data-trasnsport="list"]').html( html );

        if(response.data.pages != 0){

            var pagination          = $('#hb-bs-pagination').html();
            var pagination_template = Handlebars.compile(pagination);
            var pagination_html     = pagination_template({
                currentPage:    response.data.current,
                totalPage:      response.data.pages,
                size:           response.data.step,
                options:        {}
            });
        }else{
            var pagination_html = '';
        }

        window.ssi.log(response.data.result);


        $('[data-trasnsport="pagination"]').html(pagination_html);

    },

    edit_modal: {

        reset: function(modal){
            
            $(modal).find('.transport-box').html('');
            $(modal).find('[data-block="car-documents"]').html('');
            $(modal).find('[data-block="car-photo"]').html('');
            $(modal).find('.modal-title > *').text('');
            
        },

        load_info: function(id, title, modal){

            window.linker_transport.edit_modal.reset(modal);

            $(modal).find('.modal-title > .ttl').text(title);

            $.konjax('send',window.ssi.api.transport.transportEdit, {id: id}, {

                data_processor: {

                    success:    function(response){

                        window.linker_transport.edit_modal.assign_ajax(
                            response.data.widget,
                            modal,
                            response.data.info
                        );

                    }

                }

            });


        },

        assign_ajax: function(widget, modal, info){

            var widget_doc      = $(widget.docs);
            var widget_numbers  = $(widget.numbers);
            var widget_gallery  = $(widget.gallery);

            var docs            = modal.find('[data-block="car-documents"]');
            var numbers         = modal.find('[data-block="car-photo"]');
            var gallery         = modal.find('[data-block="car-gallery"]');




            docs.html(      widget_doc.clone() );
            numbers.html(   widget_numbers.clone() );
            gallery.html(   widget_gallery.clone() );

            // var images = gallery.children('.imgDoc img');
            //
            // console.log(images);


            linker_transport.edit_modal.init_gallery_uploader_to_avatar_change();


            docs.find('p.title'); // TODO;

            var form_info = $('#transport-modal-edit-info');

            var disabled = !!!(info.hasRequests == "yes");

            form_info.find('select[name="car_type"]').prop('disabled', disabled);
            form_info.find('select[name="car_type"] option[value="' + info.carTypeID + '"]').prop('selected', true).prop('disabled', disabled);
            form_info.find('input[name="car_body_type"]').prop('disabled', disabled);
            form_info.find('input[name="car_body_type"][value="' + info.type + '"]').prop('checked', true).prop('disabled', disabled);
            form_info.find('input[name="liftingCapacity"]').val(info.liftingCapacity).prop('disabled', disabled);
            form_info.find('input[name="volume"]').val(info.volume).prop('disabled', disabled);
            form_info.find('input[name="sizeX"]').val(info.sizeX).prop('disabled', disabled);
            form_info.find('input[name="sizeY"]').val(info.sizeY).prop('disabled', disabled);
            form_info.find('input[name="sizeZ"]').val(info.sizeZ).prop('disabled', disabled);
            form_info.find('[name="info"]').val(info.info).prop('disabled', disabled);

            if (disabled === true) {

                form_info.find('button[name="action"][value="save"]').hide();
                modal.find('.modal-title i').attr('class','').addClass('fa').addClass('fa-eye')

            }else {

                form_info.find('button[name="action"][value="save"]').show();
                modal.find('.modal-title i').attr('class','').addClass('fa').addClass('fa-pencil')

            }

            $.each( $('#modal_edit_my_transport [data-widget="files"] .companyDocs span > i.fa'), function(i, icon){

                var _icon = $(icon).clone();
                var _text = $(icon).parent().text().trim();
                var span = $(icon).parent();
                span.html( _icon );
                span.append(  i18n(_text) );

            });

            $.each( $('#modal_edit_my_transport [data-widget="files"] > p.title'), function(i, paragraph){

                var html = $(paragraph).html();

                var to_replace = [
                    'file.widget.car.docs',
                    'file.widget.car.numbers',
                    'file.widget.car.photoes',
                    'label.attach',
                ];

                $.each(to_replace, function(i, _const){

                    html = html.split( _const ).join(i18n( _const ));

                });

                $(paragraph).html( html );

                //$(paragraph).text( i18n( $(paragraph).text().trim() ) );

            });

            $.each( $('#modal_edit_my_transport .companyDocs .docName'), function(i, el){

                var html = $(el).html();
                    html = html.split( 'label.download' ).join(i18n( 'label.download' ));
                    html = html.split( 'label.delete' ).join(i18n( 'label.delete' ));

                $(el).html( html )
            });

        },
        
        updateImage: function(url){

            $.konjax('send', window.ssi.api.transport.setMainImage,{url: url, id: $('#modal_edit_my_transport').attr('data-row-id')},{});

        },

        init_gallery_uploader_to_avatar_change: function(){
            var widget_gallery = $('#modal_edit_my_transport [data-block="car-gallery"]');

            var images = widget_gallery.find('.imgDoc img');

            $.each(images, function(i, node){

                var url     = $(node).attr('src');

                var hint    = i18n('mark.image.as.main-photo');

                $(node).replaceWith($('' +

                    '<a href="'+ url +'" data-update-main-car-gallery-image title="' + hint + '">' +
                    '   <img src="' + url + '" alt="' + hint + '"/>' +
                    '</a>' +

                    ''));


            });

        }

    }

};

$(document).on('click','#transport-modal-edit-gallery [data-widget="files"] [data-widget="attach"]', function () {

    // debugger;

    // довішуємо event на "після-загрузки"


    window.linker_register_upload_callback( window.linker_transport.edit_modal.init_gallery_uploader_to_avatar_change )
    
});

$('#modal_edit_my_transport').on('show.bs.modal', function(e){

    var invoker = $(e.relatedTarget).closest('.transport-box');

    var id = invoker.attr('data-id');
    var modal = $(this);
    var title = invoker.find('.center-inner-second p.top-inner').text();

    modal.attr('data-row-id', id);

    window.linker_transport.edit_modal.load_info( id, title, modal );

});

$('#modal_edit_my_transport').on('hidden.bs.modal', function () {

    // return;

    if($(this).attr('data-do-refresh') == 'true'){

        var pagination = $('[data-this-is="transport-layout"] [data-trasnsport="pagination"] li.active a');

        if(pagination.length){
            var page = pagination.attr('data-page')
        }else{
            var page = 1;
        }


        setTimeout(function() {

            window.linker_transport.load( window.linker_transport.assign, page ); // todo read page

        },200)

    }



});
// INSERT IGNORE INTO `i18n` (`const`, `type`) VALUES
// ( 'company.list.notFound', 'const'),
// ( 'company.type.transport', 'const'),
// ( 'company.list.notFoundBackLink', 'const'),
// ( 'error.404.companyNotFound', 'const'),
// ( 'company.page.title(:company)', 'const'),
// ( 'company.list.title', 'const'),
// ( 'company.page.rating', 'const'),
// ( 'company.page.activity', 'const'),
// ( 'company.page.location', 'const'),
// ( 'company.page.tab.infromation', 'const'),
// ( 'company.page.tab.requests(:count)', 'const'),
// ( 'company.page.tab.documents(:count)', 'const'),
// ( 'company.page.tab.transport(:count)', 'const'),
// ( 'site.title(:page_title)', 'const'),
// ( 'main_page.title', 'const'),
// ( 'company.page.documents.podatkovi', 'const'),
// ( 'company.page.documents.sertifikaty', 'const'),
// ( 'company.page.documents.svidoctva', 'const'),
// ( 'company.type.expeditor', 'const'),
// ( 'company.type.cargo', 'const'),
// ( 'company.registration.title', 'const'),
// ( 'transport.page.add.title', 'const'),
// ( 'cargo.page.add.title', 'const'),
// ( 'profile.title', 'const'),
// ( 'car.type.tent', 'const'),
// ( 'car.type.izoterm', 'const'),
// ( 'car.type.refrigerator', 'const'),
// ( 'currency.hrn', 'const'),
// ( 'car.body.vantazhivka', 'const'),
// ( 'car.body.napiv prychip', 'const'),
// ( 'car.body.z prychipom', 'const'),
// ( 'transport.page.list.title', 'const'),
// ( 'site.menu.header.cargo', 'const'),
// ( 'site.menu.header.transport', 'const'),
// ( 'site.menu.header.companies', 'const'),
// ( 'site.menu.header.press', 'const'),
// ( 'site.menu.header.advertisement', 'const'),
// ( 'site.menu.header.contacts', 'const'),
// ( 'tent', 'const'),
// ( 'izoterm', 'const'),
// ( 'refrigerator', 'const'),
// ( 'transport.list.notFound', 'const'),
// ( 'transport.list.notFoundInfo', 'const'),
// ( 'company.ownership.type.tzov', 'const'),
// ( 'company.ownership.type.pp', 'const'),
// ( 'company.ownership.type.fop', 'const'),
// ( 'company.ownership.type.spd', 'const'),
// ( 'company.ownership.type.fl-p', 'const'),
// ( 'company.ownership.type.chp', 'const'),
// ( 'company.ownership.type.flp', 'const'),
// ( 'company.ownership.type.ooo', 'const'),
// ( 'profile.add_tender.title', 'const'),
// ( 'form.error.email_available', 'const'),
// ( 'form.error.not_empty', 'const'),
// ( 'form.error.email', 'const'),
// ( 'form.error.max_length', 'const'),
// ( 'form.error.min_length', 'const'),
// ( 'form.error.numeric', 'const'),
// ( 'form.error.exact_length', 'const'),
// ( 'email.confirm_registration.title', 'const'),
// ( 'form.error.regex', 'const'),
// ( 'notification.auth.confirm_email', 'const'),
// ( 'tzov', 'const'),
// ( 'pp', 'const'),
// ( 'fop', 'const'),
// ( 'spd', 'const'),
// ( 'tender.page.list.title', 'const'),
// ( 'tender.page.title', 'const'),
// ( 'cargo.page.list.title', 'const'),
// ( 'krytaya', 'const'),
// ( 'konteyner', 'const'),
// ( 'ref', 'const'),
// ( 'lesovoz', 'const'),
// ( 'cel-nomet', 'const'),
// ( 'samosval', 'const'),
// ( 'konteynerovoz', 'const'),
// ( 'bortovaya', 'const'),
// ( 'bus', 'const'),
// ( 'cargo.list.notFound', 'const'),
// ( 'cargo.list.notFoundInfo', 'const'),
// ( 'zernovoz', 'const'),
// ( 'tral-negabarit', 'const'),
// ( 'otkrytaya', 'const'),
// ( 'cisterna', 'const'),
// ( 'platforma', 'const'),
// ( 'tenders.page.add.title', 'const'),
// ( 'evakuator', 'const'),
// ( 'mikroavtobus', 'const'),
// ( 'skotovoz', 'const'),
// ( 'avtocisterna', 'const'),
// ( 'special-naya-tehnika', 'const'),
// ( 'plitovoz', 'const'),
// ( 'avtovoz', 'const'),
// ( 'avtobus', 'const'),
// ( 'mukovoz', 'const'),
// ( 'yahtovoz', 'const'),
// ( 'benzovoz', 'const'),
// ( 'pogruzchik', 'const'),
// ( 'trubovoz', 'const'),
// ( 'kran', 'const'),
// ( 'steklovoz', 'const'),
// ( 'kormovoz', 'const'),
// ( 'site.menu.header.tenders', 'const'),
// ( 'partner.propose.to.be', 'const'),
// ( 'my.partners.list', 'const'),
// ( 'label.attach', 'const'),
// ( 'profile.feedback.history', 'const'),
// ( 'frm.sort', 'const'),
// ( 'feedback.sort.all', 'const'),
// ( 'feedback.sort.positive', 'const'),
// ( 'feedback.sort.negative', 'const'),
// ( 'profile.partners.list', 'const'),
// ( 'partners.show.partners', 'const'),
// ( 'partners.sort.i.must.confirm', 'const'),
// ( 'partners.sort.i.must.wait.confirmation', 'const'),
// ( 'file.car.docs', 'const'),
// ( 'file.block.attach.car.docs', 'const'),
// ( 'file.block.delete.car.docs', 'const'),
// ( 'file.block.download.car.docs', 'const'),
// ( 'file.block.title.car.docs', 'const'),
// ( 'file.car.number.photo', 'const'),
// ( 'file.block.attach.car.number', 'const'),
// ( 'file.block.delete.car.number', 'const'),
// ( 'file.block.download.car.number', 'const'),
// ( 'file.block.title.car.number', 'const'),
// ( 'profile.my-transport.modal-edit.block.car-info', 'const'),
// ( 'profile.my-transport.modal-edit.block.transport-type', 'const'),
// ( 'some.tent', 'const'),
// ( 'some.izoterm', 'const'),
// ( 'some.refrigerator', 'const'),
// ( 'some.krytaya', 'const'),
// ( 'some.konteyner', 'const'),
// ( 'some.ref', 'const'),
// ( 'some.lesovoz', 'const'),
// ( 'some.cel-nomet', 'const'),
// ( 'some.samosval', 'const'),
// ( 'some.konteynerovoz', 'const'),
// ( 'some.bortovaya', 'const'),
// ( 'some.bus', 'const'),
// ( 'some.zernovoz', 'const'),
// ( 'some.tral-negabarit', 'const'),
// ( 'some.otkrytaya', 'const'),
// ( 'some.cisterna', 'const'),
// ( 'some.platforma', 'const'),
// ( 'some.evakuator', 'const'),
// ( 'some.mikroavtobus', 'const'),
// ( 'some.skotovoz', 'const'),
// ( 'some.avtocisterna', 'const'),
// ( 'some.special-naya-tehnika', 'const'),
// ( 'some.plitovoz', 'const'),
// ( 'some.avtovoz', 'const'),
// ( 'some.avtobus', 'const'),
// ( 'some.mukovoz', 'const'),
// ( 'some.yahtovoz', 'const'),
// ( 'some.benzovoz', 'const'),
// ( 'some.pogruzchik', 'const'),
// ( 'some.trubovoz', 'const'),
// ( 'some.kran', 'const'),
// ( 'some.steklovoz', 'const'),
// ( 'some.kormovoz', 'const'),
// ( 'profile.my-transport.modal-edit.block.car-technical-info', 'const'),
// ( 'profile.my-transport.modal-edit.lifting-possibilities', 'const'),
// ( 'units.tone', 'const'),
// ( 'profile.my-transport.modal-edit.block.volume-sizes', 'const'),
// ( 'units.meter', 'const'),
// ( 'profile.my-transport.modal-edit.block.XYZ-sizes', 'const'),
// ( 'unit-label.length', 'const'),
// ( 'unit-label.width', 'const'),
// ( 'unit-label.height', 'const'),
// ( 'profile.my-transport.modal-edit.block.text-info', 'const'),
// ( 'btn.save', 'const'),
// ( 'transport.tab.documents', 'const'),
// ( 'transport.tab.info', 'const'),
// ( 'transport.tab.gallery', 'const'),
// ( 'profile.tab.statistic.feedback', 'const'),
// ( 'profile.tab.statistic.transport.requests', 'const'),
// ( 'profile.tab.statistic.cargo.requests', 'const'),
// ( 'stat.duration.whole.time', 'const'),
// ( 'stat.feedback.proportion[:duration:]', 'const'),
// ( 'stat.feedback.chart', 'const'),
// ( 'stat.transport.proportion[:duration:]', 'const'),
// ( 'stat.transport.chart', 'const'),
// ( 'stat.cargo.proportion[:duration:]', 'const'),
// ( 'stat.cargo.chart', 'const'),
// ( 'stat.show.range.year', 'const'),
// ( 'stat.show.range.month', 'const'),
// ( 'stat.show.chart.range.diff', 'const'),
// ( 'stat.show.chart.range.all', 'const'),
// ( 'profile.tab.my-profile', 'const'),
// ( 'profile.tab.my-messages', 'const'),
// ( 'profile.tab.my-transport', 'const'),
// ( 'profile.tab.my-cargo-tickets', 'const'),
// ( 'profile.tab.my-transport-tickets', 'const'),
// ( 'profile.tab.my-tenders', 'const'),
// ( 'profile.tab.my-wallet', 'const'),
// ( 'profile.tab.my-infoblock', 'const'),
// ( 'profile.tab.my-statistic', 'const'),
// ( 'profile.tab.my-company', 'const'),
// ( 'profile.tab.my-subscription', 'const'),
// ( 'profile.tab.my-profile.requests-feedback', 'const'),
// ( 'profile.tab.my-profile.partners', 'const'),
// ( 'profile.tab.my-profile.news_partners', 'const'),
// ( 'profile.tab.my-managers', 'const'),
// ( 'stat.feedback.chart.positive', 'const'),
// ( 'stat.feedback.chart.negative', 'const'),
// ( 'company.manager.name', 'const'),
// ( 'manage.company.manager', 'const'),
// ( 'delete.my.manager', 'const'),
// ( ' asdsa ', 'const'),
// ( 'profile.tab.my-profile.add_managers', 'const'),
// ( 'placeholder.profile.icq', 'const'),
// ( 'hint.profile.icq', 'const'),
// ( 'placeholder.profile.skype', 'const'),
// ( 'hint.profile.skype', 'const'),
// ( 'placeholder.profile.email', 'const'),
// ( 'hint.profile.email', 'const'),
// ( 'placeholder.profile.phone', 'const'),
// ( 'hint.profile.phone', 'const'),
// ( 'placeholder.profile.phone.stationary', 'const'),
// ( 'hint.profile.phone.stationary', 'const'),
// ( 'placeholder.profile.name', 'const'),
// ( 'hint.profile.name', 'const'),
// ( 'placeholder.profile.password.current', 'const'),
// ( 'hint.profile.password.current', 'const'),
// ( 'placeholder.profile.password.new', 'const'),
// ( 'hint.profile.password.new', 'const'),
// ( 'placeholder.profile.password.repeat', 'const'),
// ( 'hint.profile.password.repeat', 'const'),
// ( 'placeholder.profile.company.ipn', 'const'),
// ( 'hint.profile.company.ipn', 'const'),
// ( 'placeholder.profile.company.type', 'const'),
// ( 'hint.profile.company.type', 'const'),
// ( 'placeholder.profile.company.ownsership', 'const'),
// ( 'hint.profile.company.ownsership', 'const'),
// ( 'placeholder.profile.company.phone', 'const'),
// ( 'hint.profile.company.phone', 'const'),
// ( 'placeholder.profile.company.phone.stationary', 'const'),
// ( 'hint.profile.company.phone.stationary', 'const'),
// ( 'placeholder.profile.company.name', 'const'),
// ( 'hint.profile.company.name', 'const'),
// ( 'placeholder.profile.company.address', 'const'),
// ( 'hint.profile.company.address', 'const'),
// ( 'placeholder.profile.company.realAddress', 'const'),
// ( 'hint.profile.company.realAddress', 'const'),
// ( 'placeholder.profile.company.info', 'const'),
// ( 'hint.profile.company.info', 'const'),
// ( 'placeholder.profile.news.title', 'const'),
// ( 'hint.profile.news.title', 'const'),
// ( 'placeholder.profile.news.preview', 'const'),
// ( 'hint.profile.news.preview', 'const'),
// ( 'placeholder.profile.news.text', 'const'),
// ( 'hint.profile.news.text', 'const'),
// ( 'btn.reset', 'const'),
// ( '[OK]', 'const'),
// ( '[OK][OK]', 'const'),
// ( 'car.body.number.undefined', 'const'),
// ( 'car.body.samosval', 'const'),
// ( 'profile.tab.infoblock.myinfo', 'const'),
// ( 'profile.tab.infoblock.mypass', 'const'),
// ( 'profile.tab.infoblock.companyinfo', 'const'),
// ( 'profile.tab.infoblock.newslist', 'const'),
// ( 'profile.tab.infoblock.newsadd', 'const'),
// ( '[OK][OK][OK][OK]', 'const'),
// ( 'january.genitive', 'const'),
// ( 'december.genitive', 'const'),
// ( 'january.nominative', 'const'),
// ( 'february.nominative', 'const'),
// ( 'march.nominative', 'const'),
// ( 'april.nominative', 'const'),
// ( 'may.nominative', 'const'),
// ( 'june.nominative', 'const'),
// ( 'july.nominative', 'const'),
// ( 'august.nominative', 'const'),
// ( 'september.nominative', 'const'),
// ( 'october.nominative', 'const'),
// ( 'november.nominative', 'const'),
// ( 'december.nominative', 'const'),
// ( 'stat.request.transport.outdated', 'const'),
// ( 'stat.request.transport.completed', 'const'),
// ( 'stat.request.transport.have.confirmed.proposition', 'const'),
// ( 'stat.request.transport.have.not.yet.confirmed.proposition', 'const'),
// ( 'stat.request.transport.have.not.proposition', 'const'),
// ( 'stat.request.transport', 'const'),
// ( 'stat.request.cargo.outdated', 'const'),
// ( 'stat.request.cargo.completed', 'const'),
// ( 'stat.request.cargo.have.confirmed.proposition', 'const'),
// ( 'stat.request.cargo.have.not.yet.confirmed.proposition', 'const'),
// ( 'stat.request.cargo.have.not.proposition', 'const'),
// ( 'stat.request.cargo', 'const'),
// ( 'mark.image.as.main-photo', 'const'),
// ( 'label.download', 'const'),
// ( 'My car docs [custom title]:', 'const'),
// ( 'My car numbers [custom title]:', 'const'),
// ( 'My car photoes [custom title]:', 'const');