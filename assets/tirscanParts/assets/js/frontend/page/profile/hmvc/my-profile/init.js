/**
 * Created by LINKeR on 18.05.16.
 */
$('a[href="#profile"]').on('shown.bs.tab', function (e) {

    // ініціалузуємо автозагузку поточних відкритих фідбеків

    if(typeof  e.relatedTarget !== 'undefined'){

        window.my_profile_linker_init.doLoadOfCurrentTab();

    }

});

window.my_profile_linker_init = {

    doLoadOfCurrentTab: function(){

        var current_tab = $('#profile ul.nav-tabs li.active a').attr('href');

        $('#profile ' + current_tab + ' button[name="refresh"]').click();

    }

};


$(function () {

    if( $('a[href="#profile"]').parent().hasClass('active') ){

        window.my_profile_linker_init.doLoadOfCurrentTab();

    }

});
