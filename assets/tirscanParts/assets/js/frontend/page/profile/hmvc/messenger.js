


function dialogList(){
    $.konjax('send', ssi.api.messenger.getDialogs, {
        lang_id: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {

                var source = $("#hb-messenger-dialogs").html(),
                    template = Handlebars.compile(source),
                    html = template({
                        dialogs: response.data.dialogs,
                        userID: response.data.userID
                    });

                $('[data-inside="dialog"]').html(html);
            },
            error: function () {}
        }
    })
}

function success_get_dialog(response){
    var source = $("#hb-messenger").html(),
        template = Handlebars.compile(source),
        html = template({dialog: response.data.dialog});

    $('[data-inside="dialog"]').html(html);
}


function get_dialog(id, to_user_id, theme){

    $.konjax('send', ssi.api.messenger.getDialog, {
        dialog_id: id,
        lang_id: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {
                var source = $("#hb-messenger").html(),
                    template = Handlebars.compile(source),
                    html = template({
                        loggedUser: response.data.logged_user,
                        dialog: response.data.dialog,
                        userID: response.data.userID,
                        dialogID: id,
                        toUserID: to_user_id,
                        themeDialog: theme,
                        avatar: response.data.logged_user_avatar
                    });

                $('[data-inside="dialog"]').html(html);
            },
            error: function () {}
        }
    });

}

function send_message(){

    var user_to   = $('#to-user-id').val(),
        theme_dialog = $('#theme-dialog').text(),
        dialog_id = $('#dialog-id').val();

    $.konjax('send',ssi.api.messenger.sendMessage,{
        dialog_id: dialog_id,
        user_to: user_to,
        message:$('#message-text').val(),
        lang_id: window.ssi.locale.id
    },{
        data_processor:{
            success: function(response){
                var source = $("#hb-messenger").html(),
                    template = Handlebars.compile(source),
                    html = template({
                        dialog: response.data.dialog,
                        dialogID: response.data.dialogID,
                        toUserID: response.data.toUserID,
                        themeDialog: theme_dialog,
                        userID: response.data.userID,
                        avatar: response.data.logged_user_avatar
                    });

                $('[data-inside="dialog"]').html(html);
            },
            error: function(){}
        }
    })

}

function del_message(id){

    $.konjax('send', ssi.api.messenger.delMessage, {
        message_id:id,
        dialog_id:$('#dialog-id').val(),
        lang_id: window.ssi.locale.id
    },{
        data_processor: {
            success: function (response) {
                success_get_dialog(response);
            },
            error: function () {}
        }
    });

}

function del_dialog(id){

    $.konjax('send', ssi.api.messenger.delDialog, {
        dialog_id:id,
        lang_id: window.ssi.locale.id
    },{
        data_processor: {
            success: function (response) {
                var source = $("#hb-messenger-dialogs").html(),
                    template = Handlebars.compile(source),
                    html = template({dialogs: response.data.dialogs});

                $('[data-inside="dialog"]').html(html);
            },
            error: function () {}
        }
    });

}

//function change_status(id){
//
//    $.konjax('send', ssi.api.messenger.changeStatus, {
//        dialog_id:id
//    },{
//        data_processor: {
//            success: function () {},
//            error: function () {}
//        }
//    });
//}
