function loadCargoProps() {
    $.konjax('send', window.ssi.api.tickets.get_cargo_propositions, {localeID: window.ssi.locale.id}, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_cargoMyPropositions').html());
                $('.table-cargo-my-propositions').html(template(response.data));
            }
        }
    });
}

$(function () {

    $('a[href="#ticket-cargo"], a[href="#requests-cargo-my-applications"]').on('shown.bs.tab', function (e) {
        if(typeof  e.relatedTarget !== 'undefined'){
            loadCargoProps();
        }
    });


    $(document).on('click', '.transport-prop-delete', function () {
        var request_id = $(this).attr('data-id');

        window.ssi.dialog.confirmation('cargo-prop-delete', 'Ви дійсно бажаєте відмовитись від вашої пропозиції на заявку №' + request_id + '?', request_id);
    });

    $(document).on('click', '#cargo-prop-delete [name="onConfirm"]', function () {
        var request_id = $(this).attr('value');

        $.konjax('send', window.ssi.api.tickets.delete_cargo_proposition, {requestID: request_id}, {
            data_processor: {
                success: function (response) {
                    loadCargoProps();
                }
            }
        });

        $('#transportOrderModal').modal('hide');
    });

});