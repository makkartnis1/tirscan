$(document)
    .on('click', '.go_to_company_page', function (event) {
        event.stopPropagation();
    });

$(document)
    .on('click', '[data-click="showTransportModal"]', function () {
        if (!window.ssi.authorized) {
            $('#modal-login').modal('show');
            return;
        }

        var row_info = $(this).closest('table').clone();

        // $(row_info).find('[data-click="showTransportModal"]').removeAttr('data-click');

        $('#transportOrderModal [data-table="here"]').html($(row_info));
        var garbage = '#transportOrderModal [data-table="here"] tbody tr:not([data-request-id="' + $(this).attr('data-request-id') + '"])';

        $('#transportOrderModal [data-table="here"] tbody tr').prop('data-click', false);

        $(garbage).remove();

        $('#transportOrderModal .modal-title span').text('Заяка №' + $(this).attr('data-request-id')); // todo READ LOCALED STRING

        $('#transportOrderModal').attr('data-from-placeid', $(this).attr('data-from-placeid'));
        $('#transportOrderModal').attr('data-to-placeid', $(this).attr('data-to-placeid'));

        $('#transportOrderModal .modal-body i.fa-envelope').remove();

        modal_transport_set_details();

        $('#transportOrderModal').modal('show');
    });

$('#transportOrderModal').on('shown.bs.modal', function () {
    $(this).find('iframe').attr('src', '' +
        'https://www.google.com/maps/embed/v1/directions' +
        '?key=' +
        window.tscan.google.key +
        '&origin=place_id:' + $(this).attr('data-from-placeid') +
        '&destination=place_id:' + $(this).attr('data-to-placeid') +
        '&avoid=tolls|highways' +
        '');
});

function modal_transport_set_details(){

    var modal   = $('#transportOrderModal');
    var requestID = modal.find('[data-request-id]').attr('data-request-id');
    var _list   = JSON.parse( $('tr[data-id="' + requestID + '"] script[type="application/json"]').html() );

    var list    = [];
    window.ssi.log('READING ADDITOINAL INFO: TRANSPORT');
    $.each(_list, function(prop, value){

        window.ssi.log(prop, value);

        if( value == 'no' || value == '' || value == null ){
            return true; // пропускаємо поточну ітерацію
        }

        var unit = '';


        switch(true){

            case (value == 'yes'):
                value   = '';
                unit    = '<i class="fa fa-check text-success"></i>';
                break;

            case (prop == 'paymentType'):
                value = i18n(value);
                break;

            case (prop == 'info'):
                name = '';
                unit = '';
                break;

            case (prop == 'price'):
                value = value.split('|', 2);
                unit  = value[1]; value = value[0];
                break;

            case (prop == 'condADR'):
                console.log('here>>ADR');
                unit = '<i class="fa fa-warning text-warning"></i>';
                break;

            case (prop == 'advancedPayment'):
            case (prop == 'PDV'):
                unit    = '%';
                break;

            case (prop == 'condTemperature'):
                unit    = i18n('unit.degree');
                break;

        }



        var row = {
            name:   i18n('transport.option.' + prop),
            value:  value,
            unit:   unit
        };

        list.push( row );

    });

    var source      = $('#modal-cargo-or-transport-request-additional-info').html();
    var template    = Handlebars.compile(source);
    var block       = template({list: list});

    // console.log(list, block, _list);

    modal.find('[data-info]').html( block );

}

$(document).on('click', '.payRequestService', function () {
    var _service_type = $('[name="service_type"]:checked').val(),
        _duration = 0;

    if (_service_type == 'color') {
        _duration = $('[name="service_color_duration"]').val();
    }
    else if (_service_type == 'vip') {
        _duration = $('[name="service_vip_duration"]').val();
    }

    $.konjax('send', window.ssi.api.tickets.request_order, {
        requestID: $(this).attr('data-id'),
        type: $(this).attr('data-type'),
        orderType: _service_type,
        duration: _duration
    }, {
        data_processor: {
            success: function (response) {
                $('#paidServicesModalWindow')
                    .modal('hide');
            }
        }
    });

    console.log(_service_type, _duration);

});

$(document).on('click', '.cargo-ticket-order', function (event) {
    $('#transportOrderModal').modal('hide');

    event.stopPropagation();

    $('.payRequestService')
        .attr('data-type', 'cargo')
        .attr('data-id', $(this).attr('data-id'));

    $('#paidServicesModalWindow')
        .modal('show');
});

$(document).on('click', '.transport-ticket-order', function (event) {
    $('#transportOrderModal').modal('hide');

    event.stopPropagation();

    $('.payRequestService')
        .attr('data-type', 'transport')
        .attr('data-id', $(this).attr('data-id'));

    $('#paidServicesModalWindow')
        .modal('show');
});