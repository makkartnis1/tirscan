$(document).on('click','#infoblock a[href="#news"]', function(e){

    e.preventDefault();

    window.linker_profile_request_news_list.loading(1);
});


$(document).on('click','#news ul.pagination li:not(.active) a', function (e){

    e.preventDefault();

    window.linker_profile_request_news_list.loading($(this).attr('data-page'));
});


$(document).on('click','#news .open-news', function (e){

    e.preventDefault();

    var new_id = $(this).attr('id');

    $.konjax('send', window.ssi.api.partner.get_one_news, {
        id: new_id,
        lang_id: window.ssi.locale.id
    }, {
        data_processor: {
            success: function (response) {
                var source = $('#profile-partners-one-news').html(),
                    template = Handlebars.compile(source),
                    html = template({
                        content: response.data.one_news.content,
                        preview: response.data.one_news.preview,
                        title: response.data.one_news.title
                    });

                $('#news .reviews .oneCompanyMainFigureContent .row').html(html);
            },
            error: function () {}
        }
    });

});

$(document).on('click','#back-to-list', function(e){

    e.preventDefault();

    window.linker_profile_request_news_list.loading(1);
});


window.linker_profile_request_news_list = {

    loading: function(page){
        window.linker_profile_request_news_list._sys.list( window.linker_profile_request_news_list._sys.assign, page );
    },

    _sys: {

        assign: function (data) {

            var source = $('#profile-partners-news-list').html(),
                template = Handlebars.compile(source),
                html = template({news_list: data.data.news_list});

            $('#news .reviews .oneCompanyMainFigureContent .row').html(html);

            if (data.data.pages != 0) {

                var pagination = $('#hb-bs-pagination').html(),
                    pagination_template = Handlebars.compile(pagination),
                    pagination_html = pagination_template({
                        currentPage: data.data.current,
                        totalPage: data.data.pages,
                        size: data.data.step,
                        options: {}
                    });

            } else {
                pagination_html = '';
            }

            $('#news ul.pagination').html(pagination_html);

        },

        list: function (callback, page) {

            $.konjax('send', window.ssi.api.partner.news_list, {
                page: page,
                lang_id: window.ssi.locale.id
            }, {
                data_processor: {
                    success: function (response) {
                        callback(response);
                    }
                }
            });

        }

    }

};