/**
 * Created with JetBrains PhpStorm.
 * User: T1taH1k
 * Date: 18.05.16
 * Time: 12:32
 * To change this template use File | Settings | File Templates.
 */

$('a[href="#tender"]').on('shown.bs.tab', function (e) {

    if(typeof  e.relatedTarget !== 'undefined'){

        var right = $("#for_user_right").attr('right');

        if (right == 'exp' || right == 'cargo')
        {
            $.konjax('send', window.ssi.api.tender.getMy, {}, {
                data_processor:{
                    success: function(resp){
                        window.plodnik_tender_method.assignAjaxResponseMy(resp);
                    }
                }
            });
        }

        if (right == 'exp' || right == 'trans')
        {
            $.konjax('send', window.ssi.api.tender.get, {}, {
                data_processor:{
                    success: function(resp){
                        window.plodnik_tender_method.assignAjaxResponse(resp);
                    }
                }
            });
        }
    }

});

window.plodnik_tender_method = {

    load:   function(callback, page){
        $.konjax('send', window.ssi.api.tender.getMy, {page:page}, {

            data_processor:{

                success: function(resp){

                    window.plodnik_tender_method.assignAjaxResponseMy(resp);

                }

            }

        })
    },
    loadOwner:   function(callback, page){
        $.konjax('send', window.ssi.api.tender.get, {page:page}, {

            data_processor:{

                success: function(resp){

                    window.plodnik_tender_method.assignAjaxResponse(resp);

                }

            }

        })
    },


    new_generate_content: function (result,add_delete_btn)
    {

        var insert_text = '';
        var date_load = '';
        var place_from = '';
        var place_to = '';
        var cargo_type = '';
        var weight_and_volume = '';
        var price = i18n('tender_page.hidden');
        var bets = '';
        var status = '';
        var activity = '-';

        for (var i = 0; i < result.length; i++)
        {

            date_load = '';
            place_from = '';
            place_to = '';
            cargo_type = '';
            weight_and_volume = '';
            price = i18n('tender_page.hidden');
            bets = '';
            status = '';
            activity = '-';

            if (result[i].status == 'active')
            {
                insert_text += '<a><tr class="active">';
            }
            else if(result[i].status == 'inactive')
            {
                insert_text += '<tr class="inactive">';
            }
            else
            {
                insert_text += '<tr>';
            }


            //дата завантаження
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            date_load = (result[i].dateFromLoad == result[i].dateToLoad) ? result[i].dateFromLoad : result[i].dateFromLoad+'-'+result[i].dateToLoad;
            insert_text += date_load;
            insert_text += '</a></td>';

            //місце завантаження
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            place_from = result[i].from_1+'('+result[i].place_from[0].country+')';
            insert_text += place_from;
            insert_text += '</a></td>';

            //місце розвантаження
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            place_to = result[i].to_1+'('+result[i].place_to[0].country+')';
            insert_text += place_to;
            insert_text += '</a></td>';

            //тип вантажу
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            cargo_type = result[i].cargoType;
            insert_text += cargo_type;
            insert_text += '</a></td>';

            //вага і об'єм
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            weight_and_volume = result[i].weight_and_volume;
            insert_text += weight_and_volume;
            insert_text += '</a></td>';

            //ціна
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';

            if (add_delete_btn)
            {
                price = result[i].price+' '+result[i].currency;
                price += (result[i].priceHide == 'no')?'<br>('+i18n('tender_page.otkrytaya')+')':'<br>('+i18n('tender_page.hidden')+')';
            }
            else
            {
                if (result[i].priceHide == 'no')
                    price = result[i].price+' '+result[i].currency;
            }

            insert_text += price;
            insert_text += '</a></td>';

            //позиція користувача



            //кількість ставок
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            bets = result[i].count_requests;
            if (!add_delete_btn)
            {
                insert_text += '№'+result[i].user_place_num+'/';
            }
            insert_text += bets;
            insert_text += '</a></td>';


            //статус
            insert_text += '<td><a href="/'+window.ssi.locale.uri+'/tender/'+result[i].ID+'" target="_blank" class="tender-link">';
            status = result[i].status;
            if (status == 'active')
            {
                status = i18n('tenders.status.active')+' '+result[i].dateToTender;
            }

            if (status == 'inactive')
            {
                status = i18n('tenders.status.will_active')+' '+result[i].dateFromTender;
            }

            if (status == 'completed')
            {
                status = i18n('tenders.status.completed')+' '+result[i].dateToTender;
            }

            if (status == 'stopped')
            {
                status = i18n('tenders.status.stopped');
            }

            insert_text += status;
            insert_text += '</a></td>';


            //дії
            insert_text += '<td>';

            if (add_delete_btn)
            {
                if (result[i].status == 'inactive')
                {

                    activity = '<span class="editing-block-tender">' +
                        '<a href="/'+window.ssi.locale.uri+'/tender/edit/'+result[i].ID+'">' +
                        '<i class="fa fa-pencil"></i>' +
                        '</a>&nbsp;&nbsp;' +
                        '<span class="delete_link_tender" data-action="delete_tender" data-id="'+result[i].ID+'">'+
                        '<i class="fa fa-times fa-lg red"/>'+
                        '</span>'+
                        '</span>';

                }

                if (result[i].status == 'active')
                {
                    activity = '<span class="editing-block-tender">' +
                        '<span class="delete_link_tender" data-action="stop_tender" data-id="'+result[i].ID+'">'+
                        '<i class="fa fa-ban fa-lg red"/>'+
                        '</span>'+
                        '</span>';
                }
            }

            insert_text += activity;
            insert_text += '</td>';

            insert_text += '</tr>';
        }

        return insert_text;

    },

    assignAjaxResponse: function(response){

        var result = response.data.result;

        if (!result)
        {
            $("#profile-owner-tenders-list").html("<tr><td colspan='9'>"+i18n('tenders.no_owner_tenders')+"</td></tr>");
        }
        else
        {
            var insert_text = window.plodnik_tender_method.new_generate_content(result,0);

            $("#profile-owner-tenders-list").html(insert_text);

            if(response.data.pages != 0){

                var pagination          = $('#hb-bs-pagination').html();
                var pagination_template = Handlebars.compile(pagination);
                var pagination_html     = pagination_template({
                    currentPage:    response.data.current,
                    totalPage:      response.data.pages,
                    size:           response.data.step,
                    options:        {}
                });
            }else{
                var pagination_html = '';
            }

            window.ssi.log(response.data.result);

            $('[data-tender-owner="pagination"]').html(pagination_html);
        }

    },

    assignAjaxResponseMy: function(response){

        var result = response.data.result;

        if (!result)
        {
            $("#profile-my-tenders-list").html("<tr><td colspan='9'>"+i18n('tenders.no_tenders_created')+"</td></tr>");
        }
        else
        {
            var insert_text = window.plodnik_tender_method.new_generate_content(result,1);

            $("#profile-my-tenders-list").html(insert_text);

            if(response.data.pages != 0){

                var pagination          = $('#hb-bs-pagination').html();
                var pagination_template = Handlebars.compile(pagination);
                var pagination_html     = pagination_template({
                    currentPage:    response.data.current,
                    totalPage:      response.data.pages,
                    size:           response.data.step,
                    options:        {}
                });
            }else{
                var pagination_html = '';
            }

            window.ssi.log(response.data.result);

            $('[data-tender-my="pagination"]').html(pagination_html);

        }

    }

}

$(document).on('click',' [data-action="delete_tender"]', function(){

    var id      = $(this).data('id');

    var content = $('<div>' +
        i18n('tenders.confirm_delete')+
        '</br>' +
        '<button class="btn btn-danger pull-left" name="confirm-delete-tender" value="' + id + '" type="button">'+i18n('tenders.yes')+'</button>' +
        '<button class="btn btn-default pull-right" name="decline" type="button">'+i18n('tenders.cancel')+'</button>' +
        '</div>');

    toastr.info(content.html());

});


$(document).on('click',' [data-action="stop_tender"]', function(){

    var id      = $(this).data('id');

    var content = $('<div>' +
        i18n('tenders.confirm_stop')+
        '</br>' +
        '<textarea style="color:black;" id="stop_reason" placeholder="'+i18n('tenders.write_reason')+'"></textarea>' +
        '</br>' +
        '<button class="btn btn-danger pull-left" name="confirm-stop-tender" value="' + id + '" type="button">'+i18n('tenders.yes')+'</button>' +
        '<button class="btn btn-default pull-right" name="decline" type="button">'+i18n('tenders.cancel')+'</button>' +
        '</div>');


    toastr.options.tapToDismiss = false;
    toastr.options.timeOut = 0;
    toastr.options.extendedTimeOut = 0;



    toastr.info(content.html());

});

$(document).on('click','[name="confirm-delete-tender"]', function(){

    $.konjax('send',window.ssi.api.tender.delete,{id: $(this).val()},{
        data_processor:{

            success: function(response){

                if (response.data.result == '1')
                {
                    window.plodnik_tender_method.load();
                }
                else
                {
                    toastr.error(i18n('tenders.delete_active_error'));
                }
            }

        }
    })

});

$(document).on( "click", "[name=decline]", function(){
    toastr.remove();
});

$(document).on('click','[name="confirm-stop-tender"]', function(){

    var stop_reason = $("#stop_reason").val();

    $.konjax('send',window.ssi.api.tender.stop,{id: $(this).val(), stop_reason: stop_reason},{
        data_processor:{

            success: function(response){

                if (response.data.result == '1')
                {
                    window.plodnik_tender_method.load();
                }
                else
                {
                    toastr.error(i18n('tenders.server_error'));
                }
            }

        }
    });
    toastr.remove();

});


$(document).on('click', '[data-tender-my="pagination"] li > a',function(e){

    e.preventDefault();

    if ($(this).parent().is(':not(.active)')){
        window.plodnik_tender_method.load( window.plodnik_tender_method.assignAjaxResponseMy, $(this).attr('data-page') );

    }

});


$(document).on('click', '[data-tender-owner="pagination"] li > a',function(e){

    e.preventDefault();

    if ($(this).parent().is(':not(.active)')){
        window.plodnik_tender_method.loadOwner( window.plodnik_tender_method.assignAjaxResponse, $(this).attr('data-page') );

    }

});