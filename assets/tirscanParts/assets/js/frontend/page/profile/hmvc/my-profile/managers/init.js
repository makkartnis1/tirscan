

$(document).on('click','#generate-link', function(){

    $.konjax('send', window.ssi.api.profile.generateManagerLink, {
        lang_uri: window.ssi.locale.uri
    }, {
        data_processor: {
            success: function (response) {

                var source = $('#profile-managers-link').html(),
                    template = Handlebars.compile(source),
                    html = template({link: response.data.registration_manager_link});

                $('#managers #link').html(html);

            },
            error: function(){
                console.log('error');
            }
        }
    });
});