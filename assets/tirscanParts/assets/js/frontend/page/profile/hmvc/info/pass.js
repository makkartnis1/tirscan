/**
 * Created by LINKeR on 27.04.16.
 */
$(function () {

    $(document).on('submit', '#tab-infoblock-mypass form', function (e) {

        e.preventDefault();

        $('#tab-infoblock-mypass input').inputError('remove');;

        var pass_is_too_short   = !( $('#mypass-cur-pass').val().length >= ssi.profile.pass.length[0] );
        var pass_is_too_long    = !( $('#mypass-cur-pass').val().length <= ssi.profile.pass.length[1] );
        var pass_cur_not_match  = ( $('#mypass-cur-pass').val().match(ssi.profile.pass.regexp) == null);
        var pass_new_is_diff    = !( $('#mypass-new-pass').val()        === $('#mypass-repeat-pass').val() );
        var pass_new_not_match  = ( $('#mypass-new-pass').val().match(ssi.profile.pass.regexp) == null);
        var pass_new_too_short  = !( $('#mypass-new-pass').val().length >= ssi.profile.pass.length[0] );
        var pass_new_too_long   = !( $('#mypass-new-pass').val().length <= ssi.profile.pass.length[1] );
        console.log(pass_cur_not_match, pass_new_not_match);
        
        if(pass_is_too_short){
            $('#mypass-cur-pass')
                
                .inputError('add',i18n('profile.pass.current.invalid.min.length[:min:]',
                    {"[:min:]":ssi.profile.pass.length[0]}));
        }


        if(pass_is_too_long){

            $('#mypass-cur-pass')
                
                .inputError('add',i18n('profile.pass.current.invalid.max.length[:max:]',
                    {"[:max:]":ssi.profile.pass.length[1]})
                );
        }

        if(pass_new_not_match){
            $('#mypass-new-pass').inputError('add',i18n('profile.pass.not.match'));
        }

        // if(pass_cur_not_match){
        //     $('#mypass-cur-pass').inputError('add',i18n('profile.pass.not.match'));
        // }
        

        if(pass_new_is_diff ){

            $('#mypass-new-pass').inputError('add',i18n('profile.pass.new.not.match'));
            $('#mypass-repeat-pass').inputError('add');

        }else{

            if(pass_new_too_long){
                $('#mypass-new-pass').inputError('add',i18n('profile.pass.new.invalid.max.length[:max:]',
                    {"[:max:]":ssi.profile.pass.length[1]})
                );
            }

            if(pass_new_too_short){
                $('#mypass-new-pass').inputError('add',i18n('profile.pass.new.invalid.min.length[:min:]',
                    {"[:min:]":ssi.profile.pass.length[0]})
                );
            }

        }

        var form_is_ok = !!( $('#tab-infoblock-mypass .form-group.errorForm').length == 0 );

        if(form_is_ok){

            var data = {
                current:    $('#mypass-cur-pass').val(),
                new_1:      $('#mypass-new-pass').val(),
                new_2:      $('#mypass-repeat-pass').val()
            };

            $.konjax('send',ssi.api.profile.changePWD, data, {
                data_processor:{
                    success: function(response, xhr){

                        $('#tab-infoblock-mypass form input').val('');
                    },
                    error: function(response, xhr){

                        if(typeof response.data.validation === typeof {}){

                            $.each(response.data.validation, function(db_column, row){

                                switch (db_column){
                                    case ('hash'):      $('#mypass-new-pass').inputError('add', i18n_validation(row));
                                    case ('curr_path'): $('#mypass-cur-pass').inputError('add', i18n_validation(row));
                                }


                            });

                        }

                    }

                }
            });

        }else{

            toastr.warning(i18n( 'profile.pass.form.invalid' ));

        }

    });

});