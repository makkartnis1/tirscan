function loadCargoTickets() {
    $.konjax('send', window.ssi.api.tickets.get_cargo, {
        localeID: window.ssi.locale.id,
        type: $('[name="filter-my-cargo-requests-status"]').val()
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_cargoMyRequests').html());
                $('.table-cargo-my-requests').html(template(response.data));
            }
        }
    });
}

function loadCargoOrderCompanies(request_id) {
    $.konjax('send', window.ssi.api.tickets.get_cargo_ticket_companies, {
        localeID: window.ssi.locale.id,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                var template = Handlebars.compile($('#template_cargoCompanyProps').html());
                $('#orderCargoCompaniesProps').html(template(response.data));

                if (response.data.companies.length == 0) {
                    $('#orderCargoInProcessModalWindow').modal('hide');
                }
            }
        }
    });
}

function changeCargoOrderProp(status, app_id) {
    $.konjax('send', window.ssi.api.tickets.set_cargo_proposition, {
        action: status,
        appID: app_id
    }, {
        data_processor: {
            success: function (response) {
                loadCargoOrderCompanies($('#orderCargoInProcessModalWindow').attr('data-id'))
                loadCargoTickets();
            }
        }
    });
}

function closeCargoTicket(request_id, _rating, text) {
    $.konjax('send', window.ssi.api.tickets.close_cargo_ticket, {
        rating: _rating,
        feedback: text,
        requestID: request_id
    }, {
        data_processor: {
            success: function (response) {
                $('#CargoRatingModal').modal('hide');
                loadCargoTickets();
            }
        }
    });
}

$(function () {

    $('#submitCargoCloseTicket').click(function () {
        var text = $('[name="closeCargoTicketText"]').val();
        var rating = $('[name="closeCargoTicketRating"]:checked').val();
        var request_id = $('#CargoRatingModal').attr('data-id');

        if (text.length < 10) {
            toastr.error('Будь ласка, залиште змістовний коментар щодо роботи компанії')
        }

        closeCargoTicket(request_id, rating, text);
    });

    $(document).on('click', '.cargo-ticket-delete', function (event) {
        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        window.ssi.dialog.confirmation('cargo-ticket-delete-yes', 'Ви дійсно бажаєте видалити заявку №' + request_id + '?', request_id);
    });

    $(document).on('click', '#cargo-ticket-delete-yes [name="onConfirm"]', function () {
        $('#transportOrderModal').modal('hide');

        var request_id = $(this).attr('value');

        $.konjax('send', window.ssi.api.tickets.delete_cargo_request, {requestID: request_id}, {
            data_processor: {
                success: function (response) {
                    loadCargoTickets();
                }
            }
        });
    });

    $(document).on('click', '.accept-cargo-company-prop', function () {
        changeCargoOrderProp('accept', $(this).attr('data-id'));
    });

    $(document).on('click', '.reject-cargo-company-prop', function () {
        changeCargoOrderProp('reject', $(this).attr('data-id'));
    });

    $(document).on('click', '.cargo-ticket-props', function (event) {
        $('#transportOrderModal').modal('hide');

        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        $('.cargo-ticket-apps-number').text(request_id);
        $('#orderCargoInProcessModalWindow')
            .attr('data-id', request_id)
            .modal('show');
    });

    $(document).on('click', '.cargo-ticket-close', function (event) {
        $('#transportOrderModal').modal('hide');

        event.stopPropagation();

        var request_id = $(this).attr('data-id');

        $('.cargo-ticket-close-number').text(request_id);
        $('#CargoRatingModal')
            .attr('data-id', request_id)
            .modal('show');
    });

    $('#orderCargoInProcessModalWindow').on('shown.bs.modal', function () {
        var request_id = $(this).attr('data-id');

        loadCargoOrderCompanies(request_id);
    });

    $('[name="filter-my-cargo-requests-status"]').change(function () {
        loadCargoTickets();
    });

    $('a[href="#ticket-cargo"], a[href="#requests-cargo-my-requests"]').on('shown.bs.tab', function (e) {
        if(typeof  e.relatedTarget !== 'undefined'){
            loadCargoTickets();
        }
    });

    var hash = window.location.hash;
    if (hash == "#ticket-cargo") {
        loadCargoTickets();
    }

});