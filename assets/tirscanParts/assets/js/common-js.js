$(function(){

    $('#subscribe-to-news').on('click', function(){
        var user_id = $('#user_id').val(),
            company_id = $('#company_id').val();

        $.konjax('send', ssi.api.subscribe.subscribeToNews, {userID: user_id, companyID: company_id}, {
            data_processor: {
                success: function () {
                    console.log('success');
                },
                error: function () {
                    console.log('error');
                }
            }
        });

    });

    var form = '#feedback form';

    $(document).on('submit', form, function(e) {

        e.preventDefault();

        var formData = $(form).serializeJSON();

        $.konjax('send', ssi.api.feedback.sendFeedback, {post: formData}, {
            data_processor: {
                success: function () {
                    console.log('success');

                    $('#feedback').modal('hide');
                    $('#feedback-form').trigger('reset');
                },
                error: function () {
                    console.log('error');
                }
            }
        });

    });

    if (window.ssi.authorized == 1) {
        $(document).ready(getNewData);
    }

    function getNewData() {
        setInterval(function() {
            $.konjax('send', ssi.api.messenger.getNewData, {}, {
                data_processor: {
                    success: function (response) {
                        var test  = '{"count": '+response.data.count+'}';
                        var count = JSON.parse(test);

                        $('[count="message"]').text(count.count);
                    },
                    error: function(){
                        console.log('error')
                    }
                },
                before: function(){},
                after: function(){}
            })
        }, 20000);
    }

    $('#my-messages').on('click', function (){
        var url = "/"+window.ssi.locale.uri+"/profile/#messenger";
        $(location).attr('href',url);
    });

    $(document).on('shown.bs.tab', 'a[href="#messenger"]', function (){
        dialogList();
    });

});