var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('sass', function(){
   return gulp.src('assets/css/scss/partStyle.scss')
       .pipe(sass())
       .pipe(gulp.dest('assets/css'))
       .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function(){
    browserSync({
       server: {
           baseDir: './'
       },
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'sass'], function() {
    gulp.watch('assets/css/scss/partStyle.scss', ['sass']);
    gulp.watch('*.html', browserSync.reload);
});

gulp.task('default', ['watch']);