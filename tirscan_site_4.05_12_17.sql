-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Час створення: Трв 04 2016 р., 11:16
-- Версія сервера: 5.5.44-MariaDB-1~wheezy
-- Версія PHP: 5.4.41-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `tirscan_site`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Cargo`
--

CREATE TABLE IF NOT EXISTS `Frontend_Cargo` (
  `ID` int(10) unsigned NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `cargoType` varchar(256) NOT NULL,
  `transportTypeID` int(10) unsigned DEFAULT NULL,
  `volume` decimal(12,3) DEFAULT NULL,
  `weight` decimal(12,3) DEFAULT NULL,
  `carCount` int(10) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) DEFAULT NULL,
  `sizeY` decimal(12,3) DEFAULT NULL,
  `sizeZ` decimal(12,3) DEFAULT NULL,
  `priceID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `onlyTransporter` enum('yes','no') NOT NULL,
  `status` enum('active','deleted','completed') NOT NULL DEFAULT 'active',
  `parser` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=898 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Cargo`
--

INSERT INTO `Frontend_Cargo` (`ID`, `dateFrom`, `dateTo`, `created`, `info`, `cargoType`, `transportTypeID`, `volume`, `weight`, `carCount`, `sizeX`, `sizeY`, `sizeZ`, `priceID`, `userID`, `docTIR`, `docCMR`, `docT1`, `docSanPassport`, `docSanBook`, `loadFromSide`, `loadFromTop`, `loadFromBehind`, `loadTent`, `condPlomb`, `condLoad`, `condBelts`, `condRemovableStands`, `condHardSide`, `condCollectableCargo`, `condTemperature`, `condPalets`, `condADR`, `onlyTransporter`, `status`, `parser`) VALUES
(726, '2016-04-21', '2016-05-21', '2016-04-21 09:49:22', '', 'осветительные приборы', 1, '2.000', '0.400', NULL, NULL, NULL, NULL, 1194, 2604, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031901'),
(727, '2016-04-22', '2016-05-22', '2016-04-21 09:49:27', '', 'тнп', 1, '86.000', '20.000', NULL, NULL, NULL, NULL, 1195, 2606, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031899'),
(728, '2016-04-21', '2016-05-21', '2016-04-21 09:49:30', '', 'металлоконструкции', 1, '20.000', '15.000', NULL, NULL, NULL, NULL, 1196, 2604, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031897'),
(729, '2016-04-21', '2016-05-21', '2016-04-21 09:49:33', '', 'тара', 4, '60.000', '10.000', NULL, NULL, NULL, NULL, 1197, 2604, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031895'),
(730, '2016-04-21', '2016-05-21', '2016-04-21 09:49:36', 'догруз', 'салфетки бумажные', 4, '8.500', '0.560', NULL, NULL, NULL, NULL, 1198, 2604, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031893'),
(731, '2016-04-21', '2016-05-21', '2016-04-21 09:49:39', '', 'битум в мешках', 4, '1.000', '1.000', NULL, NULL, NULL, NULL, 1199, 2604, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218031891'),
(733, '2016-04-21', '2016-05-21', '2016-04-21 10:01:50', '', 'комбикорм в мешках', 4, NULL, '1.000', NULL, NULL, NULL, NULL, 1201, 2610, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218033761'),
(734, '2016-04-22', '2016-05-22', '2016-04-21 10:01:55', '', 'сырье.', 10, NULL, '40.000', NULL, NULL, NULL, NULL, 1202, 2612, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218014763'),
(735, '2016-04-21', '2016-05-21', '2016-04-21 10:01:59', '', 'тнп на паллетах', 4, '40.000', '10.000', NULL, NULL, NULL, NULL, 1203, 2633, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217998821'),
(736, '2016-04-21', '2016-05-21', '2016-04-21 10:02:01', 'ДОГРУЗ', 'Оборудование.', 1, NULL, '3.000', NULL, NULL, NULL, NULL, 1204, 2612, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217991103'),
(737, '2016-04-21', '2016-05-21', '2016-04-21 10:02:05', '', 'тнп на паллетах', 4, '86.000', '20.000', NULL, NULL, NULL, NULL, 1205, 2633, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217990439'),
(738, '2016-04-21', '2016-05-21', '2016-04-21 10:02:07', '2 точки в Одессе', 'ЛВИ/', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1206, 2612, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217990281'),
(739, '2016-04-21', '2016-05-21', '2016-04-21 10:02:09', '', 'тнп на паллетах', 4, '86.000', '20.000', NULL, NULL, NULL, NULL, 1207, 2633, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217949107'),
(740, '2016-04-21', '2016-05-21', '2016-04-21 10:02:11', '', 'Блоки.', 18, NULL, '20.000', NULL, NULL, NULL, NULL, 1208, 2612, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217616283'),
(741, '2016-04-21', '2016-05-21', '2016-04-21 10:02:13', '', 'Блоки.', 14, NULL, '20.000', NULL, NULL, NULL, NULL, 1209, 2612, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217616219'),
(742, '2016-04-21', '2016-05-21', '2016-04-21 10:02:14', '', 'Сырье.', 1, NULL, '5.000', NULL, NULL, NULL, NULL, 1210, 2612, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217463535'),
(743, '2016-04-21', '2016-05-21', '2016-04-21 10:05:45', '', 'МИН. ВОДА', 4, NULL, '20.000', NULL, NULL, NULL, NULL, 1221, 2649, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218001465'),
(744, '2016-04-21', '2016-05-21', '2016-04-21 10:05:46', '', 'вода на палетах', 4, NULL, '21.000', NULL, NULL, NULL, NULL, 1222, 2649, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217499791'),
(745, '2016-04-21', '2016-05-21', '2016-04-21 10:05:47', '', 'вода на палетах', 4, NULL, '21.000', NULL, NULL, NULL, NULL, 1223, 2649, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216958845'),
(746, '2016-04-21', '2016-05-21', '2016-04-21 10:05:49', '', 'посуда одноразовая', 4, '4.500', '0.400', NULL, NULL, NULL, NULL, 1224, 2650, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218017531'),
(747, '2016-04-21', '2016-05-21', '2016-04-21 10:05:51', '', 'шпала пропитанная', 1, NULL, '8.000', NULL, NULL, NULL, NULL, 1225, 2650, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217821639'),
(748, '2016-04-21', '2016-05-21', '2016-04-21 10:05:55', '', 'тнп на паллетах', 1, '86.000', '26.000', 4, NULL, NULL, NULL, 1226, 2651, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217714181'),
(749, '2016-04-21', '2016-05-21', '2016-04-21 10:05:56', 'торг', 'тнп в коробках', 4, '14.000', '0.700', NULL, NULL, NULL, NULL, 1227, 2650, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216134907'),
(750, '2016-04-21', '2016-05-21', '2016-04-21 10:05:59', '', 'игрушки', 4, '18.000', '1.000', NULL, NULL, NULL, NULL, 1228, 2656, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034359'),
(751, '2016-04-21', '2016-05-21', '2016-04-21 10:06:02', '', 'плитка керамическая', 1, NULL, '5.000', NULL, NULL, NULL, NULL, 1229, 2665, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034357'),
(752, '2016-04-21', '2016-05-21', '2016-04-21 10:06:05', '3 места погрузки', 'посуда в коробках', 4, '6.500', '1.300', NULL, NULL, NULL, NULL, 1230, 2673, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034355'),
(753, '2016-04-21', '2016-05-21', '2016-04-21 10:06:06', '', 'вода на палетах', 4, NULL, '21.000', NULL, NULL, NULL, NULL, 1231, 2649, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034353'),
(754, '2016-04-21', '2016-05-21', '2016-04-21 10:06:09', 'Манипулятор', 'кирпич на паллетах', 16, NULL, '10.000', NULL, NULL, NULL, NULL, 1232, 2678, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034351'),
(755, '2016-04-21', '2016-05-21', '2016-04-21 10:06:12', '', 'тнп 15-17пал', 4, NULL, '5.000', NULL, NULL, NULL, NULL, 1233, 2680, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218025241'),
(756, '2016-04-21', '2016-05-21', '2016-04-21 10:06:13', '', 'тнп 27 пал', 4, NULL, '12.000', NULL, NULL, NULL, NULL, 1234, 2680, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217941465'),
(757, '2016-04-22', '2016-05-22', '2016-04-21 10:06:16', '', 'груз 200 , + 3 чел.', 13, NULL, '0.200', NULL, NULL, NULL, NULL, 1235, 2684, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218008399'),
(758, '2016-04-21', '2016-05-21', '2016-04-21 10:06:16', 'через Керчь, самозаезд', 'два катка', 15, NULL, '11.000', NULL, NULL, NULL, NULL, 1236, 2684, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217810307'),
(759, '2016-04-21', '2016-05-21', '2016-04-21 10:06:17', '', 'металлолом', 16, NULL, '800.000', 5, NULL, NULL, NULL, 1237, 2684, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217709743'),
(760, '2016-04-21', '2016-05-21', '2016-04-21 10:06:22', 'догрузом', 'двигатель', 4, NULL, '0.070', NULL, NULL, NULL, NULL, 1238, 2692, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217224713'),
(761, '2016-04-22', '2016-05-22', '2016-04-21 10:06:25', '', 'руда в биг бегах', 1, '96.000', '40.000', NULL, NULL, NULL, NULL, 1239, 2700, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218027553'),
(762, '2016-04-22', '2016-05-22', '2016-04-21 10:06:25', '', 'трубы', 1, '86.000', '20.000', NULL, NULL, NULL, NULL, 1240, 2700, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218027117'),
(763, '2016-04-21', '2016-05-21', '2016-04-21 10:06:26', '', 'стройматериалы', 1, '86.000', '20.000', NULL, NULL, NULL, NULL, 1241, 2700, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218002893'),
(764, '2016-04-21', '2016-05-21', '2016-04-21 10:06:28', '', 'дрова', 1, '60.000', '20.000', NULL, NULL, NULL, NULL, 1242, 2700, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995211'),
(765, '2016-04-21', '2016-05-21', '2016-04-21 10:06:30', '', 'доска', 1, '30.000', '23.000', NULL, NULL, NULL, NULL, 1243, 2700, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217994345'),
(766, '2016-04-21', '2016-05-21', '2016-04-21 10:06:32', '', 'лес круглый', 1, '30.000', '30.000', NULL, NULL, NULL, NULL, 1244, 2700, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217971357'),
(767, '2016-04-21', '2016-05-21', '2016-04-21 10:06:33', '', 'плиты', 1, '100.000', '22.000', NULL, NULL, NULL, NULL, 1245, 2700, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217801685'),
(768, '2016-04-21', '2016-05-21', '2016-04-21 10:06:36', 'срочно! на сейчас!', 'металлопрокат', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1246, 2707, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034341'),
(769, '2016-04-21', '2016-05-21', '2016-04-21 10:06:37', '', 'доска сухая', 4, NULL, '18.000', NULL, NULL, NULL, NULL, 1247, 2709, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218029563'),
(770, '2016-04-21', '2016-05-21', '2016-04-21 10:06:39', '', 'доска сухая', 4, NULL, '18.000', NULL, NULL, NULL, NULL, 1248, 2709, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218029461'),
(771, '2016-04-21', '2016-05-21', '2016-04-21 10:06:40', 'сцепки', 'кукуруза', 14, NULL, '30.000', NULL, NULL, NULL, NULL, 1249, 2709, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217790703'),
(772, '2016-04-21', '2016-05-21', '2016-04-21 10:06:42', '', 'кукуруза дробленая', 14, NULL, '40.000', NULL, NULL, NULL, NULL, 1250, 2717, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034337'),
(773, '2016-04-21', '2016-05-21', '2016-04-21 10:06:47', '1 пал.', 'тнп', 4, NULL, '0.200', NULL, NULL, NULL, NULL, 1251, 2722, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217944169'),
(774, '2016-04-21', '2016-05-21', '2016-04-21 10:06:51', 'догруз,выгрузка в любое время,15 мешков', 'полимеры', 13, NULL, '0.400', NULL, NULL, NULL, NULL, 1252, 2725, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217909787'),
(775, '2016-04-21', '2016-05-21', '2016-04-21 10:06:53', '', 'жби', 1, NULL, '25.000', NULL, NULL, NULL, NULL, 1253, 2729, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217732759'),
(776, '2016-04-21', '2016-05-21', '2016-04-21 10:06:54', '', 'жби', 1, NULL, '25.000', NULL, NULL, NULL, NULL, 1254, 2729, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217268323'),
(777, '2016-04-21', '2016-05-21', '2016-04-21 10:06:57', '', 'трубы 12м', 12, NULL, '20.000', NULL, NULL, NULL, NULL, 1255, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034335'),
(778, '2016-04-21', '2016-05-21', '2016-04-21 10:06:58', '', 'строй. добавка', 4, NULL, '15.000', NULL, NULL, NULL, NULL, 1256, 2739, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217926299'),
(779, '2016-04-21', '2016-05-21', '2016-04-21 10:07:01', '', 'тнп на паллетах', 1, '86.000', '20.000', NULL, NULL, NULL, NULL, 1257, 2747, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217714039'),
(780, '2016-04-21', '2016-05-21', '2016-04-21 10:07:04', '', 'колеса', 1, NULL, '10.000', NULL, NULL, NULL, NULL, 1258, 2757, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995973'),
(781, '2016-04-21', '2016-05-21', '2016-04-21 10:07:06', '', 'колеса', 1, NULL, '10.000', NULL, NULL, NULL, NULL, 1259, 2757, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995277'),
(782, '2016-04-21', '2016-05-21', '2016-04-21 10:07:06', '', 'оборудование', 1, NULL, '21.000', NULL, NULL, NULL, NULL, 1260, 2757, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217982261'),
(783, '2016-04-22', '2016-05-22', '2016-04-21 10:07:08', 'На УТРО, выгрузка 22.04', 'жби изделия', 1, NULL, '13.000', NULL, NULL, NULL, NULL, 1261, 2757, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217957643'),
(784, '2016-04-22', '2016-05-22', '2016-04-21 10:07:09', '', 'металлопрокат', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1262, 2767, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217944705'),
(785, '2016-04-21', '2016-05-21', '2016-04-21 10:07:10', '', 'огнеупоры в биг-бегах', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1263, 2757, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217896471'),
(786, '2016-04-21', '2016-05-21', '2016-04-21 10:07:13', 'ДОГРУЗ!!!', 'труба канализационная', 4, '20.000', '1.000', NULL, NULL, NULL, NULL, 1264, 2776, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034327'),
(787, '2016-04-25', '2016-05-25', '2016-04-21 10:07:16', '', 'тнп', 4, NULL, '2.500', NULL, NULL, NULL, NULL, 1265, 2777, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217757187'),
(788, '2016-04-21', '2016-05-21', '2016-04-21 10:07:17', '', 'тнп', 1, NULL, '21.000', NULL, NULL, NULL, NULL, 1266, 2777, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216871443'),
(789, '2016-04-21', '2016-05-21', '2016-04-21 10:07:21', 'ч/з Тернопіль', 'тнв', 4, '92.000', '20.000', NULL, NULL, NULL, NULL, 1267, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218004607'),
(790, '2016-04-21', '2016-05-21', '2016-04-21 10:07:22', 'ч/з Тернопіль', 'тнв', 4, '92.000', '20.000', NULL, NULL, NULL, NULL, 1268, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218004537'),
(791, '2016-04-21', '2016-05-21', '2016-04-21 10:07:24', 'Софт или  НДС. 10 паллет', 'строй мат.', 1, NULL, '10.000', NULL, NULL, NULL, NULL, 1269, 2797, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '202410415'),
(792, '2016-04-21', '2016-05-21', '2016-04-21 10:09:13', '', 'Техника', 1, NULL, '4.000', NULL, NULL, NULL, NULL, 1290, 2815, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034839'),
(793, '2016-04-21', '2016-05-21', '2016-04-21 10:09:15', '', 'газоблок', 12, NULL, '24.000', NULL, NULL, NULL, NULL, 1291, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034837'),
(794, '2016-04-22', '2016-05-22', '2016-04-21 10:09:17', '320/тонна', 'кукуруза насыпью', 14, NULL, '40.000', NULL, NULL, NULL, NULL, 1292, 2826, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218029969'),
(795, '2016-04-22', '2016-05-22', '2016-04-21 10:09:21', '', 'тнп', 1, '30.000', '2.000', NULL, NULL, NULL, NULL, 1293, 2842, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218028967'),
(796, '2016-04-21', '2016-05-21', '2016-04-21 10:09:22', '', 'тнп', 4, '86.000', '20.000', NULL, NULL, NULL, NULL, 1294, 2842, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218025381'),
(797, '2016-04-21', '2016-05-21', '2016-04-21 10:09:23', '', 'плитка на палетах', 1, '86.000', '22.000', NULL, NULL, NULL, NULL, 1295, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218013689'),
(798, '2016-04-21', '2016-05-21', '2016-04-21 10:09:24', '', 'доска', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1296, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218010901'),
(799, '2016-04-21', '2016-05-21', '2016-04-21 10:09:26', '', 'Экскаватор', 15, NULL, '30.000', NULL, NULL, NULL, NULL, 1297, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218010269'),
(800, '2016-04-21', '2016-05-21', '2016-04-21 10:09:28', '', 'зерно в мешках.догруз', 1, '3.000', '0.900', NULL, NULL, NULL, NULL, 1298, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218009067'),
(801, '2016-04-21', '2016-05-21', '2016-04-21 10:09:29', '', 'лес-кругляк', 18, '40.000', '30.000', NULL, NULL, NULL, NULL, 1299, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218004721'),
(802, '2016-04-21', '2016-05-21', '2016-04-21 10:09:31', '', 'газобетон на под.', 1, NULL, '22.400', NULL, NULL, NULL, NULL, 1300, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218004237'),
(803, '2016-04-21', '2016-05-21', '2016-04-21 10:09:32', 'Контейнер Бортовая Зерновоз Негабаритный', 'удобрение в мешках по40кг', 1, NULL, '15.000', NULL, NULL, NULL, NULL, 1301, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218000695'),
(804, '2016-04-21', '2016-05-21', '2016-04-21 10:09:35', '', 'мебель в пачках', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1302, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217998531'),
(805, '2016-04-21', '2016-05-21', '2016-04-21 10:09:37', '', 'доска', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1303, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217997953'),
(806, '2016-04-22', '2016-05-22', '2016-04-21 10:09:41', '', 'пиломатериалы', 1, '32.000', '24.000', NULL, NULL, NULL, NULL, 1304, 2826, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217997139'),
(807, '2016-04-21', '2016-05-21', '2016-04-21 10:09:43', 'догруз', 'металлочерепица', 1, NULL, '4.500', NULL, NULL, NULL, NULL, 1305, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995929'),
(808, '2016-04-21', '2016-05-21', '2016-04-21 10:09:44', '', 'доска сухая ольха', 1, '4.000', '3.000', NULL, NULL, NULL, NULL, 1306, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217994449'),
(809, '2016-04-21', '2016-05-21', '2016-04-21 10:09:46', '', 'цемент', 1, '86.000', '22.000', NULL, NULL, NULL, NULL, 1307, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217993547'),
(810, '2016-04-21', '2016-05-21', '2016-04-21 10:09:47', '', 'цемент', 1, '30.000', '11.000', NULL, NULL, NULL, NULL, 1308, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217992633'),
(811, '2016-04-21', '2016-05-21', '2016-04-21 10:09:48', '', 'вторсырье', 4, '120.000', '12.000', NULL, NULL, NULL, NULL, 1309, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217992189'),
(812, '2016-04-21', '2016-05-21', '2016-04-21 10:09:50', 'сцепка,430/тонна', 'пшеница', 14, NULL, '45.000', NULL, NULL, NULL, NULL, 1310, 2826, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217986837'),
(813, '2016-04-21', '2016-05-21', '2016-04-21 10:09:51', 'Тент Контейнер Изотерм Реф. Контейнерово', 'известь', 8, NULL, '10.500', NULL, NULL, NULL, NULL, 1311, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217886901'),
(814, '2016-04-21', '2016-05-21', '2016-04-21 10:09:54', 'Зерновоз Негабаритный Платформа', 'строительные материалы', 12, '15.000', '20.000', NULL, NULL, NULL, NULL, 1312, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217886855'),
(815, '2016-04-21', '2016-05-21', '2016-04-21 10:09:55', 'Самосвал Зерновоз', 'галька', 10, '10.000', '10.000', NULL, NULL, NULL, NULL, 1313, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217886831'),
(816, '2016-04-21', '2016-05-21', '2016-04-21 10:09:57', '', 'двп', 1, NULL, '6.000', NULL, NULL, NULL, NULL, 1314, 2730, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217878937'),
(817, '2016-04-21', '2016-05-21', '2016-04-21 10:10:00', '', 'Брукивка.15 палетипиддони', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1315, 2730, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217793153'),
(818, '2016-04-21', '2016-05-21', '2016-04-21 10:10:01', '', 'тнп', 1, '86.000', '22.000', NULL, NULL, NULL, NULL, 1316, 2842, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216997641'),
(819, '2016-04-21', '2016-05-21', '2016-04-21 10:10:02', '', 'тнп', 1, '86.000', '22.000', NULL, NULL, NULL, NULL, 1317, 2842, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216997523'),
(820, '2016-04-21', '2016-05-21', '2016-04-21 10:10:04', '', 'тнп, 1 паллета', 4, NULL, '0.810', NULL, NULL, NULL, NULL, 1318, 2856, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218006233'),
(821, '2016-04-21', '2016-05-21', '2016-04-21 10:10:05', '', 'тнп, 4 паллеты', 4, NULL, '2.310', NULL, NULL, NULL, NULL, 1319, 2856, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217980609'),
(822, '2016-04-21', '2016-05-21', '2016-04-21 10:10:07', '', 'штукатурка в мешках', 4, '45.000', '10.000', NULL, NULL, NULL, NULL, 1320, 2673, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034827'),
(823, '2016-04-21', '2016-05-21', '2016-04-21 10:10:09', '6 палет', 'фрукты на палетах', 9, NULL, '6.000', NULL, NULL, NULL, NULL, 1321, 2858, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034825'),
(824, '2016-04-22', '2016-05-22', '2016-04-21 10:10:11', 'на 10-00', 'ТНП 12пал', 1, NULL, '13.000', NULL, NULL, NULL, NULL, 1322, 2862, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218021949'),
(825, '2016-04-22', '2016-05-22', '2016-04-21 10:10:14', 'на 10-00', 'ТНП 12пал', 1, NULL, '13.000', NULL, NULL, NULL, NULL, 1323, 2862, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218021869'),
(826, '2016-04-21', '2016-05-21', '2016-04-21 10:10:16', 'быстрая оплата, любая форма', 'продукция, тнп', 4, '86.000', '22.900', NULL, NULL, NULL, NULL, 1324, 2863, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034823'),
(827, '2016-04-21', '2016-05-21', '2016-04-21 10:10:18', 'По борту 3м!', 'Стройм на пал', 4, NULL, '2.500', NULL, NULL, NULL, NULL, 1325, 2867, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218000655'),
(828, '2016-04-22', '2016-05-22', '2016-04-21 10:10:20', 'СРОЧНО!!!', 'лакокраски', 4, '86.000', '20.000', NULL, NULL, NULL, NULL, 1326, 2870, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218021929'),
(829, '2016-04-22', '2016-05-22', '2016-04-21 10:10:21', '', '8 паллет, прод', 4, NULL, '4.000', NULL, NULL, NULL, NULL, 1327, 2870, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218013517'),
(830, '2016-04-21', '2016-05-21', '2016-04-21 10:10:24', '', 'тнп', 4, '85.000', '22.000', NULL, NULL, NULL, NULL, 1328, 2873, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217987783'),
(831, '2016-04-21', '2016-05-21', '2016-04-21 10:10:25', '', 'тнп', 4, '85.000', '22.000', NULL, NULL, NULL, NULL, 1329, 2873, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217987685'),
(832, '2016-04-22', '2016-05-22', '2016-04-21 10:10:27', '', 'тнп', 4, NULL, '5.000', NULL, NULL, NULL, NULL, 1330, 2887, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218034821'),
(833, '2016-04-21', '2016-05-21', '2016-04-21 10:10:29', '', 'тнп', 1, '86.000', '21.000', NULL, NULL, NULL, NULL, 1331, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030371'),
(834, '2016-04-21', '2016-05-21', '2016-04-21 10:10:31', '', 'тнп', 1, '86.000', '21.000', NULL, NULL, NULL, NULL, 1332, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030049'),
(835, '2016-04-21', '2016-05-21', '2016-04-21 10:10:33', '', 'тнп', 1, '86.000', '21.000', NULL, NULL, NULL, NULL, 1333, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030027'),
(836, '2016-04-21', '2016-05-21', '2016-04-21 10:10:35', '', 'тнп', 1, '86.000', '21.000', NULL, NULL, NULL, NULL, 1334, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030013'),
(837, '2016-04-22', '2016-05-22', '2016-04-21 10:10:36', '', 'продукти', 4, '86.000', '22.000', NULL, NULL, NULL, NULL, 1335, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218009615'),
(838, '2016-04-22', '2016-05-22', '2016-04-21 10:10:38', '', 'пиво', 4, '86.000', '22.900', NULL, NULL, NULL, NULL, 1336, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218005105'),
(839, '2016-04-22', '2016-05-22', '2016-04-21 10:10:39', '', 'пиво', 4, '86.000', '22.900', NULL, NULL, NULL, NULL, 1337, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217993041'),
(840, '2016-04-22', '2016-05-22', '2016-04-21 10:10:40', '', 'пиво', 4, '86.000', '22.900', NULL, NULL, NULL, NULL, 1338, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217974463'),
(841, '2016-04-22', '2016-05-22', '2016-04-21 10:10:41', '', 'пиво', 4, '86.000', '23.000', NULL, NULL, NULL, NULL, 1339, 2781, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217974443'),
(842, '2016-04-21', '2016-05-21', '2016-04-21 10:11:53', '', 'отруби в мешках', 4, NULL, '22.000', NULL, NULL, NULL, NULL, 1390, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218019339'),
(843, '2016-04-21', '2016-05-21', '2016-04-21 10:11:55', '', 'поддоны', 4, '86.000', '14.000', NULL, NULL, NULL, NULL, 1391, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218011263'),
(844, '2016-04-21', '2016-05-21', '2016-04-21 10:11:56', '', 'крупа в мешках', 4, NULL, '15.000', NULL, NULL, NULL, NULL, 1392, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218010315'),
(845, '2016-04-21', '2016-05-21', '2016-04-21 10:11:58', '(длн=6м)', 'металлопрокат', 1, NULL, '10.000', NULL, NULL, NULL, NULL, 1393, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218009725'),
(846, '2016-04-21', '2016-05-21', '2016-04-21 10:12:00', '(длн=12м)', 'металлические фермы', 16, NULL, '8.000', NULL, NULL, NULL, NULL, 1394, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995877'),
(847, '2016-04-21', '2016-05-21', '2016-04-21 10:12:01', '', 'металлопрокат', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1395, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217995331'),
(848, '2016-04-21', '2016-05-21', '2016-04-21 10:12:02', '', 'колесные пары', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1396, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217989637'),
(849, '2016-04-21', '2016-05-21', '2016-04-21 10:12:03', '', 'рис в мешках', 4, NULL, '22.000', NULL, NULL, NULL, NULL, 1397, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217989477'),
(850, '2016-04-21', '2016-05-21', '2016-04-21 10:12:03', '', 'зерно в мешках', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1398, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217987817'),
(851, '2016-04-21', '2016-05-21', '2016-04-21 10:12:06', '', 'профнастил', 1, NULL, '18.500', NULL, NULL, NULL, NULL, 1399, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217982159'),
(852, '2016-04-21', '2016-05-21', '2016-04-21 10:12:07', '', 'бумага', 4, '86.000', '12.000', NULL, NULL, NULL, NULL, 1400, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217982127'),
(853, '2016-04-22', '2016-05-22', '2016-04-21 10:12:08', '(длн=2,2м шир=0,8м выс=1,4м)', 'редуктора', 1, NULL, '5.000', NULL, NULL, NULL, NULL, 1401, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217981543'),
(854, '2016-04-21', '2016-05-21', '2016-04-21 10:12:10', '(длн=6м шир=1,5м)', 'лист метал.', 1, '86.000', '20.000', NULL, NULL, NULL, NULL, 1402, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217942919'),
(855, '2016-04-21', '2016-05-21', '2016-04-21 10:12:12', '(длн=6м)', 'пластик', 4, NULL, '7.000', NULL, NULL, NULL, NULL, 1403, 2955, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217939847'),
(856, '2016-04-21', '2016-05-21', '2016-04-21 10:12:13', '', 'комбикорм в мешках', 1, NULL, '22.700', NULL, NULL, NULL, NULL, 1404, 2955, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217913899'),
(857, '2016-04-21', '2016-05-21', '2016-04-21 10:12:15', 'Через Київ + частина б\\г з ПДВ', 'тнп на піддонах', 4, '86.000', '22.000', NULL, NULL, NULL, NULL, 1405, 2959, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218035283'),
(858, '2016-04-21', '2016-05-21', '2016-04-21 10:12:18', 'ул.Краснослободская, 1', 'тнп', 4, '12.000', '5.500', NULL, NULL, NULL, NULL, 1406, 2964, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030355'),
(859, '2016-04-22', '2016-05-22', '2016-04-21 10:12:20', '', 'ТНП в биг-бегах', 1, '20.000', '1.000', NULL, NULL, NULL, NULL, 1407, 2964, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217913935'),
(860, '2016-04-21', '2016-05-21', '2016-04-21 10:12:22', '', 'стройматериалы', 4, NULL, '21.000', NULL, NULL, NULL, NULL, 1408, 2968, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218035281'),
(861, '2016-04-21', '2016-05-21', '2016-04-21 10:12:24', '', 'тнп', 6, NULL, '20.000', NULL, NULL, NULL, NULL, 1409, 2971, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218030061'),
(862, '2016-04-21', '2016-05-21', '2016-04-21 10:12:25', '', 'тнп', 6, NULL, '20.000', NULL, NULL, NULL, NULL, 1410, 2971, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218015839'),
(863, '2016-04-21', '2016-05-21', '2016-04-21 10:12:27', 'контейнер', 'ТНП', 8, '86.000', '18.000', NULL, NULL, NULL, NULL, 1411, 2977, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218035279'),
(864, '2016-04-21', '2016-05-21', '2016-04-21 10:12:29', '', 'плитка на пал', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1412, 2985, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218029121'),
(865, '2016-04-21', '2016-05-21', '2016-04-21 10:12:31', 'от заказч., без комиссии', 'Цемент на паллетах', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1413, 2985, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217997183'),
(866, '2016-04-21', '2016-05-21', '2016-04-21 10:12:32', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'Цемент на паллетах', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1414, 2985, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217997013'),
(867, '2016-04-21', '2016-05-21', '2016-04-21 10:12:33', '', 'стройматериалы', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1415, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217972897'),
(868, '2016-04-21', '2016-05-21', '2016-04-21 10:12:34', '', 'стройматериалы', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1416, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217972879'),
(869, '2016-04-21', '2016-05-21', '2016-04-21 10:12:37', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент в мешках на палл', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1417, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217972835'),
(870, '2016-04-21', '2016-05-21', '2016-04-21 10:12:39', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент в мешках на палл', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1418, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217972807'),
(871, '2016-04-21', '2016-05-21', '2016-04-21 10:12:40', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент в мешках', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1419, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217972517'),
(872, '2016-04-21', '2016-05-21', '2016-04-21 10:12:43', '', 'тнп', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1420, 2989, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217907829'),
(873, '2016-04-21', '2016-05-21', '2016-04-21 10:12:44', '', 'Мука', 8, NULL, '20.000', NULL, NULL, NULL, NULL, 1421, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217828575'),
(874, '2016-04-21', '2016-05-21', '2016-04-21 10:12:45', '', 'доска', 1, '21.000', '21.000', NULL, NULL, NULL, NULL, 1422, 2985, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217798765'),
(875, '2016-04-21', '2016-05-21', '2016-04-21 10:12:46', '', 'доска', 1, '21.000', '21.000', NULL, NULL, NULL, NULL, 1423, 2985, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217798733'),
(876, '2016-04-21', '2016-05-21', '2016-04-21 10:12:47', '', 'цемент в мешках', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1424, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217709845'),
(877, '2016-04-21', '2016-05-21', '2016-04-21 10:12:48', '', 'цемент в мешках', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1425, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217621389'),
(878, '2016-04-21', '2016-05-21', '2016-04-21 10:12:49', '', 'металл', 1, NULL, '5.000', NULL, NULL, NULL, NULL, 1426, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217575591'),
(879, '2016-04-21', '2016-05-21', '2016-04-21 10:12:51', '', 'Мука', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1427, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217504965'),
(880, '2016-04-21', '2016-05-21', '2016-04-21 10:12:52', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'стройматериалы', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1428, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217431679'),
(881, '2016-04-21', '2016-05-21', '2016-04-21 10:12:53', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1429, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216648979'),
(882, '2016-04-21', '2016-05-21', '2016-04-21 10:12:54', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1430, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216286047'),
(883, '2016-04-21', '2016-05-21', '2016-04-21 10:12:54', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'цемент', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1431, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '216121707'),
(884, '2016-04-21', '2016-05-21', '2016-04-21 10:12:55', 'ГРУЗОВЛАДЕЛЕЦ, без комиссии', 'кирпич на пал', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1432, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '215686097'),
(885, '2016-04-21', '2016-05-21', '2016-04-21 10:12:56', 'ГРУЗОВЛАДЕЛЕЦ', 'цемент', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1433, 2985, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '215635583'),
(886, '2016-04-21', '2016-05-21', '2016-04-21 10:12:58', '', 'пиво', 1, NULL, '23.000', NULL, NULL, NULL, NULL, 1434, 3008, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '218035277'),
(887, '2016-04-21', '2016-05-21', '2016-04-21 10:13:00', '', 'плитка тротуарная на палл', 1, NULL, '20.000', NULL, NULL, NULL, NULL, 1435, 3012, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217982791'),
(888, '2016-04-21', '2016-05-21', '2016-04-21 10:13:01', '', 'сырье в мешках', 4, NULL, '2.000', NULL, NULL, NULL, NULL, 1436, 3012, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217932523'),
(889, '2016-04-21', '2016-05-21', '2016-04-21 10:13:02', '', 'сырье в мешках погрузка в', 4, NULL, '21.000', NULL, NULL, NULL, NULL, 1437, 3012, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217917889'),
(890, '2016-04-21', '2016-05-21', '2016-04-21 10:13:02', '', 'шпала', 1, NULL, '22.000', NULL, NULL, NULL, NULL, 1438, 3012, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217901259'),
(891, '2016-04-21', '2016-05-21', '2016-04-21 10:13:04', '', 'секции заборные', 1, '80.000', '21.000', NULL, NULL, NULL, NULL, 1439, 3012, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'no', 'active', '217897823');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Cargo_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Cargo_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5383 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Cargo_to_Geo_Relation`
--

INSERT INTO `Frontend_Cargo_to_Geo_Relation` (`ID`, `type`, `geoID`, `requestID`, `groupNumber`) VALUES
(4452, 'from', 1067, 731, 1),
(4791, 'from', 1067, 791, 1),
(5183, 'from', 1067, 860, 1),
(4453, 'from', 1068, 731, 1),
(4483, 'from', 1068, 737, 1),
(4511, 'from', 1068, 742, 1),
(4792, 'from', 1068, 791, 1),
(4796, 'from', 1068, 792, 1),
(4913, 'from', 1068, 812, 1),
(5184, 'from', 1068, 860, 1),
(4424, 'from', 1069, 726, 1),
(4430, 'from', 1069, 727, 1),
(4436, 'from', 1069, 728, 1),
(4443, 'from', 1069, 729, 1),
(4448, 'from', 1069, 730, 1),
(4454, 'from', 1069, 731, 1),
(4460, 'from', 1069, 733, 1),
(4466, 'from', 1069, 734, 1),
(4472, 'from', 1069, 735, 1),
(4478, 'from', 1069, 736, 1),
(4484, 'from', 1069, 737, 1),
(4490, 'from', 1069, 738, 1),
(4494, 'from', 1069, 739, 1),
(4500, 'from', 1069, 740, 1),
(4506, 'from', 1069, 741, 1),
(4512, 'from', 1069, 742, 1),
(4518, 'from', 1069, 743, 1),
(4524, 'from', 1069, 744, 1),
(4530, 'from', 1069, 745, 1),
(4536, 'from', 1069, 746, 1),
(4542, 'from', 1069, 747, 1),
(4548, 'from', 1069, 748, 1),
(4554, 'from', 1069, 749, 1),
(4558, 'from', 1069, 750, 1),
(4564, 'from', 1069, 751, 1),
(4570, 'from', 1069, 752, 1),
(4576, 'from', 1069, 753, 1),
(4581, 'from', 1069, 754, 1),
(4587, 'from', 1069, 755, 1),
(4593, 'from', 1069, 756, 1),
(4598, 'from', 1069, 757, 1),
(4608, 'from', 1069, 759, 1),
(4614, 'from', 1069, 760, 1),
(4620, 'from', 1069, 761, 1),
(4624, 'from', 1069, 762, 1),
(4630, 'from', 1069, 763, 1),
(4636, 'from', 1069, 764, 1),
(4642, 'from', 1069, 765, 1),
(4648, 'from', 1069, 766, 1),
(4654, 'from', 1069, 767, 1),
(4660, 'from', 1069, 768, 1),
(4666, 'from', 1069, 769, 1),
(4672, 'from', 1069, 770, 1),
(4678, 'from', 1069, 771, 1),
(4684, 'from', 1069, 772, 1),
(4690, 'from', 1069, 773, 1),
(4696, 'from', 1069, 774, 1),
(4702, 'from', 1069, 775, 1),
(4707, 'from', 1069, 776, 1),
(4711, 'from', 1069, 777, 1),
(4717, 'from', 1069, 778, 1),
(4723, 'from', 1069, 779, 1),
(4729, 'from', 1069, 780, 1),
(4735, 'from', 1069, 781, 1),
(4741, 'from', 1069, 782, 1),
(4747, 'from', 1069, 783, 1),
(4753, 'from', 1069, 784, 1),
(4759, 'from', 1069, 785, 1),
(4765, 'from', 1069, 786, 1),
(4771, 'from', 1069, 787, 1),
(4775, 'from', 1069, 788, 1),
(4781, 'from', 1069, 789, 1),
(4787, 'from', 1069, 790, 1),
(4793, 'from', 1069, 791, 1),
(4797, 'from', 1069, 792, 1),
(4803, 'from', 1069, 793, 1),
(4807, 'from', 1069, 794, 1),
(4813, 'from', 1069, 795, 1),
(4819, 'from', 1069, 796, 1),
(4825, 'from', 1069, 797, 1),
(4831, 'from', 1069, 798, 1),
(4837, 'from', 1069, 799, 1),
(4843, 'from', 1069, 800, 1),
(4849, 'from', 1069, 801, 1),
(4855, 'from', 1069, 802, 1),
(4861, 'from', 1069, 803, 1),
(4866, 'from', 1069, 804, 1),
(4872, 'from', 1069, 805, 1),
(4878, 'from', 1069, 806, 1),
(4884, 'from', 1069, 807, 1),
(4890, 'from', 1069, 808, 1),
(4896, 'from', 1069, 809, 1),
(4902, 'from', 1069, 810, 1),
(4908, 'from', 1069, 811, 1),
(4914, 'from', 1069, 812, 1),
(4920, 'from', 1069, 813, 1),
(4926, 'from', 1069, 814, 1),
(4932, 'from', 1069, 815, 1),
(4938, 'from', 1069, 816, 1),
(4942, 'from', 1069, 817, 1),
(4948, 'from', 1069, 818, 1),
(4954, 'from', 1069, 819, 1),
(4959, 'from', 1069, 820, 1),
(4964, 'from', 1069, 821, 1),
(4970, 'from', 1069, 822, 1),
(4974, 'from', 1069, 823, 1),
(4980, 'from', 1069, 824, 1),
(4986, 'from', 1069, 825, 1),
(4991, 'from', 1069, 826, 1),
(4995, 'from', 1069, 827, 1),
(5001, 'from', 1069, 828, 1),
(5007, 'from', 1069, 829, 1),
(5013, 'from', 1069, 830, 1),
(5019, 'from', 1069, 831, 1),
(5025, 'from', 1069, 832, 1),
(5030, 'from', 1069, 833, 1),
(5036, 'from', 1069, 834, 1),
(5042, 'from', 1069, 835, 1),
(5048, 'from', 1069, 836, 1),
(5052, 'from', 1069, 837, 1),
(5058, 'from', 1069, 838, 1),
(5064, 'from', 1069, 839, 1),
(5070, 'from', 1069, 840, 1),
(5075, 'from', 1069, 841, 1),
(5081, 'from', 1069, 842, 1),
(5086, 'from', 1069, 843, 1),
(5092, 'from', 1069, 844, 1),
(5098, 'from', 1069, 845, 1),
(5102, 'from', 1069, 846, 1),
(5108, 'from', 1069, 847, 1),
(5114, 'from', 1069, 848, 1),
(5120, 'from', 1069, 849, 1),
(5126, 'from', 1069, 850, 1),
(5131, 'from', 1069, 851, 1),
(5137, 'from', 1069, 852, 1),
(5143, 'from', 1069, 853, 1),
(5149, 'from', 1069, 854, 1),
(5155, 'from', 1069, 855, 1),
(5161, 'from', 1069, 856, 1),
(5167, 'from', 1069, 857, 1),
(5173, 'from', 1069, 858, 1),
(5179, 'from', 1069, 859, 1),
(5185, 'from', 1069, 860, 1),
(5191, 'from', 1069, 861, 1),
(5197, 'from', 1069, 862, 1),
(5202, 'from', 1069, 863, 1),
(5208, 'from', 1069, 864, 1),
(5214, 'from', 1069, 865, 1),
(5220, 'from', 1069, 866, 1),
(5226, 'from', 1069, 867, 1),
(5238, 'from', 1069, 869, 1),
(5244, 'from', 1069, 870, 1),
(5250, 'from', 1069, 871, 1),
(5256, 'from', 1069, 872, 1),
(5262, 'from', 1069, 873, 1),
(5268, 'from', 1069, 874, 1),
(5274, 'from', 1069, 875, 1),
(5280, 'from', 1069, 876, 1),
(5286, 'from', 1069, 877, 1),
(5292, 'from', 1069, 878, 1),
(5298, 'from', 1069, 879, 1),
(5304, 'from', 1069, 880, 1),
(5310, 'from', 1069, 881, 1),
(5316, 'from', 1069, 882, 1),
(5322, 'from', 1069, 883, 1),
(5328, 'from', 1069, 884, 1),
(5334, 'from', 1069, 885, 1),
(5340, 'from', 1069, 886, 1),
(5346, 'from', 1069, 887, 1),
(5352, 'from', 1069, 888, 1),
(5358, 'from', 1069, 889, 1),
(5364, 'from', 1069, 890, 1),
(5369, 'from', 1069, 891, 1),
(4422, 'from', 1070, 726, 1),
(4423, 'from', 1071, 726, 1),
(4641, 'from', 1071, 765, 1),
(4764, 'from', 1071, 786, 1),
(4458, 'from', 1072, 733, 1),
(4540, 'from', 1072, 747, 1),
(4751, 'from', 1072, 784, 1),
(4894, 'from', 1072, 809, 1),
(4900, 'from', 1072, 810, 1),
(4924, 'from', 1072, 814, 1),
(5112, 'from', 1072, 848, 1),
(5159, 'from', 1072, 856, 1),
(5212, 'from', 1072, 865, 1),
(5218, 'from', 1072, 866, 1),
(5236, 'from', 1072, 869, 1),
(5242, 'from', 1072, 870, 1),
(5248, 'from', 1072, 871, 1),
(5278, 'from', 1072, 876, 1),
(5284, 'from', 1072, 877, 1),
(5308, 'from', 1072, 881, 1),
(5314, 'from', 1072, 882, 1),
(5320, 'from', 1072, 883, 1),
(5332, 'from', 1072, 885, 1),
(4459, 'from', 1073, 733, 1),
(4465, 'from', 1073, 734, 1),
(4541, 'from', 1073, 747, 1),
(4553, 'from', 1073, 749, 1),
(4563, 'from', 1073, 751, 1),
(4659, 'from', 1073, 768, 1),
(4677, 'from', 1073, 771, 1),
(4716, 'from', 1073, 778, 1),
(4722, 'from', 1073, 779, 1),
(4752, 'from', 1073, 784, 1),
(4786, 'from', 1073, 790, 1),
(4836, 'from', 1073, 799, 1),
(4842, 'from', 1073, 800, 1),
(4860, 'from', 1073, 803, 1),
(4895, 'from', 1073, 809, 1),
(4901, 'from', 1073, 810, 1),
(4925, 'from', 1073, 814, 1),
(5000, 'from', 1073, 828, 1),
(5080, 'from', 1073, 842, 1),
(5097, 'from', 1073, 845, 1),
(5107, 'from', 1073, 847, 1),
(5113, 'from', 1073, 848, 1),
(5142, 'from', 1073, 853, 1),
(5148, 'from', 1073, 854, 1),
(5160, 'from', 1073, 856, 1),
(5178, 'from', 1073, 859, 1),
(5213, 'from', 1073, 865, 1),
(5219, 'from', 1073, 866, 1),
(5237, 'from', 1073, 869, 1),
(5243, 'from', 1073, 870, 1),
(5249, 'from', 1073, 871, 1),
(5255, 'from', 1073, 872, 1),
(5279, 'from', 1073, 876, 1),
(5285, 'from', 1073, 877, 1),
(5309, 'from', 1073, 881, 1),
(5315, 'from', 1073, 882, 1),
(5321, 'from', 1073, 883, 1),
(5327, 'from', 1073, 884, 1),
(5333, 'from', 1073, 885, 1),
(5345, 'from', 1073, 887, 1),
(5357, 'from', 1073, 889, 1),
(4535, 'from', 1074, 746, 1),
(4629, 'from', 1074, 763, 1),
(4802, 'from', 1074, 793, 1),
(4865, 'from', 1074, 804, 1),
(4979, 'from', 1074, 824, 1),
(5012, 'from', 1074, 830, 1),
(5018, 'from', 1074, 831, 1),
(5029, 'from', 1074, 833, 1),
(5035, 'from', 1074, 834, 1),
(5041, 'from', 1074, 835, 1),
(5047, 'from', 1074, 836, 1),
(5101, 'from', 1074, 846, 1),
(5125, 'from', 1074, 850, 1),
(5172, 'from', 1074, 858, 1),
(5267, 'from', 1074, 874, 1),
(5363, 'from', 1074, 890, 1),
(4428, 'from', 1075, 727, 1),
(4429, 'from', 1076, 727, 1),
(4489, 'from', 1076, 738, 1),
(4854, 'from', 1076, 802, 1),
(5166, 'from', 1076, 857, 1),
(5225, 'from', 1076, 867, 1),
(4628, 'from', 1077, 763, 1),
(4801, 'from', 1077, 793, 1),
(5100, 'from', 1077, 846, 1),
(4434, 'from', 1078, 728, 1),
(4435, 'from', 1079, 728, 1),
(4613, 'from', 1079, 760, 1),
(4907, 'from', 1079, 811, 1),
(4705, 'from', 1081, 776, 1),
(4665, 'from', 1082, 769, 1),
(4706, 'from', 1082, 776, 1),
(4441, 'from', 1083, 729, 1),
(4700, 'from', 1083, 775, 1),
(5135, 'from', 1083, 852, 1),
(5260, 'from', 1083, 873, 1),
(4442, 'from', 1084, 729, 1),
(4635, 'from', 1084, 764, 1),
(4701, 'from', 1084, 775, 1),
(5136, 'from', 1084, 852, 1),
(5261, 'from', 1084, 873, 1),
(5291, 'from', 1084, 878, 1),
(5297, 'from', 1084, 879, 1),
(4580, 'from', 1085, 754, 1),
(4597, 'from', 1085, 757, 1),
(4710, 'from', 1085, 777, 1),
(4958, 'from', 1085, 820, 1),
(4963, 'from', 1085, 821, 1),
(4990, 'from', 1085, 826, 1),
(5085, 'from', 1085, 843, 1),
(5130, 'from', 1085, 851, 1),
(5201, 'from', 1085, 863, 1),
(5368, 'from', 1085, 891, 1),
(4446, 'from', 1086, 730, 1),
(4568, 'from', 1086, 752, 1),
(4882, 'from', 1086, 807, 1),
(5118, 'from', 1086, 849, 1),
(4447, 'from', 1087, 730, 1),
(4499, 'from', 1087, 740, 1),
(4505, 'from', 1087, 741, 1),
(4547, 'from', 1087, 748, 1),
(4569, 'from', 1087, 752, 1),
(4883, 'from', 1087, 807, 1),
(5119, 'from', 1087, 849, 1),
(4740, 'from', 1090, 782, 1),
(4552, 'from', 1091, 749, 1),
(4562, 'from', 1091, 751, 1),
(4658, 'from', 1091, 768, 1),
(4715, 'from', 1091, 778, 1),
(4785, 'from', 1091, 790, 1),
(5096, 'from', 1091, 845, 1),
(5106, 'from', 1091, 847, 1),
(5141, 'from', 1091, 853, 1),
(5177, 'from', 1091, 859, 1),
(5326, 'from', 1091, 884, 1),
(5356, 'from', 1091, 889, 1),
(4464, 'from', 1092, 734, 1),
(4585, 'from', 1093, 755, 1),
(4591, 'from', 1093, 756, 1),
(4733, 'from', 1093, 781, 1),
(4745, 'from', 1093, 783, 1),
(4952, 'from', 1093, 819, 1),
(4586, 'from', 1094, 755, 1),
(4592, 'from', 1094, 756, 1),
(4683, 'from', 1094, 772, 1),
(4734, 'from', 1094, 781, 1),
(4746, 'from', 1094, 783, 1),
(4953, 'from', 1094, 819, 1),
(4470, 'from', 1095, 735, 1),
(4492, 'from', 1095, 739, 1),
(4471, 'from', 1096, 735, 1),
(4493, 'from', 1096, 739, 1),
(4671, 'from', 1096, 770, 1),
(4848, 'from', 1096, 801, 1),
(4985, 'from', 1096, 825, 1),
(5091, 'from', 1096, 844, 1),
(4476, 'from', 1098, 736, 1),
(4618, 'from', 1098, 761, 1),
(4779, 'from', 1098, 789, 1),
(5005, 'from', 1098, 829, 1),
(5062, 'from', 1098, 839, 1),
(5073, 'from', 1098, 841, 1),
(4477, 'from', 1099, 736, 1),
(4517, 'from', 1099, 743, 1),
(4523, 'from', 1099, 744, 1),
(4529, 'from', 1099, 745, 1),
(4575, 'from', 1099, 753, 1),
(4619, 'from', 1099, 761, 1),
(4780, 'from', 1099, 789, 1),
(4806, 'from', 1099, 794, 1),
(5006, 'from', 1099, 829, 1),
(5063, 'from', 1099, 839, 1),
(5074, 'from', 1099, 841, 1),
(4931, 'from', 1101, 815, 1),
(4482, 'from', 1102, 737, 1),
(4612, 'from', 1103, 760, 1),
(4906, 'from', 1103, 811, 1),
(4488, 'from', 1104, 738, 1),
(4853, 'from', 1104, 802, 1),
(4689, 'from', 1106, 773, 1),
(4824, 'from', 1106, 797, 1),
(4871, 'from', 1106, 805, 1),
(4877, 'from', 1106, 806, 1),
(4919, 'from', 1106, 813, 1),
(4937, 'from', 1106, 816, 1),
(4498, 'from', 1107, 740, 1),
(4504, 'from', 1107, 741, 1),
(4510, 'from', 1108, 742, 1),
(4830, 'from', 1110, 798, 1),
(4516, 'from', 1114, 743, 1),
(4522, 'from', 1114, 744, 1),
(4528, 'from', 1114, 745, 1),
(4574, 'from', 1114, 753, 1),
(4534, 'from', 1117, 746, 1),
(4999, 'from', 1118, 828, 1),
(5254, 'from', 1118, 872, 1),
(4769, 'from', 1119, 787, 1),
(4811, 'from', 1119, 795, 1),
(4817, 'from', 1119, 796, 1),
(4968, 'from', 1119, 822, 1),
(4972, 'from', 1119, 823, 1),
(5056, 'from', 1119, 838, 1),
(5068, 'from', 1119, 840, 1),
(5189, 'from', 1119, 861, 1),
(5195, 'from', 1119, 862, 1),
(5338, 'from', 1119, 886, 1),
(4770, 'from', 1120, 787, 1),
(4774, 'from', 1120, 788, 1),
(4812, 'from', 1120, 795, 1),
(4818, 'from', 1120, 796, 1),
(4969, 'from', 1120, 822, 1),
(4973, 'from', 1120, 823, 1),
(5051, 'from', 1120, 837, 1),
(5057, 'from', 1120, 838, 1),
(5069, 'from', 1120, 840, 1),
(5190, 'from', 1120, 861, 1),
(5196, 'from', 1120, 862, 1),
(5339, 'from', 1120, 886, 1),
(4546, 'from', 1121, 748, 1),
(4694, 'from', 1124, 774, 1),
(5206, 'from', 1124, 864, 1),
(4695, 'from', 1125, 774, 1),
(4889, 'from', 1125, 808, 1),
(5207, 'from', 1125, 864, 1),
(5273, 'from', 1125, 875, 1),
(4602, 'from', 1129, 758, 1),
(4606, 'from', 1130, 759, 1),
(5153, 'from', 1130, 855, 1),
(4607, 'from', 1131, 759, 1),
(4647, 'from', 1131, 766, 1),
(5154, 'from', 1131, 855, 1),
(5165, 'from', 1132, 857, 1),
(5224, 'from', 1132, 867, 1),
(5344, 'from', 1135, 887, 1),
(4634, 'from', 1136, 764, 1),
(4640, 'from', 1138, 765, 1),
(4646, 'from', 1139, 766, 1),
(4652, 'from', 1140, 767, 1),
(4653, 'from', 1141, 767, 1),
(4728, 'from', 1141, 780, 1),
(4947, 'from', 1141, 818, 1),
(5303, 'from', 1141, 880, 1),
(4664, 'from', 1142, 769, 1),
(4670, 'from', 1144, 770, 1),
(4676, 'from', 1145, 771, 1),
(4682, 'from', 1146, 772, 1),
(5024, 'from', 1148, 832, 1),
(4688, 'from', 1149, 773, 1),
(4721, 'from', 1153, 779, 1),
(4727, 'from', 1154, 780, 1),
(4946, 'from', 1154, 818, 1),
(4739, 'from', 1155, 782, 1),
(4757, 'from', 1157, 785, 1),
(4758, 'from', 1158, 785, 1),
(5351, 'from', 1158, 888, 1),
(4763, 'from', 1159, 786, 1),
(4773, 'from', 1160, 788, 1),
(4795, 'from', 1166, 792, 1),
(4805, 'from', 1168, 794, 1),
(4823, 'from', 1170, 797, 1),
(4829, 'from', 1171, 798, 1),
(4835, 'from', 1172, 799, 1),
(4841, 'from', 1174, 800, 1),
(5079, 'from', 1174, 842, 1),
(4847, 'from', 1175, 801, 1),
(4984, 'from', 1175, 825, 1),
(4859, 'from', 1176, 803, 1),
(4864, 'from', 1178, 804, 1),
(4870, 'from', 1180, 805, 1),
(4876, 'from', 1183, 806, 1),
(4888, 'from', 1185, 808, 1),
(4912, 'from', 1189, 812, 1),
(4918, 'from', 1191, 813, 1),
(4930, 'from', 1193, 815, 1),
(4936, 'from', 1195, 816, 1),
(5302, 'from', 1196, 880, 1),
(4978, 'from', 1199, 824, 1),
(5011, 'from', 1201, 830, 1),
(5017, 'from', 1201, 831, 1),
(5023, 'from', 1202, 832, 1),
(5028, 'from', 1203, 833, 1),
(5034, 'from', 1205, 834, 1),
(5040, 'from', 1205, 835, 1),
(5046, 'from', 1205, 836, 1),
(5050, 'from', 1207, 837, 1),
(5090, 'from', 1218, 844, 1),
(5124, 'from', 1220, 850, 1),
(5147, 'from', 1224, 854, 1),
(5350, 'from', 1226, 888, 1),
(5171, 'from', 1227, 858, 1),
(5230, 'from', 1232, 868, 1),
(5231, 'from', 1233, 868, 1),
(5232, 'from', 1234, 868, 1),
(5266, 'from', 1239, 874, 1),
(5272, 'from', 1240, 875, 1),
(5290, 'from', 1241, 878, 1),
(5296, 'from', 1243, 879, 1),
(5362, 'from', 1246, 890, 1),
(4820, 'to', 1067, 796, 2),
(4975, 'to', 1067, 823, 2),
(5150, 'to', 1067, 854, 2),
(4589, 'to', 1068, 755, 2),
(4595, 'to', 1068, 756, 2),
(4698, 'to', 1068, 774, 2),
(4743, 'to', 1068, 782, 2),
(4821, 'to', 1068, 796, 2),
(4976, 'to', 1068, 823, 2),
(5151, 'to', 1068, 854, 2),
(4427, 'to', 1069, 726, 2),
(4433, 'to', 1069, 727, 2),
(4440, 'to', 1069, 728, 2),
(4445, 'to', 1069, 729, 2),
(4451, 'to', 1069, 730, 2),
(4456, 'to', 1069, 731, 2),
(4463, 'to', 1069, 733, 2),
(4469, 'to', 1069, 734, 2),
(4475, 'to', 1069, 735, 2),
(4481, 'to', 1069, 736, 2),
(4487, 'to', 1069, 737, 2),
(4491, 'to', 1069, 738, 2),
(4497, 'to', 1069, 739, 2),
(4503, 'to', 1069, 740, 2),
(4509, 'to', 1069, 741, 2),
(4515, 'to', 1069, 742, 2),
(4521, 'to', 1069, 743, 2),
(4527, 'to', 1069, 744, 2),
(4533, 'to', 1069, 745, 2),
(4539, 'to', 1069, 746, 2),
(4545, 'to', 1069, 747, 2),
(4551, 'to', 1069, 748, 2),
(4557, 'to', 1069, 749, 2),
(4561, 'to', 1069, 750, 2),
(4567, 'to', 1069, 751, 2),
(4573, 'to', 1069, 752, 2),
(4579, 'to', 1069, 753, 2),
(4584, 'to', 1069, 754, 2),
(4590, 'to', 1069, 755, 2),
(4596, 'to', 1069, 756, 2),
(4601, 'to', 1069, 757, 2),
(4605, 'to', 1069, 758, 2),
(4611, 'to', 1069, 759, 2),
(4617, 'to', 1069, 760, 2),
(4623, 'to', 1069, 761, 2),
(4627, 'to', 1069, 762, 2),
(4633, 'to', 1069, 763, 2),
(4639, 'to', 1069, 764, 2),
(4645, 'to', 1069, 765, 2),
(4651, 'to', 1069, 766, 2),
(4657, 'to', 1069, 767, 2),
(4663, 'to', 1069, 768, 2),
(4669, 'to', 1069, 769, 2),
(4675, 'to', 1069, 770, 2),
(4681, 'to', 1069, 771, 2),
(4687, 'to', 1069, 772, 2),
(4693, 'to', 1069, 773, 2),
(4699, 'to', 1069, 774, 2),
(4704, 'to', 1069, 775, 2),
(4709, 'to', 1069, 776, 2),
(4714, 'to', 1069, 777, 2),
(4720, 'to', 1069, 778, 2),
(4726, 'to', 1069, 779, 2),
(4732, 'to', 1069, 780, 2),
(4738, 'to', 1069, 781, 2),
(4744, 'to', 1069, 782, 2),
(4750, 'to', 1069, 783, 2),
(4756, 'to', 1069, 784, 2),
(4762, 'to', 1069, 785, 2),
(4768, 'to', 1069, 786, 2),
(4772, 'to', 1069, 787, 2),
(4778, 'to', 1069, 788, 2),
(4784, 'to', 1069, 789, 2),
(4790, 'to', 1069, 790, 2),
(4794, 'to', 1069, 791, 2),
(4800, 'to', 1069, 792, 2),
(4804, 'to', 1069, 793, 2),
(4810, 'to', 1069, 794, 2),
(4816, 'to', 1069, 795, 2),
(4822, 'to', 1069, 796, 2),
(4828, 'to', 1069, 797, 2),
(4834, 'to', 1069, 798, 2),
(4840, 'to', 1069, 799, 2),
(4846, 'to', 1069, 800, 2),
(4852, 'to', 1069, 801, 2),
(4858, 'to', 1069, 802, 2),
(4869, 'to', 1069, 804, 2),
(4881, 'to', 1069, 806, 2),
(4887, 'to', 1069, 807, 2),
(4893, 'to', 1069, 808, 2),
(4899, 'to', 1069, 809, 2),
(4905, 'to', 1069, 810, 2),
(4911, 'to', 1069, 811, 2),
(4917, 'to', 1069, 812, 2),
(4923, 'to', 1069, 813, 2),
(4929, 'to', 1069, 814, 2),
(4935, 'to', 1069, 815, 2),
(4941, 'to', 1069, 816, 2),
(4945, 'to', 1069, 817, 2),
(4951, 'to', 1069, 818, 2),
(4957, 'to', 1069, 819, 2),
(4962, 'to', 1069, 820, 2),
(4967, 'to', 1069, 821, 2),
(4971, 'to', 1069, 822, 2),
(4977, 'to', 1069, 823, 2),
(4983, 'to', 1069, 824, 2),
(4989, 'to', 1069, 825, 2),
(4994, 'to', 1069, 826, 2),
(4998, 'to', 1069, 827, 2),
(5004, 'to', 1069, 828, 2),
(5010, 'to', 1069, 829, 2),
(5016, 'to', 1069, 830, 2),
(5022, 'to', 1069, 831, 2),
(5027, 'to', 1069, 832, 2),
(5033, 'to', 1069, 833, 2),
(5039, 'to', 1069, 834, 2),
(5045, 'to', 1069, 835, 2),
(5049, 'to', 1069, 836, 2),
(5055, 'to', 1069, 837, 2),
(5061, 'to', 1069, 838, 2),
(5067, 'to', 1069, 839, 2),
(5072, 'to', 1069, 840, 2),
(5078, 'to', 1069, 841, 2),
(5084, 'to', 1069, 842, 2),
(5089, 'to', 1069, 843, 2),
(5095, 'to', 1069, 844, 2),
(5099, 'to', 1069, 845, 2),
(5105, 'to', 1069, 846, 2),
(5111, 'to', 1069, 847, 2),
(5117, 'to', 1069, 848, 2),
(5123, 'to', 1069, 849, 2),
(5129, 'to', 1069, 850, 2),
(5134, 'to', 1069, 851, 2),
(5140, 'to', 1069, 852, 2),
(5146, 'to', 1069, 853, 2),
(5152, 'to', 1069, 854, 2),
(5158, 'to', 1069, 855, 2),
(5164, 'to', 1069, 856, 2),
(5170, 'to', 1069, 857, 2),
(5176, 'to', 1069, 858, 2),
(5182, 'to', 1069, 859, 2),
(5188, 'to', 1069, 860, 2),
(5194, 'to', 1069, 861, 2),
(5200, 'to', 1069, 862, 2),
(5205, 'to', 1069, 863, 2),
(5211, 'to', 1069, 864, 2),
(5217, 'to', 1069, 865, 2),
(5223, 'to', 1069, 866, 2),
(5229, 'to', 1069, 867, 2),
(5235, 'to', 1069, 868, 2),
(5241, 'to', 1069, 869, 2),
(5247, 'to', 1069, 870, 2),
(5253, 'to', 1069, 871, 2),
(5259, 'to', 1069, 872, 2),
(5265, 'to', 1069, 873, 2),
(5271, 'to', 1069, 874, 2),
(5277, 'to', 1069, 875, 2),
(5283, 'to', 1069, 876, 2),
(5289, 'to', 1069, 877, 2),
(5295, 'to', 1069, 878, 2),
(5301, 'to', 1069, 879, 2),
(5307, 'to', 1069, 880, 2),
(5313, 'to', 1069, 881, 2),
(5319, 'to', 1069, 882, 2),
(5325, 'to', 1069, 883, 2),
(5331, 'to', 1069, 884, 2),
(5337, 'to', 1069, 885, 2),
(5343, 'to', 1069, 886, 2),
(5349, 'to', 1069, 887, 2),
(5355, 'to', 1069, 888, 2),
(5361, 'to', 1069, 889, 2),
(5367, 'to', 1069, 890, 2),
(5372, 'to', 1069, 891, 2),
(4450, 'to', 1071, 730, 2),
(4468, 'to', 1071, 734, 2),
(4520, 'to', 1071, 743, 2),
(4526, 'to', 1071, 744, 2),
(4638, 'to', 1071, 764, 2),
(5009, 'to', 1071, 829, 2),
(5163, 'to', 1071, 856, 2),
(4425, 'to', 1072, 726, 2),
(4712, 'to', 1072, 777, 2),
(4748, 'to', 1072, 783, 2),
(5233, 'to', 1072, 868, 2),
(4426, 'to', 1073, 726, 2),
(4502, 'to', 1073, 740, 2),
(4508, 'to', 1073, 741, 2),
(4544, 'to', 1073, 747, 2),
(4632, 'to', 1073, 763, 2),
(4713, 'to', 1073, 777, 2),
(4719, 'to', 1073, 778, 2),
(4749, 'to', 1073, 783, 2),
(4839, 'to', 1073, 799, 2),
(5122, 'to', 1073, 849, 2),
(5234, 'to', 1073, 868, 2),
(5240, 'to', 1073, 869, 2),
(5246, 'to', 1073, 870, 2),
(5312, 'to', 1073, 881, 2),
(5354, 'to', 1073, 888, 2),
(4432, 'to', 1074, 727, 2),
(4583, 'to', 1074, 754, 2),
(4668, 'to', 1074, 769, 2),
(4674, 'to', 1074, 770, 2),
(4692, 'to', 1074, 773, 2),
(4910, 'to', 1074, 811, 2),
(4982, 'to', 1074, 824, 2),
(4988, 'to', 1074, 825, 2),
(5054, 'to', 1074, 837, 2),
(5060, 'to', 1074, 838, 2),
(5348, 'to', 1074, 887, 2),
(4879, 'to', 1075, 806, 2),
(5323, 'to', 1075, 883, 2),
(4600, 'to', 1076, 757, 2),
(4610, 'to', 1076, 759, 2),
(4644, 'to', 1076, 765, 2),
(4656, 'to', 1076, 767, 2),
(4880, 'to', 1076, 806, 2),
(4898, 'to', 1076, 809, 2),
(4904, 'to', 1076, 810, 2),
(5083, 'to', 1076, 842, 2),
(5228, 'to', 1076, 867, 2),
(5252, 'to', 1076, 871, 2),
(5306, 'to', 1076, 880, 2),
(5318, 'to', 1076, 882, 2),
(5324, 'to', 1076, 883, 2),
(5330, 'to', 1076, 884, 2),
(5336, 'to', 1076, 885, 2),
(5360, 'to', 1076, 889, 2),
(4431, 'to', 1077, 727, 2),
(4486, 'to', 1079, 737, 2),
(4857, 'to', 1079, 802, 2),
(4438, 'to', 1081, 728, 2),
(4649, 'to', 1081, 766, 2),
(4439, 'to', 1082, 728, 2),
(4650, 'to', 1082, 766, 2),
(5014, 'to', 1083, 830, 2),
(5015, 'to', 1084, 830, 2),
(4444, 'to', 1085, 729, 2),
(4455, 'to', 1085, 731, 2),
(4703, 'to', 1085, 775, 2),
(4708, 'to', 1085, 776, 2),
(5026, 'to', 1085, 832, 2),
(5071, 'to', 1085, 840, 2),
(4549, 'to', 1086, 748, 2),
(4621, 'to', 1086, 761, 2),
(4679, 'to', 1086, 771, 2),
(4814, 'to', 1086, 795, 2),
(4826, 'to', 1086, 797, 2),
(4844, 'to', 1086, 800, 2),
(5076, 'to', 1086, 841, 2),
(5093, 'to', 1086, 844, 2),
(5132, 'to', 1086, 851, 2),
(5174, 'to', 1086, 858, 2),
(5203, 'to', 1086, 863, 2),
(5370, 'to', 1086, 891, 2),
(4532, 'to', 1087, 745, 2),
(4550, 'to', 1087, 748, 2),
(4622, 'to', 1087, 761, 2),
(4626, 'to', 1087, 762, 2),
(4680, 'to', 1087, 771, 2),
(4809, 'to', 1087, 794, 2),
(4815, 'to', 1087, 795, 2),
(4827, 'to', 1087, 797, 2),
(4845, 'to', 1087, 800, 2),
(4886, 'to', 1087, 807, 2),
(5066, 'to', 1087, 839, 2),
(5077, 'to', 1087, 841, 2),
(5094, 'to', 1087, 844, 2),
(5110, 'to', 1087, 847, 2),
(5133, 'to', 1087, 851, 2),
(5175, 'to', 1087, 858, 2),
(5204, 'to', 1087, 863, 2),
(5371, 'to', 1087, 891, 2),
(4449, 'to', 1088, 730, 2),
(4467, 'to', 1088, 734, 2),
(4525, 'to', 1088, 744, 2),
(5008, 'to', 1088, 829, 2),
(4461, 'to', 1089, 733, 2),
(4462, 'to', 1090, 733, 2),
(4731, 'to', 1090, 780, 2),
(4737, 'to', 1090, 781, 2),
(4799, 'to', 1090, 792, 2),
(4916, 'to', 1090, 812, 2),
(4961, 'to', 1090, 820, 2),
(5003, 'to', 1090, 828, 2),
(5366, 'to', 1090, 890, 2),
(4501, 'to', 1091, 740, 2),
(4507, 'to', 1091, 741, 2),
(5121, 'to', 1091, 849, 2),
(5257, 'to', 1093, 872, 2),
(5287, 'to', 1093, 877, 2),
(4474, 'to', 1094, 735, 2),
(4578, 'to', 1094, 753, 2),
(4662, 'to', 1094, 768, 2),
(4767, 'to', 1094, 786, 2),
(5258, 'to', 1094, 872, 2),
(5288, 'to', 1094, 877, 2),
(4851, 'to', 1096, 801, 2),
(4922, 'to', 1096, 813, 2),
(4473, 'to', 1097, 735, 2),
(4513, 'to', 1098, 742, 2),
(4537, 'to', 1098, 746, 2),
(4603, 'to', 1098, 758, 2),
(4724, 'to', 1098, 779, 2),
(4760, 'to', 1098, 785, 2),
(5115, 'to', 1098, 848, 2),
(4514, 'to', 1099, 742, 2),
(4538, 'to', 1099, 746, 2),
(4566, 'to', 1099, 751, 2),
(4604, 'to', 1099, 758, 2),
(4725, 'to', 1099, 779, 2),
(4761, 'to', 1099, 785, 2),
(4928, 'to', 1099, 814, 2),
(5116, 'to', 1099, 848, 2),
(5157, 'to', 1099, 855, 2),
(5216, 'to', 1099, 865, 2),
(5222, 'to', 1099, 866, 2),
(4479, 'to', 1100, 736, 2),
(4996, 'to', 1100, 827, 2),
(5168, 'to', 1100, 857, 2),
(4480, 'to', 1101, 736, 2),
(4997, 'to', 1101, 827, 2),
(5128, 'to', 1101, 850, 2),
(5169, 'to', 1101, 857, 2),
(4485, 'to', 1103, 737, 2),
(4856, 'to', 1103, 802, 2),
(4599, 'to', 1104, 757, 2),
(4897, 'to', 1104, 809, 2),
(4495, 'to', 1105, 739, 2),
(5037, 'to', 1105, 834, 2),
(4496, 'to', 1106, 739, 2),
(4868, 'to', 1106, 804, 2),
(5038, 'to', 1106, 834, 2),
(4559, 'to', 1109, 750, 2),
(4560, 'to', 1110, 750, 2),
(4555, 'to', 1111, 749, 2),
(4782, 'to', 1111, 789, 2),
(4788, 'to', 1111, 790, 2),
(5020, 'to', 1111, 831, 2),
(4556, 'to', 1112, 749, 2),
(4783, 'to', 1112, 789, 2),
(4789, 'to', 1112, 790, 2),
(5021, 'to', 1112, 831, 2),
(4850, 'to', 1113, 801, 2),
(5156, 'to', 1114, 855, 2),
(4519, 'to', 1115, 743, 2),
(4531, 'to', 1116, 745, 2),
(4543, 'to', 1118, 747, 2),
(4754, 'to', 1119, 784, 2),
(4949, 'to', 1119, 818, 2),
(4955, 'to', 1119, 819, 2),
(4755, 'to', 1120, 784, 2),
(4950, 'to', 1120, 818, 2),
(4956, 'to', 1120, 819, 2),
(4625, 'to', 1121, 762, 2),
(4565, 'to', 1123, 751, 2),
(4927, 'to', 1123, 814, 2),
(4571, 'to', 1124, 752, 2),
(4992, 'to', 1124, 826, 2),
(5269, 'to', 1124, 874, 2),
(5275, 'to', 1124, 875, 2),
(4572, 'to', 1125, 752, 2),
(4616, 'to', 1125, 760, 2),
(4934, 'to', 1125, 815, 2),
(4993, 'to', 1125, 826, 2),
(5088, 'to', 1125, 843, 2),
(5104, 'to', 1125, 846, 2),
(5210, 'to', 1125, 864, 2),
(5264, 'to', 1125, 873, 2),
(5270, 'to', 1125, 874, 2),
(5276, 'to', 1125, 875, 2),
(5294, 'to', 1125, 878, 2),
(5300, 'to', 1125, 879, 2),
(4577, 'to', 1126, 753, 2),
(4661, 'to', 1126, 768, 2),
(4766, 'to', 1126, 786, 2),
(4582, 'to', 1127, 754, 2),
(4588, 'to', 1128, 755, 2),
(4594, 'to', 1128, 756, 2),
(5186, 'to', 1130, 860, 2),
(5145, 'to', 1131, 853, 2),
(5187, 'to', 1131, 860, 2),
(4609, 'to', 1132, 759, 2),
(4643, 'to', 1132, 765, 2),
(4655, 'to', 1132, 767, 2),
(5359, 'to', 1132, 889, 2),
(4615, 'to', 1134, 760, 2),
(4631, 'to', 1135, 763, 2),
(5311, 'to', 1135, 881, 2),
(5353, 'to', 1135, 888, 2),
(4637, 'to', 1137, 764, 2),
(4940, 'to', 1141, 816, 2),
(5282, 'to', 1141, 876, 2),
(4667, 'to', 1143, 769, 2),
(4673, 'to', 1143, 770, 2),
(4685, 'to', 1147, 772, 2),
(4686, 'to', 1148, 772, 2),
(4892, 'to', 1148, 808, 2),
(5044, 'to', 1148, 835, 2),
(4691, 'to', 1150, 773, 2),
(5053, 'to', 1150, 837, 2),
(4697, 'to', 1151, 774, 2),
(4718, 'to', 1152, 778, 2),
(5281, 'to', 1154, 876, 2),
(4730, 'to', 1155, 780, 2),
(4736, 'to', 1155, 781, 2),
(4742, 'to', 1156, 782, 2),
(5032, 'to', 1158, 833, 2),
(5181, 'to', 1158, 859, 2),
(4776, 'to', 1161, 788, 2),
(4832, 'to', 1161, 798, 2),
(4777, 'to', 1162, 788, 2),
(4833, 'to', 1162, 798, 2),
(4944, 'to', 1162, 817, 2),
(4966, 'to', 1162, 821, 2),
(5139, 'to', 1162, 852, 2),
(5193, 'to', 1162, 861, 2),
(5199, 'to', 1162, 862, 2),
(5342, 'to', 1162, 886, 2),
(4863, 'to', 1165, 803, 2),
(4875, 'to', 1165, 805, 2),
(4798, 'to', 1167, 792, 2),
(4808, 'to', 1169, 794, 2),
(4838, 'to', 1173, 799, 2),
(4862, 'to', 1177, 803, 2),
(4867, 'to', 1179, 804, 2),
(4873, 'to', 1181, 805, 2),
(4874, 'to', 1182, 805, 2),
(4885, 'to', 1184, 807, 2),
(5109, 'to', 1184, 847, 2),
(4891, 'to', 1186, 808, 2),
(4903, 'to', 1187, 810, 2),
(4909, 'to', 1188, 811, 2),
(4915, 'to', 1190, 812, 2),
(4960, 'to', 1190, 820, 2),
(5002, 'to', 1190, 828, 2),
(5365, 'to', 1190, 890, 2),
(4921, 'to', 1192, 813, 2),
(4933, 'to', 1194, 815, 2),
(4939, 'to', 1196, 816, 2),
(4943, 'to', 1197, 817, 2),
(4965, 'to', 1198, 821, 2),
(5192, 'to', 1198, 861, 2),
(5341, 'to', 1198, 886, 2),
(4981, 'to', 1200, 824, 2),
(4987, 'to', 1200, 825, 2),
(5031, 'to', 1204, 833, 2),
(5059, 'to', 1205, 838, 2),
(5043, 'to', 1206, 835, 2),
(5065, 'to', 1208, 839, 2),
(5082, 'to', 1216, 842, 2),
(5087, 'to', 1217, 843, 2),
(5103, 'to', 1219, 846, 2),
(5127, 'to', 1221, 850, 2),
(5138, 'to', 1222, 852, 2),
(5198, 'to', 1222, 862, 2),
(5144, 'to', 1223, 853, 2),
(5162, 'to', 1225, 856, 2),
(5180, 'to', 1226, 859, 2),
(5209, 'to', 1228, 864, 2),
(5215, 'to', 1229, 865, 2),
(5221, 'to', 1230, 866, 2),
(5227, 'to', 1231, 867, 2),
(5305, 'to', 1231, 880, 2),
(5317, 'to', 1231, 882, 2),
(5329, 'to', 1231, 884, 2),
(5239, 'to', 1235, 869, 2),
(5245, 'to', 1236, 870, 2),
(5251, 'to', 1237, 871, 2),
(5263, 'to', 1238, 873, 2),
(5299, 'to', 1238, 879, 2),
(5293, 'to', 1242, 878, 2),
(5335, 'to', 1244, 885, 2),
(5347, 'to', 1245, 887, 2);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Comments`
--

CREATE TABLE IF NOT EXISTS `Frontend_Comments` (
  `ID` int(10) unsigned NOT NULL,
  `text` varchar(1000) NOT NULL,
  `companyID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cargoID` int(10) unsigned DEFAULT NULL,
  `transportID` int(10) unsigned DEFAULT NULL,
  `rating` enum('positive','negative','neutral') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Companies`
--

CREATE TABLE IF NOT EXISTS `Frontend_Companies` (
  `ID` int(10) unsigned NOT NULL,
  `approvedByAdmin` enum('no','yes') NOT NULL DEFAULT 'no',
  `trustableMark` enum('no','yes') NOT NULL,
  `registered` datetime NOT NULL,
  `typeID` int(10) unsigned NOT NULL,
  `ipn` varchar(100) NOT NULL,
  `ownershipTypeID` int(11) NOT NULL,
  `primaryLocaleID` int(10) unsigned NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phoneStationary` varchar(45) DEFAULT NULL,
  `phoneCodeID` int(11) NOT NULL,
  `phoneStationaryCodeID` int(11) DEFAULT NULL,
  `foundationDate` date DEFAULT NULL,
  `parser` varchar(30) DEFAULT NULL,
  `url` varchar(200) NOT NULL,
  `rating` decimal(3,2) unsigned NOT NULL DEFAULT '1.00'
) ENGINE=InnoDB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Companies`
--

INSERT INTO `Frontend_Companies` (`ID`, `approvedByAdmin`, `trustableMark`, `registered`, `typeID`, `ipn`, `ownershipTypeID`, `primaryLocaleID`, `phone`, `phoneStationary`, `phoneCodeID`, `phoneStationaryCodeID`, `foundationDate`, `parser`, `url`, `rating`) VALUES
(536, 'yes', 'yes', '2011-08-12 00:00:00', 1, '2263100203', 5, 1, '(67)6401810', '', 4, 1, NULL, '14115127471', '', '1.00'),
(537, 'yes', 'yes', '2015-10-26 00:00:00', 3, '40058852', 9, 1, '(68)7295393', '(67)3289338', 4, 4, NULL, '15072598698', '', '1.00'),
(538, 'yes', 'yes', '2009-01-20 00:00:00', 1, '2255724032', 5, 1, '(99)9045807', '(93)5656336', 4, 4, NULL, '18103044756', '', '1.00'),
(539, 'yes', 'yes', '2005-04-19 00:00:00', 1, '37900552', 8, 1, '(56)3721472', '(562)339839', 4, 4, NULL, '12219685320', '', '1.00'),
(540, 'yes', 'yes', '2006-07-31 00:00:00', 1, '38088275', 8, 1, '(67)6401900', '(67)6410154', 4, 4, NULL, '18478122923', '', '1.00'),
(541, 'yes', 'yes', '2015-05-12 00:00:00', 1, '2628206175', 3, 1, '(68)3318813', '(95)4211401', 4, 4, NULL, '15423892357', '', '1.00'),
(542, 'yes', 'yes', '2009-02-18 00:00:00', 2, '2295513398', 7, 1, '(50)5387606', '(362)286455', 4, 4, NULL, '17817103279', '', '1.00'),
(543, 'yes', 'yes', '2006-03-06 00:00:00', 1, '', 6, 1, '(572)973808', '(50)3258035', 4, 4, NULL, '11626492454', '', '1.00'),
(544, 'yes', 'yes', '2004-09-17 00:00:00', 1, '37906140', 8, 1, '(44)4618933', '(97)2562450', 4, 4, NULL, '19851295578', '', '1.00'),
(545, 'yes', 'yes', '2010-03-26 00:00:00', 3, '3024220053', 7, 1, '(67)6132714', '(99)7177500', 4, 4, NULL, '15965667694', '', '1.00'),
(546, 'yes', 'yes', '2007-04-23 00:00:00', 2, '', 6, 1, '(67)6060066', '(67)6060066', 4, 4, NULL, '12667928577', '', '1.00'),
(547, 'yes', 'yes', '2012-12-06 00:00:00', 1, '39577834', 9, 1, '(67)4069883', '', 4, 1, NULL, '10465293267', '', '1.00'),
(548, 'yes', 'yes', '2013-10-15 00:00:00', 3, '2467021668', 7, 1, '(93)6592487', '(95)5971789', 4, 4, NULL, '18732910735', '', '1.00'),
(549, 'yes', 'yes', '2015-04-16 00:00:00', 1, '3176309723', 3, 1, '(50)2269033', '', 4, 1, NULL, '10827799163', '', '1.00'),
(550, 'yes', 'yes', '2014-11-21 00:00:00', 3, '2984619054', 3, 1, '(96)3896761', '(50)5901982', 4, 4, NULL, '14136383595', '', '1.00'),
(551, 'yes', 'yes', '2009-04-16 00:00:00', 3, '', 8, 1, '(44)3835735', '(63)3768231', 4, 4, NULL, '12007579293', '', '1.00'),
(552, 'yes', 'yes', '2010-09-06 00:00:00', 1, '3055209039', 10, 1, '(50)3081990', '(98)8321756', 4, 4, NULL, '16466834034', '', '1.00'),
(553, 'yes', 'yes', '2010-02-26 00:00:00', 1, '37499037', 8, 1, '(44)2291768', '(22)1006119', 4, 7, NULL, 'vsstrans', '', '1.00'),
(554, 'yes', 'yes', '2012-12-25 00:00:00', 3, '', 6, 1, '(50)4980035', '(67)6526529', 4, 4, NULL, '12641664291', '', '1.00'),
(555, 'yes', 'yes', '2016-02-02 00:00:00', 2, '40247341', 8, 1, '(093)0829846', '(095)3237784', 4, 4, NULL, '18195069554', '', '1.00'),
(556, 'yes', 'yes', '2015-06-12 00:00:00', 1, '39244489', 2, 1, '(67)8297007', '', 4, 1, NULL, '16598168086', '', '1.00'),
(557, 'yes', 'yes', '2016-02-07 00:00:00', 3, '2771804289', 7, 1, '(99)5446362', '(67)7029858', 4, 4, NULL, '11375487151', '', '1.00'),
(558, 'yes', 'yes', '2015-07-26 00:00:00', 2, '3134603587', 3, 1, '(98)8679476', '(63)2705164', 4, 4, NULL, '19079925079', '', '1.00'),
(559, 'yes', 'yes', '2009-10-20 00:00:00', 3, '', 8, 1, '(99)1760963', '', 4, 1, NULL, '19333856394', '', '1.00'),
(560, 'yes', 'yes', '2012-05-27 00:00:00', 3, '2216520955', 10, 1, '(66)0980462', '(96)5100175', 4, 4, NULL, '19787632639', '', '1.00'),
(561, 'yes', 'yes', '2011-09-15 00:00:00', 3, '', 6, 1, '(96)6664324', '', 4, 1, NULL, '18904068248', '', '1.00'),
(562, 'yes', 'yes', '2015-04-13 00:00:00', 2, '2526102321', 5, 1, '(66)4542840', '(98)6627754', 4, 4, NULL, '12511457691', '', '1.00'),
(563, 'yes', 'yes', '2007-06-04 00:00:00', 2, '32351161', 6, 1, '(56)2385434', '(56)2385450', 4, 4, NULL, '13920774262', '', '1.00'),
(564, 'yes', 'yes', '2009-02-26 00:00:00', 1, '37807598', 8, 1, '(56)7320968', '(56)7703113', 4, 4, NULL, '17655292941', '', '1.00'),
(565, 'yes', 'yes', '2003-03-26 00:00:00', 2, '33766181', 8, 1, '(44)2233144', '(63)7338115', 4, 4, NULL, '16344062150', '', '1.00'),
(566, 'yes', 'yes', '2015-06-03 00:00:00', 2, '39761812', 8, 1, '(99)2712327', '', 4, 1, NULL, '11300265366', '', '1.00'),
(567, 'yes', 'yes', '2015-03-04 00:00:00', 1, '3398201017', 5, 1, '(93)4529446', '', 4, 1, NULL, '18110110608', '', '1.00'),
(568, 'yes', 'yes', '2009-09-29 00:00:00', 1, '3071801331', 10, 1, '(067)3728699', '', 4, 1, NULL, '14673608091', '', '1.00'),
(569, 'yes', 'yes', '2007-02-15 00:00:00', 1, '22344384', 8, 1, '(32)2452222', '', 4, 1, NULL, '13088150332', '', '1.00'),
(570, 'yes', 'yes', '2007-12-04 00:00:00', 1, '', 6, 1, '(63)5812369', '(67)7560839', 4, 4, NULL, '12987497357', '', '1.00'),
(571, 'yes', 'yes', '2003-11-25 00:00:00', 1, '1944005618', 5, 1, '(67)7598379', '(99)2329622', 4, 4, NULL, '14595744565', '', '1.00'),
(572, 'yes', 'yes', '2012-04-25 00:00:00', 1, '2549709207', 7, 1, '(50)2801473', '(67)1475992', 4, 4, NULL, '19913205951', '', '1.00'),
(573, 'yes', 'yes', '2010-08-05 00:00:00', 1, '3127221378', 5, 1, '(98)7041741', '(99)0136551', 4, 4, NULL, '12481252144', '', '1.00'),
(574, 'yes', 'yes', '2010-06-24 00:00:00', 1, '', 6, 1, '(99)0187721', '(93)4925101', 4, 4, NULL, '19133452203', '', '1.00'),
(575, 'yes', 'yes', '2007-07-13 00:00:00', 1, '2075505915', 7, 1, '(44)2292882', '(50)3564957', 4, 4, NULL, '19123062989', '', '1.00'),
(577, 'yes', 'yes', '2006-03-30 00:00:00', 1, '', 9, 1, '(352)524418', '(67)3515396', 4, 4, NULL, '10751107121', '', '1.00'),
(578, 'yes', 'yes', '2015-02-19 00:00:00', 1, '2784618682', 3, 1, '(96)7913429', '', 4, 1, NULL, '17442233284', '', '1.00'),
(579, 'yes', 'yes', '2013-10-04 00:00:00', 1, '3435709336', 5, 1, '(66)4012041', '', 4, 1, NULL, 'zhabscky', '', '1.00'),
(580, 'yes', 'yes', '2007-09-05 00:00:00', 1, '25544907', 15, 1, '(32)2404540', '(32)2404541', 4, 4, NULL, '13111119758', '', '1.00'),
(581, 'yes', 'yes', '2007-11-20 00:00:00', 1, '3091107304', 5, 1, '(67)5724090', '', 4, 1, NULL, '14537154683', '', '1.00'),
(582, 'yes', 'yes', '2013-10-03 00:00:00', 3, '38903376', 8, 1, '(93)7532229', '(95)3743096', 4, 4, NULL, '16715608108', '', '1.00'),
(583, 'yes', 'yes', '2006-03-14 00:00:00', 1, '', 4, 1, '(67)4676507', '(63)6649641', 4, 4, NULL, '10180066691', '', '1.00'),
(584, 'yes', 'yes', '2014-05-19 00:00:00', 3, '3463506497', 7, 1, '(63)7382877', '(96)8410858', 4, 4, NULL, '15036313792', '', '1.00'),
(585, 'yes', 'yes', '2006-07-03 00:00:00', 1, '14328081', 8, 1, '(6264)60780', '', 4, 1, NULL, '14177818384', '', '1.00'),
(586, 'yes', 'yes', '2005-11-25 00:00:00', 2, '2729221483', 7, 1, '(0612)428669', '(50)4849902', 4, 4, NULL, '14863705106', '', '1.00'),
(587, 'yes', 'yes', '2007-07-27 00:00:00', 2, '', 6, 1, '(532)566550', '(50)3460916', 4, 4, NULL, '11078851773', '', '1.00'),
(588, 'yes', 'yes', '2014-07-30 00:00:00', 1, '39262419', 8, 1, '(67)4737686', '', 4, 1, NULL, '10901441703', '', '1.00'),
(589, 'yes', 'yes', '2011-05-04 00:00:00', 2, '35543450', 6, 1, '(066)9173760', '', 4, 1, NULL, '13845397656', '', '1.00'),
(590, 'yes', 'yes', '2015-04-20 00:00:00', 1, '2962816669', 5, 1, '(96)3188892', '(99)4024895', 4, 4, NULL, '12886309826', '', '1.00'),
(591, 'yes', 'yes', '2015-09-16 00:00:00', 2, '39834911', 8, 1, '(57)7136821', '(67)5378706', 4, 4, NULL, '18647577521', '', '1.00'),
(592, 'yes', 'yes', '2010-02-01 00:00:00', 1, '30588701', 27, 1, '(57)7154210', '(67)5780204', 4, 4, NULL, '14463534714', '', '1.00'),
(593, 'yes', 'yes', '2009-01-28 00:00:00', 2, '2632423986', 5, 1, '(542)655295', '(98)7490357', 4, 4, NULL, '13425887963', '', '1.00'),
(594, 'yes', 'yes', '2007-04-13 00:00:00', 2, '', 6, 1, '(5692)63187', '(67)9543904', 4, 4, NULL, '14580910205', '', '1.00'),
(595, 'yes', 'yes', '2009-06-18 00:00:00', 1, '', 28, 1, '(44)2234712', '(67)5002978', 4, 4, NULL, '11843524387', '', '1.00'),
(596, 'yes', 'yes', '2010-01-25 00:00:00', 2, '', 3, 1, '(67)2598095', '(95)0750717', 4, 4, NULL, '19377862229', '', '1.00'),
(597, 'yes', 'yes', '2007-12-04 00:00:00', 1, '', 6, 1, '(542)362156', '(50)8454247', 4, 4, NULL, '16470139609', '', '1.00'),
(598, 'yes', 'yes', '2011-05-27 00:00:00', 2, '2571910090', 10, 1, '(66)7803490', '(98)8095665', 4, 4, NULL, '16903931767', '', '1.00'),
(599, 'yes', 'yes', '2005-02-25 00:00:00', 2, '2237220019', 7, 1, '(552)381191', '(50)3964414', 4, 4, NULL, '16458901539', '', '1.00'),
(600, 'yes', 'yes', '2010-05-20 00:00:00', 1, '2766417099', 3, 1, '(95)4202202', '(50)3180747', 4, 4, NULL, '14227084703', '', '1.00'),
(601, 'yes', 'yes', '2009-08-19 00:00:00', 2, '2748400316', 7, 1, '(067)5620877', '', 4, 1, NULL, '18711222342', '', '1.00'),
(602, 'yes', 'yes', '2008-11-19 00:00:00', 1, '', 10, 1, '(50)5224720', '(97)7065976', 4, 4, NULL, '12143937601', '', '1.00'),
(603, 'yes', 'yes', '2013-10-21 00:00:00', 2, '2468207990', 7, 1, '(67)3931699', '', 4, 1, NULL, '15384071299', '', '1.00'),
(604, 'yes', 'yes', '2009-08-13 00:00:00', 3, '23106824', 8, 1, '(432)351738', '(63)6899318', 4, 4, NULL, '15236564029', '', '1.00'),
(605, 'yes', 'yes', '2005-09-05 00:00:00', 1, '30552979', 24, 1, '(44)4965568', '', 4, 1, NULL, '13591746077', '', '1.00'),
(606, 'yes', 'yes', '2007-01-18 00:00:00', 2, '', 6, 1, '(66)7555163', '(67)6912143', 4, 4, NULL, '13640749829', '', '1.00'),
(607, 'yes', 'yes', '2012-12-14 00:00:00', 1, '', 5, 1, '(96)5445777', '(99)2062553', 4, 4, NULL, '10441504446', '', '1.00'),
(608, 'yes', 'yes', '2009-08-07 00:00:00', 1, '22373196', 8, 1, '(98)5009219', '', 4, 1, NULL, '11408070643', '', '1.00'),
(609, 'yes', 'yes', '2009-08-07 00:00:00', 2, '14338636', 29, 1, '(6452)90386', '(6452)90388', 4, 4, NULL, '15053212382', '', '1.00'),
(610, 'yes', 'yes', '2009-08-11 00:00:00', 3, '', 5, 1, '(66)5049199', '(99)7332622', 4, 4, NULL, '13284564630', '', '1.00'),
(611, 'yes', 'yes', '2006-10-23 00:00:00', 1, '2373000230', 7, 1, '(352)284262', '(50)1022736', 4, 4, NULL, '15396376986', '', '1.00'),
(612, 'yes', 'yes', '2015-09-12 00:00:00', 2, '3212618642', 7, 1, '(66)6415106', '(63)4982759', 4, 4, NULL, '11017204054', '', '1.00'),
(613, 'yes', 'yes', '2008-01-11 00:00:00', 1, '3210308780', 5, 1, '(97)7029821', '(66)1883531', 4, 4, NULL, '16152231589', '', '1.00'),
(614, 'yes', 'yes', '2009-09-02 00:00:00', 1, '3030405434', 5, 1, '(57)7395169', '', 4, 1, NULL, '15573647496', '', '1.00'),
(615, 'yes', 'yes', '2015-11-03 00:00:00', 1, '40033707', 9, 1, '(98)7409742', '', 4, 1, NULL, '15381867702', '', '1.00'),
(616, 'yes', 'yes', '2013-09-15 00:00:00', 3, '2869321236', 3, 1, '(97)5076190', '(99)1552117', 4, 4, NULL, '19935299006', '', '1.00'),
(617, 'yes', 'yes', '2005-10-04 00:00:00', 1, '2999710392', 7, 1, '(57)2520032', '', 4, 1, NULL, 'polshin', '', '1.00'),
(618, 'yes', 'yes', '2008-02-15 00:00:00', 2, '2501512269', 7, 1, '(67)1599788', '(50)9565733', 4, 4, NULL, '14702276735', '', '1.00'),
(619, 'yes', 'yes', '2013-03-10 00:00:00', 1, '33575281', 8, 1, '(97)3161427', '(50)3216933', 4, 4, NULL, '10423171371', '', '1.00'),
(620, 'yes', 'yes', '2006-11-03 00:00:00', 2, '2708007016', 3, 1, '(67)3092424', '(67)4706414', 4, 4, NULL, '19136029533', '', '1.00'),
(621, 'yes', 'yes', '2005-04-01 00:00:00', 1, '37186682', 8, 1, '(50)3035974', '(96)9496727', 4, 4, NULL, '15961886997', '', '1.00'),
(622, 'yes', 'yes', '2014-02-27 00:00:00', 1, '33610760', 8, 1, '(66)4002182', '(67)1464801', 4, 4, NULL, '14865489614', '', '1.00'),
(623, 'yes', 'yes', '2007-02-21 00:00:00', 1, '2572414436', 5, 1, '(50)7671316', '(67)6282436', 4, 4, NULL, '15538706102', '', '1.00'),
(624, 'yes', 'yes', '2015-09-21 00:00:00', 2, '24883107', 6, 1, '(68)7168405', '', 4, 1, NULL, '16267916753', '', '1.00'),
(625, 'yes', 'yes', '2014-06-21 00:00:00', 1, '38301231', 8, 1, '(99)3773331', '(68)5223272', 4, 4, NULL, '17951594117', '', '1.00'),
(626, 'yes', 'yes', '2005-10-10 00:00:00', 2, '19292376', 8, 1, '(5136)27111', '(5136)24845', 4, 4, NULL, '12101202399', '', '1.00'),
(627, 'yes', 'yes', '2010-04-27 00:00:00', 1, '', 5, 1, '(57)7174918', '(50)3035048', 4, 4, NULL, '15787546232', '', '1.00'),
(628, 'yes', 'yes', '2012-10-31 00:00:00', 2, '31736045', 8, 1, '(50)5789482', '(56)4400881', 4, 4, NULL, '15478140947', '', '1.00'),
(629, 'yes', 'yes', '2014-02-24 00:00:00', 3, '39145784', 9, 1, '(50)1582734', '', 4, 1, NULL, '11278769373', '', '1.00'),
(630, 'no', 'no', '0000-00-00 00:00:00', 2, '', 2, 1, '0634300600', '', 16, 1, NULL, NULL, '', '1.00'),
(631, 'no', 'no', '0000-00-00 00:00:00', 2, '', 2, 1, '0934527685', '7687678687', 3, 2, NULL, NULL, '', '1.00'),
(632, 'no', 'no', '2016-04-21 14:46:00', 1, '', 1, 1, '5858585858585858', '', 14, 1, NULL, NULL, '', '1.00'),
(633, 'no', 'no', '2016-04-21 14:52:34', 1, '', 1, 1, '0503397530', '', 3, 1, NULL, NULL, '', '1.00'),
(634, 'no', 'no', '2016-04-21 14:57:36', 1, '', 1, 1, '020202020202', '', 14, 1, NULL, NULL, '', '1.00'),
(635, 'no', 'no', '2016-04-21 15:02:13', 1, '', 2, 1, '0503397530', '', 3, 1, NULL, NULL, '', '1.00'),
(636, 'no', 'no', '2016-04-21 15:15:00', 3, '', 1, 1, '0000000000000', '', 3, 1, NULL, NULL, '', '1.00'),
(637, 'no', 'no', '2016-04-21 16:49:07', 3, '', 2, 1, '0202020202020', '', 3, 1, NULL, NULL, '', '1.00'),
(638, 'no', 'no', '2016-04-21 16:54:36', 1, '', 1, 1, '673333333', '', 4, 1, NULL, NULL, '', '1.00'),
(639, 'no', 'no', '2016-04-21 16:57:54', 1, '', 1, 1, '673333333', '', 4, 1, NULL, NULL, '', '1.00'),
(640, 'no', 'no', '2016-04-21 16:59:25', 1, '', 1, 1, '673320398', '', 4, 1, NULL, NULL, '', '1.00');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Locales` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `realAddress` text,
  `companyID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1203 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_Locales`
--

INSERT INTO `Frontend_Company_Locales` (`ID`, `name`, `address`, `realAddress`, `companyID`, `localeID`, `info`) VALUES
(1006, 'ФЛ-П Брода Е.Н. (Підприємець)', '', '', 536, 1, ''),
(1007, 'ФЛ-П Брода Е.Н. (Предприниматель)', '', '', 536, 2, ''),
(1008, 'ТОВ "ДААР Логiстик Компанi" (Юридична особа)', '', 'Киевская 146', 537, 1, ''),
(1009, 'ТОВ "ДААР Логiстик Компанi" (Юридическое лицо)', '', 'Киевская 146', 537, 2, ''),
(1010, 'ФЛ-П Слобожан О.А. (Підприємець)', '', 'ул. Пушкинская 7,офис 6', 538, 1, 'Компания оказывает транспортно-експедиторские услуги по перевозке грузов. Мы обезпечиваем персонализированный сервис для каждого из наших клиентов и стремимся находить наилучшие решения для каждой конкретной перевозки,исходя из минимизации затрат и повышения качества оказываемых нами услуг. Мы приглашаем Вас к взаимовыгодному и плодотворному сотрудничеству. <br>'),
(1011, 'ФЛ-П Слобожан О.А. (Предприниматель)', '', 'ул. Пушкинская 7,офис 6', 538, 2, 'Компания оказывает транспортно-експедиторские услуги по перевозке грузов. Мы обезпечиваем персонализированный сервис для каждого из наших клиентов и стремимся находить наилучшие решения для каждой конкретной перевозки,исходя из минимизации затрат и повышения качества оказываемых нами услуг. Мы приглашаем Вас к взаимовыгодному и плодотворному сотрудничеству. <br>'),
(1012, 'ООО "ТРАНС КЛАСС ЛТД" (Юридична особа)', '', '', 539, 1, 'Компания ООО " ТРАНС КЛАСС  ЛТД " занимается  транспортно-экспедиционной деятельностью. У нас большой опыт работы с нестандартными, негабаритными, опасными (ADR) и особо ценными грузами. По желанию клиента предоставляем услуги страхования, брокера, таможенно-лицензионного склада, а так же полных логистических схем по доставке груза (от двери до двери). <br>География нашей деятельности состоит из работы: по Украине, со странами  Европы (Польша, Германия, Бельгия, Англия, Испания, Болгария, Италия, Швейцария, Финляндия и др.), страны СНГ  и Азии.<br>Компания начала свою деятельность в г. Днепропетровске в начале 2005г., (филиалы в г. Донецке, Харькове). На протяжении всего времени, наша компания зарекомендовала себя как надежный и стабильный партнер на рынке транспортно-экспедиционных услуг.<br>Мы всегда открыты для диалога с клиентом и будем рады, если для обслуживания Вашего бизнеса Вы воспользуетесь нашими услугами.<br>КОМПАНИЯ НА ПОСТОЯННУЮ РАБОТУ ПРИГЛАШАЕТ МЕНЕДЖЕРОВ - ЛОГИСТОВ (г. Днепропетровск, г. Харьков)<br>требования:<br>- опыт работы от 1 года;<br>- муж, жен от 25 лет;<br>- з/п до 40% от прибыли.<br>резюме отправлять по адресу:<br>'),
(1013, 'ООО "ТРАНС КЛАСС ЛТД" (Юридическое лицо)', '', '', 539, 2, 'Компания ООО " ТРАНС КЛАСС  ЛТД " занимается  транспортно-экспедиционной деятельностью. У нас большой опыт работы с нестандартными, негабаритными, опасными (ADR) и особо ценными грузами. По желанию клиента предоставляем услуги страхования, брокера, таможенно-лицензионного склада, а так же полных логистических схем по доставке груза (от двери до двери). <br>География нашей деятельности состоит из работы: по Украине, со странами  Европы (Польша, Германия, Бельгия, Англия, Испания, Болгария, Италия, Швейцария, Финляндия и др.), страны СНГ  и Азии.<br>Компания начала свою деятельность в г. Днепропетровске в начале 2005г., (филиалы в г. Донецке, Харькове). На протяжении всего времени, наша компания зарекомендовала себя как надежный и стабильный партнер на рынке транспортно-экспедиционных услуг.<br>Мы всегда открыты для диалога с клиентом и будем рады, если для обслуживания Вашего бизнеса Вы воспользуетесь нашими услугами.<br>КОМПАНИЯ НА ПОСТОЯННУЮ РАБОТУ ПРИГЛАШАЕТ МЕНЕДЖЕРОВ - ЛОГИСТОВ (г. Днепропетровск, г. Харьков)<br>требования:<br>- опыт работы от 1 года;<br>- муж, жен от 25 лет;<br>- з/п до 40% от прибыли.<br>резюме отправлять по адресу:<br>'),
(1014, 'ООО "ОТП плюс" (Юридична особа)', '', 'улица Бутырина, 25', 540, 1, '.<br>Вам необходимо перевезти груз из пункта А в пункт Б, разово или регулярно, в малых или больших объемах - приглашаем к сотрудничеству! Ваши грузовые автомобили простаивают в городе N в готовности осуществить доставку подходящего груза - будем рады вас видеть!<br>Основные преимущества сотрудничества с "ОТП плюс":<br>1. <br>. Ваша задача решается специалистами в своем деле с многолетним опытом.<br>2. <br>. За рыночную цену вы получаете качественную услугу: мы проконтролируем сделку на всех этапах, от "кузни" до документальной "упаковки".<br>2. <br>. Гражданская ответственность экспедитора ООО "ОТП плюс" по всем выполняемым рейсам застрахована.<br>'),
(1015, 'ООО "ОТП плюс" (Юридическое лицо)', '', 'улица Бутырина, 25', 540, 2, '.<br>Вам необходимо перевезти груз из пункта А в пункт Б, разово или регулярно, в малых или больших объемах - приглашаем к сотрудничеству! Ваши грузовые автомобили простаивают в городе N в готовности осуществить доставку подходящего груза - будем рады вас видеть!<br>Основные преимущества сотрудничества с "ОТП плюс":<br>1. <br>. Ваша задача решается специалистами в своем деле с многолетним опытом.<br>2. <br>. За рыночную цену вы получаете качественную услугу: мы проконтролируем сделку на всех этапах, от "кузни" до документальной "упаковки".<br>2. <br>. Гражданская ответственность экспедитора ООО "ОТП плюс" по всем выполняемым рейсам застрахована.<br>'),
(1016, 'ФОП Ребанов Игорь Евгеньевич (Підприємець)', '', '', 541, 1, ''),
(1017, 'ФОП Ребанов Игорь Евгеньевич (Предприниматель)', '', '', 541, 2, ''),
(1018, 'ФЛП Ситняк А.В.', '', 'ул.М.Вовчка 50/5', 542, 1, ''),
(1019, 'ФЛП Ситняк А.В.', '', 'ул.М.Вовчка 50/5', 542, 2, ''),
(1020, 'ЧП Фирма "ПЛАСТ"', '', '', 543, 1, ''),
(1021, 'ЧП Фирма "ПЛАСТ"', '', '', 543, 2, ''),
(1022, 'ООО "Транспортная компания"Имидж" (Юридична особа)', '', 'Бориспольская, 11 A', 544, 1, ''),
(1023, 'ООО "Транспортная компания"Имидж" (Юридическое лицо)', '', 'Бориспольская, 11 A', 544, 2, ''),
(1024, 'ФЛП Турубар Р.В. (Підприємець)', '', 'пгт Мирное ул.Парковая 10', 545, 1, ''),
(1025, 'ФЛП Турубар Р.В. (Предприниматель)', '', 'пгт Мирное ул.Парковая 10', 545, 2, ''),
(1026, 'ЧП Косяк О. А.', '', '', 546, 1, 'спд косяк о.а. перевозчик <br>'),
(1027, 'ЧП Косяк О. А.', '', '', 546, 2, 'спд косяк о.а. перевозчик <br>'),
(1028, 'ТОВ "М ЕНД С - ЗАХІД" (Юридична особа)', '', '', 547, 1, 'Компанія "М ЕНД С ФОРВАРДІНГ- ЗАХІД" надає транспортно експедиційні, митно-брокерські та консалтингові послуги.<br>  У 2015р. отримали статус "ЛІДЕР ГАЛУЗІ 2015"<br>Напрямки оптимізації:<br>-Транспортна логістика вантажних перевезень усіма видами транспорту (автомобільним, морським та залізничним), в тому числі консолідовані вантажі, в сфері міжнародних (експортно-імпортних) вантажних перевезень з\\в країни Західної та Східної Європи, СНД, Південно-Східної Азії<br>-Митна логістика та митно-брокерське обслуговування(наші брокерські відділи працюють на митних постах «Городок», «Малехів», «Запитів», «Луцьк», «Ужгород»<br>-Консалтинговий супровід та юридична підтримка клієнта від початку взаємодії і включно до завершення торговельно-економічної інформації та поставки вантажу одержувачу.<br>'),
(1029, 'ТОВ "М ЕНД С - ЗАХІД" (Юридическое лицо)', '', '', 547, 2, 'Компанія "М ЕНД С ФОРВАРДІНГ- ЗАХІД" надає транспортно експедиційні, митно-брокерські та консалтингові послуги.<br>  У 2015р. отримали статус "ЛІДЕР ГАЛУЗІ 2015"<br>Напрямки оптимізації:<br>-Транспортна логістика вантажних перевезень усіма видами транспорту (автомобільним, морським та залізничним), в тому числі консолідовані вантажі, в сфері міжнародних (експортно-імпортних) вантажних перевезень з\\в країни Західної та Східної Європи, СНД, Південно-Східної Азії<br>-Митна логістика та митно-брокерське обслуговування(наші брокерські відділи працюють на митних постах «Городок», «Малехів», «Запитів», «Луцьк», «Ужгород»<br>-Консалтинговий супровід та юридична підтримка клієнта від початку взаємодії і включно до завершення торговельно-економічної інформації та поставки вантажу одержувачу.<br>'),
(1030, 'ФЛП Сенюта Г.Б. (Юридична особа)', '', '', 548, 1, 'Работаем ежедневно с 08.00 до 22.00 .<br>Уважаемые клиенты и партнеры!<br>Мы заинтересованы в работе с постоянными и проверенными заказчиками/перевозчиками, всегда работаем качественно, оперативно, справедливо, с желанием завоевать свой авторитет!<br>Будем благодарны всем, кто оставит свой положительный отзыв на Ларди-Транс, после работы с нами.<br>Если у вас возникла конфликтная ситуация, есть предложение или вы заметили НЕ качественную работу наших сотрудников - просьба сообщить Администрации на e-mail:  <br>  (В письме обязательно указать: Имя сотр. /№тел., Ваше ЧП/Имя/№тел., подробное описание причины/ситуации/предложения.)<br>Индивидуальный подход к каждому !!!!<br>'),
(1031, 'ФЛП Сенюта Г.Б. (Юридическое лицо)', '', '', 548, 2, 'Работаем ежедневно с 08.00 до 22.00 .<br>Уважаемые клиенты и партнеры!<br>Мы заинтересованы в работе с постоянными и проверенными заказчиками/перевозчиками, всегда работаем качественно, оперативно, справедливо, с желанием завоевать свой авторитет!<br>Будем благодарны всем, кто оставит свой положительный отзыв на Ларди-Транс, после работы с нами.<br>Если у вас возникла конфликтная ситуация, есть предложение или вы заметили НЕ качественную работу наших сотрудников - просьба сообщить Администрации на e-mail:  <br>  (В письме обязательно указать: Имя сотр. /№тел., Ваше ЧП/Имя/№тел., подробное описание причины/ситуации/предложения.)<br>Индивидуальный подход к каждому !!!!<br>'),
(1032, 'ФОП Петренко Олена Вікторівна (Підприємець)', '', '', 549, 1, ''),
(1033, 'ФОП Петренко Олена Вікторівна (Предприниматель)', '', '', 549, 2, ''),
(1034, 'ФОП Линник С.А (Підприємець)', '', '', 550, 1, ''),
(1035, 'ФОП Линник С.А (Предприниматель)', '', '', 550, 2, ''),
(1036, 'ООО "Алтаир" (Юридична особа)', '', '', 551, 1, ''),
(1037, 'ООО "Алтаир" (Юридическое лицо)', '', '', 551, 2, ''),
(1038, 'ФО-П Шипша Р.И.', '', 'квартал 287, д 16,кв200', 552, 1, ''),
(1039, 'ФО-П Шипша Р.И.', '', 'квартал 287, д 16,кв200', 552, 2, ''),
(1040, 'ООО "Витал Спец Сервис" (Юридична особа)', '', 'пр. Отрадный 95г оф. 414', 553, 1, ''),
(1041, 'ООО "Витал Спец Сервис" (Юридическое лицо)', '', 'пр. Отрадный 95г оф. 414', 553, 2, ''),
(1042, 'ЧП Аква-Саббия', '', '', 554, 1, ''),
(1043, 'ЧП Аква-Саббия', '', '', 554, 2, ''),
(1044, 'ООО "Гарант Транс Плюс" (Юридична особа)', '', 'Украина, Киев', 555, 1, ''),
(1045, 'ООО "Гарант Транс Плюс" (Юридическое лицо)', '', 'Украина, Киев', 555, 2, ''),
(1046, 'ПП "ТД "МЕТАЛУРГ" (Юридична особа)', '', '', 556, 1, ''),
(1047, 'ПП "ТД "МЕТАЛУРГ" (Юридическое лицо)', '', '', 556, 2, ''),
(1048, 'ФЛП Малая Наталья Валериевна (Підприємець)', '', '', 557, 1, 'Оказываем услуги по организации автомобильных грузоперевозок.<br>Основным видом нашей деятельности являются информационные услуги:<br>- заказчикам транспорта мы помогаем быстро найти оптимальный вариант доставки;<br>- перевозчиков груза обеспечиваем информацией о заказах на транспорт;<br>- для организаций, имеющих свой транспорт, осуществляем подбор попутных грузов.<br>Цель нашей работы - упростить и ускорить поиск автотранспорта, обеспечить Вам максимально эффективный подход к организации перевозок при минимальных затратах.<br>'),
(1049, 'ФЛП Малая Наталья Валериевна (Предприниматель)', '', '', 557, 2, 'Оказываем услуги по организации автомобильных грузоперевозок.<br>Основным видом нашей деятельности являются информационные услуги:<br>- заказчикам транспорта мы помогаем быстро найти оптимальный вариант доставки;<br>- перевозчиков груза обеспечиваем информацией о заказах на транспорт;<br>- для организаций, имеющих свой транспорт, осуществляем подбор попутных грузов.<br>Цель нашей работы - упростить и ускорить поиск автотранспорта, обеспечить Вам максимально эффективный подход к организации перевозок при минимальных затратах.<br>'),
(1050, 'ФОП Cулименко М.В (Підприємець)', '', '', 558, 1, ''),
(1051, 'ФОП Cулименко М.В (Предприниматель)', '', '', 558, 2, ''),
(1052, 'ООО "РСФ "Ладо-Сервис" (Юридична особа)', '', 'ул.Фрунзе 18', 559, 1, 'Принцип, который мы исповедуем: «Каждый  должен заниматься своим делом! И подходить  к своей работе с максимальным профессионализмом!».  Выбор «своего дела» нами сделан давно – транспорт, и все что связано с этим понятием в современном мире . По образованию, опыту предыдущей работы и образу мыслей мы «транспортники». Нам всем уже чуть-чуть за тридцать – это время  зрелости, пора активных свершений. Мы готовы к ним!<br>'),
(1053, 'ООО "РСФ "Ладо-Сервис" (Юридическое лицо)', '', 'ул.Фрунзе 18', 559, 2, 'Принцип, который мы исповедуем: «Каждый  должен заниматься своим делом! И подходить  к своей работе с максимальным профессионализмом!».  Выбор «своего дела» нами сделан давно – транспорт, и все что связано с этим понятием в современном мире . По образованию, опыту предыдущей работы и образу мыслей мы «транспортники». Нам всем уже чуть-чуть за тридцать – это время  зрелости, пора активных свершений. Мы готовы к ним!<br>'),
(1054, 'ФО-П Карслиян О. К.', '', '', 560, 1, ''),
(1055, 'ФО-П Карслиян О. К.', '', '', 560, 2, ''),
(1056, 'ЧП Чмихун И.Н.', '', '', 561, 1, ''),
(1057, 'ЧП Чмихун И.Н.', '', '', 561, 2, ''),
(1058, 'ФЛ-П Лавренчук С.В. (Підприємець)', '', '', 562, 1, ''),
(1059, 'ФЛ-П Лавренчук С.В. (Предприниматель)', '', '', 562, 2, ''),
(1060, 'ЧП "Экспохим" (Юридична особа)', '', '', 563, 1, ''),
(1061, 'ЧП "Экспохим" (Юридическое лицо)', '', '', 563, 2, ''),
(1062, 'ООО "МИРОТРАНС" (Юридична особа)', '', 'http://www.mirotrans.com.ua', 564, 1, 'Компания «МИРОТРАНС» работает на рынке с 2009 года и охватывает разнообразные сферы рынка, связанного с грузовыми перевозками. С момента основания компания осуществляла перевозку грузов по территории Украины и странам СНГ автомобильным транспортом, но с каждым годом мы осваивали новые направления. На сегодняшний день  мы предлагаем:<br>Более 5-ти лет успешной работы в сфере грузоперевозок позволили накопить обширный опыт и наладить партнёрские отношения с компаниями, занятыми в разных сферах деятельности. Наши постоянные клиенты это крупнейшие компании Украины, Китая, Болгарии, Казахстана, России, Беларуси, Германии, Турции, Польши, и др.<br>Если Ваша компания имеет необходимость в транспорте, обращайтесь к нам. Мы поможем Вам в поиске подходящего транспорта для Вашего груза.<br>'),
(1063, 'ООО "МИРОТРАНС" (Юридическое лицо)', '', 'http://www.mirotrans.com.ua', 564, 2, 'Компания «МИРОТРАНС» работает на рынке с 2009 года и охватывает разнообразные сферы рынка, связанного с грузовыми перевозками. С момента основания компания осуществляла перевозку грузов по территории Украины и странам СНГ автомобильным транспортом, но с каждым годом мы осваивали новые направления. На сегодняшний день  мы предлагаем:<br>Более 5-ти лет успешной работы в сфере грузоперевозок позволили накопить обширный опыт и наладить партнёрские отношения с компаниями, занятыми в разных сферах деятельности. Наши постоянные клиенты это крупнейшие компании Украины, Китая, Болгарии, Казахстана, России, Беларуси, Германии, Турции, Польши, и др.<br>Если Ваша компания имеет необходимость в транспорте, обращайтесь к нам. Мы поможем Вам в поиске подходящего транспорта для Вашего груза.<br>'),
(1064, 'ООО "ЛТЭК-ТРАНС" (Юридична особа)', '', '', 565, 1, 'Воспользовавшись услугами компании «ЛТЭК-ТРАНС», вы можете рассчитывать на индивидуальный подход и разработку оптимального маршрута грузовых автомобильных перевозок с учетом персональных запросов и срочности доставки. Мы берем на себя не только непосредственно процесс транспортировки груза, но и все организационные моменты, связанные с оформление необходимого пакета документов. Мы очень ответственно подходим к вопросу сохранности груза, поэтому гарантируем аккуратность и бережность транспортировки.<br>-Транспортно-экспедиционные услуги: Украина, СНГ, АЗИЯ, Ближний Восток, Балканы;<br>-Организация перевозок негабаритных, большегрузных, сборных грузов, сельскохозяйственной и другой техники;<br>-TIR. CMR.ADR от 0,5 до 20т;<br>-Страхование ответственности, грузов, автотранспорта.<br>Viber: +380(63)7338115<br>'),
(1065, 'ООО "ЛТЭК-ТРАНС" (Юридическое лицо)', '', '', 565, 2, 'Воспользовавшись услугами компании «ЛТЭК-ТРАНС», вы можете рассчитывать на индивидуальный подход и разработку оптимального маршрута грузовых автомобильных перевозок с учетом персональных запросов и срочности доставки. Мы берем на себя не только непосредственно процесс транспортировки груза, но и все организационные моменты, связанные с оформление необходимого пакета документов. Мы очень ответственно подходим к вопросу сохранности груза, поэтому гарантируем аккуратность и бережность транспортировки.<br>-Транспортно-экспедиционные услуги: Украина, СНГ, АЗИЯ, Ближний Восток, Балканы;<br>-Организация перевозок негабаритных, большегрузных, сборных грузов, сельскохозяйственной и другой техники;<br>-TIR. CMR.ADR от 0,5 до 20т;<br>-Страхование ответственности, грузов, автотранспорта.<br>Viber: +380(63)7338115<br>'),
(1066, 'ООО "Экспресс Ком" (Юридична особа)', '', '', 566, 1, ''),
(1067, 'ООО "Экспресс Ком" (Юридическое лицо)', '', '', 566, 2, ''),
(1068, 'ФЛ-П Голубничий Алексей Андреевич (Підприємець)', '', '', 567, 1, ''),
(1069, 'ФЛ-П Голубничий Алексей Андреевич (Предприниматель)', '', '', 567, 2, ''),
(1070, 'ФО-П Мельник І. С.', '', '', 568, 1, ''),
(1071, 'ФО-П Мельник І. С.', '', '', 568, 2, ''),
(1072, 'ООО "Галмод" (Юридична особа)', '', 'вул. Рудненська, 8', 569, 1, ''),
(1073, 'ООО "Галмод" (Юридическое лицо)', '', 'вул. Рудненська, 8', 569, 2, ''),
(1074, 'ЧП Мельничук О. В.', '', '', 570, 1, 'автоперевозки<br>'),
(1075, 'ЧП Мельничук О. В.', '', '', 570, 2, 'автоперевозки<br>'),
(1076, 'ФЛ-П Грицина В.А. (Підприємець)', '', 'г. Сумы, ул. Ковпака, 4', 571, 1, ''),
(1077, 'ФЛ-П Грицина В.А. (Предприниматель)', '', 'г. Сумы, ул. Ковпака, 4', 571, 2, ''),
(1078, 'ФЛП Майлянс А.А.', '', '', 572, 1, 'Обратившись в диспетчерскую компанию ЧП Майлянс А.А. <br>Вы всегда будете свободны от проблем и беспокойства и уверены в качественной, своевременной доставке Вашего груза. Спокойны за судьбу своего груза на каждом участке пути. Уверены в нашей максимальной ответственности и внимательности к Вам и Вашему заказу на всех стадиях его выполнения. <br>Уверены, что наши расценки полностью соответствуют реальному качеству наших услуг <br> Основная задача нашей Компании заключается в предоставлении  индивидуального пакета услуг для грузоотправителя и грузоперевозчика.<br>Достаточный опыт позволяет нам осуществлять доставку любых грузов, в том числе и сборных грузов, в любых направлениях автомобильным транспортом. Комплексный подход позволяет Компании осуществлять перевозку грузов, а так же индивидуальный подход и полная информация для перевозчика. Компания использует свои ресурсы с максимальной эффективностью.<br>Мы открыты для конструктивного диалога с каждым из наших существующих и потенциальных Клиентов.<br>Достижение и сохранение ведущих позиций на рынке диспетчерской  деятельности.<br>Поддержание репутации Компании.<br>'),
(1079, 'ФЛП Майлянс А.А.', '', '', 572, 2, 'Обратившись в диспетчерскую компанию ЧП Майлянс А.А. <br>Вы всегда будете свободны от проблем и беспокойства и уверены в качественной, своевременной доставке Вашего груза. Спокойны за судьбу своего груза на каждом участке пути. Уверены в нашей максимальной ответственности и внимательности к Вам и Вашему заказу на всех стадиях его выполнения. <br>Уверены, что наши расценки полностью соответствуют реальному качеству наших услуг <br> Основная задача нашей Компании заключается в предоставлении  индивидуального пакета услуг для грузоотправителя и грузоперевозчика.<br>Достаточный опыт позволяет нам осуществлять доставку любых грузов, в том числе и сборных грузов, в любых направлениях автомобильным транспортом. Комплексный подход позволяет Компании осуществлять перевозку грузов, а так же индивидуальный подход и полная информация для перевозчика. Компания использует свои ресурсы с максимальной эффективностью.<br>Мы открыты для конструктивного диалога с каждым из наших существующих и потенциальных Клиентов.<br>Достижение и сохранение ведущих позиций на рынке диспетчерской  деятельности.<br>Поддержание репутации Компании.<br>'),
(1080, 'ФЛ-П Пидкивка И.М. (Підприємець)', '', '', 573, 1, ''),
(1081, 'ФЛ-П Пидкивка И.М. (Предприниматель)', '', '', 573, 2, ''),
(1082, 'ЧП "Восток"', '', 'набережная 109', 574, 1, ''),
(1083, 'ЧП "Восток"', '', 'набережная 109', 574, 2, ''),
(1084, 'ФЛП Ларичев В. Ю. (Підприємець)', '', '', 575, 1, ''),
(1085, 'ФЛП Ларичев В. Ю. (Предприниматель)', '', '', 575, 2, ''),
(1086, 'ТОВ "Ватра-Світлоприлад"', '', '', 577, 1, ' Тов" Ватра-Світлоприлад"<br>'),
(1087, 'ТОВ "Ватра-Світлоприлад"', '', '', 577, 2, ' Тов" Ватра-Світлоприлад"<br>'),
(1088, 'ФОП Чепель Евгения Николаевна (Підприємець)', '', '', 578, 1, 'Вспомогательное обслуживание наземного транспорта.<br>'),
(1089, 'ФОП Чепель Евгения Николаевна (Предприниматель)', '', '', 578, 2, 'Вспомогательное обслуживание наземного транспорта.<br>'),
(1090, 'ФЛ-П Жабский В.Ю. (Юридична особа)', '', 'ул. Космическая, 21', 579, 1, ''),
(1091, 'ФЛ-П Жабский В.Ю. (Юридическое лицо)', '', 'ул. Космическая, 21', 579, 2, ''),
(1092, 'ПТЕП "Автотрансекспедиція" (Юридична особа)', '', 'вул.Городоцька 291', 580, 1, 'Що заощаджувати, щоб бути швидшим?<br>Час чи гроші?<br>І час, і гроші. Також берегти своє здоров’я та нерви,<br>І мати чистий спокій з логістикою автоперевезень<br>ЗАВЖДИ<br>У це складно повірити, проте реальний факт переконливо підтверджує наші слова:<br>З 1998 року компанія «АВТОТРАНСЕКСПЕДИЦІЯ» <br>вчасно, надійно, відповідально <br>забезпечує логістику автоперевезень, знаходячи оптимальну ціну<br>І наші постійні партнери<br>«Кока-кола Беверидж Україна», «Перша приватна броварня», «Львівський Жиркомбінат», «Росан-Трейд», ВАТ «Іскра», «Екран-віконні конструкції», концерн «Електрон», компанія «Ніко-Захід», «Львіввікнопласт», <br>«Галпідшипник», «Азов-Галичина»<br>ЗНАЮТЬ<br>«АВТОТРАНСЕКСПЕДИЦІЯ» компанія з перевіреною репутацією<br>Телефонуйте (032) 240-45-40<br>Вантажівки є по всій Україні, СНД та Європі:<br>саме там, де потрібно<br>саме стільки, скільки потрібно<br>І доїдуть вони саме туди, куди потрібно Вам<br>Заощаджуйте свій час і гроші, будьте швидшими за інших<br>«АВТОТРАНСЕКСПЕДИЦІЯ»<br>завжди на крок швидше<br>'),
(1093, 'ПТЕП "Автотрансекспедиція" (Юридическое лицо)', '', 'вул.Городоцька 291', 580, 2, 'Що заощаджувати, щоб бути швидшим?<br>Час чи гроші?<br>І час, і гроші. Також берегти своє здоров’я та нерви,<br>І мати чистий спокій з логістикою автоперевезень<br>ЗАВЖДИ<br>У це складно повірити, проте реальний факт переконливо підтверджує наші слова:<br>З 1998 року компанія «АВТОТРАНСЕКСПЕДИЦІЯ» <br>вчасно, надійно, відповідально <br>забезпечує логістику автоперевезень, знаходячи оптимальну ціну<br>І наші постійні партнери<br>«Кока-кола Беверидж Україна», «Перша приватна броварня», «Львівський Жиркомбінат», «Росан-Трейд», ВАТ «Іскра», «Екран-віконні конструкції», концерн «Електрон», компанія «Ніко-Захід», «Львіввікнопласт», <br>«Галпідшипник», «Азов-Галичина»<br>ЗНАЮТЬ<br>«АВТОТРАНСЕКСПЕДИЦІЯ» компанія з перевіреною репутацією<br>Телефонуйте (032) 240-45-40<br>Вантажівки є по всій Україні, СНД та Європі:<br>саме там, де потрібно<br>саме стільки, скільки потрібно<br>І доїдуть вони саме туди, куди потрібно Вам<br>Заощаджуйте свій час і гроші, будьте швидшими за інших<br>«АВТОТРАНСЕКСПЕДИЦІЯ»<br>завжди на крок швидше<br>'),
(1094, 'ФЛ-П Лиходей О. Н. (Підприємець)', '', '', 581, 1, ''),
(1095, 'ФЛ-П Лиходей О. Н. (Предприниматель)', '', '', 581, 2, ''),
(1096, 'ООО "Матеріор" (Юридична особа)', '', 'Буськ', 582, 1, 'Транспортна логістика вантажного автотранспорту. Закупка та продаж деревини. Власне виробництво (доска,дрова).Надаєм послуги з сушки дров. Потребуєм автотранспорт по напрямкам Україна-Литва,Україна-Німеччина.<br>'),
(1097, 'ООО "Матеріор" (Юридическое лицо)', '', 'Буськ', 582, 2, 'Транспортна логістика вантажного автотранспорту. Закупка та продаж деревини. Власне виробництво (доска,дрова).Надаєм послуги з сушки дров. Потребуєм автотранспорт по напрямкам Україна-Литва,Україна-Німеччина.<br>'),
(1098, 'СПД Коваль М. П.', '', '', 583, 1, ''),
(1099, 'СПД Коваль М. П.', '', '', 583, 2, ''),
(1100, 'ФЛП Кривоносов Владислав Дмитриевич (Підприємець)', '', '', 584, 1, ''),
(1101, 'ФЛП Кривоносов Владислав Дмитриевич (Предприниматель)', '', '', 584, 2, ''),
(1102, 'ООО "Краматорская транспортная компания" (Юридична особа)', '', '', 585, 1, ''),
(1103, 'ООО "Краматорская транспортная компания" (Юридическое лицо)', '', '', 585, 2, ''),
(1104, 'ФЛП  Донец Н. В.', '', 'Хортицкой р-он', 586, 1, ''),
(1105, 'ФЛП  Донец Н. В.', '', 'Хортицкой р-он', 586, 2, ''),
(1106, 'ЧП Манюк И. С.', '', '', 587, 1, ''),
(1107, 'ЧП Манюк И. С.', '', '', 587, 2, ''),
(1108, 'ООО "МАСС ТРАНС" (Юридична особа)', '', '', 588, 1, ''),
(1109, 'ООО "МАСС ТРАНС" (Юридическое лицо)', '', '', 588, 2, ''),
(1110, 'ЧП "Авива-Транс"', '', '', 589, 1, ''),
(1111, 'ЧП "Авива-Транс"', '', '', 589, 2, ''),
(1112, 'ФЛ-П Буланова Надежда Игоревна (Підприємець)', '', '', 590, 1, ''),
(1113, 'ФЛ-П Буланова Надежда Игоревна (Предприниматель)', '', '', 590, 2, ''),
(1114, 'ООО "ХАЙВЕЙ" (Юридична особа)', '', '', 591, 1, ''),
(1115, 'ООО "ХАЙВЕЙ" (Юридическое лицо)', '', '', 591, 2, ''),
(1116, 'ЧФ "ХарьковАгрозапчасть" (Юридична особа)', '', 'haz99@mail.ru', 592, 1, 'ООО "Харьковагро" 057 7154210. Грузоперевозки, Экспедирование, продажа запчастей, грузовой и с/х техники<br>'),
(1117, 'ЧФ "ХарьковАгрозапчасть" (Юридическое лицо)', '', 'haz99@mail.ru', 592, 2, 'ООО "Харьковагро" 057 7154210. Грузоперевозки, Экспедирование, продажа запчастей, грузовой и с/х техники<br>'),
(1118, 'ФЛ-П Павлова С. И. (Підприємець)', '', 'ул.Шишкаревская.86', 593, 1, ''),
(1119, 'ФЛ-П Павлова С. И. (Предприниматель)', '', 'ул.Шишкаревская.86', 593, 2, ''),
(1120, 'ЧП Григорьева Е. А.', '', '', 594, 1, 'Автоперевозки.Экспедиции<br>'),
(1121, 'ЧП Григорьева Е. А.', '', '', 594, 2, 'Автоперевозки.Экспедиции<br>'),
(1122, 'АСОЦІАЦІЯ ВАНТАЖНИХ ПЕРЕВІЗНИКІВ КИЄВА', '', '', 595, 1, ''),
(1123, 'АСОЦІАЦІЯ ВАНТАЖНИХ ПЕРЕВІЗНИКІВ КИЄВА', '', '', 595, 2, ''),
(1124, 'ФОП Сільник І. С.', '', '', 596, 1, ''),
(1125, 'ФОП Сільник І. С.', '', '', 596, 2, ''),
(1126, 'ЧП Бровко Е. С.', '', '', 597, 1, ''),
(1127, 'ЧП Бровко Е. С.', '', '', 597, 2, ''),
(1128, 'ФО-П Манько А. В. (Підприємець)', '', 'Васильковский район', 598, 1, ''),
(1129, 'ФО-П Манько А. В. (Предприниматель)', '', 'Васильковский район', 598, 2, ''),
(1130, 'ФЛП Польской В. А. (Підприємець)', '', '', 599, 1, 'Грузоперевозки по Украине.<br>'),
(1131, 'ФЛП Польской В. А. (Предприниматель)', '', '', 599, 2, 'Грузоперевозки по Украине.<br>'),
(1132, 'ФОП Шевченко С.Г.', '', '', 600, 1, 'Наша транспортная компания оказывает услуги грузоперевозок с 2005 года. Работая более 9-ти лет, мы зарекомендовали себя как качественный, надежный и ответственный перевозчик грузов с температурным режимом и без.<br>Наш автопарк – 15 автопоездов-рефрижераторов (EURO III). Все авто в отличном техническом состоянии. Опытные и ответственные водители отлично понимают специфику работы. Полуприцепы чистые, сухие. Имеется оснастка для дополнительного крепления и фиксации груза (стойки, ремни).<br>Всегда открыты к взаимовыгодному сотрудничеству.<br>'),
(1133, 'ФОП Шевченко С.Г.', '', '', 600, 2, 'Наша транспортная компания оказывает услуги грузоперевозок с 2005 года. Работая более 9-ти лет, мы зарекомендовали себя как качественный, надежный и ответственный перевозчик грузов с температурным режимом и без.<br>Наш автопарк – 15 автопоездов-рефрижераторов (EURO III). Все авто в отличном техническом состоянии. Опытные и ответственные водители отлично понимают специфику работы. Полуприцепы чистые, сухие. Имеется оснастка для дополнительного крепления и фиксации груза (стойки, ремни).<br>Всегда открыты к взаимовыгодному сотрудничеству.<br>'),
(1134, 'ФЛП Дубовик А.М.', '', '', 601, 1, 'Автоперевозки<br>'),
(1135, 'ФЛП Дубовик А.М.', '', '', 601, 2, 'Автоперевозки<br>'),
(1136, 'ФО-П Падусенко Ю.П.', '', 'кобища 8', 602, 1, 'Мы предлагаем широкий спектр услуг в области грузоперевозок. Выполняем перевозки автотранспортом различной грузоподьемности "от 1 до 23 тон" и кубатуры "от 10 до 110". <br>Привлекаем перевозкам только транспорт имеющий СМР страхование или ТИР. Грузоперевозки осуществлябтся в максимально зжатые сроки. Ваш груз мы доставим в сохранности.<br>'),
(1137, 'ФО-П Падусенко Ю.П.', '', 'кобища 8', 602, 2, 'Мы предлагаем широкий спектр услуг в области грузоперевозок. Выполняем перевозки автотранспортом различной грузоподьемности "от 1 до 23 тон" и кубатуры "от 10 до 110". <br>Привлекаем перевозкам только транспорт имеющий СМР страхование или ТИР. Грузоперевозки осуществлябтся в максимально зжатые сроки. Ваш груз мы доставим в сохранности.<br>'),
(1138, 'ФЛП Скорохода Р.М. (Юридична особа)', '', '', 603, 1, ''),
(1139, 'ФЛП Скорохода Р.М. (Юридическое лицо)', '', '', 603, 2, ''),
(1140, 'ООО "Грандис" (Юридична особа)', '', 'ул. Максимовича, 25а', 604, 1, ''),
(1141, 'ООО "Грандис" (Юридическое лицо)', '', 'ул. Максимовича, 25а', 604, 2, ''),
(1142, 'ДП "ЛИС" ООО "ЛЕГИОН" (Юридична особа)', '', 'пр-т Воссоединения, 7-А,оф.528', 605, 1, 'Уважаемые партнёры!<br>Просим Вас принимать заявки от нашей компании только с указанием контактов зарегистрированных на данном сайте или указанных на официальном сайте компании ДП "ЛИС" ООО "Легион".<br>'),
(1143, 'ДП "ЛИС" ООО "ЛЕГИОН" (Юридическое лицо)', '', 'пр-т Воссоединения, 7-А,оф.528', 605, 2, 'Уважаемые партнёры!<br>Просим Вас принимать заявки от нашей компании только с указанием контактов зарегистрированных на данном сайте или указанных на официальном сайте компании ДП "ЛИС" ООО "Легион".<br>'),
(1144, 'ЧП Хлоров А. С. (Підприємець)', '', '', 606, 1, ''),
(1145, 'ЧП Хлоров А. С. (Предприниматель)', '', '', 606, 2, ''),
(1146, 'ФЛ-П Новиков В.Н. (Підприємець)', '', '', 607, 1, 'Экспедиторские услуги, помощь в поиске транспорта для осуществления перевозки груза.<br>'),
(1147, 'ФЛ-П Новиков В.Н. (Предприниматель)', '', '', 607, 2, 'Экспедиторские услуги, помощь в поиске транспорта для осуществления перевозки груза.<br>'),
(1148, 'ООО "Зевс" (Юридична особа)', '', '', 608, 1, ''),
(1149, 'ООО "Зевс" (Юридическое лицо)', '', '', 608, 2, ''),
(1150, 'ПКФ ООО "ТАНА"', '', '', 609, 1, 'Грузоотправители, грузополучатели, попутные грузоперевозки собственным автомобильным транспортом<br>'),
(1151, 'ПКФ ООО "ТАНА"', '', '', 609, 2, 'Грузоотправители, грузополучатели, попутные грузоперевозки собственным автомобильным транспортом<br>'),
(1152, 'ФЛ-П Коломиец И. С. (Підприємець)', '', 'ул. 1-я Заводская, 1', 610, 1, 'ЧП Коломиец. Быстро и надежно организуем автотранспортную перевозку на территории Украины. Предоставим необходимый автомобиль в нужное время и в нужное место. Подищем подходящий груз, догруз.Надеемся на плодотворное сотрудничество!!!<br>'),
(1153, 'ФЛ-П Коломиец И. С. (Предприниматель)', '', 'ул. 1-я Заводская, 1', 610, 2, 'ЧП Коломиец. Быстро и надежно организуем автотранспортную перевозку на территории Украины. Предоставим необходимый автомобиль в нужное время и в нужное место. Подищем подходящий груз, догруз.Надеемся на плодотворное сотрудничество!!!<br>'),
(1154, 'ФЛП Сломушинский И. В.', '', '', 611, 1, ''),
(1155, 'ФЛП Сломушинский И. В.', '', '', 611, 2, ''),
(1156, 'ФЛП Прибылова К.О. (Підприємець)', '', '', 612, 1, ''),
(1157, 'ФЛП Прибылова К.О. (Предприниматель)', '', '', 612, 2, ''),
(1158, 'ФЛ-П Харченко Ю. Д. (Підприємець)', '', '', 613, 1, ''),
(1159, 'ФЛ-П Харченко Ю. Д. (Предприниматель)', '', '', 613, 2, ''),
(1160, 'ФЛ-П Яманко Г.В. (Підприємець)', '', 'Харьков, улица Киргизская,19', 614, 1, ''),
(1161, 'ФЛ-П Яманко Г.В. (Предприниматель)', '', 'Харьков, улица Киргизская,19', 614, 2, ''),
(1162, 'ТОВ "ТрансАвто+" (Юридична особа)', '', '', 615, 1, 'Мы серьезная экспедиторская компания, работаем оператором таких грузоотправителей: ИП Кока-Кола, ЧАО ИДС, ЧАО Карлберг, ООО Олипм, ЧАО Оболонь. Всегда открыты к сотрудничеству к новым надежным перевозчикам и заказчикам, со своей стороны гарантируем порядочность, оперативное решение возникающих вопросов во время перевозки и своевременную оплату. Не смотря на то, что как компания мы появились на рынке совсем недавно, наши сотрудники настоящие профессионалы и уже не первый год на рынке. Наша цель стать крупнейшей экспедицией по перевозкам внутри Украины, поэтому репутация для нас превыше всего.<br>Больше о нас тут <br>'),
(1163, 'ТОВ "ТрансАвто+" (Юридическое лицо)', '', '', 615, 2, 'Мы серьезная экспедиторская компания, работаем оператором таких грузоотправителей: ИП Кока-Кола, ЧАО ИДС, ЧАО Карлберг, ООО Олипм, ЧАО Оболонь. Всегда открыты к сотрудничеству к новым надежным перевозчикам и заказчикам, со своей стороны гарантируем порядочность, оперативное решение возникающих вопросов во время перевозки и своевременную оплату. Не смотря на то, что как компания мы появились на рынке совсем недавно, наши сотрудники настоящие профессионалы и уже не первый год на рынке. Наша цель стать крупнейшей экспедицией по перевозкам внутри Украины, поэтому репутация для нас превыше всего.<br>Больше о нас тут <br>'),
(1164, 'ФОП КРИВОШЕЯ В.С. (Юридична особа)', '', 'Леваневського 30 кв,283', 616, 1, ''),
(1165, 'ФОП КРИВОШЕЯ В.С. (Юридическое лицо)', '', 'Леваневського 30 кв,283', 616, 2, ''),
(1166, 'ФЛП Польшин А. А. (Підприємець)', '', 'ул.Зерновая, 2 оф.209', 617, 1, ''),
(1167, 'ФЛП Польшин А. А. (Предприниматель)', '', 'ул.Зерновая, 2 оф.209', 617, 2, ''),
(1168, 'ФЛП Чуева Л. В. (Підприємець)', '', '', 618, 1, ''),
(1169, 'ФЛП Чуева Л. В. (Предприниматель)', '', '', 618, 2, ''),
(1170, 'ООО "Торговый дом Кривбасс Ойл" (Юридична особа)', '', 'ул. Октябрьская 18', 619, 1, ''),
(1171, 'ООО "Торговый дом Кривбасс Ойл" (Юридическое лицо)', '', 'ул. Октябрьская 18', 619, 2, ''),
(1172, 'ФОП Довгаленко В. I.', '', '', 620, 1, 'Перевозки транспортом,оборудованным гидробортом.Паллетная доставка грузов,доставка груза в составе сборного.Перевозки по Украине под таможенным контролем,перевозки в температурном режиме,перевозки опасных грузов АDR класс 1,3,4,6,8,9.Собственный парк автомобилей г/п 10-15тн.Постоянно транспорт в Киеве,Днепропетровске,Кременчуге,Кировограде,Черкассах.<br>'),
(1173, 'ФОП Довгаленко В. I.', '', '', 620, 2, 'Перевозки транспортом,оборудованным гидробортом.Паллетная доставка грузов,доставка груза в составе сборного.Перевозки по Украине под таможенным контролем,перевозки в температурном режиме,перевозки опасных грузов АDR класс 1,3,4,6,8,9.Собственный парк автомобилей г/п 10-15тн.Постоянно транспорт в Киеве,Днепропетровске,Кременчуге,Кировограде,Черкассах.<br>'),
(1174, 'ООО "Грин Ман Компани ГМК" (Юридична особа)', '', 'Черепина 29', 621, 1, ''),
(1175, 'ООО "Грин Ман Компани ГМК" (Юридическое лицо)', '', 'Черепина 29', 621, 2, ''),
(1176, 'ООО "НПК ЗАПОРОЖАВТОБЫТХИМ" (Юридична особа)', '', 'ул. 8 Марта 190', 622, 1, 'Украинское предприятие ООО "НПК "Запорожавтобытхим" является крупным производителем химической продукции в Украине. На сегодняшний день мы предлагаем: лаки, краски, растворители и автохимию в широком ассортименте.<br>Основными направлениями и приоритетами деятельности предприятия ООО "НПК "Запорожавтобытхим" являются: <br>- всестороннее удовлетворение потребностей заказчика в химической продукции и сырье, включая доставку в сжатые сроки, учитывая все пожелания относительно химического состава и качества поcтавляемой продукции; <br>- гибкий график оплаты поставляемой химической продукции и разумная ценовая политика предприятия; <br>- удобные условия поставки химической продукции в любую точку не только Украины, но и за рубеж, благодаря наличию собственного транспорта, грамотно организованной работе отделов логистики и внешнеэкономической деятельности.<br>Продукция  ООО "НПК "Запорожавтобытхим"выпускаеться  на современном производстве из высококачественного сырья по ГОСТ и ТУ. Наше предприятие положительно зарекомендовало себя как надежного партнера на региональном и Украинском рынке, за счет современной организации производства и выпуска сертифицированной продукции.<br>'),
(1177, 'ООО "НПК ЗАПОРОЖАВТОБЫТХИМ" (Юридическое лицо)', '', 'ул. 8 Марта 190', 622, 2, 'Украинское предприятие ООО "НПК "Запорожавтобытхим" является крупным производителем химической продукции в Украине. На сегодняшний день мы предлагаем: лаки, краски, растворители и автохимию в широком ассортименте.<br>Основными направлениями и приоритетами деятельности предприятия ООО "НПК "Запорожавтобытхим" являются: <br>- всестороннее удовлетворение потребностей заказчика в химической продукции и сырье, включая доставку в сжатые сроки, учитывая все пожелания относительно химического состава и качества поcтавляемой продукции; <br>- гибкий график оплаты поставляемой химической продукции и разумная ценовая политика предприятия; <br>- удобные условия поставки химической продукции в любую точку не только Украины, но и за рубеж, благодаря наличию собственного транспорта, грамотно организованной работе отделов логистики и внешнеэкономической деятельности.<br>Продукция  ООО "НПК "Запорожавтобытхим"выпускаеться  на современном производстве из высококачественного сырья по ГОСТ и ТУ. Наше предприятие положительно зарекомендовало себя как надежного партнера на региональном и Украинском рынке, за счет современной организации производства и выпуска сертифицированной продукции.<br>'),
(1178, 'ФЛ-П Драчук К.А. (Підприємець)', '', '', 623, 1, 'оказывает услуги по организации автомобильных грузоперевозок. Готовы предложить любой вид транспорта (тенты, цельнометы, рефрижераторы, самосвалы, зерновозы) для транспортировки в пределах Украины и стран СНГ. Возможна перевозка грузов автотранспортом от 1 до 30тн и от 3 до 120м3, а также сборных грузов.<br>Умеренные тарифы,большой выбор автомашин,выгодные договорные условия,любые виды оплат.<br>Цель нашей работы - поиск автотранспорта, обеспечение перевозок при минимальных затратах.<br>'),
(1179, 'ФЛ-П Драчук К.А. (Предприниматель)', '', '', 623, 2, 'оказывает услуги по организации автомобильных грузоперевозок. Готовы предложить любой вид транспорта (тенты, цельнометы, рефрижераторы, самосвалы, зерновозы) для транспортировки в пределах Украины и стран СНГ. Возможна перевозка грузов автотранспортом от 1 до 30тн и от 3 до 120м3, а также сборных грузов.<br>Умеренные тарифы,большой выбор автомашин,выгодные договорные условия,любые виды оплат.<br>Цель нашей работы - поиск автотранспорта, обеспечение перевозок при минимальных затратах.<br>'),
(1180, 'ЧП "Стохид" (Юридична особа)', '', '', 624, 1, ''),
(1181, 'ЧП "Стохид" (Юридическое лицо)', '', '', 624, 2, ''),
(1182, 'ООО "Торгово-Транспортная Компания "Туркус" (Юридична особа)', '', '', 625, 1, 'Если Вы столкнулись с необходимостью осуществить перевозку грузов по территории  Украины и при этом Вы хотите сэкономить свои деньги, сохранить время и свои нервы, то Ваш выбор – Торгово  – Транспортная Компания «ТУРКУС» <br>ООО ТТК "ТУРКУС"  оказывает услуги по подбору груза и экспедированию транспорта.<br>Наши основные принципы работы:<br> - ориентация на долгосрочное сотрудничество с каждым клиентом;<br> - персонализированный подход к каждому клиенту;<br> - обработка запросов клиентов в минимальные сроки;<br> - предоставление актуальной информации по грузам клиента;<br> - быстрая и качественная работа с перевозчиками;<br> - соблюдение оговоренных сроков доставки;<br>Мы с огромным вниманием относимся к качеству нашей работы, поэтому можете быть уверены, что Ваши грузы будут доставлены вовремя, в целостности и сохранности, опыт наших специалистов поможет Вам оценить нашу работы на самом высоком уровне.<br>МЫ РАБОТАЕМ ДЛЯ ВАС!<br>'),
(1183, 'ООО "Торгово-Транспортная Компания "Туркус" (Юридическое лицо)', '', '', 625, 2, 'Если Вы столкнулись с необходимостью осуществить перевозку грузов по территории  Украины и при этом Вы хотите сэкономить свои деньги, сохранить время и свои нервы, то Ваш выбор – Торгово  – Транспортная Компания «ТУРКУС» <br>ООО ТТК "ТУРКУС"  оказывает услуги по подбору груза и экспедированию транспорта.<br>Наши основные принципы работы:<br> - ориентация на долгосрочное сотрудничество с каждым клиентом;<br> - персонализированный подход к каждому клиенту;<br> - обработка запросов клиентов в минимальные сроки;<br> - предоставление актуальной информации по грузам клиента;<br> - быстрая и качественная работа с перевозчиками;<br> - соблюдение оговоренных сроков доставки;<br>Мы с огромным вниманием относимся к качеству нашей работы, поэтому можете быть уверены, что Ваши грузы будут доставлены вовремя, в целостности и сохранности, опыт наших специалистов поможет Вам оценить нашу работы на самом высоком уровне.<br>МЫ РАБОТАЕМ ДЛЯ ВАС!<br>'),
(1184, 'ООО "Восток" (Юридична особа)', '', 'ул. Дружбы народов 8', 626, 1, ''),
(1185, 'ООО "Восток" (Юридическое лицо)', '', 'ул. Дружбы народов 8', 626, 2, ''),
(1186, 'ФЛ-П Войтенко В.В (Підприємець)', '', '', 627, 1, ''),
(1187, 'ФЛ-П Войтенко В.В (Предприниматель)', '', '', 627, 2, ''),
(1188, 'ООО "Поли-Пак" (Юридична особа)', '', '', 628, 1, ''),
(1189, 'ООО "Поли-Пак" (Юридическое лицо)', '', '', 628, 2, ''),
(1190, 'ТОВ "Квалес Україна" (Підприємець)', '', 'г. Луцк', 629, 1, ''),
(1191, 'ТОВ "Квалес Україна" (Предприниматель)', '', 'г. Луцк', 629, 2, ''),
(1192, 'Attila', 'akjshfjlahfjlahfahjfahflaha', NULL, 630, 1, ''),
(1193, 'Dreamext Trans', 'Зацепи 10', NULL, 631, 1, ''),
(1194, 'dlkghsljghslgh', 'kfakfhafhlahffha', NULL, 632, 1, ''),
(1195, 'тр.е.к.', 'електроапаратна,3.оф 406', NULL, 633, 1, ''),
(1196, 'ggggggg', 'hghghgghg', NULL, 634, 1, 'Разнообразный и богатый опыт новая модель организационной деятельности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития. Разнообразный и богатый опыт реализация намеченных плановых заданий позволяет выполнять важные задания по разработке новых предложений. Значимость этих проблем настолько очевидна, что новая модель организационной деятельности требуют определения и уточнения системы обучения кадров, соответствует насущным потребностям.\n\nЗначимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития. Таким образом дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки соответствующий условий активизации. Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет оценить значение форм развития. Равным образом рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития. Равным образом дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке новых предложений.'),
(1197, 'Трек-схід', 'Луцк, Электроаппаратная,3, оф.406', NULL, 635, 1, ''),
(1198, 'sgsg', 'iriwuriuwiruruwuwuo', NULL, 636, 1, 'Разнообразный и богатый опыт новая модель организационной деятельности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития. Разнообразный и богатый опыт реализация намеченных плановых заданий позволяет выполнять важные задания по разработке новых предложений. Значимость этих проблем настолько очевидна, что новая модель организационной деятельности требуют определения и уточнения системы обучения кадров, соответствует насущным потребностям.\n\nЗначимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития. Таким образом дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки соответствующий условий активизации. Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет оценить значение форм развития. Равным образом рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития. Равным образом дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке новых предложений.'),
(1199, 'jadjakdaj', 'офрвлфлворфолрфлрвф', NULL, 637, 1, ''),
(1200, 'aaaq', 'Шкіл ', NULL, 638, 1, ''),
(1201, 'aaaa', 'унарипр', NULL, 639, 1, ''),
(1202, 'aaaa', 'Шкільна 1', NULL, 640, 1, '');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Ownership_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Ownership_Types` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_Ownership_Types`
--

INSERT INTO `Frontend_Company_Ownership_Types` (`ID`, `name`, `localeName`) VALUES
(1, 'ТзОВ', 'tzov'),
(2, 'ПП', 'pp'),
(3, 'ФОП', 'fop'),
(4, 'СПД', 'spd'),
(5, 'ФЛ-П', ''),
(6, 'ЧП', ''),
(7, 'ФЛП', ''),
(8, 'ООО', ''),
(9, 'ТОВ', ''),
(10, 'ФО-П', ''),
(11, 'T.E.C.', ''),
(12, 'МЧП', ''),
(13, 'ДТЕП', ''),
(14, 'ЧП"ТА', ''),
(15, 'ПТЕП', ''),
(16, 'Transport', ''),
(17, 'ОАО', ''),
(18, 'МП', ''),
(19, 'ЗГО', ''),
(20, 'ПрАТ', ''),
(21, 'Леонідова', ''),
(22, 'ЧПКФ', ''),
(23, 'ЧАО', ''),
(24, 'ДП', ''),
(25, 'ПВКП', ''),
(26, 'ЧПФ', ''),
(27, 'ЧФ', ''),
(28, 'АСОЦІАЦІЯ', ''),
(29, 'ПКФ', '');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1348 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_to_Geo_Relation`
--

INSERT INTO `Frontend_Company_to_Geo_Relation` (`ID`, `geoID`, `companyID`) VALUES
(1081, 1067, 536),
(1086, 1067, 538),
(1095, 1067, 541),
(1101, 1067, 543),
(1118, 1067, 549),
(1121, 1067, 550),
(1145, 1067, 560),
(1160, 1067, 566),
(1163, 1067, 567),
(1178, 1067, 572),
(1193, 1067, 579),
(1199, 1067, 581),
(1225, 1067, 591),
(1228, 1067, 592),
(1292, 1067, 614),
(1300, 1067, 617),
(1328, 1067, 627),
(1082, 1068, 536),
(1087, 1068, 538),
(1096, 1068, 541),
(1102, 1068, 543),
(1119, 1068, 549),
(1122, 1068, 550),
(1146, 1068, 560),
(1161, 1068, 566),
(1164, 1068, 567),
(1179, 1068, 572),
(1194, 1068, 579),
(1200, 1068, 581),
(1226, 1068, 591),
(1229, 1068, 592),
(1293, 1068, 614),
(1301, 1068, 617),
(1329, 1068, 627),
(1083, 1069, 536),
(1085, 1069, 537),
(1088, 1069, 538),
(1091, 1069, 539),
(1094, 1069, 540),
(1097, 1069, 541),
(1100, 1069, 542),
(1103, 1069, 543),
(1105, 1069, 544),
(1108, 1069, 545),
(1111, 1069, 546),
(1114, 1069, 547),
(1117, 1069, 548),
(1120, 1069, 549),
(1123, 1069, 550),
(1125, 1069, 551),
(1127, 1069, 552),
(1129, 1069, 553),
(1132, 1069, 555),
(1135, 1069, 556),
(1138, 1069, 557),
(1141, 1069, 558),
(1144, 1069, 559),
(1147, 1069, 560),
(1149, 1069, 561),
(1151, 1069, 562),
(1154, 1069, 563),
(1157, 1069, 564),
(1159, 1069, 565),
(1162, 1069, 566),
(1165, 1069, 567),
(1168, 1069, 568),
(1171, 1069, 569),
(1174, 1069, 570),
(1177, 1069, 571),
(1180, 1069, 572),
(1182, 1069, 573),
(1185, 1069, 574),
(1187, 1069, 575),
(1190, 1069, 577),
(1192, 1069, 578),
(1195, 1069, 579),
(1198, 1069, 580),
(1201, 1069, 581),
(1203, 1069, 582),
(1205, 1069, 583),
(1208, 1069, 584),
(1211, 1069, 585),
(1214, 1069, 586),
(1217, 1069, 587),
(1219, 1069, 588),
(1222, 1069, 589),
(1224, 1069, 590),
(1227, 1069, 591),
(1230, 1069, 592),
(1233, 1069, 593),
(1236, 1069, 594),
(1238, 1069, 595),
(1241, 1069, 596),
(1244, 1069, 597),
(1247, 1069, 598),
(1250, 1069, 599),
(1253, 1069, 600),
(1256, 1069, 601),
(1259, 1069, 602),
(1262, 1069, 603),
(1265, 1069, 604),
(1267, 1069, 605),
(1270, 1069, 606),
(1273, 1069, 607),
(1276, 1069, 608),
(1279, 1069, 609),
(1282, 1069, 610),
(1285, 1069, 611),
(1288, 1069, 612),
(1291, 1069, 613),
(1294, 1069, 614),
(1296, 1069, 615),
(1299, 1069, 616),
(1302, 1069, 617),
(1305, 1069, 618),
(1308, 1069, 619),
(1311, 1069, 620),
(1314, 1069, 621),
(1316, 1069, 622),
(1319, 1069, 623),
(1321, 1069, 624),
(1324, 1069, 625),
(1327, 1069, 626),
(1330, 1069, 627),
(1333, 1069, 628),
(1336, 1069, 629),
(1342, 1069, 635),
(1317, 1070, 623),
(1184, 1071, 574),
(1210, 1071, 585),
(1318, 1071, 623),
(1109, 1072, 546),
(1306, 1072, 619),
(1331, 1072, 628),
(1090, 1073, 539),
(1110, 1073, 546),
(1134, 1073, 556),
(1137, 1073, 557),
(1140, 1073, 558),
(1143, 1073, 559),
(1153, 1073, 563),
(1156, 1073, 564),
(1191, 1073, 578),
(1221, 1073, 589),
(1235, 1073, 594),
(1255, 1073, 601),
(1307, 1073, 619),
(1323, 1073, 625),
(1332, 1073, 628),
(1084, 1074, 537),
(1186, 1074, 575),
(1246, 1074, 598),
(1269, 1074, 606),
(1295, 1074, 615),
(1298, 1074, 616),
(1320, 1074, 624),
(1249, 1076, 599),
(1189, 1079, 577),
(1284, 1079, 611),
(1098, 1081, 542),
(1099, 1082, 542),
(1104, 1085, 544),
(1124, 1085, 551),
(1128, 1085, 553),
(1131, 1085, 555),
(1148, 1085, 561),
(1158, 1085, 565),
(1181, 1085, 573),
(1204, 1085, 583),
(1218, 1085, 588),
(1223, 1085, 590),
(1237, 1085, 595),
(1266, 1085, 605),
(1339, 1085, 632),
(1343, 1085, 636),
(1251, 1086, 600),
(1286, 1086, 612),
(1252, 1087, 600),
(1287, 1087, 612),
(1325, 1089, 626),
(1326, 1090, 626),
(1089, 1091, 539),
(1133, 1091, 556),
(1136, 1091, 557),
(1139, 1091, 558),
(1142, 1091, 559),
(1152, 1091, 563),
(1155, 1091, 564),
(1220, 1091, 589),
(1254, 1091, 601),
(1322, 1091, 625),
(1092, 1093, 540),
(1093, 1094, 540),
(1126, 1094, 552),
(1216, 1094, 587),
(1258, 1094, 602),
(1212, 1098, 586),
(1271, 1098, 607),
(1107, 1099, 545),
(1213, 1099, 586),
(1272, 1099, 607),
(1315, 1099, 622),
(1150, 1101, 562),
(1188, 1103, 577),
(1283, 1103, 611),
(1260, 1105, 603),
(1261, 1106, 603),
(1334, 1109, 629),
(1337, 1109, 630),
(1338, 1109, 631),
(1340, 1109, 633),
(1344, 1109, 637),
(1345, 1109, 638),
(1346, 1109, 639),
(1347, 1109, 640),
(1335, 1110, 629),
(1106, 1114, 545),
(1234, 1118, 594),
(1112, 1119, 547),
(1166, 1119, 568),
(1169, 1119, 569),
(1196, 1119, 580),
(1239, 1119, 596),
(1274, 1119, 608),
(1113, 1120, 547),
(1116, 1120, 548),
(1167, 1120, 568),
(1170, 1120, 569),
(1197, 1120, 580),
(1202, 1120, 582),
(1240, 1120, 596),
(1275, 1120, 608),
(1115, 1122, 548),
(1172, 1124, 570),
(1263, 1124, 604),
(1289, 1124, 613),
(1173, 1125, 570),
(1264, 1125, 604),
(1290, 1125, 613),
(1215, 1126, 587),
(1175, 1130, 571),
(1231, 1130, 593),
(1242, 1130, 597),
(1280, 1130, 610),
(1303, 1130, 618),
(1312, 1130, 621),
(1176, 1131, 571),
(1232, 1131, 593),
(1243, 1131, 597),
(1281, 1131, 610),
(1304, 1131, 618),
(1313, 1131, 621),
(1248, 1132, 599),
(1130, 1133, 554),
(1207, 1141, 584),
(1310, 1148, 620),
(1278, 1158, 609),
(1209, 1159, 585),
(1183, 1163, 574),
(1268, 1178, 606),
(1297, 1178, 616),
(1206, 1196, 584),
(1309, 1206, 620),
(1245, 1209, 598),
(1257, 1213, 602),
(1277, 1226, 609);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Types` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_Types`
--

INSERT INTO `Frontend_Company_Types` (`ID`, `name`, `localeName`) VALUES
(1, 'Експедитор', 'company.type.expeditor'),
(2, 'Транспорт', 'company.type.transport'),
(3, 'Вантажотримач', 'company.type.cargo');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Complaints`
--

CREATE TABLE IF NOT EXISTS `Frontend_Complaints` (
  `ID` int(10) unsigned NOT NULL,
  `message` varchar(1000) NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complainCompanyID` int(10) unsigned NOT NULL,
  `status` enum('readed','unread') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблиця скарг';

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Currencies`
--

CREATE TABLE IF NOT EXISTS `Frontend_Currencies` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `code` char(3) NOT NULL,
  `status` enum('active','unactive') NOT NULL,
  `UAHRate` decimal(12,2) unsigned NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Currencies`
--

INSERT INTO `Frontend_Currencies` (`ID`, `name`, `code`, `status`, `UAHRate`, `localeName`) VALUES
(1, 'Гривня', 'UAN', 'active', '1.00', 'UAN'),
(2, 'Долар', 'USD', 'active', '0.00', 'USD'),
(3, 'Рубль', 'RUB', 'active', '0.00', 'RUB'),
(4, 'Євро', 'EUR', 'active', '0.00', 'EUR');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Dialogs`
--

CREATE TABLE IF NOT EXISTS `Frontend_Dialogs` (
  `id` int(10) unsigned NOT NULL,
  `theme` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Geo`
--

CREATE TABLE IF NOT EXISTS `Frontend_Geo` (
  `ID` int(10) unsigned NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `placeID` varchar(45) NOT NULL,
  `parentID` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `type` enum('country','region','city') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1259 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Geo`
--

INSERT INTO `Frontend_Geo` (`ID`, `lat`, `lng`, `placeID`, `parentID`, `level`, `type`) VALUES
(1067, 49.993500, 36.230385, 'ChIJiw-rY5-gJ0ERCr6kGmgYTC0', 1068, 3, 'city'),
(1068, 49.993500, 36.230385, 'ChIJAfZeLutYJ0ERoEH2iIQGAQE', 1069, 2, 'region'),
(1069, 48.379433, 31.165581, 'ChIJjw5wVMHZ0UAREED2iIQGAQA', NULL, 1, 'country'),
(1070, 48.523945, 37.707581, 'ChIJPVQ1OCPE30ARhvd60ZfFBP0', 1071, 3, 'city'),
(1071, 48.015884, 37.802849, 'ChIJe9U9ohib30ARJEyiBAomT7o', 1069, 2, 'region'),
(1072, 47.910484, 33.391785, 'ChIJe6tUMeDf2kARbhhrfRc6-rA', 1073, 3, 'city'),
(1073, 48.464718, 35.046185, 'ChIJoevoYl_c20ARlxgBvjfDzlk', 1069, 2, 'region'),
(1074, 50.052952, 30.766712, 'ChIJ94pF_F3O1EARB10ge68K4KY', 1069, 2, 'region'),
(1075, 46.811722, 33.490196, 'ChIJJ_pDErycw0ARSaq2pvi38p0', 1076, 3, 'city'),
(1076, 46.635418, 32.616867, 'ChIJh1TvTkORw0ARQEH2iIQGAQE', 1069, 2, 'region'),
(1077, 50.511082, 30.790899, 'ChIJ7xzVfdHb1EARiZaO_Nyy6_Y', 1074, 3, 'city'),
(1078, 49.304771, 25.704943, 'ChIJ921du7mxMUcRKa2DZn9KdQ4', 1079, 4, 'city'),
(1079, 49.553516, 25.594767, 'ChIJZdKe1ltLMEcRUED2iIQGAQE', 1069, 2, 'region'),
(1081, 50.619900, 26.251617, 'ChIJ14l8LapsL0cRhOTE26uBdAI', 1082, 3, 'city'),
(1082, 50.619900, 26.251617, 'ChIJERyljUNML0cRIED2iIQGAQE', 1069, 2, 'region'),
(1083, 51.498199, 31.289351, 'ChIJlzXucYlI1UYRtThg59NIIyo', 1084, 3, 'city'),
(1084, 51.498199, 31.289351, 'ChIJT7SGL2DAKkERMEH2iIQGAQE', 1069, 2, 'region'),
(1085, 50.450100, 30.523399, 'ChIJBUVa4U7P1EAR_kYBF9IxSXY', 1069, 2, 'city'),
(1086, 46.482525, 30.723309, 'ChIJQ0yGC4oxxkARbBfyjOKPnxI', 1087, 3, 'city'),
(1087, 46.484585, 30.732599, 'ChIJaRSOmoExxkARxdd-tWjwax8', 1069, 2, 'region'),
(1088, 47.097134, 37.543365, 'ChIJK1jnvqfm5kARzrV1CjAY0aU', 1071, 3, 'city'),
(1089, 47.822762, 31.184082, 'ChIJ-Ruuw-i6z0ARbnXgFDkpfrg', 1090, 3, 'city'),
(1090, 46.975033, 31.994583, 'ChIJydRVsbqaxUARLq1R8Q3RgpM', 1069, 2, 'region'),
(1091, 48.464718, 35.046185, 'ChIJj0YI_QPj20ARuhrB8tXzHAo', 1073, 3, 'city'),
(1092, 48.632961, 35.224155, 'ChIJVRXbeMRE2UARlI_tb1XWV_E', 1073, 3, 'city'),
(1093, 49.065784, 33.410034, 'ChIJ3xgCm6tT10ARHFucngBOIEk', 1094, 3, 'city'),
(1094, 49.588268, 34.551418, 'ChIJ5cdqFjh110ARcEH2iIQGAQE', 1069, 2, 'region'),
(1095, 51.326832, 28.802896, 'ChIJ4ap0ufiHKUcRCsP53TK-M8E', 1096, 3, 'city'),
(1096, 50.254650, 28.658667, 'ChIJC9KIyxTxK0cRoED2iIQGAQE', 1069, 2, 'region'),
(1097, 50.367836, 33.979671, 'ChIJ96NVLLvVKUERGtDRtB-DHwg', 1094, 3, 'city'),
(1098, 47.838799, 35.139568, 'ChIJA7uF-j1n3EARSj9NB9lcZ34', 1099, 3, 'city'),
(1099, 47.838799, 35.139568, 'ChIJ0U8jLBVn3EARjGp5k7-Oh-E', 1069, 2, 'region'),
(1100, 49.422981, 26.987133, 'ChIJixe7REMGMkcRZMnZJDsL89k', 1101, 3, 'city'),
(1101, 49.422981, 26.987133, 'ChIJtbpfwCz5LUcRkED2iIQGAQE', 1069, 2, 'region'),
(1102, 49.833855, 36.324013, 'ChIJlSWMAMlzJ0ER9g6xR8uvOg4', 1068, 3, 'city'),
(1103, 49.553516, 25.594767, 'ChIJdc6CS602MEcR6FSx7UekhMQ', 1079, 3, 'city'),
(1104, 46.751526, 33.367821, 'ChIJW-VT-fqPw0AR-Fld4_pJ3ec', 1076, 3, 'city'),
(1105, 48.922634, 24.711117, 'ChIJHTiwNGzBMEcRwkGe5ZQj09Y', 1106, 3, 'city'),
(1106, 48.922634, 24.711117, 'ChIJHTiwNGzBMEcRn1rIHA4suJc', 1069, 2, 'region'),
(1107, 49.083591, 24.499718, 'ChIJHYw_DWWXMEcRRgARZEtfUwY', 1087, 3, 'city'),
(1108, 49.545727, 35.738094, 'ChIJ63kJASaG2EARyWd-h5sllVA', 1068, 3, 'city'),
(1109, 50.747234, 25.325382, 'ChIJXZaFoeuZJUcRbLiNIqJ0UtI', 1110, 3, 'city'),
(1110, 50.747234, 25.325382, 'ChIJBYI5hgM0JEcRQED2iIQGAQE', 1069, 2, 'region'),
(1111, 48.292080, 25.935837, 'ChIJBc324n8INEcRem15WfaWs8U', 1112, 3, 'city'),
(1112, 48.291683, 25.935217, 'ChIJH2F-TOwINEcR3I8UhwBnFyw', 1069, 2, 'region'),
(1113, 50.953213, 28.645754, 'ChIJ0fDwWQ7bK0cRODv52PKUrlk', 1096, 3, 'city'),
(1114, 46.855022, 35.358700, 'ChIJI-XtJOKxwkARSpGi82_h4aE', 1099, 3, 'city'),
(1115, 48.147514, 37.297134, 'ChIJkd9XEqQk3kARe1AsF0ymR4w', 1071, 3, 'city'),
(1116, 47.758579, 29.533915, 'ChIJh_DM1h4gzEARE_id-25M9ik', 1087, 3, 'city'),
(1117, 50.109940, 30.629885, 'ChIJJZuNXGiX1EAR75RQAG5TlzE', 1074, 3, 'city'),
(1118, 48.523117, 34.613682, 'ChIJ7d-kDjba20AR-IUoSyi549A', 1073, 3, 'city'),
(1119, 49.839684, 24.029716, 'ChIJV5oQCXzdOkcR4ngjARfFI0I', 1120, 3, 'city'),
(1120, 49.839684, 24.029716, 'ChIJb5xjPmndOkcRYj26h4iMDWM', 1069, 2, 'region'),
(1121, 46.295223, 30.648085, 'ChIJ4zSCoPzLx0ARkVtoNc1oMjg', 1087, 3, 'city'),
(1122, 50.387177, 24.219946, 'ChIJ-6mlotXeJEcRhsBaeORTd-4', 1120, 3, 'city'),
(1123, 46.369247, 35.341633, 'ChIJYyfZNbcx6EARosCnBlK1-40', 1099, 3, 'city'),
(1124, 49.233082, 28.468218, 'ChIJiWRaGWVbLUcR_nTd7lnh1Ms', 1125, 3, 'city'),
(1125, 49.233082, 28.468218, 'ChIJiy6tDP5UzUARsED2iIQGAQE', 1069, 2, 'region'),
(1126, 49.588268, 34.551418, 'ChIJa7rmJeQl2EARgsrkud9rPs8', 1094, 3, 'city'),
(1127, 50.256821, 31.781765, 'ChIJd5Y_-x9N1EAR9xj4J6R7_pI', 1074, 3, 'city'),
(1128, 50.109722, 36.115501, 'ChIJye4bwzq7J0ERKMx0RYCzMA0', 1068, 3, 'city'),
(1129, 44.952118, 34.102417, 'ChIJxRyZ397d6kARQAt4vC9CKZw', NULL, 1, 'city'),
(1130, 50.907700, 34.798100, 'ChIJYaRz_CACKUERKGyDazb2dNs', 1131, 3, 'city'),
(1131, 50.907700, 34.798100, 'ChIJYaRz_CACKUER0KUajIxyN1A', 1069, 2, 'region'),
(1132, 46.635418, 32.616867, 'ChIJZ_mbXaIaxEAR_iExjoxHpfM', 1076, 3, 'city'),
(1133, 44.616650, 33.525368, 'ChIJ9USRZe8llUARzDRN_6-nLb0', NULL, 1, 'city'),
(1134, 49.776314, 28.726854, 'ChIJFSKwlEriLEcRmqvRPEIgnFg', 1125, 3, 'city'),
(1135, 47.567459, 34.394814, 'ChIJx07vBnCj3EARxGnU1ZHPOzY', 1073, 3, 'city'),
(1136, 49.444572, 36.037510, 'ChIJN741ReCZ2EAR_NIoxEHp_Zk', 1084, 3, 'city'),
(1137, 46.351509, 34.335510, 'ChIJsZK4ohpCwkARPgBIoixRXXY', 1071, 3, 'city'),
(1138, 48.853199, 37.605301, 'ChIJzfwgVc2a30ARYnfShIcrj98', 1071, 3, 'city'),
(1139, 51.240944, 33.205051, 'ChIJnxaWSPNJKkERsunE02dKCT4', 1131, 3, 'city'),
(1140, 48.719200, 32.677097, 'ChIJKUAabc-N0EARCF0i7ttfnKg', 1141, 3, 'city'),
(1141, 48.507935, 32.262318, 'ChIJ_Y7vgK9C0EARlBkNFDXFp9g', 1069, 2, 'region'),
(1142, 49.691181, 30.478016, 'ChIJ5XQ4zVNx00ARNlbrNxZ-DZ4', 1082, 3, 'city'),
(1143, 50.350704, 31.318892, 'ChIJEau0uLv-1EARL4otsp7142Q', 1074, 3, 'city'),
(1144, 51.220757, 27.644220, 'ChIJeVzEG54bKUcRqnSCuUzwEjU', 1096, 3, 'city'),
(1145, 48.910477, 34.911598, 'ChIJo3RIMawM2UARjoCQCeEj2dM', 1073, 3, 'city'),
(1146, 49.896263, 34.341965, 'ChIJKTtqI17s10ARlRdl1OZ4fq8', 1094, 3, 'city'),
(1147, 49.381470, 32.147423, 'ChIJhVGjTc410UARLzPRGNyr1Ok', 1148, 3, 'city'),
(1148, 49.444431, 32.059769, 'ChIJg26lOnZL0UARiu-AtokbRyg', 1069, 2, 'region'),
(1149, 49.041428, 24.395039, 'ChIJJeHPK2-iMEcRDCILLBRzOsU', 1106, 3, 'city'),
(1150, 50.391899, 30.368267, 'ChIJM8wmjybK1EAR5CLzKWJ9lF8', 1074, 3, 'city'),
(1151, 49.951111, 35.940754, 'ChIJkdSGQA-aJ0ER3u2GZxuZrfU', 1068, 3, 'city'),
(1152, 47.817265, 34.754871, 'ChIJS4OnrGCG3EAR8jk1ywd6mnI', 1073, 3, 'city'),
(1153, 48.480122, 34.021225, 'ChIJu6i0BnFK2kARcLSKbQimPmo', 1073, 3, 'city'),
(1154, 49.049683, 33.228470, 'ChIJzUU6Ilyy0EARcolcKrTJDew', 1141, 3, 'city'),
(1155, 48.045124, 30.888432, 'ChIJoegIajlIzkARd6n2TbDlHcs', 1090, 3, 'city'),
(1156, 49.195293, 31.095556, 'ChIJj0n4JijR00AREnPfSgMXCac', 1068, 3, 'city'),
(1157, 48.906616, 38.443359, 'ChIJxdBMqPEPIEERqg-3pVDQyk0', 1158, 3, 'city'),
(1158, 48.574039, 39.307816, 'ChIJ8zUQQaAY4EARuDtkU2fs3IQ', 1069, 2, 'region'),
(1159, 48.738968, 37.584351, 'ChIJm5vqwKSX30AR7HgmWcHe_Ww', 1071, 3, 'city'),
(1160, 49.748486, 24.137381, 'ChIJEY64_3zpOkcR0d7EgsBXJ88', 1120, 3, 'city'),
(1161, 48.173462, 23.297249, 'ChIJS-oU1kIpOEcRojgYcgYxiao', 1162, 3, 'city'),
(1162, 48.620800, 22.287884, 'ChIJDQKKqwsTOUcRLtC2-ZaZ5Fk', 1069, 2, 'region'),
(1163, 48.015884, 37.802849, 'ChIJLZqRAJWQ4EARhG-Fxf1eMzY', 1071, 3, 'city'),
(1164, 47.685326, 41.825893, 'ChIJIffqXTC6HUER4Jfxg6WjAgE', 1165, 2, 'region'),
(1165, 61.524010, 105.318756, 'ChIJ-yRniZpWPEURE_YRZvj9CRQ', NULL, 1, 'country'),
(1166, 49.694405, 36.359547, 'ChIJsfUISehlJ0EREOTjZOMdTdQ', 1068, 3, 'city'),
(1167, 47.409874, 32.441227, 'ChIJ57TNvcoaxUARDONQvW0Ybbw', 1090, 3, 'city'),
(1168, 47.940323, 35.432812, 'ChIJWdyEZRFB3EARGloZATh__to', 1099, 3, 'city'),
(1169, 46.624020, 31.100065, 'ChIJS1JECHEVxkARGKj_YlbY_Jk', 1087, 3, 'city'),
(1170, 48.472340, 25.279245, 'ChIJb3jSbGO1NkcRuhPu3bQgouQ', 1106, 3, 'city'),
(1171, 50.834953, 25.882986, 'ChIJ_8X7H4xfL0cRcgIDi_TtB4I', 1110, 3, 'city'),
(1172, 47.648926, 34.610519, 'ChIJAXtBLriQ3EARkXC1F2GYsng', 1073, 3, 'city'),
(1173, 48.344120, 33.524479, 'ChIJtdan6jlZ2kAR-7lGBt4t1q4', 1073, 3, 'city'),
(1174, 48.411633, 33.694160, 'ChIJdYnTzata2kARauO5belwGzs', 1073, 3, 'city'),
(1175, 50.317223, 29.054144, 'ChIJeTaysimCLEcRcbTGl-C8QdA', 1096, 3, 'city'),
(1176, 48.204010, 34.873238, 'ChIJBbgHcqmM20ARQFie4VmkfOA', 1073, 3, 'city'),
(1177, 55.055466, 73.316734, 'ChIJ6VtPzxrtqUMRKTPPvsfODQg', 1165, 2, 'city'),
(1178, 49.796799, 30.131084, 'ChIJnXsiNz5C00AR6MYsWPgR44Q', 1074, 3, 'city'),
(1179, 48.530491, 25.041204, 'ChIJ38v0fpXSNkcRAL-HQ7ip4sE', 1106, 3, 'city'),
(1180, 49.070251, 23.854315, 'ChIJXSCxop0ROkcRhfMFF0hwPLU', 1106, 3, 'city'),
(1181, 54.049999, 57.816666, 'ChIJkVL8sDX_2UMR0SnUxa2GaCY', 1182, 3, 'city'),
(1182, 54.231216, 56.164528, 'ChIJ_UeUVQW02UMRNFQ0VFqKPHA', 1165, 2, 'region'),
(1183, 48.305374, 24.470259, 'ChIJrz-mQyAfN0cR5NS93lpJu_o', 1106, 3, 'city'),
(1184, 45.350193, 28.850191, 'ChIJH1ZB9-Rlt0ARfWJy7C3Ymr0', 1087, 3, 'city'),
(1185, 48.458439, 27.792929, 'ChIJE-czwXEvM0cRZ_UVp-a1x34', 1125, 3, 'city'),
(1186, 48.769928, 30.215441, 'ChIJ17G7ibsN0kARUZkv1IAsMUg', 1148, 3, 'city'),
(1187, 46.596390, 33.229832, 'ChIJ4cNCGhTtw0ARYkTwHJsQK-E', 1076, 3, 'city'),
(1188, 50.488377, 30.333094, 'ChIJpTSfSS8zK0cRhnpSG3oKMVE', 1074, 3, 'city'),
(1189, 50.163624, 35.540245, 'ChIJlZQTo3LoJ0ERnH0x-1bTr0c', 1068, 3, 'city'),
(1190, 46.975033, 31.994583, 'ChIJ1RNy-4nLxUARgFbgufo5Dpc', 1090, 3, 'city'),
(1191, 48.554417, 25.519939, 'ChIJNcZCQrtOMUcRac87XuPaXtU', 1106, 3, 'city'),
(1192, 50.092087, 29.543669, 'ChIJ1YKsw3alLEcRasxhec2GuQA', 1096, 3, 'city'),
(1193, 48.696716, 26.582537, 'ChIJd7V6-HO4M0cRN1H35EI7rII', 1101, 3, 'city'),
(1194, 49.013008, 28.504219, 'ChIJI2NoGV-pMkcRBOMfTZDBMdg', 1125, 3, 'city'),
(1195, 48.926842, 23.917402, 'ChIJH0PTYDsEOkcRmpNlfKGDrrg', 1106, 3, 'city'),
(1196, 48.507935, 32.262318, 'ChIJZxtECCdD0EAR2OigSUHFKOg', 1141, 3, 'city'),
(1197, 48.146351, 23.030212, 'ChIJfWm-Il46OEcRW-fiEqgMNfU', 1162, 3, 'city'),
(1198, 48.449306, 22.711712, 'ChIJP4z6unisOUcRqsKSp9UA2gI', 1162, 3, 'city'),
(1199, 50.457325, 29.812654, 'ChIJy-3lNtlAK0cRqdrprDayZAQ', 1074, 3, 'city'),
(1200, 50.299244, 30.720802, 'ChIJ3cO2GNDB1EARkJTLnXZg8ds', 1074, 3, 'city'),
(1201, 50.063766, 29.904968, 'ChIJtWSR9h9V00AR0Prd_F6OZmM', 1074, 3, 'city'),
(1202, 49.426113, 31.261852, 'ChIJBXbL4jrm00ARu8j5qpOIUI0', 1148, 3, 'city'),
(1203, 50.579140, 30.215075, 'ChIJuQvBlRsuK0cRSo-Icn4FVgY', 1074, 3, 'city'),
(1204, 49.521004, 39.562065, 'ChIJOafgLSvkIUERToinsXTm-Co', 1158, 3, 'city'),
(1205, 50.412300, 30.360193, 'ChIJ6ZuohbvL1EARBi69rgUtfCE', 1074, 3, 'city'),
(1206, 49.444431, 32.059769, 'ChIJf5dkYIZL0UAR2LXLqSPn3Pg', 1148, 3, 'city'),
(1207, 50.099472, 24.345448, 'ChIJrzUf6rUyJUcRstEMHa26ckg', 1120, 3, 'city'),
(1208, 46.185452, 30.341492, 'ChIJUcAJETLdx0AReCaG53Cm9Go', 1087, 3, 'city'),
(1209, 50.262318, 30.289282, 'ChIJgz3-him31EAR_ayvafT3_w8', 1074, 3, 'city'),
(1210, 50.254650, 28.658667, 'ChIJXTX6K6NkLEcRKeK52aPSSvE', 1096, 3, 'city'),
(1211, 48.676517, 38.099079, 'ChIJpfIxLlXl30ARd-egGvUb5ok', 1071, 3, 'city'),
(1212, 46.619930, 32.707268, 'ChIJkxeVKvUFxEARr0KCri5OWIQ', 1076, 3, 'city'),
(1213, 49.783653, 33.252663, 'ChIJuV4YuO_-1kARK61tQcx0N4s', 1094, 3, 'city'),
(1214, 55.703297, 21.144279, 'ChIJi0RH5Prb5EYRoYdtFGe8rIA', 1215, 2, 'city'),
(1215, 55.169437, 23.881275, 'ChIJE74zDxSU3UYRubpdpdNUCvM', NULL, 1, 'country'),
(1216, 46.626705, 32.441372, 'ChIJT4LLP00ZxEARDk-9EBu4TOA', 1076, 3, 'city'),
(1217, 48.384949, 28.867336, 'ChIJp57hpbwEzUARWhwsroEKCOM', 1125, 3, 'city'),
(1218, 50.087883, 28.600628, 'ChIJ_aK1SKFfLEcRXxAHJX4Rn6o', 1096, 3, 'city'),
(1219, 49.104000, 29.209274, 'ChIJeQOKQIG80kARbDUSrypA66c', 1125, 3, 'city'),
(1220, 50.627377, 30.916430, 'ChIJH2OT34om1UARpgVsyYWZKpM', 1074, 3, 'city'),
(1221, 48.531597, 25.480886, 'ChIJsZUVlw9MMUcRQkxcLu1bgSw', 1101, 3, 'city'),
(1222, 48.620800, 22.287884, 'ChIJ2eO2RLkZOUcRPCoEMAGupg0', 1162, 3, 'city'),
(1223, 50.770836, 33.525829, 'ChIJI5yyX97zKUERchYZZbTL308', 1131, 3, 'city'),
(1224, 48.584908, 34.812923, 'ChIJgUP5bd5h2UARxgWs6LfY3Zc', 1073, 3, 'city'),
(1225, 47.835960, 36.844845, 'ChIJDYCa8Uj93UARuRaDeKw1Obk', 1071, 3, 'city'),
(1226, 48.948177, 38.487877, 'ChIJF_xkgvIQIEERwhKRyz5fLNM', 1158, 3, 'city'),
(1227, 50.216282, 30.217613, 'ChIJBa8oaeGx1EARqH3npiyFNwA', 1074, 3, 'city'),
(1228, 49.225513, 29.048887, 'ChIJ7-2sJoS20kARJxjMDUO7pwk', 1125, 3, 'city'),
(1229, 47.441471, 35.286728, 'ChIJXaHxAeQd3UARPnxU2SGgFAs', 1099, 3, 'city'),
(1230, 47.246410, 35.709835, 'ChIJ47Ggj75r3UARY48o8fv5OUo', 1099, 3, 'city'),
(1231, 46.114925, 32.910221, 'ChIJkWlqLjJqwUARYJhcjZp2YLM', 1076, 3, 'city'),
(1232, 31.200092, 29.918739, 'ChIJ0w9xJpHE9RQRuWvuKabN4LQ', 1233, 3, 'city'),
(1233, 48.663269, 33.096786, 'ChIJB_gms5Kd0EAR2cAd5E99EQ4', 1234, 2, 'region'),
(1234, 26.820553, 30.802498, 'ChIJ6TZcw3aJNhQRRMTEJQmgRSw', NULL, 1, 'country'),
(1235, 55.918091, 39.178436, 'ChIJNeKdY6cRS0ERH-gQs_mXOEU', 1073, 3, 'city'),
(1236, 47.545940, 33.922867, 'ChIJu9imb2xR20ARxq7zeZMuJgU', 1073, 3, 'city'),
(1237, 46.178432, 34.806816, 'ChIJt_QjDNje6UARdAacI1WyXzw', 1076, 3, 'city'),
(1238, 48.550983, 28.516285, 'ChIJu736eNIkzUARE4SaeWjYgsU', 1125, 3, 'city'),
(1239, 49.366394, 29.658381, 'ChIJASqYvE7n0kARQPG0firc3G8', 1074, 3, 'city'),
(1240, 49.194096, 29.530937, 'ChIJm4k84lTq0kAR74FQvt6Xa1Y', 1125, 3, 'city'),
(1241, 51.564674, 31.784777, 'ChIJmZ61t5DNKkER5cELi3iK_BU', 1084, 3, 'city'),
(1242, 49.448551, 28.522655, 'ChIJdxv26F0VLUcR6p4nufqu4UY', 1125, 3, 'city'),
(1243, 51.608196, 31.232691, 'ChIJYekpKNM41UYR0FAiklk-Flc', 1084, 3, 'city'),
(1244, 46.521095, 32.520126, 'ChIJjY0nwpACxEARSMABHoSLEao', 1076, 3, 'city'),
(1245, 50.590477, 30.483198, 'ChIJXWQiRVjT1EARpJ5SK-iO4A8', 1074, 3, 'city'),
(1246, 50.317116, 30.298166, 'ChIJ05GAS4a21EARYeLRXmjSoWQ', 1074, 3, 'city'),
(1247, 48.197186, 22.637094, 'ChIJbc2BeNRaOEcRt2EBkKfEE6w', 1162, 3, 'city'),
(1257, 50.832321, 25.456697, 'ChIJuSVxo-6TJUcRBt6ltnmjsKQ', 1258, 4, 'city'),
(1258, 50.892330, 25.711143, 'ChIJK284D7-MJUcRDk3vGPLOrJw', 1110, 3, 'region');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Geo_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Geo_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `fullAddress` varchar(1000) NOT NULL,
  `country` char(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1259 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Geo_Locales`
--

INSERT INTO `Frontend_Geo_Locales` (`ID`, `localeID`, `geoID`, `name`, `fullAddress`, `country`) VALUES
(1067, 1, 1067, 'Харків', 'Харків, Харківська область, Україна', 'UA'),
(1068, 1, 1068, 'Харківська область', 'Харківська область, Україна', 'UA'),
(1069, 1, 1069, 'Україна', 'Україна', 'UA'),
(1070, 1, 1070, 'Костянтинівка', 'Костянтинівка, Донецька область, Україна', 'UA'),
(1071, 1, 1071, 'Донецька область', 'Донецька область, Україна', 'UA'),
(1072, 1, 1072, 'Кривий Ріг', 'Кривий Ріг, Дніпропетровська область, Україна', 'UA'),
(1073, 1, 1073, 'Дніпропетровська область', 'Дніпропетровська область, Україна', 'UA'),
(1074, 1, 1074, 'Київська область', 'Київська область, Україна', 'UA'),
(1075, 1, 1075, 'Каховка', 'Каховка, Херсонська область, Україна', 'UA'),
(1076, 1, 1076, 'Херсонська область', 'Херсонська область, Україна', 'UA'),
(1077, 1, 1077, 'Бровари', 'Бровари, Київська область, Україна', 'UA'),
(1078, 1, 1078, 'Теребовля', 'Теребовля, Тернопільська область, Україна', 'UA'),
(1079, 1, 1079, 'Тернопільська область', 'Тернопільська область, Україна', 'UA'),
(1081, 1, 1081, 'Рівне', 'Рівне, Рівненська область, Україна', 'UA'),
(1082, 1, 1082, 'Рівненська область', 'Рівненська область, Україна', 'UA'),
(1083, 1, 1083, 'Чернігів', 'Чернігів, Чернігівська область, Україна', 'UA'),
(1084, 1, 1084, 'Чернігівська область', 'Чернігівська область, Україна', 'UA'),
(1085, 1, 1085, 'Київ', 'Київ, Україна', 'UA'),
(1086, 1, 1086, 'Одеса', 'Одеса, Одеська область, Україна', 'UA'),
(1087, 1, 1087, 'Одеська область', 'Одеська область, Україна', 'UA'),
(1088, 1, 1088, 'Маріуполь', 'Маріуполь, Донецька область, Україна', 'UA'),
(1089, 1, 1089, 'Южноукраїнськ', 'Южноукраїнськ, Миколаївська область, Україна', 'UA'),
(1090, 1, 1090, 'Миколаївська область', 'Миколаївська область, Україна', 'UA'),
(1091, 1, 1091, 'Дніпропетровськ', 'Дніпропетровськ, Дніпропетровська область, Україна', 'UA'),
(1092, 1, 1092, 'Новомосковськ', 'Новомосковськ, Дніпропетровська область, Україна', 'UA'),
(1093, 1, 1093, 'Кременчук', 'Кременчук, Полтавська область, Україна', 'UA'),
(1094, 1, 1094, 'Полтавська область', 'Полтавська область, Україна', 'UA'),
(1095, 1, 1095, 'Овруч', 'Овруч, Житомирська область, Україна', 'UA'),
(1096, 1, 1096, 'Житомирська область', 'Житомирська область, Україна', 'UA'),
(1097, 1, 1097, 'Гадяч', 'Гадяч, Полтавська область, Україна', 'UA'),
(1098, 1, 1098, 'Запоріжжя', 'Запоріжжя, Запорізька область, Україна', 'UA'),
(1099, 1, 1099, 'Запорізька область', 'Запорізька область, Україна', 'UA'),
(1100, 1, 1100, 'Хмельницький', 'Хмельницький, Хмельницька область, Україна', 'UA'),
(1101, 1, 1101, 'Хмельницька область', 'Хмельницька область, Україна', 'UA'),
(1102, 1, 1102, 'Васищеве', 'Васищеве, Харківська область, Україна', 'UA'),
(1103, 1, 1103, 'Тернопіль', 'Тернопіль, Тернопільська область, Україна', 'UA'),
(1104, 1, 1104, 'Нова Каховка', 'Нова Каховка, Херсонська область, Україна', 'UA'),
(1105, 1, 1105, 'Івано-Франківськ', 'Івано-Франківськ, Івано-Франківська область, Україна', 'UA'),
(1106, 1, 1106, 'Івано-Франківська область', 'Івано-Франківська область, Україна', 'UA'),
(1107, 1, 1107, 'Слобідка', 'Слобідка, Івано-Франківська область, Україна', 'UA'),
(1108, 1, 1108, 'Старовірівка', 'Старовірівка, Харківська область, Україна', 'UA'),
(1109, 1, 1109, 'Луцьк', 'Луцьк, Волинська область, Україна', 'UA'),
(1110, 1, 1110, 'Волинська область', 'Волинська область, Україна', 'UA'),
(1111, 1, 1111, 'Чернівці', 'Чернівці, Чернівецька область, Україна', 'UA'),
(1112, 1, 1112, 'Чернівецька область', 'Чернівецька область, Україна', 'UA'),
(1113, 1, 1113, 'Коростень', 'Коростень, Житомирська область, Україна', 'UA'),
(1114, 1, 1114, 'Мелітополь', 'Мелітополь, Запорізька область, Україна', 'UA'),
(1115, 1, 1115, 'Селидове', 'Селидове, Донецька область, Україна', 'UA'),
(1116, 1, 1116, 'Котовськ', 'Котовськ, Одеська область, Україна', 'UA'),
(1117, 1, 1117, 'Обухів', 'Обухів, Київська область, Україна', 'UA'),
(1118, 1, 1118, 'Дніпродзержинськ', 'Дніпродзержинськ, Дніпропетровська область, Україна', 'UA'),
(1119, 1, 1119, 'Львів', 'Львів, Львівська область, Україна', 'UA'),
(1120, 1, 1120, 'Львівська область', 'Львівська область, Україна', 'UA'),
(1121, 1, 1121, 'Чорноморськ', 'Чорноморськ, Одеська область, Україна', 'UA'),
(1122, 1, 1122, 'Червоноград', 'Червоноград, Львівська область, Україна', 'UA'),
(1123, 1, 1123, 'Кирилівка', 'Кирилівка, Запорізька область, Україна', 'UA'),
(1124, 1, 1124, 'Вінниця', 'Вінниця, Вінницька область, Україна', 'UA'),
(1125, 1, 1125, 'Вінницька область', 'Вінницька область, Україна', 'UA'),
(1126, 1, 1126, 'Полтава', 'Полтава, Полтавська область, Україна', 'UA'),
(1127, 1, 1127, 'Яготин', 'Яготин, Київська область, Україна', 'UA'),
(1128, 1, 1128, 'Дергачі', 'Дергачі, Харківська область, Україна', 'UA'),
(1129, 1, 1129, 'Сімферополь', 'Сімферополь', 'UA'),
(1130, 1, 1130, 'Суми', 'Суми, Сумська область, Україна', 'UA'),
(1131, 1, 1131, 'Сумська область', 'Сумська область, Україна', 'UA'),
(1132, 1, 1132, 'Херсон', 'Херсон, Херсонська область, Україна', 'UA'),
(1133, 1, 1133, 'Севастополь', 'Севастополь', 'UA'),
(1134, 1, 1134, 'Глухівці', 'Глухівці, Вінницька область, Україна', 'UA'),
(1135, 1, 1135, 'Нікополь', 'Нікополь, Дніпропетровська область, Україна', 'UA'),
(1136, 1, 1136, 'Семенівка', 'Семенівка, Харківська область, Україна', 'UA'),
(1137, 1, 1137, 'Новотроїцьке', 'Новотроїцьке, Херсонська область, Україна', 'UA'),
(1138, 1, 1138, 'Слов''янськ', 'Слов''янськ, Донецька область, Україна', 'UA'),
(1139, 1, 1139, 'Конотоп', 'Конотоп, Сумська область, Україна', 'UA'),
(1140, 1, 1140, 'Знам''янка', 'Знам''янка, Кіровоградська область, Україна', 'UA'),
(1141, 1, 1141, 'Кіровоградська область', 'Кіровоградська область, Україна', 'UA'),
(1142, 1, 1142, 'Рокитне', 'Рокитне, Київська область, Україна', 'UA'),
(1143, 1, 1143, 'Баришівка', 'Баришівка, Київська область, Україна', 'UA'),
(1144, 1, 1144, 'Олевськ', 'Олевськ, Житомирська область, Україна', 'UA'),
(1145, 1, 1145, 'Магдалинівка', 'Магдалинівка, Дніпропетровська область, Україна', 'UA'),
(1146, 1, 1146, 'Орданівка', 'Орданівка, Полтавська область, Україна', 'UA'),
(1147, 1, 1147, 'Червона Слобода', 'Червона Слобода, Черкаська область, Україна', 'UA'),
(1148, 1, 1148, 'Черкаська область', 'Черкаська область, Україна', 'UA'),
(1149, 1, 1149, 'Калуш', 'Калуш, Івано-Франківська область, Україна', 'UA'),
(1150, 1, 1150, 'Вишневе', 'Вишневе, Київська область, Україна', 'UA'),
(1151, 1, 1151, 'Люботин', 'Люботин, Харківська область, Україна', 'UA'),
(1152, 1, 1152, 'Томаківка', 'Томаківка, Дніпропетровська область, Україна', 'UA'),
(1153, 1, 1153, 'Вільногірськ', 'Вільногірськ, Дніпропетровська область, Україна', 'UA'),
(1154, 1, 1154, 'Світловодськ', 'Світловодськ, Кіровоградська область, Україна', 'UA'),
(1155, 1, 1155, 'Первомайськ', 'Первомайськ, Миколаївська область, Україна', 'UA'),
(1156, 1, 1156, 'Шевченкове', 'Шевченкове, Черкаська область, Україна', 'UA'),
(1157, 1, 1157, 'Лисичанськ', 'Лисичанськ, Луганська область, Україна', 'UA'),
(1158, 1, 1158, 'Луганська область', 'Луганська область, Україна', 'UA'),
(1159, 1, 1159, 'Краматорськ', 'Краматорськ, Донецька область, Україна', 'UA'),
(1160, 1, 1160, 'Давидів', 'Давидів, Львівська область, Україна', 'UA'),
(1161, 1, 1161, 'Хуст', 'Хуст, Закарпатська область, Україна', 'UA'),
(1162, 1, 1162, 'Закарпатська область', 'Закарпатська область, Україна', 'UA'),
(1163, 1, 1163, 'Донецьк', 'Донецьк, Донецька область, Україна', 'UA'),
(1164, 1, 1164, 'Ростовська область', 'Ростовська область, Росія', 'UA'),
(1165, 1, 1165, 'Росія', 'Росія', 'UA'),
(1166, 1, 1166, 'Зміїв', 'Зміїв, Харківська область, Україна', 'UA'),
(1167, 1, 1167, 'Баштанка', 'Баштанка, Миколаївська область, Україна', 'UA'),
(1168, 1, 1168, 'Вільнянськ', 'Вільнянськ, Запорізька область, Україна', 'UA'),
(1169, 1, 1169, 'Южне', 'Южне, Одеська область, Україна', 'UA'),
(1170, 1, 1170, 'Заболотів', 'Заболотів, Івано-Франківська область, Україна', 'UA'),
(1171, 1, 1171, 'Цумань', 'Цумань, Волинська область, Україна', 'UA'),
(1172, 1, 1172, 'Марганець', 'Марганець, Дніпропетровська область, Україна', 'UA'),
(1173, 1, 1173, 'Жовті Води', 'Жовті Води, Дніпропетровська область, Україна', 'UA'),
(1174, 1, 1174, 'П''ятихатки', 'П''ятихатки, Дніпропетровська область, Україна', 'UA'),
(1175, 1, 1175, 'Коростишів', 'Коростишів, Житомирська область, Україна', 'UA'),
(1176, 1, 1176, 'Солоне', 'Солоне, Дніпропетровська область, Україна', 'UA'),
(1177, 1, 1177, 'Омська область', 'Омська область, Росія', 'UA'),
(1178, 1, 1178, 'Біла Церква', 'Біла Церква, Київська область, Україна', 'UA'),
(1179, 1, 1179, 'Коломия', 'Коломия, Івано-Франківська область, Україна', 'UA'),
(1180, 1, 1180, 'Болехів', 'Болехів, Івано-Франківська область, Україна', 'UA'),
(1181, 1, 1181, 'Міжгор''є', 'Міжгор''є, Башкортостан, Росія', 'UA'),
(1182, 1, 1182, 'Башкортостан', 'Башкортостан, Росія', 'UA'),
(1183, 1, 1183, 'Яблуниця', 'Яблуниця, Івано-Франківська область, Україна', 'UA'),
(1184, 1, 1184, 'Ізмаїл', 'Ізмаїл, Одеська область, Україна', 'UA'),
(1185, 1, 1185, 'Могилів-Подільський', 'Могилів-Подільський, Вінницька область, Україна', 'UA'),
(1186, 1, 1186, 'Умань', 'Умань, Черкаська область, Україна', 'UA'),
(1187, 1, 1187, 'Нова Маячка', 'Нова Маячка, Херсонська область, Україна', 'UA'),
(1188, 1, 1188, 'Коцюбинське', 'Коцюбинське, Київська область, Україна', 'UA'),
(1189, 1, 1189, 'Богодухів', 'Богодухів, Харківська область, Україна', 'UA'),
(1190, 1, 1190, 'Миколаїв', 'Миколаїв, Миколаївська область, Україна', 'UA'),
(1191, 1, 1191, 'Підвисоке', 'Підвисоке, Івано-Франківська область, Україна', 'UA'),
(1192, 1, 1192, 'Корнин', 'Корнин, Житомирська область, Україна', 'UA'),
(1193, 1, 1193, 'Кам''янець-Подільський', 'Кам''янець-Подільський, Хмельницька область, Україна', 'UA'),
(1194, 1, 1194, 'Тиврів', 'Тиврів, Вінницька область, Україна', 'UA'),
(1195, 1, 1195, 'Вигода', 'Вигода, Івано-Франківська область, Україна', 'UA'),
(1196, 1, 1196, 'Кіровоград', 'Кіровоград, Кіровоградська область, Україна', 'UA'),
(1197, 1, 1197, 'Виноградів', 'Виноградів, Закарпатська область, Україна', 'UA'),
(1198, 1, 1198, 'Мукачево', 'Мукачево, Закарпатська область, Україна', 'UA'),
(1199, 1, 1199, 'Макарів', 'Макарів, Київська область, Україна', 'UA'),
(1200, 1, 1200, 'Вишеньки', 'Вишеньки, Київська область, Україна', 'UA'),
(1201, 1, 1201, 'Фастів', 'Фастів, Київська область, Україна', 'UA'),
(1202, 1, 1202, 'Корсунь-Шевченківський', 'Корсунь-Шевченківський, Черкаська область, Україна', 'UA'),
(1203, 1, 1203, 'Гостомель', 'Гостомель, Київська область, Україна', 'UA'),
(1204, 1, 1204, 'Марківка', 'Марківка, Луганська область, Україна', 'UA'),
(1205, 1, 1205, 'Софіївська Борщагівка', 'Софіївська Борщагівка, Київська область, Україна', 'UA'),
(1206, 1, 1206, 'Черкаси', 'Черкаси, Черкаська область, Україна', 'UA'),
(1207, 1, 1207, 'Кам''янка-Бузька', 'Кам''янка-Бузька, Львівська область, Україна', 'UA'),
(1208, 1, 1208, 'Білгород-Дністровський', 'Білгород-Дністровський, Одеська область, Україна', 'UA'),
(1209, 1, 1209, 'Глеваха', 'Глеваха, Київська область, Україна', 'UA'),
(1210, 1, 1210, 'Житомир', 'Житомир, Житомирська область, Україна', 'UA'),
(1211, 1, 1211, 'Соледар', 'Соледар, Донецька область, Україна', 'UA'),
(1212, 1, 1212, 'Цюрупинськ', 'Цюрупинськ, Херсонська область, Україна', 'UA'),
(1213, 1, 1213, 'Хорол', 'Хорол, Полтавська область, Україна', 'UA'),
(1214, 1, 1214, 'Клайпеда', 'Клайпеда, Литва', 'UA'),
(1215, 1, 1215, 'Литва', 'Литва', 'UA'),
(1216, 1, 1216, 'Білозерка', 'Білозерка, Херсонська область, Україна', 'UA'),
(1217, 1, 1217, 'Крижопіль', 'Крижопіль, Вінницька область, Україна', 'UA'),
(1218, 1, 1218, 'Озерянка', 'Озерянка, Житомирська область, Україна', 'UA'),
(1219, 1, 1219, 'Іллінці', 'Іллінці, Вінницька область, Україна', 'UA'),
(1220, 1, 1220, 'Богданівка', 'Богданівка, Київська область, Україна', 'UA'),
(1221, 1, 1221, 'Красноставці', 'Красноставці, Івано-Франківська область, Україна', 'UA'),
(1222, 1, 1222, 'Ужгород', 'Ужгород, Закарпатська область, Україна', 'UA'),
(1223, 1, 1223, 'Плавинище', 'Плавинище, Сумська область, Україна', 'UA'),
(1224, 1, 1224, 'Партизанське', 'Партизанське, Дніпропетровська область, Україна', 'UA'),
(1225, 1, 1225, 'Велика Новосілка', 'Велика Новосілка, Донецька область, Україна', 'UA'),
(1226, 1, 1226, 'Сєвєродонецьк', 'Сєвєродонецьк, Луганська область, Україна', 'UA'),
(1227, 1, 1227, 'Багрин', 'Багрин, Київська область, Україна', 'UA'),
(1228, 1, 1228, 'Липовець', 'Липовець, Вінницька область, Україна', 'UA'),
(1229, 1, 1229, 'Василівка', 'Василівка, Запорізька область, Україна', 'UA'),
(1230, 1, 1230, 'Токмак', 'Токмак, Запорізька область, Україна', 'UA'),
(1231, 1, 1231, 'Скадовськ', 'Скадовськ, Херсонська область, Україна', 'UA'),
(1232, 1, 1232, 'Александрія', 'Александрія, Олександрія, Єгипет', 'UA'),
(1233, 1, 1233, 'Олександрія', 'Олександрія, Кіровоградська область, Україна', 'UA'),
(1234, 1, 1234, 'Єгипет', 'Єгипет', 'UA'),
(1235, 1, 1235, 'Покров', 'Покров, Владимирская обл., Росія', 'UA'),
(1236, 1, 1236, 'Мар''янське', 'Мар''янське, Дніпропетровська область, Україна', 'UA'),
(1237, 1, 1237, 'Генічеськ', 'Генічеськ, Херсонська область, Україна', 'UA'),
(1238, 1, 1238, 'Томашпіль', 'Томашпіль, Вінницька область, Україна', 'UA'),
(1239, 1, 1239, 'Тетіїв', 'Тетіїв, Київська область, Україна', 'UA'),
(1240, 1, 1240, 'Оратів', 'Оратів, Вінницька область, Україна', 'UA'),
(1241, 1, 1241, 'Березна', 'Березна, Чернігівська область, Україна', 'UA'),
(1242, 1, 1242, 'Калинівка', 'Калинівка, Вінницька область, Україна', 'UA'),
(1243, 1, 1243, 'Рівнопілля', 'Рівнопілля, Чернігівська область, Україна', 'UA'),
(1244, 1, 1244, 'Гола Пристань', 'Гола Пристань, Херсонська область, Україна', 'UA'),
(1245, 1, 1245, 'Вишгород', 'Вишгород, Київська область, Україна', 'UA'),
(1246, 1, 1246, 'Боярка', 'Боярка, Київська область, Україна', 'UA'),
(1247, 1, 1247, 'Берегове', 'Берегове, Закарпатська область, Україна', 'UA'),
(1257, 1, 1257, 'Ківерці', 'Ківерці, Волинська область, Україна', 'UA'),
(1258, 1, 1258, 'Ківерцівський район', 'Ківерцівський район, Волинська область, Україна', 'UA');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `title` varchar(45) NOT NULL,
  `uri` varchar(3) NOT NULL,
  `status` enum('active','unactive') NOT NULL,
  `googleLang` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Locales`
--

INSERT INTO `Frontend_Locales` (`ID`, `title`, `uri`, `status`, `googleLang`) VALUES
(1, 'Українська', 'ukr', 'active', 'uk'),
(2, 'Русский', 'rus', 'active', 'ru');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Locale_Vars`
--

CREATE TABLE IF NOT EXISTS `Frontend_Locale_Vars` (
  `ID` int(10) unsigned NOT NULL,
  `key` varchar(45) NOT NULL,
  `value` varchar(2000) NOT NULL,
  `localeID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Messages`
--

CREATE TABLE IF NOT EXISTS `Frontend_Messages` (
  `ID` int(10) unsigned NOT NULL,
  `dialogID` int(10) unsigned NOT NULL,
  `fromAdminID` int(10) unsigned DEFAULT NULL,
  `fromUserID` int(10) unsigned DEFAULT NULL,
  `toUserID` int(10) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('readed','unread') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unread',
  `transportID` int(10) unsigned DEFAULT NULL,
  `cargoID` int(10) unsigned DEFAULT NULL,
  `tenderID` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_News`
--

CREATE TABLE IF NOT EXISTS `Frontend_News` (
  `ID` int(10) unsigned NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_News_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_News_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `newsID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Phonecodes`
--

CREATE TABLE IF NOT EXISTS `Frontend_Phonecodes` (
  `ID` int(11) NOT NULL,
  `code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Phonecodes`
--

INSERT INTO `Frontend_Phonecodes` (`ID`, `code`) VALUES
(1, ' '),
(15, '+00'),
(14, '+359'),
(16, '+372'),
(3, '+38'),
(4, '+380'),
(7, '+48'),
(2, '+7');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Registration_Links`
--

CREATE TABLE IF NOT EXISTS `Frontend_Registration_Links` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `temp` varchar(32) NOT NULL,
  `availableUntil` datetime NOT NULL,
  `type` enum('confirm_email') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Request_Prices`
--

CREATE TABLE IF NOT EXISTS `Frontend_Request_Prices` (
  `ID` int(10) unsigned NOT NULL,
  `value` decimal(12,2) DEFAULT NULL,
  `paymentType` enum('b/g','gotivka','combined','card') DEFAULT NULL,
  `PDV` enum('yes','no') DEFAULT NULL,
  `onLoad` enum('yes','no') DEFAULT NULL,
  `onUnload` enum('yes','no') DEFAULT NULL,
  `onPrepay` enum('yes','no') DEFAULT NULL,
  `advancedPayment` int(11) DEFAULT NULL,
  `currencyID` int(10) unsigned DEFAULT NULL,
  `specifiedPrice` enum('yes','no') NOT NULL,
  `customPriceType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1486 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Request_Prices`
--

INSERT INTO `Frontend_Request_Prices` (`ID`, `value`, `paymentType`, `PDV`, `onLoad`, `onUnload`, `onPrepay`, `advancedPayment`, `currencyID`, `specifiedPrice`, `customPriceType`) VALUES
(1194, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1195, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1196, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1197, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1198, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1199, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1202, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1203, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1204, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1205, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1206, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1207, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1208, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1209, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1210, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1211, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1212, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1214, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1215, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1216, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1217, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1218, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1219, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1220, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1221, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1223, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1224, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1225, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1226, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1228, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1229, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1230, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1232, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1233, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1234, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1235, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1236, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1237, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1238, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1239, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1241, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1246, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1247, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1248, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1249, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1250, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1251, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1252, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1253, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1254, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1255, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1256, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1257, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1258, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1259, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1260, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1261, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1262, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1263, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1264, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1265, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1266, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1267, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1268, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1269, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1270, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1271, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1272, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1273, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1274, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1275, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1276, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1277, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1278, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1279, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1280, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1281, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1282, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1283, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1284, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1285, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1286, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1287, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1288, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1289, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1290, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1291, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1292, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1293, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1294, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1295, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1296, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1299, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1300, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1301, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1302, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1303, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1304, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1305, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1306, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1307, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1308, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1309, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1310, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1311, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1312, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1313, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1314, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1315, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1316, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1317, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1318, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1319, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1320, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1321, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1322, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1323, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1324, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1325, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1326, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1327, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1328, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1329, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1330, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1331, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1332, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1333, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1334, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1335, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1336, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1337, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1338, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1339, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1340, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1341, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1342, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1343, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1344, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1345, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1346, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1347, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1348, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1349, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1350, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1351, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1352, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1353, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1354, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1355, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1356, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1357, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1358, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1359, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1361, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1362, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1363, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1364, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1365, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1366, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1367, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1368, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1369, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1370, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1371, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1372, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1373, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1374, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1375, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1376, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1377, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1378, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1379, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1380, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1381, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1382, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1383, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1384, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1385, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1386, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1387, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1388, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1389, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1390, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1391, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1392, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1393, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1394, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1395, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1396, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1397, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1398, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1399, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1400, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1401, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1402, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1403, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1404, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1405, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1406, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1407, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1408, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1409, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1410, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1411, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1412, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1413, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1414, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1415, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1416, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1417, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1418, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1419, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1420, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1421, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1422, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1423, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1424, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1425, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1426, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1427, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1428, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1429, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1430, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1431, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1432, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1433, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1434, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1435, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1436, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1437, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1438, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1439, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1440, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1441, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1442, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1443, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1444, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1445, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1446, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1447, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1448, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1449, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1450, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1451, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1452, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1453, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1454, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1455, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1456, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1457, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1458, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1459, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1460, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1461, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1462, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1463, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1464, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1465, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1466, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1467, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1468, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1469, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1470, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1471, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1472, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1473, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1474, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1475, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1476, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL),
(1477, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1478, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1479, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1480, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1481, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1483, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1484, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL),
(1485, '1.00', 'b/g', 'yes', 'yes', 'yes', 'yes', 1, 1, 'yes', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Tenders`
--

CREATE TABLE IF NOT EXISTS `Frontend_Tenders` (
  `ID` int(10) unsigned NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `cargoType` varchar(256) NOT NULL,
  `transportTypeID` int(10) unsigned DEFAULT NULL,
  `volume` decimal(12,3) NOT NULL,
  `weight` decimal(12,3) NOT NULL,
  `carCount` int(10) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) DEFAULT NULL,
  `sizeY` decimal(12,3) DEFAULT NULL,
  `sizeZ` decimal(12,3) DEFAULT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `onlyTransporter` enum('yes','no') NOT NULL,
  `status` enum('active','deleted','completed') NOT NULL DEFAULT 'active',
  `price` decimal(12,2) unsigned NOT NULL,
  `currencyID` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Tenders_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Tenders_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Transports`
--

CREATE TABLE IF NOT EXISTS `Frontend_Transports` (
  `ID` int(10) unsigned NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `priceID` int(10) unsigned NOT NULL,
  `userCarID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `status` enum('active','deleted','completed') NOT NULL DEFAULT 'active',
  `parser` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=580 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Transports`
--

INSERT INTO `Frontend_Transports` (`ID`, `dateFrom`, `dateTo`, `created`, `info`, `priceID`, `userCarID`, `userID`, `docTIR`, `docCMR`, `docT1`, `docSanPassport`, `docSanBook`, `loadFromSide`, `loadFromTop`, `loadFromBehind`, `loadTent`, `condPlomb`, `condLoad`, `condBelts`, `condRemovableStands`, `condHardSide`, `condCollectableCargo`, `condTemperature`, `condPalets`, `condADR`, `status`, `parser`) VALUES
(460, '2016-04-21', '2016-05-21', '2016-04-21 10:02:17', '', 1211, 121, 2639, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328435'),
(461, '2016-04-21', '2016-05-21', '2016-04-21 10:02:19', '', 1212, 121, 2639, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328425'),
(462, '2016-04-21', '2016-05-21', '2016-04-21 10:02:20', '', 1213, 121, 2639, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328429'),
(463, '2016-04-21', '2016-05-21', '2016-04-21 10:02:22', '', 1214, 122, 2640, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328515'),
(464, '2016-04-21', '2016-05-21', '2016-04-21 10:02:25', '', 1215, 122, 2640, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328529'),
(465, '2016-04-21', '2016-05-21', '2016-04-21 10:02:29', '', 1216, 122, 2640, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54328539'),
(466, '2016-04-21', '2016-05-21', '2016-04-21 10:02:29', '', 1217, 122, 2640, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53765619'),
(467, '2016-04-21', '2016-05-21', '2016-04-21 10:02:33', 'сцепка38 палл 7,3-8,4', 1218, 123, 2643, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351377'),
(468, '2016-04-21', '2016-05-21', '2016-04-21 10:02:38', '17 е.п. оплата любая', 1219, 124, 2645, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53065346'),
(469, '2016-04-21', '2016-05-21', '2016-04-21 10:02:39', '18 е.п. гидроборт', 1220, 124, 2645, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53913441'),
(470, '2016-04-21', '2016-05-21', '2016-04-21 10:07:27', '', 1270, 125, 2803, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53782809'),
(471, '2016-04-22', '2016-05-22', '2016-04-21 10:07:29', '', 1271, 126, 2804, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54274547'),
(472, '2016-04-21', '2016-05-21', '2016-04-21 10:07:31', '', 1272, 127, 2806, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51236579'),
(473, '2016-04-21', '2016-05-21', '2016-04-21 10:07:32', '', 1273, 127, 2806, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51236577'),
(474, '2016-04-21', '2016-05-21', '2016-04-21 10:07:33', '', 1274, 127, 2806, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51236581'),
(475, '2016-04-21', '2016-05-21', '2016-04-21 10:07:33', '', 1275, 127, 2806, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51236575'),
(476, '2016-04-21', '2016-05-21', '2016-04-21 10:07:35', '', 1276, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51600367'),
(477, '2016-04-21', '2016-05-21', '2016-04-21 10:07:36', 'Гидроборт', 1277, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '46236781'),
(478, '2016-04-21', '2016-05-21', '2016-04-21 10:07:36', 'Гидроборт', 1278, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '46236891'),
(479, '2016-04-21', '2016-05-21', '2016-04-21 10:07:37', '', 1279, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '27793441'),
(480, '2016-04-21', '2016-05-21', '2016-04-21 10:07:38', '', 1280, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '28719697'),
(481, '2016-04-21', '2016-05-21', '2016-04-21 10:07:39', '', 1281, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '27917678'),
(482, '2016-04-21', '2016-05-21', '2016-04-21 10:07:40', 'Сцепка.', 1282, 128, 2809, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '50263579'),
(483, '2016-04-21', '2016-05-21', '2016-04-21 10:07:41', '', 1283, 129, 2810, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53756457'),
(484, '2016-04-21', '2016-05-21', '2016-04-21 10:07:42', '', 1284, 129, 2810, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53756425'),
(485, '2016-04-21', '2016-05-21', '2016-04-21 10:07:43', 'гидроборт', 1285, 129, 2810, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53756445'),
(486, '2016-04-21', '2016-05-21', '2016-04-21 10:07:43', 'гидроборт', 1286, 129, 2810, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53756481'),
(487, '2016-04-25', '2016-05-25', '2016-04-21 10:07:45', '', 1287, 130, 2814, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346687'),
(488, '2016-04-25', '2016-05-25', '2016-04-21 10:07:45', '', 1288, 130, 2814, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346701'),
(489, '2016-04-22', '2016-05-22', '2016-04-21 10:07:46', '', 1289, 130, 2814, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346685'),
(490, '2016-04-21', '2016-05-21', '2016-04-21 10:10:43', 'гидроборт', 1340, 131, 2895, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54104845'),
(491, '2016-04-21', '2016-05-21', '2016-04-21 10:10:45', 'гидроборт! отдельно или сборный груз!', 1341, 132, 2900, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53849481'),
(492, '2016-04-21', '2016-05-21', '2016-04-21 10:10:47', '', 1342, 133, 2902, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54327505'),
(493, '2016-04-21', '2016-05-21', '2016-04-21 10:10:48', 'возможно с НДС, рога(коники)', 1343, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51214241'),
(494, '2016-04-22', '2016-05-22', '2016-04-21 10:10:49', '', 1344, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '49094933'),
(495, '2016-04-21', '2016-05-21', '2016-04-21 10:10:49', 'возможно с НДС, рога(коники)', 1345, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '52367963'),
(496, '2016-04-21', '2016-05-21', '2016-04-21 10:10:50', 'рога(коники)', 1346, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '28051704'),
(497, '2016-04-21', '2016-05-21', '2016-04-21 10:10:50', '', 1347, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '48673831'),
(498, '2016-04-21', '2016-05-21', '2016-04-21 10:10:51', 'возможно с НДС, рога(коники)', 1348, 134, 2907, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '52367951'),
(499, '2016-04-21', '2016-05-21', '2016-04-21 10:10:53', '', 1349, 135, 2911, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '46566201'),
(500, '2016-04-21', '2016-05-21', '2016-04-21 10:10:54', '', 1350, 135, 2911, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53250458'),
(501, '2016-04-21', '2016-05-21', '2016-04-21 10:10:55', '', 1351, 135, 2911, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '49538935'),
(502, '2016-04-21', '2016-05-21', '2016-04-21 10:10:55', '', 1352, 135, 2911, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53099226'),
(503, '2016-04-21', '2016-05-21', '2016-04-21 10:10:58', '', 1353, 136, 2915, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '50588009'),
(504, '2016-04-21', '2016-05-21', '2016-04-21 10:10:58', '', 1354, 136, 2915, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54344889'),
(505, '2016-04-21', '2016-05-21', '2016-04-21 10:11:00', '', 1355, 137, 2916, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54342695'),
(506, '2016-04-21', '2016-05-21', '2016-04-21 10:11:02', '', 1356, 138, 2919, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54177511'),
(507, '2016-04-21', '2016-05-21', '2016-04-21 10:11:03', '', 1357, 139, 2920, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '52002855'),
(508, '2016-04-21', '2016-05-21', '2016-04-21 10:11:04', 'Много машин!!!', 1358, 139, 2920, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '48575467'),
(509, '2016-04-21', '2016-05-21', '2016-04-21 10:11:04', 'СВВОБОДЕН!!!!!', 1359, 139, 2920, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '45256671'),
(510, '2016-04-21', '2016-05-21', '2016-04-21 10:11:05', '', 1360, 139, 2920, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54134723'),
(511, '2016-04-21', '2016-05-21', '2016-04-21 10:11:05', 'Машина едет каждый день на КИЕВ!!!', 1361, 139, 2920, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '48575495'),
(512, '2016-04-21', '2016-05-21', '2016-04-21 10:11:07', '', 1362, 140, 2922, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53880609'),
(513, '2016-04-21', '2016-05-21', '2016-04-21 10:11:08', '', 1363, 140, 2922, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51097245'),
(514, '2016-04-21', '2016-05-21', '2016-04-21 10:11:09', '', 1364, 140, 2922, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51643143'),
(515, '2016-04-21', '2016-05-21', '2016-04-21 10:11:10', '', 1365, 140, 2922, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53882071'),
(516, '2016-04-21', '2016-05-21', '2016-04-21 10:11:10', '', 1366, 140, 2922, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53018424'),
(517, '2016-04-21', '2016-05-21', '2016-04-21 10:11:12', 'Любая загрузка Пневмоход   Перевозчик', 1367, 141, 2923, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '43275321'),
(518, '2016-04-21', '2016-05-21', '2016-04-21 10:11:13', 'Любая загрузка', 1368, 141, 2923, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '41631801'),
(519, '2016-04-21', '2016-05-21', '2016-04-21 10:11:15', 'ПЕРЕВОЗЧИК, ТАМ.СВИД, САН.ДОК.', 1369, 142, 2927, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53715693'),
(520, '2016-04-21', '2016-05-21', '2016-04-21 10:11:15', 'ПЕРЕВОЗЧИК, ТАМ.СВИД, САН.ДОК.', 1370, 142, 2927, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53801789'),
(521, '2016-04-21', '2016-05-21', '2016-04-21 10:11:16', 'Любая загрузка;пневмоход;Перевозчик.', 1371, 141, 2923, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '38441511'),
(522, '2016-04-22', '2016-05-22', '2016-04-21 10:11:16', 'Любая загрузка;пневмоход;Перевозчик.', 1372, 141, 2923, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '27292972'),
(523, '2016-04-22', '2016-05-22', '2016-04-21 10:11:18', '', 1373, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '43448761'),
(524, '2016-04-22', '2016-05-22', '2016-04-21 10:11:18', '', 1374, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '48098547'),
(525, '2016-04-22', '2016-05-22', '2016-04-21 10:11:19', 'Перевозчик,сан.паспорт,сан.книжка.', 1375, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '42678421'),
(526, '2016-04-22', '2016-05-22', '2016-04-21 10:11:20', '', 1376, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '49442735'),
(527, '2016-04-21', '2016-05-21', '2016-04-21 10:11:20', 'Перевозчик,транспорт свой.', 1377, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '39499201'),
(528, '2016-04-21', '2016-05-21', '2016-04-21 10:11:21', 'свой транспорт', 1378, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51265629'),
(529, '2016-04-22', '2016-05-22', '2016-04-21 10:11:21', 'Перевозчик,транспорт свой,сандоки,ремни.', 1379, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '43448661'),
(530, '2016-04-22', '2016-05-22', '2016-04-21 10:11:22', 'Перевозчик,транспорт свой.', 1380, 143, 2930, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '46908091'),
(531, '2016-04-22', '2016-05-22', '2016-04-21 10:11:24', 'гидроборт 19еп', 1381, 144, 2932, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54256963'),
(532, '2016-04-23', '2016-05-23', '2016-04-21 10:11:25', '', 1382, 144, 2932, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54325947'),
(533, '2016-04-22', '2016-05-22', '2016-04-21 10:11:26', '', 1383, 144, 2932, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54350077'),
(534, '2016-04-22', '2016-05-22', '2016-04-21 10:11:27', 'Борта, обрешітка, ремені! Перевізник!', 1384, 145, 2936, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351435'),
(535, '2016-04-21', '2016-05-21', '2016-04-21 10:11:29', '+сцепка 6,6х2,44 Volvo кран манипулятор', 1385, 146, 2937, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '49455107'),
(536, '2016-04-21', '2016-05-21', '2016-04-21 10:11:31', '', 1386, 147, 2945, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54349655'),
(537, '2016-04-21', '2016-05-21', '2016-04-21 10:11:31', '', 1387, 147, 2945, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54349657'),
(538, '2016-04-21', '2016-05-21', '2016-04-21 10:11:32', '', 1388, 147, 2945, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54349689'),
(539, '2016-04-22', '2016-05-22', '2016-04-21 10:11:35', '', 1389, 148, 2952, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54318471'),
(540, '2016-04-21', '2016-05-21', '2016-04-21 10:13:06', 'гидроборт', 1440, 149, 3015, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53319330'),
(541, '2016-04-21', '2016-05-21', '2016-04-21 10:13:07', '', 1441, 149, 3015, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53748505'),
(542, '2016-04-22', '2016-05-22', '2016-04-21 10:13:08', '', 1442, 150, 3025, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346001'),
(543, '2016-04-21', '2016-05-21', '2016-04-21 10:13:09', 'санпаспорт', 1443, 150, 3025, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54115379'),
(544, '2016-04-21', '2016-05-21', '2016-04-21 10:13:09', 'санпаспорт', 1444, 150, 3025, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53956333'),
(545, '2016-04-22', '2016-05-22', '2016-04-21 10:13:10', '', 1445, 150, 3025, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346077'),
(546, '2016-04-21', '2016-05-21', '2016-04-21 10:13:11', '', 1446, 151, 3026, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '48910297'),
(547, '2016-04-21', '2016-05-21', '2016-04-21 10:13:13', 'оплата любая', 1447, 151, 3026, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '38798961'),
(548, '2016-04-21', '2016-05-21', '2016-04-21 10:13:14', 'гидроборт,22пал.,санкнижка,санпаспор', 1448, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54319843'),
(549, '2016-04-21', '2016-05-21', '2016-04-21 10:13:15', 'гидроборт,22пал.,санкнижка,санпаспор', 1449, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54319841'),
(550, '2016-04-21', '2016-05-21', '2016-04-21 10:13:16', 'гидроборт,24 пал.,санпаспорт,медкнижка', 1450, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346865'),
(551, '2016-04-21', '2016-05-21', '2016-04-21 10:13:17', 'гидроборт,20пал.санпаспорт,медкнижка', 1451, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54301077'),
(552, '2016-04-21', '2016-05-21', '2016-04-21 10:13:18', 'гидроборт,24 пал.,санпаспорт,медкнижка', 1452, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54346895'),
(553, '2016-04-21', '2016-05-21', '2016-04-21 10:13:20', 'гидроборт,18пал.,санпаспорт,медкнижка', 1453, 152, 3028, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54301053'),
(554, '2016-04-21', '2016-05-21', '2016-04-21 10:13:22', '', 1454, 153, 3029, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '52019113'),
(555, '2016-04-21', '2016-05-21', '2016-04-21 10:13:26', '', 1455, 154, 3032, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '51038183'),
(556, '2016-04-21', '2016-05-21', '2016-04-21 10:13:28', 'перевозчик', 1456, 155, 3034, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '50794311'),
(557, '2016-04-21', '2016-05-21', '2016-04-21 10:13:29', 'Трал', 1457, 156, 3039, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351463'),
(558, '2016-04-21', '2016-05-21', '2016-04-21 10:13:31', '', 1458, 157, 3040, 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '52293475'),
(559, '2016-04-21', '2016-05-21', '2016-04-21 10:13:33', '', 1459, 158, 3042, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351459'),
(560, '2016-04-21', '2016-05-21', '2016-04-21 10:13:34', '', 1460, 159, 3046, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54347649'),
(561, '2016-04-21', '2016-05-21', '2016-04-21 10:13:35', 'МАЗ Ивановец, стрела 14 метров', 1461, 159, 3046, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54076117'),
(562, '2016-04-25', '2016-05-25', '2016-04-21 10:13:38', '', 1462, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54344691'),
(563, '2016-04-21', '2016-05-21', '2016-04-21 10:13:38', '', 1463, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54334737'),
(564, '2016-04-21', '2016-05-21', '2016-04-21 10:13:39', '', 1464, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54347273'),
(565, '2016-04-21', '2016-05-21', '2016-04-21 10:13:40', '', 1465, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54340319'),
(566, '2016-04-21', '2016-05-21', '2016-04-21 10:13:41', '', 1466, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54329455'),
(567, '2016-04-25', '2016-05-25', '2016-04-21 10:13:41', '', 1467, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54307389'),
(568, '2016-04-22', '2016-05-22', '2016-04-21 10:13:42', '', 1468, 160, 3051, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54236011'),
(569, '2016-04-24', '2016-05-24', '2016-04-21 10:13:43', 'Гідроборт!', 1469, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54190085'),
(570, '2016-04-24', '2016-05-24', '2016-04-21 10:13:44', 'Гідроборт!', 1470, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351155'),
(571, '2016-04-21', '2016-05-21', '2016-04-21 10:13:44', 'Гідроборт!', 1471, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54351161'),
(572, '2016-04-24', '2016-05-24', '2016-04-21 10:13:45', 'Гідроборт!', 1472, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54160617'),
(573, '2016-04-21', '2016-05-21', '2016-04-21 10:13:45', '', 1473, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54347151'),
(574, '2016-04-21', '2016-05-21', '2016-04-21 10:13:46', 'Гідроборт!', 1474, 161, 3056, 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54334943'),
(575, '2016-04-21', '2016-05-21', '2016-04-21 10:13:46', 'перевозчик,', 1475, 155, 3034, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '54228147'),
(576, '2016-04-21', '2016-05-21', '2016-04-21 10:13:47', 'с лебедкой', 1476, 146, 2937, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, NULL, 'active', '53196034'),
(577, '2021-04-20', '2021-05-20', '2016-04-21 13:05:47', '', 1478, 163, 3064, 'yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no', NULL, NULL, 10, 'active', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Transport_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Transport_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3466 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Transport_to_Geo_Relation`
--

INSERT INTO `Frontend_Transport_to_Geo_Relation` (`ID`, `requestID`, `geoID`, `type`, `groupNumber`) VALUES
(2906, 460, 1067, 'from', 1),
(2907, 460, 1068, 'from', 1),
(2908, 460, 1069, 'from', 1),
(2911, 460, 1069, 'to', 2),
(2909, 460, 1109, 'to', 2),
(2910, 460, 1110, 'to', 2),
(2912, 461, 1067, 'from', 1),
(2913, 461, 1068, 'from', 1),
(2914, 461, 1069, 'from', 1),
(2917, 461, 1069, 'to', 2),
(2915, 461, 1109, 'to', 2),
(2916, 461, 1110, 'to', 2),
(2918, 462, 1067, 'from', 1),
(2919, 462, 1068, 'from', 1),
(2920, 462, 1069, 'from', 1),
(2923, 462, 1069, 'to', 2),
(2921, 462, 1109, 'to', 2),
(2922, 462, 1110, 'to', 2),
(2926, 463, 1069, 'from', 1),
(2929, 463, 1069, 'to', 2),
(2925, 463, 1074, 'from', 1),
(2924, 463, 1077, 'from', 1),
(2927, 463, 1111, 'to', 2),
(2928, 463, 1112, 'to', 2),
(2932, 464, 1069, 'from', 1),
(2934, 464, 1069, 'to', 2),
(2930, 464, 1081, 'from', 1),
(2931, 464, 1082, 'from', 1),
(2933, 464, 1085, 'to', 2),
(2937, 465, 1069, 'from', 1),
(2940, 465, 1069, 'to', 2),
(2935, 465, 1081, 'from', 1),
(2938, 465, 1081, 'to', 2),
(2936, 465, 1082, 'from', 1),
(2939, 465, 1082, 'to', 2),
(2943, 466, 1069, 'from', 1),
(2946, 466, 1069, 'to', 2),
(2942, 466, 1073, 'from', 1),
(2941, 466, 1091, 'from', 1),
(2945, 466, 1096, 'to', 2),
(2944, 466, 1113, 'to', 2),
(2949, 467, 1069, 'from', 1),
(2951, 467, 1069, 'to', 2),
(2948, 467, 1073, 'from', 1),
(2950, 467, 1085, 'to', 2),
(2947, 467, 1091, 'from', 1),
(2954, 468, 1069, 'from', 1),
(2956, 468, 1069, 'to', 2),
(2955, 468, 1085, 'to', 2),
(2952, 468, 1086, 'from', 1),
(2953, 468, 1087, 'from', 1),
(2958, 469, 1069, 'from', 1),
(2961, 469, 1069, 'to', 2),
(2960, 469, 1073, 'to', 2),
(2957, 469, 1085, 'from', 1),
(2959, 469, 1091, 'to', 2),
(2964, 470, 1069, 'from', 1),
(2966, 470, 1069, 'to', 2),
(2965, 470, 1085, 'to', 2),
(2962, 470, 1086, 'from', 1),
(2963, 470, 1087, 'from', 1),
(2969, 471, 1069, 'from', 1),
(2970, 471, 1069, 'to', 2),
(2967, 471, 1086, 'from', 1),
(2968, 471, 1087, 'from', 1),
(2972, 472, 1069, 'from', 1),
(2973, 472, 1069, 'to', 2),
(2971, 472, 1085, 'from', 1),
(2975, 473, 1069, 'from', 1),
(2976, 473, 1069, 'to', 2),
(2974, 473, 1085, 'from', 1),
(2978, 474, 1069, 'from', 1),
(2979, 474, 1069, 'to', 2),
(2977, 474, 1085, 'from', 1),
(2981, 475, 1069, 'from', 1),
(2982, 475, 1069, 'to', 2),
(2980, 475, 1085, 'from', 1),
(2985, 476, 1069, 'from', 1),
(2988, 476, 1069, 'to', 2),
(2984, 476, 1071, 'from', 1),
(2987, 476, 1071, 'to', 2),
(2983, 476, 1163, 'from', 1),
(2986, 476, 1163, 'to', 2),
(2991, 477, 1069, 'from', 1),
(2994, 477, 1069, 'to', 2),
(2990, 477, 1071, 'from', 1),
(2993, 477, 1071, 'to', 2),
(2989, 477, 1163, 'from', 1),
(2992, 477, 1163, 'to', 2),
(2997, 478, 1069, 'from', 1),
(2999, 478, 1069, 'to', 2),
(2996, 478, 1071, 'from', 1),
(2998, 478, 1085, 'to', 2),
(2995, 478, 1163, 'from', 1),
(3002, 479, 1069, 'from', 1),
(3001, 479, 1071, 'from', 1),
(3000, 479, 1163, 'from', 1),
(3003, 479, 1163, 'to', 2),
(3004, 479, 1164, 'to', 2),
(3005, 479, 1165, 'to', 2),
(3008, 480, 1069, 'from', 1),
(3007, 480, 1071, 'from', 1),
(3006, 480, 1163, 'from', 1),
(3009, 480, 1163, 'to', 2),
(3010, 480, 1164, 'to', 2),
(3011, 480, 1165, 'to', 2),
(3014, 481, 1069, 'from', 1),
(3016, 481, 1069, 'to', 2),
(3013, 481, 1071, 'from', 1),
(3015, 481, 1071, 'to', 2),
(3012, 481, 1163, 'from', 1),
(3019, 482, 1069, 'from', 1),
(3022, 482, 1069, 'to', 2),
(3018, 482, 1071, 'from', 1),
(3021, 482, 1071, 'to', 2),
(3017, 482, 1163, 'from', 1),
(3020, 482, 1163, 'to', 2),
(3024, 483, 1069, 'from', 1),
(3026, 483, 1069, 'to', 2),
(3023, 483, 1085, 'from', 1),
(3025, 483, 1085, 'to', 2),
(3028, 484, 1069, 'from', 1),
(3030, 484, 1069, 'to', 2),
(3027, 484, 1085, 'from', 1),
(3029, 484, 1085, 'to', 2),
(3032, 485, 1069, 'from', 1),
(3034, 485, 1069, 'to', 2),
(3031, 485, 1085, 'from', 1),
(3033, 485, 1085, 'to', 2),
(3036, 486, 1069, 'from', 1),
(3038, 486, 1069, 'to', 2),
(3035, 486, 1085, 'from', 1),
(3037, 486, 1085, 'to', 2),
(3040, 487, 1069, 'from', 1),
(3043, 487, 1069, 'to', 2),
(3042, 487, 1079, 'to', 2),
(3039, 487, 1085, 'from', 1),
(3041, 487, 1103, 'to', 2),
(3045, 488, 1069, 'from', 1),
(3048, 488, 1069, 'to', 2),
(3047, 488, 1079, 'to', 2),
(3044, 488, 1085, 'from', 1),
(3046, 488, 1103, 'to', 2),
(3050, 489, 1069, 'from', 1),
(3053, 489, 1069, 'to', 2),
(3052, 489, 1079, 'to', 2),
(3049, 489, 1085, 'from', 1),
(3051, 489, 1103, 'to', 2),
(3055, 490, 1069, 'from', 1),
(3058, 490, 1069, 'to', 2),
(3057, 490, 1073, 'to', 2),
(3054, 490, 1085, 'from', 1),
(3056, 490, 1091, 'to', 2),
(3061, 491, 1069, 'from', 1),
(3063, 491, 1069, 'to', 2),
(3060, 491, 1071, 'from', 1),
(3062, 491, 1085, 'to', 2),
(3059, 491, 1159, 'from', 1),
(3067, 492, 1067, 'to', 2),
(3068, 492, 1068, 'to', 2),
(3066, 492, 1069, 'from', 1),
(3069, 492, 1069, 'to', 2),
(3064, 492, 1086, 'from', 1),
(3065, 492, 1087, 'from', 1),
(3070, 493, 1067, 'from', 1),
(3073, 493, 1067, 'to', 2),
(3071, 493, 1068, 'from', 1),
(3074, 493, 1068, 'to', 2),
(3072, 493, 1069, 'from', 1),
(3075, 493, 1069, 'to', 2),
(3078, 494, 1069, 'from', 1),
(3079, 494, 1069, 'to', 2),
(3077, 494, 1073, 'from', 1),
(3076, 494, 1091, 'from', 1),
(3080, 495, 1069, 'from', 1),
(3083, 495, 1069, 'to', 2),
(3082, 495, 1073, 'to', 2),
(3081, 495, 1091, 'to', 2),
(3084, 496, 1067, 'from', 1),
(3085, 496, 1068, 'from', 1),
(3086, 496, 1069, 'from', 1),
(3089, 496, 1069, 'to', 2),
(3088, 496, 1073, 'to', 2),
(3087, 496, 1091, 'to', 2),
(3090, 497, 1067, 'from', 1),
(3091, 497, 1068, 'from', 1),
(3092, 497, 1069, 'from', 1),
(3093, 497, 1069, 'to', 2),
(3094, 498, 1069, 'from', 1),
(3097, 498, 1069, 'to', 2),
(3096, 498, 1073, 'to', 2),
(3095, 498, 1091, 'to', 2),
(3100, 499, 1069, 'from', 1),
(3101, 499, 1069, 'to', 2),
(3098, 499, 1083, 'from', 1),
(3099, 499, 1084, 'from', 1),
(3104, 500, 1069, 'from', 1),
(3106, 500, 1069, 'to', 2),
(3105, 500, 1085, 'to', 2),
(3102, 500, 1130, 'from', 1),
(3103, 500, 1131, 'from', 1),
(3108, 501, 1069, 'from', 1),
(3109, 501, 1069, 'to', 2),
(3107, 501, 1085, 'from', 1),
(3111, 502, 1069, 'from', 1),
(3113, 502, 1069, 'to', 2),
(3110, 502, 1085, 'from', 1),
(3112, 502, 1085, 'to', 2),
(3116, 503, 1069, 'from', 1),
(3119, 503, 1069, 'to', 2),
(3118, 503, 1073, 'to', 2),
(3117, 503, 1091, 'to', 2),
(3114, 503, 1100, 'from', 1),
(3115, 503, 1101, 'from', 1),
(3122, 504, 1069, 'from', 1),
(3125, 504, 1069, 'to', 2),
(3124, 504, 1073, 'to', 2),
(3123, 504, 1091, 'to', 2),
(3120, 504, 1124, 'from', 1),
(3121, 504, 1125, 'from', 1),
(3127, 505, 1069, 'from', 1),
(3128, 505, 1069, 'to', 2),
(3126, 505, 1085, 'from', 1),
(3131, 506, 1069, 'from', 1),
(3132, 506, 1069, 'to', 2),
(3129, 506, 1119, 'from', 1),
(3130, 506, 1120, 'from', 1),
(3135, 507, 1069, 'from', 1),
(3136, 507, 1069, 'to', 2),
(3133, 507, 1130, 'from', 1),
(3134, 507, 1131, 'from', 1),
(3139, 508, 1069, 'from', 1),
(3140, 508, 1069, 'to', 2),
(3137, 508, 1130, 'from', 1),
(3138, 508, 1131, 'from', 1),
(3143, 509, 1069, 'from', 1),
(3144, 509, 1069, 'to', 2),
(3141, 509, 1130, 'from', 1),
(3142, 509, 1131, 'from', 1),
(3147, 510, 1069, 'from', 1),
(3148, 510, 1069, 'to', 2),
(3145, 510, 1130, 'from', 1),
(3146, 510, 1131, 'from', 1),
(3151, 511, 1069, 'from', 1),
(3153, 511, 1069, 'to', 2),
(3152, 511, 1085, 'to', 2),
(3149, 511, 1130, 'from', 1),
(3150, 511, 1131, 'from', 1),
(3156, 512, 1069, 'from', 1),
(3158, 512, 1069, 'to', 2),
(3157, 512, 1085, 'to', 2),
(3155, 512, 1096, 'from', 1),
(3154, 512, 1210, 'from', 1),
(3161, 513, 1069, 'from', 1),
(3163, 513, 1069, 'to', 2),
(3162, 513, 1085, 'to', 2),
(3160, 513, 1096, 'from', 1),
(3159, 513, 1210, 'from', 1),
(3166, 514, 1069, 'from', 1),
(3168, 514, 1069, 'to', 2),
(3167, 514, 1085, 'to', 2),
(3165, 514, 1096, 'from', 1),
(3164, 514, 1175, 'from', 1),
(3171, 515, 1069, 'from', 1),
(3173, 515, 1069, 'to', 2),
(3172, 515, 1085, 'to', 2),
(3170, 515, 1096, 'from', 1),
(3169, 515, 1210, 'from', 1),
(3176, 516, 1069, 'from', 1),
(3178, 516, 1069, 'to', 2),
(3177, 516, 1085, 'to', 2),
(3175, 516, 1096, 'from', 1),
(3174, 516, 1210, 'from', 1),
(3181, 517, 1069, 'from', 1),
(3182, 517, 1069, 'to', 2),
(3179, 517, 1070, 'from', 1),
(3180, 517, 1071, 'from', 1),
(3185, 518, 1069, 'from', 1),
(3186, 518, 1069, 'to', 2),
(3184, 518, 1071, 'from', 1),
(3183, 518, 1211, 'from', 1),
(3189, 519, 1069, 'from', 1),
(3191, 519, 1069, 'to', 2),
(3190, 519, 1085, 'to', 2),
(3187, 519, 1086, 'from', 1),
(3188, 519, 1087, 'from', 1),
(3194, 520, 1069, 'from', 1),
(3197, 520, 1069, 'to', 2),
(3192, 520, 1086, 'from', 1),
(3195, 520, 1086, 'to', 2),
(3193, 520, 1087, 'from', 1),
(3196, 520, 1087, 'to', 2),
(3200, 521, 1069, 'from', 1),
(3201, 521, 1069, 'to', 2),
(3199, 521, 1071, 'from', 1),
(3198, 521, 1159, 'from', 1),
(3204, 522, 1069, 'from', 1),
(3205, 522, 1069, 'to', 2),
(3202, 522, 1098, 'from', 1),
(3203, 522, 1099, 'from', 1),
(3208, 523, 1069, 'from', 1),
(3211, 523, 1069, 'to', 2),
(3207, 523, 1073, 'from', 1),
(3206, 523, 1091, 'from', 1),
(3209, 523, 1100, 'to', 2),
(3210, 523, 1101, 'to', 2),
(3214, 524, 1069, 'from', 1),
(3217, 524, 1069, 'to', 2),
(3213, 524, 1073, 'from', 1),
(3216, 524, 1076, 'to', 2),
(3212, 524, 1091, 'from', 1),
(3215, 524, 1212, 'to', 2),
(3220, 525, 1069, 'from', 1),
(3223, 525, 1069, 'to', 2),
(3219, 525, 1073, 'from', 1),
(3218, 525, 1091, 'from', 1),
(3221, 525, 1109, 'to', 2),
(3222, 525, 1110, 'to', 2),
(3226, 526, 1069, 'from', 1),
(3229, 526, 1069, 'to', 2),
(3225, 526, 1073, 'from', 1),
(3224, 526, 1091, 'from', 1),
(3227, 526, 1119, 'to', 2),
(3228, 526, 1120, 'to', 2),
(3232, 527, 1069, 'from', 1),
(3235, 527, 1069, 'to', 2),
(3231, 527, 1073, 'from', 1),
(3234, 527, 1073, 'to', 2),
(3230, 527, 1091, 'from', 1),
(3233, 527, 1091, 'to', 2),
(3238, 528, 1069, 'from', 1),
(3241, 528, 1069, 'to', 2),
(3237, 528, 1073, 'from', 1),
(3240, 528, 1073, 'to', 2),
(3236, 528, 1091, 'from', 1),
(3239, 528, 1091, 'to', 2),
(3244, 529, 1069, 'from', 1),
(3245, 529, 1069, 'to', 2),
(3243, 529, 1073, 'from', 1),
(3242, 529, 1091, 'from', 1),
(3248, 530, 1069, 'from', 1),
(3251, 530, 1069, 'to', 2),
(3247, 530, 1073, 'from', 1),
(3250, 530, 1079, 'to', 2),
(3246, 530, 1091, 'from', 1),
(3249, 530, 1103, 'to', 2),
(3253, 531, 1069, 'from', 1),
(3254, 531, 1069, 'to', 2),
(3252, 531, 1085, 'from', 1),
(3257, 532, 1069, 'from', 1),
(3258, 532, 1069, 'to', 2),
(3256, 532, 1099, 'from', 1),
(3255, 532, 1114, 'from', 1),
(3261, 533, 1069, 'from', 1),
(3262, 533, 1069, 'to', 2),
(3260, 533, 1099, 'from', 1),
(3259, 533, 1114, 'from', 1),
(3264, 534, 1069, 'from', 1),
(3267, 534, 1069, 'to', 2),
(3263, 534, 1085, 'from', 1),
(3265, 534, 1119, 'to', 2),
(3266, 534, 1120, 'to', 2),
(3270, 535, 1069, 'from', 1),
(3273, 535, 1069, 'to', 2),
(3268, 535, 1124, 'from', 1),
(3271, 535, 1124, 'to', 2),
(3269, 535, 1125, 'from', 1),
(3272, 535, 1125, 'to', 2),
(3275, 536, 1069, 'from', 1),
(3278, 536, 1069, 'to', 2),
(3274, 536, 1085, 'from', 1),
(3276, 536, 1119, 'to', 2),
(3277, 536, 1120, 'to', 2),
(3280, 537, 1069, 'from', 1),
(3282, 537, 1069, 'to', 2),
(3279, 537, 1085, 'from', 1),
(3281, 537, 1085, 'to', 2),
(3286, 538, 1069, 'to', 2),
(3285, 538, 1085, 'to', 2),
(3283, 538, 1214, 'from', 1),
(3284, 538, 1215, 'from', 1),
(3289, 539, 1069, 'from', 1),
(3290, 539, 1069, 'to', 2),
(3288, 539, 1148, 'from', 1),
(3287, 539, 1206, 'from', 1),
(3291, 540, 1067, 'from', 1),
(3294, 540, 1067, 'to', 2),
(3292, 540, 1068, 'from', 1),
(3295, 540, 1068, 'to', 2),
(3293, 540, 1069, 'from', 1),
(3296, 540, 1069, 'to', 2),
(3297, 541, 1067, 'from', 1),
(3300, 541, 1067, 'to', 2),
(3298, 541, 1068, 'from', 1),
(3301, 541, 1068, 'to', 2),
(3299, 541, 1069, 'from', 1),
(3302, 541, 1069, 'to', 2),
(3304, 542, 1069, 'from', 1),
(3307, 542, 1069, 'to', 2),
(3303, 542, 1085, 'from', 1),
(3305, 542, 1098, 'to', 2),
(3306, 542, 1099, 'to', 2),
(3310, 543, 1069, 'from', 1),
(3312, 543, 1069, 'to', 2),
(3309, 543, 1076, 'from', 1),
(3311, 543, 1085, 'to', 2),
(3308, 543, 1132, 'from', 1),
(3315, 544, 1069, 'from', 1),
(3317, 544, 1069, 'to', 2),
(3316, 544, 1085, 'to', 2),
(3314, 544, 1090, 'from', 1),
(3313, 544, 1190, 'from', 1),
(3319, 545, 1069, 'from', 1),
(3322, 545, 1069, 'to', 2),
(3318, 545, 1085, 'from', 1),
(3320, 545, 1098, 'to', 2),
(3321, 545, 1099, 'to', 2),
(3325, 546, 1069, 'from', 1),
(3326, 546, 1069, 'to', 2),
(3324, 546, 1141, 'from', 1),
(3323, 546, 1196, 'from', 1),
(3329, 547, 1069, 'from', 1),
(3330, 547, 1069, 'to', 2),
(3327, 547, 1072, 'from', 1),
(3328, 547, 1073, 'from', 1),
(3332, 548, 1069, 'from', 1),
(3333, 548, 1069, 'to', 2),
(3331, 548, 1085, 'from', 1),
(3335, 549, 1069, 'from', 1),
(3336, 549, 1069, 'to', 2),
(3334, 549, 1085, 'from', 1),
(3339, 550, 1069, 'from', 1),
(3340, 550, 1069, 'to', 2),
(3338, 550, 1162, 'from', 1),
(3337, 550, 1198, 'from', 1),
(3343, 551, 1069, 'from', 1),
(3344, 551, 1069, 'to', 2),
(3342, 551, 1074, 'from', 1),
(3341, 551, 1077, 'from', 1),
(3347, 552, 1069, 'from', 1),
(3348, 552, 1069, 'to', 2),
(3346, 552, 1162, 'from', 1),
(3345, 552, 1247, 'from', 1),
(3351, 553, 1069, 'from', 1),
(3352, 553, 1069, 'to', 2),
(3350, 553, 1148, 'from', 1),
(3349, 553, 1206, 'from', 1),
(3355, 554, 1069, 'from', 1),
(3358, 554, 1069, 'to', 2),
(3353, 554, 1093, 'from', 1),
(3354, 554, 1094, 'from', 1),
(3357, 554, 1148, 'to', 2),
(3356, 554, 1206, 'to', 2),
(3361, 555, 1069, 'from', 1),
(3362, 555, 1069, 'to', 2),
(3359, 555, 1098, 'from', 1),
(3360, 555, 1099, 'from', 1),
(3365, 556, 1069, 'from', 1),
(3368, 556, 1069, 'to', 2),
(3366, 556, 1070, 'to', 2),
(3367, 556, 1071, 'to', 2),
(3364, 556, 1073, 'from', 1),
(3363, 556, 1091, 'from', 1),
(3370, 557, 1069, 'from', 1),
(3372, 557, 1069, 'to', 2),
(3369, 557, 1085, 'from', 1),
(3371, 557, 1085, 'to', 2),
(3375, 558, 1069, 'from', 1),
(3378, 558, 1069, 'to', 2),
(3374, 558, 1073, 'from', 1),
(3377, 558, 1073, 'to', 2),
(3373, 558, 1091, 'from', 1),
(3376, 558, 1091, 'to', 2),
(3381, 559, 1069, 'from', 1),
(3382, 559, 1069, 'to', 2),
(3379, 559, 1098, 'from', 1),
(3380, 559, 1099, 'from', 1),
(3383, 560, 1067, 'from', 1),
(3384, 560, 1068, 'from', 1),
(3385, 560, 1069, 'from', 1),
(3387, 560, 1069, 'to', 2),
(3386, 560, 1085, 'to', 2),
(3388, 561, 1067, 'from', 1),
(3391, 561, 1067, 'to', 2),
(3389, 561, 1068, 'from', 1),
(3392, 561, 1068, 'to', 2),
(3390, 561, 1069, 'from', 1),
(3393, 561, 1069, 'to', 2),
(3396, 562, 1069, 'from', 1),
(3397, 562, 1069, 'to', 2),
(3394, 562, 1072, 'from', 1),
(3395, 562, 1073, 'from', 1),
(3400, 563, 1069, 'from', 1),
(3401, 563, 1069, 'to', 2),
(3398, 563, 1072, 'from', 1),
(3399, 563, 1073, 'from', 1),
(3402, 564, 1069, 'from', 1),
(3405, 564, 1069, 'to', 2),
(3403, 564, 1072, 'to', 2),
(3404, 564, 1073, 'to', 2),
(3408, 565, 1069, 'from', 1),
(3409, 565, 1069, 'to', 2),
(3407, 565, 1074, 'from', 1),
(3406, 565, 1127, 'from', 1),
(3410, 566, 1067, 'from', 1),
(3411, 566, 1068, 'from', 1),
(3412, 566, 1069, 'from', 1),
(3413, 566, 1069, 'to', 2),
(3416, 567, 1069, 'from', 1),
(3417, 567, 1069, 'to', 2),
(3414, 567, 1072, 'from', 1),
(3415, 567, 1073, 'from', 1),
(3418, 568, 1067, 'from', 1),
(3419, 568, 1068, 'from', 1),
(3420, 568, 1069, 'from', 1),
(3421, 568, 1069, 'to', 2),
(3424, 569, 1069, 'from', 1),
(3425, 569, 1069, 'to', 2),
(3423, 569, 1099, 'from', 1),
(3422, 569, 1114, 'from', 1),
(3428, 570, 1069, 'from', 1),
(3429, 570, 1069, 'to', 2),
(3426, 570, 1098, 'from', 1),
(3427, 570, 1099, 'from', 1),
(3432, 571, 1069, 'from', 1),
(3433, 571, 1069, 'to', 2),
(3431, 571, 1148, 'from', 1),
(3430, 571, 1206, 'from', 1),
(3436, 572, 1069, 'from', 1),
(3437, 572, 1069, 'to', 2),
(3435, 572, 1090, 'from', 1),
(3434, 572, 1190, 'from', 1),
(3440, 573, 1069, 'from', 1),
(3441, 573, 1069, 'to', 2),
(3438, 573, 1109, 'from', 1),
(3439, 573, 1110, 'from', 1),
(3444, 574, 1069, 'from', 1),
(3447, 574, 1069, 'to', 2),
(3445, 574, 1124, 'to', 2),
(3446, 574, 1125, 'to', 2),
(3443, 574, 1148, 'from', 1),
(3442, 574, 1206, 'from', 1),
(3450, 575, 1069, 'from', 1),
(3453, 575, 1069, 'to', 2),
(3451, 575, 1070, 'to', 2),
(3452, 575, 1071, 'to', 2),
(3449, 575, 1073, 'from', 1),
(3448, 575, 1091, 'from', 1),
(3456, 576, 1069, 'from', 1),
(3459, 576, 1069, 'to', 2),
(3454, 576, 1111, 'from', 1),
(3455, 576, 1112, 'from', 1),
(3457, 576, 1124, 'to', 2),
(3458, 576, 1125, 'to', 2),
(3461, 577, 1085, 'to', 0),
(3460, 577, 1119, 'from', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Users`
--

CREATE TABLE IF NOT EXISTS `Frontend_Users` (
  `ID` int(10) unsigned NOT NULL,
  `hash` char(128) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) NOT NULL,
  `emailApproved` enum('approved','pending') NOT NULL,
  `walletBalance` decimal(11,2) NOT NULL,
  `icq` varchar(20) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `currentRating` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phoneStationary` varchar(45) NOT NULL,
  `phoneCodeID` int(11) DEFAULT NULL,
  `phoneStationaryCodeID` int(11) DEFAULT NULL,
  `primaryLocaleID` int(10) unsigned NOT NULL,
  `parser` varchar(30) DEFAULT NULL,
  `parserMail` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3074 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Users`
--

INSERT INTO `Frontend_Users` (`ID`, `hash`, `registered`, `email`, `emailApproved`, `walletBalance`, `icq`, `skype`, `currentRating`, `companyID`, `phone`, `phoneStationary`, `phoneCodeID`, `phoneStationaryCodeID`, `primaryLocaleID`, `parser`, `parserMail`) VALUES
(2604, 'hash', '2016-04-21 09:49:19', '', 'approved', '0.00', '', '', 0, 536, '(67)6401810', '', 4, 1, 1, '14115127471', '/dinamix/email/NGuUUpnzP4dV1yYZ6UKG.png'),
(2605, 'hash', '2016-04-21 09:49:19', '', 'approved', '0.00', '', '', 0, 536, '(63)5818959', '', 4, 1, 1, '14115127471', ''),
(2606, 'hash', '2016-04-21 09:49:26', '', 'approved', '0.00', '', ' brovaru5', 0, 537, '(68)7295393', '(67)3289338', 4, 4, 1, '15072598698', '/dinamix/email/dcXEOe5uHNcYi38i1wEW.png'),
(2607, 'hash', '2016-04-21 09:49:26', '', 'approved', '0.00', '', '', 0, 537, '(97)7404443', '', 4, 1, 1, '15072598698', '/dinamix/email/arhpiAE3UFATSNNYtVOX.png'),
(2608, 'hash', '2016-04-21 09:49:26', '', 'approved', '0.00', '', ' Lagoda20101', 0, 537, '(98)9890089', '(99)3019556', 4, 4, 1, '15072598698', '/dinamix/email/zeYPAQaCLLqDso9VDHwh.png'),
(2609, 'hash', '2016-04-21 09:49:26', '', 'approved', '0.00', '', ' serdgio3333', 0, 537, '(93)4881682', '(67)3289336', 4, 4, 1, '15072598698', '/dinamix/email/rEMqwLUUalGCZh5y1PYU.png'),
(2610, 'hash', '2016-04-21 10:01:49', '', 'approved', '0.00', ' 695101713', '', 0, 538, '(99)9045807', '(93)5656336', 4, 4, 1, '18103044756', ''),
(2611, 'hash', '2016-04-21 10:01:49', '', 'approved', '0.00', ' 695101713', '', 0, 538, '(93)4454277', '(67)5722618', 4, 4, 1, '18103044756', '/dinamix/email/881Wg4dqtSIXtvLfY8H3.png'),
(2612, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', ' 380346006', ' lelik dp.ua', 0, 539, '(56)3721472', '(562)339839', 4, 4, 1, '12219685320', ''),
(2613, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' avchinnikov7', 0, 539, '(67)6411555', '(67)6411555', 4, 4, 1, '12219685320', '/dinamix/email/eF07mEM3FKeO1zkBVMtR.png'),
(2614, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' denis.denchik', 0, 539, '(67)6443787', '(66)6296269', 4, 4, 1, '12219685320', '/dinamix/email/fSo48V3Mhjv9qJ5cUtYQ.png'),
(2615, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', ' 624913869', ' irusya1505', 0, 539, '(67)6333000', '(93)1144799', 4, 4, 1, '12219685320', '/dinamix/email/qAofGWvj.png'),
(2616, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' kravcova_2016', 0, 539, '(67)6443717', '(66)5147755', 4, 4, 1, '12219685320', '/dinamix/email/Os1WuW3ryYKqx2KnlGX2.png'),
(2617, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' zoyahappy', 0, 539, '(67)6333383', '(66)5150033', 4, 4, 1, '12219685320', '/dinamix/email/FRVr1D7xA5892a9vZLUs.png'),
(2618, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' irina04051989', 0, 539, '(99)5041719', '(67)5577730', 4, 4, 1, '12219685320', ''),
(2619, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(99)5041708', '(67)6440708', 4, 4, 1, '12219685320', ''),
(2620, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6443744', '(66)5147744', 4, 4, 1, '12219685320', ''),
(2621, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6440010', '(67)6440010', 4, 4, 1, '12219685320', ''),
(2622, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6216263', '(95)2332323', 4, 4, 1, '12219685320', ''),
(2623, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6440105', '(99)5041715', 4, 4, 1, '12219685320', ''),
(2624, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' Fetisovas1', 0, 539, '(67)5674473', '(67)5674473', 4, 4, 1, '12219685320', '/dinamix/email/329cqkgxtYjKrTw6GDyp.png'),
(2625, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' transclass14', 0, 539, '(67)6443797', '(66)6298280', 4, 4, 1, '12219685320', '/dinamix/email/yPoc36lybjsjbqsajyXf.png'),
(2626, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6443808', '(562)339839', 4, 4, 1, '12219685320', ''),
(2627, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' sergeevna7373', 0, 539, '(67)6443727', '(66)5153399', 4, 4, 1, '12219685320', '/dinamix/email/Ead1qaX21glqjnNwbWXs.png'),
(2628, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' alexandr17095', 0, 539, '(67)6443767', '', 4, 1, 1, '12219685320', ''),
(2629, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' den_drogantsev', 0, 539, '(68)8113997', '(66)7654455', 4, 4, 1, '12219685320', '/dinamix/email/1yzjua6ON350danbZArO.png'),
(2630, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6273535', '(57)7604727', 4, 4, 1, '12219685320', ''),
(2631, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', '', 0, 539, '(67)6940600', '(66)7652288', 4, 4, 1, '12219685320', '/dinamix/email/MN2HopI8CyrDwD0FAGmx.png'),
(2632, 'hash', '2016-04-21 10:01:54', '', 'approved', '0.00', '', ' h.julia.a', 0, 539, '(67)6443700', '(66)7653399', 4, 4, 1, '12219685320', ''),
(2633, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', '', '', 0, 540, '(67)6401900', '(67)6410154', 4, 4, 1, '18478122923', '/dinamix/email/az8YNLho1wkYjVai6Ar8.png'),
(2634, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', '', ' +380997668807 Viber', 0, 540, '(99)7668807', '(95)3898761', 4, 4, 1, '18478122923', '/dinamix/email/XfcWFidFDwNBEZpWdeme.png'),
(2635, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', ' 382966871', ' Viber +38 050 6750093', 0, 540, '(67)6410154', '(50)6750093', 4, 4, 1, '18478122923', '/dinamix/email/zkfH6xylK0CiQRFKpROm.png'),
(2636, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', '', '', 0, 540, '(928)6018153', '', 2, 1, 1, '18478122923', ''),
(2637, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', '', ' elena_shevchenko59', 0, 540, '(67)6401900', '(50)8480839', 4, 4, 1, '18478122923', '/dinamix/email/az8YNLho1wkYjVai6Ar8.png'),
(2638, 'hash', '2016-04-21 10:01:58', '', 'approved', '0.00', '', '', 0, 540, '(50)1357795', '', 4, 1, 1, '18478122923', '/dinamix/email/82XOANOsKHIxVN2S8okX.png'),
(2639, 'hash', '2016-04-21 10:02:16', '', 'approved', '0.00', '', '', 0, 541, '(68)3318813', '(95)4211401', 4, 4, 1, '15423892357', '/dinamix/email/w719OmLhzTt8VdFa4xyU.png'),
(2640, 'hash', '2016-04-21 10:02:21', '', 'approved', '0.00', '', '', 0, 542, '(50)5387606', '(362)286455', 4, 4, 1, '17817103279', ''),
(2641, 'hash', '2016-04-21 10:02:21', '', 'approved', '0.00', '', '', 0, 542, '(362)286455', '', 4, 1, 1, '17817103279', '/dinamix/email/DIgoCobhjE6lXXlor5Kr.png'),
(2642, 'hash', '2016-04-21 10:02:21', '', 'approved', '0.00', '', '', 0, 542, '(36)2286455', '(068)8461818', 4, 4, 1, '17817103279', ''),
(2643, 'hash', '2016-04-21 10:02:32', '', 'approved', '0.00', '', '', 0, 543, '(572)973808', '(50)3258035', 4, 4, 1, '11626492454', '/dinamix/email/3gd8C9LqW0qnJPyqXCwV.png'),
(2644, 'hash', '2016-04-21 10:02:32', '', 'approved', '0.00', '', '', 0, 543, '(50)3258035', '(57)2973808', 4, 4, 1, '11626492454', ''),
(2645, 'hash', '2016-04-21 10:02:37', '', 'approved', '0.00', '', '', 0, 544, '(44)4618933', '(97)2562450', 4, 4, 1, '19851295578', '/dinamix/email/6VoTYvKW5bGGTpwYrNv2.png'),
(2646, 'hash', '2016-04-21 10:02:37', '', 'approved', '0.00', '', '', 0, 544, '(50)5166672', '(67)3881446', 4, 4, 1, '19851295578', ''),
(2647, 'hash', '2016-04-21 10:02:37', '', 'approved', '0.00', '', '', 0, 544, '(97)2562450', '(50)3349176', 4, 4, 1, '19851295578', ''),
(2648, 'hash', '2016-04-21 10:02:37', '', 'approved', '0.00', '', '', 0, 544, '(67)4434665', '(44)4618933', 4, 4, 1, '19851295578', ''),
(2649, 'hash', '2016-04-21 10:05:43', '', 'approved', '0.00', '', '', 0, 545, '(67)6132714', '(99)7177500', 4, 4, 1, '15965667694', ''),
(2650, 'hash', '2016-04-21 10:05:48', '', 'approved', '0.00', '', '', 0, 546, '(67)6060066', '(67)6060066', 4, 4, 1, '12667928577', ''),
(2651, 'hash', '2016-04-21 10:05:53', '', 'approved', '0.00', '', ' vaselban', 0, 547, '(67)4069883', '', 4, 1, 1, '10465293267', '/dinamix/email/6snV5miGkRq8ckqzDpRc.png'),
(2652, 'hash', '2016-04-21 10:05:53', '', 'approved', '0.00', '', ' o.vityuk', 0, 547, '(67)5055095', '', 4, 1, 1, '10465293267', '/dinamix/email/KXXVqRgev12NwJjvuMVw.png'),
(2653, 'hash', '2016-04-21 10:05:53', '', 'approved', '0.00', '', '', 0, 547, '(67)4460686', '', 4, 1, 1, '10465293267', '/dinamix/email/Upg7Mj661hIhRxp14OP2.png'),
(2654, 'hash', '2016-04-21 10:05:53', '', 'approved', '0.00', '', ' Dima_gerasimenko109', 0, 547, '(66)1820246', '(67)5486170', 4, 4, 1, '10465293267', ''),
(2655, 'hash', '2016-04-21 10:05:53', '', 'approved', '0.00', '', ' fedzhora_rz', 0, 547, '(67)3410472', '', 4, 1, 1, '10465293267', '/dinamix/email/ZlpYbfOCW1jvDjGIeRii.png'),
(2656, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', ' anastasija10521', 0, 548, '(93)6592487', '(95)5971789', 4, 4, 1, '18732910735', '/dinamix/email/5OMH27vpYYetJRLDnklH.png'),
(2657, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(73)0331593', '(68)7878297', 4, 4, 1, '18732910735', '/dinamix/email/u9XcnX7w1qsY6hsHOjV9.png'),
(2658, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(66)9940513', '(68)4515700', 4, 4, 1, '18732910735', '/dinamix/email/Ch1DymHLT2BeUzWRcgoi.png'),
(2659, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(97)6507323', '(50)0790107', 4, 4, 1, '18732910735', '/dinamix/email/EbKSFLC3yWhky9btTr3X.png'),
(2660, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(66)8161802', '(67)2849040', 4, 4, 1, '18732910735', '/dinamix/email/mwtLxuJDSk7TmoLJYijP.png'),
(2661, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(98)0011272', '(50)9296982', 4, 4, 1, '18732910735', '/dinamix/email/TbLd66zjtFvJhdBmkZsC.png'),
(2662, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', ' arty-evgen', 0, 548, '(913)0797428', '(923)4638876', 2, 2, 1, '18732910735', '/dinamix/email/Xb6ZNsNasrcAz92VUdy4.png'),
(2663, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(97)3155951', '', 4, 1, 1, '18732910735', '/dinamix/email/EL3Pwsr7XaIwd0wS4ulJ.png'),
(2664, 'hash', '2016-04-21 10:05:58', '', 'approved', '0.00', '', '', 0, 548, '(99)4516467', '(68)9770368', 4, 4, 1, '18732910735', '/dinamix/email/dVasJmlZ7g7OcWvvzlHD.png'),
(2665, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(50)2269033', '', 4, 1, 1, '10827799163', '/dinamix/email/8kzBOlmJFxJvu9v0lygH.png'),
(2666, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(95)3962897', '(95)3962897', 4, 4, 1, '10827799163', '/dinamix/email/n2v8AlnlMGnwxlaE4zsZ.png'),
(2667, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(96)3224302', '(93)1165851', 4, 4, 1, '10827799163', '/dinamix/email/j3W6ZY6cNYq2ZeMpW3X4.png'),
(2668, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', ' dvrn-irina', 0, 549, '(63)4568835', '(68)3696075', 4, 4, 1, '10827799163', '/dinamix/email/BOpVl219gWeYSs2imf9h.png'),
(2669, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(68)8194830', '(99)4998125', 4, 4, 1, '10827799163', ''),
(2670, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(98)6047352', '(95)0554156', 4, 4, 1, '10827799163', '/dinamix/email/AnTt3rrhtcgtphZY57Wr.png'),
(2671, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(50)9477388', '(96)5599096', 4, 4, 1, '10827799163', '/dinamix/email/QdjUgYPC2DHr8dgcREjG.png'),
(2672, 'hash', '2016-04-21 10:06:01', '', 'approved', '0.00', '', '', 0, 549, '(95)4054410', '(96)1180619', 4, 4, 1, '10827799163', '/dinamix/email/tWgbOa4rtuzNf0wAwNi0.png'),
(2673, 'hash', '2016-04-21 10:06:05', '', 'approved', '0.00', '', '', 0, 550, '(96)3896761', '(50)5901982', 4, 4, 1, '14136383595', '/dinamix/email/WIG8WQ5qgvC7eznWqu4t.png'),
(2674, 'hash', '2016-04-21 10:06:05', '', 'approved', '0.00', '', '', 0, 550, '(66)3230433', '(96)9212011', 4, 4, 1, '14136383595', ''),
(2675, 'hash', '2016-04-21 10:06:05', '', 'approved', '0.00', '', '', 0, 550, '(63)7610934', '(96)9237822', 4, 4, 1, '14136383595', ''),
(2676, 'hash', '2016-04-21 10:06:05', '', 'approved', '0.00', '', '', 0, 550, '(96)9382470', '(66)3085150', 4, 4, 1, '14136383595', '/dinamix/email/7UdInH235Lu76RrwkU9Q.png'),
(2677, 'hash', '2016-04-21 10:06:05', '', 'approved', '0.00', '', '', 0, 550, '(50)5901982', '', 4, 1, 1, '14136383595', ''),
(2678, 'hash', '2016-04-21 10:06:08', '', 'approved', '0.00', '', '', 0, 551, '(44)3835735', '(63)3768231', 4, 4, 1, '12007579293', ''),
(2679, 'hash', '2016-04-21 10:06:08', '', 'approved', '0.00', '', '', 0, 551, '(63)3768231', '(44)3835735', 4, 4, 1, '12007579293', '/dinamix/email/VvK4fZwE8gxgu5AIi8zQ.png'),
(2680, 'hash', '2016-04-21 10:06:11', '', 'approved', '0.00', '', '', 0, 552, '(50)3081990', '(98)8321756', 4, 4, 1, '16466834034', ''),
(2681, 'hash', '2016-04-21 10:06:11', '', 'approved', '0.00', '', '', 0, 552, '(97)5175245', '', 4, 1, 1, '16466834034', ''),
(2682, 'hash', '2016-04-21 10:06:11', '', 'approved', '0.00', '', '', 0, 552, '(96)1592916', '', 4, 1, 1, '16466834034', ''),
(2683, 'hash', '2016-04-21 10:06:11', '', 'approved', '0.00', '', '', 0, 552, '(50)3081990', '(98)8321756', 4, 4, 1, '16466834034', '/dinamix/email/wUW8XCIY.png'),
(2684, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', ' vss_logistics', 0, 553, '(44)2291768', '(22)1006119', 4, 7, 1, '18045481444', ''),
(2685, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', ' aleksandr.vss', 0, 553, '(67)7253528', '', 4, 1, 1, '18045481444', ''),
(2686, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', ' vssaleksey', 0, 553, '(67)5486290', '', 4, 1, 1, '18045481444', '/dinamix/email/u54CHNB9P48luwYkbEqZ.png'),
(2687, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', '', 0, 553, '(97)0779666', '', 4, 1, 1, '18045481444', '/dinamix/email/LtFpxxXlMQgbyNQRvoLT.png'),
(2688, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', ' vitaliy210788', 0, 553, '(67)4818183', '(539)468640', 4, 7, 1, '18045481444', '/dinamix/email/SJvKhEOlwLrznaYrXhGh.png'),
(2689, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', '', 0, 553, '(97)0099619', '', 4, 1, 1, '18045481444', '/dinamix/email/30FbBHTL2A2cVOP2JXA9.png'),
(2690, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', '', 0, 553, '(67)4035032', '', 4, 1, 1, '18045481444', ''),
(2691, 'hash', '2016-04-21 10:06:15', '', 'approved', '0.00', '', '', 0, 553, '(44)2230293', '', 4, 1, 1, '18045481444', ''),
(2692, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(50)4980035', '(67)6526529', 4, 4, 1, '12641664291', '/dinamix/email/672zgbrTuVx9iLYgKFD0.png'),
(2693, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(093)0121815', '', 4, 1, 1, '12641664291', '/dinamix/email/672zgbrTuVx9iLYgKFD0.png'),
(2694, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(67)6501124', '', 4, 1, 1, '12641664291', ''),
(2695, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(978)0465305', '', 2, 1, 1, '12641664291', ''),
(2696, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(978)0465352', '', 2, 1, 1, '12641664291', ''),
(2697, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(094)8931792', '', 4, 1, 1, '12641664291', ''),
(2698, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(96)4326832', '', 4, 1, 1, '12641664291', ''),
(2699, 'hash', '2016-04-21 10:06:21', '', 'approved', '0.00', '', '', 0, 554, '(50)4949501', '', 4, 1, 1, '12641664291', ''),
(2700, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(093)0829846', '(095)3237784', 4, 4, 1, '18195069554', '/dinamix/email/tCyIPbunY4KM8X8y7ubP.png'),
(2701, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(68)7072996', '', 4, 1, 1, '18195069554', ''),
(2702, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(68)7072997', '', 4, 1, 1, '18195069554', ''),
(2703, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(95)3237784', '', 4, 1, 1, '18195069554', ''),
(2704, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(68)9648028', '', 4, 1, 1, '18195069554', ''),
(2705, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', '', '', 0, 555, '(93)9858718', '', 4, 1, 1, '18195069554', ''),
(2706, 'hash', '2016-04-21 10:06:24', '', 'approved', '0.00', ' 190127551', ' yra_litash', 0, 555, '(67)6856189', '', 4, 1, 1, '18195069554', '/dinamix/email/pehGiR4qO06qRWfeTCQo.png'),
(2707, 'hash', '2016-04-21 10:06:35', '', 'approved', '0.00', '', '', 0, 556, '(67)8297007', '', 4, 1, 1, '16598168086', '/dinamix/email/Hfk500UpWjM9EPTBa1vP.png'),
(2708, 'hash', '2016-04-21 10:06:35', '', 'approved', '0.00', '', '', 0, 556, '(67)8279009', '', 4, 1, 1, '16598168086', ''),
(2709, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(99)5446362', '(67)7029858', 4, 4, 1, '11375487151', '/dinamix/email/ee7zvr4jxQEVex4NdQgz.png'),
(2710, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(97)1044648', '(66)0716166', 4, 4, 1, '11375487151', '/dinamix/email/DIkjWqQqdnC2TRrSObcp.png'),
(2711, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(66)4900615', '(97)2583639', 4, 4, 1, '11375487151', '/dinamix/email/IkqLWCZpNx08XbGuw0JE.png'),
(2712, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(50)0861250', '', 4, 1, 1, '11375487151', ''),
(2713, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(99)5446362', '(67)7029858', 4, 4, 1, '11375487151', '/dinamix/email/AquwrhXuh1TKGSht3EgW.png'),
(2714, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(95)5270506', '(96)9201872', 4, 4, 1, '11375487151', ''),
(2715, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(95)8156778', '(97)2762015', 4, 4, 1, '11375487151', '/dinamix/email/GbahpF0pBvmpUhyTkVGu.png'),
(2716, 'hash', '2016-04-21 10:06:37', '', 'approved', '0.00', '', '', 0, 557, '(99)2098525', '(67)3796386', 4, 4, 1, '11375487151', '/dinamix/email/WhdIsVaezgBdUxFipeNV.png'),
(2717, 'hash', '2016-04-21 10:06:42', '', 'approved', '0.00', '', ' marina1985725', 0, 558, '(98)8679476', '(63)2705164', 4, 4, 1, '19079925079', '/dinamix/email/3pOQnNZaJjaqRmwQaqgD.png'),
(2718, 'hash', '2016-04-21 10:06:42', '', 'approved', '0.00', '', ' Poprigunchik88', 0, 558, '(73)0400526', '(68)1802527', 4, 4, 1, '19079925079', ''),
(2719, 'hash', '2016-04-21 10:06:42', '', 'approved', '0.00', '', ' irinagvozdeva89', 0, 558, '(97)3796466', '(99)3671703', 4, 4, 1, '19079925079', '/dinamix/email/wHcqqn3CELH3L59F8WbY.png'),
(2720, 'hash', '2016-04-21 10:06:42', '', 'approved', '0.00', '', ' maraorel7', 0, 558, '(98)1037530', '(95)8459943', 4, 4, 1, '19079925079', '/dinamix/email/UNaZKMvSdEA1JD7Uy1FD.png'),
(2721, 'hash', '2016-04-21 10:06:42', '', 'approved', '0.00', '', ' skandakova89', 0, 558, '(66)9482727', '(96)2573226', 4, 4, 1, '19079925079', '/dinamix/email/5YrVFlQRaPAhrYPkNeAg.png'),
(2722, 'hash', '2016-04-21 10:06:47', '', 'approved', '0.00', '', '', 0, 559, '(99)1760963', '', 4, 1, 1, '19333856394', ''),
(2723, 'hash', '2016-04-21 10:06:47', '', 'approved', '0.00', '', '', 0, 559, '(99)1760963', '', 4, 1, 1, '19333856394', ''),
(2724, 'hash', '2016-04-21 10:06:47', '', 'approved', '0.00', '', '', 0, 559, '(63)6374949', '', 4, 1, 1, '19333856394', ''),
(2725, 'hash', '2016-04-21 10:06:50', '', 'approved', '0.00', '', '', 0, 560, '(66)0980462', '(96)5100175', 4, 4, 1, '19787632639', ''),
(2726, 'hash', '2016-04-21 10:06:50', '', 'approved', '0.00', '', '', 0, 560, '(66)2144668', '(96)5100175', 4, 4, 1, '19787632639', ''),
(2727, 'hash', '2016-04-21 10:06:50', '', 'approved', '0.00', '', '', 0, 560, '(95)6835581', '', 4, 1, 1, '19787632639', ''),
(2728, 'hash', '2016-04-21 10:06:50', '', 'approved', '0.00', '', '', 0, 560, '(50)6729995', '', 4, 1, 1, '19787632639', ''),
(2729, 'hash', '2016-04-21 10:06:53', '', 'approved', '0.00', '', '', 0, 561, '(96)6664324', '', 4, 1, 1, '18904068248', ''),
(2730, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', '', 0, 562, '(66)4542840', '(98)6627754', 4, 4, 1, '12511457691', '/dinamix/email/CJFW0CseoQ5uXKVr3UWR.png'),
(2731, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', ' astras38', 0, 562, '(66)8893008', '(96)3778606', 4, 4, 1, '12511457691', '/dinamix/email/QzvjvQdTdZlfN59qmPO4.png'),
(2732, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', ' lenakotenko', 0, 562, '(50)8081838', '(63)8133282', 4, 4, 1, '12511457691', '/dinamix/email/mnxCt5a4e0nycGU4SQBB.png'),
(2733, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', ' irakubyk', 0, 562, '(66)1605331', '(66)2324595', 4, 4, 1, '12511457691', '/dinamix/email/mLSleRlKzNNH3QFP9vv8.png'),
(2734, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', ' nikolay-os', 0, 562, '(63)9588925', '', 4, 1, 1, '12511457691', '/dinamix/email/52yifZ7eFitibPH4zSTc.png'),
(2735, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', '', 0, 562, '(96)5489235', '', 4, 1, 1, '12511457691', '/dinamix/email/CYo2xNCdF2oubAtS9spw.png'),
(2736, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', '', 0, 562, '(93)9718057', '(95)6615309', 4, 4, 1, '12511457691', '/dinamix/email/vG07uDFnDzoMDOgJMunz.png'),
(2737, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', '', 0, 562, '(97)9420804', '', 4, 1, 1, '12511457691', '/dinamix/email/7Cy50drxucfaqNVnqigR.png'),
(2738, 'hash', '2016-04-21 10:06:56', '', 'approved', '0.00', '', '', 0, 562, '(98)8787185', '(99)9856354', 4, 4, 1, '12511457691', '/dinamix/email/wCogaww6FLXRUOhCkPfc.png'),
(2739, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(56)2385434', '(56)2385450', 4, 4, 1, '13920774262', ''),
(2740, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(97)1264627', '(93)8034887', 4, 4, 1, '13920774262', ''),
(2741, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(67)6318862', '', 4, 1, 1, '13920774262', ''),
(2742, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(96)4487057', '', 4, 1, 1, '13920774262', ''),
(2743, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(67)6344956', '(562)385424', 4, 4, 1, '13920774262', '/dinamix/email/2kjjgARwvM9RPW6Xt4Fo.png'),
(2744, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(56)7916349', '', 4, 1, 1, '13920774262', ''),
(2745, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(99)4525944', '', 4, 1, 1, '13920774262', ''),
(2746, 'hash', '2016-04-21 10:06:58', '', 'approved', '0.00', '', '', 0, 563, '(067)6375966', '(0562)385450', 3, 3, 1, '13920774262', ''),
(2747, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(56)7320968', '(56)7703113', 4, 4, 1, '17655292941', '/dinamix/email/jVm7aDdYvl1La8ExbxjF.png'),
(2748, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', ' geradnepro', 0, 564, '(67)6112629', '', 4, 1, 1, '17655292941', '/dinamix/email/hc1K1oCSXude4bLdQgWc.png'),
(2749, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(67)5575440', '', 4, 1, 1, '17655292941', ''),
(2750, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(67)6112610', '', 4, 1, 1, '17655292941', '/dinamix/email/YaqNt3WfewW0CCMOibwe.png'),
(2751, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(67)5667264', '', 4, 1, 1, '17655292941', '/dinamix/email/5ZSHYLlIWKEDwAVcYWMM.png'),
(2752, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', ' julia.melnychyuk', 0, 564, '(67)6360060', '', 4, 1, 1, '17655292941', '/dinamix/email/KRl7D1cyTNBdu2rarC1P.png'),
(2753, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', ' borislav.bonin', 0, 564, '(87)8626194', '', 14, 1, 1, '17655292941', ''),
(2754, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(97)4927960', '', 4, 1, 1, '17655292941', '/dinamix/email/yB3SQnPI4S8LhN2Glyop.png'),
(2755, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(67)8270056', '(67)6340445', 4, 4, 1, '17655292941', '/dinamix/email/NeAps5k6oAS9s2sdSRYh.png'),
(2756, 'hash', '2016-04-21 10:07:01', '', 'approved', '0.00', '', '', 0, 564, '(67)7876333', '(67)7876333', 4, 4, 1, '17655292941', ''),
(2757, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', '', ' ltek-trans', 0, 565, '(44)2233144', '(63)7338115', 4, 4, 1, '16344062150', ''),
(2758, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 653971628', ' ltek-trans', 0, 565, '(50)4748228', '', 4, 1, 1, '16344062150', '/dinamix/email/lQ3MXJUPBEAH8EIxgjIq.png'),
(2759, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 426395825', '', 0, 565, '(95)8878917', '', 4, 1, 1, '16344062150', '/dinamix/email/aupwsVJWQODAdbSwMnal.png'),
(2760, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 616464640', ' ltek-trans04', 0, 565, '(95)5911245', '(63)7338115', 4, 4, 1, '16344062150', '/dinamix/email/Gnh9lvbdFmEqDoZRjZOd.png'),
(2761, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 592484968', '', 0, 565, '(95)1229393', '', 4, 1, 1, '16344062150', '/dinamix/email/0hDYsnJJz7iirT8nhUaW.png'),
(2762, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 274435932', ' ltek-trans05', 0, 565, '(50)3285641', '(50)3261604', 4, 4, 1, '16344062150', '/dinamix/email/LTqzOMd2gYEWF9w63ecA.png'),
(2763, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 577045828', '', 0, 565, '(99)0979801', '', 4, 1, 1, '16344062150', '/dinamix/email/aupwsVJWQODAdbSwMnal.png'),
(2764, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 225492105', '', 0, 565, '(66)8534883', '(44)2233144', 4, 4, 1, '16344062150', '/dinamix/email/0hDYsnJJz7iirT8nhUaW.png'),
(2765, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 303125142', ' ltek-trans', 0, 565, '(50)4759383', '(67)6417341', 4, 4, 1, '16344062150', '/dinamix/email/Gnh9lvbdFmEqDoZRjZOd.png'),
(2766, 'hash', '2016-04-21 10:07:04', '', 'approved', '0.00', ' 583692003', ' Stas_mgr8', 0, 565, '(50)3483688', '(99)9045025', 4, 4, 1, '16344062150', '/dinamix/email/lQ3MXJUPBEAH8EIxgjIq.png'),
(2767, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(99)2712327', '', 4, 1, 1, '11300265366', '/dinamix/email/7C5cTzDByBnHmhpks9DK.png'),
(2768, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(98)3047390', '(50)9867144', 4, 4, 1, '11300265366', '/dinamix/email/6yROiBmYArkOS6M8WUNG.png'),
(2769, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', ' alexandr.gl.1', 0, 566, '(99)2712327', '(97)7075965', 4, 4, 1, '11300265366', '/dinamix/email/tQgiRccspcPI9sH8vW4m.png'),
(2770, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(97)4952282', '(50)5464606', 4, 4, 1, '11300265366', '/dinamix/email/fsbS1wr1WPgwVro604Qx.png'),
(2771, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', ' 633887040', '', 0, 566, '(50)1757490', '(68)4888661', 4, 4, 1, '11300265366', '/dinamix/email/uOvDXJLbxDMOUTpRoga1.png'),
(2772, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(98)2016427', '(63)4033654', 4, 4, 1, '11300265366', ''),
(2773, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(50)9844193', '(97)6879474', 4, 4, 1, '11300265366', '/dinamix/email/8sowOrAEsydruVAs9O1D.png'),
(2774, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(50)2100875', '(96)7766991', 4, 4, 1, '11300265366', '/dinamix/email/yTztaASWacjOBHt80u9l.png'),
(2775, 'hash', '2016-04-21 10:07:09', '', 'approved', '0.00', '', '', 0, 566, '(98)0711005', '(63)9596619', 4, 4, 1, '11300265366', '/dinamix/email/vDvz7u87zWSOE7c7YdaT.png'),
(2776, 'hash', '2016-04-21 10:07:13', '', 'approved', '0.00', '', '', 0, 567, '(93)4529446', '', 4, 1, 1, '18110110608', '/dinamix/email/TJNsQaZGB5xxXYd7j3qn.png'),
(2777, 'hash', '2016-04-21 10:07:15', '', 'approved', '0.00', ' 457393454', ' igor_evanwijk', 0, 568, '(067)3728699', '', 4, 1, 1, '14673608091', ''),
(2778, 'hash', '2016-04-21 10:07:15', '', 'approved', '0.00', '', '', 0, 568, '(067)3728699', '', 3, 1, 1, '14673608091', ''),
(2779, 'hash', '2016-04-21 10:07:15', '', 'approved', '0.00', ' 297665553', ' SkelDry', 0, 568, '(67)3400839', '', 4, 1, 1, '14673608091', '/dinamix/email/KHWosEJDJkm2Ondrm8Yh.png'),
(2780, 'hash', '2016-04-21 10:07:15', '', 'approved', '0.00', '', ' marushchak_olha', 0, 568, '(98)7951127', '', 4, 1, 1, '14673608091', '/dinamix/email/ckbGPVBRbBoB2WZKORTC.png'),
(2781, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 471760003', '', 0, 569, '(32)2452222', '', 4, 1, 1, '13088150332', ''),
(2782, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 584821458', '', 0, 569, '(99)7892191', '(67)6755935', 4, 4, 1, '13088150332', '/dinamix/email/OGyyojchNr9gCVNCb1DV.png'),
(2783, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', ' Galmod_Andriy', 0, 569, '(32)2675682', '(32)2458506', 4, 4, 1, '13088150332', '/dinamix/email/I6UaKg2PmSInehbNscsi.png'),
(2784, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(32)2458844', '(67)7339617', 4, 4, 1, '13088150332', '/dinamix/email/4Hct82YJRgyyYz16rFYz.png'),
(2785, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 672825794', ' Galmod(Bogdan)', 0, 569, '(67)3725008', '', 4, 1, 1, '13088150332', '/dinamix/email/ILPqjJ2jdn5JVj65UPsv.png'),
(2786, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 625279209', ' Syenya83', 0, 569, '(66)2328612', '(32)2441111', 4, 4, 1, '13088150332', '/dinamix/email/raSRbQPeoadcrXvUJohv.png'),
(2787, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 363293864', '', 0, 569, '(322)675753', '(322)458510', 4, 4, 1, '13088150332', '/dinamix/email/BmAatCZRuS5gDbbFyAbb.png'),
(2788, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(32)2443333', '(67)1812206', 4, 4, 1, '13088150332', '/dinamix/email/f1lPmmp5ruiUlxo2X0pQ.png'),
(2789, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 697790435', ' Nazariy-1', 0, 569, '(32)2441444', '(50)4313100', 4, 4, 1, '13088150332', '/dinamix/email/XLSET3nKeOLzUu4WAMpZ.png'),
(2790, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(97)7770878', '(67)9630376', 4, 4, 1, '13088150332', '/dinamix/email/KEL2uPuzHvuJKnUOWJmd.png'),
(2791, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 595366647', ' nadin.osychka', 0, 569, '(67)1579408', '(97)3425278', 4, 4, 1, '13088150332', '/dinamix/email/Hr4xICOi7afjkQDAPr49.png'),
(2792, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(32)2458507', '(67)2747627', 4, 4, 1, '13088150332', ''),
(2793, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(32)2458787', '(97)6244033', 4, 4, 1, '13088150332', ''),
(2794, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 660261491', ' galmod.taras', 0, 569, '(98)0650568', '(99)3146798', 4, 4, 1, '13088150332', '/dinamix/email/JkA45GGkcNrjYl0nSfdc.png'),
(2795, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', '', '', 0, 569, '(322)452222', '(322)458545', 4, 4, 1, '13088150332', '/dinamix/email/fqq3reKL2v4cXXf6FkYv.png'),
(2796, 'hash', '2016-04-21 10:07:21', '', 'approved', '0.00', ' 698942550', ' galmodyuriy', 0, 569, '(63)4543120', '(67)3724993', 4, 4, 1, '13088150332', '/dinamix/email/3thETv2rAN3Chmn76xbO.png'),
(2797, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(63)5812369', '(67)7560839', 4, 4, 1, '12987497357', '/dinamix/email/PPfEnJBQ.png'),
(2798, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(67)9156304', '', 4, 1, 1, '12987497357', ''),
(2799, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(66)7834617', '', 4, 1, 1, '12987497357', ''),
(2800, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(67)7560839', '(63)5812369', 4, 4, 1, '12987497357', ''),
(2801, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(63)1307533', '', 4, 1, 1, '12987497357', '/dinamix/email/PPfEnJBQ.png'),
(2802, 'hash', '2016-04-21 10:07:23', '', 'approved', '0.00', '', '', 0, 570, '(67)7560839', '', 4, 1, 1, '12987497357', ''),
(2803, 'hash', '2016-04-21 10:07:26', '', 'approved', '0.00', '', ' ignanko88', 0, 571, '(67)7598379', '(99)2329622', 4, 4, 1, '14595744565', '/dinamix/email/cZMwKQeRZorGr3qTJXVR.png'),
(2804, 'hash', '2016-04-21 10:07:29', '', 'approved', '0.00', ' 680221679', ' gruzim.gruz1', 0, 572, '(50)2801473', '(67)1475992', 4, 4, 1, '19913205951', '/dinamix/email/GsQvoFpqM3BtlfyIkQXP.png'),
(2805, 'hash', '2016-04-21 10:07:29', '', 'approved', '0.00', '', ' lukyanchyck', 0, 572, '(97)4983347', '(93)6407449', 4, 4, 1, '19913205951', ''),
(2806, 'hash', '2016-04-21 10:07:31', '', 'approved', '0.00', '', ' klerck3', 0, 573, '(98)7041741', '(99)0136551', 4, 4, 1, '12481252144', ''),
(2807, 'hash', '2016-04-21 10:07:31', '', 'approved', '0.00', '', '', 0, 573, '(99)0136551', '', 4, 1, 1, '12481252144', ''),
(2808, 'hash', '2016-04-21 10:07:31', '', 'approved', '0.00', '', '', 0, 573, '(95)7663473', '', 4, 1, 1, '12481252144', ''),
(2809, 'hash', '2016-04-21 10:07:34', '', 'approved', '0.00', '', '', 0, 574, '(99)0187721', '(93)4925101', 4, 4, 1, '19133452203', ''),
(2810, 'hash', '2016-04-21 10:07:41', '', 'approved', '0.00', '', '', 0, 575, '(44)2292882', '(50)3564957', 4, 4, 1, '19123062989', '/dinamix/email/4G5GgRtF.png'),
(2811, 'hash', '2016-04-21 10:07:41', '', 'approved', '0.00', '', '', 0, 575, '(67)2306282', '', 4, 1, 1, '19123062989', ''),
(2812, 'hash', '2016-04-21 10:07:41', '', 'approved', '0.00', '', '', 0, 575, '(067)2504233', '', 3, 1, 1, '19123062989', ''),
(2813, 'hash', '2016-04-21 10:07:41', '', 'approved', '0.00', '', '', 0, 575, '(96)9754354', '(44)3530563', 4, 4, 1, '19123062989', '/dinamix/email/z3NG71GBHmoF42DXBDga.png'),
(2814, 'hash', '2016-04-21 10:07:44', '', 'approved', '0.00', ' 273002384', '', 0, 577, '(352)524418', '(67)3515396', 4, 4, 1, '10751107121', '/dinamix/email/aiEzcTPv2Ois2hktJpDm.png'),
(2815, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(96)7913429', '', 4, 1, 1, '17442233284', ''),
(2816, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(68)8817212', '(99)7273668', 4, 4, 1, '17442233284', '/dinamix/email/i3d229aOaQAsNeeYyN63.png'),
(2817, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(93)8260043', '(99)4195264', 4, 4, 1, '17442233284', '/dinamix/email/ZdxLDxyHqjfC06EEc0kL.png'),
(2818, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(98)2400481', '(63)6781313', 4, 4, 1, '17442233284', ''),
(2819, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(96)6631969', '(63)4072406', 4, 4, 1, '17442233284', '/dinamix/email/uWRN1K99if1K81Cq5Egs.png'),
(2820, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(96)8598076', '(63)5287897', 4, 4, 1, '17442233284', '/dinamix/email/JVSHSjVMlhivfHhGkddC.png'),
(2821, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(96)7842687', '(93)2177058', 4, 4, 1, '17442233284', '/dinamix/email/NWZf0BkbuzbShY65z4wb.png'),
(2822, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', ' chsv54', 0, 578, '(68)1976718', '(93)4141818', 4, 4, 1, '17442233284', '/dinamix/email/i9CtfBEsBKYbDE4EdyNZ.png'),
(2823, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', ' tontschykcom', 0, 578, '(99)0794246', '(96)1910993', 4, 4, 1, '17442233284', '/dinamix/email/cB1BMXymUfMP37S4IiNz.png'),
(2824, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(98)3354853', '(99)4117653', 4, 4, 1, '17442233284', ''),
(2825, 'hash', '2016-04-21 10:09:12', '', 'approved', '0.00', '', '', 0, 578, '(96)6856331', '(66)4765597', 4, 4, 1, '17442233284', ''),
(2826, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(66)4012041', '', 4, 1, 1, '14664374883', '/dinamix/email/2e8d8JJ2EQSLpdUuLSuz.png'),
(2827, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(67)1816026', '(66)1872257', 4, 4, 1, '14664374883', ''),
(2828, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(66)9810894', '(98)3813977', 4, 4, 1, '14664374883', ''),
(2829, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(067)9239685', '(066)0433798', 3, 3, 1, '14664374883', ''),
(2830, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(50)6795030', '(96)3226902', 4, 4, 1, '14664374883', ''),
(2831, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(97)3168248', '(66)8141281', 4, 4, 1, '14664374883', ''),
(2832, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(95)4696387', '(98)3837213', 4, 4, 1, '14664374883', ''),
(2833, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(99)6240056', '(96)0325706', 4, 4, 1, '14664374883', ''),
(2834, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(96)1246553', '(50)6172923', 4, 4, 1, '14664374883', ''),
(2835, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(96)4153712', '(99)7996966', 4, 4, 1, '14664374883', ''),
(2836, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(97)6863163', '(99)4480145', 4, 4, 1, '14664374883', ''),
(2837, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(66)8248438', '(98)3856154', 4, 4, 1, '14664374883', ''),
(2838, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(66)4012041', '(93)3443192', 4, 4, 1, '14664374883', ''),
(2839, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(99)9186919', '(67)3754147', 4, 4, 1, '14664374883', ''),
(2840, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(98)3814201', '(66)6302496', 4, 4, 1, '14664374883', ''),
(2841, 'hash', '2016-04-21 10:09:17', '', 'approved', '0.00', '', '', 0, 579, '(97)3168218', '(95)1037792', 4, 4, 1, '14664374883', ''),
(2842, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', '', 0, 580, '(32)2404540', '(32)2404541', 4, 4, 1, '13111119758', ''),
(2843, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' vprystajko', 0, 580, '(67)3737052', '', 4, 1, 1, '13111119758', '/dinamix/email/Q3BhbmtwSg9lHvB2r7gK.png'),
(2844, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 653687363', ' igorbumka', 0, 580, '(67)6722343', '(32)2404540', 4, 4, 1, '13111119758', '/dinamix/email/HiulsJw4vAOkwZPPwvit.png'),
(2845, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' bogdan892706', 0, 580, '(67)3234859', '', 4, 1, 1, '13111119758', '/dinamix/email/7YPnYN5B8txhRgVEIfoK.png'),
(2846, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 677385229', ' Demsky_V', 0, 580, '(67)6722059', '', 4, 1, 1, '13111119758', '/dinamix/email/a4ipGRlNFLB7ATsQhH7f.png'),
(2847, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 569412185', ' Відділ внутрішніх перевезень', 0, 580, '(32)2404540', '(67)2776372', 4, 4, 1, '13111119758', '/dinamix/email/syYsDN96kf4yvBmuUDAQ.png'),
(2848, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 335804176', ' autotransexpedition', 0, 580, '(322)400202', '(50)3704688', 4, 4, 1, '13111119758', '/dinamix/email/m2hIT1iYYFStHPuMgOUy.png'),
(2849, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 648613690', ' vendetta14651', 0, 580, '(32)2970533', '(32)2404540', 4, 4, 1, '13111119758', '/dinamix/email/YDku8ZYJRQ052PlwjtKX.png'),
(2850, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', '', 0, 580, '(99)1005215', '(32)2404540', 4, 4, 1, '13111119758', ''),
(2851, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' moskvytyn', 0, 580, '(67)3737062', '', 4, 1, 1, '13111119758', '/dinamix/email/NE28E0bhQwpWnIadRCRD.png'),
(2852, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' olja.m7', 0, 580, '(67)6724189', '(32)2404540', 4, 4, 1, '13111119758', '/dinamix/email/vju47T8PjsmG4JuDfC8T.png'),
(2853, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' shevchykt', 0, 580, '(67)3240681', '(32)2404540', 4, 4, 1, '13111119758', '/dinamix/email/ZjxXSSrNChMVYOUCI3u4.png'),
(2854, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', ' 497851435', ' veremchukyb', 0, 580, '(32)2404540', '(67)7006276', 4, 4, 1, '13111119758', ''),
(2855, 'hash', '2016-04-21 10:09:20', '', 'approved', '0.00', '', ' makahaka', 0, 580, '(67)3225079', '(32)2404540', 4, 4, 1, '13111119758', '/dinamix/email/C28lpXzgsvIeBK2aQsaY.png'),
(2856, 'hash', '2016-04-21 10:10:04', '', 'approved', '0.00', '', '', 0, 581, '(67)5724090', '', 4, 1, 1, '14537154683', ''),
(2857, 'hash', '2016-04-21 10:10:04', '', 'approved', '0.00', '', '', 0, 581, '(67)5724090', '', 4, 1, 1, '14537154683', '/dinamix/email/CurleGvlGh60zhC6TcHW.png'),
(2858, 'hash', '2016-04-21 10:10:08', '', 'approved', '0.00', '', ' materiorserg', 0, 582, '(93)7532229', '(95)3743096', 4, 4, 1, '16715608108', ''),
(2859, 'hash', '2016-04-21 10:10:08', '', 'approved', '0.00', '', '', 0, 582, '(98)2102502', '', 4, 1, 1, '16715608108', '/dinamix/email/AtHUzXUeijvzJAdMImfw.png'),
(2860, 'hash', '2016-04-21 10:10:08', '', 'approved', '0.00', '', '', 0, 582, '(093)2812901', '', 3, 1, 1, '16715608108', ''),
(2861, 'hash', '2016-04-21 10:10:08', '', 'approved', '0.00', '', ' materiorserg', 0, 582, '(63)8796294', '', 4, 1, 1, '16715608108', ''),
(2862, 'hash', '2016-04-21 10:10:10', '', 'approved', '0.00', '', '', 0, 583, '(67)4676507', '(63)6649641', 4, 4, 1, '10180066691', ''),
(2863, 'hash', '2016-04-21 10:10:15', '', 'approved', '0.00', '', '', 0, 584, '(63)7382877', '(96)8410858', 4, 4, 1, '15036313792', ''),
(2864, 'hash', '2016-04-21 10:10:15', '', 'approved', '0.00', '', '', 0, 584, '(97)4878842', '(95)1672817', 4, 4, 1, '15036313792', '/dinamix/email/v2r6AiH45B8hbwlcaSa7.png'),
(2865, 'hash', '2016-04-21 10:10:15', '', 'approved', '0.00', '', '', 0, 584, '(97)4878842', '', 4, 1, 1, '15036313792', '/dinamix/email/4mi5UGuSoYYZnwE95fTB.png'),
(2866, 'hash', '2016-04-21 10:10:15', '', 'approved', '0.00', '', '', 0, 584, '(97)8105586', '(95)1838292', 4, 4, 1, '15036313792', '/dinamix/email/ycgi6RyX3G3dClrypFPX.png'),
(2867, 'hash', '2016-04-21 10:10:17', '', 'approved', '0.00', '', '', 0, 585, '(6264)60780', '', 4, 1, 1, '14177818384', ''),
(2868, 'hash', '2016-04-21 10:10:17', '', 'approved', '0.00', '', '', 0, 585, '(098)1059584', '(98)1059584', 4, 4, 1, '14177818384', '/dinamix/email/RBB8jRtfG9biLuH5Fzgy.png'),
(2869, 'hash', '2016-04-21 10:10:17', '', 'approved', '0.00', '', '', 0, 585, '(000)0000', '', 15, 1, 1, '14177818384', ''),
(2870, 'hash', '2016-04-21 10:10:20', '', 'approved', '0.00', '', '', 0, 586, '(0612)428669', '(50)4849902', 4, 4, 1, '14863705106', '/dinamix/email/PeIELiqRWFnJBLt2raQH.png'),
(2871, 'hash', '2016-04-21 10:10:20', '', 'approved', '0.00', '', '', 0, 586, '(95)4273878', '(63)8539558', 4, 4, 1, '14863705106', '/dinamix/email/2idpmSExPxVEq6JSMtiv.png'),
(2872, 'hash', '2016-04-21 10:10:20', '', 'approved', '0.00', '', '', 0, 586, '(66)9081551', '(67)5623910', 4, 4, 1, '14863705106', '/dinamix/email/D9xt8Vo0uurKwFsTIBlb.png'),
(2873, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(532)566550', '(50)3460916', 4, 4, 1, '11078851773', ''),
(2874, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(68)2576694', '', 4, 1, 1, '11078851773', ''),
(2875, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(50)1014349', '', 4, 1, 1, '11078851773', ''),
(2876, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(66)3824564', '(97)6354069', 4, 4, 1, '11078851773', ''),
(2877, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', ' lenapoltava5', 0, 587, '(95)5066828', '(68)1663421', 4, 4, 1, '11078851773', ''),
(2878, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(50)3460916', '', 4, 1, 1, '11078851773', ''),
(2879, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(50)9009605', '(97)1612973', 4, 4, 1, '11078851773', '/dinamix/email/jsUdJei3ZCjQ2FtyrU8d.png'),
(2880, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', ' 570-599-124', '', 0, 587, '(50)5540756', '', 4, 1, 1, '11078851773', '/dinamix/email/rwwJr2GtXyyXL5V2VbYb.png'),
(2881, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(66)3198900', '(67)3182941', 4, 4, 1, '11078851773', ''),
(2882, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(50)3041924', '', 4, 1, 1, '11078851773', ''),
(2883, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(50)4050535', '', 4, 1, 1, '11078851773', ''),
(2884, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', ' 659507996', ' svetlana_777*', 0, 587, '(66)0062746', '(50)4075100', 4, 4, 1, '11078851773', '/dinamix/email/guFYZqSsXQxHiK8bbd2z.png'),
(2885, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', '', 0, 587, '(99)5332217', '(67)5006641', 4, 4, 1, '11078851773', ''),
(2886, 'hash', '2016-04-21 10:10:23', '', 'approved', '0.00', '', ' elona113', 0, 587, '(50)4040815', '(98)4181228', 4, 4, 1, '11078851773', '/dinamix/email/rklGh5VHt8JNLiIyvdpu.png'),
(2887, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)4737686', '', 4, 1, 1, '10901441703', '/dinamix/email/68EFewOrxr2y9KJfNltq.png'),
(2888, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)5087608', '', 4, 1, 1, '10901441703', ''),
(2889, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(95)3468158', '(67)3814899', 4, 4, 1, '10901441703', ''),
(2890, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)4737686', '', 4, 1, 1, '10901441703', '/dinamix/email/C23RQKMjskdi6dA1RW02.png'),
(2891, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)4737684', '', 4, 1, 1, '10901441703', '/dinamix/email/mFFaHWWbwzHuZXfOSqCD.png'),
(2892, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)2096640', '', 4, 1, 1, '10901441703', ''),
(2893, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(97)1872355', '', 4, 1, 1, '10901441703', ''),
(2894, 'hash', '2016-04-21 10:10:26', '', 'approved', '0.00', '', '', 0, 588, '(67)3814833', '', 4, 1, 1, '10901441703', ''),
(2895, 'hash', '2016-04-21 10:10:43', '', 'approved', '0.00', '', '', 0, 589, '(066)9173760', '', 4, 1, 1, '13845397656', '/dinamix/email/v9DVBvQl.png'),
(2896, 'hash', '2016-04-21 10:10:43', '', 'approved', '0.00', '', '', 0, 589, '(066)5279398', '(098)1231250', 4, 4, 1, '13845397656', '/dinamix/email/o5w0js9IZ9h6wqH7uV5T.png'),
(2897, 'hash', '2016-04-21 10:10:43', '', 'approved', '0.00', '', '', 0, 589, '(97)3631063', '(95)7324826', 4, 4, 1, '13845397656', ''),
(2898, 'hash', '2016-04-21 10:10:43', '', 'approved', '0.00', '', '', 0, 589, '(096)0414683', '', 4, 1, 1, '13845397656', ''),
(2899, 'hash', '2016-04-21 10:10:43', '', 'approved', '0.00', '', '', 0, 589, '(098)0358762', '', 3, 1, 1, '13845397656', ''),
(2900, 'hash', '2016-04-21 10:10:45', '', 'approved', '0.00', '', '', 0, 590, '(96)3188892', '(99)4024895', 4, 4, 1, '12886309826', '/dinamix/email/V5s9oora.png'),
(2901, 'hash', '2016-04-21 10:10:45', '', 'approved', '0.00', '', ' serg7083', 0, 590, '(50)9898041', '', 4, 1, 1, '12886309826', '/dinamix/email/YF3jtSNeUefQj6HzrnRM.png'),
(2902, 'hash', '2016-04-21 10:10:46', '', 'approved', '0.00', '', '', 0, 591, '(57)7136821', '(67)5378706', 4, 4, 1, '18647577521', '/dinamix/email/cz9TKWmnT5JBaiZYTieh.png'),
(2903, 'hash', '2016-04-21 10:10:46', '', 'approved', '0.00', '', '', 0, 591, '(99)7128431', '', 4, 1, 1, '18647577521', ''),
(2904, 'hash', '2016-04-21 10:10:46', '', 'approved', '0.00', '', '', 0, 591, '(50)4860809', '', 4, 1, 1, '18647577521', '/dinamix/email/jVDYWIcDVry20BgnTdLg.png'),
(2905, 'hash', '2016-04-21 10:10:46', '', 'approved', '0.00', '', '', 0, 591, '(50)3259479', '', 4, 1, 1, '18647577521', '/dinamix/email/U9vi9fE2gO1nasymALsu.png'),
(2906, 'hash', '2016-04-21 10:10:46', '', 'approved', '0.00', '', '', 0, 591, '(50)3053295', '', 4, 1, 1, '18647577521', '/dinamix/email/5xOBsqxhyYIn9Ho8eVDy.png'),
(2907, 'hash', '2016-04-21 10:10:48', '', 'approved', '0.00', '', '', 0, 592, '(57)7154210', '(67)5780204', 4, 4, 1, '14463534714', ''),
(2908, 'hash', '2016-04-21 10:10:48', '', 'approved', '0.00', ' 465469518', '', 0, 592, '(50)8399817', '(96)9346113', 4, 4, 1, '14463534714', '/dinamix/email/k79JjaoLIvpgAF2bxUzr.png'),
(2909, 'hash', '2016-04-21 10:10:48', '', 'approved', '0.00', '', '', 0, 592, '(67)5461808', '(50)4015583', 4, 4, 1, '14463534714', ''),
(2910, 'hash', '2016-04-21 10:10:48', '', 'approved', '0.00', '', '', 0, 592, '(50)4015582', '(57)7154602', 4, 4, 1, '14463534714', ''),
(2911, 'hash', '2016-04-21 10:10:52', '', 'approved', '0.00', '', '', 0, 593, '(542)655295', '(98)7490357', 4, 4, 1, '13425887963', ''),
(2912, 'hash', '2016-04-21 10:10:52', '', 'approved', '0.00', '', '', 0, 593, '(0542)659849', '(050)9319902', 4, 4, 1, '13425887963', ''),
(2913, 'hash', '2016-04-21 10:10:52', '', 'approved', '0.00', '', '', 0, 593, '(50)9319902', '', 4, 1, 1, '13425887963', ''),
(2914, 'hash', '2016-04-21 10:10:52', '', 'approved', '0.00', '', '', 0, 593, '(50)8379976', '', 4, 1, 1, '13425887963', ''),
(2915, 'hash', '2016-04-21 10:10:57', '', 'approved', '0.00', '', '', 0, 594, '(5692)63187', '(67)9543904', 4, 4, 1, '14580910205', ''),
(2916, 'hash', '2016-04-21 10:11:00', '', 'approved', '0.00', '', '', 0, 595, '(44)2234712', '(67)5002978', 4, 4, 1, '11843524387', '/dinamix/email/MHfcekn5.png'),
(2917, 'hash', '2016-04-21 10:11:00', '', 'approved', '0.00', '', '', 0, 595, '(67)2356065', '', 4, 1, 1, '11843524387', '/dinamix/email/MHfcekn5.png'),
(2918, 'hash', '2016-04-21 10:11:00', '', 'approved', '0.00', '', '', 0, 595, '(67)5002978', '', 4, 1, 1, '11843524387', '/dinamix/email/MHfcekn5.png'),
(2919, 'hash', '2016-04-21 10:11:01', '', 'approved', '0.00', '', '', 0, 596, '(67)2598095', '(95)0750717', 4, 4, 1, '19377862229', ''),
(2920, 'hash', '2016-04-21 10:11:03', '', 'approved', '0.00', '', '', 0, 597, '(542)362156', '(50)8454247', 4, 4, 1, '16470139609', '/dinamix/email/YTyESTYzx33NzJd4qDQJ.png'),
(2921, 'hash', '2016-04-21 10:11:03', '', 'approved', '0.00', '', '', 0, 597, '(066)0488764', '', 4, 1, 1, '16470139609', '');
INSERT INTO `Frontend_Users` (`ID`, `hash`, `registered`, `email`, `emailApproved`, `walletBalance`, `icq`, `skype`, `currentRating`, `companyID`, `phone`, `phoneStationary`, `phoneCodeID`, `phoneStationaryCodeID`, `primaryLocaleID`, `parser`, `parserMail`) VALUES
(2922, 'hash', '2016-04-21 10:11:06', '', 'approved', '0.00', '', '', 0, 598, '(66)7803490', '(98)8095665', 4, 4, 1, '16903931767', '/dinamix/email/gPBnVM17tzE9dsK31yGF.png'),
(2923, 'hash', '2016-04-21 10:11:11', '', 'approved', '0.00', '', '', 0, 599, '(552)381191', '(50)3964414', 4, 4, 1, '16458901539', ''),
(2924, 'hash', '2016-04-21 10:11:11', '', 'approved', '0.00', '', '', 0, 599, '(552)381191', '(552)381190', 4, 4, 1, '16458901539', '/dinamix/email/jND4JRFmNLenzKOhYZHF.png'),
(2925, 'hash', '2016-04-21 10:11:11', '', 'approved', '0.00', '', '', 0, 599, '(552)381191', '(552)381190', 4, 4, 1, '16458901539', '/dinamix/email/LwFcxciAIpfrdSI8jsLd.png'),
(2926, 'hash', '2016-04-21 10:11:11', '', 'approved', '0.00', '', '', 0, 599, '(050)3805819', '', 4, 1, 1, '16458901539', ''),
(2927, 'hash', '2016-04-21 10:11:15', '', 'approved', '0.00', '', ' merkanta-office', 0, 600, '(95)4202202', '(50)3180747', 4, 4, 1, '14227084703', '/dinamix/email/g1iAarkATt4KTZDoOXKI.png'),
(2928, 'hash', '2016-04-21 10:11:15', '', 'approved', '0.00', '', ' merkanta-office', 0, 600, '(95)4202202', '(512)443364', 4, 4, 1, '14227084703', '/dinamix/email/v23bE2qWNecRicxglfIc.png'),
(2929, 'hash', '2016-04-21 10:11:15', '', 'approved', '0.00', '', '', 0, 600, '(50)3180747', '(512)496430', 4, 4, 1, '14227084703', ''),
(2930, 'hash', '2016-04-21 10:11:17', '', 'approved', '0.00', '', '', 0, 601, '(067)5620877', '', 4, 1, 1, '18711222342', ''),
(2931, 'hash', '2016-04-21 10:11:18', '', 'approved', '0.00', '', '', 0, 601, '(67)5620877', '(98)4210090', 4, 4, 1, '18711222342', ''),
(2932, 'hash', '2016-04-21 10:11:23', '', 'approved', '0.00', '', '', 0, 602, '(50)5224720', '(97)7065976', 4, 4, 1, '12143937601', '/dinamix/email/rvIspiVFfvn9AZqIZB82.png'),
(2933, 'hash', '2016-04-21 10:11:23', '', 'approved', '0.00', '', '', 0, 602, '(066)4318529', '(098)7462569', 3, 3, 1, '12143937601', ''),
(2934, 'hash', '2016-04-21 10:11:23', '', 'approved', '0.00', '', '', 0, 602, '(50)5224720', '(97)7065976', 4, 4, 1, '12143937601', ''),
(2935, 'hash', '2016-04-21 10:11:23', '', 'approved', '0.00', '', '', 0, 602, '(50)5152379', '(96)89429958', 4, 4, 1, '12143937601', '/dinamix/email/DIs3aTx6ip3IaIEVOFdc.png'),
(2936, 'hash', '2016-04-21 10:11:27', '', 'approved', '0.00', '', '', 0, 603, '(67)3931699', '', 4, 1, 1, '15384071299', '/dinamix/email/wJEyy02x.png'),
(2937, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(432)351738', '(63)6899318', 4, 4, 1, '15236564029', ''),
(2938, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(68)8501642', '(93)1174547', 4, 4, 1, '15236564029', ''),
(2939, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(67)4312587', '', 4, 1, 1, '15236564029', ''),
(2940, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(098)6199324', '(063)6899318', 3, 3, 1, '15236564029', '/dinamix/email/VDKpOYU1Lw33lqVyQSLm.png'),
(2941, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(97)3964042', '', 4, 1, 1, '15236564029', ''),
(2942, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(63)0408899', '(67)7214169', 4, 4, 1, '15236564029', ''),
(2943, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(067)7960292', '', 4, 1, 1, '15236564029', ''),
(2944, 'hash', '2016-04-21 10:11:28', '', 'approved', '0.00', '', '', 0, 604, '(67)2638076', '', 4, 1, 1, '15236564029', ''),
(2945, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', '', '', 0, 605, '(44)4965568', '', 4, 1, 1, '13591746077', '/dinamix/email/GIazAv4DN3QqZIeaZLc5.png'),
(2946, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', ' 388542428', ' gabriella-lis', 0, 605, '(50)3302368', '', 4, 1, 1, '13591746077', '/dinamix/email/sS3OvxXtruUZxGTCHuQh.png'),
(2947, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', '', ' Natalia-lis', 0, 605, '(050)4652448', '', 3, 1, 1, '13591746077', '/dinamix/email/Lk9R5mTALinTMDTf4WaT.png'),
(2948, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', ' 496623996', ' sergey.lis1', 0, 605, '(44)4965568', '(050)3872200', 4, 4, 1, '13591746077', '/dinamix/email/CG6Q49AT30z5VuBTb09p.png'),
(2949, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', ' 330392290', ' toma-lis', 0, 605, '(50)3879009', '(44)4925609', 4, 4, 1, '13591746077', '/dinamix/email/QhEsJLOXxh2DrA02DHBT.png'),
(2950, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', '', ' tamara.ak2', 0, 605, '(095)8638499', '(050)3875577', 4, 4, 1, '13591746077', ''),
(2951, 'hash', '2016-04-21 10:11:30', '', 'approved', '0.00', '', ' lis-legion-yana', 0, 605, '(50)3874488', '', 4, 1, 1, '13591746077', '/dinamix/email/yDPcH3zNFh45llEo99Ij.png'),
(2952, 'hash', '2016-04-21 10:11:34', '', 'approved', '0.00', '', '', 0, 606, '(66)7555163', '(67)6912143', 4, 4, 1, '13640749829', ''),
(2953, 'hash', '2016-04-21 10:11:34', '', 'approved', '0.00', '', ' valysha711', 0, 606, '(67)6912143', '', 4, 1, 1, '13640749829', '/dinamix/email/fg1ClGNWrcJ5smOddPEX.png'),
(2954, 'hash', '2016-04-21 10:11:34', '', 'approved', '0.00', '', '', 0, 606, '(66)7555163', '(68)5975845', 4, 4, 1, '13640749829', ''),
(2955, 'hash', '2016-04-21 10:11:52', '', 'approved', '0.00', '', '', 0, 607, '(96)5445777', '(99)2062553', 4, 4, 1, '10441504446', ''),
(2956, 'hash', '2016-04-21 10:11:52', '', 'approved', '0.00', '', '', 0, 607, '(096)5445777', '(099)2062553', 4, 4, 1, '10441504446', ''),
(2957, 'hash', '2016-04-21 10:11:52', '', 'approved', '0.00', '', '', 0, 607, '(93)9295812', '(67)9500084', 4, 4, 1, '10441504446', ''),
(2958, 'hash', '2016-04-21 10:11:52', '', 'approved', '0.00', '', '', 0, 607, '(66)2289549', '(98)4310463', 4, 4, 1, '10441504446', ''),
(2959, 'hash', '2016-04-21 10:12:15', '', 'approved', '0.00', '', '', 0, 608, '(98)5009219', '', 4, 1, 1, '11408070643', ''),
(2960, 'hash', '2016-04-21 10:12:15', '', 'approved', '0.00', '', '', 0, 608, '(98)5009219', '', 4, 1, 1, '11408070643', ''),
(2961, 'hash', '2016-04-21 10:12:15', '', 'approved', '0.00', '', ' andreyts111', 0, 608, '(67)3130877', '', 4, 1, 1, '11408070643', '/dinamix/email/N8N2QzgEwtoIchk5QfCb.png'),
(2962, 'hash', '2016-04-21 10:12:15', '', 'approved', '0.00', '', '', 0, 608, '(096)8588681', '', 4, 1, 1, '11408070643', ''),
(2963, 'hash', '2016-04-21 10:12:15', '', 'approved', '0.00', '', '', 0, 608, '(67)6797375', '', 4, 1, 1, '11408070643', '/dinamix/email/NMaFyYiwTFuQ3eLhbPgs.png'),
(2964, 'hash', '2016-04-21 10:12:17', '', 'approved', '0.00', '', '', 0, 609, '(6452)90386', '(6452)90388', 4, 4, 1, '15053212382', ''),
(2965, 'hash', '2016-04-21 10:12:17', '', 'approved', '0.00', '', '', 0, 609, '(50)4753546', '(97)0842760', 4, 4, 1, '15053212382', '/dinamix/email/aCtjbjHaCeEhOYfoSu79.png'),
(2966, 'hash', '2016-04-21 10:12:17', '', 'approved', '0.00', '', '', 0, 609, '(50)4754026', '', 4, 1, 1, '15053212382', '/dinamix/email/ah9ssXnbVjK1EgSQVPdc.png'),
(2967, 'hash', '2016-04-21 10:12:17', '', 'approved', '0.00', '', ' rooman141', 0, 609, '(50)4227125', '', 4, 1, 1, '15053212382', '/dinamix/email/6NO8uzu7dqycGW1wctwL.png'),
(2968, 'hash', '2016-04-21 10:12:21', '', 'approved', '0.00', ' 590657929', ' sv-yulia', 0, 610, '(66)5049199', '(99)7332622', 4, 4, 1, '13284564630', ''),
(2969, 'hash', '2016-04-21 10:12:21', '', 'approved', '0.00', '', '', 0, 610, '(97)8877834', '(50)9098931', 4, 4, 1, '13284564630', '/dinamix/email/7JhqpN66tzoSs5BMJX0j.png'),
(2970, 'hash', '2016-04-21 10:12:21', '', 'approved', '0.00', '', '', 0, 610, '(66)5049199', '(68)3199733', 4, 4, 1, '13284564630', ''),
(2971, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(352)284262', '(50)1022736', 4, 4, 1, '15396376986', ''),
(2972, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(642)495855', '(96)9757956', 4, 4, 1, '15396376986', ''),
(2973, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(67)2577373', '', 4, 1, 1, '15396376986', ''),
(2974, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(352)284262', '(67)2080459', 4, 4, 1, '15396376986', ''),
(2975, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(67)3708737', '(95)0819377', 4, 4, 1, '15396376986', ''),
(2976, 'hash', '2016-04-21 10:12:23', '', 'approved', '0.00', '', '', 0, 611, '(67)2550282', '', 4, 1, 1, '15396376986', ''),
(2977, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', ' katalina-k911', 0, 612, '(66)6415106', '(63)4982759', 4, 4, 1, '11017204054', '/dinamix/email/86DqHDbGheVE7Q1FT4aZ.png'),
(2978, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', ' pribylov1984', 0, 612, '(63)4989892', '(50)6450163', 4, 4, 1, '11017204054', '/dinamix/email/gEj4K6ECpBSwDpxqAZoA.png'),
(2979, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', '', 0, 612, '(98)0702253', '(63)7477752', 4, 4, 1, '11017204054', '/dinamix/email/6XY4XdNBahdzFFt3DP0A.png'),
(2980, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', '', 0, 612, '(97)5406552', '(93)2186403', 4, 4, 1, '11017204054', '/dinamix/email/dIlJiW4gON5ZSOQV7Jsc.png'),
(2981, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', '', 0, 612, '(95)1302263', '(95)1302263', 4, 4, 1, '11017204054', ''),
(2982, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', '', 0, 612, '(99)2621328', '', 4, 1, 1, '11017204054', '/dinamix/email/Zc5SH05L4PNSuxxuMroP.png'),
(2983, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', ' Simontovskaya', 0, 612, '(66)1533297', '', 4, 1, 1, '11017204054', '/dinamix/email/aycQcoYFAgufrAFBNTns.png'),
(2984, 'hash', '2016-04-21 10:12:27', '', 'approved', '0.00', '', '', 0, 612, '(67)1525978', '(93)9577569', 4, 4, 1, '11017204054', '/dinamix/email/CqckseYcZAYJDsCbc6Db.png'),
(2985, 'hash', '2016-04-21 10:12:29', '', 'approved', '0.00', '', ' ulianna6125', 0, 613, '(97)7029821', '(66)1883531', 4, 4, 1, '16152231589', ''),
(2986, 'hash', '2016-04-21 10:12:29', '', 'approved', '0.00', '', ' ulianna6125', 0, 613, '(96)5923088', '', 4, 1, 1, '16152231589', '/dinamix/email/agvkg58cbcTedxgh1YQL.png'),
(2987, 'hash', '2016-04-21 10:12:29', '', 'approved', '0.00', '', ' ulianna6125', 0, 613, '(66)1883531', '(93)9984516', 4, 4, 1, '16152231589', '/dinamix/email/agvkg58cbcTedxgh1YQL.png'),
(2988, 'hash', '2016-04-21 10:12:29', '', 'approved', '0.00', '', '', 0, 613, '(097)7029821', '', 3, 1, 1, '16152231589', ''),
(2989, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(57)7395169', '', 4, 1, 1, '15573647496', '/dinamix/email/7DQXzQ0C.png'),
(2990, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' aleksandra_prays1', 0, 614, '(99)0016370', '(67)5750690', 4, 4, 1, '15573647496', '/dinamix/email/7DQXzQ0C.png'),
(2991, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' dem1_god', 0, 614, '(99)3778201', '(67)1453400', 4, 4, 1, '15573647496', '/dinamix/email/Jgw47vGnBi4mWtp2yTKo.png'),
(2992, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(95)6933574', '(67)5708038', 4, 4, 1, '15573647496', ''),
(2993, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' ane4ka2357', 0, 614, '(95)3238075', '(63)3278045', 4, 4, 1, '15573647496', '/dinamix/email/GtxierYmB08JV4lcW5sp.png'),
(2994, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' Kotakhimoto', 0, 614, '(67)6336341', '(99)2911292', 4, 4, 1, '15573647496', '/dinamix/email/v1OoFSXGmOhbKw2mqzBc.png'),
(2995, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' Bygas543', 0, 614, '(66)0469198', '(57)7395169', 4, 4, 1, '15573647496', '/dinamix/email/LMvynoWJM4bX9Vjsyzm4.png'),
(2996, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(68)7957631', '(99)7539713', 4, 4, 1, '15573647496', ''),
(2997, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' kalita1233', 0, 614, '(66)8067879', '(97)7472373', 4, 4, 1, '15573647496', '/dinamix/email/lDJfvN6pvmzsTrTd3OBP.png'),
(2998, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' dashaaaa777', 0, 614, '(67)5789994', '(66)6701751', 4, 4, 1, '15573647496', '/dinamix/email/3NAmZC7J40GumRFEGtCN.png'),
(2999, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' hoz.twins', 0, 614, '(50)3257327', '(98)3418644', 4, 4, 1, '15573647496', '/dinamix/email/fw7eU7T0LmKAMQvZuGOV.png'),
(3000, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(93)7518206', '(98)9628843', 4, 4, 1, '15573647496', '/dinamix/email/0Lz4gn3eCSQW67CqfUx5.png'),
(3001, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' olja230705', 0, 614, '(95)8058578', '(67)5746064', 4, 4, 1, '15573647496', '/dinamix/email/CFgqp7zbqhkofgOR9pFb.png'),
(3002, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', ' 377109952', '', 0, 614, '(50)6224751', '(63)1144329', 4, 4, 1, '15573647496', '/dinamix/email/f8dHC2n5tNpbOeUNYL2p.png'),
(3003, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(68)3499503', '(99)2111878', 4, 4, 1, '15573647496', ''),
(3004, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' tania.h79', 0, 614, '(93)4690337', '(66)0419730', 4, 4, 1, '15573647496', '/dinamix/email/WMlShZjUY0bHYbawV4Nr.png'),
(3005, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' Jora-Grek', 0, 614, '(93)8901030', '(95)9387343', 4, 4, 1, '15573647496', '/dinamix/email/f9IsSVjnKPLr6guCNMem.png'),
(3006, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', ' vasiliy_logist', 0, 614, '(95)6718059', '(67)5753556', 4, 4, 1, '15573647496', '/dinamix/email/7DQXzQ0C.png'),
(3007, 'hash', '2016-04-21 10:12:42', '', 'approved', '0.00', '', '', 0, 614, '(57)7395169', '(50)9368231', 4, 4, 1, '15573647496', '/dinamix/email/r4BQEkrpJDFB87rYjEXg.png'),
(3008, 'hash', '2016-04-21 10:12:58', '', 'approved', '0.00', '', '', 0, 615, '(98)7409742', '', 4, 1, 1, '15381867702', '/dinamix/email/nm7WOeQV.png'),
(3009, 'hash', '2016-04-21 10:12:58', '', 'approved', '0.00', '', ' alenka.alekseenko1', 0, 615, '(67)8261928', '', 4, 1, 1, '15381867702', '/dinamix/email/OY6ElClkZAxEo76jpCpw.png'),
(3010, 'hash', '2016-04-21 10:12:58', '', 'approved', '0.00', '', '', 0, 615, '(67)8281980', '', 4, 1, 1, '15381867702', '/dinamix/email/j6d60gM7VlnQjIH2Tj3d.png'),
(3011, 'hash', '2016-04-21 10:12:58', '', 'approved', '0.00', '', '', 0, 615, '(67)8286838', '', 4, 1, 1, '15381867702', '/dinamix/email/0ZVf8xZDGfSfiJjfLbvV.png'),
(3012, 'hash', '2016-04-21 10:12:59', '', 'approved', '0.00', '', '', 0, 616, '(97)5076190', '(99)1552117', 4, 4, 1, '19935299006', ''),
(3013, 'hash', '2016-04-21 10:12:59', '', 'approved', '0.00', '', '', 0, 616, '(67)1377540', '', 4, 1, 1, '19935299006', ''),
(3014, 'hash', '2016-04-21 10:12:59', '', 'approved', '0.00', '', '', 0, 616, '(97)7375034', '', 4, 1, 1, '19935299006', ''),
(3015, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', ' 217026670', '', 0, 617, '(57)2520032', '', 4, 1, 1, '15914860088', ''),
(3016, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', ' 217026670', ' alex_polshin', 0, 617, '(66)7496644', '(572)520032', 4, 4, 1, '15914860088', '/dinamix/email/n84P5N3WBfs7YYbU7MgW.png'),
(3017, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', ' rmc9000', 0, 617, '(98)5513081', '(93)4176807', 4, 4, 1, '15914860088', '/dinamix/email/leDD22mNbNGbp2xKpbFd.png'),
(3018, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(98)4654650', '(50)9460825', 4, 4, 1, '15914860088', ''),
(3019, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(95)0418208', '(67)5744244', 4, 4, 1, '15914860088', '/dinamix/email/JKrNHmM05NJ4pOEmGBq7.png'),
(3020, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(95)7296336', '(68)8167737', 4, 4, 1, '15914860088', '/dinamix/email/QvVF5ELpYAW1kzlMmpXg.png'),
(3021, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(67)5741760', '', 4, 1, 1, '15914860088', ''),
(3022, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(97)5597737', '(66)3117114', 4, 4, 1, '15914860088', '/dinamix/email/Ha4nTyVwicko4qOtkr9b.png'),
(3023, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', ' sergej.jarowoj', 0, 617, '(67)8367777', '(67)8367777', 4, 4, 1, '15914860088', '/dinamix/email/ViKoff2msnW7PJ0HveBD.png'),
(3024, 'hash', '2016-04-21 10:13:06', '', 'approved', '0.00', '', '', 0, 617, '(66)7851129', '(572)520032', 4, 4, 1, '15914860088', '/dinamix/email/rFonPo1U1Xll5u6maar5.png'),
(3025, 'hash', '2016-04-21 10:13:08', '', 'approved', '0.00', '', '', 0, 618, '(67)1599788', '(50)9565733', 4, 4, 1, '14702276735', '/dinamix/email/9F511SY5BRP0oipVvEJ2.png'),
(3026, 'hash', '2016-04-21 10:13:11', '', 'approved', '0.00', '', '', 0, 619, '(97)3161427', '(50)3216933', 4, 4, 1, '10423171371', '/dinamix/email/XGoLGVJAkGHf1n8QIoJ3.png'),
(3027, 'hash', '2016-04-21 10:13:11', '', 'approved', '0.00', '', '', 0, 619, '(98)0194868', '(98)0194868', 4, 4, 1, '10423171371', ''),
(3028, 'hash', '2016-04-21 10:13:13', '', 'approved', '0.00', '', ' CARRIER', 0, 620, '(67)3092424', '(67)4706414', 4, 4, 1, '19136029533', '/dinamix/email/dwGkqe0r.png'),
(3029, 'hash', '2016-04-21 10:13:22', '', 'approved', '0.00', '', '', 0, 621, '(50)3035974', '(96)9496727', 4, 4, 1, '15961886997', '/dinamix/email/n0Z7elPBYyjFArJkTU9w.png'),
(3030, 'hash', '2016-04-21 10:13:22', '', 'approved', '0.00', '', '', 0, 621, '(542)780674', '(99)7433065', 4, 4, 1, '15961886997', ''),
(3031, 'hash', '2016-04-21 10:13:22', '', 'approved', '0.00', '', ' vladimir-1970k', 0, 621, '(542)780674', '(99)7433065', 4, 4, 1, '15961886997', '/dinamix/email/n0Z7elPBYyjFArJkTU9w.png'),
(3032, 'hash', '2016-04-21 10:13:26', '', 'approved', '0.00', ' 361363396', ' roman_0860', 0, 622, '(66)4002182', '(67)1464801', 4, 4, 1, '14865489614', '/dinamix/email/ZUId9FP0.png'),
(3033, 'hash', '2016-04-21 10:13:26', '', 'approved', '0.00', '', '', 0, 622, '(97)9729652', '(63)8084303', 4, 4, 1, '14865489614', '/dinamix/email/sWFrQWH7ViUFtDJhde3j.png'),
(3034, 'hash', '2016-04-21 10:13:28', '', 'approved', '0.00', '', '', 0, 623, '(50)7671316', '(67)6282436', 4, 4, 1, '15538706102', '/dinamix/email/jEhgDLzh.png'),
(3035, 'hash', '2016-04-21 10:13:28', '', 'approved', '0.00', '', '', 0, 623, '(095)5591390', '', 4, 1, 1, '15538706102', ''),
(3036, 'hash', '2016-04-21 10:13:28', '', 'approved', '0.00', '', '', 0, 623, '(67)6282436', '(50)7671316', 4, 4, 1, '15538706102', ''),
(3037, 'hash', '2016-04-21 10:13:28', '', 'approved', '0.00', '', '', 0, 623, '(67)6282436', '(50)7671316', 4, 4, 1, '15538706102', ''),
(3038, 'hash', '2016-04-21 10:13:28', '', 'approved', '0.00', '', '', 0, 623, '(95)0453744', '', 4, 1, 1, '15538706102', ''),
(3039, 'hash', '2016-04-21 10:13:29', '', 'approved', '0.00', '', '', 0, 624, '(68)7168405', '', 4, 1, 1, '16267916753', '/dinamix/email/prLyled1oREeHduEIctM.png'),
(3040, 'hash', '2016-04-21 10:13:31', '', 'approved', '0.00', '', ' zlyka000', 0, 625, '(99)3773331', '(68)5223272', 4, 4, 1, '17951594117', '/dinamix/email/0nGYfANLjMvlr4hJ9SGU.png'),
(3041, 'hash', '2016-04-21 10:13:31', '', 'approved', '0.00', '', '', 0, 625, '(96)1863037', '(68)0221068', 4, 4, 1, '17951594117', ''),
(3042, 'hash', '2016-04-21 10:13:32', '', 'approved', '0.00', '', '', 0, 626, '(5136)27111', '(5136)24845', 4, 4, 1, '12101202399', '/dinamix/email/V7M6Lt9U.png'),
(3043, 'hash', '2016-04-21 10:13:32', '', 'approved', '0.00', '', '', 0, 626, '(50)5636990', '', 4, 1, 1, '12101202399', ''),
(3044, 'hash', '2016-04-21 10:13:32', '', 'approved', '0.00', '', '', 0, 626, '(067)5119626', '', 3, 1, 1, '12101202399', ''),
(3045, 'hash', '2016-04-21 10:13:32', '', 'approved', '0.00', '', '', 0, 626, '(50)3181120', '', 4, 1, 1, '12101202399', ''),
(3046, 'hash', '2016-04-21 10:13:34', '', 'approved', '0.00', '', '', 0, 627, '(57)7174918', '(50)3035048', 4, 4, 1, '15787546232', ''),
(3047, 'hash', '2016-04-21 10:13:34', '', 'approved', '0.00', '', ' BezsonovaVera', 0, 627, '(50)6132832', '', 4, 1, 1, '15787546232', '/dinamix/email/P4y6KaKdKOSjB0CprbF4.png'),
(3048, 'hash', '2016-04-21 10:13:34', '', 'approved', '0.00', '', '', 0, 627, '(50)3035048', '', 4, 1, 1, '15787546232', '/dinamix/email/Xx8UbjH5HzW9KOSNsXR2.png'),
(3049, 'hash', '2016-04-21 10:13:34', '', 'approved', '0.00', '', '', 0, 627, '(95)5216163', '(97)5458689', 4, 4, 1, '15787546232', '/dinamix/email/QER7vO5GPuJeZVzkqI8r.png'),
(3050, 'hash', '2016-04-21 10:13:34', '', 'approved', '0.00', '', '', 0, 627, '(50)7366060', '', 4, 1, 1, '15787546232', ''),
(3051, 'hash', '2016-04-21 10:13:37', '', 'approved', '0.00', '', ' polipak.kr', 0, 628, '(50)5789482', '(56)4400881', 4, 4, 1, '15478140947', '/dinamix/email/vvBvdBs7jhAdlBO3uJ2B.png'),
(3052, 'hash', '2016-04-21 10:13:37', '', 'approved', '0.00', '', '', 0, 628, '(67)9295614', '(95)9046567', 4, 4, 1, '15478140947', ''),
(3053, 'hash', '2016-04-21 10:13:37', '', 'approved', '0.00', '', ' polipak.kr', 0, 628, '(98)4325952', '(50)6709235', 4, 4, 1, '15478140947', '/dinamix/email/vvBvdBs7jhAdlBO3uJ2B.png'),
(3054, 'hash', '2016-04-21 10:13:37', '', 'approved', '0.00', '', ' miria2014', 0, 628, '(50)4543344', '', 4, 1, 1, '15478140947', ''),
(3055, 'hash', '2016-04-21 10:13:37', '', 'approved', '0.00', '', '', 0, 628, '(67)1665935', '(564)642541', 4, 4, 1, '15478140947', ''),
(3056, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(50)1582734', '', 4, 1, 1, '11278769373', '/dinamix/email/M31Ntu3Rrp2azBWnAr9c.png'),
(3057, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(67)4369117', '', 4, 1, 1, '11278769373', ''),
(3058, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(50)5693428', '', 4, 1, 1, '11278769373', '/dinamix/email/U95qfRxPITXarugyl738.png'),
(3059, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(68)6861808', '', 4, 1, 1, '11278769373', ''),
(3060, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', ' logist_123', 0, 629, '(50)5021866', '', 4, 1, 1, '11278769373', '/dinamix/email/lfZrt4lwXeVtaHci2AVi.png'),
(3061, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(95)2442849', '', 4, 1, 1, '11278769373', '/dinamix/email/SfnYFTDoYEo7Y59BScv6.png'),
(3062, 'hash', '2016-04-21 10:13:43', '', 'approved', '0.00', '', '', 0, 629, '(050)4305065', '', 4, 1, 1, '11278769373', '/dinamix/email/6MI9b4GHmnXl3R1IXof5.png'),
(3064, 'e10adc3949ba59abbe56e057f20f883e', '2016-04-21 11:25:51', 'makkartnis@gmail.com', 'approved', '0.00', '7381287', 'makkartnis', 0, 631, '0934527685', '7687678687', 3, 2, 1, NULL, NULL),
(3066, 'ab908685ade82cb87bc1688da067d34e', '2016-04-21 11:52:34', 'shef@transinfo.com.ua', 'approved', '0.00', '', '', 0, 633, '0503397530', '', 3, 1, 1, NULL, NULL),
(3067, 'cc03e747a6afbbcbf8be7668acfebee5', '2016-04-21 11:57:36', 'attilasoftwaretest@gmail.com', 'approved', '0.00', NULL, NULL, 0, 634, '', '', 14, 1, 1, NULL, NULL),
(3068, 'cc03e747a6afbbcbf8be7668acfebee5', '2016-04-21 12:02:13', 'rikalsky@gmail.com', 'approved', '0.00', NULL, NULL, 0, 635, '', '', 3, 1, 1, NULL, NULL),
(3073, 'bab970a9c7f730c0a6a0cbc0495f22e8', '2016-04-21 13:59:25', 'igor.kushil@gmail.com', 'approved', '0.00', '', '', 0, 640, '673320398', '', 4, 1, 1, NULL, NULL);

--
-- Тригери `Frontend_Users`
--
DELIMITER $$
CREATE TRIGGER `check_user_company` AFTER UPDATE ON `Frontend_Users`
 FOR EACH ROW BEGIN
	IF NEW.companyID <> OLD.companyID THEN  
		    DELETE FROM Frontend_User_to_Company_Ownership WHERE
            userID = NEW.ID AND companyID = OLD.companyID;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Cars`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Cars` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `number` varchar(45) DEFAULT NULL,
  `info` text,
  `volume` decimal(12,3) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) unsigned DEFAULT NULL,
  `sizeY` decimal(12,3) unsigned DEFAULT NULL,
  `sizeZ` decimal(12,3) unsigned DEFAULT NULL,
  `liftingCapacity` decimal(12,3) unsigned DEFAULT NULL,
  `carTypeID` int(10) unsigned NOT NULL,
  `type` enum('vantazhivka','napiv prychip','z prychipom') NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_Cars`
--

INSERT INTO `Frontend_User_Cars` (`ID`, `userID`, `number`, `info`, `volume`, `sizeX`, `sizeY`, `sizeZ`, `liftingCapacity`, `carTypeID`, `type`, `status`) VALUES
(121, 2639, NULL, NULL, '86.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(122, 2640, NULL, NULL, NULL, NULL, NULL, NULL, '2.000', 1, 'vantazhivka', 'active'),
(123, 2643, NULL, NULL, '105.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(124, 2645, NULL, NULL, '47.000', NULL, NULL, NULL, '5.000', 1, 'vantazhivka', 'active'),
(125, 2803, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(126, 2804, NULL, NULL, '86.000', NULL, NULL, NULL, '25.000', 1, 'vantazhivka', 'active'),
(127, 2806, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(128, 2809, NULL, NULL, NULL, NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(129, 2810, NULL, NULL, '20.000', NULL, NULL, NULL, '1.700', 1, 'vantazhivka', 'active'),
(130, 2814, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(131, 2895, NULL, NULL, '35.000', NULL, NULL, NULL, '5.000', 1, 'vantazhivka', 'active'),
(132, 2900, NULL, NULL, '47.000', NULL, NULL, NULL, '7.000', 1, 'vantazhivka', 'active'),
(133, 2902, NULL, NULL, '40.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(134, 2907, NULL, NULL, '105.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(135, 2911, NULL, NULL, '45.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(136, 2915, NULL, NULL, '42.000', NULL, NULL, NULL, '8.000', 1, 'vantazhivka', 'active'),
(137, 2916, NULL, NULL, '100.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(138, 2919, NULL, NULL, '50.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(139, 2920, NULL, NULL, '15.000', NULL, NULL, NULL, '2.700', 1, 'vantazhivka', 'active'),
(140, 2922, NULL, NULL, '42.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(141, 2923, NULL, NULL, '92.000', NULL, NULL, NULL, '21.000', 1, 'vantazhivka', 'active'),
(142, 2927, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(143, 2930, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(144, 2932, NULL, NULL, '50.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(145, 2936, NULL, NULL, '93.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(146, 2937, NULL, NULL, NULL, NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(147, 2945, NULL, NULL, '96.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(148, 2952, NULL, NULL, '90.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(149, 3015, NULL, NULL, '56.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(150, 3025, NULL, NULL, '86.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(151, 3026, NULL, NULL, '50.000', NULL, NULL, NULL, '40.000', 1, 'vantazhivka', 'active'),
(152, 3028, NULL, NULL, '60.000', NULL, NULL, NULL, '14.000', 1, 'vantazhivka', 'active'),
(153, 3029, NULL, NULL, '86.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(154, 3032, NULL, NULL, '30.000', NULL, NULL, NULL, '5.500', 1, 'vantazhivka', 'active'),
(155, 3034, NULL, NULL, '92.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(156, 3039, NULL, NULL, NULL, NULL, NULL, NULL, '30.000', 1, 'vantazhivka', 'active'),
(157, 3040, NULL, NULL, NULL, NULL, NULL, NULL, '5.000', 1, 'vantazhivka', 'active'),
(158, 3042, NULL, NULL, '95.000', NULL, NULL, NULL, '22.000', 1, 'vantazhivka', 'active'),
(159, 3046, NULL, NULL, '90.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(160, 3051, NULL, NULL, '110.000', NULL, NULL, NULL, '20.000', 1, 'vantazhivka', 'active'),
(161, 3056, NULL, NULL, '45.000', NULL, NULL, NULL, '10.000', 1, 'vantazhivka', 'active'),
(163, 3064, NULL, NULL, '20.000', '20.000', '30.000', '10.000', '10.000', 3, 'vantazhivka', 'active'),
(164, 3068, NULL, NULL, '86.000', '2.500', '2.700', '13.600', '21.000', 3, 'napiv prychip', 'active'),
(165, 3067, NULL, NULL, '33.000', '33.000', '33.000', '33.000', '12.000', 10, 'napiv prychip', 'active'),
(166, 3068, NULL, NULL, '88.000', '3.000', '3.000', '13.000', '20.000', 3, 'napiv prychip', 'active');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Car_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Car_Types` (
  `ID` int(10) unsigned NOT NULL,
  `localeName` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_Car_Types`
--

INSERT INTO `Frontend_User_Car_Types` (`ID`, `localeName`, `name`) VALUES
(1, 'tent', 'Тент'),
(2, 'izoterm', 'Ізотерм'),
(3, 'refrigerator', 'Рефрижератор'),
(4, 'krytaya', 'Крытая'),
(5, 'konteyner', 'Контейнер'),
(6, 'ref', 'Реф.'),
(7, 'lesovoz', 'Лесовоз'),
(8, 'cel-nomet', 'Цельномет'),
(9, 'izoterm', 'Изотерм'),
(10, 'samosval', 'Самосвал'),
(11, 'konteynerovoz', 'Контейнеровоз'),
(12, 'bortovaya', 'Бортовая'),
(13, 'bus', 'Бус'),
(14, 'zernovoz', 'Зерновоз'),
(15, 'tral-negabarit', 'Трал / Негабарит'),
(16, 'otkrytaya', 'Открытая'),
(17, 'cisterna', 'Цистерна'),
(18, 'platforma', 'Платформа');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6105 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_Locales`
--

INSERT INTO `Frontend_User_Locales` (`ID`, `name`, `localeID`, `userID`) VALUES
(5176, 'ФЛ-П Брода Е.Н.Екатерина', 1, 2604),
(5177, 'ФЛ-П Брода Е.Н.Екатерина', 2, 2604),
(5178, 'Катя 2', 1, 2605),
(5179, 'Катя 2', 2, 2605),
(5180, 'ТОВ "ДААР Логiстик Компанi"Сергей Александрович', 1, 2606),
(5181, 'ТОВ "ДААР Логiстик Компанi"Сергей Александрович', 2, 2606),
(5182, 'Андрей', 1, 2607),
(5183, 'Андрей', 2, 2607),
(5184, 'Наташа', 1, 2608),
(5185, 'Наташа', 2, 2608),
(5186, 'Сергей', 1, 2609),
(5187, 'Сергей', 2, 2609),
(5188, 'ФЛ-П Слобожан О.А.Евгений Барисович', 1, 2610),
(5189, 'ФЛ-П Слобожан О.А.Евгений Барисович', 2, 2610),
(5190, 'Женя', 1, 2611),
(5191, 'Женя', 2, 2611),
(5192, 'ООО "ТРАНС КЛАСС ЛТД"ТРАНС КЛАСС', 1, 2612),
(5193, 'ООО "ТРАНС КЛАСС ЛТД"ТРАНС КЛАСС', 2, 2612),
(5194, 'Влад', 1, 2613),
(5195, 'Влад', 2, 2613),
(5196, 'Денис', 1, 2614),
(5197, 'Денис', 2, 2614),
(5198, 'Елизавета', 1, 2615),
(5199, 'Елизавета', 2, 2615),
(5200, 'Женя', 1, 2616),
(5201, 'Женя', 2, 2616),
(5202, 'Зоя', 1, 2617),
(5203, 'Зоя', 2, 2617),
(5204, 'Ирина Сергеевна', 1, 2618),
(5205, 'Ирина Сергеевна', 2, 2618),
(5206, 'Наталия', 1, 2619),
(5207, 'Наталия', 2, 2619),
(5208, 'Наталья', 1, 2620),
(5209, 'Наталья', 2, 2620),
(5210, 'Олег', 1, 2621),
(5211, 'Олег', 2, 2621),
(5212, 'Ольга', 1, 2622),
(5213, 'Ольга', 2, 2622),
(5214, 'Роман', 1, 2623),
(5215, 'Роман', 2, 2623),
(5216, 'Светлана', 1, 2624),
(5217, 'Светлана', 2, 2624),
(5218, 'Таня', 1, 2625),
(5219, 'Таня', 2, 2625),
(5220, 'Татьяна', 1, 2626),
(5221, 'Татьяна', 2, 2626),
(5222, 'Яна', 1, 2627),
(5223, 'Яна', 2, 2627),
(5224, 'Александр', 1, 2628),
(5225, 'Александр', 2, 2628),
(5226, 'Дима', 1, 2629),
(5227, 'Дима', 2, 2629),
(5228, 'Ирина', 1, 2630),
(5229, 'Ирина', 2, 2630),
(5230, 'Юлия Владимировна', 1, 2631),
(5231, 'Юлия Владимировна', 2, 2631),
(5232, 'Юля', 1, 2632),
(5233, 'Юля', 2, 2632),
(5234, 'ООО "ОТП плюс"Литвинов И.А.', 1, 2633),
(5235, 'ООО "ОТП плюс"Литвинов И.А.', 2, 2633),
(5236, 'Андрей', 1, 2634),
(5237, 'Андрей', 2, 2634),
(5238, 'Денис', 1, 2635),
(5239, 'Денис', 2, 2635),
(5240, 'Дмитрий', 1, 2636),
(5241, 'Дмитрий', 2, 2636),
(5242, 'Елена', 1, 2637),
(5243, 'Елена', 2, 2637),
(5244, 'Света', 1, 2638),
(5245, 'Света', 2, 2638),
(5246, 'ФОП Ребанов Игорь ЕвгеньевичЛана Григорьевна', 1, 2639),
(5247, 'ФОП Ребанов Игорь ЕвгеньевичЛана Григорьевна', 2, 2639),
(5248, 'ФЛП Ситняк А.В.Анатолий', 1, 2640),
(5249, 'ФЛП Ситняк А.В.Анатолий', 2, 2640),
(5250, 'Анатолий', 1, 2641),
(5251, 'Анатолий', 2, 2641),
(5252, 'Вера', 1, 2642),
(5253, 'Вера', 2, 2642),
(5254, 'ЧП Фирма "ПЛАСТ"Наталья', 1, 2643),
(5255, 'ЧП Фирма "ПЛАСТ"Наталья', 2, 2643),
(5256, 'Наталья', 1, 2644),
(5257, 'Наталья', 2, 2644),
(5258, 'ООО "Транспортная компания"Имидж"Вячеслав, Иван', 1, 2645),
(5259, 'ООО "Транспортная компания"Имидж"Вячеслав, Иван', 2, 2645),
(5260, 'Вячеслав', 1, 2646),
(5261, 'Вячеслав', 2, 2646),
(5262, 'Иван', 1, 2647),
(5263, 'Иван', 2, 2647),
(5264, 'Лариса бухгалтерБухгалтер', 1, 2648),
(5265, 'Лариса бухгалтерБухгалтер', 2, 2648),
(5266, 'ФЛП Турубар Р.В.Петр Владимирович', 1, 2649),
(5267, 'ФЛП Турубар Р.В.Петр Владимирович', 2, 2649),
(5268, 'ЧП Косяк О. А.Елена', 1, 2650),
(5269, 'ЧП Косяк О. А.Елена', 2, 2650),
(5270, 'ТОВ "М ЕНД С - ЗАХІД"Василий', 1, 2651),
(5271, 'ТОВ "М ЕНД С - ЗАХІД"Василий', 2, 2651),
(5272, 'Александр', 1, 2652),
(5273, 'Александр', 2, 2652),
(5274, 'Григорий', 1, 2653),
(5275, 'Григорий', 2, 2653),
(5276, 'Дмитрий', 1, 2654),
(5277, 'Дмитрий', 2, 2654),
(5278, 'Роман', 1, 2655),
(5279, 'Роман', 2, 2655),
(5280, 'ФЛП Сенюта Г.Б.Галина Борисовна', 1, 2656),
(5281, 'ФЛП Сенюта Г.Б.Галина Борисовна', 2, 2656),
(5282, 'АлександрМенеджер по организации автомобильных грузоперевозок.', 1, 2657),
(5283, 'АлександрМенеджер по организации автомобильных грузоперевозок.', 2, 2657),
(5284, 'ВикторМенеджер по организации автомобильных грузоперевозок.', 1, 2658),
(5285, 'ВикторМенеджер по организации автомобильных грузоперевозок.', 2, 2658),
(5286, 'ИринаМенеджер по организации автомобильных грузоперевозок.', 1, 2659),
(5287, 'ИринаМенеджер по организации автомобильных грузоперевозок.', 2, 2659),
(5288, 'СветланаМенеджер по организации автомобильных грузоперевозок.', 1, 2660),
(5289, 'СветланаМенеджер по организации автомобильных грузоперевозок.', 2, 2660),
(5290, 'ВикторияМенеджер по организации автомобильных грузоперевозок.', 1, 2661),
(5291, 'ВикторияМенеджер по организации автомобильных грузоперевозок.', 2, 2661),
(5292, 'Жанна+79234638876 - Viber', 1, 2662),
(5293, 'Жанна+79234638876 - Viber', 2, 2662),
(5294, 'МаринаМенеджер по организации автомобильных грузоперевозок.', 1, 2663),
(5295, 'МаринаМенеджер по организации автомобильных грузоперевозок.', 2, 2663),
(5296, 'Татьяна', 1, 2664),
(5297, 'Татьяна', 2, 2664),
(5298, 'ФОП Петренко Олена ВікторівнаЕлена', 1, 2665),
(5299, 'ФОП Петренко Олена ВікторівнаЕлена', 2, 2665),
(5300, 'Виктория', 1, 2666),
(5301, 'Виктория', 2, 2666),
(5302, 'Ирина', 1, 2667),
(5303, 'Ирина', 2, 2667),
(5304, 'Ирина Д.', 1, 2668),
(5305, 'Ирина Д.', 2, 2668),
(5306, 'Мария', 1, 2669),
(5307, 'Мария', 2, 2669),
(5308, 'Нина', 1, 2670),
(5309, 'Нина', 2, 2670),
(5310, 'Оксана Зинченко', 1, 2671),
(5311, 'Оксана Зинченко', 2, 2671),
(5312, 'Роман', 1, 2672),
(5313, 'Роман', 2, 2672),
(5314, 'ФОП Линник С.АЮлия', 1, 2673),
(5315, 'ФОП Линник С.АЮлия', 2, 2673),
(5316, 'Елена', 1, 2674),
(5317, 'Елена', 2, 2674),
(5318, 'Людмила', 1, 2675),
(5319, 'Людмила', 2, 2675),
(5320, 'Оксана', 1, 2676),
(5321, 'Оксана', 2, 2676),
(5322, 'Юлия', 1, 2677),
(5323, 'Юлия', 2, 2677),
(5324, 'ООО "Алтаир"Андрей', 1, 2678),
(5325, 'ООО "Алтаир"Андрей', 2, 2678),
(5326, 'Андрей', 1, 2679),
(5327, 'Андрей', 2, 2679),
(5328, 'ФО-П Шипша Р.И.Шипша Руслан', 1, 2680),
(5329, 'ФО-П Шипша Р.И.Шипша Руслан', 2, 2680),
(5330, 'Валентина', 1, 2681),
(5331, 'Валентина', 2, 2681),
(5332, 'Инна', 1, 2682),
(5333, 'Инна', 2, 2682),
(5334, 'Руслан', 1, 2683),
(5335, 'Руслан', 2, 2683),
(5336, 'ООО "Витал Спец Сервис"секретарь', 1, 2684),
(5337, 'ООО "Витал Спец Сервис"секретарь', 2, 2684),
(5338, 'Александр', 1, 2685),
(5339, 'Александр', 2, 2685),
(5340, 'Алексей', 1, 2686),
(5341, 'Алексей', 2, 2686),
(5342, 'Андрей', 1, 2687),
(5343, 'Андрей', 2, 2687),
(5344, 'Виталий', 1, 2688),
(5345, 'Виталий', 2, 2688),
(5346, 'Михаил', 1, 2689),
(5347, 'Михаил', 2, 2689),
(5348, 'Светлана', 1, 2690),
(5349, 'Светлана', 2, 2690),
(5350, 'Татьяна', 1, 2691),
(5351, 'Татьяна', 2, 2691),
(5352, 'ЧП Аква-СаббияВиктория', 1, 2692),
(5353, 'ЧП Аква-СаббияВиктория', 2, 2692),
(5354, 'Алла', 1, 2693),
(5355, 'Алла', 2, 2693),
(5356, 'Алла К', 1, 2694),
(5357, 'Алла К', 2, 2694),
(5358, 'Виктория', 1, 2695),
(5359, 'Виктория', 2, 2695),
(5360, 'Марина', 1, 2696),
(5361, 'Марина', 2, 2696),
(5362, 'Марина Р', 1, 2697),
(5363, 'Марина Р', 2, 2697),
(5364, 'Роман', 1, 2698),
(5365, 'Роман', 2, 2698),
(5366, 'Татьяна', 1, 2699),
(5367, 'Татьяна', 2, 2699),
(5368, 'ООО "Гарант Транс Плюс"Мороз Олег Иванович', 1, 2700),
(5369, 'ООО "Гарант Транс Плюс"Мороз Олег Иванович', 2, 2700),
(5370, 'Алина', 1, 2701),
(5371, 'Алина', 2, 2701),
(5372, 'Игорь', 1, 2702),
(5373, 'Игорь', 2, 2702),
(5374, 'Илья', 1, 2703),
(5375, 'Илья', 2, 2703),
(5376, 'Олег', 1, 2704),
(5377, 'Олег', 2, 2704),
(5378, 'Олеся', 1, 2705),
(5379, 'Олеся', 2, 2705),
(5380, 'Юрий', 1, 2706),
(5381, 'Юрий', 2, 2706),
(5382, 'ПП "ТД "МЕТАЛУРГ"Андрей', 1, 2707),
(5383, 'ПП "ТД "МЕТАЛУРГ"Андрей', 2, 2707),
(5384, 'Андрей', 1, 2708),
(5385, 'Андрей', 2, 2708),
(5386, 'ФЛП Малая Наталья ВалериевнаНаталья', 1, 2709),
(5387, 'ФЛП Малая Наталья ВалериевнаНаталья', 2, 2709),
(5388, 'Лана', 1, 2710),
(5389, 'Лана', 2, 2710),
(5390, 'Лена', 1, 2711),
(5391, 'Лена', 2, 2711),
(5392, 'Людмила', 1, 2712),
(5393, 'Людмила', 2, 2712),
(5394, 'Наталия', 1, 2713),
(5395, 'Наталия', 2, 2713),
(5396, 'Наталья', 1, 2714),
(5397, 'Наталья', 2, 2714),
(5398, 'Юлия', 1, 2715),
(5399, 'Юлия', 2, 2715),
(5400, 'Юрий', 1, 2716),
(5401, 'Юрий', 2, 2716),
(5402, 'ФОП Cулименко М.ВМарина', 1, 2717),
(5403, 'ФОП Cулименко М.ВМарина', 2, 2717),
(5404, 'Андрей', 1, 2718),
(5405, 'Андрей', 2, 2718),
(5406, 'Ирина', 1, 2719),
(5407, 'Ирина', 2, 2719),
(5408, 'Марина', 1, 2720),
(5409, 'Марина', 2, 2720),
(5410, 'Татьяна', 1, 2721),
(5411, 'Татьяна', 2, 2721),
(5412, 'ООО "РСФ "Ладо-Сервис"Юлия', 1, 2722),
(5413, 'ООО "РСФ "Ладо-Сервис"Юлия', 2, 2722),
(5414, 'Юлия', 1, 2723),
(5415, 'Юлия', 2, 2723),
(5416, 'Яна', 1, 2724),
(5417, 'Яна', 2, 2724),
(5418, 'ФО-П Карслиян О. К.Наталья', 1, 2725),
(5419, 'ФО-П Карслиян О. К.Наталья', 2, 2725),
(5420, 'Дмитрий', 1, 2726),
(5421, 'Дмитрий', 2, 2726),
(5422, 'Иван', 1, 2727),
(5423, 'Иван', 2, 2727),
(5424, 'Олександр', 1, 2728),
(5425, 'Олександр', 2, 2728),
(5426, 'ЧП Чмихун И.Н.Андрей', 1, 2729),
(5427, 'ЧП Чмихун И.Н.Андрей', 2, 2729),
(5428, 'ФЛ-П Лавренчук С.В.Светлана', 1, 2730),
(5429, 'ФЛ-П Лавренчук С.В.Светлана', 2, 2730),
(5430, 'Аня', 1, 2731),
(5431, 'Аня', 2, 2731),
(5432, 'Еленаviber +380(63)8133282', 1, 2732),
(5433, 'Еленаviber +380(63)8133282', 2, 2732),
(5434, 'Ирина', 1, 2733),
(5435, 'Ирина', 2, 2733),
(5436, 'Наталья', 1, 2734),
(5437, 'Наталья', 2, 2734),
(5438, 'Ангелина', 1, 2735),
(5439, 'Ангелина', 2, 2735),
(5440, 'Андрей', 1, 2736),
(5441, 'Андрей', 2, 2736),
(5442, 'Лера*', 1, 2737),
(5443, 'Лера*', 2, 2737),
(5444, 'Наталья*', 1, 2738),
(5445, 'Наталья*', 2, 2738),
(5446, 'ЧП "Экспохим"Дмитрий', 1, 2739),
(5447, 'ЧП "Экспохим"Дмитрий', 2, 2739),
(5448, 'Анна', 1, 2740),
(5449, 'Анна', 2, 2740),
(5450, 'Вита', 1, 2741),
(5451, 'Вита', 2, 2741),
(5452, 'Денис', 1, 2742),
(5453, 'Денис', 2, 2742),
(5454, 'Дмитрий', 1, 2743),
(5455, 'Дмитрий', 2, 2743),
(5456, 'Лариса', 1, 2744),
(5457, 'Лариса', 2, 2744),
(5458, 'Ольга', 1, 2745),
(5459, 'Ольга', 2, 2745),
(5460, 'Усенко Татьяна', 1, 2746),
(5461, 'Усенко Татьяна', 2, 2746),
(5462, 'ООО "МИРОТРАНС"Mirotrans Ltd.', 1, 2747),
(5463, 'ООО "МИРОТРАНС"Mirotrans Ltd.', 2, 2747),
(5464, 'АлександраРуководитель отдела', 1, 2748),
(5465, 'АлександраРуководитель отдела', 2, 2748),
(5466, 'Виталий', 1, 2749),
(5467, 'Виталий', 2, 2749),
(5468, 'Игорь', 1, 2750),
(5469, 'Игорь', 2, 2750),
(5470, 'Михаил', 1, 2751),
(5471, 'Михаил', 2, 2751),
(5472, 'Юля', 1, 2752),
(5473, 'Юля', 2, 2752),
(5474, 'БориславБолгарское представительство', 1, 2753),
(5475, 'БориславБолгарское представительство', 2, 2753),
(5476, 'Артем', 1, 2754),
(5477, 'Артем', 2, 2754),
(5478, 'Алина', 1, 2755),
(5479, 'Алина', 2, 2755),
(5480, 'Жанна', 1, 2756),
(5481, 'Жанна', 2, 2756),
(5482, 'ООО "ЛТЭК-ТРАНС"ООО ЛТЭК-ТРАНС', 1, 2757),
(5483, 'ООО "ЛТЭК-ТРАНС"ООО ЛТЭК-ТРАНС', 2, 2757),
(5484, 'Александр', 1, 2758),
(5485, 'Александр', 2, 2758),
(5486, 'Алина', 1, 2759),
(5487, 'Алина', 2, 2759),
(5488, 'Артем', 1, 2760),
(5489, 'Артем', 2, 2760),
(5490, 'Инна', 1, 2761),
(5491, 'Инна', 2, 2761),
(5492, 'Ирина', 1, 2762),
(5493, 'Ирина', 2, 2762),
(5494, 'Оксана', 1, 2763),
(5495, 'Оксана', 2, 2763),
(5496, 'Олеся', 1, 2764),
(5497, 'Олеся', 2, 2764),
(5498, 'Оля', 1, 2765),
(5499, 'Оля', 2, 2765),
(5500, 'Станислав', 1, 2766),
(5501, 'Станислав', 2, 2766),
(5502, 'ООО "Экспресс Ком"Александр', 1, 2767),
(5503, 'ООО "Экспресс Ком"Александр', 2, 2767),
(5504, 'Cергей', 1, 2768),
(5505, 'Cергей', 2, 2768),
(5506, 'Александр', 1, 2769),
(5507, 'Александр', 2, 2769),
(5508, 'Андрей', 1, 2770),
(5509, 'Андрей', 2, 2770),
(5510, 'Виктория', 1, 2771),
(5511, 'Виктория', 2, 2771),
(5512, 'ЕЛЕНА', 1, 2772),
(5513, 'ЕЛЕНА', 2, 2772),
(5514, 'Елена', 1, 2773),
(5515, 'Елена', 2, 2773),
(5516, 'Ирина', 1, 2774),
(5517, 'Ирина', 2, 2774),
(5518, 'Наталья', 1, 2775),
(5519, 'Наталья', 2, 2775),
(5520, 'ФЛ-П Голубничий Алексей АндреевичВалерия', 1, 2776),
(5521, 'ФЛ-П Голубничий Алексей АндреевичВалерия', 2, 2776),
(5522, 'ФО-П Мельник І. С.Ігор', 1, 2777),
(5523, 'ФО-П Мельник І. С.Ігор', 2, 2777),
(5524, 'Ігор', 1, 2778),
(5525, 'Ігор', 2, 2778),
(5526, 'Андрій', 1, 2779),
(5527, 'Андрій', 2, 2779),
(5528, 'Ольга', 1, 2780),
(5529, 'Ольга', 2, 2780),
(5530, 'ООО "Галмод"Бухгалтерія (питання оплат)', 1, 2781),
(5531, 'ООО "Галмод"Бухгалтерія (питання оплат)', 2, 2781),
(5532, 'Анатолій', 1, 2782),
(5533, 'Анатолій', 2, 2782),
(5534, 'Андрій', 1, 2783),
(5535, 'Андрій', 2, 2783),
(5536, 'Богдан', 1, 2784),
(5537, 'Богдан', 2, 2784),
(5538, 'Богдан Іващук', 1, 2785),
(5539, 'Богдан Іващук', 2, 2785),
(5540, 'Василь', 1, 2786),
(5541, 'Василь', 2, 2786),
(5542, 'Володя,Олег', 1, 2787),
(5543, 'Володя,Олег', 2, 2787),
(5544, 'Дмитро', 1, 2788),
(5545, 'Дмитро', 2, 2788),
(5546, 'Назар', 1, 2789),
(5547, 'Назар', 2, 2789),
(5548, 'Наталя, Оксана', 1, 2790),
(5549, 'Наталя, Оксана', 2, 2790),
(5550, 'Оля, Надя', 1, 2791),
(5551, 'Оля, Надя', 2, 2791),
(5552, 'Роман', 1, 2792),
(5553, 'Роман', 2, 2792),
(5554, 'Роман Олива', 1, 2793),
(5555, 'Роман Олива', 2, 2793),
(5556, 'Тарас', 1, 2794),
(5557, 'Тарас', 2, 2794),
(5558, 'ТОВ Швидкий світ', 1, 2795),
(5559, 'ТОВ Швидкий світ', 2, 2795),
(5560, 'Юрій', 1, 2796),
(5561, 'Юрій', 2, 2796),
(5562, 'ЧП Мельничук О. В.Олег', 1, 2797),
(5563, 'ЧП Мельничук О. В.Олег', 2, 2797),
(5564, 'Алексей', 1, 2798),
(5565, 'Алексей', 2, 2798),
(5566, 'Иван', 1, 2799),
(5567, 'Иван', 2, 2799),
(5568, 'Олег', 1, 2800),
(5569, 'Олег', 2, 2800),
(5570, 'Ольга', 1, 2801),
(5571, 'Ольга', 2, 2801),
(5572, 'ЧП Иванец С.М.', 1, 2802),
(5573, 'ЧП Иванец С.М.', 2, 2802),
(5574, 'ФЛ-П Грицина В.А.Юрий, Валентина', 1, 2803),
(5575, 'ФЛ-П Грицина В.А.Юрий, Валентина', 2, 2803),
(5576, 'ФЛП Майлянс А.А.Анна Анатольевна', 1, 2804),
(5577, 'ФЛП Майлянс А.А.Анна Анатольевна', 2, 2804),
(5578, 'Оксанаменеджер по Европе и СНГ', 1, 2805),
(5579, 'Оксанаменеджер по Европе и СНГ', 2, 2805),
(5580, 'ФЛ-П Пидкивка И.М.Игорь', 1, 2806),
(5581, 'ФЛ-П Пидкивка И.М.Игорь', 2, 2806),
(5582, 'Игорь', 1, 2807),
(5583, 'Игорь', 2, 2807),
(5584, 'Роман', 1, 2808),
(5585, 'Роман', 2, 2808),
(5586, 'ЧП "Восток"Эдуард', 1, 2809),
(5587, 'ЧП "Восток"Эдуард', 2, 2809),
(5588, 'ФЛП Ларичев В. Ю.Татьяна', 1, 2810),
(5589, 'ФЛП Ларичев В. Ю.Татьяна', 2, 2810),
(5590, 'Виталий', 1, 2811),
(5591, 'Виталий', 2, 2811),
(5592, 'Галина', 1, 2812),
(5593, 'Галина', 2, 2812),
(5594, 'Наталия', 1, 2813),
(5595, 'Наталия', 2, 2813),
(5596, 'ТОВ "Ватра-Світлоприлад"Наталя', 1, 2814),
(5597, 'ТОВ "Ватра-Світлоприлад"Наталя', 2, 2814),
(5598, 'ФОП Чепель Евгения НиколаевнаЧепель Евгения Николаевна', 1, 2815),
(5599, 'ФОП Чепель Евгения НиколаевнаЧепель Евгения Николаевна', 2, 2815),
(5600, 'Виталина', 1, 2816),
(5601, 'Виталина', 2, 2816),
(5602, 'Галина', 1, 2817),
(5603, 'Галина', 2, 2817),
(5604, 'Лена', 1, 2818),
(5605, 'Лена', 2, 2818),
(5606, 'Людмила', 1, 2819),
(5607, 'Людмила', 2, 2819),
(5608, 'Людмила', 1, 2820),
(5609, 'Людмила', 2, 2820),
(5610, 'Нина', 1, 2821),
(5611, 'Нина', 2, 2821),
(5612, 'Сергей', 1, 2822),
(5613, 'Сергей', 2, 2822),
(5614, 'Антоніна', 1, 2823),
(5615, 'Антоніна', 2, 2823),
(5616, 'Наталья', 1, 2824),
(5617, 'Наталья', 2, 2824),
(5618, 'Юрий', 1, 2825),
(5619, 'Юрий', 2, 2825),
(5620, 'ФЛ-П Жабский В.Ю.Владислав', 1, 2826),
(5621, 'ФЛ-П Жабский В.Ю.Владислав', 2, 2826),
(5622, 'Юлия', 1, 2827),
(5623, 'Юлия', 2, 2827),
(5624, 'Вика', 1, 2828),
(5625, 'Вика', 2, 2828),
(5626, 'Виктория', 1, 2829),
(5627, 'Виктория', 2, 2829),
(5628, 'Елена', 1, 2830),
(5629, 'Елена', 2, 2830),
(5630, 'Натали', 1, 2831),
(5631, 'Натали', 2, 2831),
(5632, 'Нина', 1, 2832),
(5633, 'Нина', 2, 2832),
(5634, 'Оля*', 1, 2833),
(5635, 'Оля*', 2, 2833),
(5636, 'Света', 1, 2834),
(5637, 'Света', 2, 2834),
(5638, 'Юлия', 1, 2835),
(5639, 'Юлия', 2, 2835),
(5640, 'Юлия*', 1, 2836),
(5641, 'Юлия*', 2, 2836),
(5642, 'Александра', 1, 2837),
(5643, 'Александра', 2, 2837),
(5644, 'Владислав', 1, 2838),
(5645, 'Владислав', 2, 2838),
(5646, 'Катя #', 1, 2839),
(5647, 'Катя #', 2, 2839),
(5648, 'Таня', 1, 2840),
(5649, 'Таня', 2, 2840),
(5650, 'Эсмира', 1, 2841),
(5651, 'Эсмира', 2, 2841),
(5652, 'ПТЕП "Автотрансекспедиція"', 1, 2842),
(5653, 'ПТЕП "Автотрансекспедиція"', 2, 2842),
(5654, 'Іван', 1, 2843),
(5655, 'Іван', 2, 2843),
(5656, 'Ігор', 1, 2844),
(5657, 'Ігор', 2, 2844),
(5658, 'Богдан', 1, 2845),
(5659, 'Богдан', 2, 2845),
(5660, 'Володимир', 1, 2846),
(5661, 'Володимир', 2, 2846),
(5662, 'Галя', 1, 2847),
(5663, 'Галя', 2, 2847),
(5664, 'ДмитроВідділ внутрішніх перевезень', 1, 2848),
(5665, 'ДмитроВідділ внутрішніх перевезень', 2, 2848),
(5666, 'МаркоВідділ закордонних перевезень', 1, 2849),
(5667, 'МаркоВідділ закордонних перевезень', 2, 2849),
(5668, 'Наталя', 1, 2850),
(5669, 'Наталя', 2, 2850),
(5670, 'Олексій', 1, 2851),
(5671, 'Олексій', 2, 2851),
(5672, 'ОляВідділ внутрішніх перевезень', 1, 2852),
(5673, 'ОляВідділ внутрішніх перевезень', 2, 2852),
(5674, 'ТарасВідділ внутрішніх перевезень', 1, 2853),
(5675, 'ТарасВідділ внутрішніх перевезень', 2, 2853),
(5676, 'УлянаВідділ закордонних перевезень', 1, 2854),
(5677, 'УлянаВідділ закордонних перевезень', 2, 2854),
(5678, 'ЮрійВідділ внутрішніх перевезень', 1, 2855),
(5679, 'ЮрійВідділ внутрішніх перевезень', 2, 2855),
(5680, 'ФЛ-П Лиходей О. Н.Оксана', 1, 2856),
(5681, 'ФЛ-П Лиходей О. Н.Оксана', 2, 2856),
(5682, 'Оксана', 1, 2857),
(5683, 'Оксана', 2, 2857),
(5684, 'ООО "Матеріор"сергей', 1, 2858),
(5685, 'ООО "Матеріор"сергей', 2, 2858),
(5686, 'Александр', 1, 2859),
(5687, 'Александр', 2, 2859),
(5688, 'Андрій', 1, 2860),
(5689, 'Андрій', 2, 2860),
(5690, 'олег', 1, 2861),
(5691, 'олег', 2, 2861),
(5692, 'СПД Коваль М. П.Галина', 1, 2862),
(5693, 'СПД Коваль М. П.Галина', 2, 2862),
(5694, 'ФЛП Кривоносов Владислав ДмитриевичВладислав', 1, 2863),
(5695, 'ФЛП Кривоносов Владислав ДмитриевичВладислав', 2, 2863),
(5696, 'Екатерина', 1, 2864),
(5697, 'Екатерина', 2, 2864),
(5698, 'Леся', 1, 2865),
(5699, 'Леся', 2, 2865),
(5700, 'Татьяна', 1, 2866),
(5701, 'Татьяна', 2, 2866),
(5702, 'ООО "Краматорская транспортная компания"Синявский Алексей Юрьевич', 1, 2867),
(5703, 'ООО "Краматорская транспортная компания"Синявский Алексей Юрьевич', 2, 2867),
(5704, 'АллаНачальник отдела', 1, 2868),
(5705, 'АллаНачальник отдела', 2, 2868),
(5706, 'Бердник Юрий не работает в компании', 1, 2869),
(5707, 'Бердник Юрий не работает в компании', 2, 2869),
(5708, 'ФЛП  Донец Н. В.Надежда', 1, 2870),
(5709, 'ФЛП  Донец Н. В.Надежда', 2, 2870),
(5710, 'Евгений', 1, 2871),
(5711, 'Евгений', 2, 2871),
(5712, 'Наталья', 1, 2872),
(5713, 'Наталья', 2, 2872),
(5714, 'ЧП Манюк И. С.Манюк И С', 1, 2873),
(5715, 'ЧП Манюк И. С.Манюк И С', 2, 2873),
(5716, 'Алина', 1, 2874),
(5717, 'Алина', 2, 2874),
(5718, 'Борис', 1, 2875),
(5719, 'Борис', 2, 2875),
(5720, 'Валерия', 1, 2876),
(5721, 'Валерия', 2, 2876),
(5722, 'Елена', 1, 2877),
(5723, 'Елена', 2, 2877),
(5724, 'Игорь', 1, 2878),
(5725, 'Игорь', 2, 2878),
(5726, 'Ирина', 1, 2879),
(5727, 'Ирина', 2, 2879),
(5728, 'Карина', 1, 2880),
(5729, 'Карина', 2, 2880),
(5730, 'Марта', 1, 2881),
(5731, 'Марта', 2, 2881),
(5732, 'Наталья', 1, 2882),
(5733, 'Наталья', 2, 2882),
(5734, 'Оксана', 1, 2883),
(5735, 'Оксана', 2, 2883),
(5736, 'Света', 1, 2884),
(5737, 'Света', 2, 2884),
(5738, 'Татьяна', 1, 2885),
(5739, 'Татьяна', 2, 2885),
(5740, 'Элона', 1, 2886),
(5741, 'Элона', 2, 2886),
(5742, 'ООО "МАСС ТРАНС"Андрей', 1, 2887),
(5743, 'ООО "МАСС ТРАНС"Андрей', 2, 2887),
(5744, 'Алексей', 1, 2888),
(5745, 'Алексей', 2, 2888),
(5746, 'Алла', 1, 2889),
(5747, 'Алла', 2, 2889),
(5748, 'Андрей', 1, 2890),
(5749, 'Андрей', 2, 2890),
(5750, 'Екатерина', 1, 2891),
(5751, 'Екатерина', 2, 2891),
(5752, 'Елена', 1, 2892),
(5753, 'Елена', 2, 2892),
(5754, 'Лера (практикант)', 1, 2893),
(5755, 'Лера (практикант)', 2, 2893),
(5756, 'Сергей', 1, 2894),
(5757, 'Сергей', 2, 2894),
(5758, 'ЧП "Авива-Транс"Татьяна', 1, 2895),
(5759, 'ЧП "Авива-Транс"Татьяна', 2, 2895),
(5760, 'Виктория', 1, 2896),
(5761, 'Виктория', 2, 2896),
(5762, 'Вова', 1, 2897),
(5763, 'Вова', 2, 2897),
(5764, 'Ирина', 1, 2898),
(5765, 'Ирина', 2, 2898),
(5766, 'Катерина', 1, 2899),
(5767, 'Катерина', 2, 2899),
(5768, 'ФЛ-П Буланова Надежда ИгоревнаНадежда', 1, 2900),
(5769, 'ФЛ-П Буланова Надежда ИгоревнаНадежда', 2, 2900),
(5770, 'Сергей', 1, 2901),
(5771, 'Сергей', 2, 2901),
(5772, 'ООО "ХАЙВЕЙ"Владимир', 1, 2902),
(5773, 'ООО "ХАЙВЕЙ"Владимир', 2, 2902),
(5774, 'Алена', 1, 2903),
(5775, 'Алена', 2, 2903),
(5776, 'Анна', 1, 2904),
(5777, 'Анна', 2, 2904),
(5778, 'Елена', 1, 2905),
(5779, 'Елена', 2, 2905),
(5780, 'Ирина', 1, 2906),
(5781, 'Ирина', 2, 2906),
(5782, 'ЧФ "ХарьковАгрозапчасть"Сергей Евгеньевич', 1, 2907),
(5783, 'ЧФ "ХарьковАгрозапчасть"Сергей Евгеньевич', 2, 2907),
(5784, 'Ольга', 1, 2908),
(5785, 'Ольга', 2, 2908),
(5786, 'Светлана', 1, 2909),
(5787, 'Светлана', 2, 2909),
(5788, 'Юлия', 1, 2910),
(5789, 'Юлия', 2, 2910),
(5790, 'ФЛ-П Павлова С. И.Костя, Светлана', 1, 2911),
(5791, 'ФЛ-П Павлова С. И.Костя, Светлана', 2, 2911),
(5792, 'Костя', 1, 2912),
(5793, 'Костя', 2, 2912),
(5794, 'Костя', 1, 2913),
(5795, 'Костя', 2, 2913),
(5796, 'Светлана', 1, 2914),
(5797, 'Светлана', 2, 2914),
(5798, 'ЧП Григорьева Е. А.Елена', 1, 2915),
(5799, 'ЧП Григорьева Е. А.Елена', 2, 2915),
(5800, 'АСОЦІАЦІЯ ВАНТАЖНИХ ПЕРЕВІЗНИКІВ КИЄВАМалецкий Виталий', 1, 2916),
(5801, 'АСОЦІАЦІЯ ВАНТАЖНИХ ПЕРЕВІЗНИКІВ КИЄВАМалецкий Виталий', 2, 2916),
(5802, 'Катя', 1, 2917),
(5803, 'Катя', 2, 2917),
(5804, 'Марков Александр', 1, 2918),
(5805, 'Марков Александр', 2, 2918),
(5806, 'ФОП Сільник І. С.СТЕПАН', 1, 2919),
(5807, 'ФОП Сільник І. С.СТЕПАН', 2, 2919),
(5808, 'ЧП Бровко Е. С.Евгений', 1, 2920),
(5809, 'ЧП Бровко Е. С.Евгений', 2, 2920),
(5810, 'Оксана', 1, 2921),
(5811, 'Оксана', 2, 2921),
(5812, 'ФО-П Манько А. В.Андрей, Татьяна', 1, 2922),
(5813, 'ФО-П Манько А. В.Андрей, Татьяна', 2, 2922),
(5814, 'ФЛП Польской В. А.Польской В. А.', 1, 2923),
(5815, 'ФЛП Польской В. А.Польской В. А.', 2, 2923),
(5816, 'Владимир', 1, 2924),
(5817, 'Владимир', 2, 2924),
(5818, 'Лена', 1, 2925),
(5819, 'Лена', 2, 2925),
(5820, 'Сергей', 1, 2926),
(5821, 'Сергей', 2, 2926),
(5822, 'ФОП Шевченко С.Г.Александр, Сергей', 1, 2927),
(5823, 'ФОП Шевченко С.Г.Александр, Сергей', 2, 2927),
(5824, 'Александр', 1, 2928),
(5825, 'Александр', 2, 2928),
(5826, 'Сергей', 1, 2929),
(5827, 'Сергей', 2, 2929),
(5828, 'ФЛП Дубовик А.М.Андрей Михайлович', 1, 2930),
(5829, 'ФЛП Дубовик А.М.Андрей Михайлович', 2, 2930),
(5830, 'Андрей Михайлович', 1, 2931),
(5831, 'Андрей Михайлович', 2, 2931),
(5832, 'ФО-П Падусенко Ю.П.Олег', 1, 2932),
(5833, 'ФО-П Падусенко Ю.П.Олег', 2, 2932),
(5834, 'володя', 1, 2933),
(5835, 'володя', 2, 2933),
(5836, 'олег', 1, 2934),
(5837, 'олег', 2, 2934),
(5838, 'тамила', 1, 2935),
(5839, 'тамила', 2, 2935),
(5840, 'ФЛП Скорохода Р.М.Роман', 1, 2936),
(5841, 'ФЛП Скорохода Р.М.Роман', 2, 2936),
(5842, 'ООО "Грандис"Ирина, Сергей', 1, 2937),
(5843, 'ООО "Грандис"Ирина, Сергей', 2, 2937),
(5844, 'Ігор', 1, 2938),
(5845, 'Ігор', 2, 2938),
(5846, 'Геннадий', 1, 2939),
(5847, 'Геннадий', 2, 2939),
(5848, 'Елена', 1, 2940),
(5849, 'Елена', 2, 2940),
(5850, 'Людмила', 1, 2941),
(5851, 'Людмила', 2, 2941),
(5852, 'Сергей', 1, 2942),
(5853, 'Сергей', 2, 2942),
(5854, 'Юрій Лабунець', 1, 2943),
(5855, 'Юрій Лабунець', 2, 2943),
(5856, 'Юрий', 1, 2944),
(5857, 'Юрий', 2, 2944),
(5858, 'ДП "ЛИС" ООО "ЛЕГИОН"ЛИС-ЛЕГИОН', 1, 2945),
(5859, 'ДП "ЛИС" ООО "ЛЕГИОН"ЛИС-ЛЕГИОН', 2, 2945),
(5860, 'Людмила', 1, 2946),
(5861, 'Людмила', 2, 2946),
(5862, 'Наталия(044)4965568(факс)', 1, 2947),
(5863, 'Наталия(044)4965568(факс)', 2, 2947),
(5864, 'Сергей,', 1, 2948),
(5865, 'Сергей,', 2, 2948),
(5866, 'Тамара', 1, 2949),
(5867, 'Тамара', 2, 2949),
(5868, 'Тома', 1, 2950),
(5869, 'Тома', 2, 2950),
(5870, 'Яна', 1, 2951),
(5871, 'Яна', 2, 2951),
(5872, 'ЧП Хлоров А. С.Валентина', 1, 2952),
(5873, 'ЧП Хлоров А. С.Валентина', 2, 2952),
(5874, 'Валентина', 1, 2953),
(5875, 'Валентина', 2, 2953),
(5876, 'Валентина', 1, 2954),
(5877, 'Валентина', 2, 2954),
(5878, 'ФЛ-П Новиков В.Н.Виталий', 1, 2955),
(5879, 'ФЛ-П Новиков В.Н.Виталий', 2, 2955),
(5880, 'Виталий', 1, 2956),
(5881, 'Виталий', 2, 2956),
(5882, 'Кристина', 1, 2957),
(5883, 'Кристина', 2, 2957),
(5884, 'Марина', 1, 2958),
(5885, 'Марина', 2, 2958),
(5886, 'ООО "Зевс"Ігор', 1, 2959),
(5887, 'ООО "Зевс"Ігор', 2, 2959),
(5888, 'Ігор', 1, 2960),
(5889, 'Ігор', 2, 2960),
(5890, 'Андрій', 1, 2961),
(5891, 'Андрій', 2, 2961),
(5892, 'Володимир', 1, 2962),
(5893, 'Володимир', 2, 2962),
(5894, 'Лена', 1, 2963),
(5895, 'Лена', 2, 2963),
(5896, 'ПКФ ООО "ТАНА"Шибанов Виталий Сергеевич', 1, 2964),
(5897, 'ПКФ ООО "ТАНА"Шибанов Виталий Сергеевич', 2, 2964),
(5898, 'Виталий', 1, 2965),
(5899, 'Виталий', 2, 2965),
(5900, 'Наталья', 1, 2966),
(5901, 'Наталья', 2, 2966),
(5902, 'Роман', 1, 2967),
(5903, 'Роман', 2, 2967),
(5904, 'ФЛ-П Коломиец И. С.Ирина, Людмила, Оксана', 1, 2968),
(5905, 'ФЛ-П Коломиец И. С.Ирина, Людмила, Оксана', 2, 2968),
(5906, 'Ирина, Оксана', 1, 2969),
(5907, 'Ирина, Оксана', 2, 2969),
(5908, 'Людмила', 1, 2970),
(5909, 'Людмила', 2, 2970),
(5910, 'ФЛП Сломушинский И. В.Игорь', 1, 2971),
(5911, 'ФЛП Сломушинский И. В.Игорь', 2, 2971),
(5912, 'Александр', 1, 2972),
(5913, 'Александр', 2, 2972),
(5914, 'Вікторія', 1, 2973),
(5915, 'Вікторія', 2, 2973),
(5916, 'Игорь', 1, 2974),
(5917, 'Игорь', 2, 2974),
(5918, 'Оксана', 1, 2975),
(5919, 'Оксана', 2, 2975),
(5920, 'Роксолана', 1, 2976),
(5921, 'Роксолана', 2, 2976),
(5922, 'ФЛП Прибылова К.О.Екатерина, вайбер+380634982759', 1, 2977),
(5923, 'ФЛП Прибылова К.О.Екатерина, вайбер+380634982759', 2, 2977),
(5924, 'Александр', 1, 2978),
(5925, 'Александр', 2, 2978),
(5926, 'Вікторія', 1, 2979),
(5927, 'Вікторія', 2, 2979),
(5928, 'Елена Витальевна', 1, 2980),
(5929, 'Елена Витальевна', 2, 2980),
(5930, 'Наташа', 1, 2981),
(5931, 'Наташа', 2, 2981),
(5932, 'Світлана', 1, 2982),
(5933, 'Світлана', 2, 2982),
(5934, 'Светлана  Николаевна', 1, 2983),
(5935, 'Светлана  Николаевна', 2, 2983),
(5936, 'Цаплина  Алиса', 1, 2984),
(5937, 'Цаплина  Алиса', 2, 2984),
(5938, 'ФЛ-П Харченко Ю. Д.ЮЛИЯ', 1, 2985),
(5939, 'ФЛ-П Харченко Ю. Д.ЮЛИЯ', 2, 2985),
(5940, 'Ольга', 1, 2986),
(5941, 'Ольга', 2, 2986),
(5942, 'ЮЛЯ', 1, 2987),
(5943, 'ЮЛЯ', 2, 2987),
(5944, 'ЮЛЯ(киевстар)', 1, 2988),
(5945, 'ЮЛЯ(киевстар)', 2, 2988),
(5946, 'ФЛ-П Яманко Г.В.ООО "Салютем Плюс"', 1, 2989),
(5947, 'ФЛ-П Яманко Г.В.ООО "Салютем Плюс"', 2, 2989),
(5948, 'Александра', 1, 2990),
(5949, 'Александра', 2, 2990),
(5950, 'Алексей', 1, 2991),
(5951, 'Алексей', 2, 2991),
(5952, 'Анатолий', 1, 2992),
(5953, 'Анатолий', 2, 2992),
(5954, 'Анна', 1, 2993),
(5955, 'Анна', 2, 2993),
(5956, 'Антон', 1, 2994),
(5957, 'Антон', 2, 2994),
(5958, 'Василий Я.', 1, 2995),
(5959, 'Василий Я.', 2, 2995),
(5960, 'Виктор', 1, 2996),
(5961, 'Виктор', 2, 2996),
(5962, 'Виктория', 1, 2997),
(5963, 'Виктория', 2, 2997),
(5964, 'Дарья', 1, 2998),
(5965, 'Дарья', 2, 2998),
(5966, 'Олег', 1, 2999),
(5967, 'Олег', 2, 2999),
(5968, 'Олеся', 1, 3000),
(5969, 'Олеся', 2, 3000),
(5970, 'Оля', 1, 3001),
(5971, 'Оля', 2, 3001),
(5972, 'Соня', 1, 3002),
(5973, 'Соня', 2, 3002),
(5974, 'Таня Л', 1, 3003),
(5975, 'Таня Л', 2, 3003),
(5976, 'Татьяна', 1, 3004),
(5977, 'Татьяна', 2, 3004),
(5978, 'Георгий Васильевич', 1, 3005),
(5979, 'Георгий Васильевич', 2, 3005),
(5980, 'Василий М,', 1, 3006),
(5981, 'Василий М,', 2, 3006),
(5982, 'Александр Алексеевич', 1, 3007),
(5983, 'Александр Алексеевич', 2, 3007),
(5984, 'ТОВ "ТрансАвто+"Ирина', 1, 3008),
(5985, 'ТОВ "ТрансАвто+"Ирина', 2, 3008),
(5986, 'Алена', 1, 3009),
(5987, 'Алена', 2, 3009),
(5988, 'Кристина', 1, 3010),
(5989, 'Кристина', 2, 3010),
(5990, 'Наталья', 1, 3011),
(5991, 'Наталья', 2, 3011),
(5992, 'ФОП КРИВОШЕЯ В.С.Вячеслав', 1, 3012),
(5993, 'ФОП КРИВОШЕЯ В.С.Вячеслав', 2, 3012),
(5994, 'Алена', 1, 3013),
(5995, 'Алена', 2, 3013),
(5996, 'Артем', 1, 3014),
(5997, 'Артем', 2, 3014),
(5998, 'ФЛП Польшин А. А.', 1, 3015),
(5999, 'ФЛП Польшин А. А.', 2, 3015),
(6000, 'Александр Анатольевич', 1, 3016),
(6001, 'Александр Анатольевич', 2, 3016),
(6002, 'Алексей', 1, 3017),
(6003, 'Алексей', 2, 3017),
(6004, 'Виталий', 1, 3018),
(6005, 'Виталий', 2, 3018),
(6006, 'Владимир', 1, 3019),
(6007, 'Владимир', 2, 3019),
(6008, 'Маргарита', 1, 3020),
(6009, 'Маргарита', 2, 3020),
(6010, 'Марина', 1, 3021),
(6011, 'Марина', 2, 3021),
(6012, 'Светлана', 1, 3022),
(6013, 'Светлана', 2, 3022),
(6014, 'Сергей Анатольевич', 1, 3023),
(6015, 'Сергей Анатольевич', 2, 3023),
(6016, 'Сергей Валерьевич', 1, 3024),
(6017, 'Сергей Валерьевич', 2, 3024),
(6018, 'ФЛП Чуева Л. В.Людмила, Геннадий', 1, 3025),
(6019, 'ФЛП Чуева Л. В.Людмила, Геннадий', 2, 3025),
(6020, 'ООО "Торговый дом Кривбасс Ойл"Александр', 1, 3026),
(6021, 'ООО "Торговый дом Кривбасс Ойл"Александр', 2, 3026),
(6022, 'Юлия', 1, 3027),
(6023, 'Юлия', 2, 3027),
(6024, 'ФОП Довгаленко В. I.Виталий, Наташа', 1, 3028),
(6025, 'ФОП Довгаленко В. I.Виталий, Наташа', 2, 3028),
(6026, 'ООО "Грин Ман Компани ГМК"Александр', 1, 3029),
(6027, 'ООО "Грин Ман Компани ГМК"Александр', 2, 3029),
(6028, 'Александр', 1, 3030),
(6029, 'Александр', 2, 3030),
(6030, 'Владимир', 1, 3031),
(6031, 'Владимир', 2, 3031),
(6032, 'ООО "НПК ЗАПОРОЖАВТОБЫТХИМ"Роман Николаевич', 1, 3032),
(6033, 'ООО "НПК ЗАПОРОЖАВТОБЫТХИМ"Роман Николаевич', 2, 3032),
(6034, 'Александр', 1, 3033),
(6035, 'Александр', 2, 3033),
(6036, 'ФЛ-П Драчук К.А.Елена', 1, 3034),
(6037, 'ФЛ-П Драчук К.А.Елена', 2, 3034),
(6038, 'Евгения', 1, 3035),
(6039, 'Евгения', 2, 3035),
(6040, 'Еленазаказчик', 1, 3036),
(6041, 'Еленазаказчик', 2, 3036),
(6042, 'Еленаэкспедиция', 1, 3037),
(6043, 'Еленаэкспедиция', 2, 3037),
(6044, 'Елена', 1, 3038),
(6045, 'Елена', 2, 3038),
(6046, 'ЧП "Стохид"Максим', 1, 3039),
(6047, 'ЧП "Стохид"Максим', 2, 3039),
(6048, 'ООО "Торгово-Транспортная Компания "Туркус"Татьяна', 1, 3040),
(6049, 'ООО "Торгово-Транспортная Компания "Туркус"Татьяна', 2, 3040),
(6050, 'Алексей,Инесса', 1, 3041),
(6051, 'Алексей,Инесса', 2, 3041),
(6052, 'ООО "Восток"Трущенко Сергей', 1, 3042),
(6053, 'ООО "Восток"Трущенко Сергей', 2, 3042),
(6054, 'Александр', 1, 3043),
(6055, 'Александр', 2, 3043),
(6056, 'Андрей', 1, 3044),
(6057, 'Андрей', 2, 3044),
(6058, 'Сергей', 1, 3045),
(6059, 'Сергей', 2, 3045),
(6060, 'ФЛ-П Войтенко В.ВВиталий', 1, 3046),
(6061, 'ФЛ-П Войтенко В.ВВиталий', 2, 3046),
(6062, 'Вероника', 1, 3047),
(6063, 'Вероника', 2, 3047),
(6064, 'Виталий', 1, 3048),
(6065, 'Виталий', 2, 3048),
(6066, 'Екатерина', 1, 3049),
(6067, 'Екатерина', 2, 3049),
(6068, 'Серж', 1, 3050),
(6069, 'Серж', 2, 3050),
(6070, 'ООО "Поли-Пак"Дычик Вячеслав Борисович', 1, 3051),
(6071, 'ООО "Поли-Пак"Дычик Вячеслав Борисович', 2, 3051),
(6072, 'Ирина', 1, 3052),
(6073, 'Ирина', 2, 3052),
(6074, 'Катерина', 1, 3053),
(6075, 'Катерина', 2, 3053),
(6076, 'Татьяна', 1, 3054),
(6077, 'Татьяна', 2, 3054),
(6078, 'Татьяна Валериевнагл. бухгалтер', 1, 3055),
(6079, 'Татьяна Валериевнагл. бухгалтер', 2, 3055),
(6080, 'ТОВ "Квалес Україна"Ксения', 1, 3056),
(6081, 'ТОВ "Квалес Україна"Ксения', 2, 3056),
(6082, 'Анатолий', 1, 3057),
(6083, 'Анатолий', 2, 3057),
(6084, 'Володимир', 1, 3058),
(6085, 'Володимир', 2, 3058),
(6086, 'Ніна', 1, 3059),
(6087, 'Ніна', 2, 3059),
(6088, 'Оксана', 1, 3060),
(6089, 'Оксана', 2, 3060),
(6090, 'Ольга', 1, 3061),
(6091, 'Ольга', 2, 3061),
(6092, 'Павло', 1, 3062),
(6093, 'Павло', 2, 3062),
(6095, 'Олександр Плоднік', 1, 3064),
(6097, 'Воробей Володимир', 1, 3066),
(6098, 'mfafahlfh', 1, 3067),
(6099, 'Рикальський Олександр', 1, 3068),
(6104, 'Igor Kushil', 1, 3073);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Socials`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Socials` (
  `ID` int(10) unsigned NOT NULL,
  `network` varchar(45) NOT NULL,
  `identity` varchar(1000) NOT NULL,
  `socialUID` varchar(1000) NOT NULL,
  `email` varchar(100) NOT NULL,
  `userID` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_Socials`
--

INSERT INTO `Frontend_User_Socials` (`ID`, `network`, `identity`, `socialUID`, `email`, `userID`) VALUES
(7, 'facebook', 'https://www.facebook.com/app_scoped_user_id/100001805845331/', '100001805845331', 'makkartnis@gmail.com', 3064),
(10, 'facebook', 'https://www.facebook.com/app_scoped_user_id/1013826571997951/', '1013826571997951', 'igor.kushil@gmail.com', 3073);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Cargo_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Cargo_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `cargoID` int(10) unsigned NOT NULL,
  `status` enum('accepted','rejected','pending') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Company_Ownership`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Company_Ownership` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_to_Company_Ownership`
--

INSERT INTO `Frontend_User_to_Company_Ownership` (`ID`, `userID`, `companyID`) VALUES
(495, 2604, 536),
(496, 2606, 537),
(497, 2610, 538),
(498, 2612, 539),
(499, 2633, 540),
(500, 2639, 541),
(501, 2640, 542),
(502, 2643, 543),
(503, 2645, 544),
(504, 2649, 545),
(505, 2650, 546),
(506, 2651, 547),
(507, 2656, 548),
(508, 2665, 549),
(509, 2673, 550),
(510, 2678, 551),
(511, 2680, 552),
(512, 2684, 553),
(513, 2692, 554),
(514, 2700, 555),
(515, 2707, 556),
(516, 2709, 557),
(517, 2717, 558),
(518, 2722, 559),
(519, 2725, 560),
(520, 2729, 561),
(521, 2730, 562),
(522, 2739, 563),
(523, 2747, 564),
(524, 2757, 565),
(525, 2767, 566),
(526, 2776, 567),
(527, 2777, 568),
(528, 2781, 569),
(529, 2797, 570),
(530, 2803, 571),
(531, 2804, 572),
(532, 2806, 573),
(533, 2809, 574),
(534, 2810, 575),
(535, 2814, 577),
(536, 2815, 578),
(537, 2826, 579),
(538, 2842, 580),
(539, 2856, 581),
(540, 2858, 582),
(541, 2862, 583),
(542, 2863, 584),
(543, 2867, 585),
(544, 2870, 586),
(545, 2873, 587),
(546, 2887, 588),
(547, 2895, 589),
(548, 2900, 590),
(549, 2902, 591),
(550, 2907, 592),
(551, 2911, 593),
(552, 2915, 594),
(553, 2916, 595),
(554, 2919, 596),
(555, 2920, 597),
(556, 2922, 598),
(557, 2923, 599),
(558, 2927, 600),
(559, 2930, 601),
(560, 2932, 602),
(561, 2936, 603),
(562, 2937, 604),
(563, 2945, 605),
(564, 2952, 606),
(565, 2955, 607),
(566, 2959, 608),
(567, 2964, 609),
(568, 2968, 610),
(569, 2971, 611),
(570, 2977, 612),
(571, 2985, 613),
(572, 2989, 614),
(573, 3008, 615),
(574, 3012, 616),
(575, 3015, 617),
(576, 3025, 618),
(577, 3026, 619),
(578, 3028, 620),
(579, 3029, 621),
(580, 3032, 622),
(581, 3034, 623),
(582, 3039, 624),
(583, 3040, 625),
(584, 3042, 626),
(585, 3046, 627),
(586, 3051, 628),
(587, 3056, 629),
(589, 3066, 633),
(590, 3067, 634),
(591, 3068, 635),
(596, 3073, 640);

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Tender_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Tender_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `tenderID` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requestNumber` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Transport_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Transport_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `transportID` int(10) unsigned NOT NULL,
  `status` enum('accepted','rejected','pending') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Type_Rights`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Type_Rights` (
  `ID` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `userTypeID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(5) unsigned NOT NULL,
  `const` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` enum('const','text-block') NOT NULL DEFAULT 'const'
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `i18n`
--

INSERT INTO `i18n` (`id`, `const`, `type`) VALUES
(273, 'company.list.notFound', 'const'),
(276, 'company.type.transport', 'const'),
(278, 'company.list.notFoundBackLink', 'const'),
(279, 'error.404.companyNotFound', 'const'),
(280, 'company.page.title(:company)', 'const'),
(281, 'company.list.title', 'const'),
(285, 'company.page.rating', 'const'),
(286, 'company.page.activity', 'const'),
(287, 'company.page.location', 'const'),
(288, 'company.page.tab.infromation', 'const'),
(289, 'company.page.tab.requests(:count)', 'const'),
(290, 'company.page.tab.documents(:count)', 'const'),
(291, 'company.page.tab.transport(:count)', 'const'),
(301, 'site.title(:page_title)', 'const'),
(302, 'main_page.title', 'const'),
(317, 'company.page.documents.podatkovi', 'const'),
(318, 'company.page.documents.sertifikaty', 'const'),
(319, 'company.page.documents.svidoctva', 'const'),
(323, 'company.type.expeditor', 'const'),
(324, 'company.type.cargo', 'const'),
(327, 'company.registration.title', 'const'),
(328, 'transport.page.add.title', 'const'),
(329, 'cargo.page.add.title', 'const'),
(330, 'profile.title', 'const'),
(334, 'car.type.tent', 'const'),
(335, 'car.type.izoterm', 'const'),
(336, 'car.type.refrigerator', 'const'),
(337, 'currency.hrn', 'const'),
(338, 'car.body.vantazhivka', 'const'),
(339, 'car.body.napiv prychip', 'const'),
(340, 'car.body.z prychipom', 'const'),
(342, 'transport.page.list.title', 'const'),
(343, 'site.menu.header.cargo', 'const'),
(344, 'site.menu.header.transport', 'const'),
(345, 'site.menu.header.companies', 'const'),
(346, 'site.menu.header.press', 'const'),
(347, 'site.menu.header.advertisement', 'const'),
(348, 'site.menu.header.contacts', 'const'),
(349, 'tent', 'const'),
(350, 'izoterm', 'const'),
(351, 'refrigerator', 'const'),
(352, 'transport.list.notFound', 'const'),
(354, 'transport.list.notFoundInfo', 'const'),
(355, 'company.ownership.type.tzov', 'const'),
(356, 'company.ownership.type.pp', 'const'),
(357, 'company.ownership.type.fop', 'const'),
(358, 'company.ownership.type.spd', 'const'),
(359, 'company.ownership.type.fl-p', 'const'),
(360, 'company.ownership.type.chp', 'const'),
(361, 'company.ownership.type.flp', 'const'),
(362, 'company.ownership.type.ooo', 'const'),
(363, 'profile.add_tender.title', 'const'),
(364, 'form.error.email_available', 'const'),
(365, 'form.error.not_empty', 'const'),
(366, 'form.error.email', 'const'),
(367, 'form.error.max_length', 'const'),
(368, 'form.error.min_length', 'const'),
(369, 'form.error.numeric', 'const'),
(370, 'form.error.exact_length', 'const'),
(371, 'email.confirm_registration.title', 'const'),
(372, 'form.error.regex', 'const'),
(373, 'notification.auth.confirm_email', 'const'),
(374, 'tzov', 'const'),
(375, 'pp', 'const'),
(376, 'fop', 'const'),
(377, 'spd', 'const'),
(378, 'tender.page.list.title', 'const'),
(379, 'tender.page.title', 'const'),
(380, 'cargo.page.list.title', 'const'),
(381, 'krytaya', 'const'),
(382, 'konteyner', 'const'),
(383, 'ref', 'const'),
(384, 'lesovoz', 'const'),
(385, 'cel-nomet', 'const'),
(386, 'samosval', 'const'),
(393, 'konteynerovoz', 'const'),
(394, 'bortovaya', 'const'),
(395, 'bus', 'const'),
(396, 'cargo.list.notFound', 'const'),
(397, 'cargo.list.notFoundInfo', 'const'),
(398, 'zernovoz', 'const'),
(399, 'tral-negabarit', 'const'),
(400, 'otkrytaya', 'const'),
(401, 'cisterna', 'const'),
(407, 'platforma', 'const');

-- --------------------------------------------------------

--
-- Структура таблиці `i18n_locale`
--

CREATE TABLE IF NOT EXISTS `i18n_locale` (
  `i18nID` int(5) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `translate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `i18n_locale`
--

INSERT INTO `i18n_locale` (`i18nID`, `localeID`, `translate`) VALUES
(273, 1, 'Компаній по заданим критеріям не знайдено!'),
(273, 2, 'company.list.notFound'),
(276, 1, 'Транспорт'),
(276, 2, 'company.type.transport'),
(278, 1, 'Назад до списку компаній'),
(278, 2, 'company.list.notFoundBackLink'),
(279, 1, 'Компанії не існує'),
(280, 1, 'Компанія «:company»'),
(280, 2, 'company.page.title(:company)'),
(281, 1, 'Список компаній порталу'),
(281, 2, 'company.list.title'),
(285, 1, 'Рейтинг компанії'),
(285, 2, 'company.page.rating'),
(286, 1, 'Діяльність'),
(286, 2, 'company.page.activity'),
(287, 1, 'Розташування'),
(287, 2, 'company.page.location'),
(288, 1, 'Інформація'),
(288, 2, 'company.page.tab.infromation'),
(289, 1, 'Заявки на біржі (:count)'),
(289, 2, 'company.page.tab.requests(:count)'),
(290, 1, 'Документи (:count)'),
(290, 2, 'company.page.tab.documents(:count)'),
(291, 1, 'Наш транспорт (:count)'),
(291, 2, 'company.page.tab.transport(:count)'),
(301, 1, ':page_title - TIRSCAN'),
(301, 2, 'site.title(:page_title)'),
(302, 1, 'Головна сторінка'),
(302, 2, 'main_page.title'),
(317, 1, 'Податкові'),
(317, 2, 'company.page.documents.podatkovi'),
(318, 1, 'Сертифікати'),
(318, 2, 'company.page.documents.sertifikaty'),
(319, 1, 'Свідоцтва'),
(319, 2, 'company.page.documents.svidoctva'),
(323, 1, 'Експедитор'),
(323, 2, 'company.type.expeditor'),
(324, 1, 'Вантажотримач'),
(324, 2, 'company.type.cargo'),
(327, 1, 'Реєстрація на порталі'),
(327, 2, 'company.registration.title'),
(328, 1, 'Додати транспорт'),
(329, 1, 'Додати вантаж'),
(330, 1, 'Мій профіль'),
(334, 1, 'Тент'),
(335, 1, 'Ізотерм'),
(336, 1, 'Рефрижератор'),
(337, 1, 'currency.hrn'),
(338, 1, 'вантажівка'),
(339, 1, 'напівпричіп'),
(340, 1, 'з причіпом'),
(342, 1, 'Транспорт'),
(342, 2, 'transport.page.list.title'),
(343, 1, 'Вантажі'),
(343, 2, 'site.menu.header.cargo'),
(344, 1, 'Транспорт'),
(344, 2, 'site.menu.header.transport'),
(345, 1, 'Компанії'),
(345, 2, 'site.menu.header.companies'),
(346, 1, 'Прес-центр'),
(346, 2, 'site.menu.header.press'),
(347, 1, 'Оголошення'),
(347, 2, 'site.menu.header.advertisement'),
(348, 1, 'Контакти'),
(348, 2, 'site.menu.header.contacts'),
(349, 1, 'тент'),
(350, 1, 'ізотерм'),
(351, 1, 'рефрижератор'),
(352, 1, 'Транспортних заявок по заданому фільтру не знайдено!'),
(354, 1, 'Наразі в системі не зареєстровано заявок, що відповідають вказаним фільтрам. Спробуйте пізніше або змініть критерії фільтрування.'),
(355, 1, 'ТзОВ'),
(356, 1, 'ПП'),
(357, 1, 'ФОП'),
(358, 1, 'СПД'),
(359, 1, 'ФЛ-П'),
(360, 1, 'ЧП'),
(361, 1, 'ФЛП'),
(362, 1, 'ООО'),
(363, 1, 'Створення тендеру'),
(364, 1, 'Вказаний e-mail вже зайнятий у системі'),
(365, 1, 'Поле обов''язкове для заповнення'),
(366, 1, 'Введіть правильну e-mail адресу'),
(367, 1, 'Кількість символів не повинна перевищувати :1'),
(368, 1, 'Мінімально допустима кількість символів - :1'),
(369, 1, 'Допускаються лише цифри'),
(370, 1, 'Допустима кількість символів - :1'),
(371, 1, 'email.confirm_registration.title'),
(372, 1, 'Допускаються лише літери латинського алфавіту, цифри та спецсимволи !@#$%^_- (пароль повинен починатись з літери)'),
(373, 1, 'Вам необхідно підтвердити ваш e-mail. Вам було відправлено лист для підтвердження реєстрації'),
(374, 1, 'tzov'),
(374, 2, 'tzov'),
(375, 1, 'pp'),
(375, 2, 'pp'),
(376, 1, 'fop'),
(376, 2, 'fop'),
(377, 1, 'spd'),
(377, 2, 'spd'),
(378, 1, 'Список тендерів'),
(379, 1, 'Тендер №125478'),
(380, 1, 'Вантажі'),
(381, 1, 'krytaya'),
(382, 1, 'konteyner'),
(383, 1, 'ref'),
(384, 1, 'lesovoz'),
(385, 1, 'cel-nomet'),
(386, 1, 'samosval'),
(393, 1, 'konteynerovoz'),
(394, 1, 'bortovaya'),
(395, 1, 'bus'),
(396, 1, 'cargo.list.notFound'),
(397, 1, 'cargo.list.notFoundInfo'),
(398, 1, 'zernovoz'),
(399, 1, 'tral-negabarit'),
(400, 1, 'otkrytaya'),
(401, 1, 'cisterna'),
(407, 1, 'platforma');

-- --------------------------------------------------------

--
-- Структура таблиці `x_admins`
--

CREATE TABLE IF NOT EXISTS `x_admins` (
  `id` int(11) unsigned NOT NULL,
  `srt` int(11) NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `pass` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `temp_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fb_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `vk_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tw_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `go_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця адміністраторів CMS';

--
-- Дамп даних таблиці `x_admins`
--

INSERT INTO `x_admins` (`id`, `srt`, `group_id`, `email`, `pass`, `name`, `login`, `active`, `register_date`, `phone`, `temp_key`, `fb_link`, `vk_link`, `tw_link`, `go_link`) VALUES
(1, 6, 1, 'ap@artis.ms', 'e10adc3949ba59abbe56e057f20f883e', 'root', 'root', 1, '2014-11-05 15:19:00', '+380934527685', '', 'https://www.facebook.com/profile.php?id=100001805845331', 'https://vk.com/kindolp', '', ''),
(9, 0, 2, 'admin@admin.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Адміністратор', '', 1, '2016-01-16 12:26:31', '+380934527666, +380934527668', '', '', '', '', ''),
(11, 0, 3, 'moderator@moderator.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Модератор', '', 1, '2016-01-16 12:55:01', '+380934527688', '', '', '', '', ''),
(12, 0, 4, 'menedjer@menedjer.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Менеджер', '', 1, '2016-01-16 12:55:54', '+380934527685', '', '', '', '', '');

--
-- Тригери `x_admins`
--
DELIMITER $$
CREATE TRIGGER `ins_admin_locale` AFTER INSERT ON `x_admins`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT id FROM x_lngs;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_admin_locales (lng_id,admin_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_groups`
--

CREATE TABLE IF NOT EXISTS `x_admin_groups` (
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_groups`
--

INSERT INTO `x_admin_groups` (`id`) VALUES
(1),
(2),
(3),
(4);

--
-- Тригери `x_admin_groups`
--
DELIMITER $$
CREATE TRIGGER `ins_admin_group_locales` AFTER INSERT ON `x_admin_groups`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT id FROM x_plugin_lngs;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_admin_group_locales (plugin_lng_id,admin_group_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_group_locales`
--

CREATE TABLE IF NOT EXISTS `x_admin_group_locales` (
  `id` int(10) unsigned NOT NULL,
  `admin_group_id` int(10) unsigned NOT NULL,
  `plugin_lng_id` smallint(5) unsigned NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_group_locales`
--

INSERT INTO `x_admin_group_locales` (`id`, `admin_group_id`, `plugin_lng_id`, `title`, `preview`) VALUES
(1, 1, 1, 'Суперадміністратор', 'управління адміністраторами та доступ до управління всієї системи'),
(2, 2, 1, 'Адміністратор', 'управління всією системою, максимальні права доступу'),
(3, 3, 1, 'Модератор', 'управління користувачами та інформаційними розділами порталу'),
(4, 4, 1, 'Менеджер', 'управління заявками');

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_group_rights`
--

CREATE TABLE IF NOT EXISTS `x_admin_group_rights` (
  `ID` int(10) unsigned NOT NULL,
  `rightKey` varchar(100) NOT NULL,
  `status` enum('allowed','forbidden') NOT NULL DEFAULT 'allowed',
  `adminGroupID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_locales`
--

CREATE TABLE IF NOT EXISTS `x_admin_locales` (
  `id` int(10) unsigned NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  `lng_id` smallint(5) unsigned NOT NULL,
  `role` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_locales`
--

INSERT INTO `x_admin_locales` (`id`, `admin_id`, `lng_id`, `role`, `preview`, `name`) VALUES
(1, 1, 1, 'Адміністратор сайту 2W Entertainment', 'Технічний супровід проекту 2W ENTERTAINMENT.<br><span style="font-size: 12px;text-transform: none;">\nЗони контролю: проектування та впровадження функціональних рішень, системне адміністрування, технічна підтримка та розвиток сайту.</span>', 'Олександр Плоднік'),
(2, 1, 7, 'Site administrator of 2W Entertainment', 'Technical support of the project 2W Entertainment.<br><span style="font-size: 12px;text-transform: none;">Areas of control: design and implementation of functional solutions, system administration and site development.</span>', 'Oleksandr Plodnik'),
(3, 1, 9, 'Администратор сайта 2W Entertainment', 'Техническое сопровождение проекта 2W Entertainment.<br><span style="font-size: 12px;text-transform: none;">Зоны контроля: проектирование и внедрение функциональных решений, системное администрирование, техническая поддержка и развитие сайта.</span>', 'Александр Плодник'),
(4, 2, 1, 'Організація концертів', '«Двигун» проекту 2W Entertainment.<br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Місця проведення та інші локації для культурно-масових заходів, комунікація та розвиток звязків з українськими спільнотами за кордоном.                                             Ідеолог проекту «Без меж»</span>', 'Володимир Філяк'),
(5, 2, 7, 'Concert management', 'The «engine» of 2W Entertainment.  <br> <span style="font-size: 12px;text-transform: none;">Areas of control: Venues and locations for cultural events; communication and development of the relationship with the Ukrainian community abroad. Ideologist of the «No boundaries»</span>', 'Volodymyr Filiak'),
(6, 2, 9, 'Организация концертов', '«Двигатель» проекта 2W Entertainment. <br><span style="font-size: 12px;text-transform: none;">Зоны контроля: Места проведения и другие локации для культурно-массовых мероприятий, коммуникация и развитие отношений с украинскими сообществами за границей. Идеолог направления «Без границ»</span>', 'Владимир Филяк'),
(7, 3, 1, 'Організація концертів', 'Стратегічний партнер проекту 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Планування та організація бізнес-процесів проекту, маркетинг та партнерські програми, комунікації з артистами, концертний тур-менеджмент</span>', 'Тетяна Кільтика'),
(8, 3, 7, 'Concert management', 'Strategic partner of 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Areas of control: Planning and Business process management; marketing and partnership programmes; communications with the artists and tour management</span>', 'Tetiana Kiltyka'),
(9, 3, 9, 'Организация концертов', 'Стратегический партнер проекта 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Зоны контроля: Планирование и организация бизнес-процессов проекта, маркетинг и партнерские программы, коммуникации с артистами, концертный тур-менеджмент</span>', 'Татьяна Кильтыка'),
(10, 4, 1, 'Менеджер напрямку Production', 'Лідер напрямку виробництва колекцій української патріотичної продукції. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: менеджмент виробничого процесу на всіх етапах, дистрибуція, логістика, маркетинг та продажі</span>', 'Дмитро Кротов'),
(11, 4, 7, 'Manager of the Production unit', 'Leader of the production of the Unian patriotic collections. <br> <span style="font-size: 12px;text-transform: none;">Areas of control: management of the production process of all stages of distribution, logistic, marketing and sales</span>', 'Dmitry Krotov'),
(12, 4, 9, 'Менеджер направления Production', 'Лидер направления производства коллекций украинской патриотической продукции. <br> <span style="font-size: 12px;text-transform: none;">Зоны контроля: менеджмент производственного процесса на всех этапах, дистрибуция, логистика, маркетинг и продажи</span>', 'Дмитрий Кротов'),
(13, 5, 1, 'Юрист', 'Юридичний консультант. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Юридичний супровід усіх бізнес процесів проекту 2W Entertainment</span>', 'Інна Полякова'),
(14, 5, 7, 'Lawyer', 'Legal support of all business processes of 2W Entertainment', 'Inna Poliakova'),
(15, 5, 9, 'Юрист', 'Юридическое сопровождение всех бизнес процессов проектов 2W Entertainment', 'Инна Полякова'),
(16, 6, 1, 'PR & Контент-менеджер', 'Менеджер проекту United Ukraine - об’єднання українців за кордоном.<br> <span style="font-size: 12px;text-transform: none;">\nЗони контролю: контент-менеджмент, зв’язки з громадськістю, адміністрування соціальних мереж, комунікація з артистами та читацькою аудиторією</span>', 'Анастасія Хілевська'),
(17, 6, 7, 'PR & Content manager', 'Manager of the United Ukraine project - Ukrainian community abroad.<br> <span style="font-size: 12px;text-transform: none;">\nAreas of control: content-management, PR, social media marketing,communication with artists and general audience</span>', 'Anastasia Khilevska'),
(18, 6, 9, 'PR & Контент-менеджер', 'Менеджер проекта United Ukraine - объединение украинцев за границей.<br> <span style="font-size: 12px;text-transform: none;">\nЗоны контроля: контент-менеджмент, связи с общественностью, администрирование социальных сетей, коммуникация с артистами и читательской аудиторией</span>', 'Анастасия Хилевська'),
(19, 7, 7, '', '', ''),
(20, 7, 9, '', '', ''),
(21, 7, 1, '', '', ''),
(22, 8, 7, '', '', ''),
(23, 8, 9, '', '', ''),
(24, 8, 1, '', '', ''),
(25, 9, 7, '', '', ''),
(26, 9, 9, '', '', ''),
(27, 9, 1, '', '', ''),
(28, 11, 7, '', '', ''),
(29, 11, 9, '', '', ''),
(30, 11, 1, '', '', ''),
(31, 12, 7, '', '', ''),
(32, 12, 9, '', '', ''),
(33, 12, 1, '', '', ''),
(34, 13, 7, '', '', ''),
(35, 13, 9, '', '', ''),
(36, 13, 1, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugins`
--

CREATE TABLE IF NOT EXISTS `x_plugins` (
  `id` int(11) unsigned NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '1',
  `srt` int(11) NOT NULL,
  `icon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sub` int(10) unsigned DEFAULT NULL,
  `sub2` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця плагінів cms';

--
-- Дамп даних таблиці `x_plugins`
--

INSERT INTO `x_plugins` (`id`, `controller`, `action`, `is_show`, `srt`, `icon`, `sub`, `sub2`) VALUES
(59, '', '', 1, 7, 'fa-eye', NULL, 0),
(60, 'admin', '', 1, 1, '', 59, 0),
(61, 'admin', 'add', 1, 2, '', 59, 0),
(62, 'admin', 'edit', 0, -100, '', 59, 0),
(84, '', '', 1, -200, 'fa-users', NULL, 0),
(85, 'user', '', 1, -200, '', 84, 0),
(86, 'user', 'add', 1, 2, '', 84, 0),
(87, 'user', 'edit', 0, -100, '', 84, 0),
(128, '', '', 1, -270, 'fa-building-o', NULL, 0),
(129, 'company', '', 1, 1, '', 128, 0),
(134, 'admin', 'group', 1, 3, '', 59, 0),
(228, 'company', 'add', 1, 2, '', 128, 0),
(229, 'company', 'edit', 0, -100, '', 128, 0),
(291, 'company', 'owner_types', 1, 3, '', 302, 0),
(293, 'transport', '', 1, 4, '', 128, 0),
(296, '', '', 1, -290, 'fa-truck', NULL, 0),
(297, 'transportRequests', '', 1, 1, '', 296, -100),
(298, 'transportRequests', 'add', 1, 2, '', 296, 0),
(299, '', '', 1, -280, 'fa-th', NULL, 0),
(300, 'cargoRequests', '', 1, 1, '', 299, 0),
(301, 'cargoRequests', 'add', 1, 2, '', 299, 0),
(302, '', '', 1, 12, 'fa-cogs', NULL, 0),
(303, 'settings', 'locales', 1, 1, '', 302, 0),
(304, '', '', 1, -260, 'fa-calendar', NULL, 0),
(305, 'news', '', 1, 1, '', 304, 0),
(306, 'news', 'add', 1, 2, '', 304, 0),
(307, 'user', 'news', 1, 3, '', 84, 0),
(308, 'company', 'comments', 0, 4, '', 128, 0),
(309, 'analytics', '', 0, -300, 'fa-bar-chart-o', NULL, 0),
(310, '', '', 1, -279, 'fa-gavel', NULL, 0),
(311, 'tenders', '', 1, 0, '', 310, 0),
(312, 'tenders', 'add', 1, 0, '', 310, 0),
(313, 'settings', 'currencies', 1, 0, '', 302, 0),
(314, 'settings', 'car_types', 1, 0, '', 302, 0);

--
-- Тригери `x_plugins`
--
DELIMITER $$
CREATE TRIGGER `ins_plugin_locale` AFTER INSERT ON `x_plugins`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT id FROM x_plugin_lngs;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_plugin_locales (plugin_lng_id,plugin_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugin_lngs`
--

CREATE TABLE IF NOT EXISTS `x_plugin_lngs` (
  `id` smallint(5) unsigned NOT NULL,
  `pref` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `def` tinyint(1) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця мов плагінів cms';

--
-- Дамп даних таблиці `x_plugin_lngs`
--

INSERT INTO `x_plugin_lngs` (`id`, `pref`, `title`, `def`, `active`) VALUES
(1, 'uk', 'Українська', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugin_locales`
--

CREATE TABLE IF NOT EXISTS `x_plugin_locales` (
  `id` int(10) unsigned NOT NULL,
  `plugin_lng_id` smallint(5) unsigned NOT NULL,
  `plugin_id` int(10) unsigned NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_plugin_locales`
--

INSERT INTO `x_plugin_locales` (`id`, `plugin_lng_id`, `plugin_id`, `title`, `active`) VALUES
(301, 1, 59, 'Адміністрація', 1),
(302, 1, 60, 'Показати всіх', 1),
(303, 1, 62, 'Профіль адміністратора', 1),
(304, 1, 61, 'Створити', 1),
(305, 1, 84, 'Користувачі', 1),
(306, 1, 85, 'Показати всіх', 1),
(307, 1, 86, 'Створити користувача', 1),
(308, 1, 87, 'Профіль користувача', 1),
(309, 1, 125, 'Групи користувачів', 1),
(310, 1, 128, 'Компанії', 1),
(311, 1, 129, 'Показати всі', 1),
(312, 1, 130, 'Типи компаній', 1),
(313, 1, 134, 'Групи адміністраторів', 1),
(314, 1, 228, 'Створити компанію', 1),
(315, 1, 229, 'Профіль компанії', 1),
(316, 1, 291, 'Форми власності', 1),
(317, 1, 292, 'Транспорт компаній', 1),
(318, 1, 293, 'Транспорт компаній', 1),
(319, 1, 294, 'Додати транспорт', 1),
(320, 1, 295, 'Редагування транспорта', 1),
(321, 1, 296, 'Транспорт', 1),
(322, 1, 297, 'Всі заявки', 1),
(323, 1, 298, 'Створити заявку', 1),
(324, 1, 299, 'Вантажі', 1),
(325, 1, 300, 'Всі заявки', 1),
(326, 1, 301, 'Створити заявку', 1),
(327, 1, 302, 'Налаштування', 1),
(328, 1, 303, 'Локалізація', 1),
(329, 1, 304, 'Прес-центр', 1),
(330, 1, 305, 'Всі новини', 1),
(331, 1, 306, 'Додати', 1),
(332, 1, 307, 'Новини', 1),
(333, 1, 308, 'Коментарі', 1),
(334, 1, 309, 'Аналітика', 1),
(335, 1, 310, 'Тендери', 1),
(336, 1, 311, 'Всі тендери', 1),
(337, 1, 312, 'Створити', 1),
(338, 1, 313, 'Валюти', 1),
(339, 1, 314, 'Типи транспорта', 1);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_User_Car_Types1_idx` (`transportTypeID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Request_Prices1_idx` (`priceID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `type` (`type`,`geoID`,`requestID`,`groupNumber`),
  ADD KEY `fk_Frontend_Cargo_to_Geo_Frontend_Geo1_idx` (`geoID`),
  ADD KEY `fk_Frontend_Cargo_to_Geo_Frontend_Cargo1_idx` (`requestID`);

--
-- Індекси таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `text` (`text`(255)),
  ADD KEY `companyID` (`companyID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `cargoID` (`cargoID`),
  ADD KEY `transportID` (`transportID`);

--
-- Індекси таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Companies_Frontend_User_Types1_idx` (`typeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Company_Ownership_Types1_idx` (`ownershipTypeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Locales1_idx` (`primaryLocaleID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Phonecodes1_idx` (`phoneCodeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Phonecodes2_idx` (`phoneStationaryCodeID`);

--
-- Індекси таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Comany_Locales_Frontend_Companies1_idx` (`companyID`),
  ADD KEY `fk_Frontend_Comany_Locales_Frontend_Locales1_idx` (`localeID`);

--
-- Індекси таблиці `Frontend_Company_Ownership_Types`
--
ALTER TABLE `Frontend_Company_Ownership_Types`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Індекси таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Unique_Index` (`geoID`,`companyID`),
  ADD KEY `fk_Frontend_Company_to_Geo_Relation_Frontend_Geo1_idx` (`geoID`),
  ADD KEY `fk_Frontend_Company_to_Geo_Relation_Frontend_Companies1_idx` (`companyID`);

--
-- Індекси таблиці `Frontend_Company_Types`
--
ALTER TABLE `Frontend_Company_Types`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fromUserID` (`userID`),
  ADD KEY `complainCompanyID` (`complainCompanyID`);

--
-- Індекси таблиці `Frontend_Currencies`
--
ALTER TABLE `Frontend_Currencies`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Dialogs`
--
ALTER TABLE `Frontend_Dialogs`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `parentID` (`parentID`);

--
-- Індекси таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Geo_Locales_Frontend_Locales1_idx` (`localeID`),
  ADD KEY `fk_Frontend_Geo_Locales_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Locales`
--
ALTER TABLE `Frontend_Locales`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Locale_Vars_Frontend_Locales1_idx` (`localeID`);

--
-- Індекси таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fromAdminID` (`fromAdminID`),
  ADD KEY `fromUserID` (`fromUserID`),
  ADD KEY `toUserID` (`toUserID`),
  ADD KEY `transportID` (`transportID`),
  ADD KEY `cargoID` (`cargoID`),
  ADD KEY `tenderID` (`tenderID`),
  ADD KEY `dialogID` (`dialogID`);

--
-- Індекси таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `companyID` (`companyID`);

--
-- Індекси таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `newsID` (`newsID`),
  ADD KEY `localeID` (`localeID`);

--
-- Індекси таблиці `Frontend_Phonecodes`
--
ALTER TABLE `Frontend_Phonecodes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Індекси таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Registration_Links_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `priceCurrencyID` (`currencyID`);

--
-- Індекси таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_User_Car_Types1_idx` (`transportTypeID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Users1_idx` (`userID`),
  ADD KEY `priceCurrencyID` (`currencyID`);

--
-- Індекси таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `requestID` (`requestID`,`geoID`,`type`,`groupNumber`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Requests1_idx` (`requestID`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Transports_Frontend_Request_Prices1_idx` (`priceID`),
  ADD KEY `fk_Frontend_Transports_Frontend_User_Cars1_idx` (`userCarID`),
  ADD KEY `fk_Frontend_Transports_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `requestID` (`requestID`,`geoID`,`type`,`groupNumber`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Requests1_idx` (`requestID`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Users_Frontend_Companies1_idx` (`companyID`),
  ADD KEY `phoneCodeID` (`phoneCodeID`),
  ADD KEY `phoneStationaryCodeID` (`phoneStationaryCodeID`),
  ADD KEY `primaryLocaleID` (`primaryLocaleID`);

--
-- Індекси таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Cars_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_Cars_Frontend_User_Car_Types1_idx` (`carTypeID`);

--
-- Індекси таблиці `Frontend_User_Car_Types`
--
ALTER TABLE `Frontend_User_Car_Types`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Locales_Frontend_Locales1_idx` (`localeID`),
  ADD KEY `fk_Frontend_User_Locales_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Company_Socials_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1_idx` (`cargoID`);

--
-- Індекси таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Company_Ownership_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Company_Ownership_Frontend_Companies1_idx` (`companyID`);

--
-- Індекси таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1_idx` (`tenderID`);

--
-- Індекси таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Transport_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Transport_Requests_Frontend_Transports1_idx` (`transportID`);

--
-- Індекси таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Type_Rights_Frontend_User_Types1_idx` (`userTypeID`);

--
-- Індекси таблиці `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_Const` (`const`),
  ADD KEY `type` (`type`);

--
-- Індекси таблиці `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD UNIQUE KEY `i18nID` (`i18nID`,`localeID`),
  ADD KEY `fk_locale_idx` (`localeID`);

--
-- Індекси таблиці `x_admins`
--
ALTER TABLE `x_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `group_id` (`group_id`);

--
-- Індекси таблиці `x_admin_groups`
--
ALTER TABLE `x_admin_groups`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `x_admin_group_locales`
--
ALTER TABLE `x_admin_group_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_group_id` (`admin_group_id`),
  ADD KEY `plugin_lng_id` (`plugin_lng_id`);

--
-- Індекси таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Backend_Admin_Group_Rights_Backend_Admin_Group1_idx` (`adminGroupID`);

--
-- Індекси таблиці `x_admin_locales`
--
ALTER TABLE `x_admin_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `lng_id` (`lng_id`);

--
-- Індекси таблиці `x_plugins`
--
ALTER TABLE `x_plugins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub` (`sub`);

--
-- Індекси таблиці `x_plugin_lngs`
--
ALTER TABLE `x_plugin_lngs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pref` (`pref`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Індекси таблиці `x_plugin_locales`
--
ALTER TABLE `x_plugin_locales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lng_id` (`plugin_lng_id`,`plugin_id`),
  ADD KEY `plugin_id` (`plugin_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=898;
--
-- AUTO_INCREMENT для таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5383;
--
-- AUTO_INCREMENT для таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=641;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1203;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Ownership_Types`
--
ALTER TABLE `Frontend_Company_Ownership_Types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1348;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Types`
--
ALTER TABLE `Frontend_Company_Types`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Currencies`
--
ALTER TABLE `Frontend_Currencies`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `Frontend_Dialogs`
--
ALTER TABLE `Frontend_Dialogs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1259;
--
-- AUTO_INCREMENT для таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1259;
--
-- AUTO_INCREMENT для таблиці `Frontend_Locales`
--
ALTER TABLE `Frontend_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Phonecodes`
--
ALTER TABLE `Frontend_Phonecodes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1486;
--
-- AUTO_INCREMENT для таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=580;
--
-- AUTO_INCREMENT для таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3466;
--
-- AUTO_INCREMENT для таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3074;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Car_Types`
--
ALTER TABLE `Frontend_User_Car_Types`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6105;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=597;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=409;
--
-- AUTO_INCREMENT для таблиці `x_admins`
--
ALTER TABLE `x_admins`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблиці `x_admin_groups`
--
ALTER TABLE `x_admin_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `x_admin_group_locales`
--
ALTER TABLE `x_admin_group_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `x_admin_locales`
--
ALTER TABLE `x_admin_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблиці `x_plugins`
--
ALTER TABLE `x_plugins`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=315;
--
-- AUTO_INCREMENT для таблиці `x_plugin_lngs`
--
ALTER TABLE `x_plugin_lngs`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `x_plugin_locales`
--
ALTER TABLE `x_plugin_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=340;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_Request_Prices1` FOREIGN KEY (`priceID`) REFERENCES `Frontend_Request_Prices` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_User_Car_Types1` FOREIGN KEY (`transportTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Cargo_to_Geo_Frontend_Cargo1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_to_Geo_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  ADD CONSTRAINT `Frontend_Comments_ibfk_1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_3` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_4` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  ADD CONSTRAINT `fk_Frontend_Companies_Frontend_User_Types1` FOREIGN KEY (`typeID`) REFERENCES `Frontend_Company_Types` (`ID`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_1` FOREIGN KEY (`ownershipTypeID`) REFERENCES `Frontend_Company_Ownership_Types` (`id`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_2` FOREIGN KEY (`primaryLocaleID`) REFERENCES `Frontend_Locales` (`ID`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_3` FOREIGN KEY (`phoneCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_4` FOREIGN KEY (`phoneStationaryCodeID`) REFERENCES `Frontend_Phonecodes` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  ADD CONSTRAINT `fk_Frontend_Comany_Locales_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `Frontend_Company_Locales_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Company_to_Geo_Relation_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Company_to_Geo_Relation_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  ADD CONSTRAINT `Frontend_Complaints_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Complaints_ibfk_3` FOREIGN KEY (`complainCompanyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  ADD CONSTRAINT `FK_parentID_GEO` FOREIGN KEY (`parentID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  ADD CONSTRAINT `fk_Frontend_Geo_Locales_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Geo_Locales_Frontend_Locales1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  ADD CONSTRAINT `Frontend_Locale_Vars_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  ADD CONSTRAINT `Frontend_Messages_ibfk_7` FOREIGN KEY (`dialogID`) REFERENCES `Frontend_Dialogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_1` FOREIGN KEY (`fromAdminID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_2` FOREIGN KEY (`fromUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_3` FOREIGN KEY (`toUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_4` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_5` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_6` FOREIGN KEY (`tenderID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  ADD CONSTRAINT `Frontend_News_ibfk_2` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_News_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  ADD CONSTRAINT `Frontend_News_Locales_ibfk_1` FOREIGN KEY (`newsID`) REFERENCES `Frontend_News` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_News_Locales_ibfk_2` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  ADD CONSTRAINT `fk_Frontend_Registration_Links_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  ADD CONSTRAINT `Frontend_Request_Prices_ibfk_1` FOREIGN KEY (`currencyID`) REFERENCES `Frontend_Currencies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  ADD CONSTRAINT `Frontend_Tenders_ibfk_1` FOREIGN KEY (`transportTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_ibfk_3` FOREIGN KEY (`currencyID`) REFERENCES `Frontend_Currencies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  ADD CONSTRAINT `Frontend_Tenders_to_Geo_Relation_ibfk_2` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_to_Geo_Relation_ibfk_1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_Request_Prices1` FOREIGN KEY (`priceID`) REFERENCES `Frontend_Request_Prices` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_User_Cars1` FOREIGN KEY (`userCarID`) REFERENCES `Frontend_User_Cars` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Request_to_Geo_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Request_to_Geo_Frontend_Requests1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  ADD CONSTRAINT `fk_Frontend_Users_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`),
  ADD CONSTRAINT `Frontend_Users_ibfk_1` FOREIGN KEY (`phoneCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Users_ibfk_2` FOREIGN KEY (`phoneStationaryCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Users_ibfk_3` FOREIGN KEY (`primaryLocaleID`) REFERENCES `Frontend_Locales` (`ID`);

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  ADD CONSTRAINT `fk_Frontend_User_Cars_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_Cars_Frontend_User_Car_Types1` FOREIGN KEY (`carTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  ADD CONSTRAINT `fk_Frontend_User_Locales_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `Frontend_User_Locales_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  ADD CONSTRAINT `fk_Frontend_Company_Socials_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  ADD CONSTRAINT `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  ADD CONSTRAINT `fk_Frontend_User_to_Company_Ownership_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Company_Ownership_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  ADD CONSTRAINT `Frontend_User_to_Tender_Requests_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_User_to_Tender_Requests_ibfk_2` FOREIGN KEY (`tenderID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  ADD CONSTRAINT `fk_Frontend_User_to_Transport_Requests_Frontend_Transports1` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Transport_Requests_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  ADD CONSTRAINT `fk_Frontend_User_Type_Rights_Frontend_User_Types1` FOREIGN KEY (`userTypeID`) REFERENCES `Frontend_Company_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD CONSTRAINT `fk_i18n_idx` FOREIGN KEY (`i18nID`) REFERENCES `i18n` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_locale_idx` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `x_admins`
--
ALTER TABLE `x_admins`
  ADD CONSTRAINT `x_admins_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `x_admin_groups` (`id`) ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  ADD CONSTRAINT `fk_Backend_Admin_Group_Rights_Backend_Admin_Group1` FOREIGN KEY (`adminGroupID`) REFERENCES `x_admin_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
