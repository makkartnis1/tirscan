<?php

/**
 * Created by
 * User: LINKeR
 * Date: 25.04.16
 * Time: 17:57
 */
class Frontend_LocaleTab
{

    /**
     * @var array
     */
    protected static $locales = null;
    /**
     * @var string
     */
    protected static $pills = null;

    /**
     * @return array массив локалізацій сайту
     */
    public static function getLocales(){

        if(self::$locales === null)
            self::$locales = DB::select()->from('Frontend_Locales')->where('status','=','active')->execute()->as_array();

        return self::$locales;

    }

    /**
     * @return string       Згенерований вивід "переключалок" локалізацій
     */
    public static function getPills(){

        if(self::$pills === null){

            self::$pills = [];
            $active = true;

            $loc = self::getLocales();

            foreach ($loc as $locale){

                self::$pills[] = View::factory('frontend/content/profile/component/localisation-tab/pill',[
                    'class'     =>      $locale['uri'],
                    'caption'   =>      $locale['title'],
                    'active'    =>      $active
                ]);

                if($active)
                    $active = false;

            }

            self::$pills = implode(PHP_EOL, self::$pills);
            self::$pills = View::factory('frontend/content/profile/component/localisation-tab/pill-shell')->set('pills',  self::$pills)->render();

        }

        return self::$pills;

    }

    /**
     * @param View $view            Вьюшка в яку проініціалізовано все, крім $localeID
     * @param $replace string       Підстрока яку заміняти на
     * @return string               Массив відрендерених вьюшок із який генеруються блоки для переключення контенту
     */
    public static function getContent(View $view, $replace){

        $loc = self::getLocales();

        $result = [];
        $active = true;

        foreach ($loc as $locale){

            $curTab = $view;

            $curTab = View::factory('frontend/content/profile/component/localisation-tab/content', [
                'class'     => $locale['uri'],
                'content'   => $curTab,
                'active'    => $active,
            ])->render();
            
            
            $curTab = str_replace($replace, $locale['ID'], $curTab);

            $result[] = $curTab;

            if($active)
                $active = false;

        }

        $result = implode(PHP_EOL, $result);

        return View::factory('frontend/content/profile/component/localisation-tab/content-shell', ['tabs'=>$result])->render();

    }

}