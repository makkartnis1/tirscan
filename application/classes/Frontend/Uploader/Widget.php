<?php defined('SYSPATH') or die('No direct script access.');


class Frontend_Uploader_Widget {

    const PARAM_PRIVATE_KEY = 'asdlkasdnvkan e lbf aej ';
    
    protected $_upload          = false;
    protected $_download        = false;
    protected $_delete          = false;
//    protected $_read            = false;
    protected $_show_preview    = false;
    /**
     * @var bool
     */
    protected $_using_token     = false;

    protected $title = null;
    
    public $token_add;
    public $token_config;
    
    public $list;
    
    public $rootDir;
    public $dir;
    public $path;
    public $id;
    
    protected $_caption_download    = null;
    protected $_caption_delete      = null;
    protected $_caption_attach      = null;

    protected $response = 'linker/uploader/layout';
    
    protected $config = null;

    public static function factory($title, $config){
        
        return new self($title, $config);
        
    }
    
    public function __construct($title, $config){
        
        $this->title = $title;
        $this->config = $config;

        $this->response = View::factory( $this->response );
        
    }
    
    public function render(){

        $this->response->set([
            'title'     => $this->title,
            'block'     => [
                'files'     => $this->_file_block(),
                'upload'    => $this->_attach_block(),
            ],
            'params'    => $this->params_encode(),
        ]);
        
        return $this->response->render();

    }

    protected function get_param_hash( $json ){

        return md5(

            md5( Frontend_Auth::token() . self::PARAM_PRIVATE_KEY ) .

            md5( $json ) .

            self::PARAM_PRIVATE_KEY

        );

    }

    /**
     * Ця фукнція - для генерації даних для ініціалізації віджета в середині роута,
     * а саме на action()-ах для модифікації файлів (вигрузка і видалення),
     * щоб ненароком не засвітити токени на видалення чи токени на вигрузку
     *
     * @return string
     */
    protected function params_encode(){

        $params = [

            'captions'  => [$this->_caption_attach, $this->_caption_delete, $this->_caption_download],
            'tokens'    => [$this->token_add, $this->token_config], // token delete, token delete - є в самому віджеті

            'options'   => [
                //                      YES  NO
                ($this->_show_preview)  ? 1 : 0,
                ($this->_using_token)   ? 1 : 0,
            ],

        ];

        if( $this->_using_token ){

            if(Frontend_Auth::token() === false){
                throw HTTP_Exception::factory(500, 'Not authorized users can not use token!');
            }

            $params['auth']  =  [

                ($this->_download)      ? 1 : 0,
                ($this->_delete)        ? 1 : 0,
                ($this->_upload)        ? 1 : 0,

            ];

        }

        // така маніпуляція - щоб JSON не мав ніяких зайвих симовлів (просто перестраховка )
        $params_json = json_encode( json_decode( json_encode($params), true ) );

        $result = [

            'params'    => $params_json,

        ];

        if(array_key_exists('auth', $params)){

            $result[ 'key' ]   = $this->get_param_hash( $params_json );
        }

        return json_encode( json_decode( json_encode( $result ), true ) );


        /*
            ->captionAttach('attach [cstm]')
            ->captionDelete('delete [cstm]')
            ->captionDownload('downlad [cstm]')

            ->showPreviewIfPossible(true)

            ->usingToken(true)

            ->download(true)
            ->delete(true)
            ->upload(true)
        */

    }

    /**
     * @param $param_json
     * @param Frontend_Uploader_Widget $widget
     * @throws HTTP_Exception
     */
    public function params_apply($param_json){


        $params = json_decode($param_json, true);

        $key    = $params['key'];
        $params = $params['params'];

        if($key !== $this->get_param_hash($params))
            $this->_throw_access_denied();

        $params = json_decode($params, true);

        list($this->_caption_attach, $this->_caption_delete, $this->_caption_download) = $params['captions'];
        list($this->token_add, $this->token_config) = $params['tokens'];

        $this->_show_preview    = (boolean) $params['options'][0];
        $this->_using_token     = (boolean) $params['options'][1];

        if(!empty($params['auth'])){

                $this->_download    = (boolean) $params['auth'][0];
                $this->_delete      = (boolean) $params['auth'][1];
                $this->_upload      = (boolean) $params['auth'][2];

        }
        
//         якщо не застосувався токен - то була помилка. так як цей токен має обовязково передаватись для ціїє діїі
//        if(empty($this->token_add))
//            $this->_throw_access_denied();


    }

    protected function _file_block(){

        $blocks = [];

        //var_dump($this->list);

        foreach ($this->list as $file){

            $blocks[] = View::factory('linker/uploader/block/file',[
                'block' => [
                    'file/icon'                 => $this->_file_icon($file),
                    'file/preview-url-block'    => $this->_preview_url($file),
                    'file/delete-block'         => $this->_delete_block($file),
                    'file/download-block'       => $this->_download_block($file),
                ]
            ]);

        }

        return implode(PHP_EOL, $blocks);

    }

    protected function _file_icon($file){

        if( ($this->_using_token) AND (Frontend_Auth::token() === false) ){
            $this->_throw_access_denied();
        }

        if($this->_show_preview and Frontend_Uploader::isBrowserRenderableImage($file['ext'])){

            // preview-шка картинок
            return View::factory('linker/uploader/block/file/icon-browser-renderable', [
                'preview_url'   => ($this->_using_token)
                    ? Route::url('linker/uploader/preview',                 ['token'=>$file['token/read'],
                        'location'=>$this->path, 'id'=> $this->id, 'rootDir' => $this->rootDir, 'file'=>$file['file']])
                    : $this->_direct_url_for_no_token($file)
            ]);

        }else{

            // TODO перевірку на клаc підложки файловго типу для відображення, попросити Олега намалювати підложки на PDF, ZIP, DOC, XLS і т.д.
            // CSS стилізований аркуш символізуючий файл
            return View::factory('linker/uploader/block/file/icon-css-file-styled', [ 'extension'   => $file['ext'] ]);

        }
        
    }

    protected function _preview_url($file){

        if( $this->_using_token and empty($file['token/read']) )
            $this->_throw_access_denied();

        return View::factory('linker/uploader/block/file/preview-url',[

            'preview_url'   => ( $this->_using_token )
                ? Route::url('linker/uploader/preview',                 ['token'=>$file['token/read'],
                    'location'=>$this->path, 'id'=> $this->id, 'rootDir' => $this->rootDir, 'file'=>$file['file']])
                : $this->_direct_url_for_no_token($file),
            'name'   => $file['name'],
        ]);

    }

    protected function _direct_url_for_no_token($file){
        return '/' . $this->rootDir . '' . $this->path . '/' . $this->id . '/' . $file['file'];
    }

    protected function _delete_block($file){

        if(!$this->_delete)
            return;

        if( $this->_using_token and empty($file['token/delete']) )
            $this->_throw_access_denied();

        return View::factory('linker/uploader/block/file/delete-block',[
            'id'                => $this->id,
            'location'          => $this->path,
            'rootDir'           => $this->rootDir,
            'fileName'          => $file['file'],
            'fileTokenDelete'   => $file['token/delete'],
            'text'              => empty($this->_caption_delete) ? __db('label.delete') : $this->_caption_delete,

        ]);

    }

    protected function _download_block($file){

        if(!$this->_download)
            return;


        if(!$this->_using_token)
            throw HTTP_Exception::factory(500, 'Only tokenized urls can generate force download urls');

        if(empty($file['token/read']))
            $this->_throw_access_denied();

        return View::factory('linker/uploader/block/file/download-block',[

            'download_url'          =>  Route::url('linker/uploader/download',                 ['token'=>$file['token/read'],
                'location'=>$this->path, 'id'=> $this->id, 'rootDir' => $this->rootDir, 'file'=>$file['file']]),

            'text'                  =>  empty($this->_caption_download) ? __db('label.download') : $this->_caption_download,

        ]);

    }

    protected function _attach_block(){

        if($this->_upload){

            if($this->_using_token){

                if(empty($this->token_add) || empty($this->token_config)){

                    $this->_throw_access_denied();

                }else{

                    return View::factory('linker/uploader/block/file/attach-block',[

                        'text'      => (empty($this->_caption_attach)) ? __db('label.attach') : $this->_caption_attach,
                        'upload'    => [
                            /*
                            
                            data-upload-token="<?=        $upload['token/add']     ?>"
                            data-upload-id="<?=           $upload['id']            ?>"
                            data-upload-location="<?=     $upload['location']      ?>"
                            data-upload-dir="<?=          $upload['dir']           ?>"
                            data-upload-config="<?=       $upload['config']        ?>"
                            data-upload-config-token="<?= $upload['token/config']  ?>"
                            
                            */
                            'token/add'     => $this->token_add,
                            'id'            => $this->id,
                            'location'      => $this->path,
                            'dir'           => $this->rootDir,
                            'config'        => $this->config,
                            'token/config'  => $this->token_config,
                        ]
                        
                    ]);

                }


            }else{

                $this->_throw_access_denied();

            }

        }

    }

    protected function _throw_access_denied(){

        throw HTTP_Exception::factory(403, 'File widget access denied');
    }

    /**
     * Чи виводити  блок "Прикрепить файл"
     * @param $showBlock boolean
     * @return $this
     */
    public function upload($showBlock){
        $this->_upload          = (boolean) $showBlock;
        return $this;
    }

    /**
     * Чи виводити  блок "Скачать"
     * @param $showBlock boolean
     * @return $this
     */
    public function download($showBlock){
        $this->_download        = (boolean) $showBlock;
        return $this;
    }

    /**
     * Чи виводити  блок "Удалить"
     * @param $showBlock boolean
     * @return $this
     */
    public function delete($showBlock){
        $this->_delete          = (boolean) $showBlock;
        return $this;
    }

//    /**
//     * @param $showBlock
//     * @return $this
//     */
//    public function read($showBlock){
//        $this->_read            = (boolean) $showBlock;
//        return $this;
//    }

    /**
     * Чи показувати піктограми картинок якщо можливо
     *
     * @param $showBlock boolean
     * @return $this
     */
    public function showPreviewIfPossible($showBlock){
        $this->_show_preview    = (boolean) $showBlock;
        return $this;
    }

    /**
     * Чи використовувати токен для ссилок на читання.
     * Можна робити FALSE, якщо буде віджет лише на відображення і ГІСТЬ має правво на перегляд.
     * Так як ТОКЕН генерується при авторизації - то роботи із токенами - ГІСТЬ не має права
     *
     * @param $boolean boolean
     * @return $this
     */
    public function usingToken($boolean){
        $this->_using_token         = (boolean) $boolean;
        return $this;
    }

    public function captionDownload($caption){
        $this->_caption_download    = (string) $caption;
        return $this;
    }

    public function captionAttach($caption){
        $this->_caption_attach      = (string) $caption;
        return $this;
    }

    public function captionDelete($caption){
        $this->_caption_delete      = (string) $caption;
        return $this;
    }




}