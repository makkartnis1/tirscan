<?php

/**
 * Класс для побудови елементів форми - забезпечить від дубляжу елементів.
 * Заміна одного інпута - поміняє вигляд інпутів по всьому сайту
 *
 * Created by
 * User: LINKeR
 * Date: 26.04.16
 * Time: 13:55
 */
class Frontend_FormElement
{

    /**
     * Функція повертає розмітку інпуту
     *
     * list($input..) приймає наступні значення:
     *
     *  - приклад для одного незалежного від локалізації інпута
     * $input = [
     *      'text',                         input[type=<text>]
     *      'Frontend_Users[icq]',          input[name=<Frontend_Users[icq]>]
     *      '',                             input[value=<value>]
     *      'placeholder.profile.icq',      input[placeholder=<placeholder>]
     *      null,                           Надпис над інпутом
     *      'profile-my-icq',               id для інпута
     *      false,                          чи дублювати і виводити в різни локалізаційних контейнерах
     *      true                            чи виводити в заголовку "*" necessarily
     * ]
     *
     * - приклад для групи "телефон і код"
     * $input = [
     *      'phone+code',
     *      'Frontend_Users[phone]/Frontend_Users[phoneCodeID]',
     *      '/',
     *      'myplaceholder',
     *      null,
     *      'profile-my-phone',
     *      false,
     *      false                           чи виводити в заголовку "*" necessarily
     * ]
     *
     * - приклад для групи "локалізаційно залежного поля"
     * $input = [
     *      'text',
     *      'Frontend_Users_Locale[%localeID%][name]',
     *      '',
     *      'check',
     *      null,
     *      'profile-my-name',
     *      '%localeID%'                    тут назва пістроки яка буде заміенна на ID локалізацї
     *      true                            чи виводити в заголовку "*" necessarily
     * ]
     *
     *
     * @param $input mixed
     * @return string|View
     *
     */
    public static function compile_input($input){

        Controller_Frontend_HMVC::_addJS('/assets/js/frontend/page/form_err_extension.js');

        if(is_array($input)){

            list(
                $curInput['type'],
                $curInput['name'],
                $curInput['value'],
                $curInput['placeholder'],
                $curInput['caption'],
                $curInput['id'],
                $localeDependent,
                $curInput['necessarily']
                ) = $input;

            if($localeDependent === false){

                return View::factory('frontend/content/profile/component/input', $curInput);

            }else{

                return Frontend_LocaleTab::getContent(
                    View::factory('frontend/content/profile/component/input', $curInput),
                    $localeDependent
                );

            }

        }else{

            return 'Unknow input passed as ergument passed to '.__CLASS__.'::'.__METHOD__.'()';
        }

    }
    
}