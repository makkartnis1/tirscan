<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Breadcrumbs_Link
 *
 * @author Сергій Krid
 */
class Frontend_Breadcrumbs_Link {

    protected $active;
    protected $title;
    protected $route;
    protected $params;

    public function getTitle() {
        return $this->title;
    }

    public function getHref() {
        return Route::url($this->route, $this->params);
    }

    public function isActive() {
        return $this->active;
    }

    /**
     * @param $active bool
     * @param $title
     * @param $route
     * @param $params
     */
    public function __construct($active, $title, $route, array $params = []) {
        $this->active = $active;
        $this->title = $title;
        $this->route = $route;
        $this->params = $params;
    }

}
