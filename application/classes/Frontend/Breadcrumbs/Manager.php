<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Breadcrumbs_Manager
 *
 * @author Сергій Krid
 */
class Frontend_Breadcrumbs_Manager {

    /**
     * @var Frontend_Breadcrumbs_Link[]
     */
    protected $links = [];

    /**
     * @param Frontend_Breadcrumbs_Link $link
     */
    public function addLink(Frontend_Breadcrumbs_Link $link) {
        $this->links[] = $link;
    }

    /**
     * @return Frontend_Breadcrumbs_Link[]
     */
    public function getLinks() {
        return $this->links;
    }

}
