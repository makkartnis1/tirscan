<?php

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 28.04.16
 * Time: 11:55
 */
class Frontend_Auth {

    public static function userID() {
        if (Session::instance()->get('short_session', false)) {
            return Session::instance()->get('user', false);
        }
        else {
            return Cookie::get('user', false);
        }
    }

    public static function prolongUserSession($userID) {
        if (!Session::instance()->get('short_session', false)) {
            self::authUser($userID, false);
        }
    }

    public static function authUser($userID, $remember_me = true) {
        if (!$remember_me) {
            Session::instance()->set('user', $userID);
            Session::instance()->set('short_session', true);
        }
        else {
            Cookie::set('user', $userID, Date::YEAR);
        }

        Cookie::set('tkn', self::session_token());
    }

    public static function token() {
        return Cookie::get('tkn', false);
    }

    public static function logoutUser() {
        // чистимо сесію
        Session::instance()->delete('user');
        Session::instance()->delete('short_session');

        // чистим кукі
        Cookie::delete('user');
    }


    protected static function session_token() {
        return md5(md5(md5((100 - rand(1, 99)) / 100)) . time());
    }

}