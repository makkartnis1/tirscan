<?php defined('SYSPATH') or die('No direct script access.');


/**
 * Class Frontend_Uploader
 *
 *
 *      Розміщення фійлів АВТО:                                                           ID тарнспортного засобу
 *                          /uploads/files /private|public /cars    /number/              {{ID}}  / __FILE__
 *                          /uploads/files /private|public /cars    /docs/                {{ID}}  / __FILE__
 *                          /uploads/files /private|public /cars    /docs/                {{ID}}  / __FILE__
 *
 *      Розміщення фійлів КОМПАНІЇ:                                                     ID компанії
 *                          /uploads/files /private|public /company /podatkovi/           {{ID}}  / __FILE__
 *                          /uploads/files /private|public /company /sertifikaty/         {{ID}}  / __FILE__
 *                          /uploads/files /private|public /company /svidoctva/           {{ID}}  / __FILE__
 *
 */
class Frontend_Uploader{

    protected static $types = null;
    protected static $f_info = null;

    protected $rootDir = null;

    const SECURITY_PRIVATE_KEY = '2gvehjimkrgoofrekm3fvdewled';

    const TOKEN_READ        = 'tkn/read';
    const TOKEN_DELETE      = 'tkn/del';
    const TOKEN_ADD         = 'tkn/add';
    const TOKEN_CONFIG      = 'tkn/config';

    CONST DIR_UPL                                   = 'uploads';
//    CONST DIR_FILES                                 = '/files/';

    CONST DIR_VISIBLE_PRIVATE                       = '/files/private/';
    CONST DIR_VISIBLE_PUBLIC                        = '/files/public/';

    CONST DIR_COMPANY_TAX_DOCUMENTS                 = 'company/podatkovi';
    CONST DIR_COMPANY_CERTIFICATE_DOCUMENTS         = 'company/sertifikaty';
    CONST DIR_COMPANY_LICENSE_TRANSPORT_N_EXPEDITION= 'company/license-transport-and-expedition';
    CONST DIR_COMPANY_CERT_STATE_REGISTRATION       = 'company/cert-state-reg';
    CONST DIR_COMPANY_PROVE_DOCUMENTS               = 'company/svidoctva';

    CONST DIR_CAR_DOCS                              = 'car/docs';
    CONST DIR_CAR_NUMBERS                           = 'car/numbers';
    CONST DIR_CAR_GALLERY                           = 'car/gallery';
    CONST DIR_USER_AVATAR                           = 'user/avatar';
    CONST DIR_NEWS_IMG                              = 'news/images';

    protected $_path    = null;
    protected $_id      = null;
    protected $_dir     = null;

    /**
     * @param $path
     * @param $id
     * @param string $rootDir
     * @return Frontend_Uploader
     * * Приклад використання
     *
     * $widget = Frontend_Uploader::factory('/files/cars/docs', $id = 3, 'test-files' )
     *      ->widget('My car docs [custom title]')
     *
     *      ->captionAttach('attach [cstm]')
     *      ->captionDelete('delete [cstm]')
     *      ->captionDownload('downlad [cstm]')
     *
     *      ->showPreviewIfPossible(true)
     *
     *      ->usingToken(true)
     *
     *      ->download(true)
     *      ->delete(true)
     *      ->upload(true)
     *
     *      ->render();
     *
     */
    public static function factory($path, $id, $rootDir = Frontend_Uploader::DIR_VISIBLE_PRIVATE){
        return new self($path, $id, $rootDir);
    } // factory - щоб chain працював відразу

    protected function __construct($path, $id, $rootDir){
        $this->_path    = $path;
        $this->_id      = $id;
        $this->rootDir = $rootDir;
        $this->_dir     = $rootDir.$this->_path . '/' . $this->_id;
    }

    protected function finfo($file){
        if(self::$f_info === null)
            self::$f_info = new finfo(FILEINFO_MIME);
        return self::$f_info->file($file);
    }

    protected static function _ext($mime_type){

        if(self::$types === null)
            self::$types = Kohana::$config->load('frontend/file-types')->as_array();

        foreach (self::$types as  $ext => $type){

            if($type == $mime_type)
                return $ext;

        }

        return false;

    }

    public static function init_as_userAvatar($userID){

        return Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PUBLIC.Frontend_Uploader::DIR_USER_AVATAR,
            $userID,
            Frontend_Uploader::DIR_UPL      //      'uploads'

        );

    }

    public static function init_as_newsPreviewImage($newsID){

        return Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PUBLIC.Frontend_Uploader::DIR_NEWS_IMG,
            $newsID,
            Frontend_Uploader::DIR_UPL      //      'uploads'

        );

    }

    public function scanPath(){

        $directory = $_SERVER['DOCUMENT_ROOT'].'/'.$this->_dir;

        $files = (is_dir($directory))
            ? array_diff(scandir($directory), ['..', '.'])
            : [];

        foreach ( $files as &$file){

            $filename = $file;

            $mime_type = strtolower($this->fileType($file));

            $file = [

                'file'              => $filename,
                'name'              => substr_replace($file , '', strrpos($file , '.')),
                'type'              => $mime_type,
                'ext'               => $this->_ext($mime_type),
                'location'          => '/'.$this->_dir // це для JS функцій. по факту

            ];
            
            unset($file);

        }

        return array_values( $files );


    }

    protected function readPath(){

        $files = $this->scanPath();

        foreach ($files as &$file){

            //            if( $userID = Frontend_Auth::userID() === false )
            //                throw HTTP_Exception::factory(403); // fix на неавторизваний доступ до блоку управління файлами


            if( ($userID = Frontend_Auth::userID()) !== false ){

                $file['token/read']     = $this->tokenizer(self::TOKEN_READ,   $file['file']);

                $file['token/delete']   = $this->tokenizer(self::TOKEN_DELETE, $file['file']);

            }

            unset($file);

        }

        return $files;

    }
    
    public function hasFile($file_with_extension){
        
        $files = $this->readPath();

        foreach ($files as $file) {
            if($file['file'] == $file_with_extension)
                return true;
        }

        return false;
        
    }

    protected function fileType($file){

        /** @url http://php.net/manual/ru/function.finfo-file.php */

        $file_full_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->_dir .'/' . $file;

        $type = self::finfo($file_full_path);

        # 'some doc.rtf' => "text/rtf; charset=us-ascii";

        return ( ($pos = strpos($type, ';')) === false)
            ? $type
            : substr($type, 0, $pos);

    }

    protected function tokenizer($type, $fileOrConfig = null){

        #   Token на папку для поточночго користувача

        $token = md5( self::SECURITY_PRIVATE_KEY . md5(Frontend_Auth::token() . $this->_dir));

        switch ($type){

            case ( self::TOKEN_ADD ):       return md5( $token . 'add');                    #   Token на добавлення файлу в папку

            case ( self::TOKEN_READ ):      return md5( $token . 'get' . $fileOrConfig);    #   Token на читання файлу з папки

            case ( self::TOKEN_DELETE ):    return md5( $token . 'del' . $fileOrConfig);    #   Token на видалення файлу з папки

            case ( self::TOKEN_CONFIG ):    return md5(
                $this->tokenizer(self::TOKEN_ADD) . 'conf' . $fileOrConfig                 #   Token на використання конфігураційного файлу
            );

            default:
                throw HTTP_Exception::factory(500, 'Undefined tokenizer type');

        }

    }

    public function widget($title, $config = 'default'){
        
        $widget = Frontend_Uploader_Widget::factory($title, $config);
        
        $widget->token_add      = $this->tokenizer(self::TOKEN_ADD);
        $widget->token_config   = $this->tokenizer(self::TOKEN_CONFIG, $config);
        $widget->list           = $this->readPath();
        $widget->rootDir        = $this->rootDir;
        $widget->dir            = $this->_dir;
        $widget->path           = $this->_path;
        $widget->id             = $this->_id;
        
        return $widget;
        

//        return View::factory('debug/uploader/manage-layout')->set([
//
//            'title'         => $title,
//            'list'          => $this->readPath(),
//            'rootDir'       => $this->rootDir,
//            'dir'           => $this->_dir,
//            'location'      => $this->_path,
//            'id'            => $this->_id,
//            'upload'      => [
                /*

                  data-upload-token="<?=        $upload['token/add']     ?>"
                  data-upload-id="<?=           $upload['id']            ?>"
                  data-upload-location="<?=     $upload['location']      ?>"
                  data-upload-dir="<?=          $upload['dir']           ?>"
                  data-upload-config="<?=       $upload['config']        ?>"
                  data-upload-config-token="<?= $upload['token/config']  ?>"

                 */
//                'token/add'     => $this->tokenizer(self::TOKEN_ADD),
//                'id'            => $this->_id,
//                'location'      => $this->_path,
//                'dir'           => $this->rootDir,
//                'config'        => $config,
//                'token/config'  => $this->tokenizer(self::TOKEN_CONFIG, $config),
//
//            ],
//
//        ]);
    }

    public static function isBrowserRenderableImage($ext){

        return in_array( strtolower($ext), [ 'jpg', 'png', 'gif', 'bmp' ] );

    }

    protected function _init_file_action($token, $file, $type){

        $correct_token = $this->tokenizer($type, $file);

        if( $token !== $correct_token)
            throw HTTP_Exception::factory(403);

        $file_full_path = $_SERVER['DOCUMENT_ROOT'] .'/'. $this->_dir .'/' . $file;

        if(!is_file($file_full_path))
            throw HTTP_Exception::factory(404);

        return $file_full_path;

    }
    
    public function get_file_location($token, $file, $tokenType){

        return $file_full_path = $this->_init_file_action($token, $file, $tokenType);

    }

    public function save($token, $config, $config_token){

        $result = [
            'success'   => [],
            'warning'   => [],
            'error'     => [],
        ];

        $ok = true;

        if($this->tokenizer(self::TOKEN_ADD) !== $token){

            $ok = false;

            $result['error'][] = 'security.error.token.missmatch.base';
        }

        if($this->tokenizer(self::TOKEN_CONFIG, $config) !== $config_token){
            $ok = false;

            $result['error'][] = 'security.error.token.missmatch.config';
        }

        if($_FILES['file']['error'] !== 0){

            $result['warning'][] = 'file.upload.error.try.again.later';

            return $result;

        }

        $conf = Kohana::$config->load('uploader/settings/' . $config)->as_array();

        if( isset($conf['size']) AND $conf['size'] ){

            if($_FILES['file']['size'] >= $conf['size']){

                $ok = false;

                $result['error'][] = [
                    'sizeErr'   => 'file.size.limit.[:lim-size:]([:lim-unit:]).current.[:cur-size:]([:cur-unit:])',
                    'size'      => [
                        'cur'       => $_FILES['file']['size'],
                        'lim'       => $conf['size'],
                    ],
                ];

            }

        }

        if(!empty($conf['mime']) AND is_array($conf['mime'])){

            $curMime = strtolower($_FILES['file']['type']);


            if(!in_array($curMime, $conf['mime'])){

                $ok = false;

                $result['error'][] = [
                    'mime'  => 'file.mime.invalid.type.enabled.[:enabled:]',
                    'replace'   => ['[:enabled:]'   => $conf['mime']],
                ];
            }

        }

        $ext = strtolower( $this->_ext( strtolower($_FILES['file']['type']) ) );

        if($ext === false){

            $ok = false;

            $result['error'][] = 'file.extenstoin.could.not.establish';

        }


        if($ok){

            $dir_to_save  = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->_dir . '/';

            $file = $_FILES['file']['name'];

            $iterator = 0;

            $f = explode('.',$file);

            $user_ext = strtolower(array_pop($f));

            $file = implode('.', $f);

            if($user_ext != $ext){ // якщо розширення відрізняється - то повернемо його назад

                $file .= '.'.$user_ext;

            }



            $orig_name = $file = '' . Transliterate::factory($file)->replaceSpaces('-')->lowercase()->limit(200) ;

            if( is_file($dir_to_save . $file . '.' . $ext) ){

                while( is_file($dir_to_save . $file . '.' . $ext) ){

                    if( $iterator !== 0){

                        $file = $orig_name . " ({$iterator})";

                    }

                    $iterator++;

                }

            }

            if(!is_dir($dir_to_save))
                mkdir($dir_to_save, 0777, true);



            if(move_uploaded_file($_FILES['file']['tmp_name'], $dir_to_save . $file . '.' . $ext)){

                $after = Arr::get($conf, 'after',[]);

                $ok = true;

                if(array_key_exists('save', $after)){
                    $after_save = $after['save']($dir_to_save . $file . '.' . $ext);

                    if($after_save !== null)
                        $result['error'][] = $after_save;

                    $ok = $ok AND ($after_save === null);

                }

                if($ok){

                    $result['success'][] = ['save' => ['file.upload.success-[:filename:]', $file . '.' . $ext]];

                }



            }else{

                $ok = false;

                $result['error'][] = 'file.upload.error.try.again.later';

            }

        }

        return [$ok, $result];

    }


}