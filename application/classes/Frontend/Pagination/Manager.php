<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Pagination_Manager
 *
 * @author Сергій Krid
 */
class Frontend_Pagination_Manager {

    /** @var string */
    protected $url_template;

    /** @var int */
    protected $page_size;

    /** @var int */
    protected $page_count;

    /** @var int */
    protected $page_group_length;

    /**
     * @param $objects_count int
     * @param $url_template string
     * @param $page_size int
     * @param $page_group_length int
     */
    public function __construct($objects_count, $url_template, $page_size, $page_group_length = 2) {
        $this->url_template = $url_template;
        $this->page_size = $page_size;
        $this->page_group_length = $page_group_length;

        $this->page_count = (int) ceil($objects_count / $page_size);
    }

    public function getPageCount() {
        return $this->page_count;
    }

    protected function formEtcLink() {
        return [
            'type' => 'etc',
            'title' => '...'
        ];
    }

    protected function paginationUrl($page_number) {
        $get_str = '';
        if (count(Request::current()->query()))
            $get_str = '?' . http_build_query(Request::current()->query());

        return str_replace('xPAGEx', $page_number, $this->url_template) . $get_str;
    }

    protected function formPageLink($page_number, $current_page) {
        $url = $this->paginationUrl($page_number);
        $active = ($page_number == $current_page);

        return [
            'type' => 'page',
            'active' => $active,
            'number' => $page_number,
            'url' => $url
        ];
    }

    /**
     * @param $page
     * @return array
     */
    public function getPage($page) {
        $pages = [];

        $flag_added_etc = false;
        for ($i = 1; $i <= $this->page_count; $i++) {
            if (
                // якщо це перша або остання сторінка
                ($i == 1) || ($i == $this->page_count) ||
                // або якщо сторінка попадає в заданий діапазон навколо активної
                ( ( $i <= ($page + $this->page_group_length) ) && ( $i >= ($page - $this->page_group_length) ) )
            ) {
                $pages[] = $this->formPageLink($i, $page); // то додаємо сторінку
                $flag_added_etc = false;
            }
            else if (!$flag_added_etc) { // інакше якщо ще не вставили заглушку
                $pages[] = $this->formEtcLink(); // то додаємо заглушку
                $flag_added_etc = true;
            }
        }

        $previous = [
            'active' => ($page > 1),
            'url' => $this->paginationUrl($page - 1)
        ];
        $next = [
            'active' => ($page < $this->page_count),
            'url' => $this->paginationUrl($page + 1)
        ];

        return [
            'pages' => $pages,
            'arrows' => compact('previous', 'next')
        ];
    }

}