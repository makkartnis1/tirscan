<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Helper_Arr
 *
 * @author Сергій Krid
 */
class Frontend_Helper_Arr extends Exception {

    public static function countSub(array $array) {
        $count = 0;
        foreach ($array as $item) {
            if (is_array($item))
                $count += self::countSub($item);
            else
                $count++;
        }

        return $count;
    }

    public static function array_has_all_empty_fields(array $array){

        $ok = true;

        foreach ($array as $val) {

            if($val != ''){

                $ok = false;

                break;

            }

        }

        return $ok;

    }

}
