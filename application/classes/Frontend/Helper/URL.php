<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Helper_URL
 *
 * @author Сергій Krid
 */
class Frontend_Helper_URL {

    public static function title($string) {
        $result =  URL::title( transliterator_transliterate( 'Any-Latin; Latin-ASCII; Lower()', $string ) );
        $result = str_replace('ʹ', '', $result);

        return $result;
    }

    public static function randomString($length = 32) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters_length = strlen($characters);

        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= $characters[rand(0, $characters_length - 1)];
        }

        return $result;
    }

}
