<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Helper_Mail
 *
 * @author Сергій Krid
 */
class Frontend_Helper_Mail {

    public static function send_mail($email, $title, $template, $data) {
        $view_path = 'frontend/templates/mail/' . $template;
        $mail = View::factory($view_path, $data)->render();

        $headers = 'From: support@tirscan.com' . "\r\n" .
            'Reply-To: support@tirscan.com' . "\r\n" .
            'CC: susan@example.com' . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-Type: text/html; charset=UTF-8' . "\r\n";
            //'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";

        mail($email, $title, $mail, $headers);
    }

}
