<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Helper_String
 *
 * @author Сергій Krid
 */
class Frontend_Helper_String {

    public static function substr_if_more($string, $length, $use_dots = true) {
        if (strlen($string) > $length) {
            if ($use_dots) {
                return mb_substr($string, 0, $length) . '...';
            }
            else {
                return mb_substr($string, 0, $length);
            }
        }
        else {
            return $string;
        }
    }

    public static function none_if_null($value, $result) {
        if ($value == null) {
            return '-';
        }
        else {
            return $result;
        }
    }

    public static function request_concat_info(array $request, $is_cargo = false) {
        $config = (array)Kohana::$config->load('frontend/requests/info');
        if ($is_cargo) {
            $locales = $config['cargo']['db_locales'];
        }
        else {
            $locales = $config['transport']['db_locales'];
        }

        arsort($config['weights']);
        $result   = '';
        $length   = 0;
        $br_count = 0;
        foreach ($config['weights'] as $key => $weight) {
            if (!array_key_exists($key, $request) || !array_key_exists($key, $locales)) {
                continue;
            }

            $concat = $request[$key];
            if (($concat == 'no') || ($concat == null)) {
                continue;
            }
            $concat = __db($locales[$key], [':value:' => $concat]);
            if (strlen($result) == 0) {
                $result .= $concat;
                $length = strlen($concat);
            }
            else {
                if ($length < 30) {
                    $result .= ',' . $concat;
                    $length += strlen(',' . $concat);
                }
                else {
                    if ($br_count > 0) {
                        break;
                    }

                    $result .= '<br>' . $concat;
                    $length = 0;
                    $br_count++;
                }
            }
        }

        return $result;
    }

}
