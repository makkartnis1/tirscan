<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Frontend_Stickers_Manager
 *
 * @author Сергій Krid
 */
class Frontend_Stickers_Manager {

    protected $_default_view = 'frontend/blocks/stickers/request_form';
    protected $_allowed_keys = [];

    /** @var View */
    protected $_view;

    public function __construct(array $allowed_keys, View $view = null) {
        if ($view == null) {
            $this->_view = View::factory($this->_default_view);
        }

        $this->_allowed_keys = $allowed_keys;
    }

    protected function placeValuesFromQuery($template, $query) {
        $result = $template;
        foreach ($query as $key => $value) {
            $result = str_replace(':' . $key . ':', $value, $result);
            if (strpos($result, ':!' . $key . ':') !== false) {
                $result = str_replace(':!' . $key . ':', __db($value), $result);
            }
        }

        return $result;
    }

    public function get(array $query) {
        $keys_in_query = [];
        foreach ($query as $key => $val) {
            if ($val == null) {
                continue;
            }

            if (array_key_exists($key, $this->_allowed_keys)) {
                $keys_in_query[$key] = $this->_allowed_keys[$key];
            }
        }

        $stickers = [];
        foreach ($keys_in_query as $key => $params) {
            $value = $this->placeValuesFromQuery(Arr::get($params, 'value', ''), $query);
            $text = __db(Arr::get($params, 'i18n', ''));
            if ($value != '') {
                $text .= ': ' . $value;
            }

            $stickers[] = [
                'text'  => $text,
                'clear' => Arr::get($params, 'clear', '')
            ];
        }

        $this->_view->set('stickers', $stickers);

        return $this->_view->render();
    }

    public function show(array $query) {
        echo $this->get($query);
    }

}