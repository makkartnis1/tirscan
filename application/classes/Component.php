<?php defined('SYSPATH') or die('No direct script access.');

class Component
{
    public static function cms($name,$function,$lang,$parameters = array())
    {
        if (!Helper_Myauth::isLogin(1))
        {
            die();
        }

      Request::factory('component_cms/'.$lang.'/'.$name.'/'.$function)->method(Request::POST)->post($parameters)->execute();
    }

}