<?php defined('SYSPATH') or die('No direct script access.');

class Helper_HTML extends Kohana_HTML
{


    public static function getFileType($name)
    {
        $exp = explode('.',$name);
        return array_pop($exp);
    }

}