<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Dir {

    public static function getFilesFromDir($dir)
    {
        if (file_exists($dir))
        {
            $files = scandir($dir);
            if ($files)
            {
                foreach($files as $key => $val)
                {
                    if ($val=='.' || $val=='..') unset($files[$key]);
                }
                if (!empty($files))
                {
                    $new_files = array();
                    foreach($files as $f)
                    {
                        $new_files[] = $f;
                    }
                    return $new_files;
                }
                return null;
            }
            return null;
        }
        return null;
    }


    public static function removeDirectory($dir) {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }
}