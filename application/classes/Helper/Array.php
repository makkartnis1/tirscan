<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Array {

    //отримання масиву з вказаним ключом
    public static function change_keys($arr,$key_name)
    {
        $new_arr = array();
        foreach($arr as $a)
        {
            $new_arr[$a[$key_name]] = $a;
        }
        if (!empty($new_arr)) return $new_arr;
        return null;
    }

    //отримання масиву елементів за вказаним ключем
    public static function get_array_by_key($arr,$key_name)
    {
        $new_arr = array();
        foreach($arr as $a)
        {
            $new_arr[] = $a[$key_name];
        }
        if (!empty($new_arr)) return $new_arr;
        return null;
    }

    //отримати спільні елементи 2-вимірного масиву
    public static function get_intersect($arr = array())
    {
        $new = $arr[0];
        foreach($arr as $a)
        {
            $new = array_intersect($new,$a);
        }
        return $new;
    }

    //перевірка чи пустий 2-вимірний масив
    public static function is_empty_arr2($arr = array())
    {
        $empty = true;
        foreach($arr as $a)
        {
            if (!empty($a)) $empty = false;
        }
        return $empty;
    }

    public static function array_merge_indexed($array1){

        $args = func_get_args();

        $res = [];

        foreach ($args as $array){
            foreach ($array as $int_key => $value ){

                $res[ (int) $int_key ] = $value;

            }
        }
        
        return $res;


    }

}