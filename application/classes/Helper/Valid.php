<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Valid{

    public static function email($val)
    {
        $valid = 1;
        $val = Helper_Filter::email($val);
        if (strlen($val) < 6) $valid = 0;
        return $valid;
    }

    public static function login($val)
    {
        $valid = 1;
        $val = Helper_Filter::login($val);
        if (strlen($val) == 0) $valid = 0;
        return $valid;
    }

    public static function name($val)
    {
        $valid = 1;
        $val = Helper_Filter::name($val);
        if (strlen($val) == 0) $valid = 0;
        return $valid;
    }

    public static function phone_number($val)
    {
        $valid = 1;
        $val = Helper_Filter::phone_number($val);
        if (strlen($val) == 0) $valid = 0;
        if ($valid == 1)
        {
            if (strlen(str_replace(' ','',$val)) == 0) $valid = 1;
        }
        return $valid;
    }

    public static function pass($pass,$rpass)
    {
        $valid = 1;
        if (strlen(str_replace(' ','',$pass)) < 6) $valid = 0;

        $rpass = trim($rpass);
        if (strlen(str_replace(' ','',$rpass)) < 6) $valid = 0;

        if ($pass !== $rpass) $valid = 0;

        return $valid;
    }

    public static function is_empty($value)
    {
        $empty = 0;
        if (strlen(str_replace(' ','',$value)) == 0) $empty = 1;
        return $empty;
    }

    public static function uniq($val,$table,$col,$except_key=null,$except_key_col=null)
    {
        $res = DB::select(DB::expr('count('.$col.') as c'))
                    ->from($table)
                    ->where($col,'=',$val);
                    if ($except_key) $res = $res->where($except_key_col,'!=',$except_key);
                    $res = $res->execute()
                    ->as_array();
        if ($res[0]['c'] == 0) return true;
        return false;
    }

    public static function number($val)
    {
        $valid = 1;
        $val = Helper_Filter::number($val);
        if ($val == 0) $valid = 0;
        return $valid;
    }

    //формат дати дд.мм.рр
    public static function date($val)
    {
        $exp = explode('.',$val);
        if (count($exp) != 3) return 0;
        $date = implode('-',array_reverse($exp));
        $date = strtotime($date.' 23:59:59');
        if ($date < time() ) return 0;
        return 1;
    }

    //формат дати дд.мм.рр
    public static function interval_date($date_from,$date_to,$same_time=false)
    {
        $time_from = '';
        $exp0 = explode(' ',$date_from);

        if (isset($exp0[1])) $time_from = ' '.$exp0[1];

        $exp1 = explode('.',$exp0[0]);

        foreach($exp1 as &$e) $e = trim($e);

        $date_from = implode('-',array_reverse($exp1)).$time_from;

        $date_from_time = strtotime($date_from);

        $time_to = '';
        $exp20 = explode(' ',$date_to);

        if (isset($exp20[1])) $time_to = ' '.$exp20[1];

        $exp2 = explode('.',$exp20[0]);

        foreach($exp2 as &$e1) $e1 = trim($e1);

        $date_to = implode('-',array_reverse($exp2)).$time_to;

        $date_to_time = strtotime($date_to);


        //перевірка дат на відповідність теперішньому часу
        if (time()-5 > $date_from_time) return false;
        if (time()-5 > $date_to_time) return false;


        //перевірка на коректність проміжку дат
        $date_diff = $date_to_time - $date_from_time;

        if ($same_time)
        {
            if ($date_diff < 0) return false;
        }
        else
        {
            if ($date_diff <= 0) return false;
        }

        return true;

    }

    //формат дати рр-мм-дд
    public static function interval_date_standart($date_from,$date_to,$same_time=false)
    {

        $date_from_time = strtotime(trim($date_from));

        $date_to_time = strtotime(trim($date_to));

        //перевірка дат на відповідність теперішньому часу
        if (time()-5 > $date_from_time) return false;
        if (time()-5 > $date_to_time) return false;


        //перевірка на коректність проміжку дат
        $date_diff = $date_to_time - $date_from_time;

        if ($same_time)
        {
            if ($date_diff < 0) return false;
        }
        else
        {
            if ($date_diff <= 0) return false;
        }

        return true;

    }

}