<?php defined('SYSPATH') or die('No direct script access.');

class Helper_PrefGenerator {


    public static function generate_col($table,$col,$primary_id,$title,$sep = '-',$except_id = null)
    {
        $pref = self::_transliterate_to_ascii($title,$sep);
        $count = DB::select(DB::expr('count('.$col.') as c'))
            ->from($table)
            ->where($col,'=',$pref);
        if ($except_id) $count = $count->where($primary_id,'!=',$except_id);
        $count = $count->execute()
            ->as_array();
        $count = $count[0]['c'];
        while($count > 0)
        {
            $pref = $pref.$sep;
            $count = DB::select(DB::expr('count('.$col.') as c'))
                ->from($table)
                ->where($col,'=',$pref);
            if ($except_id) $count = $count->where($primary_id,'!=',$except_id);
            $count = $count->execute()
                ->as_array();
            $count = $count[0]['c'];
        }

        return $pref;
    }

    public static function check_unic_col($table,$col,$primary_id,$pref,$sep = '-',$except_id = null)
    {
        $count = DB::select(DB::expr('count('.$col.') as c'))
            ->from($table)
            ->where($col,'=',$pref);
        if ($except_id) $count = $count->where($primary_id,'!=',$except_id);
        $count = $count->execute()
            ->as_array();

        $count = $count[0]['c'];
        while($count > 0)
        {
            $pref = $pref.$sep;
            $count = DB::select(DB::expr('count('.$col.') as c'))
                ->from($table)
                ->where($col,'=',$pref);
            if ($except_id) $count = $count->where($primary_id,'!=',$except_id);
            $count = $count->execute()
                ->as_array();
            $count = $count[0]['c'];
        }

        return $pref;
    }

    public static function generate($table,$title,$sep = '-',$except_id = null)
    {
        $pref = self::_transliterate_to_ascii($title,$sep);
        $count = DB::select(DB::expr('count(pref) as c'))
                    ->from($table)
                    ->where('pref','=',$pref);
                    if ($except_id) $count = $count->where('id','!=',$except_id);
                    $count = $count->execute()
                    ->as_array();
        $count = $count[0]['c'];
        while($count > 0)
        {
            $pref = $pref.$sep;
            $count = DB::select(DB::expr('count(pref) as c'))
                ->from($table)
                ->where('pref','=',$pref);
                if ($except_id) $count = $count->where('id','!=',$except_id);
                $count = $count->execute()
                ->as_array();
            $count = $count[0]['c'];
        }

        return $pref;
    }

    public static function check_unic($table,$pref,$sep = '-',$except_id = null)
    {
        $count = DB::select(DB::expr('count(pref) as c'))
            ->from($table)
            ->where('pref','=',$pref);
        if ($except_id) $count = $count->where('id','!=',$except_id);
        $count = $count->execute()
            ->as_array();

        $count = $count[0]['c'];
        while($count > 0)
        {
            $pref = $pref.$sep;
            $count = DB::select(DB::expr('count(pref) as c'))
                ->from($table)
                ->where('pref','=',$pref);
            if ($except_id) $count = $count->where('id','!=',$except_id);
            $count = $count->execute()
                ->as_array();
            $count = $count[0]['c'];
        }

        return $pref;
    }

    public static function _transliterate_to_ascii($str, $sep = '_')
    {
        $str = self::rus2translit($str);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~u', $sep, $str);
        $str = trim($str, $sep);
        return $str;
    }

    private static function rus2translit($string) {

        $converter = array(

            'а' => 'a',   'б' => 'b',   'в' => 'v',

            'г' => 'g',   'д' => 'd',   'е' => 'e',

            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

            'и' => 'i',   'й' => 'y',   'к' => 'k',

            'л' => 'l',   'м' => 'm',   'н' => 'n',

            'о' => 'o',   'п' => 'p',   'р' => 'r',

            'с' => 's',   'т' => 't',   'у' => 'u',

            'ф' => 'f',   'х' => 'h',   'ц' => 'c',

            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

            'ь' => '',  'ы' => 'y',   'ъ' => '',

            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'ґ' =>'g', 'є' =>'e', 'ї'=>'ji', 'і'=>'i',



            'А' => 'A',   'Б' => 'B',   'В' => 'V',

            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

            'И' => 'I',   'Й' => 'Y',   'К' => 'K',

            'Л' => 'L',   'М' => 'M',   'Н' => 'N',

            'О' => 'O',   'П' => 'P',   'Р' => 'R',

            'С' => 'S',   'Т' => 'T',   'У' => 'U',

            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',

            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

            'Ґ' =>'G', 'Є' =>'E', 'Ї'=>'JI','І'=>'I',

        );

        return strtr($string, $converter);

    }
}