<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Test{

    public static function test()
    {
        $view = View::factory('test');
        $view->hello = 'hello world';
        echo Response::factory()->body($view);
    }

}