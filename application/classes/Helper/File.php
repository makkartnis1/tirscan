<?php defined('SYSPATH') or die('No direct script access.');

class Helper_File{

    public static function count_files($dir)
    {
        if(!file_exists($dir)) return false;
        $c = 0; // количество файлов. Считаем с нуля
        $d = dir($dir); //
        while ($str = $d->read()) {
            if ($str{0} != '.') {
                if (is_dir($dir . '/' . $str)) $c += count_files($dir . '/' . $str);
                else $c++;
            };
        }
        $d->close(); // закрываем директорию
        return $c;
    }

    public  static function type($file_name){

        $name_arr = explode('.', $file_name);

        if(count($name_arr)<=1) return false;
        return $name_arr[count($name_arr)-1];
    }
}