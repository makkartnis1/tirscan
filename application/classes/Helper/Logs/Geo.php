<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Logs_Geo{

    static function success($step,$object)
    {
        self::insertToDB($step,$object);
    }

    static function error($step,$object,$error)
    {
        self::insertToDB($step,$object,$error);
    }

    static private function insertToDB($step,$object,$error=NULL)
    {
        $step = addslashes($step);
        $object = addslashes($object);
        if ($error) $error = addslashes($error);

        DB::insert('x_geo_log',array('step','object','error'))
                    ->values(array($step,$object,$error))
                    ->execute();
    }

}