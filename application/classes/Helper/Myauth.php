<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Myauth {


    public static function logIn($is_admin,$email,$pass,$rememb_me = 0)
    {

        $table = ($is_admin == 1)?'x_admins':'x_users';
        $prefix = ($is_admin == 1)?'admin_':'';

        $hash = self::hash($pass);

        $res = DB::select()
            ->from($table)
            ->where('email','=',$email)
            ->where('pass','=',$hash)
            ->limit(1)
            ->execute()
            ->as_array();
        if (!empty($res) && $res[0]['active'] == 1)
        {
            $_SESSION[$prefix.'user_id'] = $res[0]['id'];
            $_SESSION[$prefix.'user_info'] = json_encode($res[0]);
            if ($rememb_me)
            {
                Cookie::set($prefix.'user_id',$res[0]['id']);
                Cookie::set($prefix.'user_info',json_encode($res[0]));
            }

            return 'ok';
        }
        else
        {

            if(!empty($res) && $res[0]['active'] != 1)
            {
                return 'blocked';
            }
            else
            {
                return 'not_isset';
            }
        }

    }

    public static function forseLogIn($is_admin,$email)
    {

        $table = ($is_admin == 1)?'x_admins':'x_users';
        $prefix = ($is_admin == 1)?'admin_':'';

        $res = DB::select()
            ->from($table)
            ->where('email','=',$email)
            ->where('active','=',1)
            ->limit(1)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $_SESSION[$prefix.'user_id'] = $res[0]['id'];
            $_SESSION[$prefix.'user_info'] = json_encode($res[0]);
            return true;
        }
        return false;
    }

    public static function logOut($is_admin)
    {
        $prefix = ($is_admin == 1)?'admin_':'';

        if (isset($_SESSION[$prefix.'user_id']))
        {
            unset($_SESSION[$prefix.'user_id']);
            unset($_SESSION[$prefix.'user_info']);
        }
        $user_id = Cookie::get($prefix.'user_id');
        if ($user_id)
        {
            Cookie::delete($prefix.'user_id');
            Cookie::delete($prefix.'user_info');
        }

    }

    public static function isLogin($is_admin)
    {
        $prefix = ($is_admin == 1)?'admin_':'';

        self::updateSession($is_admin);
        if (isset($_SESSION[$prefix.'user_id'])) return true;
        $user_id = Cookie::get($prefix.'user_id');
        if ($user_id) return true;
        return false;
    }

    private static function updateSession($is_admin)
    {
        $prefix = ($is_admin == 1)?'admin_':'';
        $table = ($is_admin == 1)?'x_admins':'x_users';

        $user_id = Cookie::get($prefix.'user_id');
        if ($user_id) $_SESSION[$prefix.'user_id'] = $user_id;

        if (isset($_SESSION[$prefix.'user_id']))
        {
            $res = DB::select('id')
                ->from($table)
                ->where('active','=',1)
                ->where('id','=',$_SESSION[$prefix.'user_id'])
                ->limit(1)
                ->execute()
                ->as_array();
            if (empty($res))
            {
                self::logOut($is_admin);
            }
            else
            {
                self::refresh($is_admin);
            }
        }

    }

    public static function hash($pass)
    {
        return md5($pass);
    }

    public static function getUserInfo($is_admin)
    {
        if (self::isLogin($is_admin))
        {
            $prefix = ($is_admin == 1)?'admin_':'';
            return json_decode($_SESSION[$prefix.'user_info'],true);
        }
        return false;
    }

    public static function refresh($is_admin)
    {

        $prefix = ($is_admin == 1)?'admin_':'';
        $table = ($is_admin == 1)?'x_admins':'x_users';

        $res = DB::select()
            ->from($table)
            ->where('id','=',$_SESSION[$prefix.'user_id'])
            ->limit(1)
            ->execute()
            ->as_array();

        $_SESSION[$prefix.'user_info'] = json_encode($res[0]);
        if (Cookie::get($prefix.'user_id'))
        {
            Cookie::set($prefix.'user_info',json_encode($res[0]));
        }
    }

}