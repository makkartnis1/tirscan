<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Srt  {

    public static function up($table,$id)
    {
        $page_info = DB::select('srt')
            ->from($table)
            ->where('id','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        $current_srt = $page_info[0]['srt'];

        //вибираємо номер який ми плануємо присвоїти
        $next_srt_info = DB::select('srt')
            ->from($table)
            ->where('srt','<',$current_srt)
            ->order_by('srt','desc')
            ->limit(1)
            ->execute()
            ->as_array();

        if (!empty($next_srt_info))
        {
            $next_srt = $next_srt_info[0]['srt']-1;
        }
        else
        {
            $next_srt = $current_srt-1;
        }


        //якщо існує запис, то ми переприсвоюємо номери, щоб виділити номер для srt
        if (!empty($isset_next_srt))
        {
            DB::update($table)
                ->set(array(
                    'srt'=>DB::expr('srt + 1'),
                ))
                ->where('srt','>=',$next_srt)
                ->execute();
        }

        //вибираємо номери, які збігаються з новим значенням
        $similar_srt = DB::select(DB::expr('count(id) as c'))
            ->from($table)
            ->where('srt','=',$next_srt)
            ->execute()
            ->as_array();

        //якщо існує номер який збігається з новим значенням, то переприсвоюємо номера які менші за нове значення
        if ($similar_srt[0]['c'])
        {
            DB::update($table)
                ->set(array(
                    'srt'=>DB::expr('srt - 1')
                ))
                ->where('srt','<=',$next_srt)
                ->execute();
        }

        $res = DB::update($table)
            ->set(array(
                'srt'=>$next_srt,
            ))
            ->where('id','=',$id)
            ->execute();

        if ($res) return true;
        return null;
    }

    public static function down($table,$id)
    {
        $page_info = DB::select('srt')
            ->from($table)
            ->where('id','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        $current_srt = $page_info[0]['srt'];

        //вибираємо номер який ми плануємо присвоїти
        $next_srt_info = DB::select('srt')
            ->from($table)
            ->where('srt','>',$current_srt)
            ->order_by('srt','asc')
            ->limit(1)
            ->execute()
            ->as_array();

        if (!empty($next_srt_info))
        {
            $next_srt = $next_srt_info[0]['srt']+1;
        }
        else
        {
            $next_srt = $current_srt+1;
        }


        //якщо існує запис, то ми переприсвоюємо номери, щоб виділити номер для srt
        if (!empty($isset_next_srt))
        {
            DB::update($table)
                ->set(array(
                    'srt'=>DB::expr('srt - 1'),
                ))
                ->where('srt','<=',$next_srt)
                ->execute();
        }

        //вибираємо номери, які збігаються з новим значенням
        $similar_srt = DB::select(DB::expr('count(id) as c'))
            ->from($table)
            ->where('srt', '=', $next_srt)
            ->execute()
            ->as_array();

        //якщо існує номер який збігається з новим значенням, то переприсвоюємо номера які менші за нове значення
        if ($similar_srt[0]['c']) {
            DB::update($table)
                ->set(array(
                    'srt' => DB::expr('srt + 1')
                ))
                ->where('srt', '>=', $next_srt)
                ->execute();
        }

        $res = DB::update($table)
            ->set(array(
                'srt'=>$next_srt,
            ))
            ->where('id','=',$id)
            ->execute();
        if ($res) return true;
        return null;
    }

    public static function get_max_srt($table)
    {
        $res = DB::select(DB::expr('max(srt) as m'))
            ->from($table)
            ->execute()
            ->as_array();
        return $res[0]['m'];
    }

}