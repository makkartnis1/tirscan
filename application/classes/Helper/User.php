<?php defined('SYSPATH') or die('No direct script access.');

class Helper_User{
    
    const NO_AVATAR = '/assets/css/file/no-avatar.png';

    public static function avatar($userID){

        $dir = '/' . Frontend_Uploader::DIR_UPL . Frontend_Uploader::DIR_VISIBLE_PUBLIC . Frontend_Uploader::DIR_USER_AVATAR . "/{$userID}/";

        $files = (is_dir($_SERVER['DOCUMENT_ROOT'].$dir))
            ? array_values(array_diff(scandir($_SERVER['DOCUMENT_ROOT'].$dir), ['..', '.']))
            : [];

        return (empty($files)) ? $dir.'no-avatar.jpg' : $dir.$files[0];

    }

    public static function imageNewsPreview(){
        $dir = '/' . Frontend_Uploader::DIR_UPL . Frontend_Uploader::DIR_VISIBLE_PUBLIC . Frontend_Uploader::DIR_NEWS_IMG;

        return $dir;
    }

}