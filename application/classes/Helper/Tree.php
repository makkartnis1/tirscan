<?php defined('SYSPATH') or die('No direct script access.');

//класс для побудови ієрархій

class Helper_Tree{

    public static function get_tree($menu, $parent_id = 0)
    {
        if (empty($menu)) return null;

        $res = array();
        $menu_copy = $menu;
        foreach($menu as $m)
        {
            if ($m['sub']==$parent_id)
            {
                $temp['content'] = $m;
                $temp['child'] = self::get_tree($menu_copy,$m['id']);
                $res[] = $temp;
            }
        }

        return $res;
    }

    public static function get_recursiv_ids($res,$parent_id = 0)
    {
        if (empty($res)) return null;

        $res_copy = $res;
        foreach($res as $r)
        {
            if ($r['sub']==$parent_id)
            {
                $GLOBALS['ids'][] = $r['id'];
                self::get_recursiv_ids($res_copy,$r['id']);
            }
        }
    }

    public static function set_tree_title($res,$pr_key,$title_key,$del_type = '-')
    {
        if (empty($res)) return null;

        $res = Helper_Array::change_keys($res,$pr_key);
        $res_child = self::get_tree($res);
        $GLOBALS['res'] = array();
        self::write_to_list($res_child,$title_key,$del_type);
        $res = $GLOBALS['res'];
        unset($GLOBALS['res']);
        $res = array_values($res);
        return $res;
    }

    private static function write_to_list($res,$title_key,$del_type,$del = '')
    {
        foreach($res as $r)
        {

            $GLOBALS['res'][$r['content']['id']] = $r['content'];
            $GLOBALS['res'][$r['content']['id']][$title_key] = trim($del.' '.$r['content'][$title_key]);

            if (!empty($r['child']))
            {
                self::write_to_list($r['child'],$title_key,$del_type,$del.$del_type);
            }
        }
    }

}