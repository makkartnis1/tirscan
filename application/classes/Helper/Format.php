<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Format{

    public static function date($lng, $date){

        $en = array(
            '01' => 'jan', '02' => 'feb', '03' => 'mar',

            '04' => 'apr', '05' => 'may', '06' => 'jun',

            '07' => 'jul', '08' => 'aug', '09' => 'sept',

            '10' => 'oct', '11' => 'nov', '12' => 'dec'
        );

        $ua = array(
            '01' => 'січ', '02' => 'лют', '03' => 'бер',

            '04' => 'кві', '05' => 'тра', '06' => 'чер',

            '07' => 'лип', '08' => 'сер', '09' => 'вер',

            '10' => 'жов', '11' => 'лис', '12' => 'гру'
        );

        $ru = array(
            '01' => 'янв', '02' => 'фев', '03' => 'март',

            '04' => 'апр', '05' => 'май', '06' => 'июнь',

            '07' => 'июль','08' => 'авг', '09' => 'сен',

            '10' => 'окт', '11' => 'ноя', '12' => 'дек'
        );

        $date_array = explode('.', $date);

        switch ($lng){
            case 'en':
                return array( 0 => $date_array[0], 1=>$en[$date_array[1]]);
            case  'ua':
                return array( 0 => $date_array[0], 1=>$ua[$date_array[1]]);
            case  'ru':
                return array( 0 => $date_array[0], 1=>$ru[$date_array[1]]);
        }

        return false;
    }

}