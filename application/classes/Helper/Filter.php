<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Filter{

    public static function delete_spaces($val)
    {
        $val = str_replace(' ','',$val);
        return $val;
    }

    public static function no_tags($val)
    {
        return trim(strip_tags($val));
    }

    public static function chars($val)
    {
        return trim(htmlspecialchars($val,ENT_QUOTES));
    }

    public static function email($val)
    {
        $val = preg_replace ('/[^a-zA-ZА-Яа-я0-9_\-.@]+/ui','',$val);
        return $val;
    }

    public static function login($val)
    {
        $val = preg_replace ('/[^a-zA-ZА-Яа-я0-9ІіЇїЄєҐґЁё_\-.,;]+/ui','',$val);
        return $val;
    }

    public static function name($val)
    {
        $val = preg_replace ('/[^a-zA-ZА-Яа-яІіЇїЄєҐґЁё\-\s]+/ui','',$val);
        return trim($val);
    }

    public static function phone_number($val)
    {
        $val = preg_replace ('/[^0-9\-\(\)\s]+/ui','',$val);
        return $val;
    }

    public static function number($val)
    {
        $val = (float) $val;
        return $val;
    }

    //формат дати дд.мм.рр
    public static function date($val)
    {
        $exp = explode('.',$val);
        return implode('-',array_reverse($exp));
    }

}