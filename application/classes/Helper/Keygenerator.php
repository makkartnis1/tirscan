<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Keygenerator  {

    public static function generate()
    {
        $res = '';
        $str = 'q1w2e3r4t5y6u7i8o9p0a1s2d3f4g5h6j7k8l9z1x2c3v4b5n6m7';
        for($i=1;$i<=10;$i++) $res .= $str[mt_rand(0,51)];
        return $res;
    }

}