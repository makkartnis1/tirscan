<?php defined('SYSPATH') or die('No direct script access.');

class Helper_FileNameGenerator {

    public static function generate($file_dir,$file_type)
    {
        $i = 1;
        $file_name = $file_dir.$i.'.'.$file_type;

        while (file_exists($file_name))
        {
            $i++;
            $file_name = $file_dir.$i.'.'.$file_type;
        }

        return $file_name;
    }

    public static function uniq_filename($filename, $folder){

        $i = 1;
        while (file_exists($folder.'/'.$filename)){
            $name     = explode('.',$filename);
            $n        = explode($i, $name[0]);
            $i++;
            $filename = $n[0].$i.'.'.$name[count($name)-1];
        }
        return $filename;
    }

}