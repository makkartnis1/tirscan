<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class VarCache_Buffer Богдана
 */
class VarCache_Buffer {

    /**
     *
     * @var VarCache_Buffer
     */
    protected static $_instance;
//
    protected $_data = array();
//
    private function __construct(){}
    private function __clone(){}
    /**
     * @return VarCache_Buffer
     */
    protected static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;

    }

    public function get_($key, $default = null){
        return Arr::get($this->_data, $key, $default);
    }

    public function set_($key, $value){
        $this->_data[$key] = $value;
    }

    public function unset_($key){
        unset($this->_data[$key]);
    }



    public static function set($key, $data){
        $instance = self::getInstance();
        $instance->set_($key, $data);
    }

    public static function get($key, $default = null){
        $instance = self::getInstance();
        return $instance->get_($key, $default);
    }

    public static function get_once($key, $default = null){

        $instance = self::getInstance();
        $res = $instance->get_($key, $default);
        $instance->unset_($key);
        return $res;

    }


}