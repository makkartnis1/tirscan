<?php defined('SYSPATH') or die('No direct script access.');

class Model_Parser_Parser extends Model {

	CONST CAPTCHA_TYPE_TRANSPORT = 1;
	CONST CAPTCHA_TYPE_CARGO = 2;
	CONST CAPTCHA_TYPE_COMPANY = 3;

	CONST CAPTCHA_STATUS_NEW = 1;
	CONST CAPTCHA_STATUS_USED = 0;
	CONST CAPTCHA_STATUS_NEED_NEW = 2;

    protected $company_table = 'Frontend_Companies';
    protected $company_locale_table = 'Frontend_Company_Locales';
    protected $user_table = 'Frontend_Users';
    protected $user_locale_table = 'Frontend_User_Locales';
    protected $user_to_company = 'Frontend_User_to_Company_Ownership';//власник
    protected $phone_codes = 'Frontend_Phonecodes';//телефонні коди
    protected $owner_type = 'Frontend_Company_Ownership_Types';//форма власності компанії
    protected $company_type = 'Temp_Company_Types';//тип компанії експедитор, перевізник, вантажотримач
    protected $gruz = 'Frontend_Cargo';
    protected $transport = 'Frontend_Transports';
    protected $user_cars = 'Frontend_User_Cars';
    protected $user_cars_types = 'Frontend_User_Car_Types';
    protected $priceTable = 'Frontend_Request_Prices';
    protected $frontendCargoToGeo = 'Frontend_Cargo_to_Geo_Relation';
    protected $frontendTransportToGeo = 'Frontend_Transport_to_Geo_Relation';
    protected $frontendCompanyToGeo = 'Frontend_Company_to_Geo_Relation';
    protected $parsCountry = 'Parser_Country';
    protected $captcha = 'Services_Parser';

	/**
	 * @return array
	 */
	public static function getCaptchaTypes()
	{
		return [
			self::CAPTCHA_TYPE_CARGO => '/gruz/',
			self::CAPTCHA_TYPE_COMPANY => '/user/',
			self::CAPTCHA_TYPE_TRANSPORT => '/trans/',
		];
	}

	public function getCaptchaType($type)
	{
		return Arr::get(self::getCaptchaTypes(), $type, false);
	}

	public function getCaptchaName($type)
	{
		switch ($type){
			case self::CAPTCHA_TYPE_CARGO:
				return 'грузу';
			case self::CAPTCHA_TYPE_TRANSPORT:
				return 'транспорту';
			case self::CAPTCHA_TYPE_COMPANY:
				return 'компанії';
		}
	}

	public function getCaptcha($type)
	{
		return DB::select()
			->from($this->captcha)
			->where('type', '=', $type)
			->execute()
			->as_array();
	}

	public function setCaptchaStatus($newStatus, $type)
	{
		return DB::update($this->captcha)
			->set(['status' => $newStatus])
			->where('type', '=', $type)
			->execute();
	}

	public function setCurrentCaptcha($captchaUrl, $type)
	{
		return DB::update($this->captcha)
			->set(['url' => 'https://lardi-trans.com' . $captchaUrl])
			->where('type', '=', $type)
			->execute();
	}

    public function save_company($company)
    {
        return DB::insert($this->company_table, array(
//            'nameInternational',
            'approvedByAdmin',
            'trustableMark',
            'registered',
            'typeID',
            'ipn',
            'ownershipTypeID',
            'primaryLocaleID',
            'phone',
            'phoneStationary',
            'phoneCodeID',
            'phoneStationaryCodeID',
            'parser',
        ))
            ->values(array(
//                Arr::get($company, 'nameInternational', Arr::get($company, 'name')),
                'yes',
                'yes',
                Arr::get($company, 'dateRegistration', 'NOW()'),
                Arr::get($company, 'typeID', 1),
                Arr::get($company, 'companyID', ''),
                Arr::get($company, 'owner_type_id', 2),
                Arr::get($company, 'primaryLangID', 1),
                (isset($company['contact'][0]['phone'][0])) ? $company['contact'][0]['phone'][0]['phone']:'',
                (isset($company['contact'][0]['phone'][1])) ? $company['contact'][0]['phone'][1]['phone']:'',
                (isset($company['contact'][0]['phone'][0])) ? (int)$company['contact'][0]['phone'][0]['codID']:80,
                (isset($company['contact'][0]['phone'][1])) ? (int)$company['contact'][0]['phone'][1]['codID']:NULL,
                Arr::get($company, 'companyParsID', '0'),
            ))
            ->execute();
    }

    public function save_company_locale($company, $companyID)
    {
        return DB::insert($this->company_locale_table, array(
            'name',
            'realAddress',
            'companyID',
            'localeID',
            'info',
        ))
            ->values(array(
                Arr::get($company, 'name', ''),
                Arr::get($company, 'address', ''),
                $companyID,
                Arr::get($company, 'lang', ''),
                Arr::get($company, 'description', ''),
            ))
            ->execute();
    }

    public function save_contact($contact, $primary_lang_id, $companyID)
    {
        return DB::insert($this->user_table, array(
            'hash',
            'parserMail',
            'emailApproved',
            'walletBalance',
            'icq',
            'skype',
            'currentRating',
            'companyID',
            'phone',
            'phoneStationary',
            'phoneCodeID',
            'phoneStationaryCodeID',
            'primaryLocaleID',
            'parser',
        ))
            ->values(array(
                'hash',
                $contact['email'],
                'approved',
                00,
                Arr::get($contact, 'icq', ''),
                Arr::get($contact, 'skype', ''),
                '',
                $companyID,
                (isset($contact['phone'][0])) ? $contact['phone'][0]['phone']:'',
                (isset($contact['phone'][1])) ? $contact['phone'][1]['phone']:'',
                (isset($contact['phone'][0])) ? $contact['phone'][0]['codID']:80,
                (isset($contact['phone'][1])) ? $contact['phone'][1]['codID']:NULL,
                $primary_lang_id,
                Arr::get($contact, 'parser', 'parser'),
            ))
            ->execute();
    }

    public function save_contact_locale($contact, $localeID, $userID)
    {
        return DB::insert($this->user_locale_table, array(
            'name',
            'localeID',
            'userID',
        ))
            ->values(array(
                $contact['name'],
                $localeID,
                $userID
            ))
            ->execute();
    }

    public function get_company_by_url($url)
    {
        return DB::select('id')
            ->from($this->company_table)
            ->where('parser', '=', $url)
            ->execute()
            ->as_array();
    }

    public function phone_code_id($code)
    {
        return DB::select('id')
            ->from($this->phone_codes)
            ->where('code', '=', $code)
            ->execute()
            ->as_array();
    }

    public function insert_phone_codes($code)
    {
        return [0 => DB::select('id')
            ->from($this->phone_codes)
            ->execute()
            ->as_array()[0]['id']];
        //ібо нинада нові телефонні коди
        return DB::insert($this->phone_codes, array('code'))
            ->values(array($code))
            ->execute();
    }

    public function get_owner_type_id($owner)
    {
        return DB::select('id')
            ->from($this->owner_type)
            ->where('name', '=', $owner)
            ->execute()
            ->as_array();
    }

    public function insert_owner_type($owner)
    {
        return DB::insert($this->owner_type, array('name'))
            ->values(array($owner))
            ->execute();
    }

    public function insert_ignore_company_type($name, $lang)
    {
        return DB::query(Database::INSERT, 'INSERT IGNORE INTO `'.$this->company_type.'`(`name`, `lang`) VALUES ("'.$name.'", "'.$lang.'")')->execute();
    }

    public function user_to_company($user_id, $company_id)//власник
    {
        return DB::insert($this->user_to_company, array(
            'userID',
            'companyID',
        ))
            ->values(array(
                $user_id,
                $company_id
            ))
            ->execute();
    }

    /**
     * @param $dataToInsert array
     */
    public function insertGruzTEMP($dataToInsert)
    {
        return DB::insert('Parser_Gruz',array('countries','transport','time','date','country_from','country_where','gruz','payment','contacts'))
            ->values($dataToInsert)
            ->as_object()
            ->execute();
    }

    public static function findUserByParserStatic($parser)
    {
        return self::findUserByParser($parser);
    }

    public function findUserByParser($parser)
    {
        return DB::select('*')
            ->from($this->user_table)
            ->where('parser', 'LIKE', $parser)
            ->execute()
            ->as_array();
    }

    public function findUserTransport($uId)
    {
        return DB::select()
            ->from($this->user_cars)
            ->where('userID', '=', $uId)
            ->execute()
            ->as_array();
    }

    public function insertUserTransport($data, $uId, $carType)
    {
        return DB::insert($this->user_cars, [
            'userID',
            'volume',
            'sizeX',
            'sizeY',
            'sizeZ',
            'liftingCapacity',
            'carTypeID',
            'type',
        ])
            ->values([
                $uId,
                Arr::get($data, 'volume', NULL),
                Arr::get($data, 'sizeX', NULL),
                Arr::get($data, 'sizeY', NULL),
                Arr::get($data, 'sizeZ', NULL),
                Arr::get($data, 'weigth', NULL),
                Arr::get($data, 'userCarId', 1),
                'vantazhivka',
            ])
            ->execute();
    }

    public function getUserTransportType($name)
    {
        return DB::select()
            ->from($this->user_cars_types)
            ->where('name', '=', $name)
            ->execute()
            ->as_array();
    }

    public function insertUserTransportType($name)
    {
        return DB::insert($this->user_cars_types, [
            'name',
            'localeName'
        ])
            ->values([
                $name,
                Controller_Parser_Base::str2url($name)
            ])
            ->execute();
    }

    public function insertTransport($data, $userId)
    {
        return DB::insert($this->transport, [
            'dateFrom',
            'dateTo',
            'info',
            'priceID',
            'userCarID',
            'userID',
            'docTIR',
            'docCMR',
            'docT1',
            'docSanPassport',
            'docSanBook',
            'loadFromSide',
            'loadFromTop',
            'loadFromBehind',
            'loadTent',
            'condPlomb',
            'condLoad',
            'condBelts',
            'condRemovableStands',
            'condHardSide',
            'condCollectableCargo',
            'condTemperature',
            'condPalets',
            'condADR',
            'parser'
        ])
            ->values([
//                'dateFrom',
                Arr::get($data, 'dateFrom', 'NOW()'),
//                'dateTo',
                Arr::get($data, 'dateTo', 'NOW()'),
//                'info',
                Arr::get($data, 'info', ''),
//                'priceID',
                Arr::get($data, 'priceId', 1),
//                'userCarID',
                Arr::get($data, 'userCarId', 1),
//                'userID',
                $userId,
//                'docTIR',
                'no',
//                'docCMR',
                'no',
//                'docT1',
                'no',
//                'docSanPassport',
                'no',
//                'docSanBook',
                'no',
//                'loadFromSide',
                Arr::get($data['loadFrom'], 'loadFromSide', 'no'),
//                'loadFromTop',
                Arr::get($data['loadFrom'], 'loadFromTop', 'no'),
//                'loadFromBehind',
                Arr::get($data['loadFrom'], 'loadFromBehind', 'no'),
//                'loadTent',
                Arr::get($data['loadFrom'], 'loadTent', 'no'),
//                'condPlomb',
                'no',
//                'condLoad',
                'no',
//                'condBelts',
                'no',
//                'condRemovableStands',
                'no',
//                'condHardSide',
                'no',
//                'condCollectableCargo',
                'no',
//                'condTemperature',
                Arr::get($data, 'temperature', NULL),
//                'condPalets',
                Arr::get($data, 'palets', NULL),
//                'condADR',
                Arr::get($data, 'adr', NULL),
//                'parser'
                Arr::get($data, 'id', 'parser'),
            ])
            ->execute();
    }

    public function getTransportByParser($parser)
    {
        return DB::select()
            ->from($this->transport)
            ->where('parser', 'LIKE', $parser)
            ->execute()
            ->as_array();
    }

    public function insertPrice()
    {
        return DB::insert($this->priceTable, [
            'specifiedPrice'
        ])
            ->values([
                'no'
            ])
            ->execute();
    }

    public function insertGruz($data, $userId)
    {
        return DB::insert($this->gruz, [
            'dateFrom',
            'dateTo',
            'info',
            'cargoType',
            'transportTypeID',
            'volume',
            'weight',
            'carCount',
            'sizeX',
            'sizeY',
            'sizeZ',
            'priceID',
            'userID',
            'docTIR',
            'docCMR',
            'docT1',
            'docSanPassport',
            'docSanBook',
            'loadFromSide',
            'loadFromTop',
            'loadFromBehind',
            'loadTent',
            'condPlomb',
            'condLoad',
            'condBelts',
            'condRemovableStands',
            'condHardSide',
            'condCollectableCargo',
            'condTemperature',
            'condPalets',
            'condADR',
            'onlyTransporter',
            'parser',
        ])
            ->values([
//                'dateFrom',
                Arr::get($data, 'dateFrom', 'NOW()'),
//                'dateTo',
                Arr::get($data, 'dateTo', 'NOW()'),
//                'info',
                Arr::get($data, 'info', ''),
//                'cargoType',
                Arr::get($data, 'cargo', ''),
//                'transportTypeID',
                Arr::get($data, 'transportTypeID', 1),
//                'volume',
                Arr::get($data, 'volume', NULL),
//                'weight',
                Arr::get($data, 'weigth', NULL),
//                'carCount',
                Arr::get($data, 'autoCount', NULL),
//                'sizeX',
                Arr::get($data, 'sizeX', NULL),
//                'sizeY',
                Arr::get($data, 'sizeY', NULL),
//                'sizeZ',
                Arr::get($data, 'sizeZ', NULL),
//                'priceID',
                Arr::get($data, 'priceId', 1),
//                'userID',
                $userId,
//                'docTIR',
                'no',
//                'docCMR',
                'no',
//                'docT1',
                'no',
//                'docSanPassport',
                'no',
//                'docSanBook',
                'no',
//                'loadFromSide',
                Arr::get($data['loadFrom'], 'loadFromSide', 'no'),
//                'loadFromTop',
                Arr::get($data['loadFrom'], 'loadFromTop', 'no'),
//                'loadFromBehind',
                Arr::get($data['loadFrom'], 'loadFromBehind', 'no'),
//                'loadTent',
                Arr::get($data['loadFrom'], 'loadTent', 'no'),
//                'condPlomb',
                'no',
//                'condLoad',
                'no',
//                'condBelts',
                'no',
//                'condRemovableStand',
                'no',
//                'condHardSide',
                'no',
//                'condCollectableCargo',
                'no',
//                'condTemperature',
                Arr::get($data, 'temperature', NULL),
//                'condPalets',
                Arr::get($data, 'palets', NULL),
//                'condADR',
                Arr::get($data, 'adr', NULL),
//                'onlyTransporter',
                'no',
//                'parser',
                Arr::get($data, 'id', 'parser'),
            ])
            ->execute();
    }

    public function getGruzByParser($parser)
    {
        return DB::select()
            ->from($this->gruz)
            ->where('parser', 'LIKE', $parser)
            ->execute()
            ->as_array();
    }

    public function insertGeoToGruz($ids, $adId, $destination = 'from')
    {
        $idsArray = explode(',', $ids);
        foreach ($idsArray as $item) {
            DB::insert($this->frontendCargoToGeo, [
                'type',
                'geoID',
                'requestID',
                'groupNumber',
            ])
                ->values([
                    $destination,
                    $item,
                    $adId,
                    1,
                ])
                ->execute();
        }
    }

    public function insertGeoToTransport($ids, $adId, $destination = 'from')
    {
        $idsArray = explode(',', $ids);
        foreach ($idsArray as $item) {
            DB::insert($this->frontendTransportToGeo, [
                'type',
                'geoID',
                'requestID',
                'groupNumber',
            ])
                ->values([
                    $destination,
                    $item,
                    $adId,
                    1,
                ])
                ->execute();
        }
    }

    public function insertGeoToCompany($ids, $companyId)
    {
        $idsArray = explode(',', $ids);
        foreach ($idsArray as $item) {
            DB::insert($this->frontendCompanyToGeo, [
                'geoID',
                'companyID',
            ])
                ->values([
                    $item,
                    $companyId,
                ])
                ->execute();
        }
    }

//    public function insertCountryPars($array)
//    {
//        $insert = DB::insert($this->parsCountry, ['name', 'short_name', 'short_name2', 'parser_id']);
//        foreach ($array as $item) {
//            $sig2 = Arr::get($item, 'sign2', '');
//            if(is_array($sig2)) $sig2 = '';
//            $insert->values([$item['name'], $item['sign'], $sig2, (int) $item['id']]);
//        }
//        return $insert->execute();
//    }

    public function getCountryPars()
    {
        return DB::select('*')
            ->from($this->parsCountry)
            ->where('active', '=', 1)
            ->execute()
            ->as_array();
    }



}
