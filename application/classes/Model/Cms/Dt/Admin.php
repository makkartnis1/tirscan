<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Dt_Admin extends Model {

    public function get($post,$lng_id,$group_id)
    {
        $search = array(
            'column'=>'',
            'val'=>''
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['column'] = $v['data'];
                    $search['val'] = $v['search']['value'];
                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
                }
            }
        }

        $select_arr[] = 'x_admins.*';

        $res = DB::select_array(
           $select_arr
        )
            ->from('x_admins');

        if ($search['column'])
        {
            if (isset($search['type']))
            {
                switch ($search['type']) {
                    case '=':
                        $res = $res->where($search['column'],'=',$search['val']);
                        break;
                }
            }
            else
            {
                if ($search['column']=='register_date')
                {
                    $ex = explode(' - ',$search['val']);
                    if (count($ex) > 1)
                    {
                        $date_from = trim($ex[0]);
                        $date_to = trim($ex[1]);
                        $res = $res->where('x_admins.register_date','>=',$date_from);
                        $res = $res->where('x_admins.register_date','<=',$date_to);
                    }
                    else
                    {
                        $date = trim($search['val']);
                        $res = $res->where('x_admins.register_date','like',DB::expr("'%".$date."%'"));
                    }
                }
                elseif($search['column']=='group_id')
                {
                    $res = $res->where($search['column'],'=',$search['val']);
                }
                else
                {
                    $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));

                }
            }
        }

        switch ($group_id) {
            case '2':
                $res = $res->where('x_admins.group_id','NOT IN',DB::expr('(1,2)'));
                break;
        }

        $index_order = $post['order'][0]['column'];
        $type_order = $post['order'][0]['dir'];

        $res = $res
            ->order_by($post['columns'][$index_order]['data'],$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();

        if (!empty($res))
        {
            foreach($res as &$r)
            {
                $r['group_title'] = $this->add_group($r['group_id'],$lng_id);
                $r['avatar'] = $this->get_avatar($r['id']);
            }
            return $res;
        }
        return array();
    }


    public function countAll($pr_key,$group_id)
    {
        $res = DB::select(DB::expr('count('.$pr_key.') as c'))
            ->from('x_admins');
        switch ($group_id) {
            case '2':
                $res = $res->where('x_admins.group_id','NOT IN',DB::expr('(1,2)'));
                break;
        }
            $res = $res->execute();
        return $res[0]['c'];
    }

    public function countFiltered($post,$group_id)
    {
        $search = array(
            'column'=>'',
            'val'=>''
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['column'] = $v['data'];
                    $search['val'] = $v['search']['value'];
                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
                }
            }
        }


        $select_arr[] = 'x_admins.*';
        $res = DB::select_array(
            $select_arr
        )
            ->from('x_admins');


        if ($search['column'])
        {
            if (isset($search['type']))
            {
                switch ($search['type']) {
                    case '=':
                        $res = $res->where($search['column'],'=',$search['val']);
                        break;
                }
            }
            else
            {
                if ($search['column']=='register_date')
                {
                    $ex = explode(' - ',$search['val']);
                    if (count($ex) > 1)
                    {
                        $date_from = trim($ex[0]);
                        $date_to = trim($ex[1]);
                        $res = $res->where('x_admins.register_date','>=',$date_from);
                        $res = $res->where('x_admins.register_date','<=',$date_to);
                    }
                    else
                    {
                        $date = trim($search['val']);
                        $res = $res->where('x_admins.register_date','like',DB::expr("'%".$date."%'"));
                    }
                }
                elseif($search['column']=='group_id')
                {
                    $res = $res->where($search['column'],'=',$search['val']);
                }
                else
                {
                    $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));

                }
            }
        }

        switch ($group_id) {
            case '2':
                $res = $res->where('x_admins.group_id','NOT IN',DB::expr('(1,2)'));
                break;
        }

        $res = $res->execute()->as_array();
        if (!empty($res)) return count($res);
        return 0;
    }

    private function add_group($group_id,$lng_id)
    {
        $m_group = new Model_Cms_Plugin_AdminGroup();
        return $m_group->get_title($group_id,$lng_id);
    }

    private function get_avatar($id)
    {
        $file=$_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$id.'.jpg';
        if (file_exists($file))
        {
            $img = '/uploads/images/admins/'.$id.'.jpg';
        }
        else
        {
            $img = '/public/cms/img/user.png';
        }

        return $img;
    }
}