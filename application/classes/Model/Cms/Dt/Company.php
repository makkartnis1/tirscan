<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Dt_Company extends Model {

    public $table = 'Frontend_Companies';
    public $table_locales = 'Frontend_Company_Locales';
    public $table_phone_codes = 'Frontend_Phonecodes';

    public $t_transport_req = 'Frontend_Transports';
    public $t_cargo_req = 'Frontend_Cargo';
    public $t_users = 'Frontend_Users';
    public $t_user_locales = 'Frontend_User_Locales';
    public $t_tenders = 'Frontend_Tenders';
    public $t_user_cars = 'Frontend_User_Cars';
    public $t_news = 'Frontend_News';
    public $t_comments = 'Frontend_Comments';
    public $t_company_owner = 'Frontend_User_to_Company_Ownership';

    public $table_company_types = 'Frontend_Company_Types';
    public $table_company_owner_types = 'Frontend_Company_Ownership_Types';


    public $table_geo_rel = 'Frontend_Company_to_Geo_Relation';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_geo = 'Frontend_Geo';

    public function get($post,$filter,$company_type = null)
    {
        $search = $post['search']['value'];

        $select_arr = array(
            $this->table.'.*',
            array($this->table_company_types.'.name','type'),
            array($this->table_company_owner_types.'.name','ownershipType'),
            array('phone_table.code','code_phone'),
            array('phone_stat_table.code','code_phone_stat'),
            array(DB::expr('(select name from '.$this->table_locales.' where companyID = '.$this->table.'.ID limit 1)'),'name')
        );


        $res = DB::select_array($select_arr)
            ->from($this->table)
            ->join($this->table_company_types,'left')->on($this->table_company_types.'.ID','=',$this->table.'.typeID')
            ->join($this->table_company_owner_types,'left')->on($this->table_company_owner_types.'.ID','=',$this->table.'.ownershipTypeID')

            ->join(array($this->table_phone_codes, 'phone_table'),'left')
            ->on('phone_table.ID','=',$this->table.'.phoneCodeID')

            ->join(array($this->table_phone_codes, 'phone_stat_table'),'left')
            ->on('phone_stat_table.ID','=',$this->table.'.phoneStationaryCodeID');


        switch ($company_type) {
            case 'cargo':
                $res = $res->where($this->table.'.typeID','IN',DB::expr('(1,3)'));
                break;
            case 'transport':
                $res = $res->where($this->table.'.typeID','IN',DB::expr('(1,2)'));
                break;
        }

        if ($search != '')
        {
            $res = $res->and_where_open();
            $res = $res->or_where(DB::expr("(select name from ".$this->table_locales." where companyID = ".$this->table.".ID and name like '%".$search."%' limit 1)"),'!=',NULL);
            $res = $res->or_where($this->table.'.ID','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.ipn','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.phone','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.phoneStationary','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->and_where_close();
        }

        if (isset($filter['ID']))
        {
            $res = $res->where($this->table.'.ID','LIKE',DB::expr("'%".$filter['ID']."%'"));
        }

        if (isset($filter['name']))
        {
            $res = $res->where(DB::expr("(select name from ".$this->table_locales." where companyID = ".$this->table.".ID and name like '%".$filter['name']."%' limit 1)"),'!=',NULL);
        }

        if (isset($filter['IPN']))
        {
            $res = $res->where($this->table.'.ipn','LIKE',DB::expr("'%".$filter['IPN']."%'"));
        }

        if (isset($filter['phone']))
        {
            $res = $res->and_where_open();
            $res = $res->or_where($this->table.'.phone','LIKE',DB::expr("'%".$filter['phone']."%'"));
            $res = $res->or_where($this->table.'.phoneStationary','LIKE',DB::expr("'%".$filter['phone']."%'"));
            $res = $res->and_where_close();
        }


        if (isset($filter['approvedByAdmin']))
        {
            $approvedByAdmin = ($filter['approvedByAdmin'])?'yes':'no';
            $res = $res->where($this->table.'.approvedByAdmin','=',$approvedByAdmin);
        }


        if (isset($filter['typeID']))
        {
            $res = $res->where($this->table.'.typeID','=',$filter['typeID']);
        }

        if (isset($filter['ownershipTypeID']))
        {
            $res = $res->where($this->table.'.ownershipTypeID','=',$filter['ownershipTypeID']);
        }

        if (isset($filter['registered']))
        {
            $res_filter_reqistered = $this->get_time_vars($filter['registered']);
            $res = $res->where($this->table.'.registered',$res_filter_reqistered['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_reqistered['time'].')'));
        }

        if (isset($filter['foundationDate']))
        {
            $res_filter_foundation = $this->get_time_vars($filter['foundationDate']);
            $res = $res->where($this->table.'.foundationDate',$res_filter_foundation['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_foundation['time'].')'));
        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.registered','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.registered','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['rating']))
        {
            switch ($filter['rating']) {
            case '1-2':
                $res = $res->where($this->table.'.rating','BETWEEN',array(1,1.99));
                break;
            case '2-3':
                $res = $res->where($this->table.'.rating','BETWEEN',array(2,2.99));
                break;
            case '3-4':
                $res = $res->where($this->table.'.rating','BETWEEN',array(3,3.99));
                break;
            case '4-5':
                $res = $res->where($this->table.'.rating','BETWEEN',array(4,4.99));
                break;
            case '5':
                $res = $res->where($this->table.'.rating','=',5);
                break;
            }
        }


        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                foreach($places_from as $p)
                {
                    $exp_ids = explode(',',$p);
                    foreach($exp_ids as $geo_id)
                    {
                        $res = $res->where(
                            DB::select($this->table_geo_rel.'.ID')
                                ->from($this->table_geo_rel)
                                ->where($this->table_geo_rel.'.companyID','=',DB::expr($this->table.'.ID'))
                                ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                            '!=',
                            NULL
                        );
                    }
                }
            }

        }


        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];


        $order_col = $this->table.'.'.$order_col;
        $type_order = $post['order'][0]['dir'];


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {

            foreach($res as &$r)
            {
                $r['phone'] = ($r['code_phone'] and $r['phone'])?$r['code_phone'].' '.$r['phone']:'';
                $r['phone2'] = ($r['code_phone_stat'] and $r['phoneStationary'])?$r['code_phone_stat'].' '.$r['phoneStationary']:'';
                $r['transport_req'] = $this->get_transport_req($r['ID']);
                $r['cargo_req'] = $this->get_cargo_req($r['ID']);
                $r['tenders'] = $this->get_tenders($r['ID']);
                $r['positive'] = $this->get_positive($r['ID']);
                $r['negative'] = $this->get_negative($r['ID']);
                $r['neutral'] = $this->get_neutral($r['ID']);
                $r['users'] = $this->get_users($r['ID']);
                $r['transports'] = $this->get_transports($r['ID']);
                $r['news'] = $this->get_news($r['ID']);
                $r['valuta'] = $this->get_valuta($r['ID']);
                $r['address'] = $this->get_address($r['ID']);
                $r['register_date'] = $this->get_date($r['registered']);
                $r['founding_date'] = ($r['foundationDate'])?$this->get_date($r['foundationDate']):'не вказано';
                $r['owner'] = $this->get_owner_user($r['ID']);
            }
            return $res;
        }
        return array();
    }

    private function get_owner_user($company_id)
    {
        $res = DB::select(
            array(DB::expr("(select name from ".$this->t_user_locales." where userID = ".$this->t_company_owner.".userID order by localeID limit 1)"),'name'),
            $this->t_company_owner.'.userID'
        )
                    ->from($this->t_company_owner)
                    ->where($this->t_company_owner.'.companyID','=',$company_id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return '<a target="_blank" href="/cms/uk/user/edit/'.$res[0]['userID'].'" class="link_name">'.$res[0]['name'].'</a>';
                return null;
    }

    private function get_time_vars($key)
    {
        $temp = '';
        $sign = '<=';
        switch ($key) {
            case '<1m':
                $temp = '-1 MONTH';
                $sign = '>';
                break;
            case '>1m':
                $temp = '-1 MONTH';
                break;
            case '>2m':
                $temp = '-2 MONTH';
                break;
            case '>3m':
                $temp = '-3 MONTH';
                break;
            case '>6m':
                $temp = '-6 MONTH';
                break;
            case '>1y':
                $temp = '-1 YEAR';
                break;
            case '>2y':
                $temp = '-2 YEAR';
                break;
            case '>3y':
                $temp = '-3 YEAR';
                break;
            case '>5y':
                $temp = '-5 YEAR';
                break;
            case '>10y':
                $temp = '-10 YEAR';
                break;
            case '>15y':
                $temp = '-15 YEAR';
                break;
            case '>20y':
                $temp = '-20 YEAR';
                break;
        }

        return ['sign'=>$sign,'time'=>$temp];
    }

    private function get_date($date)
    {
        $month = 60 * 60 * 24 * 29;
        $year = $month * 12;

        $now = time();
        $date = strtotime($date);
        $diff = $now - $date;

        if ($diff < $month)
        {
            return 'менше місяця';
        }
        elseif($diff >= $month and $diff < $year)
        {
            $count_month = floor($diff / $month);
            $time = 'місяців';
            if ($count_month == 1) $time = 'місяць';
            if ($count_month == 2 or $count_month == 3 or $count_month == 4) $time = 'місяця';
            return $count_month.' '.$time;
        }
        elseif($diff > $year)
        {
            $count_years = floor($diff / $year);
            $time_year = 'років';

            $str_years = (string) $count_years;
            $last_letter = $str_years{strlen($str_years)-1};
            if ($last_letter == '1') $time_year = 'рік';
            if ($last_letter == '2' or $last_letter == '3' or $last_letter == '4') $time_year = 'роки';

            $res = $count_years.' '.$time_year;

            $month_val = $diff - ($year * $count_years);
            $time_month = '';

            if ($month_val > $month)
            {
                $count_month = floor($month_val / $month);
                $time_month = 'місяців';
                if ($count_month == 1) $time_month = 'місяць';
                if ($count_month == 2 or $count_month == 3 or $count_month == 4) $time_month = 'місяця';
                $res .= ' і '.$count_month.' '.$time_month;

            }

            return $res;
        }
    }

    private function get_address($company_id)
    {
        $m = new Model_Component_Cms_Company();
        $address = $m->get_geo_name($company_id);
        return '<i>'.(($address)?$address:'не вказано').'</i>';
    }

    private function get_valuta($company_id)
    {
        return '<i>не куплено</i>';
    }

    private function get_news($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_news.'.ID) as c'))
            ->from($this->t_news)
            ->join($this->t_users,'inner')->on($this->t_users.'.ID','=',$this->t_news.'.userID')
            ->where($this->t_users.'.companyID','=',$company_id)
            ->execute()
            ->as_array();

        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/user/news?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_transports($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_user_cars.'.ID) as c'))
                    ->from($this->t_user_cars)
                    ->join($this->t_users,'inner')->on($this->t_users.'.ID','=',$this->t_user_cars.'.userID')
                    ->where($this->t_users.'.companyID','=',$company_id)
                    ->execute()
                    ->as_array();

        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/transport?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_users($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_users.'.ID) as c'))
                    ->from($this->t_users)
                    ->where($this->t_users.'.companyID','=',$company_id)
                    ->where(DB::expr('(select '.$this->t_company_owner.'.ID from '.$this->t_company_owner.' where '.$this->t_company_owner.'.userID = '.$this->t_users.'.ID)'),'=',NULL)
                    ->execute()
                    ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/user?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_neutral($company_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_comments)
            ->where('companyID','=',$company_id)
            ->where('rating','=','neutral')
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?company_id='.$company_id.'&rating=neutral" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_negative($company_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_comments)
            ->where('companyID','=',$company_id)
            ->where('rating','=','negative')
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?company_id='.$company_id.'&rating=negative" class="badge badge-important">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_positive($company_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->t_comments)
                    ->where('companyID','=',$company_id)
                    ->where('rating','=','positive')
                    ->execute()
                    ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?company_id='.$company_id.'&rating=positive" class="badge badge-success">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_tenders($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_tenders.'.ID) as c'))
            ->from($this->t_tenders)
            ->join($this->t_users,'inner')->on($this->t_users.'.ID','=',$this->t_tenders.'.userID')
            ->where($this->t_users.'.companyID','=',$company_id)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/tenders?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_cargo_req($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_cargo_req.'.ID) as c'))
            ->from($this->t_cargo_req)
            ->join($this->t_users,'inner')->on($this->t_users.'.ID','=',$this->t_cargo_req.'.userID')
            ->where($this->t_users.'.companyID','=',$company_id)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/transportRequests?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_transport_req($company_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_transport_req.'.ID) as c'))
                    ->from($this->t_transport_req)
                    ->join($this->t_users,'inner')->on($this->t_users.'.ID','=',$this->t_transport_req.'.userID')
                    ->where($this->t_users.'.companyID','=',$company_id)
                    ->execute()
                    ->as_array();
                if (!empty($res))
                {
                    $count = $res[0]['c'];

                    if ($count)
                    {
                        return '<a href="/cms/uk/cargoRequests?company_id='.$company_id.'" class="badge badge-primary">'.$count.'</a>';
                    }
                    else
                    {
                        return '<a href="#" class="badge">'.$count.'</a>';
                    }
                }
                return null;
    }

    public function countAll($company_type = null)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table);

            switch ($company_type) {
            case 'cargo':
                $res = $res->where('typeID','IN',DB::expr('(1,3)'));
                break;
            case 'transport':
                $res = $res->where('typeID','IN',DB::expr('(1,2)'));
                break;
            }

            $res = $res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$filter,$company_type = null)
    {
        $search = $post['search']['value'];

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)
            ->join(array($this->table_phone_codes, 'phone_table'),'left')
                ->on('phone_table.ID','=',$this->table.'.phoneCodeID')
            ->join(array($this->table_phone_codes, 'phone_stat_table'),'left')
                ->on('phone_stat_table.ID','=',$this->table.'.phoneStationaryCodeID');

        switch ($company_type) {
            case 'cargo':
                $res = $res->where($this->table.'.typeID','IN',DB::expr('(1,3)'));
                break;
            case 'transport':
                $res = $res->where($this->table.'.typeID','IN',DB::expr('(1,2)'));
                break;
        }


        if ($search != '')
        {
            $res = $res->and_where_open();
            $res = $res->or_where(DB::expr("(select name from ".$this->table_locales." where companyID = ".$this->table.".ID and name like '%".$search."%' limit 1)"),'!=',NULL);
            $res = $res->or_where($this->table.'.ID','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.ipn','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.phone','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.phoneStationary','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->and_where_close();
        }

        if (isset($filter['ID']))
        {
            $res = $res->where($this->table.'.ID','LIKE',DB::expr("'%".$filter['ID']."%'"));
        }

        if (isset($filter['name']))
        {
            $res = $res->where(DB::expr("(select name from ".$this->table_locales." where companyID = ".$this->table.".ID and name like '%".$filter['name']."%' limit 1)"),'!=',NULL);
        }

        if (isset($filter['IPN']))
        {
            $res = $res->where($this->table.'.ipn','LIKE',DB::expr("'%".$filter['IPN']."%'"));
        }

        if (isset($filter['phone']))
        {
            $res = $res->and_where_open();
            $res = $res->or_where($this->table.'.phone','LIKE',DB::expr("'%".$filter['phone']."%'"));
            $res = $res->or_where($this->table.'.phoneStationary','LIKE',DB::expr("'%".$filter['phone']."%'"));
            $res = $res->and_where_close();
        }


        if (isset($filter['approvedByAdmin']))
        {
            $approvedByAdmin = ($filter['approvedByAdmin'])?'yes':'no';
            $res = $res->where($this->table.'.approvedByAdmin','=',$approvedByAdmin);
        }


        if (isset($filter['typeID']))
        {
            $res = $res->where($this->table.'.typeID','=',$filter['typeID']);
        }

        if (isset($filter['ownershipTypeID']))
        {
            $res = $res->where($this->table.'.ownershipTypeID','=',$filter['ownershipTypeID']);
        }

        if (isset($filter['registered']))
        {
            $res_filter_reqistered = $this->get_time_vars($filter['registered']);
            $res = $res->where($this->table.'.registered',$res_filter_reqistered['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_reqistered['time'].')'));
        }

        if (isset($filter['foundationDate']))
        {
            $res_filter_foundation = $this->get_time_vars($filter['foundationDate']);
            $res = $res->where($this->table.'.foundationDate',$res_filter_foundation['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_foundation['time'].')'));
        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.registered','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.registered','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['rating']))
        {
            switch ($filter['rating']) {
                case '1-2':
                    $res = $res->where($this->table.'.rating','BETWEEN',array(1,2));
                    break;
                case '2-3':
                    $res = $res->where($this->table.'.rating','BETWEEN',array(2,3));
                    break;
                case '3-4':
                    $res = $res->where($this->table.'.rating','BETWEEN',array(3,4));
                    break;
                case '4-5':
                    $res = $res->where($this->table.'.rating','BETWEEN',array(4,5));
                    break;
                case '5':
                    $res = $res->where($this->table.'.rating','=',5);
                    break;
            }
        }


        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                foreach($places_from as $p)
                {
                    $exp_ids = explode(',',$p);
                    foreach($exp_ids as $geo_id)
                    {
                        $res = $res->where(
                            DB::select($this->table_geo_rel.'.ID')
                                ->from($this->table_geo_rel)
                                ->where($this->table_geo_rel.'.companyID','=',DB::expr($this->table.'.ID'))
                                ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                            '!=',
                            NULL
                        );
                    }
                }
            }

        }


        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }


}