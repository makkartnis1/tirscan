<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Dt_Parser extends Model {

    public $table = 'Services_Parser';

    public function get($post)
    {
        $select_arr[] = $this->table.'.*';

        $res = DB::select_array(
            $select_arr
        )
            ->from($this->table);

        $index_order = $post['order'][0]['column'];
        $type_order = $post['order'][0]['dir'];

        $res = $res
            ->order_by($post['columns'][$index_order]['data'],$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();

        if (!empty($res))
        {
            return $res;
        }
        return array();
    }

    public function countAll()
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table)
            ->execute();
        return $res[0]['c'];
    }

    public function countFiltered()
    {

        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table);

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

}