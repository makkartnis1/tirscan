<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Sitelang extends Model {

    public function get_by_pref($pref)
    {
        $res = DB::select('pref')
                    ->from('x_lngs')
                    ->where('pref','=',$pref)
                    ->limit(1)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0];
                return null;
    }

    public function issetLangByPref($pref)
    {
        $res = DB::select(DB::expr('count(id) as c'))
            ->from('x_lngs')
            ->where('pref','=',$pref)
            ->execute()
            ->as_array();
        if ($res[0]!=0) return true;
        return false;
    }

    public function getDefaultPrefixLang()
    {
        $res = DB::select('pref')
            ->from('x_lngs')
            ->where('def','=',1)
            ->limit(1)
            ->execute()
            ->as_array();
        return $res[0]['pref'];
    }

    public function getIdByPref($pref)
    {
        $res = DB::select('id')
            ->from('x_lngs')
            ->where('pref','=',$pref)
            ->limit(1)
            ->execute()
            ->as_array();
        return $res[0]['id'];
    }
}