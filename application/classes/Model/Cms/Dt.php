<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Dt extends Model {

    public function get($post,$plugin)
    {
        $search = array(
            'column'=>'',
            'val'=>''
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['column'] = $v['data'];
                    $search['val'] = $v['search']['value'];
                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
                }
            }
        }

        $res = DB::select()
            ->from('x_configs');

        $res = $res->where('plugin','=',$plugin);

        if ($search['column'])
        {
            if (isset($search['type']))
            {
                switch ($search['type']) {
                case '=':
                    $res = $res->where($search['column'],'=',$search['val']);
                    break;
                }
            }
            else
            {
                $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));
            }
        }
        $index_order = $post['order'][0]['column'];
        $type_order = $post['order'][0]['dir'];

        $res = $res
            ->order_by($post['columns'][$index_order]['data'],$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();

        if (!empty($res)) return $res;
        return array();
    }

    public function countAll($plugin,$pr_key)
    {
        $res = DB::select(DB::expr('count('.$pr_key.') as c'))
            ->from('x_configs')
            ->where('plugin','=',$plugin)
            ->execute();
        return $res[0]['c'];
    }

    public function countFiltered($post,$plugin,$pr_key)
    {
        $search = array(
            'column'=>'',
            'val'=>''
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['column'] = $v['data'];
                    $search['val'] = $v['search']['value'];
                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
                }
            }
        }

        $res = DB::select(DB::expr('count('.$pr_key.') as c'))
            ->from('x_configs');
        $res = $res->where('plugin','=',$plugin);

        if ($search['column'])
        {
            if (isset($search['type']))
            {
                switch ($search['type']) {
                    case '=':
                        $res = $res->where($search['column'],'=',$search['val']);
                        break;
                }
            }
            else
            {
                $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));
            }
        }
        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }

}