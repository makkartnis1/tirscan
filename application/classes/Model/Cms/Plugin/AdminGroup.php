<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_AdminGroup extends Model {

    public function get_title($id,$lng_id)
    {
        $res = DB::select('title')
                    ->from('x_admin_group_locales')
                    ->where('admin_group_id','=',$id)
                    ->where('plugin_lng_id','=',$lng_id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['title'];
                return null;
    }

    public function get_all($lng_id,$group_id)
    {
        $res = DB::select('title',array('admin_group_id','id'))
            ->from('x_admin_group_locales')
            ->where('plugin_lng_id','=',$lng_id);

            switch ($group_id) {
            case '1':
                $res = $res->where('id','NOT IN',DB::expr('(1)'));
                break;
            case '2':
                $res = $res->where('id','NOT IN',DB::expr('(1,2)'));
                break;
            }

            $res = $res->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }
}