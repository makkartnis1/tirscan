<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_CompanyTypes extends Model {

    public $table = 'Frontend_Company_Types';

    public function get_all()
    {
        $res = DB::select()
            ->from($this->table)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }


}