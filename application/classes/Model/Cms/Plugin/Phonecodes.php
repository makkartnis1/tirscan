<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Phonecodes extends Model {

    public $table = 'Frontend_Phonecodes';

    public function get_all()
    {
        $res = DB::select()
            ->from($this->table)
            ->order_by('code')
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }


}