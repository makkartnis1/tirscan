<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Company extends Model {

    public $table = 'Frontend_Companies';
    public $table_user = 'Frontend_Users';
    public $table_owner = 'Frontend_User_to_Company_Ownership';

    public function delete($id)
    {
        if (!$this->isset_users_in_company($id))
        {
            DB::delete($this->table)
                        ->where('ID','=',$id)
                        ->execute();
        }
    }

    private function isset_users_in_company($company_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->table_user)
                    ->where('companyID','=',$company_id)
                    ->execute()
                    ->as_array();
        $count = $res[0]['c'];
        if ($count > 0) return true;
        return false;
    }

    private function isset_owner_user_company($company_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table_owner)
            ->where('companyID','=',$company_id)
            ->execute()
            ->as_array();

        $count = $res[0]['c'];
        if ($count > 0) return true;
        return false;
    }
}