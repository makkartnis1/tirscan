<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Parser extends Model {

    public $table = 'Services_Parser';


    public function save($info,$id)
    {
        $status = (isset($info['status']))?1:0;

        DB::update($this->table)
            ->set(array(
                'url'=>$info['url'],
                'captcha'=>$info['captcha'],
                'status'=>1
            ))
            ->where('id','=',$id)
            ->execute();
    }

}