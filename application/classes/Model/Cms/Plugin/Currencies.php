<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Currencies extends Model {

    public $table = 'Frontend_Currencies';

    public function get_all()
    {
        $res = DB::select()
            ->from($this->table)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }


    public function add($info)
    {

        $id = DB::insert($this->table,array('name','localeName','code'))
            ->values(array($info['name'],$info['localeName'],$info['code']))
            ->execute();
        $id = $id[0];
        return $id;
    }

    public function save($info,$id)
    {
        DB::update($this->table)
            ->set(array(
                'name'=>$info['name'],
                'localeName'=>$info['localeName'],
                'code'=>$info['code']
            ))
            ->where('ID','=',$id)
            ->execute();
    }


    public function del($id)
    {
        $res = DB::delete($this->table)
            ->where('ID','=',$id)
            ->execute();
        if ($res) return true;
        return null;
    }

    public function del_all(array $ids)
    {
        $res = DB::delete($this->table)
            ->where('ID','in',DB::expr('('.implode(',',$ids).')'))
            ->execute();
        if ($res) return true;
        return null;
    }

}