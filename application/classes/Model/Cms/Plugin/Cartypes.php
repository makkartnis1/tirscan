<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Cartypes extends Model {

    public $table = 'Frontend_User_Car_Types';


    public function get_name($id)
    {
        $res = DB::select('name')
                    ->from($this->table)
                    ->where('ID','=',$id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['name'];
                return null;
    }

    public function get_all()
    {
        $res = DB::select()
            ->from($this->table)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }


    public function add($info)
    {

        $id = DB::insert($this->table,array('name','localeName'))
            ->values(array($info['name'],$info['localeName']))
            ->execute();
        $id = $id[0];
        return $id;
    }

    public function save($info,$id)
    {
        DB::update($this->table)
            ->set(array(
                'name'=>$info['name'],
                'localeName'=>$info['localeName']
            ))
            ->where('ID','=',$id)
            ->execute();
    }


    public function del($id)
    {
        $res = DB::delete($this->table)
            ->where('ID','=',$id)
            ->execute();
        if ($res) return true;
        return null;
    }

    public function del_all(array $ids)
    {
        $res = DB::delete($this->table)
            ->where('ID','in',DB::expr('('.implode(',',$ids).')'))
            ->execute();
        if ($res) return true;
        return null;
    }

}