<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_PluginLng extends Model {

    public $t_lng = 'x_plugin_lngs';

    public function get_all_active()
    {
        $res = DB::select()
                    ->from($this->t_lng)
                    ->where('active','=',1)
                    ->order_by('def','desc')
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res;
                return null;
    }

    public function get_all()
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->order_by('def','desc')
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }

    public function get_one_by_pref($pref)
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->where('pref','=',$pref)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

}