<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Lng extends Model {

    public $t_lng = 'Frontend_Locales';
    public $t_vars = 'x_lng_vars';

    //3.06.2015
    public $lang_tables = array(
        'x_city_locales'=>'city_id',
        'x_item_description_locales'=>'item_description_id',
        'x_item_locales'=>'item_id',
        'x_item_rub_locales'=>'rub_id',
        'x_item_type_locales'=>'item_type_id',
        'x_lng_var_locales'=>'lng_var_id',
        'x_map_locales'=>'map_id',
        'x_material_locales'=>'material_id',
        'x_menu_locales'=>'menu_id',
        'x_page_locales'=>'page_id',
        'x_payment_adres_locales'=>'adres_id',
        'x_payment_method_locales'=>'payment_method_id',
        'x_payment_service_locales'=>'payment_service_id',
        'x_rub_locales'=>'rub_id',
        'x_seo_locales'=>'seo_id',
        'x_voting_locales'=>'voting_id',
        'x_voting_question_locales'=>'question_id'
    );

    //16.06.2015
    public function get_def()
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->where('ID','=',1)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

    //4.06.2015
    public function deactive(array $ids)
    {
        DB::update($this->t_lng)
            ->set(array(
                'status'=>'unactive',
            ))
            ->where('id','in',DB::expr('('.implode(',',$ids).')'))
            ->execute();
    }

    //4.06.2015
    public function active(array $ids)
    {
        DB::update($this->t_lng)
            ->set(array(
                'status'=>'active',
            ))
            ->where('id','in',DB::expr('('.implode(',',$ids).')'))
            ->execute();
    }

    //4.06.2015
    public function del($id)
    {
        DB::delete($this->t_lng)
            ->where('id','=',$id)
            ->where('id','!=',1)
            ->execute();
    }

    //3.06.2015
    public function get_one($id)
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->where('id','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

    //3.06.2015
    public function add(array $data)
    {
        $title_test = str_replace(' ','',$data['title']);
        $pref_test = str_replace(' ','',$data['pref']);

        if (strlen($title_test)==0 or strlen($pref_test)==0)
        {
            $errors = array();
            if (strlen($title_test)==0)
            {
                $errors['title_empty'] = 1;
            }
            if (strlen($pref_test)==0)
            {
                $errors['pref_empty'] = 1;
            }
            return array(
                'errors'=>$errors
            );
        }

        $isset = DB::select(DB::expr('count(id) as c'))
            ->from($this->t_lng)
            ->or_where('title','=',$data['title'])
            ->or_where('pref','=',$data['pref'])
            ->execute()
            ->as_array();
        if ($isset[0]['c'])
        {
            return array(
                'errors'=>array('lang_isset'=>1)
            );
        }

        $active = isset($data['active'])?1:0;
        $def = isset($data['def'])?1:0;

        $lang_ins = DB::insert($this->t_lng,array('pref','title','def','active'))
            ->values(array($data['pref'],$data['title'],$def,$active))
            ->execute();
        $lang_id = $lang_ins[0];

        foreach($this->lang_tables as $lang_table => $lang_table_val)
        {
            $isset_lang_items = DB::select(DB::expr('count(id) as c'))
                ->from($lang_table)
                ->where('lng_id','=',$lang_id)
                ->execute()
                ->as_array();
            if ($isset_lang_items[0]['c']>0) continue;
            $vals = DB::select($lang_table_val)
                ->from($lang_table)
                ->distinct(true)
                ->execute()
                ->as_array();
            foreach($vals as $val)
            {
                DB::insert($lang_table,array('lng_id',$lang_table_val))
                    ->values(array($lang_id,$val[$lang_table_val]))
                    ->execute();
            }
        }

        return array(
            'errors'=>array(),
            'data'=>array('id'=>$lang_id)
        );

    }

    //3.06.2015
    public function edit(array $data)
    {
        $title_test = str_replace(' ','',$data['title']);
        $pref_test = str_replace(' ','',$data['pref']);

        if (strlen($title_test)==0 or strlen($pref_test)==0)
        {
            $errors = array();
            if (strlen($title_test)==0)
            {
                $errors['title_empty'] = 1;
            }
            if (strlen($pref_test)==0)
            {
                $errors['pref_empty'] = 1;
            }
            return array(
                'errors'=>$errors
            );
        }

        $isset = DB::select(DB::expr('count(id) as c'))
            ->from($this->t_lng)
            ->and_where_open()
            ->or_where('title','=',$data['title'])
            ->or_where('pref','=',$data['pref'])
            ->and_where_close()
            ->where('id','!=',$data['id'])
            ->execute()
            ->as_array();
        if ($isset[0]['c'])
        {
            return array(
                'errors'=>array('lang_isset'=>1)
            );
        }

        $active = isset($data['active'])?1:0;
        $def = isset($data['def'])?1:0;

        DB::update($this->t_lng)
            ->set(array(
                'title'=>$data['title'],
                'pref'=>$data['pref'],
                'def'=>$def,
                'active'=>$active,
            ))
            ->where('id','=',$data['id'])
            ->execute();

        return array(
            'errors'=>array()
        );

    }

    public function get_all_active()
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->where('status','=','active')
            ->order_by('id','asc')
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }

    public function get_all()
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->order_by('id','asc')
            ->execute()
            ->as_array();
        if (!empty($res)) return $res;
        return null;
    }

    public function get_one_by_pref($pref)
    {
        $res = DB::select()
            ->from($this->t_lng)
            ->where('pref','=',$pref)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

    public function get_one_by_google_lang($google_lang)
    {
        $res = DB::select()

            ->from($this->t_lng)
            ->where('googleLang','=',$google_lang)
            ->execute()
            ->as_array();

        if (!empty($res)) return $res[0];
        return null;
    }

}