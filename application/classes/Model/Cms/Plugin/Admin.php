<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Admin extends Model {

    public function get_name($id)
    {
        $res = DB::select('name')
                    ->from('x_admins')
                    ->where('id','=',$id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['name'];
                return null;
    }

    public function get_id_main_wallet()
    {
        $res = DB::select('id')
                    ->from('x_admins')
                    ->where('wallet_main','=',1)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['id'];
                return null;
    }

    public function get_one_short($id,$lang_id)
    {
        $res = DB::select('x_admins.id','x_admins.name',array('x_admin_group_locales.title','group_name'))
            ->from('x_admins')
            ->join('x_admin_group_locales','left')->on('x_admin_group_locales.admin_group_id','=','x_admins.group_id')
            ->where('x_admin_group_locales.plugin_lng_id','=',$lang_id)
            ->where('x_admins.id','=',$id)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

    public function get_all()
    {
        $res = DB::select()
                    ->from('x_admins')
                    ->order_by('name')
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res;
                return null;
    }

    public function del_by_ids($ids)
    {
            DB::delete('x_admins')
                ->where('id','in',DB::expr('('.implode(',',$ids).')'))
                ->where('id','!=',1)
                ->execute();
    }

    public function del_by_id($id)
    {
        DB::delete('x_admins')
            ->where('id','=',$id)
            ->where('id','!=',1)
            ->execute();
    }

    public function active_by_ids($ids)
    {
        $res = DB::update('x_admins')
                    ->set(array(
                        'active'=>1,
                    ))
                    ->where('id','in',DB::expr('('.implode(',',$ids).')'))
                    ->where('id','!=',1)
                    ->execute();
        if ($res) return true;
        return null;
    }

    public function blocked_by_ids($ids)
    {
        $res = DB::update('x_admins')
            ->set(array(
                'active'=>0,
            ))
            ->where('id','in',DB::expr('('.implode(',',$ids).')'))
            ->where('id','!=',1)
            ->execute();
        if ($res) return true;
        return null;
    }

    public function check_email($email,$admin_id = null)
    {
        $res = DB::select('id')
            ->from('x_admins')
            ->where('email','=',$email);
        if ($admin_id) $res = $res->where('id','!=',$admin_id);
        $res = $res->limit(1)
            ->execute()
            ->as_array();
        if (empty($res)) return false;
        return true;
    }

    public function check_login($login,$admin_id = null)
    {
        $res = DB::select('id')
            ->from('x_admins')
            ->where('login','=',$login);
        if ($admin_id) $res = $res->where('id','!=',$admin_id);
        $res = $res->limit(1)
            ->execute()
            ->as_array();
        if (empty($res)) return false;
        return true;
    }

    public function valid($data,$admin_id = null,$change_pass = 1)
    {
        $errors = array();

//        $data['login'] = trim(preg_replace ('/[^a-zA-ZА-Яа-я0-9ІіЇїЄєҐґЁё_\-.,;]+/ui','',$data['login']));
//        $l_login = mb_strlen($data['login'],'UTF-8');
//        if ($l_login==0) $errors['login'] = 1;

        $data['name'] = trim(preg_replace ('/[^a-zA-ZА-Яа-яІіЇїЄєҐґЁё\-\s]+/ui','',$data['name']));
        $l_name = mb_strlen($data['name'],'UTF-8');
        if ($l_name==0) $errors['name'] = 1;

        if ($change_pass)
        {
            $data['pass'] = trim(preg_replace ('/[^a-zA-Z0-9_\-.,;]+/ui','',$data['pass']));
            $l_pass = mb_strlen($data['pass'],'UTF-8');
            if ($l_pass < 6) $errors['pass'] = 1;

            if ($data['pass']!=$data['conf_pass']) $errors['conf_pass'] = 1;

        }

        if ($this->check_email($data['email'],$admin_id)) $errors['email_isset'] = 1;

//        if ($this->check_login($data['login'],$admin_id)) $errors['login_isset'] = 1;

        $data['phone'] = htmlspecialchars($data['phone'],ENT_QUOTES);

        return array('data'=>$data,'errors'=>$errors);
    }




    public function add($data)
    {
        $res = DB::insert('x_admins',array('email','pass','name','phone','group_id'))
                    ->values(array($data['email'],Helper_Myauth::hash($data['pass']),$data['name'],$data['phone'],$data['group_id']))
                    ->execute();
                if (!empty($res)) return $res[0];
    }

    public function save($data,$admin_id,$change_pass)
    {
        $arr = array(
            'email'=>$data['email'],
            'name'=>$data['name'],
            'phone'=>$data['phone'],
            'group_id'=>$data['group_id']
        );

        if ($change_pass)
        {
            $arr['pass'] = Helper_Myauth::hash($data['pass']);
        }

        $res = DB::update('x_admins')
                    ->set($arr)
                    ->where('id','=',$admin_id)
                    ->execute();
        if ($res) return true;
        return null;
    }

    public function get_one($admin_id)
    {
        $res = DB::select()
                    ->from('x_admins')
                    ->where('id','=',$admin_id)
                    ->limit(1)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0];
                return null;
    }
}