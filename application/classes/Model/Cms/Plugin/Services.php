<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_Services extends Model {

    public $table = 'Frontend_Services';


    public function save($info,$id)
    {
        DB::update($this->table)
            ->set(array(
                'amount'=>$info['amount']
            ))
            ->where('ID','=',$id)
            ->execute();
    }

}