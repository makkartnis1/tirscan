<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin_UserGroup extends Model {

    public function get_title($id)
    {
        $res = DB::select('name')
                    ->from('Frontend_User_Types')
                    ->where('id','=',$id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['name'];
                return null;
    }
}