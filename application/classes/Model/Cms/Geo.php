<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Geo extends Model {

    public $table = 'Frontend_Geo';
    public $table_locales = 'Frontend_Geo_Locales';

    public function isset_by_place_id($place_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table)
            ->where('placeID','=',$place_id)
            ->execute()
            ->as_array();

        if ($res[0]['c'] == 0) return false;
        return true;
    }

    public function save($place_id,$lng_id,$lat,$lon,$name,$full_name,$country,$type,$level)
    {
        $check_isset = $this->get_id_by_place_id($place_id);
        if ($check_isset)
        {
            $geo_id = $check_isset;
            if (!$this->checkIseetLocale($lng_id,$geo_id))
            {
                $status = $this->save_updateLocale($geo_id,$lng_id,$name,$full_name,$country);
                if (!$status) return false;
            }

            return $geo_id;
        }
        else
        {
            $geo_id = $this->save_create($place_id,$lng_id,$lat,$lon,$name,$full_name,$country,$type,$level);
            if (!$geo_id) return false;
            return $geo_id;
        }
    }

    public function save_updateLocale($geo_id,$lang_id,$name,$full_name,$country)
    {


        $create_status = 1;
        $db= Database::instance();
        $db->begin();
        try
        {
            DB::insert($this->table_locales,array('localeID','geoID','name','fullAddress','country'))
                ->values(array($lang_id,$geo_id,$name,$full_name,$country))
                ->execute();

            $db->commit();
        }
        catch (Database_Exception $e)
        {
            $db->rollback();
            $create_status = 0;
            Helper_Logs_Geo::error('оновлення локалізації місця по localeID = '.$lang_id,$geo_id.' '.$full_name,$e->getMessage());
        }

        if ($create_status == 1)
        {
            Helper_Logs_Geo::success('оновлення локалізації місця по localeID = '.$lang_id,$geo_id.' '.$full_name);
            return true;
        }
        else
        {
            return false;
        }
    }

    private function save_create($place_id,$lang_id,$lat,$lon,$name,$full_name,$country,$type,$level)
    {
        $create_status = 1;

        $db= Database::instance();
        $db->begin();
        try
        {
            $geo = DB::insert($this->table,array('lat','lng','placeID','level','type'))
                ->values(array($lat,$lon,$place_id,$level,$type))
                ->execute();
            $geo_id = $geo[0];

            DB::insert($this->table_locales,array('localeID','geoID','name','fullAddress','country'))
                ->values(array($lang_id,$geo_id,$name,$full_name,$country))
                ->execute();

            $db->commit();
        }
        catch (Database_Exception $e)
        {
            $db->rollback();
            $create_status = 0;
            Helper_Logs_Geo::error('збереження нового місця в базу',$full_name,$e->getMessage());
        }


        if ($create_status == 1)
        {
            Helper_Logs_Geo::success('збереження нового місця в базу',$geo_id.' '.$full_name);
            return $geo_id;
        }
        else
        {
            return false;
        }

    }

    public function set_parent($id,$parent)
    {
        DB::update($this->table)
            ->set(array(
                'parentID'=>$parent,
            ))
            ->where('ID','=',$id)
            ->execute();
    }

    public function get_id_by_place_id($place_id)
    {
        $geo = DB::select('ID')
            ->from($this->table)
            ->where('placeID','=',$place_id)
            ->execute()
            ->as_array();
        if (!empty($geo)) return $geo[0]['ID'];
        return null;
    }

    public function get_id_by_place_id_and_lang($place_id,$lang_id)
    {
        $geo = DB::select($this->table.'.ID')
            ->from($this->table)
            ->join($this->table_locales,'inner')->on($this->table_locales.'.geoID','=',$this->table.'.ID')
            ->where($this->table_locales.'.localeID','=',$lang_id)
            ->where($this->table.'.placeID','=',$place_id)
            ->execute()
            ->as_array();
        if (!empty($geo)) return $geo[0]['ID'];
        return null;
    }


    public function get_ids_by_id($id,$array_ids)
    {
        $array_ids[] = $id;
        $geo = DB::select('parentID')
            ->from($this->table)
            ->where('ID','=',$id)
            ->execute()
            ->as_array();
        if ($geo[0]['parentID']) return $this->get_ids_by_id($geo[0]['parentID'],$array_ids);
        return $array_ids;
    }

    public function get_geo_id_by_name($name,$lng_id)
    {
        $name = addslashes($name);
        $res = DB::select('geoID')
            ->from($this->table_locales)
            ->where('name','=',$name)
            ->where('localeID','=',$lng_id)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0]['geoID'];
        return null;
    }


    public function getNameByIDs($ids)
    {

        $geo = DB::select('ID')
            ->from($this->table)
            ->where('ID','in',DB::expr('('.implode(',',$ids).')'))
            ->order_by('level','desc')
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($geo)) return null;

        $geo_id = $geo[0]['ID'];

        $locales = DB::select('fullAddress')
            ->from($this->table_locales)
            ->where('geoID','=',$geo_id)
            ->order_by('localeID','asc')
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($locales)) return null;

        return $locales[0]['fullAddress'];

    }

    public function getPlaceByIds(array $ids)
    {
        $res = DB::select('ID')
            ->from($this->table)
            ->where('ID','IN',DB::expr('('.implode(',',$ids).')'))
            ->order_by('level','desc')
            ->limit(1)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0]['ID'];
        return null;
    }

    public function getCoordById($id)
    {
        $res = DB::select('lat','lng')
            ->from($this->table)
            ->where('ID','=',$id)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }

    private function checkIseetLocale($lng_id,$geo_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table_locales)
            ->where('localeID','=',$lng_id)
            ->where('geoID','=',$geo_id)
            ->execute()
            ->as_array();

        $c = $res[0]['c'];

        if ($c == 0) return false;
        return true;
    }

    public function getGeoByRequestID($requestID,$table_relative)
    {
        $geo_ids = DB::select(
            'geoID','type','groupNumber'
        )->from($table_relative)
            ->where('requestID','=',$requestID)
            ->order_by('groupNumber','asc')
            ->execute()
            ->as_array();
        if (empty($geo_ids)) return null;

        $geo_groups = array();


        foreach($geo_ids as $g)
        {
            if (!isset($geo_groups[$g['groupNumber'].'_'.$g['type']]))
            {
                $geo_groups[$g['groupNumber'].'_'.$g['type']] = array('type'=>$g['type'],'ids'=>array());
            }

            $geo_groups[$g['groupNumber'].'_'.$g['type']]['ids'][] = $g['geoID'];
        }


        foreach($geo_groups as $group_number => $group)
        {

            $geo = DB::select('ID')
                ->from($this->table)
                ->where('ID','in',DB::expr('('.implode(',',$group['ids']).')'))
                ->order_by('level','desc')
                ->limit(1)
                ->execute()
                ->as_array();

            $geo_id = $geo[0]['ID'];

            $locales = DB::select('fullAddress')
                ->from($this->table_locales)
                ->where('geoID','=',$geo_id)
                ->order_by('localeID','asc')
                ->limit(1)
                ->execute()
                ->as_array();

            $geo_groups[$group_number]['name'] = $locales[0]['fullAddress'];

        }

        return $geo_groups;
    }


}