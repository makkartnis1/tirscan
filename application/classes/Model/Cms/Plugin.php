<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cms_Plugin extends Model {


    public function getViewPlugins($lng_id,$group_id)
    {


        $res = DB::select(
            'x_plugins.id',
            'x_plugins.sub',
            'x_plugins.icon',
            'x_plugins.controller',
            'x_plugins.action',
            'x_plugin_locales.title'
        )
                    ->from('x_plugins')
                    ->join('x_plugin_locales','left')->on('x_plugin_locales.plugin_id','=','x_plugins.id')
                    ->where('x_plugin_locales.active','=',1)
                    ->where('x_plugins.is_show','=',1)
                    ->where('x_plugin_locales.plugin_lng_id','=',$lng_id);

                    if ($group_id > 2)
                    {
                        $res = $res->where('x_plugins.id','!=','59');
                    }

                    if ($group_id > 3)
                    {
                        $res = $res->where('x_plugins.id','!=','84');
                    }

                    $res = $res->order_by('srt')
                    ->execute()
                    ->as_array();

        return Helper_Tree::get_tree($res);
    }

    public function getFirst($lng_id)
    {
        $res = DB::select(
            'x_plugins.id',
            'x_plugins.controller',
            'x_plugins.action'
        )
                    ->from('x_plugins')
                    ->join('x_plugin_locales','left')->on('x_plugin_locales.plugin_id','=','x_plugins.id')
                    ->where('x_plugins.is_show','=',1)
                    ->where('x_plugin_locales.active','=',1)
                    ->where('x_plugin_locales.plugin_lng_id','=',$lng_id)
                    ->where('x_plugins.controller','!=','')
                    ->order_by('sub2')
                    ->limit(1);
//                    echo $res; die;
                    $res = $res->execute()
                    ->as_array();
                if (!empty($res)) return $res[0];
                return null;
    }


    public function getDefaultPrefixLang()
    {
        $res = DB::select('pref')
                    ->from('x_plugin_lngs')
                    ->where('def','=',1)
                    ->limit(1)
                    ->execute()
                    ->as_array();
        return $res[0]['pref'];
    }

    public function issetLangByPref($pref)
    {
        $res = DB::select(DB::expr('count(id) as c'))
                    ->from('x_plugin_lngs')
                    ->where('pref','=',$pref)
                    ->execute()
                    ->as_array();
        if ($res[0]=='1') return true;
        return false;
    }

    public function getIdByPref($pref)
    {
        $res = DB::select('id')
            ->from('x_plugin_lngs')
            ->where('pref','=',$pref)
            ->limit(1)
            ->execute()
            ->as_array();
        return $res[0]['id'];
    }

    public function getPluginInfo($controller,$action,$lng_id)
    {

        if ($action=='index') $action = '';
        $controller = strtolower($controller);


        $res = DB::select('x_plugins.id','x_plugins.sub','x_plugin_locales.title')
            ->from('x_plugins')
            ->join('x_plugin_locales','left')->on('x_plugin_locales.plugin_id','=','x_plugins.id')
            ->where('x_plugins.controller','=',$controller)
            ->where('x_plugins.action','=',$action)
            ->where('x_plugin_locales.plugin_lng_id','=',$lng_id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (!empty($res)) return $res[0];
        return null;
    }


    public function isActivePlugin($controller,$action)
    {
        $res = DB::select('is_show')
                    ->from('x_plugins')
                    ->where('controller','=',$controller)
                    ->where('action','=',$action)
                    ->execute()
                    ->as_array();
                if (!empty($res))
                {
                    if ($res[0]['is_show']) return true;
                }
                return false;
    }

}