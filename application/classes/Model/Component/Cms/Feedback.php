<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Feedback extends Model {

    public $t = 'Frontend_Feedback';

    public function delete($id)
    {
        try {

            DB::delete($this->t)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }

    public function countNew()
    {

        $res = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->t)
                    ->where('status','=','unread')
                    ->execute()
                    ->as_array();
        if (!empty($res)) return $res[0]['c'];
        return null;

    }
}
