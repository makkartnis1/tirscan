<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_SimpleComments extends Model {

    public $table = 'Frontend_Comments';


    public function valid($data)
    {
        $errors = array();

        $errors['empty_text'] = 1;

        if (!Helper_Valid::is_empty($data['text']))
        {
            unset($errors['empty_text']);
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data)
    {
        $data = $this->filter($data);

        $arr = array(
            'text'=>$data['text']
        );

        DB::update($this->table)
            ->set($arr)
            ->where('ID','=',$id)
            ->execute();
    }


    public function add($data)
    {
        $data = $this->filter($data);

        $res = DB::insert($this->table,array(
            'text'
        ))
            ->values(array(
                $data['text'],
            ))
            ->execute();

        $id = $res[0];

        return $id;
    }


    public function delete($id)
    {
        try {

            DB::delete($this->table)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table.'.*'
        )
            ->from($this->table)
            ->where($this->table.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        return $info;

    }

    private function filter($data)
    {
        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }
        return $data;
    }

}