<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Notification extends Model {


    public function delete($id)
    {
        DB::delete('x_notification_templates')
            ->where('id','=',$id)
            ->execute();
    }

    public function create($data,$langs)
    {
        $n = DB::insert('x_notification_templates',array('name'))->values(array($data['name']))->execute();
        $n_id = $n[0];
        $this->update_locales($n_id,$data,$langs);
        return $n_id;
    }

    public function update($id,$data,$langs)
    {
        DB::update('x_notification_templates')
            ->set(array(
                'name'=>$data['name'],
            ))
            ->where('id','=',$id)
            ->execute();

        $this->update_locales($id,$data,$langs);
    }


    private function update_locales($id,$data,$langs)
    {
        foreach($langs as $lang)
        {
            DB::update('x_notification_template_locales')
                ->set(array(
                    'title'=>$data['title_'.$lang['uri']],
                    'text'=>$data['text_'.$lang['uri']]
                ))
                ->where('lng_id','=',$lang['ID'])
                ->where('notification_template_id','=',$id)
                ->execute();
        }
    }

    public function get($id)
    {
        $info = DB::select()
            ->from('x_notification_templates')
            ->where('id','=',$id)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        $locales = DB::select()
            ->from('x_notification_template_locales')
            ->where('notification_template_id','=',$id)
            ->execute()
            ->as_array();

        $locales = Helper_Array::change_keys($locales,'lng_id');

        return array(
            'info'=>$info,
            'locales'=>$locales
        );
    }


}