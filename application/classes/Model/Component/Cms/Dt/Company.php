<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Company extends Model {

    public $table = 'Frontend_Companies';

    public function get($post)
    {
        $companies = [
            'Текнолайт',
            'Trianlogist',
            'Tir-parts',
            'Stellar',
            'Arbec',
            'Cardinal-trans',
            'Genspeed',
            'Liber-trans',
            'Nek',
            'Rion'
        ];

        $result = [];
        $for_sort = [];
        $i=0;
        foreach($companies as $c)
        {
            $i++;
            //рейтинг
            $marks_count = rand(50,100);
            $marks_sum = rand($marks_count,$marks_count*5);;
            $rating = $marks_sum / $marks_count;

            //транспорт
            $transports = [];
            $transports['closed'] = rand(100,10000);
            $transports['process'] = rand(0,100);
            $transports['all'] = $transports['closed'] + $transports['process'] + rand(10,1000);


            //вантажі
            $loads = [];
            $loads['closed'] = rand(100,10000);
            $loads['process'] = rand(0,100);
            $loads['all'] = $loads['closed'] + $loads['process'] + rand(10,1000);


            //тендери
            $tenders = [];
            $tenders['closed'] = rand(10,100);
            $tenders['process'] = rand(0,3);
            $tenders['all'] = $tenders['closed'] + $tenders['process'] + rand(10,30);

            $result[$i] = [
                'number'=>$i,
                'rating'=>$rating,
                'name'=>$c,
                'marks'=>$marks_count,
                'transports'=>$transports,
                'loads'=>$loads,
                'tenders'=>$tenders,
                'managers'=>rand(10,100),
                'own_transports'=>rand(0,100),
                'company_news'=>rand(0,30),
                'services_money'=>rand(0,1000)
            ];

            $for_sort[$i] = $rating;
            
        }

        arsort($for_sort);
        $new_result = [];
        foreach($for_sort as $f_k => $f_v)
            $new_result[] = $result[$f_k];

        $arr_help_1 = [
            'transports'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'loads'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'tenders'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'marks'=>'primary',
            'managers'=>'success',
            'own_transports'=>'success',
            'company_news'=>'success',
            'services_money'=>'success'

        ];

        $x = $post['start'];
        foreach($new_result as &$n)
        {

            $x++;
            $n['rating'] = '<span class="badge badge-inverse">'.$x.'</span> '.round($n['rating'],2);

            foreach($arr_help_1 as $key => $val)
            {
                if (is_array($val))
                {
                    foreach($val as $k => $v)
                    {
                        if ($n[$key][$k] > 0)
                        {
                            $n[$key][$k] = '<a href="#" class="badge badge-'.$v.'">'.$n[$key][$k].'</a>';
                        }
                        else
                        {
                            $n[$key][$k] = '<span class="no_data">---</span>';
                        }
                    }
                }
                else
                {
                    if ($n[$key] > 0)
                    {
                        $n[$key] = '<a href="#" class="badge badge-'.$val.'">'.$n[$key].'</a>';
                    }
                    else
                    {
                        $n[$key] = '<span class="no_data">---</span>';
                    }
                }
            }


        }

        return $new_result;
    }


    public function countAll()
    {
        return 10;
    }


    public function countFiltered()
    {
        return 10;
    }

}