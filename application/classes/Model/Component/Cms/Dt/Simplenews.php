<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Simplenews extends Model {

    public $table = 'Frontend_News';
    public $table_locales = 'Frontend_News_Locales';
    public $table_user_locales = 'Frontend_User_Locales';

    public function get($post,$get)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }

        $select_arr = array(
            $this->table.'.*',
            array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table.'.userID order by localeID limit 1)'),'user_name'),
            array(DB::expr('(select title from '.$this->table_locales.' where newsID = '.$this->table.'.ID order by localeID limit 1)'),'title')
        );


        $res = DB::select_array($select_arr)
            ->from($this->table);

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['no_users']))
        {
            $res = $res->where($this->table.'.userID','=',NULL);
        }

        if (isset($get['users']))
        {
            $res = $res->where($this->table.'.userID','!=',NULL);
        }

        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];

        if ($order_col == 'title')
        {
            $order_col = DB::expr('(select title from '.$this->table_locales.' where newsID = '.$this->table.'.ID order by localeID limit 1)');
        }elseif($order_col == 'user_name')
        {
            $order_col = DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table.'.userID order by localeID limit 1)');
        }
        else
        {
            $order_col = $this->table.'.'.$order_col;
        }


        $type_order = $post['order'][0]['dir'];

        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            return $res;
        }
        return array();
    }


    public function countAll($get)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table);

            if (isset($get['user_id']))
            {
                $res = $res->where($this->table.'.userID','=',$get['user_id']);
            }

            if (isset($get['no_users']))
            {
                $res = $res->where($this->table.'.userID','=',NULL);
            }

            if (isset($get['users']))
            {
                $res = $res->where($this->table.'.userID','!=',NULL);
            }

            $res =$res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$get)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }


        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table);

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['no_users']))
        {
            $res = $res->where($this->table.'.userID','=',NULL);
        }

        if (isset($get['users']))
        {
            $res = $res->where($this->table.'.userID','!=',NULL);
        }

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }



}