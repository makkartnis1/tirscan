<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Comments extends Model {

    public $t_comments = 'Frontend_Comments';
    public $t_company_locales = 'Frontend_Company_Locales';
    public $t_user_locales = 'Frontend_User_Locales';


    public function get($post,$get)
    {
        $offset = $post['start'];
        $limit = $post['length'];

        $select_arr = array(
            $this->t_comments.'.*',
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_comments.'.userID ORDER BY localeID LIMIT 1)'),'userName'),
            array(DB::expr('(SELECT name FROM '.$this->t_company_locales.' WHERE companyID = '.$this->t_comments.'.companyID limit 1)'),'companyName')
        );


        $res = DB::select_array($select_arr)
            ->from($this->t_comments);


        if (isset($get['companyID']))
            $res = $res->where($this->t_comments.'.companyID','=',$get['companyID']);

        if (isset($get['userID']))
            $res = $res->where($this->t_comments.'.userID','=',$get['userID']);

        if (isset($get['cargoID']))
            $res = $res->where($this->t_comments.'.cargoID','=',$get['cargoID']);

        if (isset($get['transportID']))
            $res = $res->where($this->t_comments.'.transportID','=',$get['transportID']);

        if (isset($get['text']))
            $res = $res->where($this->t_comments.'.text','LIKE',DB::expr("'%".$get['text']."%'"));

        $res = $res
            ->order_by($this->t_comments.'.ID','DESC')
            ->offset($offset)
            ->limit($limit)
            ->execute()->as_array();

        if (!empty($res))
        {
            $res = $this->add_short_text($res,'text');

            return $res;
        }
        return array();

    }


    private function add_short_text(array $res, $key)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r[$key]);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_'.$key] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_'.$key] = $r[$key];
        }
        return $res;
    }


    public function countAll($get)
    {
        $res = DB::select(DB::expr('count('.$this->t_comments.'.ID) as c'))
            ->from($this->t_comments);

        if (isset($get['companyID']))
            $res = $res->where($this->t_comments.'.companyID','=',$get['companyID']);

        if (isset($get['userID']))
            $res = $res->where($this->t_comments.'.userID','=',$get['userID']);

        if (isset($get['cargoID']))
            $res = $res->where($this->t_comments.'.cargoID','=',$get['cargoID']);

        if (isset($get['transportID']))
            $res = $res->where($this->t_comments.'.transportID','=',$get['transportID']);

        if (isset($get['text']))
            $res = $res->where($this->t_comments.'.text','LIKE',DB::expr("'%".$get['text']."%'"));

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }


}