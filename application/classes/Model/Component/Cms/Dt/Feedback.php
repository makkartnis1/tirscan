<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Feedback extends Model {

    public $t = 'Frontend_Feedback';

    public function get($post)
    {
        $offset = $post['start'];
        $limit = $post['length'];

        $select_arr = array(
            $this->t.'.*',
        );

        $res = DB::select_array($select_arr)
            ->from($this->t);

        $res = $res
            ->order_by($this->t.'.ID','DESC')
            ->offset($offset)
            ->limit($limit)
            ->execute()->as_array();

        if (!empty($res))
        {
            $res = $this->add_short_text($res,'message');

            foreach($res as $r)
                $this->setReaded($r['ID']);

            return $res;
        }
        return array();

    }


    private function add_short_text(array $res, $key)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r[$key]);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_'.$key] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_'.$key] = $r[$key];
        }
        return $res;
    }


    private function setReaded($id)
    {
        DB::update($this->t)
                    ->set(array(
                        'status'=>'read'
                    ))
                    ->where('ID','=',$id)
                    ->execute();
    }


    public function countAll()
    {
        $res = DB::select(DB::expr('count('.$this->t.'.ID) as c'))
            ->from($this->t);

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }


}