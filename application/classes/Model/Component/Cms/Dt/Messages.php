<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Messages extends Model {

    public $t_d = 'Frontend_Dialogs';
    public $t_d_u = 'Frontend_Dialogs_Unread';
    public $t_m = 'Frontend_Messages';
    public $t_m_u = 'Frontend_Messages_Unread';
    public $t_admins = 'x_admins';
    public $t_users = 'Frontend_Users';
    public $t_user_locales = 'Frontend_User_Locales';

    public function get($post,$id)
    {
        $offset = $post['start'];
        $limit = $post['length'];

        $select_arr = array(
            $this->t_m.'.ID',
            $this->t_m.'.created',
            $this->t_m.'.userMessageID',
            $this->t_m.'.adminMessageID',
            $this->t_m.'.message',
            $this->t_m.'.deleted',
            $this->t_m_u.'.toAdminID',
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_m.'.userMessageID ORDER BY localeID LIMIT 1)'),'userName'),
            array(DB::expr('(SELECT name FROM '.$this->t_admins.' WHERE id = '.$this->t_m.'.adminMessageID limit 1)'),'adminName')
        );


        $res = DB::select_array($select_arr)
            ->from($this->t_m)

            ->join($this->t_m_u,'left')
            ->on($this->t_m_u.'.messageID','=',$this->t_m.'.ID');


        $res = $res
            ->where($this->t_m.'.dialogID','=',$id)
            ->order_by($this->t_m.'.ID','DESC')
            ->offset($offset)
            ->limit($limit)
            ->execute()->as_array();

        if (!empty($res))
        {
            $res = $this->add_short_text($res,'message');

            foreach($res as $r)
                $this->setReaded($r['ID']);

            return $res;
        }
        return array();

    }

    private function setReaded($id_mess)
    {
        $res = DB::delete($this->t_m_u)
                    ->where('messageID','=',$id_mess)
                    ->where('toAdminID','!=','NULL')
                    ->execute();
        if ($res) return true;
        return null;
    }

    private function add_short_text(array $res, $key)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r[$key]);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_'.$key] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_'.$key] = $r[$key];
        }
        return $res;
    }


    public function countAll($id)
    {
        $res = DB::select(DB::expr('count('.$this->t_m.'.ID) as c'))
            ->from($this->t_m)
            ->where($this->t_m.'.dialogID','=',$id);

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($id)
    {
        $res = DB::select(DB::expr('count('.$this->t_m.'.ID) as c'))
            ->from($this->t_m)
            ->where($this->t_m.'.dialogID','=',$id);

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }

}