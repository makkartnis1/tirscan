<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_TransportRequests extends Model {

    public $table = 'Frontend_Transports';
    public $table_related = 'Frontend_Cargo';
    public $table_geo_rel = 'Frontend_Transport_to_Geo_Relation';
    public $table_geo_rel_related = 'Frontend_Cargo_to_Geo_Relation';
    public $table_user = 'Frontend_Users';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_user_to_req = 'Frontend_User_to_Transport_Requests';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_company = 'Frontend_Companies';
    public $table_price = 'Frontend_Request_Prices';
    public $table_cur = 'Frontend_Currencies';
    public $table_user_car = 'Frontend_User_Cars';
    public $table_user_car_type = 'Frontend_User_Car_Types';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_geo = 'Frontend_Geo';

    public function get($post,$cms_lang,$filter)
    {
        $m_geo = new Model_Cms_Geo();

        $select_arr = array(
            $this->table.'.*',
            array($this->table.'.userID','avtor_id'),
            array($this->table_user.'.companyID','avtor_company_id'),
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'avtor_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'avtor_company_name'),
            $this->table_price.'.value',
            $this->table_price.'.paymentType',
            $this->table_price.'.PDV',
            $this->table_price.'.onLoad',
            $this->table_price.'.onUnload',
            $this->table_price.'.onPrepay',
            $this->table_price.'.advancedPayment',
            $this->table_price.'.specifiedPrice',
            $this->table_price.'.customPriceType',
            array($this->table_cur.'.code','currency_name'),
            array($this->table_user_car.'.number','car_number'),
            array($this->table_user_car.'.volume','car_volume'),
            array($this->table_user_car.'.sizeX','car_sizeX'),
            array($this->table_user_car.'.sizeY','car_sizeY'),
            array($this->table_user_car.'.sizeZ','car_sizeZ'),
            array($this->table_user_car.'.liftingCapacity','car_liftingCapacity'),
            array($this->table_user_car.'.type','car_type'),
            array($this->table_user_car_type.'.name','car_type_name'),
        );


        $res = DB::select_array($select_arr)
            ->from($this->table)

            ->join($this->table_user,'left')
            ->on($this->table_user.'.ID','=',$this->table.'.userID')

            ->join($this->table_price,'left')
            ->on($this->table_price.'.ID','=',$this->table.'.priceID')

            ->join($this->table_cur,'left')
            ->on($this->table_cur.'.ID','=',$this->table_price.'.currencyID')

            ->join($this->table_user_car,'left')
            ->on($this->table_user_car.'.ID','=',$this->table.'.userCarID')

            ->join($this->table_user_car_type,'left')
            ->on($this->table_user_car_type.'.ID','=',$this->table_user_car.'.carTypeID');


        if (isset($filter['related_ids']))
        {

            foreach($filter['related_ids'] as $rel_key => $rel_val)
            {
                $res = $res->where(
                    DB::expr(
                        '(select ID from '.$this->table_geo_rel.' where requestID = '.$this->table.'.ID and type = \''.$rel_key.'\' and geoID in ('.implode(',',$rel_val).') limit 1)'
                    ),
                    '!=',
                    NULL
                );
            }

            $res = $res->where($this->table.'.dateTo','>=',$filter['date_from']);
            $res = $res->where($this->table.'.dateFrom','<=',$filter['date_from']);
            $res = $res->where($this->table.'.status','!=','completed');
            $res = $res->where($this->table.'.status','!=','outdated');

        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.created','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.created','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['dateFrom1']))
        {
            $res = $res->where($this->table.'.dateFrom','>=',$filter['dateFrom1']);
        }

        if (isset($filter['dateFrom2']))
        {
            $res = $res->where($this->table.'.dateFrom','<=',$filter['dateFrom2'].' 23:59:59');
        }

        if (isset($filter['dateTo1']))
        {
            $res = $res->where($this->table.'.dateTo','>=',$filter['dateTo1']);
        }

        if (isset($filter['dateTo2']))
        {
            $res = $res->where($this->table.'.dateTo','<=',$filter['dateTo2'].' 23:59:59');
        }

        if (isset($filter['status']))
        {
            $res = $res->where($this->table.'.status','=',$filter['status']);
        }

        if (isset($filter['userID']))
        {
            $res = $res->where($this->table.'.userID','=',$filter['userID']);
        }

        if (isset($filter['userCompanyID']))
        {
            $res = $res->where($this->table_user.'.companyID','=',$filter['userCompanyID']);
        }

        if (isset($filter['carTypeID']))
        {
            $res = $res->where($this->table_user_car.'.carTypeID','=',$filter['carTypeID']);
        }

        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                if (isset($filter['radius_from']))
                {
                    $exp_ids = explode(',',$places_from[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_from'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius_from))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();



                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius_from = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius_from = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','from')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius_from).')')),
                        '!=',
                        '0'
                    );

                }
                else
                {
                    foreach($places_from as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','from')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                '0'
                            );
                        }
                    }
                }

            }

        }

        if (isset($filter['places_to']))
        {
            $places_to = $filter['places_to'];

            if (is_array($places_to))
            {
                if (isset($filter['radius_to']))
                {
                    $exp_ids = explode(',',$places_to[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_to'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius_to))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();
                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius_to = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius_to = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','to')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius_to).')')),
                        '!=',
                        '0'
                    );
                }
                else
                {
                    foreach($places_to as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','to')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                '0'
                            );
                        }
                    }
                }


            }
        }

        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];

        $order_col = $this->table.'.ID';
        $type_order = 'DESC';


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])


            ->execute()->as_array();

        if (!empty($res))
        {
            foreach($res as &$r)
            {
                $r = $this->filter($r);

                $r['from_places'] = $this->get_places($r['ID'],'from');
                $r['to_places'] = $this->get_places($r['ID'],'to');
                $r['price'] = $this->get_price($r);
                $r['transport'] = $this->get_transport($r);
                $r['user'] = $this->get_avtor($r,$cms_lang);
                $r['status_work'] = $this->get_req_status($r['ID'],$cms_lang);
                $r['requests'] = $this->get_user_requests($r['ID'],$cms_lang);
                $r['related'] = $this->get_count_related($r['ID'],$r['dateFrom']);
                $r['created'] = date("d.m H:i",strtotime($r['created']));
                $r['load_date'] = ($r['dateFrom'] == $r['dateTo'])?date("d.m",strtotime($r['dateFrom'])):'від '.date("d.m",strtotime($r['dateFrom'])).'<br>до '.date("d.m",strtotime($r['dateTo']));
            }
            return $res;
        }
        return array();

    }

    private function get_count_related($req_id,$date_from)
    {
        $res = DB::select($this->table_geo_rel.'.groupNumber',$this->table_geo.'.ID',$this->table_geo_rel.'.type')
            ->from($this->table_geo)
            ->join($this->table_geo_rel,'inner')->on($this->table_geo.'.ID','=',$this->table_geo_rel.'.geoID')
            ->where($this->table_geo_rel.'.requestID','=',$req_id)
            ->order_by($this->table_geo.'.level','desc')
            ->execute()
            ->as_array();

        if (empty($res)) return null;

        $new_res = [];
        foreach($res as $r)
        {
            $key = $r['groupNumber'].'-'.$r['type'];
            if (!array_key_exists($key,$new_res))
                $new_res[$key] = [
                    'type'=>$r['type'],
                    'id'=>$r['ID']
                ];
        }


        $new_res_final = [];
        foreach($new_res as $r)
        {
            $key = $r['type'];
            if (!array_key_exists($key,$new_res_final))
                $new_res_final[$key] = [];
            $new_res_final[$key][] = $r['id'];
        }


        $res = DB::select(DB::expr('count('.$this->table_related.'.ID) as c'))
            ->from($this->table_related);

        foreach($new_res_final as $key => $val)
        {
            $res = $res->where(
                DB::expr(
                    '(select ID from '.$this->table_geo_rel_related.' where requestID = '.$this->table_related.'.ID and type = \''.$key.'\' and geoID in ('.implode(',',$val).') limit 1)'
                ),
                '!=',
                NULL
            );
        }

        $res = $res->where($this->table_related.'.dateTo','>=',$date_from);
        $res = $res->where($this->table_related.'.dateFrom','<=',$date_from);

        $res = $res->where($this->table_related.'.status','!=','completed');
        $res = $res->where($this->table_related.'.status','!=','outdated');

        $res = $res->execute()
            ->as_array();

        $url = [
            'date_from'=>$date_from,
            'related_ids'=>$new_res_final,
            'transport_id'=>$req_id
        ];

        return ($res[0]['c'])?'<a target="_blank" href="/cms/uk/cargoRequests?'.http_build_query($url).'" class="badge badge-primary">'.$res[0]['c'].'</a>':'<a href="#" class="badge">0</a>';

    }

    private function filter($r)
    {
        $r['car_volume'] = (float) $r['car_volume'];
        $r['car_sizeX'] = (float) $r['car_sizeX'];
        $r['car_sizeY'] = (float) $r['car_sizeY'];
        $r['car_sizeZ'] = (float) $r['car_sizeZ'];
        $r['car_liftingCapacity'] = (float) $r['car_liftingCapacity'];
        if (!$r['info']) $r['info'] = '<i>відсутній</i>';
        return $r;
    }

    private function get_places($req_id,$type)
    {
        $res = DB::select('geoID','groupNumber')
            ->from($this->table_geo_rel)
            ->where('requestID','=',$req_id)
            ->where('type','=',$type)
            ->execute()
            ->as_array();


        $new_res = [];
        foreach($res as $r)
        {
            if (!isset($new_res[$r['groupNumber']])) $new_res[$r['groupNumber']] = [];
            $new_res[$r['groupNumber']][] = $r['geoID'];
        }

        $places = [];

        foreach($new_res as $n_r)
        {
            $geo = DB::select(
                array(DB::expr('(select '.$this->table_geo_locales.'.name from '.$this->table_geo_locales.' where '.$this->table_geo_locales.'.geoID = '.$this->table_geo.'.ID order by '.$this->table_geo_locales.'.localeID limit 1)'),'name'),
                array(DB::expr('(select '.$this->table_geo_locales.'.country from '.$this->table_geo_locales.' where '.$this->table_geo_locales.'.geoID = '.$this->table_geo.'.ID order by '.$this->table_geo_locales.'.localeID limit 1)'),'country')
            )
                ->from($this->table_geo)
                ->where($this->table_geo.'.ID','in',DB::expr('('.implode(',',$n_r).')'))
                ->order_by($this->table_geo.'.level','desc')
                ->limit(1)
                ->execute()
                ->as_array();

            $places[] = $geo[0]['name'].'<br>('.$geo[0]['country'].')';

        }

        return implode('<br>',$places);
    }

    private function get_price($data)
    {
        if ($data['specifiedPrice'] == 'no') return 'не&nbsp;вказано';
        $return = [];

        if ($data['currency_name'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['currency_name'];

        }elseif($data['customPriceType'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['customPriceType'];
        }

        switch ($data['paymentType']) {
            case 'b/g':
                $return[] = 'б/г';
                break;
            case 'gotivka':
                $return[] = 'готівка';
                break;
            case 'combined':
                $return[] = 'комбінована';
                break;
            case 'card':
                $return[] = 'на картку';
                break;
        }

        if ($data['PDV']=='yes') $return[] = 'ПДВ';
        if ($data['onLoad']=='yes') $return[] = 'при&nbsp;завантаженні';
        if ($data['onUnload']=='yes') $return[] = 'при&nbsp;розвантаженні';
        if ($data['onPrepay']=='yes')
        {
            $temp = 'передоплата';
            if ($data['advancedPayment']) $temp = $temp.'&nbsp;'.$data['advancedPayment'].'%';
            $return[] = $temp;
        }

        return implode('<br>',$return);

    }


    private function get_transport($data)
    {
        $return = [];
        $return[] = $data['car_type_name'];
        switch ($data['car_type']) {
            case 'vantazhivka':
                $return[] = 'вантажівка';
                break;
            case 'napiv prychip':
                $return[] = 'напівпричіп';
                break;
            case 'z prychipom':
                $return[] = 'з&nbsp;причіпом';
                break;
        }

        $return[] = $data['car_sizeX'].'x'.$data['car_sizeY'].'x'.$data['car_sizeZ'];
        $return[] = $data['car_liftingCapacity'].'т'.'/'.$data['car_volume'].'м3';
        $car_number = ($data['car_number'])?$data['car_number']:'<i>не&nbsp;вказано</i>';
        $return[] = 'номер&nbsp;машини: '.$car_number;

        return implode('<br>',$return);

    }

    private function get_avtor($r,$cms_lang)
    {
        $user = '<a target="_blank" class="link_name" href="/cms/'.$cms_lang.'/user/edit/'.$r['avtor_id'].'/">'.$r['avtor_name'].'</a>';
        $company = '<span class="label label-primary"><a target="_blank" href="/cms/'.$cms_lang.'/company/edit/'.$r['avtor_company_id'].'/">'.$r['avtor_company_name'].'</a></span>';
        return $user.'<br>'.$company;
    }


    private function get_user_requests($req_id,$cms_lang)
    {
        $res = DB::select(
            $this->table_user_to_req.'.status',
            $this->table_user_to_req.'.created',
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table_user_to_req.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'user_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'company_name'),
            array($this->table_user.'.ID','user_id'),
            array($this->table_company.'.ID','company_id')
        )
            ->from($this->table_user_to_req)

            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_user_to_req.'.userID')
            ->join($this->table_company,'left')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
            ->where($this->table_user_to_req.'.transportID','=',$req_id)
            ->order_by($this->table_user_to_req.'.created','desc')
            ->execute()
            ->as_array();
        if (empty($res)) return '';
        $table = '<table class="display table table-bordered table-striped" style="text-align: center"><tr><td>Імя</td><td>Компанія</td><td>Статус</td><td>Дата</td></tr>';
        foreach($res as $r)
        {
            $status = '<span class="status_pending">В процесі</span>';
            switch ($r['status']) {
                case 'accepted':
                    $status = '<span class="status_accepted">Прийнято</span>';
                    break;
                case 'rejected':
                    $status = '<span class="status_rejected">Відмовлено</span>';
                    break;
            }
            $table.='<tr>
            <td><a style="padding-top:5px;display:block" href="/cms/'.$cms_lang.'/user/edit/'.$r['user_id'].'/">'.addslashes($r['user_name']).'</a></td>
            <td><span class="label label-primary"><a href="/cms/'.$cms_lang.'/company/edit/'.$r['company_id'].'/">'.addslashes($r['company_name']).'</a></span></td>
            <td><span style="padding-top:5px;display:block">'.$status.'</span></td>
            <td><span style="padding-top:5px;display:block">'.$r['created'].'</span></td>
            </tr>';
        }

        $table.='</table>';

        return $table;
    }

    private function get_req_status($req_id,$cms_lang)
    {
        $res = DB::select(
            $this->table_user_to_req.'.status',
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table_user_to_req.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'user_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'company_name'),
            array($this->table_user.'.ID','user_id'),
            array($this->table_company.'.ID','company_id')
        )
            ->from($this->table_user_to_req)
            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_user_to_req.'.userID')
            ->join($this->table_company,'left')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
            ->where($this->table_user_to_req.'.status','=','accepted')
            ->where($this->table_user_to_req.'.transportID','=',$req_id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($res)) return '<span class="status_pending">Заявки в процесі</span>';

        $user = '<a class="link_name" href="/cms/'.$cms_lang.'/user/edit/'.$res[0]['user_id'].'/">'.$res[0]['user_name'].'</a>';
        $company = '<span class="label label-primary">'.'<a href="/cms/'.$cms_lang.'/company/edit/'.$res[0]['company_id'].'/">'.$res[0]['company_name'].'</a></span>';
        return '<span class="status_approved">Заявка прийнята</span><br>'.$user.'<br>'.$company;

    }

    public function countAll()
    {

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table);

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }


    public function countFiltered($filter)
    {
        $m_geo = new Model_Cms_Geo();

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)

            ->join($this->table_user,'left')
            ->on($this->table_user.'.ID','=',$this->table.'.userID')

            ->join($this->table_price,'left')
            ->on($this->table_price.'.ID','=',$this->table.'.priceID')

            ->join($this->table_cur,'left')
            ->on($this->table_cur.'.ID','=',$this->table_price.'.currencyID')

            ->join($this->table_user_car,'left')
            ->on($this->table_user_car.'.ID','=',$this->table.'.userCarID')

            ->join($this->table_user_car_type,'left')
            ->on($this->table_user_car_type.'.ID','=',$this->table_user_car.'.carTypeID');


        if (isset($filter['related_ids']))
        {
            foreach($filter['related_ids'] as $rel_key => $rel_val)
            {
                $res = $res->where(
                    DB::expr(
                        '(select ID from '.$this->table_geo_rel.' where requestID = '.$this->table.'.ID and type = \''.$rel_key.'\' and geoID in ('.implode(',',$rel_val).') limit 1)'
                    ),
                    '!=',
                    NULL
                );
            }

            $res = $res->where($this->table.'.dateTo','>=',$filter['date_from']);
            $res = $res->where($this->table.'.dateFrom','<=',$filter['date_from']);
            $res = $res->where($this->table.'.status','!=','outdated');
            $res = $res->where($this->table.'.status','!=','completed');

        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.created','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.created','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['dateFrom1']))
        {
            $res = $res->where($this->table.'.dateFrom','>=',$filter['dateFrom1']);
        }

        if (isset($filter['dateFrom2']))
        {
            $res = $res->where($this->table.'.dateFrom','<=',$filter['dateFrom2'].' 23:59:59');
        }

        if (isset($filter['dateTo1']))
        {
            $res = $res->where($this->table.'.dateTo','>=',$filter['dateTo1']);
        }

        if (isset($filter['dateTo2']))
        {
            $res = $res->where($this->table.'.dateTo','<=',$filter['dateTo2'].' 23:59:59');
        }

        if (isset($filter['status']))
        {
            $res = $res->where($this->table.'.status','=',$filter['status']);
        }

        if (isset($filter['userID']))
        {
            $res = $res->where($this->table.'.userID','=',$filter['userID']);
        }

        if (isset($filter['userCompanyID']))
        {
            $res = $res->where($this->table_user.'.companyID','=',$filter['userCompanyID']);
        }

        if (isset($filter['carTypeID']))
        {
            $res = $res->where($this->table_user_car.'.carTypeID','=',$filter['carTypeID']);
        }

        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                if (isset($filter['radius_from']))
                {
                    $exp_ids = explode(',',$places_from[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_from'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius_from))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();



                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius_from = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius_from = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','from')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius_from).')')),
                        '!=',
                        '0'
                    );

                }
                else
                {
                    foreach($places_from as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','from')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                '0'
                            );
                        }
                    }
                }

            }

        }

        if (isset($filter['places_to']))
        {
            $places_to = $filter['places_to'];

            if (is_array($places_to))
            {
                if (isset($filter['radius_to']))
                {
                    $exp_ids = explode(',',$places_to[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_to'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius_to))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();
                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius_to = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius_to = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','to')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius_to).')')),
                        '!=',
                        '0'
                    );
                }
                else
                {
                    foreach($places_to as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select(DB::expr('count('.$this->table_geo_rel.'.ID) as c'))
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','to')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                '0'
                            );
                        }
                    }
                }


            }
        }

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }

}