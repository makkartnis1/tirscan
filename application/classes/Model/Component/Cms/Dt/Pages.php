<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Pages extends Model {

    public $table = 'Frontend_Static_Pages';

    public function get($post)
    {

        $select_arr = array(
            '*',
        );


        $res = DB::select_array($select_arr)
            ->from($this->table)->group_by('uri');



        $res = $res
            ->order_by('ID','desc')
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            return $res;
        }
        return array();
    }


    public function countAll()
    {

        $res = DB::select(DB::expr('count(distinct uri) as c'))
            ->from($this->table)->group_by('uri');

            $res =$res->execute()
            ->as_array();

        if (empty($res)) return 0;

        return $res[0]['c'];
    }


    public function countFiltered()
    {
        $res = DB::select(DB::expr('count(distinct uri) as c'))
            ->from($this->table)->group_by('uri');

        $res = $res->execute()->as_array();

        if (empty($res)) return 0;


        return $res[0]['c'];
    }



}