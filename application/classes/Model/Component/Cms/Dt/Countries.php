<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Countries extends Model {

    public $table_geo = 'Frontend_Geo';

    public function get($post)
    {
        $countries = [
            'Іспанія',
            'Україна',
            'Росія',
            'Польща',
            'Білорусь',
            'Німеччина',
            'Угорщина',
            'Румунія',
            'Молдова',
            'Італія'
        ];

        $result = [];

        $i=0;
        foreach($countries as $c)
        {
            $i++;
            //транспорт
            $transports_from = [];
            $transports_from['closed'] = rand(100,10000);
            $transports_from['process'] = rand(0,100);
            $transports_from['all'] = $transports_from['closed'] + $transports_from['process'] + rand(10,1000);

            $transports_to = [];
            $transports_to['closed'] = rand(100,10000);
            $transports_to['process'] = rand(0,100);
            $transports_to['all'] = $transports_to['closed'] + $transports_to['process'] + rand(10,1000);

            //вантажі
            $loads_from = [];
            $loads_from['closed'] = rand(100,10000);
            $loads_from['process'] = rand(0,100);
            $loads_from['all'] = $loads_from['closed'] + $loads_from['process'] + rand(10,1000);

            $loads_to = [];
            $loads_to['closed'] = rand(100,10000);
            $loads_to['process'] = rand(0,100);
            $loads_to['all'] = $loads_to['closed'] + $loads_to['process'] + rand(10,1000);



            $result[$i] = [
                'name'=>$c,
                'transports_from'=>$transports_from,
                'transports_to'=>$transports_to,
                'loads_from'=>$loads_from,
                'loads_to'=>$loads_to,
                'company_count'=>rand(10,100)
            ];

            $for_sort[$i] = $transports_from+$transports_to+$loads_from+$loads_to;

        }

        arsort($for_sort);
        $new_result = [];
        foreach($for_sort as $f_k => $f_v)
            $new_result[] = $result[$f_k];

        $arr_help_1 = [
            'transports_from'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'transports_to'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'loads_from'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'loads_to'=>[
                'closed'=>'success',
                'process'=>'primary',
                'all'=>'muted'
            ],
            'company_count'=>'primary'
        ];

        $x = 0;
        foreach($new_result as &$n)
        {

            $x++;
            foreach($arr_help_1 as $key => $val)
            {
                if (is_array($val))
                {
                    foreach($val as $k => $v)
                    {
                        if ($n[$key][$k] > 0)
                        {
                            $n[$key][$k] = '<a href="#" class="badge badge-'.$v.'">'.$n[$key][$k].'</a>';
                        }
                        else
                        {
                            $n[$key][$k] = '<span class="no_data">---</span>';
                        }
                    }
                }
                else
                {
                    if ($n[$key] > 0)
                    {
                        $n[$key] = '<a href="#" class="badge badge-'.$val.'">'.$n[$key].'</a>';
                    }
                    else
                    {
                        $n[$key] = '<span class="no_data">---</span>';
                    }
                }
            }


        }

        return $new_result;
    }


    public function countAll()
    {
        return 10;
    }


    public function countFiltered()
    {
        return 10;
    }

}