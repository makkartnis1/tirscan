<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Complaints extends Model {

    public $t_complaints = 'Frontend_Complaints';
    public $t_company_locales = 'Frontend_Company_Locales';
    public $t_user_locales = 'Frontend_User_Locales';


    public function get($post,$get)
    {
        $offset = $post['start'];
        $limit = $post['length'];

        $select_arr = array(
            $this->t_complaints.'.*',
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_complaints.'.userID ORDER BY localeID LIMIT 1)'),'userName'),
            array(DB::expr('(SELECT name FROM '.$this->t_company_locales.' WHERE complainCompanyID = '.$this->t_complaints.'.complainCompanyID limit 1)'),'companyName')
        );


        $res = DB::select_array($select_arr)
            ->from($this->t_complaints);


        if (isset($get['companyID']))
            $res = $res->where($this->t_complaints.'.complainCompanyID','=',$get['companyID']);

        if (isset($get['userID']))
            $res = $res->where($this->t_complaints.'.userID','=',$get['userID']);

        if (isset($get['text']))
            $res = $res->where($this->t_complaints.'.message','LIKE',DB::expr("'%".$get['text']."%'"));

        $res = $res
            ->order_by($this->t_complaints.'.ID','DESC')
            ->offset($offset)
            ->limit($limit)
            ->execute()->as_array();

        if (!empty($res))
        {
            $res = $this->add_short_text($res,'message');

            foreach($res as $r)
                $this->setReaded($r['ID']);

            return $res;
        }
        return array();

    }


    private function add_short_text(array $res, $key)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r[$key]);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_'.$key] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_'.$key] = $r[$key];
        }
        return $res;
    }


    private function setReaded($id)
    {
        DB::update($this->t_complaints)
                    ->set(array(
                        'status'=>'readed'
                    ))
                    ->where('ID','=',$id)
                    ->execute();
    }


    public function countAll($get)
    {
        $res = DB::select(DB::expr('count('.$this->t_complaints.'.ID) as c'))
            ->from($this->t_complaints);

        if (isset($get['companyID']))
            $res = $res->where($this->t_complaints.'.complainCompanyID','=',$get['companyID']);

        if (isset($get['userID']))
            $res = $res->where($this->t_complaints.'.userID','=',$get['userID']);

        if (isset($get['text']))
            $res = $res->where($this->t_complaints.'.message','LIKE',DB::expr("'%".$get['text']."%'"));

        $res = $res->execute()->as_array();
        return $res[0]['c'];
    }


}