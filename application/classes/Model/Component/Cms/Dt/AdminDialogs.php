<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_AdminDialogs extends Model {

    public $t_d = 'Frontend_Dialogs';
    public $t_d_u = 'Frontend_Dialogs_Unread';
    public $t_m = 'Frontend_Messages';
    public $t_m_u = 'Frontend_Messages_Unread';
    public $t_admins = 'x_admins';
    public $t_users = 'Frontend_Users';
    public $t_user_locales = 'Frontend_User_Locales';

    public function get($post)
    {
        $offset = $post['start'];
        $limit = $post['length'];

        $select_arr = array(
            $this->t_d.'.id',
            $this->t_d.'.theme',
            $this->t_d.'.fromUserID',
            $this->t_d.'.toUserID',
            $this->t_d.'.fromAdminID',
            $this->t_d.'.toAdminID',
            array($this->t_d_u.'.toAdminID','unreadAdmin'),
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_d.'.fromUserID ORDER BY localeID LIMIT 1)'),'fromUserName'),
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_d.'.toUserID ORDER BY localeID LIMIT 1)'),'toUserName'),
            array(DB::expr('(SELECT name FROM '.$this->t_admins.' WHERE id = '.$this->t_d.'.fromAdminID limit 1)'),'fromAdminName'),
            array(DB::expr('(SELECT name FROM '.$this->t_admins.' WHERE id = '.$this->t_d.'.toAdminID limit 1)'),'toAdminName'),
            array(DB::expr('(SELECT COUNT(id) as c FROM '.$this->t_m_u.' WHERE dialogID = '.$this->t_d.'.id AND toAdminID != \'NULL\')'),'countUnrMess')
        );


        $res = DB::select_array($select_arr)
            ->from($this->t_d)

            ->join($this->t_d_u,'left')
            ->on($this->t_d_u.'.dialogID','=',$this->t_d.'.id');



        $res = $res
            ->and_where_open()
                ->or_where($this->t_d.'.fromAdminID','!=',NULL)
                ->or_where($this->t_d.'.toAdminID','!=',NULL)
            ->and_where_close()
            ->order_by($this->t_d.'.updated','DESC')
            ->offset($offset)
            ->limit($limit)
            ->execute()->as_array();

        if (!empty($res))
        {
            foreach($res as &$r)
            {
                $info = $this->addAvtor($r);
                $r['adminName'] = $info['adminName'];
                $r['userName'] = $info['userName'];
                $r['adminMessageID'] = $info['adminMessageID'];
                $r['userMessageID'] = $info['userMessageID'];
                $r['message'] = $info['message'];
                $r['created'] = $info['created'];
                $r['unread'] = $info['unread'];
            }

            $res = $this->add_short_text($res,'message');

            return $res;
        }
        return array();

    }


    private function add_short_text(array $res, $key)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r[$key]);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_'.$key] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_'.$key] = $r[$key];
        }
        return $res;
    }


    public function countAll()
    {

        $res = DB::select(DB::expr('count('.$this->t_d.'.id) as c'))
            ->from($this->t_d);

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }


    public function countFiltered()
    {

        $res = DB::select(DB::expr('count('.$this->t_d.'.id) as c'))
            ->from($this->t_d);

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

    public function addAvtor($row)
    {
        $res = DB::select(
            array(DB::expr('(SELECT name FROM '.$this->t_admins.' WHERE id = '.$this->t_m.'.adminMessageID limit 1)'),'adminName'),
            array(DB::expr('(SELECT name FROM '.$this->t_user_locales.' WHERE userID = '.$this->t_m.'.userMessageID ORDER BY localeID LIMIT 1)'),'userName'),
            $this->t_m.'.message',
            $this->t_m.'.created',
            $this->t_m.'.adminMessageID',
            $this->t_m.'.userMessageID',
            array(DB::expr('(SELECT toAdminID FROM '.$this->t_m_u.' WHERE messageID = '.$this->t_m.'.ID AND '.$this->t_m_u.'.toAdminID != \'NULL\')'),'unread')
        )
            ->from($this->t_m)
            ->where($this->t_m.'.dialogID','=',$row['id'])
            ->order_by($this->t_m.'.created','desc')
            ->limit(1)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0];
        return null;
    }
}