<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_SimpleComments extends Model {

    public $table = 'Frontend_Comments';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_company_locales = 'Frontend_Company_Locales';

    public function get($post,$get)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }

        $select_arr = array(
            $this->table.'.*',
            array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table.'.userID order by localeID limit 1)'),'user_name'),
            array(DB::expr('(select name from '.$this->table_company_locales.' where companyID = '.$this->table.'.companyID order by localeID limit 1)'),'company_name'),
        );


        $res = DB::select_array($select_arr)
            ->from($this->table);

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['company_id']))
        {
            $res = $res->where($this->table.'.companyID','=',$get['company_id']);
        }

        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];

        if ($order_col == 'company_name')
        {
            $order_col = DB::expr('(select name from '.$this->table_company_locales.' where companyID = '.$this->table.'.companyID order by localeID limit 1)');
        }elseif($order_col == 'user_name')
        {
            $order_col = DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table.'.userID order by localeID limit 1)');
        }
        else
        {
            $order_col = $this->table.'.'.$order_col;
        }


        $type_order = $post['order'][0]['dir'];

        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            $res = $this->add_short_text($res);
            return $res;
        }
        return array();
    }


    public function countAll($get)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table);

            if (isset($get['user_id']))
            {
                $res = $res->where($this->table.'.userID','=',$get['user_id']);
            }

            if (isset($get['company_id']))
            {
                $res = $res->where($this->table.'.companyID','=',$get['company_id']);
            }

            $res =$res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$get)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }


        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table);

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['company_id']))
        {
            $res = $res->where($this->table.'.companyID','=',$get['company_id']);
        }

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }


    private function add_short_text($res)
    {
        foreach($res as &$r)
        {
            $exp = explode(' ',$r['text']);
            $k=0;
            $short_text_arr = array();
            foreach($exp as $e)
            {
                $k++;
                if ($k > 10)
                {
                    break;
                }
                $short_text_arr[] = $e;
            }
            $r['short_text'] = implode(' ',$short_text_arr).'...';
            if ($k <= 10) $r['short_text'] = $r['text'];
        }
        return $res;
    }


}