<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Notification extends Model {

    public function get($post,$lng_id)
    {
//        $search = array(
//            'column'=>'',
//            'val'=>''
//        );
//        foreach($post['columns'] as $v)
//        {
//            if ($v['searchable']==true)
//            {
//                if ($v['search']['value']!='')
//                {
//                    $search['column'] = $v['data'];
//                    $search['val'] = $v['search']['value'];
//                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
//                }
//            }
//        }

        $res = DB::select(
            'x_notification_templates.*',
            'x_notification_template_locales.text',
            'x_notification_template_locales.title'
        )
            ->from('x_notification_templates')
            ->join('x_notification_template_locales','left')->on('x_notification_template_locales.notification_template_id','=','x_notification_templates.id')
            ->where('x_notification_template_locales.lng_id','=',$lng_id);

//        if ($search['column'])
//        {
//            if (isset($search['type']))
//            {
//                switch ($search['type']) {
//                    case '=':
//                        $res = $res->where($search['column'],'=',$search['val']);
//                        break;
//                    case 'date_range':
//                        $ex = explode(' - ',$search['val']);
//                        if (count($ex) > 1)
//                        {
//                            $date_from = trim($ex[0]);
//                            $date_to = trim($ex[1]);
//                            $res = $res->where($search['column'],'>=',$date_from);
//                            $res = $res->where($search['column'],'<=',$date_to);
//                        }
//                        else
//                        {
//                            $date = trim($search['val']);
//                            $res = $res->where($search['column'],'like',DB::expr("'%".$date."%'"));
//                        }
//                    break;
//                    case 'recursiv':
//                        $m_rubs = new Model_Cms_Plugin_Rub();
//                        $ids = $m_rubs->get_child_ids_by_id($search['val']);
//                        $res = $res->where($search['column'],'in',DB::expr('('.implode(',',$ids).')'));
//                        break;
//                }
//            }
//            else
//            {
//                if (in_array($search['column'],$langs_arr))
//                {
//                    $res = $res->where($arr_for_search[$search['column']],'like',DB::expr("'%".$search['val']."%'"));
//                }
//                elseif($search['column']=='user_name')
//                {
//                    $res = $res->and_where_open();
//                    $res = $res->or_where('x_users.name','like',DB::expr("'%".$search['val']."%'"));
//                    $res = $res->or_where('x_admins.name','like',DB::expr("'%".$search['val']."%'"));
//                    $res = $res->and_where_close();
//                }
//                else
//                {
//                    $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));
//                }
//            }
//        }

        $res = $res
//            ->order_by($post['columns'][$index_order]['data'],$type_order)
            ->order_by('x_notification_templates.id','desc')
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            return $res;
        }
        return array();
    }


    public function countAll($pr_key)
    {
        $res = DB::select(DB::expr('count('.$pr_key.') as c'))
            ->from('x_notification_templates')
            ->execute()
            ->as_array();
        return $res[0]['c'];
    }

//    public function countFiltered($post,$pr_key)
//    {
//
//    }
}