<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_TransportWidget extends Model {

    public $table = 'Frontend_User_Cars';
    public $table_types = 'Frontend_User_Car_Types';
    public $table_user = 'Frontend_Users';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_company = 'Frontend_Companies';

    public function get($post,$langs,$get)
    {
        $search = $post['search']['value'];

        $select_arr = array(
            $this->table.'.*',
            array($this->table_user_locales.'.name','user_name'),
            array($this->table_types.'.name','car_type'),
            array($this->table_company_locales.'.name','company')
        );

        foreach($langs as $l)
        {
            $select_arr[] = array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table.'.userID and localeID = '.$l['ID'].' limit 1)'),'name_'.$l['uri']);
        }

        $res = DB::select_array($select_arr)
            ->from($this->table)
            ->join($this->table_types,'inner')->on($this->table.'.carTypeID','=',$this->table_types.'.ID')
            ->join($this->table_user,'inner')->on($this->table.'.userID','=',$this->table_user.'.ID')
            ->join($this->table_company,'inner')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
            ->join($this->table_user_locales,'left')->on($this->table_user_locales.'.userID','=',$this->table_user.'.ID')
                                                    ->on($this->table_user_locales.'.localeID','=',$this->table_user.'.primaryLocaleID')
            ->join($this->table_company_locales,'left')->on($this->table_company_locales.'.companyID','=',$this->table_user.'.companyID')
                                                       ->on($this->table_company_locales.'.localeID','=',$this->table_company.'.primaryLocaleID');

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['company_id']))
        {
            $res = $res->where($this->table_company.'.ID','=',$get['company_id']);
        }



        if ($search)
        {
            $res = $res->and_where_open();
                $res = $res->or_where($this->table.'.ID','like',DB::expr("'%".$search."%'"));
                $res = $res->or_where($this->table.'.number','like',DB::expr("'%".$search."%'"));
            $res = $res->and_where_close();
        }


        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];


        $order_col = $this->table.'.'.$order_col;
        $type_order = $post['order'][0]['dir'];


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            return $res;
        }
        return array();
    }


    public function countAll($get)
    {
        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)
            ->join($this->table_user,'inner')->on($this->table.'.userID','=',$this->table_user.'.ID');
            if (isset($get['user_id']))
            {
                $res = $res->where($this->table.'.userID','=',$get['user_id']);
            }
            if (isset($get['company_id']))
            {
                $res = $res->where($this->table_user.'.companyID','=',$get['company_id']);
            }
            $res = $res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$langs,$get)
    {
        $search = $post['search']['value'];

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)
            ->join($this->table_user,'inner')->on($this->table.'.userID','=',$this->table_user.'.ID')
            ->join($this->table_company,'inner')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
            ->join($this->table_company_locales,'left')->on($this->table_company_locales.'.companyID','=',$this->table_user.'.companyID')
            ->on($this->table_company_locales.'.localeID','=',$this->table_company.'.primaryLocaleID');

        if (isset($get['user_id']))
        {
            $res = $res->where($this->table.'.userID','=',$get['user_id']);
        }

        if (isset($get['company_id']))
        {
            $res = $res->where($this->table_company.'.ID','=',$get['company_id']);
        }

        if ($search)
        {
            $res = $res->and_where_open();
            $res = $res->or_where($this->table.'.ID','like',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.number','like',DB::expr("'%".$search."%'"));
            $res = $res->and_where_close();
        }

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

}