<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_User extends Model {

    public $table = 'Frontend_Users';
    public $table_locales = 'Frontend_User_Locales';
    public $table_company = 'Frontend_Companies';
    public $table_company_owner = 'Frontend_User_to_Company_Ownership';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_company_types = 'Frontend_Company_Types';
    public $table_phone_codes = 'Frontend_Phonecodes';
    public $t_news = 'Frontend_News';
    public $t_user_cars = 'Frontend_User_Cars';
    public $t_comments = 'Frontend_Comments';
    public $t_tenders = 'Frontend_Tenders';
    public $t_cargo_req = 'Frontend_Cargo';
    public $t_transport_req = 'Frontend_Transports';

    public function get($post,$filter)
    {
        $select_arr = array(
            $this->table.'.*',
            array($this->table_company_locales.'.name','company'),
            array($this->table_company_types.'.name','company_type'),
            array('phone_table.code','code_phone'),
            array('phone_stat_table.code','code_phone_stat'),
            array(DB::expr('(select name from '.$this->table_locales.' where userID = '.$this->table.'.ID order by localeID limit 1)'),'name'),
            array(DB::expr('(select ID from '.$this->table_company_owner.' where userID = '.$this->table.'.ID limit 1)'),'owner'),
            array(DB::expr("(select ID from ".$this->table_locales." where name LIKE '%олег%' limit 1)"),'test')
        );


        $res = DB::select_array($select_arr)
            ->from($this->table)
            ->join($this->table_company,'inner')->on($this->table_company.'.ID','=',$this->table.'.companyID')
            ->join($this->table_company_locales,'left')->on($this->table_company_locales.'.companyID','=',$this->table_company.'.ID')
            ->on($this->table_company_locales.'.localeID','=',$this->table_company.'.primaryLocaleID')

            ->join($this->table_company_types,'left')->on($this->table_company_types.'.ID','=',$this->table_company.'.typeID')

            ->join(array($this->table_phone_codes, 'phone_table'),'left')
            ->on('phone_table.ID','=',$this->table.'.phoneCodeID')

            ->join(array($this->table_phone_codes, 'phone_stat_table'),'left')
            ->on('phone_stat_table.ID','=',$this->table.'.phoneStationaryCodeID');


        if (isset($filter['company_id']))
        {
            $res = $res->where($this->table.'.companyID','=',$filter['company_id']);
        }


        if (isset($filter['ID']))
        {
            $res = $res->where($this->table.'.ID','LIKE',DB::expr("'%".$filter['ID']."%'"));
        }

        if (isset($filter['name']))
        {
            $res = $res->where(DB::expr("(select ID from ".$this->table_locales." where name LIKE '%".$filter['name']."%' and userID = ".$this->table.".ID limit 1)"),'!=',NULL);
        }

        if (isset($filter['email']))
        {
            $res = $res->where($this->table.'.email','LIKE',DB::expr("'%".$filter['email']."%'"));
        }

        if (isset($filter['phone']))
        {
            $res = $res->where($this->table.'.phone','LIKE',DB::expr("'%".$filter['phone']."%'"));
        }

        if (isset($filter['emailApproved']))
        {
            $res = $res->where($this->table.'.emailApproved','=',$filter['emailApproved']);
        }

        if (isset($filter['group']))
        {
            if ($filter['group'] == 'manager')
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_company_owner.' where userID = '.$this->table.'.ID limit 1)'),'=',NULL);
            }

            if ($filter['group'] == 'owner')
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_company_owner.' where userID = '.$this->table.'.ID limit 1)'),'!=',NULL);
            }
        }


        if (isset($filter['companyType']))
        {
            $res = $res->where($this->table_company.'.typeID','=',$filter['companyType']);
        }

        if (isset($filter['registered']))
        {
            $res_filter_reqistered = $this->get_time_vars($filter['registered']);
            $res = $res->where($this->table.'.registered',$res_filter_reqistered['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_reqistered['time'].')'));
        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.registered','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.registered','<=',$filter['dateCreate2'].' 23:59:59');
        }

        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];


        $order_col = $this->table.'.'.$order_col;
        $type_order = $post['order'][0]['dir'];


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {

            foreach($res as &$r)
            {
                $r['phone'] = ($r['code_phone'] and $r['phone'])?$r['code_phone'].' '.$r['phone']:'';
                $r['phone2'] = ($r['code_phone_stat'] and $r['phoneStationary'])?$r['code_phone_stat'].' '.$r['phoneStationary']:'';
                $r['type'] = ($r['owner'])?'<span class="label label-primary">Власник</span>':'<span class="label label-primary">Менеджер</span>';
                $r['transport_req'] = $this->get_transport_req($r['ID']);
                $r['cargo_req'] = $this->get_cargo_req($r['ID']);
                $r['tenders'] = $this->get_tenders($r['ID']);
                $r['positive'] = $this->get_positive($r['ID']);
                $r['negative'] = $this->get_negative($r['ID']);
                $r['neutral'] = $this->get_neutral($r['ID']);
                $r['transports'] = $this->get_transports($r['ID']);
                $r['news'] = $this->get_news($r['ID']);
                $r['valuta'] = $this->get_valuta($r['ID']);
                $r['register_date'] = $this->get_date($r['registered']);
                $r['company'] = '<a class="link_name" target="_blank" href="/cms/uk/company/edit/'.$r['companyID'].'">'.$r['company'].'</a>';
                $r['company_type'] = '<i>'.$r['company_type'].'</i>';
            }
            return $res;
        }
        return array();
    }


    public function countAll($filter)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->table);

            if (isset($filter['company_id']))
            {
                $res = $res->where($this->table.'.companyID','=',$filter['company_id']);
            }

            $res =$res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($filter)
    {

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)
            ->join($this->table_company,'inner')->on($this->table_company.'.ID','=',$this->table.'.companyID')
            ->join($this->table_company_locales,'left')->on($this->table_company_locales.'.companyID','=',$this->table_company.'.ID')
            ->on($this->table_company_locales.'.localeID','=',$this->table_company.'.primaryLocaleID')

            ->join(array($this->table_phone_codes, 'phone_table'),'left')
            ->on('phone_table.ID','=',$this->table.'.phoneCodeID')

            ->join(array($this->table_phone_codes, 'phone_stat_table'),'left')
            ->on('phone_stat_table.ID','=',$this->table.'.phoneStationaryCodeID');

        if (isset($filter['company_id']))
        {
            $res = $res->where($this->table.'.companyID','=',$filter['company_id']);
        }

        if (isset($filter['ID']))
        {
            $res = $res->where($this->table.'.ID','LIKE',DB::expr("'%".$filter['ID']."%'"));
        }

        if (isset($filter['name']))
        {
            $res = $res->where(DB::expr("(select ID from ".$this->table_locales." where name LIKE '%".$filter['name']."%' and userID = ".$this->table.".ID limit 1)"),'!=',NULL);
        }

        if (isset($filter['email']))
        {
            $res = $res->where($this->table.'.email','LIKE',DB::expr("'%".$filter['email']."%'"));
        }

        if (isset($filter['phone']))
        {
            $res = $res->where($this->table.'.phone','LIKE',DB::expr("'%".$filter['phone']."%'"));
        }

        if (isset($filter['emailApproved']))
        {
            $res = $res->where($this->table.'.emailApproved','=',$filter['emailApproved']);
        }

        if (isset($filter['group']))
        {
            if ($filter['group'] == 'manager')
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_company_owner.' where userID = '.$this->table.'.ID limit 1)'),'=',NULL);
            }

            if ($filter['group'] == 'owner')
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_company_owner.' where userID = '.$this->table.'.ID limit 1)'),'!=',NULL);
            }
        }


        if (isset($filter['companyType']))
        {
            $res = $res->where($this->table_company.'.typeID','=',$filter['companyType']);
        }

        if (isset($filter['registered']))
        {
            $res_filter_reqistered = $this->get_time_vars($filter['registered']);
            $res = $res->where($this->table.'.registered',$res_filter_reqistered['sign'],DB::expr('DATE_ADD(NOW(), INTERVAL '.$res_filter_reqistered['time'].')'));
        }

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.registered','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.registered','<=',$filter['dateCreate2'].' 23:59:59');
        }


        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

    private function get_time_vars($key)
    {
        $temp = '';
        $sign = '<=';
        switch ($key) {
            case '<1m':
                $temp = '-1 MONTH';
                $sign = '>';
                break;
            case '>1m':
                $temp = '-1 MONTH';
                break;
            case '>2m':
                $temp = '-2 MONTH';
                break;
            case '>3m':
                $temp = '-3 MONTH';
                break;
            case '>6m':
                $temp = '-6 MONTH';
                break;
            case '>1y':
                $temp = '-1 YEAR';
                break;
            case '>2y':
                $temp = '-2 YEAR';
                break;
            case '>3y':
                $temp = '-3 YEAR';
                break;
            case '>5y':
                $temp = '-5 YEAR';
                break;
            case '>10y':
                $temp = '-10 YEAR';
                break;
            case '>15y':
                $temp = '-15 YEAR';
                break;
            case '>20y':
                $temp = '-20 YEAR';
                break;
        }

        return ['sign'=>$sign,'time'=>$temp];
    }


    private function get_date($date)
    {
        $month = 60 * 60 * 24 * 29;
        $year = $month * 12;

        $now = time();
        $date = strtotime($date);
        $diff = $now - $date;

        if ($diff < $month)
        {
            return 'менше місяця';
        }
        elseif($diff >= $month and $diff < $year)
        {
            $count_month = floor($diff / $month);
            $time = 'місяців';
            if ($count_month == 1) $time = 'місяць';
            if ($count_month == 2 or $count_month == 3 or $count_month == 4) $time = 'місяця';
            return $count_month.' '.$time;
        }
        elseif($diff > $year)
        {
            $count_years = floor($diff / $year);
            $time_year = 'років';

            $str_years = (string) $count_years;
            $last_letter = $str_years{strlen($str_years)-1};
            if ($last_letter == '1') $time_year = 'рік';
            if ($last_letter == '2' or $last_letter == '3' or $last_letter == '4') $time_year = 'роки';

            $res = $count_years.' '.$time_year;

            $month_val = $diff - ($year * $count_years);
            $time_month = '';

            if ($month_val > $month)
            {
                $count_month = floor($month_val / $month);
                $time_month = 'місяців';
                if ($count_month == 1) $time_month = 'місяць';
                if ($count_month == 2 or $count_month == 3 or $count_month == 4) $time_month = 'місяця';
                $res .= ' і '.$count_month.' '.$time_month;

            }

            return $res;
        }
    }


    private function get_valuta($user_id)
    {
        return '<i>не куплено</i>';
    }

    private function get_news($user_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_news.'.ID) as c'))
            ->from($this->t_news)
            ->where($this->t_news.'.userID','=',$user_id)
            ->execute()
            ->as_array();

        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/user/news?user_id='.$user_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_transports($user_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_user_cars.'.ID) as c'))
            ->from($this->t_user_cars)
            ->where($this->t_user_cars.'.userID','=',$user_id)
            ->execute()
            ->as_array();

        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/transport?user_id='.$user_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_neutral($user_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_comments)
            ->where('userID','=',$user_id)
            ->where('rating','=','neutral')
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?user_id='.$user_id.'&rating=neutral" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_negative($user_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_comments)
            ->where('userID','=',$user_id)
            ->where('rating','=','negative')
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?user_id='.$user_id.'&rating=negative" class="badge badge-important">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_positive($user_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_comments)
            ->where('userID','=',$user_id)
            ->where('rating','=','positive')
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/comments?user_id='.$user_id.'&rating=positive" class="badge badge-success">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_tenders($user_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_tenders.'.ID) as c'))
            ->from($this->t_tenders)
            ->where($this->t_tenders.'.userID','=',$user_id)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/tenders?user_id='.$user_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_cargo_req($user_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_cargo_req.'.ID) as c'))
            ->from($this->t_cargo_req)
            ->where($this->t_cargo_req.'.userID','=',$user_id)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/transportRequests?user_id='.$user_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }

    private function get_transport_req($user_id)
    {
        $res = DB::select(DB::expr('count('.$this->t_transport_req.'.ID) as c'))
            ->from($this->t_transport_req)
            ->where($this->t_transport_req.'.userID','=',$user_id)
            ->execute()
            ->as_array();
        if (!empty($res))
        {
            $count = $res[0]['c'];

            if ($count)
            {
                return '<a href="/cms/uk/cargoRequests?user_id='.$user_id.'" class="badge badge-primary">'.$count.'</a>';
            }
            else
            {
                return '<a href="#" class="badge">'.$count.'</a>';
            }
        }
        return null;
    }


}