<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_UserWidget extends Model {

    public $table = 'Frontend_Users';
    public $table_locales = 'Frontend_User_Locales';
    public $table_company = 'Frontend_Companies';
    public $table_company_types = 'Frontend_Company_Types';
    public $table_phone_codes = 'Frontend_Phonecodes';
    public $table_owner = 'Frontend_User_to_Company_Ownership';
    public $table_cars = 'Frontend_User_Cars';

    public function get($post,$langs,$company_id = null,$isset_car = null,$user_type = null)
    {

        $search = $post['search']['value'];

        $select_arr = array(
            $this->table.'.*',
            array('phone_table.code','code_phone'),
            array('phone_stat_table.code','code_phone_stat')
        );

        foreach($langs as $l)
        {
            $select_arr[] = array(DB::expr('(select name from '.$this->table_locales.' where userID = '.$this->table.'.ID and localeID = '.$l['ID'].' limit 1)'),'name_'.$l['uri']);
        }

        $res = DB::select_array($select_arr)
            ->from($this->table)
            ->join($this->table_company,'inner')
                ->on($this->table_company.'.ID','=',$this->table.'.companyID')
            ->join($this->table_owner,'left')
                ->on($this->table_owner.'.userID','=',$this->table.'.ID');


            $res = $res->join(array($this->table_phone_codes, 'phone_table'),'left')
                ->on('phone_table.ID','=',$this->table.'.phoneCodeID')

            ->join(array($this->table_phone_codes, 'phone_stat_table'),'left')
                ->on('phone_stat_table.ID','=',$this->table.'.phoneStationaryCodeID');


            if ($company_id)
            {
                $res = $res->where($this->table_owner.'.userID','IS',NULL);
                $res = $res->where($this->table.'.companyID','=',$company_id);
            }

            if ($isset_car)
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_cars.' where userID = '.$this->table.'.ID limit 1)'),'!=',NULL);
            }


        switch ($user_type) {
            case 'transport':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,2)'));
                break;
            case 'cargo':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,3)'));
                break;
        }

        if ($search)
        {

                $res = $res->and_where_open();
                    $res = $res->or_where($this->table.'.ID','LIKE',DB::expr("'%".$search."%'"));
                    $res = $res->or_where(DB::expr('(select name from '.$this->table_locales.' where userID = '.$this->table.'.ID limit 1)'),'like',DB::expr("'%".$search."%'"));
                    $res = $res->or_where($this->table.'.email','like',DB::expr("'%".$search."%'"));
                $res = $res->and_where_close();
        }


        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];


        $order_col = $this->table.'.'.$order_col;
        $type_order = $post['order'][0]['dir'];


        switch ($post['columns'][$index_order]['data']) {
        case 'group':
            $order_col = $this->table_company.'.typeID';
            break;
        case 'company':
            $order_col = $this->table_company.'.nameInternational';
            break;
        }

        foreach($langs as $l)
        {
            if ($post['columns'][$index_order]['data'] = 'name_'.$l['uri'])
            {
                $order_col = DB::expr('(select name from '.$this->table_locales.' where userID = '.$this->table.'.ID and localeID = '.$l['ID'].' limit 1)');
            }
        }


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {

            foreach($res as &$r)
            {
                $r['avatar'] = $this->get_avatar($r['ID']);
                $r['phone'] = $r['code_phone'].$r['phone'];
                $r['phone2'] = $r['code_phone_stat'].$r['phoneStationary'];
            }
            return $res;
        }
        return array();
    }


    public function countAll($company_id=null,$isset_car=null,$user_type=null)
    {
        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table);
             $res = $res    ->  join($this->table_owner,'left')
                            ->    on($this->table_owner.'.userID','=',$this->table.'.ID');

             $res = $res    ->  join($this->table_company,'inner')
                            ->    on($this->table.'.companyID','=',$this->table_company.'.ID');


            if ($company_id)
            {
                $res = $res->where($this->table_owner.'.userID','IS',NULL);
                $res =$res->where($this->table.'.companyID','=',$company_id);
            }
            if ($isset_car)
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_cars.' where userID = '.$this->table.'.ID limit 1)'),'!=',NULL);
            }

            switch ($user_type) {
            case 'transport':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,2)'));
                break;
            case 'cargo':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,3)'));
                break;
            }

            $res = $res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$langs,$company_id=null,$isset_car=null,$user_type=null)
    {
        $search = $post['search']['value'];

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)
            ->join($this->table_company,'inner')
                ->on($this->table_company.'.ID','=',$this->table.'.companyID')
            ->join($this->table_owner,'left')
                ->on($this->table_owner.'.userID','=',$this->table.'.ID');


        if ($company_id)
            {
                $res = $res->where($this->table_owner.'.userID','IS',NULL);
                $res = $res->where($this->table.'.companyID','=',$company_id);
            }
            if ($isset_car)
            {
                $res = $res->where(DB::expr('(select ID from '.$this->table_cars.' where userID = '.$this->table.'.ID limit 1)'),'!=',NULL);
            }


        switch ($user_type) {
            case 'transport':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,2)'));
                break;
            case 'cargo':
                $res = $res->where($this->table_company.'.typeID','IN',DB::expr('(1,3)'));
                break;
        }

        if ($search)
        {
            $res = $res->and_where_open();
            $res = $res->or_where($this->table.'.ID','LIKE',DB::expr("'%".$search."%'"));
            $res = $res->or_where(DB::expr('(select name from '.$this->table_locales.' where userID = '.$this->table.'.ID limit 1)'),'like',DB::expr("'%".$search."%'"));
            $res = $res->or_where($this->table.'.email','like',DB::expr("'%".$search."%'"));
            $res = $res->and_where_close();
        }

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

    private function get_avatar($id)
    {
        $file=$_SERVER['DOCUMENT_ROOT'].'/uploads/images/users/'.$id.'.jpg';
        if (file_exists($file))
        {
            $img = '/uploads/images/users/'.$id.'.jpg';
        }
        else
        {
            $img = '/public/cms/img/user.png';
        }

        return $img;
    }


}