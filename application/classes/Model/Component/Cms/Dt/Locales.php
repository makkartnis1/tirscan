<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Locales extends Model {

    public $table = 'i18n';
    public $table_locales = 'i18n_locale';

    public function get($post,$langs)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }

        $select_arr = array(
            'id',
            'const'
        );

        foreach($langs as $l)
        {
            $select_arr[] = array(DB::expr('(select translate from '.$this->table_locales.' where i18nID = '.$this->table.'.id and localeID = '.$l['ID'].' limit 1)'),'translate_'.$l['uri']);
        }

        $res = DB::select_array($select_arr)
            ->from($this->table);

        if (!empty($search['columns']))
        {
            foreach($search['columns'] as $s_key => $s_val)
            {

                foreach($langs as $l)
                {
                    if ($s_val == 'translate_'.$l['uri'])
                    {
                        $res = $res->where(DB::expr('(select translate from '.$this->table_locales.' where i18nID = '.$this->table.'.id and localeID = '.$l['ID'].' limit 1)'),'like',DB::expr("'%".$search['vals'][$s_key]."%'"));
                    }
                }

                if ($s_val == 'const')
                {
                    $res = $res->where('const','like',DB::expr("'%".$search['vals'][$s_key]."%'"));
                }

            }
        }

        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];

        $type_order = $post['order'][0]['dir'];


        foreach($langs as $l)
        {
            if ($post['columns'][$index_order]['data'] = 'translate_'.$l['uri'])
            {
                $order_col = DB::expr('(select translate from '.$this->table_locales.' where i18nID = '.$this->table.'.id and localeID = '.$l['ID'].' limit 1)');
            }
        }


        $res = $res
            ->order_by('id','desc')
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            return $res;
        }
        return array();
    }


    public function countAll()
    {
        $res = DB::select(DB::expr('count(id) as c'))
            ->from($this->table);
            $res =$res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($post,$langs)
    {
        $search = array(
            'columns'=>array(),
            'vals'=>array()
        );
        foreach($post['columns'] as $v)
        {
            if ($v['searchable']==true)
            {
                if ($v['search']['value']!='')
                {
                    $search['columns'][] = $v['data'];
                    $search['vals'][] = $v['search']['value'];
                }
            }
        }


        $res = DB::select(DB::expr('count(id) as c'))
            ->from($this->table);

        if (!empty($search['columns']))
        {
            foreach($search['columns'] as $s_key => $s_val)
            {

                foreach($langs as $l)
                {
                    if ($s_val == 'translate_'.$l['uri'])
                    {
                        $res = $res->where(DB::expr('(select translate from '.$this->table_locales.' where i18nID = '.$this->table.'.id and localeID = '.$l['ID'].' limit 1)'),'like',DB::expr("'%".$search['vals'][$s_key]."%'"));
                    }
                }

                if ($s_val == 'const')
                {
                    $res = $res->where('const','like',DB::expr("'%".$search['vals'][$s_key]."%'"));
                }

            }
        }

        $res = $res->execute()->as_array();

        return $res[0]['c'];
    }

}