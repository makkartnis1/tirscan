<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_Tenders extends Model {

    public $table = 'Frontend_Tenders';
    public $table_geo_rel = 'Frontend_Tenders_to_Geo_Relation';
    public $table_user = 'Frontend_Users';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_user_to_req = 'Frontend_User_to_Tender_Requests';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_company = 'Frontend_Companies';
    public $table_cur = 'Frontend_Currencies';
    public $table_user_car_type = 'Frontend_User_Car_Types';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_geo = 'Frontend_Geo';

    public function get($post,$cms_lang,$filter)
    {
        $m_geo = new Model_Cms_Geo();

        $select_arr = array(
            $this->table.'.*',
            array($this->table.'.userID','avtor_id'),
            array($this->table_user.'.companyID','avtor_company_id'),
            array($this->table_cur.'.code','code_cur'),
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'avtor_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'avtor_company_name'),
            array($this->table_user_car_type.'.name','car_type_name')
        );


        $res = DB::select_array($select_arr)
            ->from($this->table)

            ->join($this->table_user,'left')
            ->on($this->table_user.'.ID','=',$this->table.'.userID')

            ->join($this->table_cur,'left')
            ->on($this->table_cur.'.ID','=',$this->table.'.currencyID')

            ->join($this->table_user_car_type,'left')
            ->on($this->table_user_car_type.'.ID','=',$this->table.'.transportTypeID');


        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.created','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.created','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['dateFrom1']))
        {
            $res = $res->where($this->table.'.dateFrom','>=',$filter['dateFrom1']);
        }

        if (isset($filter['dateFrom2']))
        {
            $res = $res->where($this->table.'.dateFrom','<=',$filter['dateFrom2'].' 23:59:59');
        }

        if (isset($filter['dateTo1']))
        {
            $res = $res->where($this->table.'.dateTo','>=',$filter['dateTo1']);
        }

        if (isset($filter['dateTo2']))
        {
            $res = $res->where($this->table.'.dateTo','<=',$filter['dateTo2'].' 23:59:59');
        }

        if (isset($filter['status']))
        {
            $res = $res->where($this->table.'.status','=',$filter['status']);
        }

        if (isset($filter['userID']))
        {
            $res = $res->where($this->table.'.userID','=',$filter['userID']);
        }

        if (isset($filter['userCompanyID']))
        {
            $res = $res->where($this->table_user.'.companyID','=',$filter['userCompanyID']);
        }

        if (isset($filter['carTypeID']))
        {
            $res = $res->where($this->table.'.transportTypeID','=',$filter['carTypeID']);
        }

        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                if (isset($filter['radius_from']))
                {
                    $exp_ids = explode(',',$places_from[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_from'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();



                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select($this->table_geo_rel.'.ID')
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','from')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius).')')),
                        '!=',
                        NULL
                    );

                }
                else
                {
                    foreach($places_from as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select($this->table_geo_rel.'.ID')
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','from')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                NULL
                            );
                        }
                    }
                }

            }

        }

        if (isset($filter['places_to']))
        {
            $places_to = $filter['places_to'];

            if (is_array($places_to))
            {
                if (isset($filter['radius_to']))
                {
                    $exp_ids = explode(',',$places_to[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_to'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();
                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select($this->table_geo_rel.'.ID')
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','to')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius).')')),
                        '!=',
                        NULL
                    );
                }
                else
                {
                    foreach($places_to as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select($this->table_geo_rel.'.ID')
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','to')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                NULL
                            );
                        }
                    }
                }


            }
        }


        $index_order = $post['order'][0]['column'];
        $order_col = $post['columns'][$index_order]['data'];

        $order_col = $this->table.'.ID';
        $type_order = 'DESC';


        $res = $res
            ->order_by($order_col,$type_order)
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();

        $m_tenders = new Model_Component_Cms_Tenders();

        if (!empty($res))
        {
            foreach($res as &$r)
            {
                $r = $this->filter($r);
                $r['from_places'] = $this->get_places($r['ID'],'from');
                $r['to_places'] = $this->get_places($r['ID'],'to');
                $r['price'] = $this->get_price($r);
                $r['transport'] = $this->get_transport($r);
                $r['cargo'] = $this->get_cargo($r);
                $r['user'] = $this->get_avtor($r,$cms_lang);
                $r['status_work'] = $this->get_req_status($r['ID'],$cms_lang);
                $r['requests'] = $this->get_user_requests($r['ID'],$cms_lang,$r['price']);
                $r['count_requests'] = $this->get_count_requests($r['ID']);
                $m_tenders->set_tender_status_by_date($r['ID']);
            }
            return $res;
        }
        return array();

    }


    private function get_count_requests($tender_id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->table_user_to_req)
                    ->where('tenderID','=',$tender_id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['c'];
                return 0;
    }


    private function filter($r)
    {
        $r['volume'] = (float) $r['volume'];
        $r['weight'] = (float) $r['weight'];
        $r['sizeX'] = (float) $r['sizeX'];
        $r['sizeY'] = (float) $r['sizeY'];
        $r['sizeZ'] = (float) $r['sizeZ'];
        if (!$r['info']) $r['info'] = '<i>відсутній</i>';
        return $r;
    }

    public function countAll()
    {
        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table);

        $res = $res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    public function countFiltered($filter)
    {
        $m_geo = new Model_Cms_Geo();

        $res = DB::select(DB::expr('count('.$this->table.'.ID) as c'))
            ->from($this->table)

            ->join($this->table_user,'left')
            ->on($this->table_user.'.ID','=',$this->table.'.userID');

        if (isset($filter['dateCreate1']))
        {
            $res = $res->where($this->table.'.created','>=',$filter['dateCreate1']);
        }

        if (isset($filter['dateCreate2']))
        {
            $res = $res->where($this->table.'.created','<=',$filter['dateCreate2'].' 23:59:59');
        }

        if (isset($filter['dateFrom1']))
        {
            $res = $res->where($this->table.'.dateFrom','>=',$filter['dateFrom1']);
        }

        if (isset($filter['dateFrom2']))
        {
            $res = $res->where($this->table.'.dateFrom','<=',$filter['dateFrom2'].' 23:59:59');
        }

        if (isset($filter['dateTo1']))
        {
            $res = $res->where($this->table.'.dateTo','>=',$filter['dateTo1']);
        }

        if (isset($filter['dateTo2']))
        {
            $res = $res->where($this->table.'.dateTo','<=',$filter['dateTo2'].' 23:59:59');
        }

        if (isset($filter['status']))
        {
            $res = $res->where($this->table.'.status','=',$filter['status']);
        }

        if (isset($filter['userID']))
        {
            $res = $res->where($this->table.'.userID','=',$filter['userID']);
        }

        if (isset($filter['userCompanyID']))
        {
            $res = $res->where($this->table_user.'.companyID','=',$filter['userCompanyID']);
        }

        if (isset($filter['carTypeID']))
        {
            $res = $res->where($this->table.'.transportTypeID','=',$filter['carTypeID']);
        }

        if (isset($filter['places_from']))
        {
            $places_from = $filter['places_from'];

            if (is_array($places_from))
            {
                if (isset($filter['radius_from']))
                {
                    $exp_ids = explode(',',$places_from[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_from'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();



                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select($this->table_geo_rel.'.ID')
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','from')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius).')')),
                        '!=',
                        NULL
                    );

                }
                else
                {
                    foreach($places_from as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select($this->table_geo_rel.'.ID')
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','from')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                NULL
                            );
                        }
                    }
                }

            }

        }

        if (isset($filter['places_to']))
        {
            $places_to = $filter['places_to'];

            if (is_array($places_to))
            {
                if (isset($filter['radius_to']))
                {
                    $exp_ids = explode(',',$places_to[0]);
                    $coord_id = $m_geo->getPlaceByIds($exp_ids);
                    $coords = $m_geo->getCoordById($coord_id);

                    $mylon = $coords['lng'];
                    $mylat = $coords['lat'];


                    $dist = $filter['radius_to'];

                    $lon1 = $mylon-$dist/round(cos(deg2rad($mylat))*111.0,6);
                    $lon2 = $mylon+$dist/round(cos(deg2rad($mylat))*111.0,6);

                    $lat1 = $mylat-($dist/111.0);
                    $lat2 = $mylat+($dist/111.0);


                    if (!isset($geo_ids_radius))
                    {
                        $geo_ids = DB::select('ID')
                            ->from($this->table_geo)
                            ->where('type','=','city')
                            ->where('lng','>=',$lon1)
                            ->where('lng','<=',$lon2)
                            ->where('lat','>=',$lat1)
                            ->where('lat','<=',$lat2)
                            ->execute()
                            ->as_array();
                        if (!empty($geo_ids))
                        {
                            $geo_ids_radius = Helper_Array::get_array_by_key($geo_ids,'ID');
                        }
                        else
                        {
                            $geo_ids_radius = array(0);
                        }
                    }

                    $res = $res->where(
                        DB::select($this->table_geo_rel.'.ID')
                            ->from($this->table_geo_rel)
                            ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                            ->where($this->table_geo_rel.'.type','=','to')
                            ->where($this->table_geo_rel.'.geoID','IN',DB::expr('('.implode(',',$geo_ids_radius).')')),
                        '!=',
                        NULL
                    );
                }
                else
                {
                    foreach($places_to as $p)
                    {
                        $exp_ids = explode(',',$p);
                        foreach($exp_ids as $geo_id)
                        {
                            $res = $res->where(
                                DB::select($this->table_geo_rel.'.ID')
                                    ->from($this->table_geo_rel)
                                    ->where($this->table_geo_rel.'.requestID','=',DB::expr($this->table.'.ID'))
                                    ->where($this->table_geo_rel.'.type','=','to')
                                    ->where($this->table_geo_rel.'.geoID','=',$geo_id),
                                '!=',
                                NULL
                            );
                        }
                    }
                }


            }
        }


        $res = $res->execute()
            ->as_array();
        return $res[0]['c'];
    }


    private function get_places($req_id,$type)
    {
        $res = DB::select('geoID','groupNumber')
            ->from($this->table_geo_rel)
            ->where('requestID','=',$req_id)
            ->where('type','=',$type)
            ->execute()
            ->as_array();


        $new_res = [];
        foreach($res as $r)
        {
            if (!isset($new_res[$r['groupNumber']])) $new_res[$r['groupNumber']] = [];
            $new_res[$r['groupNumber']][] = $r['geoID'];
        }

        $places = [];

        foreach($new_res as $n_r)
        {
            $geo = DB::select(
                array(DB::expr('(select '.$this->table_geo_locales.'.fullAddress from '.$this->table_geo_locales.' where '.$this->table_geo_locales.'.geoID = '.$this->table_geo.'.ID order by '.$this->table_geo_locales.'.localeID limit 1)'),'name')
            )
                ->from($this->table_geo)
                ->where($this->table_geo.'.ID','in',DB::expr('('.implode(',',$n_r).')'))
                ->order_by($this->table_geo.'.level','desc')
                ->limit(1)
                ->execute()
                ->as_array();

            $places[] = '<span class="label label-default">'.$geo[0]['name'].'</span>';

        }

        return implode('<br>',$places);
    }

    private function get_price($data)
    {

        return $data['price'].'&nbsp;'.$data['code_cur'];

        if ($data['specifiedPrice'] == 'no') return 'не&nbsp;вказано';
        $return = [];

        if ($data['currency_name'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['currency_name'];

        }elseif($data['customPriceType'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['customPriceType'];
        }


        switch ($data['paymentType']) {
            case 'b/g':
                $return[] = 'б/г';
                break;
            case 'gotivka':
                $return[] = 'готівка';
                break;
            case 'combined':
                $return[] = 'комбінована';
                break;
            case 'card':
                $return[] = 'на&nbsp;картку';
                break;
        }

        if ($data['PDV']=='yes') $return[] = 'ПДВ';
        if ($data['onLoad']=='yes') $return[] = 'при&nbsp;завантаженні';
        if ($data['onUnload']=='yes') $return[] = 'при&nbsp;розвантаженні';
        if ($data['onPrepay']=='yes')
        {
            $temp = 'передоплата';
            if ($data['advancedPayment']) $temp = $temp.'&nbsp;'.$data['advancedPayment'].'%';
            $return[] = $temp;
        }

        return implode('<br>',$return);

    }


    private function get_transport($data)
    {
        if (!$data['car_type_name']) $data['car_type_name'] = 'не&nbsp;вказано';
        if ($data['carCount'] == 0) {$data['carCount'] = '-';}else{$data['carCount'] .= 'шт';}
        if ($data['sizeX'] == 0) {$data['sizeX'] = '-';}else{$data['sizeX'].='м';}
        if ($data['sizeY'] == 0) {$data['sizeY'] = '-';}else{$data['sizeY'].='м';}
        if ($data['sizeZ'] == 0) {$data['sizeZ'] = '-';}else{$data['sizeZ'].='м';}
        $return = [];
        $return[] = $data['car_type_name'];
        $return[] = 'кількість:&nbsp;'.$data['carCount'];
        $return[] = 'габарити:&nbsp;'.$data['sizeX'].'x'.$data['sizeY'].'x'.$data['sizeZ'];
        return implode('<br>',$return);

    }

    private function get_cargo($data)
    {
        if (!$data['cargoType']) $data['cargoType'] = '-';
        if ($data['weight'] == 0) {$data['weight'] = '-';}else{$data['weight'].='т';}
        if ($data['volume'] == 0) {$data['volume'] = '-';}else{$data['volume'].='м<sup>3</sup>';}
        $return = [];
        $return[] = 'тип:&nbsp;'.$data['cargoType'];
        $return[] = 'вага:&nbsp;'.$data['weight'];
        $return[] = 'об\'єм:&nbsp;'.$data['volume'];
        return implode('<br>',$return);
    }

    private function get_avtor($r,$cms_lang)
    {
        $user = '<a target="_blank" class="link_name" href="/cms/'.$cms_lang.'/user/edit/'.$r['avtor_id'].'/">'.$r['avtor_name'].'</a>';
        $company = '<span class="label label-primary"><a target="_blank" href="/cms/'.$cms_lang.'/company/edit/'.$r['avtor_company_id'].'/">'.$r['avtor_company_name'].'</a></span>';
        return $user.'<br>'.$company;
    }


    private function get_user_requests($req_id,$cms_lang,$price)
    {
        $res = DB::select(
            $this->table_user_to_req.'.created',
            $this->table_user_to_req.'.requestNumber',
            $this->table_user_to_req.'.customPrice',
            $this->table_cur.'.code',
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table_user_to_req.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'user_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'company_name'),
            array($this->table_user.'.ID','user_id'),
            array($this->table_company.'.ID','company_id')
        )
            ->from($this->table_user_to_req)

            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_user_to_req.'.userID')
            ->join($this->table_company,'left')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
            ->join($this->table,'left')->on($this->table.'.ID','=',$this->table_user_to_req.'.tenderID')
            ->join($this->table_cur,'left')->on($this->table.'.currencyID','=',$this->table_cur.'.ID')
            ->where($this->table_user_to_req.'.tenderID','=',$req_id)
            ->order_by($this->table_user_to_req.'.created','desc')
            ->execute()
            ->as_array();
        if (empty($res)) return '';
        $table = '<table class="display table table-bordered table-striped" style="text-align: center"><tr><td>Компанія</td><td>Дата</td><td>Ставка</td></tr>';
        foreach($res as $r)
        {

            $suma = $r['customPrice'].' '.$r['code'];

            $table.='<tr>
            <td><span class="label label-primary"><a href="/cms/'.$cms_lang.'/company/edit/'.$r['company_id'].'/">'.addslashes($r['company_name']).'</a></span></td>
            <td><span style="padding-top:5px;display:block">'.$r['created'].'</span></td>
            <td>'.$suma.'</td>
            </tr>';
        }

        $table.='</table>';

        return $table;
    }

    private function get_req_status($req_id,$cms_lang)
    {
        $res = DB::select(
//            $this->table_user_to_req.'.status',
            array(DB::expr('(select '.$this->table_user_locales.'.name from '.$this->table_user_locales.' where '.$this->table_user_locales.'.userID = '.$this->table_user_to_req.'.userID order by '.$this->table_user_locales.'.localeID limit 1)'),'user_name'),
            array(DB::expr('(select '.$this->table_company_locales.'.name from '.$this->table_company_locales.' where '.$this->table_company_locales.'.companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID limit 1)'),'company_name'),
            array($this->table_user.'.ID','user_id'),
            array($this->table_company.'.ID','company_id')
        )
            ->from($this->table_user_to_req)
            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_user_to_req.'.userID')
            ->join($this->table_company,'left')->on($this->table_company.'.ID','=',$this->table_user.'.companyID')
//            ->where($this->table_user_to_req.'.status','=','accepted')
            ->where($this->table_user_to_req.'.tenderID','=',$req_id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($res)) return '<span class="status_pending">Заявки в процесі</span>';

        $user = '<a class="link_name" href="/cms/'.$cms_lang.'/user/edit/'.$res[0]['user_id'].'/">'.$res[0]['user_name'].'</a>';
        $company = '<span class="label label-primary">'.'<a href="/cms/'.$cms_lang.'/company/edit/'.$res[0]['company_id'].'/">'.$res[0]['company_name'].'</a></span>';
        return '<span class="status_approved">Заявка прийнята</span><br>'.$user.'<br>'.$company;

    }

}