<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Dt_AdminGroup extends Model {

    public function get($post,$lng_id)
    {
//        $search = array(
//            'column'=>'',
//            'val'=>''
//        );
//        foreach($post['columns'] as $v)
//        {
//            if ($v['searchable']==true)
//            {
//                if ($v['search']['value']!='')
//                {
//                    $search['column'] = $v['data'];
//                    $search['val'] = $v['search']['value'];
//                    if (isset($v['search_type'])) $search['type'] = $v['search_type'];
//                }
//            }
//        }

        $res = DB::select()
            ->from('x_admin_groups');
//        if ($search['column'])
//        {
//            if (isset($search['type']))
//            {
//                switch ($search['type']) {
//                    case '=':
//                        $res = $res->where($search['column'],'=',$search['val']);
//                        break;
//                    case 'date_range':
//                        $ex = explode(' - ',$search['val']);
//                        if (count($ex) > 1)
//                        {
//                            $date_from = trim($ex[0]);
//                            $date_to = trim($ex[1]);
//                            $res = $res->where($search['column'],'>=',$date_from);
//                            $res = $res->where($search['column'],'<=',$date_to);
//                        }
//                        else
//                        {
//                            $date = trim($search['val']);
//                            $res = $res->where($search['column'],'like',DB::expr("'%".$date."%'"));
//                        }
//                    break;
//                    case 'recursiv':
//                        $m_rubs = new Model_Cms_Plugin_Rub();
//                        $ids = $m_rubs->get_child_ids_by_id($search['val']);
//                        $res = $res->where($search['column'],'in',DB::expr('('.implode(',',$ids).')'));
//                        break;
//                }
//            }
//            else
//            {
//                if (in_array($search['column'],$langs_arr))
//                {
//                    $res = $res->where($arr_for_search[$search['column']],'like',DB::expr("'%".$search['val']."%'"));
//                }
//                elseif($search['column']=='user_name')
//                {
//                    $res = $res->and_where_open();
//                    $res = $res->or_where('x_users.name','like',DB::expr("'%".$search['val']."%'"));
//                    $res = $res->or_where('x_admins.name','like',DB::expr("'%".$search['val']."%'"));
//                    $res = $res->and_where_close();
//                }
//                else
//                {
//                    $res = $res->where($search['column'],'like',DB::expr("'%".$search['val']."%'"));
//                }
//            }
//        }

        $res = $res
//            ->order_by($post['columns'][$index_order]['data'],$type_order)
            ->order_by('id','desc')
            ->offset($post['start'])
            ->limit($post['length'])
            ->execute()->as_array();


        if (!empty($res))
        {
            foreach($res as &$r)
            {
                $loc = $this->add_group($r['id'],$lng_id);
                $r['title'] = $loc['title'];
                $r['preview'] = $loc['preview'];
            }
            return $res;
        }
        return array();
    }

    public function countAll($pr_key)
    {
        $res = DB::select(DB::expr('count('.$pr_key.') as c'))
            ->from('x_admin_groups')
            ->execute()
            ->as_array();
        return $res[0]['c'];
    }

    private function add_group($group_id,$lng_id)
    {
        $m_group = new Model_Component_Cms_AdminGroup();
        return $m_group->get_localization($group_id,$lng_id);
    }

//    public function countFiltered($post,$pr_key)
//    {
//
//    }

//    function filter(&$val)
//    {
//        $val = htmlspecialchars($val,ENT_QUOTES);
//    }
}