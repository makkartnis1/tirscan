<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_CompanyGroup extends Model {

    public function get_all()
    {
        $res = DB::select()
                    ->from('Frontend_Company_Types')
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res;
                return null;
    }

//    public function get($id)
//    {
//        $info = DB::select()
//            ->from('x_user_groups')
//            ->where('id','=',$id)
//            ->execute()
//            ->as_array();
//
//        if (empty($info)) return null;
//
//        $info = $info[0];
//
//        $locales = DB::select()
//            ->from('x_user_group_locales')
//            ->where('user_group_id','=',$id)
//            ->execute()
//            ->as_array();
//
//        $locales = Helper_Array::change_keys($locales,'lng_id');
//
//        return array('info'=>$info,'locales'=>$locales);
//    }
//
//    public function create($data,$langs)
//    {
//        $user_group = DB::insert('x_user_groups',array('id'))
//            ->values(array(''))
//            ->execute();
//        $id = $user_group[0];
//        $this->update_locales($id,$data,$langs);
//        return $id;
//    }
//
//    public function update($id,$data,$langs)
//    {
//        $this->update_locales($id,$data,$langs);
//    }
//
//    public function delete($id)
//    {
//        try {
//
//            DB::delete('x_user_groups')
//                        ->where('id','=',$id)
//                        ->execute();
//
//        } catch (Exception $e) {
//
//        }
//    }
//
//    private function update_locales($id,$data,$langs)
//    {
//        foreach($langs as $lang)
//        {
//            DB::update('x_user_group_locales')
//                ->set(array(
//                    'title'=>$data['title_'.$lang['pref']]
//                ))
//                ->where('lng_id','=',$lang['id'])
//                ->where('user_group_id','=',$id)
//                ->execute();
//        }
//    }

//    public function get_title($id,$lng_id)
//    {
//        $res = DB::select('title')
//            ->from('x_user_group_locales')
//            ->where('user_group_id','=',$id)
//            ->where('lng_id','=',$lng_id)
//            ->execute()
//            ->as_array();
//        if (!empty($res)) return $res[0]['title'];
//        return null;
//    }

}
