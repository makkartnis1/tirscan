<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Locales extends Model {

    public $table = 'i18n_locale';

    public function edit($id,$lng_id,$translate)
    {

        $isset = DB::select('i18nID')
            ->from($this->table)
            ->where('i18nID','=',$id)
            ->where('localeID','=',$lng_id)
            ->execute()
            ->as_array();

        if (empty($isset))
        {
            DB::insert($this->table,array('i18nID','localeID','translate'))
                ->values(array($id,$lng_id,$translate))
                ->execute();
        }
        else
        {
            DB::update($this->table)
                ->set(array(
                    'translate'=>$translate
                ))
                ->where('i18nID','=',$id)
                ->where('localeID','=',$lng_id)
                ->execute();
        }

    }
}