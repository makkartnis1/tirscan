<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Currencies extends Model {

    public $table = 'Frontend_Currencies';

    public function getAll()
    {
        $res = DB::select()
                    ->from($this->table)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res;
                return null;
    }

    public function get_code($id)
    {
        $res = DB::select('code')
                    ->from($this->table)
                    ->where('ID','=',$id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['code'];
                return null;
    }

}