<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_SimpleNews extends Model {

    public $table = 'Frontend_News';
    public $table_locales = 'Frontend_News_Locales';


    public function valid($data,$langs)
    {
        $errors = array();

        $errors['empty_title'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['title'][$l['ID']]))
            {
                unset($errors['empty_title']);
            }
        }

        $errors['empty_content'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['content'][$l['ID']]))
            {
                unset($errors['empty_content']);
            }
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data,$langs)
    {
        $data = $this->filter($data);

        $arr = array(
            'url'=>$data['url']
        );

        DB::update($this->table)
            ->set($arr)
            ->where('ID','=',$id)
            ->execute();

        $this->update_locales($id,$data,$langs);
    }


    public function add($data,$langs)
    {
        $data = $this->filter($data);

        $title = null;
        foreach($data['title'] as $val)
        {
            $title = $val;
            if ($title) break;
        }

        $url = Helper_PrefGenerator::generate_col('Frontend_News','url','ID',$title);


        $res = DB::insert($this->table,array(
            'url'
        ))
            ->values(array(
                $url,
            ))
            ->execute();

        $id = $res[0];

        $this->insert_locales($id,$data,$langs);

        return $id;
    }




    private function update_locales($id,$data,$langs)
    {
        foreach($langs as $lang)
        {

            $res = DB::select('ID')
                        ->from($this->table_locales)
                        ->where('localeID','=',$lang['ID'])
                        ->where('newsID','=',$id)
                        ->execute()
                        ->as_array();

            if (empty($res))
            {
                DB::insert($this->table_locales,array('title','preview','content','localeID','newsID'))
                            ->values(array(
                                $data['title'][$lang['ID']],
                                $data['preview'][$lang['ID']],
                                $data['content'][$lang['ID']],
                                $lang['ID'],
                                $id
                            ))
                            ->execute();
            }
            else
            {

            }

            DB::update($this->table_locales)
                        ->set(array(
                            'title'=>$data['title'][$lang['ID']],
                            'preview'=>$data['preview'][$lang['ID']],
                            'content'=>$data['content'][$lang['ID']],
                        ))
                        ->where('localeID','=',$lang['ID'])
                        ->where('newsID','=',$id)
                        ->execute();
        }
    }

    private function insert_locales($id,$data,$langs)
    {
        foreach($langs as $lang)
        {

            DB::insert($this->table_locales,array(
                            'title',
                            'preview',
                            'content',
                            'localeID',
                            'newsID'
                        ))
                        ->values(array(
                            $data['title'][$lang['ID']],
                            $data['preview'][$lang['ID']],
                            $data['content'][$lang['ID']],
                            $lang['ID'],
                            $id
                        ))
                        ->execute();
        }
    }

    public function delete($id)
    {
        try {

            DB::delete($this->table)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table.'.*'
        )
            ->from($this->table)
            ->where($this->table.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        $locales = DB::select()
            ->from($this->table_locales)
            ->where('newsID','=',$id)
            ->execute()
            ->as_array();

        if (!empty($locales))
        {
            $locales = Helper_Array::change_keys($locales,'localeID');
        }
        else
        {
            $locales = [];
        }

        return array('info'=>$info,'locales'=>$locales);

    }

    private function filter($data)
    {


        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        return $data;
    }

}