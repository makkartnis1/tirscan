<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Pages extends Model {

    public $table = 'Frontend_Static_Pages';

    public function valid($data,$langs)
    {
        $errors = array();

        $errors['empty_title'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['title'][$l['ID']]))
            {
                unset($errors['empty_title']);
            }
        }

        $errors['empty_content'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['content'][$l['ID']]))
            {
                unset($errors['empty_content']);
            }
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($uri,$data,$langs)
    {
        $data = $this->filter($data);

        foreach($langs as $lang)
        {

            DB::update($this->table)
                ->set(array(
                    'title'=>$data['title'][$lang['ID']],
                    'content'=>$data['content'][$lang['ID']],
                    'keywords'=>$data['keywords'][$lang['ID']],
                    'description'=>$data['description'][$lang['ID']]
                ))
                ->where('localeID','=',$lang['ID'])
                ->where('uri','=',$uri)
                ->execute();
        }

    }


    public function add($data,$langs)
    {
        $data = $this->filter($data);

        $title = null;
        foreach($data['title'] as $val)
        {
            $title = $val;
            if ($title) break;
        }

        $url = Helper_PrefGenerator::generate_col('Frontend_Static_Pages','uri','ID',$title);


        foreach($langs as $lang)
        {

            DB::insert($this->table,array(
                'title',
                'content',
                'keywords',
                'description',
                'localeID',
                'uri',
            ))
                ->values(array(
                    $data['title'][$lang['ID']],
                    $data['content'][$lang['ID']],
                    $data['keywords'][$lang['ID']],
                    $data['description'][$lang['ID']],
                    $lang['ID'],
                    $url
                ))
                ->execute();
        }

    }


    public function delete($uri)
    {
        try {

            DB::delete($this->table)
                ->where('uri','=',$uri)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($uri)
    {
        $info = DB::select(
            $this->table.'.*'
        )
            ->from($this->table)
            ->where($this->table.'.uri','=',$uri)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $new_info = array();
        foreach($info as $i)
        {
            $new_info[$i['localeID']] = $i;
        }

        return $new_info;

    }

    private function filter($data)
    {


        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        return $data;
    }

}