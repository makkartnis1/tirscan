<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Comments extends Model {

    public $t_comments = 'Frontend_Comments';

    public function delete($id)
    {
        try {

            DB::delete($this->t_comments)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }

}
