<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Transport extends Model {

    public $table = 'Frontend_User_Cars';


    public function valid($data)
    {
        $errors = array();

        if (!$data['userID']) $errors['userID'] = 1;

        if (Helper_Valid::is_empty($data['number'])) $errors['number'] = 1;
        if (Helper_Valid::is_empty($data['volume'])) $errors['volume'] = 1;
        if (Helper_Valid::is_empty($data['liftingCapacity'])) $errors['liftingCapacity'] = 1;
        if (Helper_Valid::is_empty($data['sizeX'])) $errors['sizeX'] = 1;
        if (Helper_Valid::is_empty($data['sizeY'])) $errors['sizeY'] = 1;
        if (Helper_Valid::is_empty($data['sizeZ'])) $errors['sizeZ'] = 1;

        if (!isset($errors['number']))
        {
            if (!Helper_Valid::number($data['number'])) $errors['number_val'] = 1;
        }

        if (!isset($errors['volume']))
        {
            if (!Helper_Valid::number($data['volume'])) $errors['volume_val'] = 1;
        }

        if (!isset($errors['liftingCapacity']))
        {
            if (!Helper_Valid::number($data['liftingCapacity'])) $errors['liftingCapacity_val'] = 1;
        }

        if (!isset($errors['sizeX']))
        {
            if (!Helper_Valid::number($data['sizeX'])) $errors['sizeX_val'] = 1;
        }

        if (!isset($errors['sizeY']))
        {
            if (!Helper_Valid::number($data['sizeY'])) $errors['sizeY_val'] = 1;
        }

        if (!isset($errors['sizeZ']))
        {
            if (!Helper_Valid::number($data['sizeZ'])) $errors['sizeZ_val'] = 1;
        }

        if (!$data['carTypeID']) $errors['carTypeID'] = 1;

        if (!$data['type']) $errors['type'] = 1;

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data)
    {
        $data = $this->filter($data);

        $arr = array(
            'userID'=>$data['userID'],
            'number'=>$data['number'],
            'info'=>$data['info'],
            'volume'=>$data['volume'],
            'sizeX'=>$data['sizeX'],
            'sizeY'=>$data['sizeY'],
            'sizeZ'=>$data['sizeZ'],
            'liftingCapacity'=>$data['liftingCapacity'],
            'carTypeID'=>$data['carTypeID'],
            'type'=>$data['type'],
        );

        DB::update($this->table)
            ->set($arr)
            ->where('ID','=',$id)
            ->execute();

    }


    public function add($data)
    {

        $data = $this->filter($data);

        $res = DB::insert($this->table,array(
            'userID',
            'number',
            'info',
            'volume',
            'sizeX',
            'sizeY',
            'sizeZ',
            'liftingCapacity',
            'carTypeID',
            'type'
        ))
            ->values(array(
                $data['userID'],
                $data['number'],
                $data['info'],
                $data['volume'],
                $data['sizeX'],
                $data['sizeY'],
                $data['sizeZ'],
                $data['liftingCapacity'],
                $data['carTypeID'],
                $data['type']
            ))
            ->execute();

        $id = $res[0];

        return $id;
    }





    public function delete($id)
    {
        try {

            DB::delete($this->table)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($id)
    {
        $info = DB::select()
            ->from($this->table)
            ->where($this->table.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        $m_user = new Model_Component_Cms_User();
        $info['userName'] = $m_user->get_name($info['userID']);

        $dir = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/private/car/';
        $dir_gallery = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/public/car/';

        $docs = Helper_Dir::getFilesFromDir($dir.'docs/'.$id.'/');
        $numbers = Helper_Dir::getFilesFromDir($dir.'numbers/'.$id.'/');
        $gallery = Helper_Dir::getFilesFromDir($dir_gallery.'gallery/'.$id.'/');

        $files = array(
            'docs'=>$docs,
            'numbers'=>$numbers,
            'gallery'=>$gallery
        );


        return array('info'=>$info,'files'=>$files);

    }

    private function filter($data)
    {
        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        return $data;
    }


}