<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Tenders extends Model {

    public $table_tenders = 'Frontend_Tenders';
    public $table_tenders_geo = 'Frontend_Tenders_to_Geo_Relation';
    public $table_geo = 'Frontend_Geo';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_price = 'Frontend_Request_Prices';
    public $table_cur = 'Frontend_Currencies';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_car_types = 'Frontend_User_Car_Types';
    public $table_users = 'Frontend_Users';
    public $table_tenders_requests = 'Frontend_User_to_Tender_Requests';


    public function valid($data)
    {
        $errors = array();


        if (Helper_Valid::is_empty($data['dateFromLoad']))
            $errors['dateFromLoad'] = 'Вкажіть дату завантаження "з"';


        if (Helper_Valid::is_empty($data['dateToLoad']))
            $errors['dateToLoad'] = 'Вкажіть дату завантаження "до"';


        if (!array_key_exists('dateFromLoad',$errors) and !array_key_exists('dateToLoad',$errors))
        {
            if (!Helper_Valid::interval_date_standart($data['dateFromLoad'],$data['dateToLoad'],true))
            {
                $errors['dateFromLoad'] = 'Дата завантаження вказана не коректно';
            }
        }


        if (Helper_Valid::is_empty($data['dateFromTender']))
            $errors['dateFromTender'] = 'Вкажіть дату початку торгів';


        if (Helper_Valid::is_empty($data['dateToTender']))
            $errors['dateToTender'] = 'Вкажіть дату закінчення торгів';


        if (!array_key_exists('dateFromTender',$errors) and !array_key_exists('dateToTender',$errors))
        {
            if (!Helper_Valid::interval_date_standart($data['dateFromTender'].' '.$data['timeFromTender'],$data['dateToTender'].' '.$data['timeToTender']))
            {
                $errors['dateFromTender'] = 'Дата тендера вказана не коректно';
            }
        }

        $errors['currencyID'] = 'Не вказано валюту';
        if (Helper_Valid::number($data['currencyID']))
        {
            unset($errors['currencyID']);
        }

        $errors['price'] = 'Не вказано ціну';
        if (Helper_Valid::number($data['price']))
        {
            unset($errors['price']);
        }

        if (!$data['userID']) $errors['userID'] = 'Не вибрано користувача';

        if (!$data['places_from'][0]) $errors['places_from'] = 'Не вибрано місце завантаження';
        if (!$data['places_to'][0]) $errors['places_to'] = 'Не вибрано місце розвантаження';

        $errors['cargoType'] = 'Не вказано тип вантажу';
        if (!Helper_Valid::is_empty($data['cargoType']))
        {
            unset($errors['cargoType']);
        }

        $test_weight = (float) $data['weight'];
        $test_volume = (float) $data['volume'];

        if ($test_weight <= 0 and $test_volume <= 0)
        {
            $errors['volumeAndWeight'] = 'Не вказано вагу або об\'єм вантажу';
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data)
    {
        $data = $this->filter($data);

        $this->update_geo_relation($data['places_from'],$data['places_to'],$id);
        DB::update($this->table_tenders)
            ->set(array(
                'dateFromLoad'=>$data['dateFromLoad'],
                'dateToLoad'=>$data['dateToLoad'],
                'datetimeFromTender'=>$data['dateFromTender'].' '.$data['timeFromTender'],
                'datetimeToTender'=>$data['dateToTender'].' '.$data['timeToTender'],
                'info'=>$data['info'],
                'cargoType'=>$data['cargoType'],
                'transportTypeID'=>$data['transportTypeID'],
                'volume'=>$data['volume'],
                'weight'=>$data['weight'],
                'carCount'=>$data['carCount'],
                'sizeX'=>$data['sizeX'],
                'sizeY'=>$data['sizeY'],
                'sizeZ'=>$data['sizeZ'],
                'userID'=>$data['userID'],
                'docTIR'=>$data['docTIR'],
                'docCMR'=>$data['docCMR'],
                'docT1'=>$data['docT1'],
                'docSanPassport'=>$data['docSanPassport'],
                'docSanBook'=>$data['docSanBook'],
                'loadFromSide'=>$data['loadFromSide'],
                'loadFromTop'=>$data['loadFromTop'],
                'loadFromBehind'=>$data['loadFromBehind'],
                'loadTent'=>$data['loadTent'],
                'condPlomb'=>$data['condPlomb'],
                'condLoad'=>$data['condLoad'],
                'condBelts'=>$data['condBelts'],
                'condRemovableStands'=>$data['condRemovableStands'],
                'condHardSide'=>$data['condHardSide'],
                'condCollectableCargo'=>$data['condCollectableCargo'],
                'condTemperature'=>$data['condTemperature'],
                'condPalets'=>$data['condPalets'],
                'condADR'=>$data['condADR'],
                'price'=>$data['price'],
                'priceHide'=>$data['priceHide'],
                'currencyID'=>$data['currencyID'],
                'filterCompanyYears'=>$data['filterCompanyYears'],
                'filterCompany'=>$data['filterCompany'],
                'filterForwarder'=>$data['filterForwarder'],
                'filterPartners'=>$data['filterPartners']
            ))
            ->where('ID','=',$id)
            ->execute();


        $m_tenders = new Model_Component_Cms_Tenders();
        $m_tenders->set_tender_status_by_date($id);
    }


    public function add($data)
    {
        $data = $this->filter($data);

            $res = DB::insert($this->table_tenders,array(
                'dateFromLoad',
                'dateToLoad',
                'datetimeFromTender',
                'datetimeToTender',
                'info',
                'cargoType',
                'transportTypeID',
                'volume',
                'weight',
                'carCount',
                'sizeX',
                'sizeY',
                'sizeZ',
                'userID',
                'docTIR',
                'docCMR',
                'docT1',
                'docSanPassport',
                'docSanBook',
                'loadFromSide',
                'loadFromTop',
                'loadFromBehind',
                'loadTent',
                'condPlomb',
                'condLoad',
                'condBelts',
                'condRemovableStands',
                'condHardSide',
                'condCollectableCargo',
                'condTemperature',
                'condPalets',
                'condADR',
                'currencyID',
                'price',
                'priceHide',
                'filterCompanyYears',
                'filterCompany',
                'filterForwarder',
                'filterPartners'
            ))
                ->values(array(
                    $data['dateFromLoad'],
                    $data['dateToLoad'],
                    $data['dateFromTender'].' '.$data['timeFromTender'],
                    $data['dateToTender'].' '.$data['timeToTender'],
                    $data['info'],
                    $data['cargoType'],
                    $data['transportTypeID'],
                    $data['volume'],
                    $data['weight'],
                    $data['carCount'],
                    $data['sizeX'],
                    $data['sizeY'],
                    $data['sizeZ'],
                    $data['userID'],
                    $data['docTIR'],
                    $data['docCMR'],
                    $data['docT1'],
                    $data['docSanPassport'],
                    $data['docSanBook'],
                    $data['loadFromSide'],
                    $data['loadFromTop'],
                    $data['loadFromBehind'],
                    $data['loadTent'],
                    $data['condPlomb'],
                    $data['condLoad'],
                    $data['condBelts'],
                    $data['condRemovableStands'],
                    $data['condHardSide'],
                    $data['condCollectableCargo'],
                    $data['condTemperature'],
                    $data['condPalets'],
                    $data['condADR'],
                    $data['currencyID'],
                    $data['price'],
                    $data['priceHide'],
                    $data['filterCompanyYears'],
                    $data['filterCompany'],
                    $data['filterForwarder'],
                    $data['filterPartners']
                ))
                ->execute();

            $id = $res[0];
            $this->create_geo_relation($data['places_from'],$data['places_to'],$id);

            $m_tenders = new Model_Component_Cms_Tenders();
            $m_tenders->set_tender_status_by_date($id);

            return $id;

    }


    public function delete($id)
    {
        try {

            DB::delete($this->table_tenders)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table_tenders.'.*',
            array($this->table_car_types.'.name','car_type'),
            array($this->table_cur.'.code','cur_name'),
            array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table_tenders.'.userID order by '.$this->table_user_locales.'.localeID asc limit 1)'),'user_name')
        )
            ->from($this->table_tenders)
            ->join($this->table_cur,'left')->on($this->table_cur.'.ID','=',$this->table_tenders.'.currencyID')
            ->join($this->table_car_types,'left')->on($this->table_car_types.'.ID','=',$this->table_tenders.'.transportTypeID')
            ->where($this->table_tenders.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $m_geo = new Model_Cms_Geo();

        return array('info'=>$info[0],'geo_groups'=>$m_geo->getGeoByRequestID($id,$this->table_tenders_geo));
    }

    private function filter($data)
    {

        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }


        $data['volume'] = (float) $data['volume'];
        $data['weight'] = (float) $data['weight'];
        $data['carCount'] = (int) $data['carCount'];
        if ($data['carCount'] == 0) $data['carCount'] = NULL;
        $data['sizeX'] = (float) $data['sizeX'];
        if ($data['sizeX'] == 0) $data['sizeX'] = NULL;
        $data['sizeY'] = (float) $data['sizeY'];
        if ($data['sizeY'] == 0) $data['sizeY'] = NULL;
        $data['sizeZ'] = (float) $data['sizeZ'];
        if ($data['sizeZ'] == 0) $data['sizeZ'] = NULL;

        $data['docTIR'] = (isset($data['docTIR']))?'yes':'no';
        $data['docCMR'] = (isset($data['docCMR']))?'yes':'no';
        $data['docT1'] = (isset($data['docT1']))?'yes':'no';
        $data['docSanPassport'] = (isset($data['docSanPassport']))?'yes':'no';
        $data['docSanBook'] = (isset($data['docSanBook']))?'yes':'no';
        $data['loadFromSide'] = (isset($data['loadFromSide']))?'yes':'no';
        $data['loadFromTop'] = (isset($data['loadFromTop']))?'yes':'no';
        $data['loadFromBehind'] = (isset($data['loadFromBehind']))?'yes':'no';
        $data['loadTent'] = (isset($data['loadTent']))?'yes':'no';
        $data['condPlomb'] = (isset($data['condPlomb']))?'yes':'no';
        $data['condLoad'] = (isset($data['condLoad']))?'yes':'no';
        $data['condBelts'] = (isset($data['condBelts']))?'yes':'no';
        $data['condRemovableStands'] = (isset($data['condRemovableStands']))?'yes':'no';
        $data['condHardSide'] = (isset($data['condHardSide']))?'yes':'no';
        $data['condCollectableCargo'] = (isset($data['condCollectableCargo']))?'yes':'no';
        $data['condTemperature'] = (int) $data['condTemperature'];
        $data['condPalets'] = (int) $data['condPalets'];
        $data['condADR'] = (int) $data['condADR'];
        $data['transportTypeID'] = (int) $data['transportTypeID'];
        if ($data['transportTypeID'] == 0) $data['transportTypeID'] = NULL;
        if ($data['condTemperature'] == 0) $data['condTemperature'] = NULL;
        if ($data['condPalets'] == 0) $data['condPalets'] = NULL;
        if ($data['condADR'] == 0) $data['condADR'] = NULL;
        $data['currencyID'] = (int) $data['currencyID'];
        $data['price'] = (int) $data['price'];
        $data['priceHide'] = (isset($data['priceHide']))?'yes':'no';

        $data['filterCompanyYears'] = (int) $data['filterCompanyYears'];
        if ($data['filterCompanyYears'] == 0) $data['filterCompanyYears'] = NULL;

        $data['filterCompany'] = (isset($data['filterCompany']))?'yes':'no';
        $data['filterForwarder'] = (isset($data['filterForwarder']))?'yes':'no';
        $data['filterPartners'] = (isset($data['filterPartners']))?'yes':'no';

        return $data;
    }

    private function update_currency_price($id,$data)
    {
        DB::update($this->table_price)
                    ->set(array(
                        'specifiedPrice'=>$data['specifiedPrice'],
                        'value'=>$data['value'],
                        'paymentType'=>$data['paymentType'],
                        'PDV'=>$data['PDV'],
                        'onLoad'=>$data['onLoad'],
                        'onUnload'=>$data['onUnload'],
                        'onPrepay'=>$data['onPrepay'],
                        'advancedPayment'=>$data['advancedPayment'],
                        'currencyID'=>$data['priceCurrencyID'],
                        'customPriceType'=>$data['customPriceType']
                    ))
                    ->where('ID','=',$id)
                    ->execute();
    }

    private function create_currency_price($data)
    {
        $res = DB::insert($this->table_price,
            array(
                'value',
                'paymentType',
                'PDV',
                'onLoad',
                'onUnload',
                'onPrepay',
                'advancedPayment',
                'currencyID',
                'specifiedPrice',
                'customPriceType'
                )
                    )
                    ->values(
                    array(
                        $data['value'],
                        $data['paymentType'],
                        $data['PDV'],
                        $data['onLoad'],
                        $data['onUnload'],
                        $data['onPrepay'],
                        $data['advancedPayment'],
                        $data['priceCurrencyID'],
                        $data['specifiedPrice'],
                        $data['customPriceType']
                    )
                )
                    ->execute();
                return $res[0];
    }

    private function create_geo_relation($places_from,$places_to,$id)
    {
        $i = 1;
        foreach($places_from as $p_f)
        {
            $exp = explode(',',$p_f);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_tenders_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'from',$i))
                    ->execute();
            }

            $i++;
        }

        $i = 1;
        foreach($places_to as $p_t)
        {
            $exp = explode(',',$p_t);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_tenders_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'to',$i))
                    ->execute();
            }

            $i++;
        }
    }

    private function update_geo_relation($places_from,$places_to,$id)
    {
        DB::delete($this->table_tenders_geo)
                    ->where('requestID','=',$id)
                    ->execute();

        $this->create_geo_relation($places_from,$places_to,$id);
    }

    public function set_tender_status_by_date($tender_id)
    {
        $now = time()+5;

        $tender = DB::select('datetimeFromTender','datetimeToTender','status','userID','ID')
            ->from($this->table_tenders)
            ->where('ID','=',$tender_id)
            ->execute()
            ->as_array();

        if (empty($tender)) return null;

        $from = strtotime($tender[0]['datetimeFromTender']);
        $to = strtotime($tender[0]['datetimeToTender']);
        $status = $tender[0]['status'];

        if ($now > $from and $now < $to)
        {
            if ($status == 'inactive')
            {

                $email = $this->getEmailByUserID($tender[0]['userID']);

                $subject = 'Ваш тендер розпочато';
                $text = 'Тендер створений вами успішно активовано. <a href="/ua/tender/'.$tender[0]['ID'].'/">Перейдіть за цим посиланням для перегляду</a>';
                Helper_Customemail::send($email,$subject,$text);

                $status = 'active';
            }
        }
        else
        {
            if ($now <= $from)
            {
                if ($status == 'active') $status = 'inactive';
            }

            if ($now >= $to)
            {
                if ($status == 'active')
                {

                    $member_emails = $this->getTenderMemberEmails($tender[0]['ID']);
                    $email = $this->getEmailByUserID($tender[0]['userID']);
                    $member_emails[] = $email;

                    $subject = 'Тендер завершено';
                    $text = 'Тендер завершено. <a href="/ua/tender/'.$tender[0]['ID'].'/">Перейдіть за посиланням для детальної інформації</a>';

                    foreach($member_emails as $e)
                    {
                        Helper_Customemail::send($e,$subject,$text);
                    }

                    $status = 'completed';
                }
            }
        }

        DB::update($this->table_tenders)
            ->set(array(
                'status'=>$status,
            ))
            ->where('ID','=',$tender_id)
            ->execute();
    }

    public function getEmailByUserID($user_id)
    {
        $res = DB::select('email')
            ->from($this->table_users)
            ->where('ID','=',$user_id)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0]['email'];
        return null;
    }

    public function getTenderMemberEmails($tender_id)
    {
        $res = DB::select($this->table_users.'.email')
            ->distinct(true)
            ->from($this->table_tenders_requests)
            ->join($this->table_users,'left')->on($this->table_users.'.ID','=',$this->table_tenders_requests.'.userID')
            ->where($this->table_tenders_requests.'.tenderID','=',$tender_id)
            ->execute()
            ->as_array();
        if (!empty($res)) return Helper_Array::get_array_by_key($res,'email');
        return [];
    }
}