<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_CargoRequests extends Model {

    public $table_cargo = 'Frontend_Cargo';
    public $table_cargo_geo = 'Frontend_Cargo_to_Geo_Relation';
    public $table_geo = 'Frontend_Geo';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_price = 'Frontend_Request_Prices';
    public $table_cur = 'Frontend_Currencies';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_user = 'Frontend_Users';
    public $table_car_types = 'Frontend_User_Car_Types';
    public $table_phone_code = 'Frontend_Phonecodes';

    public function valid($data)
    {
        $errors = array();

        if (isset($data['is_price']))
        {
            if (Helper_Valid::is_empty($data['price_priceCurrencyID']) and Helper_Valid::is_empty($data['price_customPriceType']))
            {
                $errors['type_currency'] = 1;
            }
        }

        $errors['dateFrom'] = 1;
        if (!Helper_Valid::is_empty($data['dateFrom']))
        {
            unset($errors['dateFrom']);
        }

        if (!$data['userID']) $errors['userID'] = 1;

        if (!$data['places_from'][0]) $errors['places_from'] = 1;
        if (!$data['places_to'][0]) $errors['places_to'] = 1;

        $errors['cargoType'] = 1;
        if (!Helper_Valid::is_empty($data['cargoType']))
        {
            unset($errors['cargoType']);
        }

        $errors['weight'] = 1;
        $test_weight = (float) $data['weight'];
        if ($test_weight > 0)
        {
            unset($errors['weight']);
        }

        $errors['volume'] = 1;
        $test_volume = (float) $data['volume'];
        if ($test_volume > 0)
        {
            unset($errors['volume']);
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data)
    {
        $data = $this->filter($data);

        $this->update_currency_price($data['priceID'],$data);
        $this->update_geo_relation($data['places_from'],$data['places_to'],$id);
        DB::update($this->table_cargo)
            ->set(array(
                'dateFrom'=>$data['dateFrom'],
                'dateTo'=>$data['dateTo'],
                'info'=>$data['info'],
                'priceID'=>$data['priceID'],
                'cargoType'=>$data['cargoType'],
                'transportTypeID'=>$data['transportTypeID'],
                'volume'=>$data['volume'],
                'weight'=>$data['weight'],
                'carCount'=>$data['carCount'],
                'sizeX'=>$data['sizeX'],
                'sizeY'=>$data['sizeY'],
                'sizeZ'=>$data['sizeZ'],
                'userID'=>$data['userID'],
                'docTIR'=>$data['docTIR'],
                'docCMR'=>$data['docCMR'],
                'docT1'=>$data['docT1'],
                'docSanPassport'=>$data['docSanPassport'],
                'docSanBook'=>$data['docSanBook'],
                'loadFromSide'=>$data['loadFromSide'],
                'loadFromTop'=>$data['loadFromTop'],
                'loadFromBehind'=>$data['loadFromBehind'],
                'loadTent'=>$data['loadTent'],
                'condPlomb'=>$data['condPlomb'],
                'condLoad'=>$data['condLoad'],
                'condBelts'=>$data['condBelts'],
                'condRemovableStands'=>$data['condRemovableStands'],
                'condHardSide'=>$data['condHardSide'],
                'condCollectableCargo'=>$data['condCollectableCargo'],
                'onlyTransporter'=>$data['onlyTransporter'],
                'condTemperature'=>$data['condTemperature'],
                'condPalets'=>$data['condPalets'],
                'condADR'=>$data['condADR']
            ))
            ->where('ID','=',$id)
            ->execute();
    }


    public function add($data)
    {
        $data = $this->filter($data);

            $priceID = $this->create_currency_price($data);
            $res = DB::insert($this->table_cargo,array(
                'dateFrom',
                'dateTo',
                'info',
                'cargoType',
                'transportTypeID',
                'volume',
                'weight',
                'carCount',
                'sizeX',
                'sizeY',
                'sizeZ',
                'userID',
                'priceID',
                'docTIR',
                'docCMR',
                'docT1',
                'docSanPassport',
                'docSanBook',
                'loadFromSide',
                'loadFromTop',
                'loadFromBehind',
                'loadTent',
                'condPlomb',
                'condLoad',
                'condBelts',
                'condRemovableStands',
                'condHardSide',
                'condCollectableCargo',
                'onlyTransporter',
                'condTemperature',
                'condPalets',
                'condADR'
            ))
                ->values(array(
                    $data['dateFrom'],
                    $data['dateTo'],
                    $data['info'],
                    $data['cargoType'],
                    $data['transportTypeID'],
                    $data['volume'],
                    $data['weight'],
                    $data['carCount'],
                    $data['sizeX'],
                    $data['sizeY'],
                    $data['sizeZ'],
                    $data['userID'],
                    $priceID,
                    $data['docTIR'],
                    $data['docCMR'],
                    $data['docT1'],
                    $data['docSanPassport'],
                    $data['docSanBook'],
                    $data['loadFromSide'],
                    $data['loadFromTop'],
                    $data['loadFromBehind'],
                    $data['loadTent'],
                    $data['condPlomb'],
                    $data['condLoad'],
                    $data['condBelts'],
                    $data['condRemovableStands'],
                    $data['condHardSide'],
                    $data['condCollectableCargo'],
                    $data['onlyTransporter'],
                    $data['condTemperature'],
                    $data['condPalets'],
                    $data['condADR']
                ))
                ->execute();

            $id = $res[0];
            $this->create_geo_relation($data['places_from'],$data['places_to'],$id);
            return $id;

    }


    public function delete($id)
    {
        try {

            DB::delete($this->table_cargo)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table_cargo.'.*',
            $this->table_price.'.specifiedPrice',
            $this->table_price.'.value',
            $this->table_price.'.paymentType',
            $this->table_price.'.PDV',
            $this->table_price.'.onLoad',
            $this->table_price.'.onUnload',
            $this->table_price.'.onPrepay',
            $this->table_price.'.advancedPayment',
            $this->table_price.'.currencyID',
            $this->table_price.'.customPriceType',
            $this->table_user.'.phone',
            $this->table_user.'.phoneStationary',
            array('table_phones_1.code', 'code'),
            array('table_phones_2.code','code_stat'),
            array($this->table_car_types.'.name','car_type'),
            array($this->table_cur.'.code','cur_name'),
            array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table_cargo.'.userID order by '.$this->table_user_locales.'.localeID asc limit 1)'),'user_name'),
            array(DB::expr('(select name from '.$this->table_company_locales.' where companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID asc limit 1)'),'company_name')
        )
            ->from($this->table_cargo)
            ->join($this->table_price,'left')->on($this->table_price.'.ID','=',$this->table_cargo.'.priceID')
            ->join($this->table_cur,'left')->on($this->table_cur.'.ID','=',$this->table_price.'.currencyID')
            ->join($this->table_car_types,'left')->on($this->table_car_types.'.ID','=',$this->table_cargo.'.transportTypeID')
            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_cargo.'.userID')
            ->join(DB::expr($this->table_phone_code.' as table_phones_1'),'left')->on('table_phones_1.ID','=',$this->table_user.'.phoneCodeID')
            ->join(DB::expr($this->table_phone_code.' as table_phones_2'),'left')->on('table_phones_2.ID','=',$this->table_user.'.phoneStationaryCodeID')
            ->where($this->table_cargo.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $m_geo = new Model_Cms_Geo();

        return array(
            'info'=>$info[0],
            'geo_groups'=>$m_geo->getGeoByRequestID($id,$this->table_cargo_geo),
            'price'=>$this->get_price($info[0]),
            'transport'=>$this->get_transport($info[0]),
            'cargo'=>$this->get_cargo($info[0]),
        );
    }

    private function get_cargo($data)
    {
        if (!$data['cargoType']) $data['cargoType'] = '-';
        if ($data['weight'] == 0) {$data['weight'] = '-';}else{$data['weight'].='т';}
        if ($data['volume'] == 0) {$data['volume'] = '-';}else{$data['volume'].='м<sup>3</sup>';}
        $return = [];
        $return[] = 'тип:&nbsp;'.$data['cargoType'];
        $return[] = 'вага:&nbsp;'.$data['weight'];
        $return[] = 'об\'єм:&nbsp;'.$data['volume'];
        return implode('<br>',$return);
    }

    private function get_transport($data)
    {
        if (!$data['car_type']) $data['car_type'] = 'не&nbsp;вказано';
        if ($data['carCount'] == 0) {$data['carCount'] = '-';}else{$data['carCount'] .= 'шт';}
        if ($data['sizeX'] == 0) {$data['sizeX'] = '-';}else{$data['sizeX'].='м';}
        if ($data['sizeY'] == 0) {$data['sizeY'] = '-';}else{$data['sizeY'].='м';}
        if ($data['sizeZ'] == 0) {$data['sizeZ'] = '-';}else{$data['sizeZ'].='м';}
        $return = [];
        $return[] = $data['car_type'];
        $return[] = 'кількість:&nbsp;'.$data['carCount'];
        $return[] = 'габарити:&nbsp;'.$data['sizeX'].'x'.$data['sizeY'].'x'.$data['sizeZ'];
        return implode('<br>',$return);

    }

    private function get_price($data)
    {
        if ($data['specifiedPrice'] == 'no') return 'не&nbsp;вказано';
        $return = [];

        if ($data['cur_name'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['cur_name'];

        }elseif($data['customPriceType'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['customPriceType'];
        }


        switch ($data['paymentType']) {
            case 'b/g':
                $return[] = 'б/г';
                break;
            case 'gotivka':
                $return[] = 'готівка';
                break;
            case 'combined':
                $return[] = 'комбінована';
                break;
            case 'card':
                $return[] = 'на&nbsp;картку';
                break;
        }

        if ($data['PDV']=='yes') $return[] = 'ПДВ';
        if ($data['onLoad']=='yes') $return[] = 'при&nbsp;завантаженні';
        if ($data['onUnload']=='yes') $return[] = 'при&nbsp;розвантаженні';
        if ($data['onPrepay']=='yes')
        {
            $temp = 'передоплата';
            if ($data['advancedPayment']) $temp = $temp.'&nbsp;'.$data['advancedPayment'].'%';
            $return[] = $temp;
        }

        return implode('<br>',$return);

    }

    private function filter($data)
    {

        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        $data['specifiedPrice'] = (isset($data['is_price']))?'yes':'no';
        $data['value'] = (float) $data['price_value'];
        $data['PDV'] = (isset($data['price_pdv']))?'yes':'no';
        $data['onLoad'] = (isset($data['price_onLoad']))?'yes':'no';
        $data['onUnload'] = (isset($data['price_onUnload']))?'yes':'no';
        $data['onPrepay'] = (isset($data['price_onPrepay']))?'yes':'no';
        $data['advancedPayment'] = (float) $data['price_advancedPayment'];
        $data['paymentType'] = $data['price_paymentType'];
        $data['customPriceType'] = $data['price_customPriceType'];
        $data['priceCurrencyID'] = ($data['price_priceCurrencyID'])?$data['price_priceCurrencyID']:NULL;

        $data['volume'] = (float) $data['volume'];
        $data['weight'] = (float) $data['weight'];
        $data['carCount'] = (int) $data['carCount'];
        if ($data['carCount'] == 0) $data['carCount'] = NULL;
        $data['sizeX'] = (float) $data['sizeX'];
        if ($data['sizeX'] == 0) $data['sizeX'] = NULL;
        $data['sizeY'] = (float) $data['sizeY'];
        if ($data['sizeY'] == 0) $data['sizeY'] = NULL;
        $data['sizeZ'] = (float) $data['sizeZ'];
        if ($data['sizeZ'] == 0) $data['sizeZ'] = NULL;

        $data['docTIR'] = (isset($data['docTIR']))?'yes':'no';
        $data['docCMR'] = (isset($data['docCMR']))?'yes':'no';
        $data['docT1'] = (isset($data['docT1']))?'yes':'no';
        $data['docSanPassport'] = (isset($data['docSanPassport']))?'yes':'no';
        $data['docSanBook'] = (isset($data['docSanBook']))?'yes':'no';
        $data['loadFromSide'] = (isset($data['loadFromSide']))?'yes':'no';
        $data['loadFromTop'] = (isset($data['loadFromTop']))?'yes':'no';
        $data['loadFromBehind'] = (isset($data['loadFromBehind']))?'yes':'no';
        $data['loadTent'] = (isset($data['loadTent']))?'yes':'no';
        $data['condPlomb'] = (isset($data['condPlomb']))?'yes':'no';
        $data['condLoad'] = (isset($data['condLoad']))?'yes':'no';
        $data['condBelts'] = (isset($data['condBelts']))?'yes':'no';
        $data['condRemovableStands'] = (isset($data['condRemovableStands']))?'yes':'no';
        $data['condHardSide'] = (isset($data['condHardSide']))?'yes':'no';
        $data['condCollectableCargo'] = (isset($data['condCollectableCargo']))?'yes':'no';
        $data['onlyTransporter'] = (isset($data['onlyTransporter']))?'yes':'no';
        $data['condTemperature'] = (int) $data['condTemperature'];
        $data['condPalets'] = (int) $data['condPalets'];
        $data['condADR'] = (int) $data['condADR'];
        $data['transportTypeID'] = (int) $data['transportTypeID'];
        if ($data['transportTypeID'] == 0) $data['transportTypeID'] = NULL;
        if ($data['condTemperature'] == 0) $data['condTemperature'] = NULL;
        if ($data['condPalets'] == 0) $data['condPalets'] = NULL;
        if ($data['condADR'] == 0) $data['condADR'] = NULL;
        $data['dateTo'] = ($data['dateTo'])?$data['dateTo']:NULL;

        return $data;
    }

    private function update_currency_price($id,$data)
    {
        DB::update($this->table_price)
                    ->set(array(
                        'specifiedPrice'=>$data['specifiedPrice'],
                        'value'=>$data['value'],
                        'paymentType'=>$data['paymentType'],
                        'PDV'=>$data['PDV'],
                        'onLoad'=>$data['onLoad'],
                        'onUnload'=>$data['onUnload'],
                        'onPrepay'=>$data['onPrepay'],
                        'advancedPayment'=>$data['advancedPayment'],
                        'currencyID'=>$data['priceCurrencyID'],
                        'customPriceType'=>$data['customPriceType']
                    ))
                    ->where('ID','=',$id)
                    ->execute();
    }

    private function create_currency_price($data)
    {
        $res = DB::insert($this->table_price,
            array(
                'value',
                'paymentType',
                'PDV',
                'onLoad',
                'onUnload',
                'onPrepay',
                'advancedPayment',
                'currencyID',
                'specifiedPrice',
                'customPriceType'
                )
                    )
                    ->values(
                    array(
                        $data['value'],
                        $data['paymentType'],
                        $data['PDV'],
                        $data['onLoad'],
                        $data['onUnload'],
                        $data['onPrepay'],
                        $data['advancedPayment'],
                        $data['priceCurrencyID'],
                        $data['specifiedPrice'],
                        $data['customPriceType']
                    )
                )
                    ->execute();
                return $res[0];
    }

    private function create_geo_relation($places_from,$places_to,$id)
    {
        $i = 1;
        foreach($places_from as $p_f)
        {
            $exp = explode(',',$p_f);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_cargo_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'from',$i))
                    ->execute();
            }

            $i++;
        }

        $i = 1;
        foreach($places_to as $p_t)
        {
            $exp = explode(',',$p_t);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_cargo_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'to',$i))
                    ->execute();
            }

            $i++;
        }
    }

    private function update_geo_relation($places_from,$places_to,$id)
    {
        DB::delete($this->table_cargo_geo)
                    ->where('requestID','=',$id)
                    ->execute();

        $this->create_geo_relation($places_from,$places_to,$id);
    }
}