<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Company extends Model {

    public $table = 'Frontend_Companies';
    public $table_locales = 'Frontend_Company_Locales';
    public $table_owner = 'Frontend_User_to_Company_Ownership';
    public $table_geo_rel = 'Frontend_Company_to_Geo_Relation';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_geo = 'Frontend_Geo';

    public function valid($data,$langs)
    {
        $errors = array();

        if (!$data['country_places']) $errors['country_places'] = 1;

        if (!$data['typeID']) $errors['typeID'] = 1;

        if (!$data['ownershipTypeID']) $errors['ownershipTypeID'] = 1;

        if (!$data['phoneCodeID']) $errors['phoneCodeID'] = 1;


        $errors['empty_ipn'] = 1;
        if (!Helper_Valid::is_empty($data['ipn']))
        {
            unset($errors['empty_ipn']);
        }



        $errors['empty_phone'] = 1;
        if (!Helper_Valid::is_empty($data['phone']))
        {
            unset($errors['empty_phone']);
            if (!Helper_Valid::phone_number($data['phone'])) $errors['phone'] = 1;
        }



        //перевірка назви
        $errors['not_isset_name'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['name'][$l['ID']]))
            {
                unset($errors['not_isset_name']);
            }
        }

        //перевірка адреси
        $errors['not_isset_address'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['address'][$l['ID']]))
            {
                unset($errors['not_isset_address']);
            }
        }

        if (!$data['userID'])
        {
            $m_user = new Model_Component_Cms_User();
            $valid_user = $m_user->valid_owner($data['new_user_info'],$langs);
            if (!empty($valid_user['errors']))
            {
                $errors['new_user'] = $valid_user['errors'];
            }
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data,$langs)
    {
        array_walk_recursive($data, array($this,'filter'));

        $approvedByAdmin = isset($data['approvedByAdmin'])?'yes':'no';
        $trustableMark = isset($data['trustableMark'])?'yes':'no';
        $data['phoneStationaryCodeID'] = ($data['phoneStationaryCodeID'])?$data['phoneStationaryCodeID']:NULL;

        $arr = array(
            'approvedByAdmin'=>$approvedByAdmin,
            'trustableMark'=>$trustableMark,
            'typeID'=>$data['typeID'],
            'ipn'=>$data['ipn'],
            'ownershipTypeID'=>$data['ownershipTypeID'],
            'phone'=>$data['phone'],
            'phoneStationary'=>$data['phoneStationary'],
            'phoneCodeID'=>$data['phoneCodeID'],
            'phoneStationaryCodeID'=>$data['phoneStationaryCodeID']
        );

        DB::update($this->table)
            ->set($arr)
            ->where('ID','=',$id)
            ->execute();

        $this->locales($id,$data,$langs);

        $this->owner($data,$id,$langs);

        $this->del_places($id);

        $this->add_new_places($id,$data['country_places']);
    }


    public function add($data,$langs)
    {
        array_walk_recursive($data, array($this,'filter'));

        $primary_locale_id = null;
        foreach($data['address'] as $key => $val)
        {
            $primary_locale_id = $key;
            break;
        }

        $approvedByAdmin = isset($data['approvedByAdmin'])?'yes':'no';
        $trustableMark = isset($data['trustableMark'])?'yes':'no';
        $data['phoneStationaryCodeID'] = ($data['phoneStationaryCodeID'])?$data['phoneStationaryCodeID']:NULL;

        $res = DB::insert($this->table,array(
            'registered',
            'approvedByAdmin',
            'trustableMark',
            'typeID',
            'ipn',
            'ownershipTypeID',
            'phone',
            'phoneStationary',
            'phoneCodeID',
            'phoneStationaryCodeID',
            'primaryLocaleID'
        ))
            ->values(array(
                date("Y-m-d H:i:s"),
                $approvedByAdmin,
                $trustableMark,
                $data['typeID'],
                $data['ipn'],
                $data['ownershipTypeID'],
                $data['phone'],
                $data['phoneStationary'],
                $data['phoneCodeID'],
                $data['phoneStationaryCodeID'],
                $primary_locale_id
            ))
            ->execute();

        $id = $res[0];

        $this->locales($id,$data,$langs);

        $this->owner($data,$id,$langs);

        $this->add_new_places($id,$data['country_places']);

        return $id;
    }


    private function owner($data,$company_id,$langs)
    {
        if ($data['userID'])
        {
             $owner = DB::select(DB::expr('count(ID) as c'))
                         ->from($this->table_owner)
                         ->where('companyID','=',$company_id)
                         ->where('userID','=',$data['userID'])
                         ->execute()
                         ->as_array();

             if ($owner[0]['c'] == 0)
             {
                 DB::delete($this->table_owner)
                     ->where('companyID','=',$company_id)
                     ->execute();

                 DB::insert($this->table_owner,array('userID','companyID'))
                             ->values(array($data['userID'],$company_id))
                             ->execute();

             }

        }
        else
        {

               $data['new_user_info']['companyID'] = $company_id;

               $m_user = new Model_Component_Cms_User();
               $user_id = $m_user->add($data['new_user_info'],$langs);

               DB::delete($this->table_owner)
                        ->where('companyID','=',$company_id)
                        ->execute();

                DB::insert($this->table_owner,array('userID','companyID'))
                    ->values(array($user_id,$company_id))
                    ->execute();
        }
    }


    private function locales($id,$data,$langs)
    {
        foreach($langs as $lang)
        {

                $isset = DB::select('ID')
                            ->from($this->table_locales)
                            ->where('localeID','=',$lang['ID'])
                            ->where('companyID','=',$id)
                            ->execute()
                            ->as_array();

                if (empty($isset))
                {
                    if ($data['address'][$lang['ID']])
                    {

                        DB::insert($this->table_locales,array('name','address','realAddress','info','localeID','companyID'))
                                    ->values(array(
                                        $data['name'][$lang['ID']],
                                        $data['address'][$lang['ID']],
                                        $data['realAddress'][$lang['ID']],
                                        $data['info'][$lang['ID']],
                                        $lang['ID'],
                                        $id
                                    ))
                                    ->execute();
                    }
                }
                else
                {
                    DB::update($this->table_locales)
                                ->set(array(
                                    'name'=>$data['name'][$lang['ID']],
                                    'address'=>$data['address'][$lang['ID']],
                                    'realAddress'=>$data['realAddress'][$lang['ID']],
                                    'info'=>$data['info'][$lang['ID']],
                                ))
                                ->where('localeID','=',$lang['ID'])
                                ->where('companyID','=',$id)
                                ->execute();
                }

        }
    }

    public function delete($id)
    {
        try {

            DB::delete($this->table)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table.'.*'
        )
            ->from($this->table)
            ->where($this->table.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        $locales = DB::select()
            ->from($this->table_locales)
            ->where('companyID','=',$id)
            ->execute()
            ->as_array();

        if (!empty($locales))
        {
            $locales = Helper_Array::change_keys($locales,'localeID');
        }
        else
        {
            $locales = [];
        }


        $owner = DB::select('userID')
                    ->from($this->table_owner)
                    ->where('companyID','=',$id)
                    ->execute()
                    ->as_array();

        if (!empty($owner))
        {
            $info['userID'] = $owner[0]['userID'];
            $m_user = new Model_Component_Cms_User();
            $info['userName'] = $m_user->get_name($info['userID']);
        }

        $dir_for_company_files = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/private/company/';

        $podatkovi = Helper_Dir::getFilesFromDir($dir_for_company_files.'podatkovi/'.$id.'/');
        $sertifikaty = Helper_Dir::getFilesFromDir($dir_for_company_files.'sertifikaty/'.$id.'/');
        $license = Helper_Dir::getFilesFromDir($dir_for_company_files.'license-transport-and-expedition/'.$id.'/');
        $cert = Helper_Dir::getFilesFromDir($dir_for_company_files.'cert-state-reg/'.$id.'/');

        $files = array(
            'podatkovi'=>$podatkovi,
            'sertifikaty'=>$sertifikaty,
            'license-transport-and-expedition'=>$license,
            'cert-state-reg'=>$cert
        );

        return array('info'=>$info,'locales'=>$locales,'files'=>$files,'geo_name'=>$this->get_geo_name($id),'geo_ids'=>$this->get_geo_ids($id));

    }

    public function get_name($company_id)
    {
        $res = DB::select('name')
                    ->from($this->table_locales)
                    ->where('companyID','=',$company_id)
                    ->order_by('localeID')
                    ->limit(1)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['name'];
                return null;
    }

    private function get_geo_ids($company_id)
    {
        $res = DB::select('geoID')
                    ->from($this->table_geo_rel)
                    ->where('companyID','=',$company_id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return implode(',',Helper_Array::get_array_by_key($res,'geoID'));
                return null;
    }

    public function get_geo_name($company_id)
    {

        $res = DB::select('geoID')
                    ->from($this->table_geo_rel)
                    ->where('companyID','=',$company_id)
                    ->execute()
                    ->as_array();

        if (empty($res)) return null;

        $geo_ids = implode(',',Helper_Array::get_array_by_key($res,'geoID'));


        $geo = DB::select('ID')
                    ->from($this->table_geo)
                    ->where('ID','in',DB::expr('('.$geo_ids.')'))
                    ->order_by('level','desc')
                    ->limit(1)
                    ->execute()
                    ->as_array();

        $geo_id = $geo[0]['ID'];


        $locales = DB::select('fullAddress')
                    ->from($this->table_geo_locales)
                    ->where('geoID','=',$geo_id)
                    ->execute()
                    ->as_array();

        if (empty($locales)) return null;

        $name = '';
        foreach($locales as $l)
        {
            if ($l['fullAddress']) $name = $l['fullAddress'];
        }
        return $name;
    }

    private function add_new_places($company_id,$places)
    {
        $exp = explode(',',$places);
        foreach($exp as $e)
        {
            DB::insert($this->table_geo_rel,array('geoID','companyID'))
                        ->values(array($e,$company_id))
                        ->execute();
        }
    }

    private function del_places($company_id)
    {
        DB::delete($this->table_geo_rel)
                    ->where('companyID','=',$company_id)
                    ->execute();
    }

    function filter(&$val)
    {
        $val = trim($val);
    }

}