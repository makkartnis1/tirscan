<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_User extends Model {

    public $table = 'Frontend_Users';
    public $table_company = 'Frontend_Companies';
    public $table_company_locales = 'Frontend_Company_Locales';
    public $table_locales = 'Frontend_User_Locales';


    public function valid_owner($data,$langs)
    {
        $errors = array();

        if (!Helper_Valid::pass($data['pass'],$data['conf_pass'])) $errors['pass'] = 1;

        if (!Helper_Valid::uniq($data['email'],$this->table,'email',null,'ID')) $errors['email_isset'] = 1;

        //перевірка імен користувача
        $errors['not_isset_name'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['name'][$l['ID']]))
            {
                unset($errors['not_isset_name']);
                if (!Helper_Valid::name($data['name'][$l['ID']])) $errors['name'] = 1;
            }
        }

        if (!$data['phoneCodeID']) $errors['phoneCodeID'] = 1;

        $errors['empty_phone'] = 1;
        if (!Helper_Valid::is_empty($data['phone']))
        {
            unset($errors['empty_phone']);
            if (!Helper_Valid::phone_number($data['phone'])) $errors['phone'] = 1;
        }

        return array('data'=>$data,'errors'=>$errors);
    }

    public function valid($data,$langs,$id = null,$change_pass = 1)
    {
        $errors = array();

        if (!$data['companyID']) $errors['companyID'] = 1;

        if ($change_pass)
        {
            if (!Helper_Valid::pass($data['pass'],$data['conf_pass'])) $errors['pass'] = 1;
        }

        if (!Helper_Valid::uniq($data['email'],$this->table,'email',$id,'ID')) $errors['email_isset'] = 1;

        //перевірка імен користувача
        $errors['not_isset_name'] = 1;
        foreach($langs as $l)
        {
            if (!Helper_Valid::is_empty($data['name'][$l['ID']]))
            {
                unset($errors['not_isset_name']);
                if (!Helper_Valid::name($data['name'][$l['ID']])) $errors['name'] = 1;
            }
        }

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data,$langs)
    {
        $data = $this->filter($data);

        $change_pass = isset($data['change_pass'])?1:0;
        $emailApproved = isset($data['emailApproved'])?'approved':'pending';

        $data['phoneCodeID'] = ($data['phoneCodeID'])?$data['phoneCodeID']:NULL;
        $data['phoneStationaryCodeID'] = ($data['phoneStationaryCodeID'])?$data['phoneStationaryCodeID']:NULL;

        $arr = array(
            'email'=>$data['email'],
            'emailApproved'=>$emailApproved,
            'icq'=>$data['icq'],
            'skype'=>$data['skype'],
            'companyID'=>$data['companyID'],
            'phone'=>$data['phone'],
            'phoneStationary'=>$data['phoneStationary'],
            'phoneCodeID'=>$data['phoneCodeID'],
            'phoneStationaryCodeID'=>$data['phoneStationaryCodeID']
        );

        if ($change_pass)
        {
            $arr['hash'] = Helper_Myauth::hash($data['pass']);
        }

        DB::update($this->table)
            ->set($arr)
            ->where('ID','=',$id)
            ->execute();

        $this->locales($id,$data,$langs);

    }


    public function add($data,$langs)
    {

        $data = $this->filter($data);

        $primary_locale_id = null;
        foreach($data['name'] as $key => $val)
        {
            $primary_locale_id = $key;
            break;
        }


        $emailApproved = isset($data['emailApproved'])?'approved':'pending';

        $data['phoneCodeID'] = ($data['phoneCodeID'])?$data['phoneCodeID']:NULL;
        $data['phoneStationaryCodeID'] = ($data['phoneStationaryCodeID'])?$data['phoneStationaryCodeID']:NULL;

        $res = DB::insert($this->table,array(
            'email',
            'emailApproved',
            'icq',
            'skype',
            'hash',
            'companyID',
            'phone',
            'phoneStationary',
            'phoneCodeID',
            'phoneStationaryCodeID',
            'primaryLocaleID'
        ))
            ->values(array(
                $data['email'],
                $emailApproved,
                $data['icq'],
                $data['skype'],
                Helper_Myauth::hash($data['pass']),
                $data['companyID'],
                $data['phone'],
                $data['phoneStationary'],
                $data['phoneCodeID'],
                $data['phoneStationaryCodeID'],
                $primary_locale_id
            ))
            ->execute();

        $id = $res[0];

        $this->locales($id,$data,$langs);

        return $id;
    }




    private function locales($id,$data,$langs)
    {
        foreach($langs as $lang)
        {

                $isset = DB::select('ID')
                            ->from($this->table_locales)
                            ->where('localeID','=',$lang['ID'])
                            ->where('userID','=',$id)
                            ->execute()
                            ->as_array();

                if (empty($isset))
                {
                    if ($data['name'][$lang['ID']])
                    {

                        DB::insert($this->table_locales,array('name','localeID','userID'))
                                    ->values(array(
                                        $data['name'][$lang['ID']],
                                        $lang['ID'],
                                        $id
                                    ))
                                    ->execute();
                    }
                }
                else
                {
                    DB::update($this->table_locales)
                                ->set(array(
                                    'name'=>$data['name'][$lang['ID']]
                                ))
                                ->where('localeID','=',$lang['ID'])
                                ->where('userID','=',$id)
                                ->execute();
                }

        }
    }

    public function delete($id)
    {
        try {

            DB::delete($this->table)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }

    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table.'.*',
            array($this->table_company_locales.'.name','company_name')
        )
            ->from($this->table)
            ->join($this->table_company,'left')->on($this->table_company.'.ID','=',$this->table.'.companyID')
            ->join($this->table_company_locales,'left')->on($this->table_company_locales.'.companyID','=',$this->table.'.companyID')
            ->where($this->table.'.ID','=',$id)
            ->where($this->table_company_locales.'.localeID','=',DB::expr($this->table_company.'.primaryLocaleID'))
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $info = $info[0];

        $locales = DB::select()
            ->from($this->table_locales)
            ->where('userID','=',$id)
            ->execute()
            ->as_array();

        if (!empty($locales))
        {
            $locales = Helper_Array::change_keys($locales,'localeID');
        }
        else
        {
            $locales = [];
        }

        return array('info'=>$info,'locales'=>$locales);

    }

    private function filter($data)
    {
        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        return $data;
    }

    public function get_name($id)
    {
        $res = DB::select('name')
                    ->from($this->table_locales)
                    ->where('userID','=',$id)
                    ->execute()
                    ->as_array();
                if (!empty($res))
                {
                    foreach($res as $r)
                    {
                        if ($r['name']) return $r['name'];
                    }
                }
                return null;
    }
}