<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_Messages extends Model {

    public $t_m = 'Frontend_Messages';
    public $t_d = 'Frontend_Dialogs';

    public function delete($id)
    {
        try {

            DB::delete($this->t_m)
                ->where('id','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }

    public function checkDialogID($id)
    {
        $res = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->t_m)
                    ->where('dialogID','=',$id)
                    ->execute()
                    ->as_array();
        $count = $res[0]['c'];
        if ($count > 0) return true;
        return false;
    }

    public function getTheme($dialog_id)
    {
        $res = DB::select('theme')
                    ->from($this->t_d)
                    ->where('id','=',$dialog_id)
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['theme'];
                return null;
    }

}
