<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_TransportRequests extends Model {

    public $table_transport = 'Frontend_Transports';
    public $table_transport_geo = 'Frontend_Transport_to_Geo_Relation';
    public $table_geo = 'Frontend_Geo';
    public $table_geo_locales = 'Frontend_Geo_Locales';
    public $table_price = 'Frontend_Request_Prices';
    public $table_cur = 'Frontend_Currencies';
    public $table_cars = 'Frontend_User_Cars';
    public $table_car_types = 'Frontend_User_Car_Types';
    public $table_user_locales = 'Frontend_User_Locales';
    public $table_user = 'Frontend_Users';
    public $table_phone_code = 'Frontend_Phonecodes';
    public $table_company_locales = 'Frontend_Company_Locales';



    public function valid($data)
    {
        $errors = array();

        if (isset($data['is_price']))
        {
            if (Helper_Valid::is_empty($data['price_priceCurrencyID']) and Helper_Valid::is_empty($data['price_customPriceType']))
            {
                $errors['typeCurrency'] = 1;
            }
        }

        $errors['dateFrom'] = 1;
        if (!Helper_Valid::is_empty($data['dateFrom']))
        {
            unset($errors['dateFrom']);
        }

        if (!$data['userID']) $errors['userID'] = 1;
        if (!$data['userCarID']) $errors['userCarID'] = 1;

        if (!$data['places_from'][0]) $errors['places_from'] = 1;
        if (!$data['places_to'][0]) $errors['places_to'] = 1;

        return array('data'=>$data,'errors'=>$errors);
    }


    public function save($id,$data)
    {
        $data = $this->filter($data);

        $this->update_currency_price($data['priceID'],$data);
        $this->update_geo_relation($data['places_from'],$data['places_to'],$id);
        DB::update($this->table_transport)
            ->set(array(
                'dateFrom'=>$data['dateFrom'],
                'dateTo'=>$data['dateTo'],
                'info'=>$data['info'],
                'userCarID'=>$data['userCarID'],
                'priceID'=>$data['priceID'],
                'userID'=>$data['userID'],
                'docTIR'=>$data['docTIR'],
                'docCMR'=>$data['docCMR'],
                'docT1'=>$data['docT1'],
                'docSanPassport'=>$data['docSanPassport'],
                'docSanBook'=>$data['docSanBook'],
                'loadFromSide'=>$data['loadFromSide'],
                'loadFromTop'=>$data['loadFromTop'],
                'loadFromBehind'=>$data['loadFromBehind'],
                'loadTent'=>$data['loadTent'],
                'condPlomb'=>$data['condPlomb'],
                'condLoad'=>$data['condLoad'],
                'condBelts'=>$data['condBelts'],
                'condRemovableStands'=>$data['condRemovableStands'],
                'condHardSide'=>$data['condHardSide'],
                'condCollectableCargo'=>$data['condCollectableCargo'],
                'condTemperature'=>$data['condTemperature'],
                'condPalets'=>$data['condPalets'],
                'condADR'=>$data['condADR']
            ))
            ->where('ID','=',$id)
            ->execute();
    }


    public function add($data)
    {
        $data = $this->filter($data);

            $priceID = $this->create_currency_price($data);
            $res = DB::insert($this->table_transport,array(
                'dateFrom',
                'dateTo',
                'info',
                'userCarID',
                'userID',
                'priceID',
                'docTIR',
                'docCMR',
                'docT1',
                'docSanPassport',
                'docSanBook',
                'loadFromSide',
                'loadFromTop',
                'loadFromBehind',
                'loadTent',
                'condPlomb',
                'condLoad',
                'condBelts',
                'condRemovableStands',
                'condHardSide',
                'condCollectableCargo',
                'condTemperature',
                'condPalets',
                'condADR'
            ))
                ->values(array(
                    $data['dateFrom'],
                    $data['dateTo'],
                    $data['info'],
                    $data['userCarID'],
                    $data['userID'],
                    $priceID,
                    $data['docTIR'],
                    $data['docCMR'],
                    $data['docT1'],
                    $data['docSanPassport'],
                    $data['docSanBook'],
                    $data['loadFromSide'],
                    $data['loadFromTop'],
                    $data['loadFromBehind'],
                    $data['loadTent'],
                    $data['condPlomb'],
                    $data['condLoad'],
                    $data['condBelts'],
                    $data['condRemovableStands'],
                    $data['condHardSide'],
                    $data['condCollectableCargo'],
                    $data['condTemperature'],
                    $data['condPalets'],
                    $data['condADR']
                ))
                ->execute();

            $id = $res[0];
            $this->create_geo_relation($data['places_from'],$data['places_to'],$id);
            return $id;

    }


    public function delete($id)
    {
        try {

            DB::delete($this->table_transport)
                ->where('ID','=',$id)
                ->execute();

        } catch (Exception $e) {

        }
    }


    public function get_one($id)
    {
        $info = DB::select(
            $this->table_transport.'.*',
            $this->table_price.'.specifiedPrice',
            $this->table_price.'.value',
            $this->table_price.'.paymentType',
            $this->table_price.'.PDV',
            $this->table_price.'.onLoad',
            $this->table_price.'.onUnload',
            $this->table_price.'.onPrepay',
            $this->table_price.'.advancedPayment',
            $this->table_price.'.currencyID',
            $this->table_cars.'.number',
            $this->table_cars.'.volume',
            $this->table_cars.'.sizeX',
            $this->table_cars.'.sizeY',
            $this->table_cars.'.sizeZ',
            $this->table_cars.'.liftingCapacity',
            $this->table_cars.'.type',
            $this->table_price.'.customPriceType',
            $this->table_user.'.phone',
            $this->table_user.'.phoneStationary',
            array('table_phones_1.code', 'code'),
            array('table_phones_2.code','code_stat'),
            array($this->table_car_types.'.name','car_type'),
            array($this->table_cur.'.code','cur_name'),
            array(DB::expr('(select name from '.$this->table_user_locales.' where userID = '.$this->table_transport.'.userID order by '.$this->table_user_locales.'.localeID asc limit 1)'),'user_name'),
            array(DB::expr('(select name from '.$this->table_company_locales.' where companyID = '.$this->table_user.'.companyID order by '.$this->table_company_locales.'.localeID asc limit 1)'),'company_name')
        )
            ->from($this->table_transport)
            ->join($this->table_price,'left')->on($this->table_price.'.ID','=',$this->table_transport.'.priceID')
            ->join($this->table_cur,'left')->on($this->table_cur.'.ID','=',$this->table_price.'.currencyID')
            ->join($this->table_cars,'left')->on($this->table_cars.'.ID','=',$this->table_transport.'.userCarID')
            ->join($this->table_user,'left')->on($this->table_user.'.ID','=',$this->table_transport.'.userID')
            ->join(DB::expr($this->table_phone_code.' as table_phones_1'),'left')->on('table_phones_1.ID','=',$this->table_user.'.phoneCodeID')
            ->join(DB::expr($this->table_phone_code.' as table_phones_2'),'left')->on('table_phones_2.ID','=',$this->table_user.'.phoneStationaryCodeID')
            ->join($this->table_car_types,'left')->on($this->table_car_types.'.ID','=',$this->table_cars.'.carTypeID')
            ->where($this->table_transport.'.ID','=',$id)
            ->limit(1)
            ->execute()
            ->as_array();

        if (empty($info)) return null;

        $m_geo = new Model_Cms_Geo();

        return array(
            'info'=>$info[0],
            'geo_groups'=>$m_geo->getGeoByRequestID($id,$this->table_transport_geo),
            'price'=>$this->get_price($info[0]),
            'transport'=>$this->get_transport($info[0])
        );
    }


    private function get_transport($data)
    {
        $return = [];
        $return[] = $data['car_type'];
        switch ($data['type']) {
            case 'vantazhivka':
                $return[] = 'вантажівка';
                break;
            case 'napiv prychip':
                $return[] = 'напівпричіп';
                break;
            case 'z prychipom':
                $return[] = 'з&nbsp;причіпом';
                break;
        }

        $return[] = $data['sizeX'].'x'.$data['sizeY'].'x'.$data['sizeZ'];
        $return[] = $data['liftingCapacity'].'т'.'/'.$data['volume'].'м3';
        $return[] = 'номер&nbsp;машини:'.$data['number'];

        return implode('<br>',$return);

    }


    private function get_price($data)
    {
        if ($data['specifiedPrice'] == 'no') return 'не&nbsp;вказано';
        $return = [];

        if ($data['cur_name'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['cur_name'];

        }elseif($data['customPriceType'])
        {
            $return[] = $data['value'].'&nbsp;'.$data['customPriceType'];
        }


        switch ($data['paymentType']) {
            case 'b/g':
                $return[] = 'б/г';
                break;
            case 'gotivka':
                $return[] = 'готівка';
                break;
            case 'combined':
                $return[] = 'комбінована';
                break;
            case 'card':
                $return[] = 'на&nbsp;картку';
                break;
        }

        if ($data['PDV']=='yes') $return[] = 'ПДВ';
        if ($data['onLoad']=='yes') $return[] = 'при&nbsp;завантаженні';
        if ($data['onUnload']=='yes') $return[] = 'при&nbsp;розвантаженні';
        if ($data['onPrepay']=='yes')
        {
            $temp = 'передоплата';
            if ($data['advancedPayment']) $temp = $temp.'&nbsp;'.$data['advancedPayment'].'%';
            $return[] = $temp;
        }

        return implode('<br>',$return);

    }

    public function filter($data)
    {

        foreach($data as &$val)
        {
            if (is_array($val))
            {
                foreach($val as &$v)
                {
                    $v = trim($v);
                }
            }
            else
            {
                $val = trim($val);
            }
        }

        $data['specifiedPrice'] = (isset($data['is_price']))?'yes':'no';
        $data['value'] = (float) $data['price_value'];
        $data['PDV'] = (isset($data['price_pdv']))?'yes':'no';
        $data['onLoad'] = (isset($data['price_onLoad']))?'yes':'no';
        $data['onUnload'] = (isset($data['price_onUnload']))?'yes':'no';
        $data['onPrepay'] = (isset($data['price_onPrepay']))?'yes':'no';
        $data['advancedPayment'] = (float) $data['price_advancedPayment'];
        $data['paymentType'] = $data['price_paymentType'];
        $data['priceCurrencyID'] = ($data['price_priceCurrencyID'])?$data['price_priceCurrencyID']:NULL;
        $data['customPriceType'] = $data['price_customPriceType'];

        $data['docTIR'] = (isset($data['docTIR']))?'yes':'no';
        $data['docCMR'] = (isset($data['docCMR']))?'yes':'no';
        $data['docT1'] = (isset($data['docT1']))?'yes':'no';
        $data['docSanPassport'] = (isset($data['docSanPassport']))?'yes':'no';
        $data['docSanBook'] = (isset($data['docSanBook']))?'yes':'no';
        $data['loadFromSide'] = (isset($data['loadFromSide']))?'yes':'no';
        $data['loadFromTop'] = (isset($data['loadFromTop']))?'yes':'no';
        $data['loadFromBehind'] = (isset($data['loadFromBehind']))?'yes':'no';
        $data['loadTent'] = (isset($data['loadTent']))?'yes':'no';
        $data['condPlomb'] = (isset($data['condPlomb']))?'yes':'no';
        $data['condLoad'] = (isset($data['condLoad']))?'yes':'no';
        $data['condBelts'] = (isset($data['condBelts']))?'yes':'no';
        $data['condRemovableStands'] = (isset($data['condRemovableStands']))?'yes':'no';
        $data['condHardSide'] = (isset($data['condHardSide']))?'yes':'no';
        $data['condCollectableCargo'] = (isset($data['condCollectableCargo']))?'yes':'no';
        $data['condTemperature'] = (int) $data['condTemperature'];
        $data['condPalets'] = (int) $data['condPalets'];
        $data['condADR'] = (int) $data['condADR'];
        if ($data['condTemperature'] == 0) $data['condTemperature'] = NULL;
        if ($data['condPalets'] == 0) $data['condPalets'] = NULL;
        if ($data['condADR'] == 0) $data['condADR'] = NULL;
        $data['dateTo'] = ($data['dateTo'])?$data['dateTo']:NULL;

        return $data;
    }

    private function update_currency_price($id,$data)
    {
        DB::update($this->table_price)
                    ->set(array(
                        'specifiedPrice'=>$data['specifiedPrice'],
                        'value'=>$data['value'],
                        'paymentType'=>$data['paymentType'],
                        'PDV'=>$data['PDV'],
                        'onLoad'=>$data['onLoad'],
                        'onUnload'=>$data['onUnload'],
                        'onPrepay'=>$data['onPrepay'],
                        'advancedPayment'=>$data['advancedPayment'],
                        'currencyID'=>$data['priceCurrencyID'],
                        'customPriceType'=>$data['customPriceType']
                    ))
                    ->where('ID','=',$id)
                    ->execute();
    }

    private function create_currency_price($data)
    {
        $res = DB::insert($this->table_price,
            array(
                'value',
                'paymentType',
                'PDV',
                'onLoad',
                'onUnload',
                'onPrepay',
                'advancedPayment',
                'currencyID',
                'specifiedPrice',
                'customPriceType'
                )
                    )
                    ->values(
                    array(
                        $data['value'],
                        $data['paymentType'],
                        $data['PDV'],
                        $data['onLoad'],
                        $data['onUnload'],
                        $data['onPrepay'],
                        $data['advancedPayment'],
                        $data['priceCurrencyID'],
                        $data['specifiedPrice'],
                        $data['customPriceType']
                    )
                )
                    ->execute();
                return $res[0];
    }

    private function create_geo_relation($places_from,$places_to,$id)
    {
        $i = 1;
        foreach($places_from as $p_f)
        {
            $exp = explode(',',$p_f);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_transport_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'from',$i))
                    ->execute();
            }

            $i++;
        }

        $i = 1;
        foreach($places_to as $p_t)
        {
            $exp = explode(',',$p_t);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->table_transport_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,$e,'to',$i))
                    ->execute();
            }

            $i++;
        }
    }

    private function update_geo_relation($places_from,$places_to,$id)
    {
        DB::delete($this->table_transport_geo)
                    ->where('requestID','=',$id)
                    ->execute();

        $this->create_geo_relation($places_from,$places_to,$id);
    }
}