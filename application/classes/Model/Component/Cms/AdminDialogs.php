<?php defined('SYSPATH') or die('No direct script access.');

class Model_Component_Cms_AdminDialogs extends Model {

    public $t_m = 'Frontend_Messages';
    public $t_m_u = 'Frontend_Messages_Unread';
    public $t_d = 'Frontend_Dialogs';
    public $t_d_u = 'Frontend_Dialogs_Unread';

    public function delete($dialog_id)
    {
        try {

            DB::delete($this->t_d)
                ->where('id','=',$dialog_id)
                ->execute();

        } catch (Exception $e) {

        }
    }

    public function countNew()
    {
        $res = DB::select(DB::expr('count(id) as c'))
                    ->from($this->t_m_u)
                    ->where('toAdminID','!=','NULL')
                    ->group_by('dialogID')
                    ->execute()
                    ->as_array();
                if (!empty($res)) return $res[0]['c'];
                return null;
    }

}
