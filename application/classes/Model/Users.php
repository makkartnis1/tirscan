<?php defined('SYSPATH') or die('No direct script access.');

class Model_Users {

    public static $tables = [
        'users' => 'Frontend_User_Emails',
        'companies' => 'Frontend_Companies',
    ];

    public static function changePassword($new_password) {
        $email = Session::instance()->get('auth', false);

        DB::update('Frontend_User_Emails')
            ->set([
                'hash' => md5($new_password)
            ])
            ->where('email', '=', $email)
            ->execute();
    }

    public static function updateCompany($data) {
        $email = Session::instance()->get('auth', false);

        DB::update('Frontend_User_Emails')
            ->set([
                'email' => $data['email'],
                'pib' => $data['pib'],
                'address' => $data['address'],
                'street' => $data['street'],
                'company' => $data['company'],
                'country' => $data['country'],
                'phone' => $data['phone'],
                'phone2' => $data['phone2'],
                'icq' => $data['icq'],
                'skype' => $data['skype'],
                'ipn' => $data['ipn'],
                'info' => $data['info'],
            ])
            ->where('email', '=', $email)
            ->execute();
    }

    public static function addCompany($email, $password, $activated, $type, $user_info) {
        DB::insert(self::$tables['users'], ['email', 'hash', 'activated', 'type',
            'pib', 'address', 'street', 'company', 'country', 'icq', 'skype', 'phone', 'phone2', 'info', 'ipn'
        ])
            ->values([$email, $password, $activated, $type,
                $user_info['user-pib'], $user_info['user-address'], $user_info['user-street'], $user_info['user-company'],
                $user_info['user-country'], $user_info['user-icq'], $user_info['user-skype'], $user_info['user-phone'],
                $user_info['user-phone2'], $user_info['user-info'], ''
            ])
            ->execute();
    }

    public static function getCompany($email) {
        $query = DB::select('email', 'hash', 'type', 'pib', 'address', 'street', 'company', 'country', 'icq', 'skype', 'phone', 'phone2',
        'info', 'ipn')
            ->from('Frontend_User_Emails')
            ->where('email', '=', $email)
            ->execute()
            ->as_array();

        return $query[0];
    }

}
