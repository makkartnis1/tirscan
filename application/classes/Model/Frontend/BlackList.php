<?php defined('SYSPATH') or die('No direct script access.');


class Model_Frontend_BlackList extends Model_Frontend_ORM
{
    const TABLE = 'Frontend_Black_List';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'company' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'companyID'
        ],
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

    public function rules(){
        $rules = [
            'companyID' => [
                [ 'not_empty' ]
            ],

            'fromUserID' => [
                [ 'not_empty' ]
            ]
        ];

        return $rules;
    }

    public static function get_list($user_id, $localeID, $limit, $offset){

        $query = DB::select(
            'FBL.*',
            'FCL.name'
        )
            ->from([self::TABLE, 'FBL'])

            ->join([Model_Frontend_Company_Locale::TABLE, 'FCL'], 'LEFT')
            ->on('FCL.companyID', '=', 'FBL.companyID')
            ->on('FCL.localeID', '=', DB::expr($localeID))

            ->where('fromUserID', '=', $user_id)
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function del_company_with_blacklist($companyID, $user_id){

        $query = DB::delete(self::TABLE)
            ->where('companyID', '=', $companyID)
            ->and_where('fromUserID', '=', $user_id)
            ->execute();

        return $query;

    }

}