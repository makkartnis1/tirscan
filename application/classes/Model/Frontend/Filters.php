<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Filters
 *
 * @author Сергій Krid
 */
class Model_Frontend_Filters {

    public static function decimal($value, $decimals) {
        list($decimal) = array_values(localeconv());

        $value = str_replace(',', $decimal, $value);
        $value = str_replace('.', $decimal, $value);

        if (! is_numeric($value))
            return $value;

        return number_format($value, $decimals, $decimal, '');
    }

    public static function onlyDigits($value) {
        $string = preg_replace("/([^\d])+/", (string) '', $value);
        $string = ($string === false) ? '' : $string;

        return $string;
    }
    public static function null_if_empty_string($value){

        return (empty($value)) ? null : $value;

    }

}