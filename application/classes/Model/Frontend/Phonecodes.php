<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Phonecodes
 *
 * @author Сергій Krid
 */
class Model_Frontend_Phonecodes extends Model_Frontend_ORM {

    protected static $_codes = null;

    const TABLE = 'Frontend_Phonecodes';

    protected $_table_name = self::TABLE;

    public static function phoneCodes(){

        if(self::$_codes === null)
            self::$_codes = DB::select()->from(self::TABLE)->execute()->as_array();

        return self::$_codes;

    }

}