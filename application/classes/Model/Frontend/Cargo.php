<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Cargo
 *
 * @author Сергій Krid
 */
class Model_Frontend_Cargo extends Model_Frontend_ORM {

    const TABLE        = 'Frontend_Cargo';
    const TABLE_TO_GEO = 'Frontend_Cargo_to_Geo_Relation';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'price' => [
            'model' => 'Frontend_Request_Price',
            'foreign_key' => 'priceID'
        ],
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

    public function getPostName() {
        return 'cargo';
    }

    public function addGeoRelation(Model_Frontend_Geo $geo, $type) {
        DB::insert(self::TABLE_TO_GEO, ['geoID', 'requestID', 'type'])
          ->values([$geo->ID, $this->ID, $type])
          ->execute();
    }

    public function addGeoRelationByIDs(array $ids, $type, $group) {
        foreach ($ids as $id) {
            DB::insert(self::TABLE_TO_GEO, ['geoID', 'requestID', 'type', 'groupNumber'])
              ->values([$id, $this->ID, $type, $group])
              ->execute();
        }
    }

    /*
     * ALTER TABLE `tirscan_site`.`Frontend_Cargo_to_Geo_Relation` DROP INDEX `type`, ADD UNIQUE `type` (`type`, `geoID`, `requestID`, `groupNumber`) USING BTREE;
     */

    public static function query_getCargoForProfile(Model_Frontend_User $user) {
        $query = DB::select(
            ['t_cargo.ID', 'ID'],

            't_cargo.created',
            'dateFrom',
            'dateTo',

            // інформація про авто
            ['t_user_car.sizeX', 'car_sizeX'],
            ['t_user_car.sizeY', 'car_sizeY'],
            ['t_user_car.sizeZ', 'car_sizeZ'],
            ['t_user_car.volume', 'car_volume'],
            ['t_user_car.liftingCapacity', 'car_liftingCapacity'],
            ['t_user_car.type', 'car_type'],
            ['t_user_car_type.localeName', 'car_type_localeName']
        )
                   ->from([self::TABLE, 't_cargo'])
            // join машини і її типу
                   ->join([Model_Frontend_User_Car::TABLE, 't_user_car'], 'LEFT')
                   ->on('t_cargo.userCarID', '=', 't_user_car.ID')
                   ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
                   ->on('t_user_car.carTypeID', '=', 't_user_car_type.ID')
                   ->where('t_cargo.userID', '=', $user->ID)
                   ->order_by('t_cargo.created', 'DESC');

        return $query->execute();
    }

    public static function query_getPlaces($locale_id, $request_id, $type) {
        $locale_id = 1; // TODO HOTFIX не вставляє переклади на інших мовах ЗАГЛУШКА

        $query = DB::select('geoID')
                   ->from(self::TABLE_TO_GEO)
                   ->where('requestID', '=', $request_id)
                   ->and_where('type', '=', $type)
                   ->and_where('groupNumber', '=', 1);

//        if (count($query->execute()->as_array()) == 0) {
//            var_dump($request_id); die;
//        }

        $geo_result = DB::select('ID')
                        ->from(Model_Frontend_Geo::TABLE)
                        ->where('ID', 'IN', $query)
                        ->order_by('level', 'DESC')
                        ->limit(1)
                        ->execute()
                        ->as_array();

        foreach ($geo_result as &$result) {
            $result = $result['ID'];
            unset($result);
        }

//        $i = 0;
//        do {
        $query_geo_locale = DB::select(['fullAddress', $type . '_' . 'fullAddress'], ['name', $type . '_' . 'name'], ['country', $type . '_' . 'country'])
                              ->from(Model_Frontend_Geo_Locale::TABLE)
                              ->where('geoID', 'IN', $geo_result)
                              ->and_where('localeID', '=', $locale_id)
                              ->execute()
                              ->as_array();

//            if (count($query_geo_locale) == 0) {
//                $locale_uri = ORM::factory('Frontend_Locale', $locale_id)->googleLang;
//                $place_ids  = DB::select('placeID')
//                                ->from(Model_Frontend_Geo::TABLE)
//                                ->where('ID', 'IN', $geo_result)
//                                ->execute()
//                                ->as_array();
//
//                foreach ($place_ids as $place_id) {
//                    Request::factory('/cms/geo/' . $locale_uri . '/add')
//                           ->post([
//                               'place_id' => $place_id['placeID'],
//                           ])
//                           ->execute()
//                           ->body();
//
//                    echo $place_id['placeID'];
//                }
//            }
//
//            if ($i++ > 10) {
//                break;
//            }
//        } while (count($query_geo_locale) == 0);

        $query_geo = DB::select(['placeID', $type . '_' . 'placeID'])
                       ->from(Model_Frontend_Geo::TABLE)
                       ->where('ID', 'IN', $geo_result)
                       ->execute()
                       ->as_array();


        return array_merge($query_geo_locale[0], $query_geo[0]);
    }

    public static function query_getOtherPlaces($locale_id, $request_id, $type) {
        $locale_id = 1; // TODO HOTFIX не вставляє переклади на інших мовах ЗАГЛУШКА

        $query = DB::select('geoID', 'groupNumber')
                   ->from(self::TABLE_TO_GEO)
                   ->where('requestID', '=', $request_id)
                   ->and_where('type', '=', $type)
                   ->and_where('groupNumber', '>', 1);

        $q_ids = $query->execute()->as_array();
        $ids   = [];
        foreach ($q_ids as $q_id) {
            $ids[ $q_id['geoID'] ] = $q_id['groupNumber'];
        }

        $func_result = [];
        if (count($ids) > 0) {
            $geo_result = DB::select('ID')
                            ->from(Model_Frontend_Geo::TABLE)
                            ->where('ID', 'IN', array_keys($ids))
                            ->and_where('type', '=', DB::expr("'city'"))
                            ->execute()
                            ->as_array();

            foreach ($geo_result as &$result) {
                $result = $result['ID'];
                unset($result);
            }

            if (count($geo_result) > 0) {
                $query_geo_locale = DB::select(['geoID', $type . '_' . 'geoID'], ['fullAddress', $type . '_' . 'fullAddress'], ['name', $type . '_' . 'name'], ['country', $type . '_' . 'country'])
                                      ->from(Model_Frontend_Geo_Locale::TABLE)
                                      ->where('geoID', 'IN', $geo_result)
                                      ->and_where('localeID', '=', $locale_id)
                                      ->execute()
                                      ->as_array();

                $query_geo = DB::select(['placeID', $type . '_' . 'placeID'])
                               ->from(Model_Frontend_Geo::TABLE)
                               ->where('ID', 'IN', $geo_result)
                               ->execute()
                               ->as_array();

                asort($ids);
                $query_geo_loc_temp = [];
                foreach ($query_geo_locale as $key => $value) {
                    $query_geo_loc_temp[$value[$type . '_geoID']] = $value;
                }
                $query_geo_locale = [];
                foreach ($ids as $key => $value) {
                    if (array_key_exists($key, $query_geo_loc_temp)) {
                        $query_geo_locale[] = $query_geo_loc_temp[$key];
                    }
                }
                
                foreach ($query_geo_locale as $key => $value) {
                    $merged            = array_merge($value, $query_geo[$key]);
                    $func_result[$key] = $merged;
                }
            }
        }

        return [
            $type . '_otherGeo' => $func_result
        ];
    }

    public static function query_dropViews($requestID, $new_views_old) {
        DB::update(self::TABLE)
          ->set(['views_old' => $new_views_old])
          ->where('ID', '=', $requestID)
          ->execute();
    }

    public static function query_getCargoSelect($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = DB::select(
            ['t_cargo.ID', 'ID'],

            't_cargo.views',
            [DB::expr('(`t_cargo`.`views`-`t_cargo`.`views_old`)'), 'new_views'],

            't_cargo.parser',
            't_cargo.isVip',
            't_cargo.isColored',
            't_cargo.status',
            't_cargo.created',
            ['t_cargo.created', 'createdRaw'],
            't_cargo.info',
            'dateFrom',
            'dateTo',
            't_user.companyID',
            ['t_comp.typeID', 'companyTypeID'],

            // інформація про ціну
            't_price.value',
            ['t_curr.code', 'valueCode'],

            't_cargo.userID',
            't_cargo.cargoType',
            't_cargo.carCount',
            't_cargo.volume',
            't_cargo.weight',
            't_cargo.sizeX',
            't_cargo.sizeY',
            't_cargo.sizeZ',

            // інформація про користувача і компанію (вибираємо існуючі переклади)
            [DB::expr('IFNULL(`t_user_loc`.`name`, `t_user_loc_prim`.`name`)'), 'username'], // ім'я користувача
            [DB::expr('IFNULL(`t_comp_loc`.`name`, `t_comp_loc_prim`.`name`)'), 'company_name'], // назва компанії
            ['t_ph_codes.code', 'phoneCode'],
            't_user.phone',

            // інформація про авто
            ['t_user_car_type.localeName', 'car_type_localeName']
        )
                   ->from([self::TABLE, 't_cargo'])
            // join ціни
                   ->join([Model_Frontend_Request_Price::TABLE, 't_price'], 'LEFT')
                   ->on('t_cargo.priceID', '=', 't_price.ID')
                   ->join([Model_Frontend_Currency::TABLE, 't_curr'], 'LEFT')
                   ->on('t_price.currencyID', '=', 't_curr.ID')
            // join машини і її типу
                   ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
                   ->on('t_cargo.transportTypeID', '=', 't_user_car_type.ID')
            // join користувача
                   ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
                   ->on('t_cargo.userID', '=', 't_user.ID')
            // join таблиці телефонних кодів
                   ->join([Model_Frontend_Phonecodes::TABLE, 't_ph_codes'], 'LEFT')
                   ->on('t_user.phoneCodeID', '=', 't_ph_codes.id')
            // join локалей користувача (на поточній мові і мові реєстрації)
                   ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc'], 'LEFT')
                   ->on('t_user.ID', '=', 't_user_loc.userID')
                   ->on('t_user_loc.localeID', '=', DB::expr($localeID))
                   ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc_prim'], 'LEFT')
                   ->on('t_user.ID', '=', 't_user_loc_prim.userID')
                   ->on('t_user.primaryLocaleID', '=', 't_user_loc_prim.localeID')
            // join компанії
                   ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
                   ->on('t_user.companyID', '=', 't_comp.ID')
            // join локалей компанії (на поточній мові і мові реєстрації)
                   ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc'], 'LEFT')
                   ->on('t_comp.ID', '=', 't_comp_loc.companyID')
                   ->on('t_comp_loc.localeID', '=', DB::expr($localeID))
                   ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc_prim'], 'LEFT')
                   ->on('t_comp.ID', '=', 't_comp_loc_prim.companyID')
                   ->on('t_comp.primaryLocaleID', '=', 't_comp_loc_prim.localeID')
            // join geo
                   ->join([self::TABLE_TO_GEO, 't_cargo_to_geo_from'], 'INNER')
                   ->on('t_cargo.ID', '=', 't_cargo_to_geo_from.requestID')
                   ->on('t_cargo_to_geo_from.type', '=', DB::expr('"from"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
                   ->on('t_cargo_to_geo_from.geoID', '=', 't_geo_from.ID')
                   ->join([self::TABLE_TO_GEO, 't_cargo_to_geo_to'], 'INNER')
                   ->on('t_cargo.ID', '=', 't_cargo_to_geo_to.requestID')
                   ->on('t_cargo_to_geo_to.type', '=', DB::expr('"to"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
                   ->on('t_cargo_to_geo_to.geoID', '=', 't_geo_to.ID')
                   ->group_by('t_cargo.ID')
                   ->order_by('t_cargo.created', 'DESC');


        // додаєм фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        return $query;
    }

    public static function assignPlacesToCargo($localeID, $result_cargo) {
        foreach ($result_cargo as &$cargo) {
            $places_from = self::query_getPlaces($localeID, $cargo['ID'], 'from');
            $places_to   = self::query_getPlaces($localeID, $cargo['ID'], 'to');

            $places_from_other = self::query_getOtherPlaces($localeID, $cargo['ID'], 'from');;
            $places_to_other = self::query_getOtherPlaces($localeID, $cargo['ID'], 'to');;

            $cargo = array_merge($cargo, $places_from, $places_to, $places_from_other, $places_to_other);
            unset($cargo);
        }

        return $result_cargo;
    }

    public static function query_getCargoTickets($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getCargoInfoSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $result_tr = $query
            ->select(['t_app_comp.ID', 'appCompID'])
            ->join([Model_Frontend_User_Cargo_Application::TABLE, 't_app'], 'LEFT')
            ->on('t_app.cargoID', '=', 't_cargo.ID')
            ->on('t_app.status', '=', DB::expr("'accepted'"))
            ->join([Model_Frontend_User::TABLE, 't_app_user'], 'LEFT')
            ->on('t_app_user.ID', '=', 't_app.userID')
            ->join([Model_Frontend_Company::TABLE, 't_app_comp'], 'LEFT')
            ->on('t_app_comp.ID', '=', 't_app_user.companyID')
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return self::assignPlacesToCargo($localeID, $result_tr);
    }

    public static function query_getCargoInfoSelect($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getCargoSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $query// довибираємо пачтку данних для JSON-а

        ->select(['t_cargo.docTIR', 'json_docTIR'])
        ->select(['t_cargo.docCMR', 'json_docCMR'])
        ->select(['t_cargo.docT1', 'json_docT1'])
        ->select(['t_cargo.docSanPassport', 'json_docSanPassport'])
        ->select(['t_cargo.docSanBook', 'json_docSanBook'])
        ->select(['t_cargo.loadFromSide', 'json_loadFromSide'])
        ->select(['t_cargo.loadFromTop', 'json_loadFromTop'])
        ->select(['t_cargo.loadFromBehind', 'json_loadFromBehind'])
        ->select(['t_cargo.loadTent', 'json_loadTent'])
        ->select(['t_cargo.condPlomb', 'json_condPlomb'])
        ->select(['t_cargo.condLoad', 'json_condLoad'])
        ->select(['t_cargo.condBelts', 'json_condBelts'])
        ->select(['t_cargo.condRemovableStands', 'json_condRemovableStands'])
        ->select(['t_cargo.condHardSide', 'json_condHardSide'])
        ->select(['t_cargo.condCollectableCargo', 'json_condCollectableCargo'])
        ->select(['t_cargo.condTemperature', 'json_condTemperature'])
        ->select(['t_cargo.condPalets', 'json_condPalets'])
        ->select(['t_cargo.condADR', 'json_condADR'])
        ->select(['t_cargo.onlyTransporter', 'json_onlyTransporter'])
        ->select(['t_price.paymentType', 'json_paymentType'])
        ->select(['t_price.PDV', 'json_PDV'])
        ->select(['t_price.onLoad', 'json_onLoad'])
        ->select(['t_price.onUnload', 'json_onUnload'])
        ->select(['t_price.onPrepay', 'json_onPrepay'])
        ->select(['t_price.advancedPayment', 'json_advancedPayment'])//%
        ->select(['t_cargo.info', 'json_info'])

        ->select(['t_cargo.carCount', 'json_carCount'])

        ->select(['t_cargo.sizeX', 'json_sizeX'])
        ->select(['t_cargo.sizeY', 'json_sizeY'])
        ->select(['t_cargo.sizeZ', 'json_sizeZ'])

        ->select(['t_cargo.cargoType', 'json_cargoType'])
        ->select(['t_price.customPriceType', '_json_customPriceType'])
        ->select(['t_price.value', '_json_price'])
        ->select(['t_curr.code', '_json_currency_code']);

        __db('transport.option.carCount');
        return $query;
    }

    public static function query_getCargo($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getCargoInfoSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $result_cargo = $query
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return self::assignPlacesToCargo($localeID, $result_cargo);
    }

    public static function query_getNewCargoCountSince(array $filters = [], $since_time) {
        $filters[] = [
            'created', '>', $since_time
        ];

        return self::query_getCargoCount($filters);
    }

    public static function query_getCargoCount(array $filters = []) {
        $query = DB::select([DB::expr('COUNT(DISTINCT(t_cargo.ID))'), 'count'])
                   ->from([self::TABLE, 't_cargo'])
            // join ціни
                   ->join([Model_Frontend_Request_Price::TABLE, 't_price'], 'LEFT')
                   ->on('t_cargo.priceID', '=', 't_price.ID')
            // join користувача
                   ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
                   ->on('t_cargo.userID', '=', 't_user.ID')
            // join компанії
                   ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
                   ->on('t_user.companyID', '=', 't_comp.ID')
            // join geo
                   ->join([self::TABLE_TO_GEO, 't_cargo_to_geo_from'], 'INNER')
                   ->on('t_cargo.ID', '=', 't_cargo_to_geo_from.requestID')
                   ->on('t_cargo_to_geo_from.type', '=', DB::expr('"from"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
                   ->on('t_cargo_to_geo_from.geoID', '=', 't_geo_from.ID')
                   ->join([self::TABLE_TO_GEO, 't_cargo_to_geo_to'], 'INNER')
                   ->on('t_cargo.ID', '=', 't_cargo_to_geo_to.requestID')
                   ->on('t_cargo_to_geo_to.type', '=', DB::expr('"to"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
                   ->on('t_cargo_to_geo_to.geoID', '=', 't_geo_to.ID');

        // додаємо фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        $result = $query
            ->execute()
            ->as_array();


        return $result[0]['count'];
    }

}