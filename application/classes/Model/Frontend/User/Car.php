<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_User_Car
 *
 * @author Сергій Krid
 */
class Model_Frontend_User_Car extends Model_Frontend_ORM {

    const TABLE    = 'Frontend_User_Cars';
    const NO_IMAGE = '/assets/css/file/no-img.jpg';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ],
        'carType' => [
            'model' => 'Frontend_User_Car_Type',
            'foreign_key' => 'carTypeID'
        ]
    ];

    public $documents = [];

    public function rules() {
        return [
            'type' => [
                ['Model_Frontend_Validation::in_array', [':value', ['vantazhivka', 'napiv prychip', 'z prychipom']]]
            ],
            'sizeX' => [
//                [ 'not_empty' ],
['numeric']
            ],
            'sizeY' => [
//                [ 'not_empty' ],
['numeric']
            ],
            'sizeZ' => [
//                [ 'not_empty' ],
['numeric']
            ],
            'liftingCapacity' => [
//                [ 'not_empty' ],
['numeric']
            ],
            'volume' => [
//                [ 'not_empty' ],
['numeric']
            ]
        ];
    }

    public function filters() {
        return [
            'sizeX' => [
                array('Model_Frontend_Filters::decimal', array(':value', 3))
            ],
            'sizeY' => [
                array('Model_Frontend_Filters::decimal', array(':value', 3))
            ],
            'sizeZ' => [
                array('Model_Frontend_Filters::decimal', array(':value', 3))
            ],
            'liftingCapacity' => [
                array('Model_Frontend_Filters::decimal', array(':value', 3))
            ],
            'volume' => [
                array('Model_Frontend_Filters::decimal', array(':value', 3))
            ],
            'avatar' => [
                [
                    function ($value) {
                        return (empty($value)) ? NULL : $value;
                    }
                ]
            ],
        ];
    }

    public function getTitle() {
        $number = $this->number;
        if ($number == null) {
            $number = 'номер не вказано';
        }

        return $number . ' (' . __db('car.body.' . $this->type) . ', ' . $this->liftingCapacity . ' kg, ' . $this->volume . ' m3)';
    }

    public function getTypeTitle() {
        return __db($this->carType->localeName) . ', ' . __db('car.body.' . $this->type);
    }

    public static function title($type, $number = null) {

        if ($number === null) {
            $number = 'car.body.number.undefined';
        }

        return [$number, 'car.body.' . $type];
    }

    public static function query_getTransportByCompanyID($company_id) {
        $query_transport = DB::select(
            't_trans.ID',
            't_trans.number',
            't_trans.info',
            't_trans.volume',
            't_trans.sizeX',
            't_trans.sizeY',
            't_trans.sizeZ',
            't_trans.liftingCapacity',
            't_trans.type',
            't_cartype.localeName'
        )
                             ->from([self::TABLE, 't_trans'])
                             ->join([Model_Frontend_User::TABLE, 't_user'], 'INNER')
                             ->on('t_trans.userID', '=', 't_user.ID')
                             ->join([Model_Frontend_User_Car_Type::TABLE, 't_cartype'], 'INNER')
                             ->on('t_trans.carTypeID', '=', 't_cartype.ID')
                             ->where('t_user.companyID', '=', $company_id)
                             ->execute()
                             ->as_array();

        return $query_transport;
    }

    /**
     * @param $userID INT ID користувача
     * @return Database_Query_Builder_Select
     */
    public static function query_getUserTransport_no_execute($userID) {

        return DB::select()
                 ->from([self::TABLE, 'car'])
                 ->where('car.userID', '=', $userID)
                 ->join([Model_Frontend_User_Car_Type::TABLE, 'type'], 'INNER')
                 ->on('car.carTypeID', '=', 'type.ID');

    }

    public static function up_table_main_img() {

        // TODO

        $query = DB::query(null, "ALTER TABLE {self::TABLE} ADD `avatar` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'avatar file name from: Frontend_Uploader::DIR_UPL . Frontend_Uploader::DIR_VISIBLE_PUBLIC . Frontend_Uploader::DIR_CAR_GALLERY . \"/{" . '$ID' . "}\"' ;");
        $query->execute();
    }

}