<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_User_Car_Type
 *
 * @author Сергій Krid
 */
class Model_Frontend_User_Car_Type extends Model_Frontend_ORM {

    protected static $types = [
        'vantazhivka'       => '',
        'napiv prychip'     => '2',
        'z prychipom'       => '3',
    ];

    const TABLE = 'Frontend_User_Car_Types';

    protected $_table_name = self::TABLE;

    public static function cssIcon($enumType=null){

        if($enumType === null)
            return self::$types;

        return Arr::get(self::$types, $enumType);

    }

}