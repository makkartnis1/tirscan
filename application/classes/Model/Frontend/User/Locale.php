<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_User_Locale
 *
 * @author Сергій Krid
 */
class Model_Frontend_User_Locale extends Model_Frontend_ORM {

    const TABLE = 'Frontend_User_Locales';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ],
        'locale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'localeID'
        ]
    ];

    public function rules() {
        return [
            'name' => [
                [ 'not_empty' ]
            ]
        ];
    }

    public function filters() {
        return [
            'name' => [
                [ 'trim' ],
                [ 'Model_Frontend_Filters::null_if_empty_string', [':value'] ],
            ]
        ];
    }

}