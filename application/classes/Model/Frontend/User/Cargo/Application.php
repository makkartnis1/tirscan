<?php defined('SYSPATH') or die('No direct script access.');

class Model_Frontend_User_Cargo_Application extends Model_Frontend_ORM {

    const TABLE = 'Frontend_User_to_Cargo_Requests';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ],
        'request' => [
            'model' => 'Frontend_Cargo',
            'foreign_key' => 'cargoID'
        ]
    ];

    public static function query_applicationExist($company_id, $user_id) {
        $query_outside = DB::select()
            ->from([self::TABLE, 't_app'])
            ->join([Model_Frontend_Cargo::TABLE, 't_cargo'], 'INNER')
            ->on('t_app.cargoID', '=', 't_cargo.ID')
            ->join([Model_Frontend_User::TABLE, 't_user'], 'INNER')
            ->on('t_cargo.userID', '=', 't_user.ID')
            ->where('t_app.userID', '=', $user_id)
            ->and_where('t_user.companyID', '=', $company_id)
            ->and_where('t_app.status', '=', DB::expr("'accepted'"))
            ->execute()
            ->as_array();
        $outside_contacts = !empty($query_outside);

        $query_inside = DB::select()
            ->from([self::TABLE, 't_app'])
            ->join([Model_Frontend_Cargo::TABLE, 't_cargo'], 'INNER')
            ->on('t_app.cargoID', '=', 't_cargo.ID')
            ->join([Model_Frontend_User::TABLE, 't_user'], 'INNER')
            ->on('t_app.userID', '=', 't_user.ID')
            ->where('t_cargo.userID', '=', $user_id)
            ->and_where('t_user.companyID', '=', $company_id)
            ->and_where('t_app.status', '=', DB::expr("'accepted'"))
            ->execute()
            ->as_array();
        $inside_contacts = !empty($query_inside);

        return ($outside_contacts || $inside_contacts);
    }

    public static function query_getCompaniesIdsByRequest($user_id, $request_id) {
        $companies = DB::select('t_comp.ID', ['t_app.ID', 'appID'])
                       ->from([self::TABLE, 't_app'])
                       ->join([Model_Frontend_User::TABLE, 't_user'], 'INNER')
                       ->on('t_user.ID', '=', 't_app.userID')
                       ->join([Model_Frontend_Company::TABLE, 't_comp'], 'INNER')
                       ->on('t_comp.ID', '=', 't_user.companyID')
                       ->join([Model_Frontend_Cargo::TABLE, 't_trans'], 'INNER')
                       ->on('t_app.cargoID', '=', 't_trans.ID')
                       ->where('t_app.cargoID', '=', $request_id)
                       ->and_where('t_trans.userID', '=', $user_id)
                       ->and_where('t_app.status', '=', DB::expr("'pending'"))
                       ->execute()
                       ->as_array();

        return $companies;
    }

}