<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_User_Social
 *
 * @author Сергій Krid
 */
class Model_Frontend_User_Social extends Model_Frontend_ORM {

    const TABLE = 'Frontend_User_Socials';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

}