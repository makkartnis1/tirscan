<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class Frontend_ORM
 *
 * @author Сергій Krid
 */
class Model_Frontend_ORM extends ORM {

    public function getPostName() {
        $class = str_replace( 'Model_Frontend_', '', get_called_class() );

        return strtolower($class);
    }

}