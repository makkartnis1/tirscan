<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Subscribe
 *
 * @author
 */
class Model_Frontend_Subscription extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Subscription';

    protected $_table_name = self::TABLE;

    public function rules(){
        $rules = [
            'companyID' => [
                [ 'not_empty' ]
            ],
            'userID' => [
                [ 'not_empty' ]
            ]
        ];

        return $rules;
    }

    public static function get_company_list($user_id, $localeID, $limit, $offset){

        $query = DB::select(
            'FS.*',
            'FCL.name'
        )
            ->from([self::TABLE, 'FS'])

            ->join([Model_Frontend_Company_Locale::TABLE, 'FCL'], 'LEFT')
            ->on('FCL.companyID', '=', 'FS.companyID')
            ->on('FCL.localeID', '=', DB::expr($localeID))

            ->where('userID', '=', $user_id)
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function del_company_with_subscribe($companyID, $user_id){

        $query = DB::delete(self::TABLE)
            ->where('companyID', '=', $companyID)
            ->and_where('userID', '=', $user_id)
            ->execute();

        return $query;

    }

}