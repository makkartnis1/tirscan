<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Frontend_Messages_Dialogs
 *
 * @author
 */
class Model_Frontend_Messages_Dialogs extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Dialogs';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [];

    public function rules(){
        $rules = [
            'fromUserID' => [
                ['not_empty']
            ],

            'toUserID' => [
                ['not_empty']
            ],

            'theme' => [
                ['not_empty']
            ],
        ];

        return $rules;
    }

    public static function createDialog($theme, $to_user_id, $from_user_id){
        $query = DB::insert(self::TABLE, ['theme', 'toUserID', 'fromUserID'])
            ->values([$theme, $to_user_id, $from_user_id])
            ->execute();

        return $query;
    }

    public static function addStatus($to_user_id, $dialog_id){
        $query = DB::insert('Frontend_Dialogs_Unread', ['toUserID', 'dialogID'])
            ->values([$to_user_id, $dialog_id])
            ->execute();

        return $query;
    }

    public static function updateStatus($id, $to_user_id) {
        $query = DB::delete('Frontend_Dialogs_Unread')
            ->where('dialogID', '=', $id)
            ->and_where('toUserID', '=', $to_user_id)
            ->execute();

        return $query;
    }

    public static function getLastDialog(){
        $query = DB::select('id')
            ->from(self::TABLE)
            ->order_by('id', 'DESC')
            ->limit('1')
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getDialogsByUser($localeID, $id) {
        $query = DB::select(
                'FD.*',
                ['FUL_to.name', 'name_to'],
                ['FUL_from.name', 'name_from']
            )
            ->from([self::TABLE, 'FD'])

            ->join([Model_Frontend_User_Locale::TABLE, 'FUL_to'], 'LEFT')
            ->on('FUL_to.userID', '=', 'FD.toUserID')
            ->on('FUL_to.localeID', '=', DB::expr($localeID))

            ->join([Model_Frontend_User_Locale::TABLE, 'FUL_from'], 'LEFT')
            ->on('FUL_from.userID', '=', 'FD.fromUserID')
            ->on('FUL_from.localeID', '=', DB::expr($localeID))

            ->where('FD.deleted', '=', 'false')

            ->and_where('FD.fromUserID', '=', $id)
            ->or_where('FD.toUserID', '=', $id)

            ->order_by('created', 'DESC')
            ->execute()
            ->as_array();

        return $query;
    }

    public static function delDialog($id) {
        $query = DB::update(self::TABLE)
            ->set(['deleted' => 'true'])
            ->where('ID', '=', $id)
            ->execute();

        return $query;
    }

//    public static function getUnreadDialogs($user_id){
//        $query = DB::select('dialogID')
//            ->from('Frontend_Messages_Unread')
//            ->where('toUserID', '=', $user_id)
//            ->execute()
//            ->as_array();
//
//        return $query;
//    }

}