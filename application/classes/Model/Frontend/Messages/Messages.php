<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Frontend_Messages_Messages
 *
 * @author
 */
class Model_Frontend_Messages_Messages extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Messages';

    protected $_table_name = self::TABLE;

    protected $_has_many = [
        'locales' => [
            'model' => 'Frontend_Users_Locale',
            'foreign_key' => 'userID'
        ]
    ];

    public function rules(){
        $rules = [
            'message' => [
                ['not_empty']
            ]
        ];

        return $rules;
    }

    public static function addMessage($dialog_id, $message, $user_message_id) {
        $query = DB::insert(self::TABLE, ['dialogID', 'message', 'userMessageID'])
            ->values([$dialog_id, $message, $user_message_id])
            ->execute();

        return $query;
    }

    public static function addStatus($to_user_id, $dialog_id, $message_id){
        $query = DB::insert('Frontend_Messages_Unread', ['toUserID', 'dialogID', 'messageID'])
            ->values([$to_user_id, $dialog_id, $message_id])
            ->execute();

        return $query;
    }

    public static function getMessagesByDialog($id, $locale_id) {
        $query = DB::select(
                'FM.*',
                'FMD.toUserID',
                'FMD.theme',
                'FUL.name'
            )
            ->from([self::TABLE, 'FM'])

            ->join([Model_Frontend_User_Locale::TABLE, 'FUL'], 'LEFT')
            ->on('FUL.userID', '=', 'FM.userMessageID')
            ->on('FUL.localeID', '=', DB::expr($locale_id))

            ->join([Model_Frontend_Messages_Dialogs::TABLE, 'FMD'], 'LEFT')
            ->on('FMD.id', '=', 'FM.dialogID')

            ->where('FM.dialogID', '=', $id)
            ->and_where('FM.deleted', '=', 'false')
            ->order_by('FM.created', 'DESC')
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getLastMessage(){
        $query = DB::select('id')
            ->from(self::TABLE)
            ->order_by('id', 'DESC')
            ->limit('1')
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getUnreadMessages($user_id){
        $query = DB::select([DB::expr('COUNT(id)'), 'count'])
            ->from('Frontend_Messages_Unread')
            ->where('toUserID', '=', $user_id)
            ->execute()
            ->as_array();

        return $query[0]['count'];
    }

    public static function changeStatusMessage($id, $user_id) {
        $query = DB::insert('Frontend_Messages_Unread', ['dialogID', 'toUserID'])
            ->values([$id, $user_id])
            ->execute();

        return $query;
    }

    public static function delStatusMessage($id, $user_id) {
        $query = DB::delete('Frontend_Messages_Unread')
            ->where('dialogID', '=', $id)
            ->and_where('toUserID', '=', $user_id)
            ->execute();

        return $query;
    }

    public static function delMessage($id){
        $query = DB::update(self::TABLE)
            ->set(['deleted' => 'true'])
            ->where('ID', '=', $id)
            ->execute();

        return $query;
    }

}