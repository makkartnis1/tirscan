<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Currency
 *
 * @author Сергій Krid
 */
class Model_Frontend_Currency extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Currencies';

    protected $_table_name = self::TABLE;

}