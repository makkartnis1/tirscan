<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Locale
 *
 * @author Сергій Krid
 */
class Model_Frontend_Locale extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Locales';

    protected $_table_name = self::TABLE;

    public function rules() {
        return [
            'uri' => [
                [ 'not_empty' ],
                [ 'alpha' ],
                [ 'max_length', [':value', 3] ],
            ],
            'status' => [
                [ 'Model_Frontend_Validation::in_array', [ ':value', [ 'active', 'unactive' ] ] ]
            ],
            'title' => [
                [ 'not_empty' ]
            ]
        ];
    }

}