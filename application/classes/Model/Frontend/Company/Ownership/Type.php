<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Company_Ownership_Type
 *
 * @author Сергій Krid
 */
class Model_Frontend_Company_Ownership_Type extends Model_Frontend_ORM {

    protected static $_types = null;

    const TABLE = 'Frontend_Company_Ownership_Types';

    protected $_table_name = self::TABLE;

    protected $_has_many = [
        'companies' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'typeID'
        ]
    ];

    public static function getTypes(){
        if (self::$_types == null) {
            $types = DB::select()->from(self::TABLE)->execute()->as_array();

            $result = [];
            foreach ($types as $type) {
                if (__db($type['localeName']) == '') {
                    continue;
                }
                else {
                    $type['localeName'] = __db($type['localeName']);
                }

                $result[] = $type;
            }

            self::$_types = $result;
        }

        return self::$_types;

    }

}