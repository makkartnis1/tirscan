<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Company_Ownership_Type
 *
 */
class Model_Frontend_Company_Ownership_CompanyOwnership extends Model_Frontend_ORM {

    protected static $_types = null;

    const TABLE = 'Frontend_User_to_Company_Ownership';

    protected $_table_name = self::TABLE;

    protected $_has_one =[
        'companies' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'ID'
        ],

        'users' => [
            'model' => 'Frontend_Users',
            'foreign_key' => 'ID'
        ]
    ];

}