<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Company_Locale
 *
 * @author Сергій Krid
 */
class Model_Frontend_Company_Locale extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Company_Locales';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'company' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'companyID'
        ],
        'locale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'localeID'
        ]
    ];

    public function rules() {
        return [
            'name' => [
//                [ 'not_empty' ],
                [ 'min_length', [':value', 4] ],
                [ 'max_length', [':value', 100] ],
            ],
            'address' => [
//                [ 'not_empty' ],
            ]
        ];
    }

    public function filters() {
        
        return [
            'name' => [
                [ 'trim' ],
                [ 'Model_Frontend_Filters::null_if_empty_string', [':value'] ],
            ],
            'address' => [
                [ 'trim' ],
                [ 'Model_Frontend_Filters::null_if_empty_string', [':value'] ],
            ],
            'info' => [
                [ 'trim' ],
                [ 'Model_Frontend_Filters::null_if_empty_string', [':value'] ],
            ],
            'realAddress' => [
                [ 'trim' ],
                [ 'Model_Frontend_Filters::null_if_empty_string', [':value'] ],
            ],
        ];
        
    }

}