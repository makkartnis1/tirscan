<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Geo_Locales
 *
 * @author Сергій Krid
 */
class Model_Frontend_Geo_Locale extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Geo_Locales';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'geo' => [
            'model' => 'Frontend_Geo',
            'foreign_key' => 'geoID'
        ],
        'locale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'localeID'
        ]
    ];

}