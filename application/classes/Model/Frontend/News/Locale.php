<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_News_Locale
 *
 * @author
 */
class Model_Frontend_News_Locale extends Model_Frontend_ORM {

    const TABLE = 'Frontend_News_Locales';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'news' => [
            'model' => 'Frontend_News_NewsSite',
            'foreign_key' => 'ID'
        ]
    ];

    public function rules()
    {
        $rules = [
            'title' => [
                ['not_empty']
            ],

            'preview' => [
                ['not_empty']
            ],

            'content' => [
                ['not_empty']
            ]
        ];

        return $rules;
    }
}