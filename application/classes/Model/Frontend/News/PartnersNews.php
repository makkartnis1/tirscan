<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Frontend_News_PartnersNews
 *
 * @author
 */
class Model_Frontend_News_PartnersNews extends Model_Frontend_ORM
{
    const TABLE = 'Frontend_Subscription';

    protected $_table_name = self::TABLE;

    public static function getCompanyISigned($id){
        $query = DB::select(
            'companyID'
        )
            ->from(self::TABLE)
            ->where('userID', '=', $id)
            ->execute()
            ->as_array();

        return $query;
    }


}
