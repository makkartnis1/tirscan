<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Frontend_News_NewsSite
 *
 * @author
 */
class Model_Frontend_News_NewsSite extends Model_Frontend_ORM
{
    const TABLE = 'Frontend_News';

    protected $_table_name = self::TABLE;

    protected $_has_many = [
        'locales' => [
            'model' => 'Frontend_News_Locale',
            'foreign_key' => 'newsID'
        ]
    ];

    public function rules(){
        $rules = [
            'userID' => [
                [ 'not_empty' ]
            ]
        ];

        return $rules;
    }


    public static function getLastNews($limit, $localeID){
        $query = DB::select(
                'FN.ID',
                'FN.createDate',
                'FN.companyID',
                'FN.userID',
                'FN.url',
                'FNL.title',
                'FNL.preview',
                'FN.image'
            )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))
            ->order_by('createDate', 'DESC')
            ->where('FN.companyID', '=', null)
            ->where('FN.userID', '=', null)
            ->where('FNL.localeID', '=', $localeID)
            ->limit($limit)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getAllNews($localeID, $limit, $offset){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.url',
            'FNL.title',
            'FNL.preview',
            'FNL.content'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('createDate', 'DESC')
            ->where('FN.companyID', '=', null)
            ->where('FN.userID', '=', null)
            ->where('FNL.localeID', '=', $localeID)
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function countNews(){
        $query = DB::select('companyID', 'userID', [DB::expr('COUNT(ID)'), 'count'])
            ->from(self::TABLE)
            ->where('companyID', '=', null)
            ->where('userID', '=', null)
            ->execute()
            ->as_array();

        return $query[0]['count'];
    }

    public static function countCompanyNews($company_id){
        $query = DB::select([DB::expr('COUNT(ID)'), 'count'])
            ->from(self::TABLE)
            ->where('companyID', '=', $company_id)
            ->execute()
            ->as_array();

        return $query[0]['count'];
    }

    public static function getOneNewsById($id, $localeID){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.url',
            'FN.image',
            'FNL.title',
            'FNL.preview',
            'FNL.content'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('createDate', 'DESC')
            ->where('FN.ID', '=', $id)
            ->and_where('FNL.localeID', '=', $localeID)
            ->execute()
            ->as_array();

        return $query[0];
    }

    public static function getLastNewsByCompanyId($limit, $id, $localeID){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.image',
            'FN.url',
            'FN.companyID',
            'FNL.title'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('FN.createDate', 'DESC')
            ->where('FN.companyID', '=', $id)
            ->and_where('FNL.localeID', '=', $localeID)
            ->limit($limit)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getNewsByCompanyId($id, $localeID, $limit, $offset){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.url',
            'FN.companyID',
            'FNL.title',
            'FNL.preview'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('FN.createDate', 'DESC')
            ->where('FN.companyID', '=', $id)
            ->and_where('FNL.localeID', '=', $localeID)
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getMyNews($id, $localeID){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.url',
            'FN.companyID',
            'FNL.title',
            'FNL.preview'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('FN.createDate', 'DESC')
            ->where('FN.userID', '=', $id)
            ->and_where('FNL.localeID', '=', $localeID)
//            ->limit($limit)
//            ->offset($offset)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function getMyOneNews($user_id, $news_id, $localeID){
        $query = DB::select(
            'FN.ID',
            'FN.createDate',
            'FN.url',
            'FN.companyID',
            'FNL.title',
            'FNL.preview',
            'FNL.content'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->where('FN.userID', '=', $user_id)
            ->and_where('FN.ID', '=', $news_id)
            ->and_where('FNL.localeID', '=', $localeID)
            ->order_by('FN.createDate', 'DESC')
            ->execute()
            ->as_array();

        return $query[0];
    }

    public static function countCompanyNewsISigned($array, $localeID){
        $query = DB::select([DB::expr('COUNT(FNL.ID)'), 'count'])
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->where('FN.companyID', 'IN', $array)
            ->execute()
            ->as_array();

        return $query[0]['count'];
    }


    public static function getCompanyNewsISigned($array, $localeID) {
        $query = DB::select(
            'FN.ID',
            'FN.companyID',
            'FN.createDate',
            'FN.url',
            'FNL.title',
            'FNL.preview'
        )
            ->from([self::TABLE, 'FN'])

            ->join([Model_Frontend_News_Locale::TABLE, 'FNL'], 'LEFT')
            ->on('FNL.newsID', '=', 'FN.ID')
            ->on('FNL.localeID', '=', DB::expr($localeID))

            ->order_by('FN.createDate', 'DESC')
            ->where('FN.companyID','IN', $array)
            ->and_where('FNL.localeID', '=', $localeID);

        return $query;
    }

    public static function updateNews($id, $data){

        DB::update(Model_Frontend_News_Locale::TABLE)
            ->set($data)
            ->where('newsID', '=', $id)
            ->execute();
    }
}