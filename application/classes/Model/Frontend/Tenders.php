<?php defined('SYSPATH') or die('No direct script access.');


class Model_Frontend_Tenders extends Model {

    const TABLE = 'Frontend_Tenders';
    const TABLE_TO_GEO = 'Frontend_Tenders_to_Geo_Relation';
    const TABLE_TO_BIDS = 'Frontend_User_to_Tender_Requests';

    public $t_tenders = 'Frontend_Tenders';
    public $t_tenders_geo = 'Frontend_Tenders_to_Geo_Relation';
    public $t_tenders_requests = 'Frontend_User_to_Tender_Requests';
    public $t_tenders_auto_requests = 'Frontend_User_to_Tender_Auto_Requests';
    public $t_geo_locales = 'Frontend_Geo_Locales';
    public $t_geo = 'Frontend_Geo';
    public $t_user_locales = 'Frontend_User_Locales';
    public $t_users = 'Frontend_Users';
    public $t_phone_codes = 'Frontend_Phonecodes';
    public $t_company_locales = 'Frontend_Company_Locales';
    public $t_car_types = 'Frontend_User_Car_Types';
    public $t_currencies = 'Frontend_Currencies';

    public static function query_getCargoCount(array $filters = []) {
        $query = DB::select([DB::expr('COUNT(DISTINCT(t_tender.ID))'), 'count'])
            ->from([self::TABLE, 't_tender'])

            // join користувача
            ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
            ->on('t_tender.userID', '=', 't_user.ID')

            // join компанії
            ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
            ->on('t_user.companyID', '=', 't_comp.ID')

            // join geo
            ->join([self::TABLE_TO_GEO, 't_tender_to_geo_from'], 'INNER')
            ->on('t_tender.ID', '=', 't_tender_to_geo_from.requestID')
            ->on('t_tender_to_geo_from.type', '=', DB::expr('"from"'))
            ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
            ->on('t_tender_to_geo_from.geoID', '=', 't_geo_from.ID')
            ->join([self::TABLE_TO_GEO, 't_tender_to_geo_to'], 'INNER')
            ->on('t_tender.ID', '=', 't_tender_to_geo_to.requestID')
            ->on('t_tender_to_geo_to.type', '=', DB::expr('"to"'))
            ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
            ->on('t_tender_to_geo_to.geoID', '=', 't_geo_to.ID')

            ->where_open()
            ->where('t_tender.status', '=', 'inactive')
            ->or_where('t_tender.status', '=', 'active')
            ->or_where('t_tender.status', '=', 'stopped')
            ->where_close();

        // додаємо фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        $result = $query
            ->execute()
            ->as_array();

        return $result[0]['count'];
    }

    public static function query_getPlaces($locale_id, $request_id, $type) {
        $locale_id = 1; // TODO HOTFIX не вставляє переклади на інших мовах ЗАГЛУШКА

        $query = DB::select('geoID')
            ->from(self::TABLE_TO_GEO)
            ->where('requestID', '=', $request_id)
            ->and_where('type', '=', $type);

        $geo_result = DB::select('ID')
            ->from(Model_Frontend_Geo::TABLE)
            ->where('ID', 'IN', $query)
            ->order_by('level', 'DESC')
            ->limit(1)
            ->execute()
            ->as_array();

        foreach ($geo_result as &$result) {
            $result = $result['ID'];
            unset($result);
        }

        $query_geo_locale = DB::select(['fullAddress', $type . '_' . 'fullAddress'], ['name', $type . '_' . 'name'], ['country', $type . '_' . 'country' ])
            ->from(Model_Frontend_Geo_Locale::TABLE)
            ->where('geoID', 'IN', $geo_result)
            ->and_where('localeID', '=', $locale_id)
            ->execute()
            ->as_array();

        // TODO: переклади якщо не існує

        $query_geo = DB::select(['placeID', $type . '_' . 'placeID'])
            ->from(Model_Frontend_Geo::TABLE)
            ->where('ID', 'IN', $geo_result)
            ->execute()
            ->as_array();


        return array_merge($query_geo_locale[0], $query_geo[0]);
    }

    public static function query_getTenders($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = DB::select(
            ['t_tender.ID', 'ID'],

            't_tender.created',
            'dateFromLoad',
            'dateToLoad',
            't_user.companyID',

            't_tender.userID',
            't_tender.cargoType',
            't_tender.carCount',
            't_tender.volume',
            't_tender.weight',
            't_tender.sizeX',
            't_tender.sizeY',
            't_tender.sizeZ',
            't_tender.price',
            't_tender.priceHide',
            ['t_curr.code', 'priceCurrency'],
            't_tender.datetimeFromTender',
            't_tender.datetimeToTender',
            't_tender.status',

            // інформація про користувача і компанію (вибираємо існуючі переклади)
            [DB::expr('IFNULL(`t_user_loc`.`name`, `t_user_loc_prim`.`name`)'), 'username'], // ім'я користувача
            [DB::expr('IFNULL(`t_comp_loc`.`name`, `t_comp_loc_prim`.`name`)'), 'company_name'], // назва компанії
            ['t_ph_codes.code', 'phoneCode'],
            't_user.phone',

            // інформація про авто
            ['t_user_car_type.localeName', 'car_type_localeName']
        )
            ->from([self::TABLE, 't_tender'])
            // join машини і її типу
            ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
            ->on('t_tender.transportTypeID', '=', 't_user_car_type.ID')

            // join користувача
            ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
            ->on('t_tender.userID', '=', 't_user.ID')

            // join таблиці телефонних кодів
            ->join([Model_Frontend_Phonecodes::TABLE, 't_ph_codes'], 'LEFT')
            ->on('t_user.phoneCodeID', '=', 't_ph_codes.id')

            // join таблиці валют
            ->join([Model_Frontend_Currency::TABLE, 't_curr'], 'LEFT')
            ->on('t_tender.currencyID', '=', 't_curr.ID')

            // join локалей користувача (на поточній мові і мові реєстрації)
            ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc'], 'LEFT')
            ->on('t_user.ID', '=', 't_user_loc.userID')
            ->on('t_user_loc.localeID', '=', DB::expr($localeID))
            ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc_prim'], 'LEFT')
            ->on('t_user.ID', '=', 't_user_loc_prim.userID')
            ->on('t_user.primaryLocaleID', '=', 't_user_loc_prim.localeID')

            // join компанії
            ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
            ->on('t_user.companyID', '=', 't_comp.ID')

            // join локалей компанії (на поточній мові і мові реєстрації)
            ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc'], 'LEFT')
            ->on('t_comp.ID', '=', 't_comp_loc.companyID')
            ->on('t_comp_loc.localeID', '=', DB::expr($localeID))
            ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc_prim'], 'LEFT')
            ->on('t_comp.ID', '=', 't_comp_loc_prim.companyID')
            ->on('t_comp.primaryLocaleID', '=', 't_comp_loc_prim.localeID')

            // join geo
            ->join([self::TABLE_TO_GEO, 't_tender_to_geo_from'], 'INNER')
            ->on('t_tender.ID', '=', 't_tender_to_geo_from.requestID')
            ->on('t_tender_to_geo_from.type', '=', DB::expr('"from"'))
            ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
            ->on('t_tender_to_geo_from.geoID', '=', 't_geo_from.ID')
            ->join([self::TABLE_TO_GEO, 't_tender_to_geo_to'], 'INNER')
            ->on('t_tender.ID', '=', 't_tender_to_geo_to.requestID')
            ->on('t_tender_to_geo_to.type', '=', DB::expr('"to"'))
            ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
            ->on('t_tender_to_geo_to.geoID', '=', 't_geo_to.ID')

            ->where_open()
            ->where('t_tender.status', '=', 'inactive')
            ->or_where('t_tender.status', '=', 'active')
            ->or_where('t_tender.status', '=', 'stopped')
            ->where_close()

            ->group_by('t_tender.ID')
            ->order_by('t_tender.ID', 'DESC');


        // додаєм фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        $result_tender = $query
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        $tender_ids = Arr::pluck($result_tender, 'ID');
        $bid_count_ = [];
        if (count($tender_ids) > 0) {
            $bid_count = DB::select([DB::expr('COUNT(t_bids.tenderID)'), 'count'], 't_bids.tenderID')
                           ->from([Model_Frontend_Tenders::TABLE_TO_BIDS, 't_bids'])
                           ->where('t_bids.tenderID', 'IN', $tender_ids)
                           ->group_by('t_bids.tenderID')
                           ->execute()
                           ->as_array();

            foreach ($bid_count as $bid) {
                $bid_count_[$bid['tenderID']] = $bid['count'];
            }
        }

        foreach ($result_tender as &$tender) {
            $places_from = self::query_getPlaces($localeID, $tender['ID'], 'from');
            $places_to = self::query_getPlaces($localeID, $tender['ID'], 'to');

            $tender['bid_count'] = (int) Arr::get($bid_count_, $tender['ID'], 0);

            $tender = array_merge($tender, $places_from, $places_to);
            unset($tender);
        }

        return $result_tender;
    }

    public function validate($data)
    {
        $errors = [];

        if (Helper_Valid::is_empty($data['dateFromLoad']))
            $errors['dateFromLoad'] = __db('tenders.validate.load_date.from.empty');

        if (Helper_Valid::is_empty($data['dateToLoad']))
            $errors['dateToLoad'] = __db('tenders.validate.load_date.to.empty');

        if (!array_key_exists('dateFromLoad',$errors) and !array_key_exists('dateToLoad',$errors))
        {
            if (!Helper_Valid::interval_date($data['dateFromLoad'],$data['dateToLoad'],true))
            {
                $errors['dateFromLoad'] = __db('tenders.validate.load_date.from.incorrect');
            }
        }

        if (empty($data['load_place']))
            $errors['startPointTender'] = __db('tenders.validate.place.from.empty');


        if (empty($data['unload_place']))
            $errors['endPointTender'] = __db('tenders.validate.place.to.empty');


        if (Helper_Valid::is_empty($data['weightCargo']) and Helper_Valid::is_empty($data['volumeCargo']))
        {
            $errors['weightVolumeCargo'] = __db('tenders.validate.weight_cargo.empty');
        }

        if (!array_key_exists('weightVolumeCargo',$errors))
        {
            if (!Helper_Valid::is_empty($data['weightCargo']))
            {
                if (is_numeric($data['weightCargo']))
                {
                    if ($data['weightCargo'] <= 0)
                        $errors['weightCargo'] = __db('tenders.validate.weight_cargo.number');
                }
                else
                {
                    $errors['weightCargo'] = __db('tenders.validate.weight_cargo.number');
                }

            }

            if (!Helper_Valid::is_empty($data['volumeCargo']))
            {
                if (is_numeric($data['volumeCargo']))
                {
                    if ($data['volumeCargo'] <= 0)
                        $errors['volumeCargo'] = __db('tenders.validate.volume_cargo.number');
                }
                else
                {
                    $errors['volumeCargo'] = __db('tenders.validate.volume_cargo.number');
                }

            }

        }


        if (Helper_Valid::is_empty($data['dateFromTender']))
            $errors['dateFromTender'] = __db('tenders.validate.date_tender.from.empty');


        if (Helper_Valid::is_empty($data['dateToTender']))
            $errors['dateToTender'] = __db('tenders.validate.date_tender.to.empty');

        if (!array_key_exists('dateFromTender',$errors) and !array_key_exists('dateToTender',$errors))
        {
            if (!Helper_Valid::interval_date($data['dateFromTender'].' '.$data['timeFromTender'],$data['dateToTender'].' '.$data['timeToTender']))
            {
                $errors['dateFromTender'] = __db('tenders.validate.date_tender.from.incorrect');
            }
        }


        if (is_numeric($data['startPrice']))
        {
            if ($data['startPrice'] <= 0)
                $errors['startPrice'] = __db('tenders.validate.price.incorrect');
        }
        else
        {
            $errors['startPrice'] = __db('tenders.validate.price.incorrect');
        }

        if (!isset($data['rules']))
        {
            $errors['rules'] = __db('tenders.validate.rules');
        }

        return $errors;
    }

    public function filter_for_edit($data)
    {
        $data['dateFromLoad'] = date("d.m.Y",strtotime($data['info']['dateFromLoad']));
        $data['dateToLoad'] = date("d.m.Y",strtotime($data['info']['dateToLoad']));

        $data['dateFromTender'] = date("d.m.Y",strtotime($data['info']['datetimeFromTender']));
        $data['dateToTender'] = date("d.m.Y",strtotime($data['info']['datetimeToTender']));

        $data['timeFromTender'] = date("H:i",strtotime($data['info']['datetimeFromTender']));
        $data['timeToTender'] = date("H:i",strtotime($data['info']['datetimeToTender']));

        $data['loads'] = $data['place_from'];
        $data['unloads'] = $data['place_to'];
        $data['typeCargo'] = $data['info']['cargoType'];
        $data['carType'] = $data['info']['transportTypeID'];
        $data['weightCargo'] = $data['info']['weight'];
        $data['volumeCargo'] = $data['info']['volume'];
        $data['truckQnt'] = $data['info']['carCount'];
        $data['carLength'] = $data['info']['sizeX'];
        $data['carWidth'] = $data['info']['sizeY'];
        $data['carHeight'] = $data['info']['sizeZ'];

        $data['doc_TIR'] = ($data['info']['docTIR']=='yes')?1:'';
        $data['doc_CMR'] = ($data['info']['docCMR']=='yes')?1:'';
        $data['doc_T1'] = ($data['info']['docT1']=='yes')?1:'';
        $data['doc_sanpassport'] = ($data['info']['docSanPassport']=='yes')?1:'';
        $data['doc_sanbook'] = ($data['info']['docSanBook']=='yes')?1:'';
        $data['load_side'] = ($data['info']['loadFromSide']=='yes')?1:'';
        $data['load_top'] = ($data['info']['loadFromTop']=='yes')?1:'';
        $data['load_behind'] = ($data['info']['loadFromBehind']=='yes')?1:'';
        $data['load_tent'] = ($data['info']['loadTent']=='yes')?1:'';
        $data['cond_plomb'] = ($data['info']['condPlomb']=='yes')?1:'';
        $data['cond_reload'] = ($data['info']['condLoad']=='yes')?1:'';
        $data['cond_belts'] = ($data['info']['condBelts']=='yes')?1:'';
        $data['cond_removable_stands'] = ($data['info']['condRemovableStands']=='yes')?1:'';
        $data['cond_bort'] = ($data['info']['condHardSide']=='yes')?1:'';
        $data['cond_collectable'] = ($data['info']['condCollectableCargo']=='yes')?1:'';

        $data['startPrice'] = $data['info']['price'];
        $data['currency'] = $data['info']['currencyID'];

        $data['priceHide'] = ($data['info']['priceHide']=='yes')?1:'';

        $data['companyYears'] = $data['info']['filterCompanyYears'];
        $data['filterCompanyYears'] = ($data['info']['filterCompany']=='yes')?1:'';
        $data['filterForwarder'] = ($data['info']['filterForwarder']=='yes')?1:'';
        $data['filterPartners'] = ($data['info']['filterPartners']=='yes')?1:'';

        $data['t_amount'] = $data['info']['condTemperature'];
        $data['pallets_amount'] = $data['info']['condPalets'];
        $data['ADR_amount'] = $data['info']['condADR'];
        $data['info'] = $data['info']['info'];

        return $data;
    }

    public function filter($data)
    {
        $date_filter = [
            'dateFromLoad',
            'dateToLoad',
            'dateFromTender',
            'dateToTender',
        ];

        $trim_filter = [
            'typeCargo',
            'weightCargo',
            'volumeCargo',
            'carLength',
            'carWidth',
            'carHeight',
            't_amount',
            'pallets_amount',
            'ADR_amount',
        ];


        $null_filter = [
            'typeCargo',
            'carType',
            'weightCargo',
            'volumeCargo',
            'carLength',
            'carWidth',
            'carHeight',
            't_amount',
            'pallets_amount',
            'ADR_amount',
        ];

        $checkbox_filter = [
            'priceHide',
            'filterCompanyYears',
            'filterForwarder',
            'filterPartners',
            'doc_TIR',
            'doc_CMR',
            'doc_T1',
            'doc_sanpassport',
            'doc_sanbook',
            'load_side',
            'load_top',
            'load_behind',
            'load_tent',
            'cond_plomb',
            'cond_reload',
            'cond_belts',
            'cond_removable_stands',
            'cond_bort',
            'cond_collectable',
            't',
            'pallets',
            'ADR',
        ];


        foreach($trim_filter as $form_element)
        {
            $data[$form_element] = trim($data[$form_element]);
        }

        foreach($null_filter as $form_element)
        {
            $data[$form_element] = ( $data[$form_element] ) ? $data[$form_element] : NULL;
        }

        foreach($date_filter as $form_element)
        {
            $data[$form_element] = Helper_Filter::date($data[$form_element]);
        }

        foreach($checkbox_filter as $form_element)
        {
            $data[$form_element] = ( isset($data[$form_element]) ) ? "yes" : "no" ;
        }

        $data['truckQnt'] = (int) $data['truckQnt'];
        $data['truckQnt'] = ( $data['truckQnt'] <= 0 ) ? NULL : $data['truckQnt'] ;

        $data['startPrice'] = (float) $data['startPrice'];
        $data['startPrice'] = ( $data['startPrice'] <= 0 ) ? NULL : $data['startPrice'] ;

        if ($data['t'])
        {
            if ($data['t_amount'])
            {
                $data['t_amount'] = (int) $data['t_amount'];
            }
        }

        if ($data['pallets'])
        {
            if ($data['pallets_amount'])
            {
                $data['pallets_amount'] = (int) $data['pallets_amount'];
                $data['pallets_amount'] = ( $data['pallets_amount'] <= 0 ) ? 1 : $data['pallets_amount'] ;
            }
        }

        if ($data['ADR'])
        {
            if ($data['ADR_amount'])
            {
                $data['ADR_amount'] = (int) $data['ADR_amount'];
                $data['ADR_amount'] = ( $data['ADR_amount'] <= 0 ) ? 1 : $data['ADR_amount'] ;
            }
        }

        return $data;
    }

    public function update($data,$id)
    {
        $upd_status = 0;

        $db= Database::instance();
        $db->begin();
        try
        {

            DB::update($this->t_tenders)
                ->set(array(
                    'dateFromLoad'=>$data['dateFromLoad'],
                    'dateToLoad'=>$data['dateToLoad'],
                    'datetimeFromTender'=>$data['dateFromTender'].' '.$data['timeFromTender'],
                    'datetimeToTender'=>$data['dateToTender'].' '.$data['timeToTender'],
                    'info'=>$data['info'],
                    'cargoType'=>$data['typeCargo'],
                    'transportTypeID'=>$data['carType'],
                    'volume'=>$data['volumeCargo'],
                    'weight'=>$data['weightCargo'],
                    'carCount'=>$data['truckQnt'],
                    'sizeX'=>$data['carLength'],
                    'sizeY'=>$data['carWidth'],
                    'sizeZ'=>$data['carHeight'],
                    'docTIR'=>$data['doc_TIR'],
                    'docCMR'=>$data['doc_CMR'],
                    'docT1'=>$data['doc_T1'],
                    'docSanPassport'=>$data['doc_sanpassport'],
                    'docSanBook'=>$data['doc_sanbook'],
                    'loadFromSide'=>$data['load_side'],
                    'loadFromTop'=>$data['load_top'],
                    'loadFromBehind'=>$data['load_behind'],
                    'loadTent'=>$data['load_tent'],
                    'condPlomb'=>$data['cond_plomb'],
                    'condLoad'=>$data['cond_reload'],
                    'condBelts'=>$data['cond_belts'],
                    'condRemovableStands'=>$data['cond_removable_stands'],
                    'condHardSide'=>$data['cond_bort'],
                    'condCollectableCargo'=>$data['cond_collectable'],
                    'price'=>$data['startPrice'],
                    'currencyID'=>$data['currency'],
                    'priceHide'=>$data['priceHide'],
                    'filterCompanyYears'=>$data['companyYears'],
                    'filterCompany'=>$data['filterCompanyYears'],
                    'filterForwarder'=>$data['filterForwarder'],
                    'filterPartners'=>$data['filterPartners'],
                    'condTemperature'=>$data['t_amount'],
                    'condPalets'=>$data['pallets_amount'],
                    'condADR'=>$data['ADR_amount']
                ))
                ->where('ID','=',$id)
                ->execute();

            $this->create_geo_relation($data['load_place'],$data['unload_place'],$id);

            $db->commit();

            $upd_status = 1;
        }
        catch (Database_Exception $e)
        {
            $db->rollback();
        }

        $this->updateStatusTender($id);

        return $upd_status;

    }

    public function create($data,$user_id)
    {
        $tender_id = false;

        $db= Database::instance();
        $db->begin();
        try
        {
            $res_tender = DB::insert($this->t_tenders,
                array(
                    'dateFromLoad',
                    'dateToLoad',
                    'datetimeFromTender',
                    'datetimeToTender',
                    'info',
                    'cargoType',
                    'transportTypeID',
                    'volume',
                    'weight',
                    'carCount',
                    'sizeX',
                    'sizeY',
                    'sizeZ',
                    'userID',
                    'docTIR',
                    'docCMR',
                    'docT1',
                    'docSanPassport',
                    'docSanBook',
                    'loadFromSide',
                    'loadFromTop',
                    'loadFromBehind',
                    'loadTent',
                    'condPlomb',
                    'condLoad',
                    'condBelts',
                    'condRemovableStands',
                    'condHardSide',
                    'condCollectableCargo',
                    'price',
                    'currencyID',
                    'priceHide',
                    'filterCompanyYears',
                    'filterCompany',
                    'filterForwarder',
                    'filterPartners',
                    'condTemperature',
                    'condPalets',
                    'condADR'
                )
            )
                ->values(array(
                    $data['dateFromLoad'],
                    $data['dateToLoad'],
                    $data['dateFromTender'].' '.$data['timeFromTender'],
                    $data['dateToTender'].' '.$data['timeToTender'],
                    $data['info'],
                    $data['typeCargo'],
                    $data['carType'],
                    $data['volumeCargo'],
                    $data['weightCargo'],
                    $data['truckQnt'],
                    $data['carLength'],
                    $data['carWidth'],
                    $data['carHeight'],
                    $user_id,
                    $data['doc_TIR'],
                    $data['doc_CMR'],
                    $data['doc_T1'],
                    $data['doc_sanpassport'],
                    $data['doc_sanbook'],
                    $data['load_side'],
                    $data['load_top'],
                    $data['load_behind'],
                    $data['load_tent'],
                    $data['cond_plomb'],
                    $data['cond_reload'],
                    $data['cond_belts'],
                    $data['cond_removable_stands'],
                    $data['cond_bort'],
                    $data['cond_collectable'],
                    $data['startPrice'],
                    $data['currency'],
                    $data['priceHide'],
                    $data['companyYears'],
                    $data['filterCompanyYears'],
                    $data['filterForwarder'],
                    $data['filterPartners'],
                    $data['t_amount'],
                    $data['pallets_amount'],
                    $data['ADR_amount']
                ))
                ->execute();

            $tender_id = $res_tender[0];

            $this->create_geo_relation($data['load_place'],$data['unload_place'],$tender_id);

            $db->commit();
        }
        catch (Database_Exception $e)
        {
            $db->rollback();
        }

        $this->updateStatusTender($tender_id);
        return $tender_id;
    }

    private function create_geo_relation($places_from,$places_to,$id)
    {
        //очищення старих звязків
        DB::delete($this->t_tenders_geo)
            ->where('requestID','=',$id)
            ->execute();

        //створення нових звязків
        $i = 1;
        foreach($places_from as $p_f)
        {
            $exp = explode(',',$p_f);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->t_tenders_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,(int)$e,'from',$i))
                    ->execute();
            }

            $i++;
        }

        foreach($places_to as $p_t)
        {
            $exp = explode(',',$p_t);
            if (empty($exp)) continue;

            foreach($exp as $e)
            {
                DB::insert($this->t_tenders_geo,array('requestID','geoID','type','groupNumber'))
                    ->values(array($id,(int)$e,'to',$i))
                    ->execute();
            }

            $i++;
        }
    }


    public function auto_bet($tender_id,$user_id,$auto_bet)
    {

        $auto_bet = (int) $auto_bet;

        //перевірка чи автоматична ставка є додатнім числом
        if ($auto_bet <= 0) return __db('tenders.auto_rate.incorrect');

        //перевірка чи автоматична ставка не перевищує рекомендовану ціну
        $check_auto_rate = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_tenders)
            ->where('ID','=',$tender_id)
            ->where('price','<=',$auto_bet)
            ->execute()
            ->as_array();

        $check_auto_rate = $check_auto_rate[0]['c'];

        if ($check_auto_rate > 0) return __db('tenders.auto_rate.incorrect');

        //перевірка чи автоматична ставка є мінімальною
        $check_best = DB::select(DB::expr('count(ID) as c'))
                    ->from($this->t_tenders_auto_requests)
                    ->where('tenderID','=',$tender_id)
                    ->where('price','<=',$auto_bet)
                    ->execute()
                    ->as_array();

        if ($check_best[0]['c'] > 0) return 'Автоставка не є найкращою';

        //вибір автоматичної ставки
        $auto_rate_info = DB::select('ID')
            ->from($this->t_tenders_auto_requests)
            ->where('tenderID','=',$tender_id)
            ->where('userID','=',$user_id)
            ->execute()
            ->as_array();



        if (empty($auto_rate_info))
        {
            DB::insert($this->t_tenders_auto_requests,array('userID','tenderID','price'))
                ->values(array($user_id,$tender_id,$auto_bet))
                ->execute();
        }
        else
        {
            DB::update($this->t_tenders_auto_requests)
                ->set(array(
                    'price'=>$auto_bet,
                    'updated'=>date("Y-m-d H:i:s")
                ))
                ->where('tenderID','=',$tender_id)
                ->where('userID','=',$user_id)
                ->execute();
        }
    }


    private function robot($tender_id,$user_id,$cur_bet)
    {

        $robots = DB::select('price','userID')
                    ->from($this->t_tenders_auto_requests)
                    ->where('tenderID','=',$tender_id)
                    ->where('userID','!=',$user_id)
                    ->where('price','<',$cur_bet)
                    ->order_by('price','desc')
                    ->limit(2)
                    ->execute()
                    ->as_array();

        if (empty($robots)) return;


        $req = DB::select('requestNumber')
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$tender_id)
            ->order_by('ID','desc')
            ->execute()
            ->as_array();

        $req_number = '';
        if (!empty($req)) $req_number = $req[0]['requestNumber'];
        if ($req_number !== '') $req_number++;

        $next_bet = $cur_bet - 1;
        if (count($robots) > 1)
        {
            //якщо роботів більше одного
            $next_bet = $robots[0]['price'];
        }

        DB::insert($this->t_tenders_requests,array('userID','tenderID','customPrice','requestNumber'))
            ->values(array($robots[0]['userID'],$tender_id,$next_bet,$req_number))
            ->execute();

        $this->robot($tender_id,$robots[0]['userID'],$next_bet);
    }

    public function hidden_bet($tender_id,$user_id,$custom_price)
    {
        $status = false;

        $req_number = '';

        $req = DB::select('requestNumber')
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$tender_id)
            ->order_by('ID','desc')
            ->execute()
            ->as_array();

        if (!empty($req)) $req_number = $req[0]['requestNumber'];
        if ($req_number !== '') $req_number++;


        $db= Database::instance();
        $db->begin();
        try
        {
            DB::insert($this->t_tenders_requests,array('userID','tenderID','customPrice','requestNumber'))
                ->values(array($user_id,$tender_id,$custom_price,$req_number))
                ->execute();

            $db->commit();

            $status = true;

        }
        catch (Database_Exception $e)
        {
            $db->rollback();
            $status = false;
        }

        if ($status)
        {
            $this->robot($tender_id,$user_id,$custom_price);
        }

        return $status;

    }

    public function hidden_bet_valid($tender_price,$custom_price,$user_id,$tender_id)
    {
        $custom_price = (int) $custom_price;

        if ($custom_price <= 0) return __db('tenders.bet.incorrect');

        if (!$user_id) return __db('tenders.bet.incorrect');

        $req = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$tender_id)
            ->where('customPrice','<=',$custom_price)
            ->execute()
            ->as_array();

        if ($req[0]['c'] > 0) return __db('tenders.bet.not_best');

        if ($custom_price > $tender_price) return __db('tenders.bet.higher');

        return null;
    }

    public function get_one($id,$user_id)
    {
        $this->updateStatusTender($id);

        //вибір загальної інформації про тендер
        $tender = DB::select($this->t_tenders.'.*',$this->t_car_types.'.name',$this->t_currencies.'.code')
            ->from($this->t_tenders)
            ->join($this->t_car_types,'left')->on($this->t_tenders.'.transportTypeID','=',$this->t_car_types.'.ID')
            ->join($this->t_currencies,'left')->on($this->t_tenders.'.currencyID','=',$this->t_currencies.'.ID')
            ->where($this->t_tenders.'.ID','=',$id)
            ->where($this->t_tenders.'.status','!=','deleted')
            ->execute()
            ->as_array();

        if (empty($tender)) return null;
        $tender = $tender[0];

        //вибір кількості ставок
        $count_requests = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$id)
            ->execute()
            ->as_array();

        $count_requests = $count_requests[0]['c'];

        //вибір місця завантаження
        $place_from = $this->get_places($id,'from');

        //вибір місця розвантаження
        $place_to = $this->get_places($id,'to');

        //вибір імені користувача
        $user_info = DB::select(
            $this->t_users.'.companyID',
            $this->t_users.'.phone',
            $this->t_users.'.phoneStationary',
            array('ph1.code','phone_code'),
            array('ph2.code','phone_stat_code'),
            array(DB::expr("(select name from ".$this->t_user_locales." where userID = ".$tender['userID']." limit 1)"),'user_name')
        )
            ->from($this->t_users)
            ->join(array($this->t_phone_codes,'ph1'),'left')->on('ph1.ID','=',$this->t_users.'.phoneCodeID')
            ->join(array($this->t_phone_codes,'ph2'),'left')->on('ph2.ID','=',$this->t_users.'.phoneStationaryCodeID')
            ->where($this->t_users.'.ID','=',$tender['userID'])
            ->execute()
            ->as_array();

        $user_name = $user_info[0]['user_name'];
        $company_id = $user_info[0]['companyID'];

        $phone = $user_info[0]['phone_code'].$user_info[0]['phone'];
        $phone_stat = $user_info[0]['phone_stat_code'].$user_info[0]['phoneStationary'];

        //вибір назви компанії
        $company = DB::select('name')
            ->from($this->t_company_locales)
            ->where('companyID','=',$company_id)
            ->execute()
            ->as_array();

        $company_name = $company[0]['name'];
        $company_url = $company_id . '-' . Frontend_Helper_URL::title($company_name);

        //вибір кроку згідно відсоткового співвідношення
        $rate_step = $tender['price']/100;
        //приведення кроку до кратності зі значенням "5"
        $rate_step = ceil($rate_step / 5) * 5;

//        $rate_step = 50;

        $isset_requests = false;

        //ініціалізація змінних
        $current_rate = __db('tenders.bet.empty');
        $user_rate = __db('tenders.bet.empty');
        $user_place_num = __db('tenders.bet.empty');
        $next_rate = __db('tenders.bet.not_accepted');
        $user_rate_num = 0;
        $next_rate_num = 0;
        $next_rate_val = 0;
        $next_recom_price = 0;
        $isset_user_rates = false;
        $requests = [];
        $auto_rate = '';

        //вибір поточної ставки
        $current_rate_info = DB::select(
            $this->t_tenders_requests.'.requestNumber',
            $this->t_tenders_requests.'.customPrice',
            $this->t_tenders_requests.'.created',
            $this->t_tenders_requests.'.userID',
            $this->t_users.'.companyID',
            array(DB::expr('(select name from '.$this->t_company_locales.' where companyID = '.$this->t_users.'.companyID order by localeID limit 1)'),'company_name')
        )
            ->from($this->t_tenders_requests)
            ->join($this->t_users,'left')->on($this->t_users.'.ID','=',$this->t_tenders_requests.'.userID')
            ->where($this->t_tenders_requests.'.tenderID','=',$id)
            ->order_by($this->t_tenders_requests.'.ID','desc')
            ->execute()
            ->as_array();

        if (!empty($current_rate_info))
        {
            $isset_requests = true;

            foreach($current_rate_info as &$r)
            {
                $r['company_url'] = Route::url('frontend_site_company_page', array('company_url_name' => $r['companyID'] . '-' . Frontend_Helper_URL::title($r['company_name'])));
            }

            $requests = $current_rate_info;
        }

        if ($isset_requests)
        {

            $current_rate = $current_rate_info[0]['requestNumber'];
            $current_rate = $tender['price'] - ($current_rate * $rate_step);
            $current_rate = $current_rate.' '.$tender['code'];

            //вибір ставки користувача
            if ($user_id)
            {
                $user_rate_info = DB::select('requestNumber','customPrice')
                    ->from($this->t_tenders_requests)
                    ->where('tenderID','=',$id)
                    ->where('userID','=',$user_id)
                    ->order_by('ID','desc')
                    ->limit(1)
                    ->execute()
                    ->as_array();

                if (!empty($user_rate_info))
                {
                    $isset_user_rates = true;
                    $user_rate_num = $user_rate_info[0]['requestNumber'];

                    $user_rate = $user_rate_info[0]['customPrice'].' '.$tender['code'];

                }

            }

            //вибір наступної ставки
            $next_rate_info = DB::select('requestNumber','customPrice')
                ->from($this->t_tenders_requests)
                ->where('tenderID','=',$id)
                ->order_by('ID','desc')
                ->limit(1)
                ->execute()
                ->as_array();


            $next_rate_num = $next_rate_info[0]['requestNumber'];
            $next_rate_temp = $next_rate_info[0]['customPrice'] - $rate_step;
            if ($next_rate_temp > 0)
            {
                if ($next_rate_temp > 5)
                {
                    $next_rate_temp = floor($next_rate_temp / 5) * 5;
                }
                $next_rate = $next_rate_temp.' '.$tender['code'];
                $next_rate_val = $next_rate_temp;
            }

            $user_place_num = $next_rate_num - $user_rate_num;

        }
        else
        {
            $next_rate_temp = $tender['price'];
            if ($next_rate_temp > 0)
            {
                $next_rate = $next_rate_temp.' '.$tender['code'];
                $next_rate_val = $next_rate_temp;
            }
        }


        //вибір автоматичної ставки користувача
        $avto_rate_info = DB::select('price')
            ->from($this->t_tenders_auto_requests)
            ->where('userID','=',$user_id)
            ->where('tenderID','=',$id)
            ->execute()
            ->as_array();

        if (!empty($avto_rate_info))
        {
            $auto_rate = $avto_rate_info[0]['price'];
        }


        return [
            'info'=>$tender,
            'count_requests'=>$count_requests,
            'place_from'=>$place_from,
            'place_to'=>$place_to,
            'user_name'=>$user_name,
            'phone'=>$phone,
            'phone_stat'=>$phone_stat,
            'company_name'=>$company_name,
            'company_id'=>$company_id,
            'company_url'=>Route::url('frontend_site_company_page', array('company_url_name' => $company_url)),
            'rate_step'=>$rate_step.' '.$tender['code'],
            'current_rate'=>$current_rate,
            'user_place_num'=>$user_place_num,
            'user_rate'=>$user_rate,
            'next_rate'=>$next_rate,
            'next_rate_val'=>$next_rate_val,
            'requests'=>$requests,
            'step'=>$rate_step,
            'currency'=>$tender['code'],
            'isset_user_rates'=>$isset_user_rates,
            'auto_rate'=>$auto_rate
        ];


    }

    private function get_places($req_id,$type)
    {
        $res = DB::select('geoID','groupNumber')
            ->from($this->t_tenders_geo)
            ->where('requestID','=',$req_id)
            ->where('type','=',$type)
            ->execute()
            ->as_array();


        $new_res = [];
        foreach($res as $r)
        {
            if (!isset($new_res[$r['groupNumber']])) $new_res[$r['groupNumber']] = [];
            $new_res[$r['groupNumber']][] = $r['geoID'];
        }

        $places = [];

        foreach($new_res as $n_r)
        {
            $geo = DB::select(
                array(DB::expr('(select '.$this->t_geo_locales.'.fullAddress from '.$this->t_geo_locales.' where '.$this->t_geo_locales.'.geoID = '.$this->t_geo.'.ID order by '.$this->t_geo_locales.'.localeID limit 1)'),'name'),
                array(DB::expr('(select '.$this->t_geo_locales.'.country from '.$this->t_geo_locales.' where '.$this->t_geo_locales.'.geoID = '.$this->t_geo.'.ID order by '.$this->t_geo_locales.'.localeID limit 1)'),'country')
            )
                ->from($this->t_geo)
                ->where($this->t_geo.'.ID','in',DB::expr('('.implode(',',$n_r).')'))
                ->order_by($this->t_geo.'.level','desc')
                ->execute()
                ->as_array();

            $places[] = array(
                'name' => $geo[0]['name'],
                'country' => $geo[0]['country'],
                'value' => implode(',',$n_r)
            );

            break; //для вибору лише першого місця завантаження

        }

        return $places;
    }

    private function updateStatusTender($tenderID)
    {
        $now = time()+5;

        $tender = DB::select(
            $this->t_tenders.'.datetimeFromTender',
            $this->t_tenders.'.datetimeToTender',
            $this->t_tenders.'.status',
            $this->t_tenders.'.userID',
            $this->t_tenders.'.ID',
            $this->t_users.'.email'
        )
            ->from($this->t_tenders)
            ->join($this->t_users,'left')->on($this->t_users.'.ID','=',$this->t_tenders.'.userID')
            ->where($this->t_tenders.'.status','!=','completed')
            ->where($this->t_tenders.'.status','!=','deleted')
            ->where($this->t_tenders.'.ID','=',$tenderID)
            ->execute()
            ->as_array();

        if (empty($tender)) return false;

        $tender = $tender[0];

        $this->setTenderStatus($tender['ID'],$tender['datetimeFromTender'],$tender['datetimeToTender'],$tender['status'],$tender['email'],true);

    }

    public function updateStatusTenders($notifications = false)
    {
        $now = time()+5;

        $tenders = DB::select(
            $this->t_tenders.'.datetimeFromTender',
            $this->t_tenders.'.datetimeToTender',
            $this->t_tenders.'.status',
            $this->t_tenders.'.userID',
            $this->t_tenders.'.ID',
            $this->t_users.'.email'
        )
            ->from($this->t_tenders)
            ->join($this->t_users,'left')->on($this->t_users.'.ID','=',$this->t_tenders.'.userID')
            ->where($this->t_tenders.'.status','!=','completed')
            ->where($this->t_tenders.'.status','!=','deleted')
            ->execute()
            ->as_array();

        if (empty($tenders)) return false;

        foreach($tenders as $t)
        {
            $this->setTenderStatus($t['ID'],$t['datetimeFromTender'],$t['datetimeToTender'],$t['status'],$t['email'],$notifications);
        }

    }

    private function setTenderStatus($tenderID,$dateFrom,$dateTo,$status,$avtorEmail,$notifications)
    {
        $newStatus = $status;
        $nowInSec = time()+5;
        $dateFromInSec = strtotime($dateFrom);
        $dateToInSec = strtotime($dateTo);

        if ($nowInSec > $dateFromInSec and $nowInSec < $dateToInSec)
        {
            if ($status == 'inactive')
                $newStatus = 'active';
        }
        else
        {
            if ($nowInSec <= $dateFromInSec)
            {
                if ($status == 'active')
                    $newStatus = 'inactive';
            }

            if ($nowInSec >= $dateToInSec)
            {
                if ($status == 'active')
                    $newStatus = 'completed';
            }
        }

        if ($status == $newStatus) return false;

        DB::update($this->t_tenders)
            ->set(array(
                'status'=>$newStatus,
            ))
            ->where('ID','=',$tenderID)
            ->execute();

        if ($notifications)
        {
            $this->avtorNotification($tenderID,$newStatus,$avtorEmail);
            $this->membersNotification($tenderID,$newStatus);
        }

    }


    private function memberStopNotification($tenderID)
    {
        $member_emails = $this->getTenderMemberEmails($tenderID);

        if (empty($member_emails)) return false;

        foreach($member_emails as $e)
        {
            $subject = __db('tenders.model.notification.stop.subject');
            $text = __db('tenders.model.notification.stop.text[:link:]',['[:link:]'=>'http://tirscan.com/ua/tender/'.$tenderID.'/']);
            Helper_Customemail::send($e,$subject,$text);
        }

    }

    private function avtorNotification($tenderID,$status,$avtorEmail)
    {
        if ($status == 'completed')
        {
            $subject = __db('tenders.model.notification.completed.subject.owner');
            $text = __db('tenders.model.notification.completed.text.owner[:link:]',['[:link:]'=>'http://tirscan.com/ua/tender/'.$tenderID.'/']);
            Helper_Customemail::send($avtorEmail,$subject,$text);
        }
    }

    private function membersNotification($tenderID,$status)
    {
        $member_emails = $this->getTenderMemberEmails($tenderID);

        if (empty($member_emails)) return false;

        if ($status == 'completed')
        {
            foreach($member_emails as $e)
            {
                $subject = __db('tenders.model.notification.completed.subject');
                $text = __db('tenders.model.notification.completed.text[:link:]',['[:link:]'=>'http://tirscan.com/ua/tender/'.$tenderID.'/']);
                Helper_Customemail::send($e,$subject,$text);
            }
        }
    }

    public function countAllByUser($userID)
    {
        $tenders = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_tenders)
            ->where('userID','=',$userID)
            ->where('status','!=','deleted')
            ->execute()
            ->as_array();

        return $tenders[0]['c'];
    }

    public function getAllByUser($userID,$offset,$limit)
    {
        $tender_ids = DB::select('ID')
            ->from($this->t_tenders)
            ->where('userID','=',$userID)
            ->where('status','!=','deleted')
            ->offset($offset)
            ->limit($limit)
            ->execute()
            ->as_array();


        foreach($tender_ids as $tender_id)
            $this->updateStatusTender($tender_id);


        $tenders = DB::select($this->t_tenders.'.*',$this->t_car_types.'.name',$this->t_currencies.'.code')
            ->from($this->t_tenders)
            ->join($this->t_car_types,'left')->on($this->t_tenders.'.transportTypeID','=',$this->t_car_types.'.ID')
            ->join($this->t_currencies,'left')->on($this->t_tenders.'.currencyID','=',$this->t_currencies.'.ID')
            ->where($this->t_tenders.'.userID','=',$userID)
            ->where($this->t_tenders.'.status','!=','deleted')
            ->order_by($this->t_tenders.'.ID','desc')
            ->offset($offset)
            ->limit($limit)
            ->execute()
            ->as_array();

        if (empty($tenders)) return null;

        return $this->addResultParams($tenders);

    }

    public function countAllOwnerByUser($user_id)
    {

        $res = DB::select(DB::expr('count('.$this->t_tenders_requests.'.ID) as c'))
            ->distinct(TRUE)
            ->from($this->t_tenders_requests)
            ->join($this->t_tenders,'inner')->on($this->t_tenders.'.ID','=',$this->t_tenders_requests.'.tenderID')
            ->where($this->t_tenders_requests.'.userID','=',$user_id)
            ->where($this->t_tenders.'.status','!=','deleted')
            ->execute()
            ->as_array();

        return $res[0]['c'];
    }

    public function getAllOwnerByUser($user_id,$offset,$limit)
    {
        $tender_ids = $this->getTenderIdsByUser($user_id);

        if (empty($tender_ids)) return null;

        foreach($tender_ids as $tender_id)
            $this->updateStatusTender($tender_id);


        $tenders = DB::select($this->t_tenders.'.*',$this->t_car_types.'.name',$this->t_currencies.'.code')
            ->from($this->t_tenders)
            ->join($this->t_car_types,'left')->on($this->t_tenders.'.transportTypeID','=',$this->t_car_types.'.ID')
            ->join($this->t_currencies,'left')->on($this->t_tenders.'.currencyID','=',$this->t_currencies.'.ID')
            ->where($this->t_tenders.'.ID','in',DB::expr('('.implode(',',$tender_ids).')'))
            ->where($this->t_tenders.'.status','!=','deleted')
            ->order_by($this->t_tenders.'.ID','desc')
            ->offset($offset)
            ->limit($limit)
            ->execute()
            ->as_array();

        if (empty($tenders)) return null;

        return $this->addResultParams($tenders,$user_id);
    }

    public function delete($id)
    {

        $res = DB::select('status')
            ->from($this->t_tenders)
            ->where('ID','=',$id)
            ->execute()
            ->as_array();

        if (empty($res)) return 0;

        if ($res[0]['status'] != 'inactive') return 0;


        DB::update($this->t_tenders)
            ->set(array(
                'status'=>'deleted'
            ))
            ->where('ID','=',$id)
            ->execute();
        return 1;
    }

    private function getTenderIdsByUser($user_id)
    {
        $res = DB::select($this->t_tenders_requests.'.tenderID')
            ->distinct(TRUE)
            ->from($this->t_tenders_requests)
            ->join($this->t_tenders,'inner')->on($this->t_tenders.'.ID','=',$this->t_tenders_requests.'.tenderID')
            ->where($this->t_tenders_requests.'.userID','=',$user_id)
            ->where($this->t_tenders.'.status','!=','deleted')
            ->execute()
            ->as_array();

        if (empty($res)) return [];

        return Helper_Array::get_array_by_key($res,'tenderID');
    }

    private function addResultParams(array $tenders, $user_id = null)
    {
        foreach($tenders as &$t)
        {
            //вибір кількості ставок
            $count_requests = DB::select(DB::expr('count(ID) as c'))
                ->from($this->t_tenders_requests)
                ->where('tenderID','=',$t['ID'])
                ->execute()
                ->as_array();

            $count_requests = $count_requests[0]['c'];

            //вибір місця завантаження
            $place_from = $this->get_places($t['ID'],'from');

            //вибір місця розвантаження
            $place_to = $this->get_places($t['ID'],'to');

            //вибір імені користувача
            $user_info = DB::select(
                $this->t_users.'.companyID',
                $this->t_users.'.phone',
                $this->t_users.'.phoneStationary',
                array('ph1.code','phone_code'),
                array('ph2.code','phone_stat_code'),
                array(DB::expr("(select name from ".$this->t_user_locales." where userID = ".$t['userID']." limit 1)"),'user_name')
            )
                ->from($this->t_users)
                ->join(array($this->t_phone_codes,'ph1'),'left')->on('ph1.ID','=',$this->t_users.'.phoneCodeID')
                ->join(array($this->t_phone_codes,'ph2'),'left')->on('ph2.ID','=',$this->t_users.'.phoneStationaryCodeID')
                ->where($this->t_users.'.ID','=',$t['userID'])
                ->execute()
                ->as_array();

            $user_name = $user_info[0]['user_name'];
            $company_id = $user_info[0]['companyID'];

            $phone = $user_info[0]['phone_code'].$user_info[0]['phone'];
            $phone_stat = $user_info[0]['phone_stat_code'].$user_info[0]['phoneStationary'];

            //вибір назви компанії
            $company = DB::select('name')
                ->from($this->t_company_locales)
                ->where('companyID','=',$company_id)
                ->execute()
                ->as_array();

            $company_name = $company[0]['name'];
            $company_url = $company_id . '-' . Frontend_Helper_URL::title($company_name);        //вибір кількості ставок

            $exp_from_place = explode(', ',$place_from[0]['name']);
            $from_1 = $exp_from_place[0];
            $from_2 = (isset($exp_from_place[1]))?$exp_from_place[1]:$from_1;

            $exp_to_place = explode(', ',$place_to[0]['name']);
            $to_1 = $exp_to_place[0];
            $to_2 = (isset($exp_to_place[1]))?$exp_to_place[1]:$to_1;


            $cargo_arr = [];
            if ($t['name']) $cargo_arr[] = $t['name'];
            if ($t['cargoType']) $cargo_arr[] = $t['cargoType'];

            $weight_and_volume = [];
            if ($t['weight']) $weight_and_volume[] = $t['weight'].' т';
            if ($t['volume']) $weight_and_volume[] = $t['volume'].' м<sup>3</sup>';

            $t['count_requests'] = ($count_requests) ? $count_requests : '-';
            $t['place_from'] = $place_from;
            $t['place_to'] = $place_to;
            $t['user_name'] = $user_name;
            $t['phone'] = $phone;
            $t['phone_stat'] = $phone_stat;
            $t['company_name'] = $company_name;
            $t['company_id'] = $company_id;
            $t['company_url'] = Route::url('frontend_site_company_page', array('company_url_name' => $company_url));
            $t['currency'] = $t['code'];
            $t['dateFromLoad'] = date("d.m.Y",strtotime($t['dateFromLoad']));
            $t['dateToLoad'] = date("d.m.Y",strtotime($t['dateToLoad']));
            $t['dateFromTender'] = date("d.m.Y H:i",strtotime($t['datetimeFromTender']));
            $t['dateToTender'] = date("d.m.Y H:i",strtotime($t['datetimeToTender']));

            $t['from_1'] = $from_1;
            $t['from_2'] = $from_2;
            $t['to_1'] = $to_1;
            $t['to_2'] = $to_2;
            $t['cargo_info'] = implode(', ',$cargo_arr);
            $t['weight_and_volume'] = implode(' / ',$weight_and_volume);
            if ($user_id) $t['user_place_num'] = $this->getUserPlaceNum($user_id,$t['ID']);
        }

        return $tenders;

    }

    public function stop($id,$reason)
    {
        DB::update($this->t_tenders)
            ->set(array(
                'status'=>'stopped',
                'stop_reason'=>$reason
            ))
            ->where('ID','=',$id)
            ->execute();

        $this->memberStopNotification($id);

        return 1;
    }

    private function getUserPlaceNum($user_id,$tender_id)
    {
        //вибір наступної ставки
        $next_rate_info = DB::select('requestNumber','customPrice')
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$tender_id)
            ->order_by('ID','desc')
            ->limit(1)
            ->execute()
            ->as_array();

        $next_rate_num = $next_rate_info[0]['requestNumber']+1;

        //вибір ставки користувача
        $user_rate_info = DB::select('requestNumber','customPrice')
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$tender_id)
            ->where('userID','=',$user_id)
            ->order_by('ID','desc')
            ->limit(1)
            ->execute()
            ->as_array();

        $user_rate_num = $user_rate_info[0]['requestNumber'];

        $user_place_num = $next_rate_num - $user_rate_num;

        return $user_place_num;

    }

    public function getEmailByUserID($user_id)
    {
        $res = DB::select('email')
            ->from($this->t_users)
            ->where('ID','=',$user_id)
            ->execute()
            ->as_array();
        if (!empty($res)) return $res[0]['email'];
        return null;
    }

    public function getTenderMemberEmails($tender_id)
    {
        $res = DB::select($this->t_users.'.email')
            ->distinct(true)
            ->from($this->t_tenders_requests)
            ->join($this->t_users,'left')->on($this->t_users.'.ID','=',$this->t_tenders_requests.'.userID')
            ->where($this->t_tenders_requests.'.tenderID','=',$tender_id)
            ->execute()
            ->as_array();
        if (!empty($res)) return Helper_Array::get_array_by_key($res,'email');
        return [];
    }


    public function check_changes($id,$status,$count_requests)
    {
        $changes = 0;

        $tender_info = DB::select('status')
            ->from($this->t_tenders)
            ->where('ID','=',$id)
            ->execute()
            ->as_array();

        if (empty($tender_info)) return $changes;

        $req_count = DB::select(DB::expr('count(ID) as c'))
            ->from($this->t_tenders_requests)
            ->where('tenderID','=',$id)
            ->execute()
            ->as_array();

        if ($status != $tender_info[0]['status']) $changes = 1;

        if ($count_requests != $req_count[0]['c']) $changes = 1;

        return $changes;
    }
}