<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Geo
 *
 * @author Сергій Krid
 */
class Model_Frontend_Geo extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Geo';

    protected $_table_name = self::TABLE;

    public static function query_getGeoIDsByLatLng($lon1, $lat1, $lon2, $lat2) {
        $geo_ids = DB::select('ID')
                     ->from(self::TABLE)
                     ->where('type', '=', 'city')
                     ->where('lng', '>=', $lon1)
                     ->where('lng', '<=', $lon2)
                     ->where('lat', '>=', $lat1)
                     ->where('lat', '<=', $lat2)
                     ->execute()
                     ->as_array();

        $result = Helper_Array::get_array_by_key($geo_ids, 'ID');
        if ($result != null) {
            return $result;
        }
        else {
            return [];
        }
    }

    public static function query_getUniqueGeoCount() {
        $cargo_query = DB::select(['t_cargo_geo.ID', 'geoID'])
                         ->from([Model_Frontend_Cargo::TABLE, 't_cargo'])
                         ->join([Model_Frontend_Cargo::TABLE_TO_GEO, 't_cargo_to_geo'], 'INNER')
                         ->on('t_cargo_to_geo.requestID', '=', 't_cargo.ID')
                         ->join([self::TABLE, 't_cargo_geo'], 'INNER')
                         ->on('t_cargo_geo.ID', '=', 't_cargo_to_geo.geoID')
                         ->on('t_cargo_geo.parentID', 'IS', DB::expr('NULL'))
                         ->on('t_cargo_geo.level', '=', DB::expr(1))
                         ->on('t_cargo_geo.type', '=', DB::expr("'country'"));

        $transport_query = DB::select(['t_trans_geo.ID', 'geoID'])
                             ->union($cargo_query, false)
                             ->from([Model_Frontend_Cargo::TABLE, 't_trans'])
                             ->join([Model_Frontend_Cargo::TABLE_TO_GEO, 't_trans_to_geo'], 'INNER')
                             ->on('t_trans_to_geo.requestID', '=', 't_trans.ID')
                             ->join([self::TABLE, 't_trans_geo'], 'INNER')
                             ->on('t_trans_geo.ID', '=', 't_trans_to_geo.geoID')
                             ->on('t_trans_geo.parentID', 'IS', DB::expr('NULL'))
                             ->on('t_trans_geo.level', '=', DB::expr(1))
                             ->on('t_trans_geo.type', '=', DB::expr("'country'"));

        $company_query = DB::select(['t_comp_geo.ID', 'geoID'])
                           ->from([Model_Frontend_Company::TABLE, 't_comp'])
                           ->join([Model_Frontend_Company::TABLE_TO_GEO, 't_comp_to_geo'], 'INNER')
                           ->on('t_comp_to_geo.companyID', '=', 't_comp.ID')
                           ->join([self::TABLE, 't_comp_geo'], 'INNER')
                           ->on('t_comp_geo.ID', '=', 't_comp_to_geo.geoID')
                           ->on('t_comp_geo.parentID', 'IS', DB::expr('NULL'))
                           ->on('t_comp_geo.level', '=', DB::expr(1))
                           ->on('t_comp_geo.type', '=', DB::expr("'country'"));

        $union_query = DB::select()
                         ->union($company_query, false)
                         ->from([$transport_query, 'cargo_transport_union']);

        $result = DB::select([DB::expr('COUNT(*)'), 'count'])
                    ->from([$union_query, 'union_query'])
                    ->execute()
                    ->as_array();

        return (int)$result[0]['count'];
    }

}