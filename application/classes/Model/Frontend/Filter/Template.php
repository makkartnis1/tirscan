<?php defined('SYSPATH') or die('No direct script access.');


class Model_Frontend_Filter_Template extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Filter_Templates';

    protected $_table_name = self::TABLE;

    public function rules() {
        return [
            'name' => [
                ['not_empty']
            ],
            'query' => [
                ['not_empty']
            ]
        ];
    }

}