<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_User
 *
 * @author Сергій Krid
 */
class Model_Frontend_User extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Users';
    const TABLE_TO_COMPANY_OWNERSHIP = 'Frontend_User_to_Company_Ownership';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'company' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'companyID'
        ],
        'primaryLocale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'primaryLocaleID'
        ],
        'phoneCode' => [
            'model' => 'Frontend_Phonecodes',
            'foreign_key' => 'phoneCodeID'
        ],
        'phoneStationaryCode' => [
            'model' => 'Frontend_Phonecodes',
            'foreign_key' => 'phoneStationaryCodeID'
        ]
    ];

    protected $_has_many = [
        'cars' => [
            'model' => 'Frontend_User_Car',
            'foreign_key' => 'userID'
        ],
        'locales' => [
            'model' => 'Frontend_User_Locale',
            'foreign_key' => 'userID'
        ]
    ];

    protected $password = null;

    /** @var null|Model_Frontend_User_Locale */
    protected $_locale = null;

    public function rules() {
        $rules = [
            'email' => [
                [ 'not_empty' ],
                [ 'email' ]
            ],
            'emailApproved' => [
                [ 'Model_Frontend_Validation::in_array', [ ':value', [ 'approved', 'pending' ] ] ]
            ],
            'phone' => [
                [ 'not_empty' ],
//                [ 'exact_length', [':value', 10]]
            ],
            'phoneStationary' => [
//                [ 'exact_length', [':value', 10]],
                [ 'numeric' ]
            ],
            'icq' => [
                [ 'numeric' ]
            ]
        ];

        if ($this->password !== null)
            $rules['hash'] = [
                [ 'not_empty' ],
                [ 'regex', [$this->password, Controller_Frontend_Company::PASS_REGEX] ],
                [ 'min_length', [$this->password, Controller_Frontend_Company::PASS_MIN_LENGTH] ],
                [ 'max_length', [$this->password, Controller_Frontend_Company::PASS_MAX_LENGTH] ]
            ];

        return $rules;
    }

    public function filters()
    {
        return [
            'icq' => [
                [ 'trim' ]
            ],
            'skype' => [
                [ 'trim' ]
            ],
            'phone' => [
                ['Model_Frontend_Filters::onlyDigits', [':value']]
            ],
            'phoneStationary' => [
                ['Model_Frontend_Filters::onlyDigits', [':value']]
            ]
        ];
    }

    function __set($column, $value) {
        if ($column == 'password') {
            $this->password = $value;
            $this->updateHash();
        }
        else
            parent::__set($column, $value);
    }

    public function updateHash() {
        $this->hash = md5($this->password);
    }

    public function checkPasswordEqual($password) {
        if (! $this->loaded())
            return false;

        return (md5($password) == $this->hash);
    }

    public function makeOwner(Model_Frontend_Company $company) {
        // видаляємо попередніх власників
        DB::delete(self::TABLE_TO_COMPANY_OWNERSHIP)
            ->where('companyID', '=', $company->ID)
            ->execute();

        // робимо користувача власником
        DB::insert(self::TABLE_TO_COMPANY_OWNERSHIP, ['userID', 'companyID'])
            ->values([$this->ID, $company->ID])
            ->execute();
    }

    /**
     * @param Model_Frontend_Locale $site_locale
     * @return Model_Frontend_User_Locale
     */
    public function getLocale(Model_Frontend_Locale $site_locale) {
        if (is_null($this->_locale)) { // lazy load
            // CASE для сортування (шукана мова в пріоритеті)
            $case_str = sprintf('CASE WHEN `localeID` = %d THEN 2 ELSE 1 END', $site_locale->ID);

            $this->_locale = $this->locales
                ->and_where_open()
                ->where('localeID', '=', $site_locale->ID)
                ->or_where('localeID', '=', $this->primaryLocaleID)
                ->and_where_close()
                ->order_by(
                    DB::expr($case_str),
                    'ASC')
                ->find();
        }

        return $this->_locale;
    }

    public function isOwnerOfProfileCompany(){

        if (! $this->loaded()) // якщо користувач не підтянутий з бази, то доступу у нього немає
            return false;

        return (boolean)

            count(
                DB::select()
                ->from(self::TABLE_TO_COMPANY_OWNERSHIP)
                ->where('companyID',    '=', $this->companyID)
                ->where('userID',       '=', $this->ID)
                ->limit(1)
                ->execute()
                ->as_array()
            );

    }

    public function hasRights($right) {
        if (! $this->loaded()) // якщо користувач не підтянутий з бази, то доступу у нього немає
            return false;

        $group = Kohana::$config->load( 'frontend/groups.' . $this->company->type->ID );

        return ( is_null($group) ) ? false : in_array($right, $group);
    }

    public function email_available() {
        return ! ORM::factory('Frontend_User', ['email' => $this->email])->loaded();
    }

//    public static function _profile_myInfo($userID){
//
//        $to_select = [
//
//            [
//                'table_name'    => $from = self::TABLE,
//                'prefix'        => 'fu_',
//                'columns'       => ['ID', 'hash', 'registered', 'email', 'emailApproved', 'walletBalance', 'icq', 'skype', 'currentRating', 'companyID', 'phone', 'phoneStationary', 'phoneCodeID', 'phoneStationaryCodeID', 'primaryLocaleID', 'parser', 'parserMail'],
//            ],
//
//            [
//                'table_name'    => $fc = 'Frontend_Companies',
//                'prefix'        => 'fc_',
//                'columns'       => ['ID', 'approvedByAdmin', 'trustableMark', 'registered', 'typeID', 'ipn', 'ownershipTypeID', 'primaryLocaleID', 'phone', 'phoneStationary', 'phoneCodeID', 'phoneStationaryCodeID', 'foundationDate', 'parser', 'url', 'rating'],
//            ],
//
//            [
//                'table_name'    => $fct = 'Frontend_Company_Types',
//                'prefix'        => 'fct_',
//                'columns'       => ['ID', 'name', 'localeName'],
//            ],
//
//            [
//                'table_name'    => $fcot = 'Frontend_Company_Ownership_Types',
//                'prefix'        => 'fcot_',
//                'columns'       => ['ID', 'name', 'localeName'],
//            ],
//
////            [
////                'table_name'    => 'Frontend_Company_Ownership_Types',
////                'prefix'        => 'fcot_',
////                'columns'       => ['ID', 'name', 'localeName'],
////            ]
//
//
//
//
//        ];
//
//        $select = DB::select();
//
//        foreach ($to_select as $table_block){
//
//            $table  = $table_block['table_name'];
//            $pref   = $table_block['prefix'];
//
//
//            foreach ($table_block['columns'] as $column){
//
//                $select->select( [$table.'.'.$column, $pref.$column] );
//
//            }
//
//        }
//
//
//
//
//        $select
//            ->from($from)
//            ->join($fc, 'INNER')
//            ->on($from.'')
//
//
//    }

}