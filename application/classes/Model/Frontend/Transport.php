<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Transport
 *
 * @author Сергій Krid
 */
class Model_Frontend_Transport extends Model_Frontend_ORM {

    const TABLE        = 'Frontend_Transports';
    const TABLE_TO_GEO = 'Frontend_Transport_to_Geo_Relation';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'price' => [
            'model' => 'Frontend_Request_Price',
            'foreign_key' => 'priceID'
        ],
        'userCar' => [
            'model' => 'Frontend_User_Car',
            'foreign_key' => 'userCarID',
        ],
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

    public function getPostName() {
        return 'transport';
    }

    public function addGeoRelation(Model_Frontend_Geo $geo, $type) {
        DB::insert(self::TABLE_TO_GEO, ['geoID', 'requestID', 'type'])
          ->values([$geo->ID, $this->ID, $type])
          ->execute();
    }

    public function addGeoRelationByIDs(array $ids, $type, $group) {
        foreach ($ids as $id) {
            DB::insert(self::TABLE_TO_GEO, ['geoID', 'requestID', 'type', 'groupNumber'])
              ->values([$id, $this->ID, $type, $group])
              ->execute();
        }
    }

    public static function query_getTransportsForProfile(Model_Frontend_User $user) {
        $query = DB::select(
            ['t_trans.ID', 'ID'],

            't_trans.created',
            'dateFrom',
            'dateTo',

            // інформація про авто
            ['t_user_car.sizeX', 'car_sizeX'],
            ['t_user_car.sizeY', 'car_sizeY'],
            ['t_user_car.sizeZ', 'car_sizeZ'],
            ['t_user_car.volume', 'car_volume'],
            ['t_user_car.liftingCapacity', 'car_liftingCapacity'],
            ['t_user_car.type', 'car_type'],
            ['t_user_car_type.localeName', 'car_type_localeName']
        )
                   ->from([self::TABLE, 't_trans'])
            // join машини і її типу
                   ->join([Model_Frontend_User_Car::TABLE, 't_user_car'], 'LEFT')
                   ->on('t_trans.userCarID', '=', 't_user_car.ID')
                   ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
                   ->on('t_user_car.carTypeID', '=', 't_user_car_type.ID')
                   ->where('t_trans.userID', '=', $user->ID)
                   ->order_by('t_trans.created', 'DESC');

        return $query->execute();
    }

    public static function query_getPlaces($locale_id, $request_id, $type) {
        $locale_id = 1; // TODO HOTFIX не вставляє переклади на інших мовах ЗАГЛУШКА

        $query = DB::select('geoID')
                   ->from(self::TABLE_TO_GEO)
                   ->where('requestID', '=', $request_id)
                   ->and_where('type', '=', $type)
                   ->and_where('groupNumber', '=', 1);

//        if (count($query->execute()->as_array()) == 0) {
//            var_dump($request_id); die;
//        }

        $geo_result = DB::select('ID')
                        ->from(Model_Frontend_Geo::TABLE)
                        ->where('ID', 'IN', $query)
                        ->order_by('level', 'DESC')
                        ->limit(1)
                        ->execute()
                        ->as_array();

        foreach ($geo_result as &$result) {
            $result = $result['ID'];
            unset($result);
        }

//        $i = 0;
//        do {
        $query_geo_locale = DB::select(['fullAddress', $type . '_' . 'fullAddress'], ['name', $type . '_' . 'name'], ['country', $type . '_' . 'country'])
                              ->from(Model_Frontend_Geo_Locale::TABLE)
                              ->where('geoID', 'IN', $geo_result)
                              ->and_where('localeID', '=', $locale_id)
                              ->execute()
                              ->as_array();

        
//            if (count($query_geo_locale) == 0) {
//                $locale_uri = ORM::factory('Frontend_Locale', $locale_id)->googleLang;
//                $place_ids  = DB::select('placeID')
//                                ->from(Model_Frontend_Geo::TABLE)
//                                ->where('ID', 'IN', $geo_result)
//                                ->execute()
//                                ->as_array();
//
//                foreach ($place_ids as $place_id) {
//                    Request::factory('/cms/geo/' . $locale_uri . '/add')
//                           ->post([
//                               'place_id' => $place_id['placeID'],
//                           ])
//                           ->execute()
//                           ->body();
//
//                    echo $place_id['placeID'];
//                }
//            }
//
//            if ($i++ > 10) {
//                break;
//            }
//        } while (count($query_geo_locale) == 0);

        $query_geo = DB::select(['placeID', $type . '_' . 'placeID'])
                       ->from(Model_Frontend_Geo::TABLE)
                       ->where('ID', 'IN', $geo_result)
                       ->execute()
                       ->as_array();


        return array_merge($query_geo_locale[0], $query_geo[0]);
    }

    public static function query_getOtherPlaces($locale_id, $request_id, $type) {
        $locale_id = 1; // TODO HOTFIX не вставляє переклади на інших мовах ЗАГЛУШКА

        $query = DB::select('geoID', 'groupNumber')
                   ->from(self::TABLE_TO_GEO)
                   ->where('requestID', '=', $request_id)
                   ->and_where('type', '=', $type)
                   ->and_where('groupNumber', '>', 1);

        $q_ids = $query->execute()->as_array();
        $ids   = [];
        foreach ($q_ids as $q_id) {
            $ids[ $q_id['geoID'] ] = $q_id['groupNumber'];
        }

        $func_result = [];
        if (count($ids) > 0) {
            $geo_result = DB::select('ID')
                            ->from(Model_Frontend_Geo::TABLE)
                            ->where('ID', 'IN', array_keys($ids))
                            ->and_where('type', '=', DB::expr("'city'"))
                            ->execute()
                            ->as_array();

            foreach ($geo_result as &$result) {
                $result = $result['ID'];
                unset($result);
            }

            if (count($geo_result) > 0) {
                $query_geo_locale = DB::select(['geoID', $type . '_' . 'geoID'], ['fullAddress', $type . '_' . 'fullAddress'], ['name', $type . '_' . 'name'], ['country', $type . '_' . 'country'])
                                      ->from(Model_Frontend_Geo_Locale::TABLE)
                                      ->where('geoID', 'IN', $geo_result)
                                      ->and_where('localeID', '=', $locale_id)
                                      ->execute()
                                      ->as_array();

                $query_geo = DB::select(['placeID', $type . '_' . 'placeID'])
                               ->from(Model_Frontend_Geo::TABLE)
                               ->where('ID', 'IN', $geo_result)
                               ->execute()
                               ->as_array();

                asort($ids);
                $query_geo_loc_temp = [];
                foreach ($query_geo_locale as $key => $value) {
                    $query_geo_loc_temp[$value[$type . '_geoID']] = $value;
                }
                $query_geo_locale = [];
                foreach ($ids as $key => $value) {
                    if (array_key_exists($key, $query_geo_loc_temp)) {
                        $query_geo_locale[] = $query_geo_loc_temp[$key];
                    }
                }

                foreach ($query_geo_locale as $key => $value) {
                    $merged            = array_merge($value, $query_geo[$key]);
                    $func_result[$key] = $merged;
                }
            }
        }

        return [
            $type . '_otherGeo' => $func_result
        ];
    }

    public static function query_dropViews($requestID, $new_views_old) {
        DB::update(self::TABLE)
            ->set(['views_old' => $new_views_old])
            ->where('ID', '=', $requestID)
            ->execute();
    }

    public static function query_getTransportsSelect($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = DB::select(
            ['t_trans.ID', 'ID'],

            't_trans.views',
            [DB::expr('(`t_trans`.`views`-`t_trans`.`views_old`)'), 'new_views'],

            't_trans.parser',
            't_trans.isVip',
            't_trans.isColored',
            't_trans.status',
            't_trans.userID',
            't_trans.created',
            ['t_trans.created', 'createdRaw'],
            't_trans.info',
            'dateFrom',
            'dateTo',
            't_user.companyID',
            ['t_comp.typeID', 'companyTypeID'],

            // інформація про користувача і компанію (вибираємо існуючі переклади)
            [DB::expr('IFNULL(`t_user_loc`.`name`, `t_user_loc_prim`.`name`)'), 'username'], // ім'я користувача
            [DB::expr('IFNULL(`t_comp_loc`.`name`, `t_comp_loc_prim`.`name`)'), 'company_name'], // назва компанії
            ['t_ph_codes.code', 'phoneCode'],
            't_user.phone',

            // інформація про ціну
            't_price.value',
            ['t_curr.code', 'valueCode'],

            // інформація про авто
            ['t_user_car.sizeX', 'car_sizeX'],
            ['t_user_car.sizeY', 'car_sizeY'],
            ['t_user_car.sizeZ', 'car_sizeZ'],
            ['t_user_car.volume', 'car_volume'],
            ['t_user_car.liftingCapacity', 'car_liftingCapacity'],
            ['t_user_car.type', 'car_type'],
            ['t_user_car_type.localeName', 'car_type_localeName']
        )
                   ->from([self::TABLE, 't_trans'])
            // join машини і її типу
                   ->join([Model_Frontend_User_Car::TABLE, 't_user_car'], 'LEFT')
                   ->on('t_trans.userCarID', '=', 't_user_car.ID')
                   ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
                   ->on('t_user_car.carTypeID', '=', 't_user_car_type.ID')
            // join ціни
                   ->join([Model_Frontend_Request_Price::TABLE, 't_price'], 'LEFT')
                   ->on('t_trans.priceID', '=', 't_price.ID')
                   ->join([Model_Frontend_Currency::TABLE, 't_curr'], 'LEFT')
                   ->on('t_price.currencyID', '=', 't_curr.ID')
            // join користувача
                   ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
                   ->on('t_trans.userID', '=', 't_user.ID')
            // join таблиці телефонних кодів
                   ->join([Model_Frontend_Phonecodes::TABLE, 't_ph_codes'], 'LEFT')
                   ->on('t_user.phoneCodeID', '=', 't_ph_codes.id')
            // join локалей користувача (на поточній мові і мові реєстрації)
                   ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc'], 'LEFT')
                   ->on('t_user.ID', '=', 't_user_loc.userID')
                   ->on('t_user_loc.localeID', '=', DB::expr($localeID))
                   ->join([Model_Frontend_User_Locale::TABLE, 't_user_loc_prim'], 'LEFT')
                   ->on('t_user.ID', '=', 't_user_loc_prim.userID')
                   ->on('t_user.primaryLocaleID', '=', 't_user_loc_prim.localeID')
            // join компанії
                   ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
                   ->on('t_user.companyID', '=', 't_comp.ID')
            // join локалей компанії (на поточній мові і мові реєстрації)
                   ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc'], 'LEFT')
                   ->on('t_comp.ID', '=', 't_comp_loc.companyID')
                   ->on('t_comp_loc.localeID', '=', DB::expr($localeID))
                   ->join([Model_Frontend_Company_Locale::TABLE, 't_comp_loc_prim'], 'LEFT')
                   ->on('t_comp.ID', '=', 't_comp_loc_prim.companyID')
                   ->on('t_comp.primaryLocaleID', '=', 't_comp_loc_prim.localeID')
            // join geo
                   ->join([self::TABLE_TO_GEO, 't_trans_to_geo_from'], 'INNER')
                   ->on('t_trans.ID', '=', 't_trans_to_geo_from.requestID')
                   ->on('t_trans_to_geo_from.type', '=', DB::expr('"from"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
                   ->on('t_trans_to_geo_from.geoID', '=', 't_geo_from.ID')
                   ->join([self::TABLE_TO_GEO, 't_trans_to_geo_to'], 'INNER')
                   ->on('t_trans.ID', '=', 't_trans_to_geo_to.requestID')
                   ->on('t_trans_to_geo_to.type', '=', DB::expr('"to"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
                   ->on('t_trans_to_geo_to.geoID', '=', 't_geo_to.ID')
                   ->group_by('t_trans.ID')
                   ->order_by('t_trans.created', 'DESC');

        // додаєм фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        return $query;
    }

    public static function assignPlacesToTransport($localeID, $result_tr) {
        foreach ($result_tr as &$transport) {
            $places_from = self::query_getPlaces($localeID, $transport['ID'], 'from');
            $places_to   = self::query_getPlaces($localeID, $transport['ID'], 'to');

            $places_from_other = self::query_getOtherPlaces($localeID, $transport['ID'], 'from');
            $places_to_other   = self::query_getOtherPlaces($localeID, $transport['ID'], 'to');

            $transport = array_merge($transport, $places_from, $places_to, $places_from_other, $places_to_other);
            unset($transport);
        }

        return $result_tr;
    }

    public static function query_getTransportTickets($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getTransportInfoSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $result_tr = $query
            ->select(['t_app_comp.ID', 'appCompID'])
            ->join([Model_Frontend_User_Transport_Application::TABLE, 't_app'], 'LEFT')
            ->on('t_app.transportID', '=', 't_trans.ID')
            ->on('t_app.status', '=', DB::expr("'accepted'"))
            ->join([Model_Frontend_User::TABLE, 't_app_user'], 'LEFT')
            ->on('t_app_user.ID', '=', 't_app.userID')
            ->join([Model_Frontend_Company::TABLE, 't_app_comp'], 'LEFT')
            ->on('t_app_comp.ID', '=', 't_app_user.companyID')
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return self::assignPlacesToTransport($localeID, $result_tr);
    }

    public static function query_getTransportInfoSelect($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getTransportsSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $query// довибираємо пачтку данних для JSON-а

        ->select(['t_trans.docTIR', 'json_docTIR'])
        ->select(['t_trans.docCMR', 'json_docCMR'])
        ->select(['t_trans.docT1', 'json_docT1'])
        ->select(['t_trans.docSanPassport', 'json_docSanPassport'])
        ->select(['t_trans.docSanBook', 'json_docSanBook'])
        ->select(['t_trans.loadFromSide', 'json_loadFromSide'])
        ->select(['t_trans.loadFromTop', 'json_loadFromTop'])
        ->select(['t_trans.loadFromBehind', 'json_loadFromBehind'])
        ->select(['t_trans.loadTent', 'json_loadTent'])
        ->select(['t_trans.condPlomb', 'json_condPlomb'])
        ->select(['t_trans.condLoad', 'json_condLoad'])
        ->select(['t_trans.condBelts', 'json_condBelts'])
        ->select(['t_trans.condRemovableStands', 'json_condRemovableStands'])
        ->select(['t_trans.condHardSide', 'json_condHardSide'])
        ->select(['t_trans.condCollectableCargo', 'json_condCollectableCargo'])
        ->select(['t_trans.condTemperature', 'json_condTemperature'])
        ->select(['t_trans.condPalets', 'json_condPalets'])
        ->select(['t_trans.condADR', 'json_condADR'])
        ->select(['t_price.paymentType', 'json_paymentType'])
        ->select(['t_price.PDV', 'json_PDV'])
        ->select(['t_price.onLoad', 'json_onLoad'])
        ->select(['t_price.onUnload', 'json_onUnload'])
        ->select(['t_price.onPrepay', 'json_onPrepay'])
        ->select(['t_price.advancedPayment', 'json_advancedPayment'])//%
        ->select(['t_trans.info', 'json_info'])

        ->select(['t_user_car.sizeX', 'json_sizeX'])
        ->select(['t_user_car.sizeY', 'json_sizeY'])
        ->select(['t_user_car.sizeZ', 'json_sizeZ'])

        ->select(['t_price.customPriceType', '_json_customPriceType'])
        ->select(['t_price.value', '_json_price'])
        ->select(['t_curr.code', '_json_currency_code']);

        return $query;
    }

    public static function query_getTransports($localeID, $geoSearch, $limit, $offset, array $filters = []) {
        $query = self::query_getTransportInfoSelect($localeID, $geoSearch, $limit, $offset, $filters);

        $result_tr = $query
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return self::assignPlacesToTransport($localeID, $result_tr);
    }

    public static function query_getNewTransportCountSince(array $filters = [], $since_time) {
        $filters[] = [
            'created', '>', $since_time
        ];

        return self::query_getTransportCount($filters);
    }

    public static function query_getTransportCount(array $filters = []) {
        $query = DB::select([DB::expr('COUNT(DISTINCT(t_trans.ID))'), 'count'])
                   ->from([self::TABLE, 't_trans'])
            // join машини і її типу
                   ->join([Model_Frontend_User_Car::TABLE, 't_user_car'], 'LEFT')
                   ->on('t_trans.userCarID', '=', 't_user_car.ID')
                   ->join([Model_Frontend_User_Car_Type::TABLE, 't_user_car_type'], 'LEFT')
                   ->on('t_user_car.carTypeID', '=', 't_user_car_type.ID')
            // join ціни
                   ->join([Model_Frontend_Request_Price::TABLE, 't_price'], 'LEFT')
                   ->on('t_trans.priceID', '=', 't_price.ID')
            // join користувача
                   ->join([Model_Frontend_User::TABLE, 't_user'], 'LEFT')
                   ->on('t_trans.userID', '=', 't_user.ID')
            // join компанії
                   ->join([Model_Frontend_Company::TABLE, 't_comp'], 'LEFT')
                   ->on('t_user.companyID', '=', 't_comp.ID')
            // join geo
                   ->join([self::TABLE_TO_GEO, 't_trans_to_geo_from'], 'INNER')
                   ->on('t_trans.ID', '=', 't_trans_to_geo_from.requestID')
                   ->on('t_trans_to_geo_from.type', '=', DB::expr('"from"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_from'], 'INNER')
                   ->on('t_trans_to_geo_from.geoID', '=', 't_geo_from.ID')
                   ->join([self::TABLE_TO_GEO, 't_trans_to_geo_to'], 'INNER')
                   ->on('t_trans.ID', '=', 't_trans_to_geo_to.requestID')
                   ->on('t_trans_to_geo_to.type', '=', DB::expr('"to"'))
                   ->join([Model_Frontend_Geo::TABLE, 't_geo_to'], 'INNER')
                   ->on('t_trans_to_geo_to.geoID', '=', 't_geo_to.ID');

        // додаємо фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        $result = $query
            ->execute()
            ->as_array();

        return $result[0]['count'];
    }

}