<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Validation
 *
 * @author Сергій Krid
 */
class Model_Frontend_Validation {

    public static function in_array($value, $allowed) {
        return in_array($value, $allowed, true);
    }

    public static function is_float($value) {
        $float_val = floatval($value);

        return ($float_val != 0);
    }

}