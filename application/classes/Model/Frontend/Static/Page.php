<?php defined('SYSPATH') or die('No direct script access.');

/**
        CREATE TABLE IF NOT EXISTS `tirscan_site`.`Frontend_Static_Pages` (
            `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `localeID` INT UNSIGNED NOT NULL,
            `title` VARCHAR(200) NOT NULL,
            `keywords` VARCHAR(400) NOT NULL,
            `description` VARCHAR(400) NOT NULL,
            `content` TEXT NOT NULL,
            `created` TIMESTAMP NOT NULL,
            `uri` VARCHAR(200) NOT NULL,
        PRIMARY KEY (`ID`),
        INDEX `fk_Frontend_Static_Pages_Frontend_Locales1_idx` (`localeID` ASC),
        CONSTRAINT `fk_Frontend_Static_Pages_Frontend_Locales1`
        FOREIGN KEY (`localeID`)
        REFERENCES `tirscan_site`.`Frontend_Locales` (`ID`)
        ON DELETE CASCADE
        ON UPDATE CASCADE)
        ENGINE = InnoDB
 */

class Model_Frontend_Static_Page extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Static_Pages';

    public $_table_name = self::TABLE;

    protected $_belongs_to = [
        'locale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'localeID'
        ]
    ];

    public function rules() {
        return [
            ['not_empty']
        ];
    }

}