<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Company
 *
 */
class Model_Frontend_Comment extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Comments';

    public $_table_name = self::TABLE;

    public static function query_getCommentsForCompany($companyID, $localeID) {
        $result = DB::select(
            't_comm.*',
            ['t_owner.userID', 'authorUserID'],
            ['t_comp.ID', 'authorCompanyID'],
            [DB::expr('IFNULL(`t_loc`.`name`, `t_loc_prim`.`name`)'), 'name']
            )
            ->from([self::TABLE, 't_comm'])
            ->where('t_comm.companyID', '=', $companyID)
            ->join([Model_Frontend_User::TABLE, 't_user'], 'INNER')
            ->on('t_comm.userID', '=', 't_user.ID')
            ->join([Model_Frontend_User::TABLE_TO_COMPANY_OWNERSHIP, 't_owner'], 'INNER')
            ->on('t_user.companyID', '=', 't_owner.companyID')
            ->join([Model_Frontend_Company::TABLE, 't_comp'], 'INNER')
            ->on('t_user.companyID', '=', 't_comp.ID')
            ->join([Model_Frontend_Company_Locale::TABLE, 't_loc'], 'LEFT')
            ->on('t_loc.companyID', '=', 't_comp.ID')
            ->on('t_loc.localeID', '=', DB::expr($localeID))
            ->join([Model_Frontend_Company_Locale::TABLE, 't_loc_prim'], 'LEFT')
            ->on('t_loc_prim.companyID', '=', 't_comp.ID')
            ->on('t_loc_prim.localeID', '=', 't_comp.primaryLocaleID')
            ->execute()
            ->as_array();

        return $result;
    }

    /**
     * @param $userID
     * @param $from null|boolean NULL - всі реквести, TRUE - від моєї компанії | FALSE - на мою копанію
     * @return $this
     */
    public static function query_feedbacks($userID, $from = null){

        $companyID = DB::select('companyID')->from('Frontend_Users')->where('ID','=', $userID)->limit(1)->execute()->as_array();
        $companyID = $companyID[0]['companyID'];

        $query = DB::select()->from([self::TABLE, 'comments'])

            # всі повідомлення від всіх користувачів
            ->join(['Frontend_Users', 'users'], 'INNER')
            ->on('users.ID','=', 'comments.userID');

        if($from === null){

            $query
                ->and_where_open()
                # відправлені на МОЮ компанію
                ->where('comments.companyID', '=', $companyID)

                # або від моєї компанії
                ->or_where('users.companyID','=', $companyID)
                ->and_where_close();

        }else{

            if($from){

                $query
                    ->and_where_open()
                    # або від моєї компанії
                    ->or_where('users.companyID','=', $companyID)
                    ->and_where_close();

            }else{

                $query
                    ->and_where_open()
                    # відправлені на МОЮ компанію
                    ->where('comments.companyID', '=', $companyID)
                    ->and_where_close();

            }

        }

        return $query;

    }

    public static function query_feedbacks_additional($userID, $currentLocaleID){

        $query = self::query_feedbacks($userID);

        $currentLocaleID = (int) $currentLocaleID;

        $query

            ->join(['Frontend_Companies', 'companies'], 'INNER')
            ->on('companies.ID', '=', 'comments.companyID')

            ->join(['Frontend_Company_Locales', 'company_locale'], 'INNER')
            ->on('company_locale.localeID','=', 'companies.primaryLocaleID')
            ->on('company_locale.companyID','=','companies.ID')

            ->join(['Frontend_Company_Locales', 'company_locale2'], 'INNER')
            ->on('company_locale2.localeID','=', DB::expr( $currentLocaleID ))
            ->on('company_locale2.companyID','=','companies.ID')

            ->join(['Frontend_User_Locales','ful_1'], 'LEFT')
            ->on('ful_1.localeID','=',DB::expr( $currentLocaleID ))
            ->on('ful_1.userID','=', 'comments.userID')

            ->join(['Frontend_User_Locales','ful_2'], 'LEFT')
            ->on('ful_2.localeID','=', 'users.primaryLocaleID')
            ->on('ful_2.userID','=', 'comments.userID')

        ;

        return $query;

    }



}