<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Registration_Link
 *
 * @author Сергій Krid
 */
class Model_Frontend_Registration_Link extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Registration_Links';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

    public function initRegistrationLink($type, $casual_password = false) {
        // вираховуєм дату та час "дійсна до"
        $link_datetime = new DateTime('now');
        $link_datetime->add(
            DateInterval::createFromDateString( Kohana::$config->load('frontend/site.user.registration.temp_link_ttl') )
        );

        $this->availableUntil = $link_datetime->format('Y-m-d H:i:s');
        $this->temp = Frontend_Helper_URL::randomString( (int) Kohana::$config->load('frontend/site.user.registration.temp_link_length') );
        $this->type = $type;
        $this->casual_password = $casual_password;

        return $this;
    }

    public static function getNewPassword($id, $temp) {
        $query = DB::select(
            'casual_password'
        )
            ->from(self::TABLE)
            ->where('userID', '=', $id)
            ->and_where('temp', '=', $temp)
            ->execute()
            ->as_array();

        return $query;
    }

    public static function query_deleteOutdatedLinks() {
        DB::delete(self::TABLE)
            ->where('availableUntil', '<', DB::expr('NOW()'))
            ->execute();
    }

}