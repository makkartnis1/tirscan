<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Registration_Link
 *
 */
class Model_Frontend_Registration_ManagerLink extends Model_Frontend_ORM
{

    const TABLE = 'Frontend_Managers_Links';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'company' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'companyID'
        ]
    ];

}