<?php defined('SYSPATH') or die('No direct script access.');

class Model_Frontend_Wallet_History extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Wallet_History';

    protected $_table_name = self::TABLE;

    public static function query_get($userID) {
        $query = DB::select()
            ->from(self::TABLE)
            ->where('userID', '=', $userID)
            ->order_by('created', 'DESC');

        return $query->execute()->as_array();
    }

    public static function query_insertHistory($userID, $type, $description, $amount) {
        try {
            Model_Frontend_Transaction::start();

            $orm_user = ORM::factory('Frontend_User', $userID);
            if (($orm_user->wallet + $amount) < 0) {
                Model_Frontend_Transaction::rollback();
                return false;
            }

            DB::insert(self::TABLE, ['userID', 'type', 'description', 'amount'])
              ->values([$userID, $type, $description, $amount])
              ->execute();

            $orm_user->wallet = $orm_user->wallet + $amount;
            $orm_user->save();

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            throw $e;
            throw new HTTP_Exception_500();
        }
    }

}