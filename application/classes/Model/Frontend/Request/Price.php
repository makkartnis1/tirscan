<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Request_Price
 *
 * @author Сергій Krid
 */
class Model_Frontend_Request_Price extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Request_Prices';

    protected $_table_name = self::TABLE;

    public function getPostName() {
        return 'request_price';
    }

}