<?php defined('SYSPATH') or die('No direct script access.');

class Model_Frontend_Request_Dialog extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Request_Dialogs';

    protected $_table_name = self::TABLE;

    protected static function filterQueryRequest($type, $request_id, Database_Query_Builder_Select $query) {
        if ($type == 'cargo') {
            $query
                ->join([Model_Frontend_User_Cargo_Application::TABLE, 't_app'], 'INNER')
                ->on('t_app.ID', '=', 't_dialog.r_cargoID');

            $query->and_where('t_app.cargoID', '=', $request_id);
        }
        else {
            $query
                ->join([Model_Frontend_User_Transport_Application::TABLE, 't_app'], 'INNER')
                ->on('t_app.ID', '=', 't_dialog.r_transportID');

            $query->and_where('t_app.transportID', '=', $request_id);
        }
    }

    public static function query_getNewForApplicationCount($type, $app_id) {
        if ($type == 'cargo') {
            $result = DB::select('cargoID')
                        ->from(Model_Frontend_User_Cargo_Application::TABLE)
                        ->where('ID', '=', $app_id)
                        ->execute()
                        ->as_array();

            return self::query_getNewForRequestCount($type, $result[0]['cargoID']);
        }
        else {
            $result = DB::select('transportID')
                        ->from(Model_Frontend_User_Transport_Application::TABLE)
                        ->where('ID', '=', $app_id)
                        ->execute()
                        ->as_array();

            return self::query_getNewForRequestCount($type, $result[0]['transportID']);
        }
    }

    public static function query_getNewForPropositionCount($type, $request_id) {
        $query = DB::select([DB::expr('COUNT(t_dialog.ID)'), 'count'])
                   ->from([self::TABLE, 't_dialog'])
                   ->where('t_dialog.status', '=', DB::expr("'new'"))
                   ->and_where('t_dialog.owner', '=', DB::expr("'owner'"));

        self::filterQueryRequest($type, $request_id, $query);

        $result = $query->execute()->as_array();

        return $result[0]['count'];
    }

    public static function query_getNewForRequestCount($type, $request_id) {
        $query = DB::select([DB::expr('COUNT(t_dialog.ID)'), 'count'])
                   ->from([self::TABLE, 't_dialog'])
                   ->where('t_dialog.status', '=', DB::expr("'new'"))
                   ->and_where('t_dialog.owner', '=', DB::expr("'user'"));

        self::filterQueryRequest($type, $request_id, $query);

        $result = $query->execute()->as_array();

        return $result[0]['count'];
    }

    public static function query_getDialogs($type, $request_id) {
        if ($type == 'cargo') {
            $query = DB::select()
                       ->from([Model_Frontend_User_Cargo_Application::TABLE, 't_app'])
                       ->where('t_app.cargoID', '=', $request_id);
        }
        else {
            $query = DB::select()
                       ->from([Model_Frontend_User_Transport_Application::TABLE, 't_app'])
                       ->where('t_app.transportID', '=', $request_id);
        }

        $query->select(
            't_app.userID',
            ['t_uu_loc.name', 'userName']
        )
              ->join([Model_Frontend_User::TABLE, 't_uu'], 'INNER')
              ->on('t_app.userID', '=', 't_uu.ID')
              ->join([Model_Frontend_User_Locale::TABLE, 't_uu_loc'], 'INNER')
              ->on('t_uu_loc.userID', '=', 't_uu.ID')
              ->on('t_uu_loc.localeID', '=', 't_uu.primaryLocaleID')
              ->group_by('userID');

        $result = $query->execute()->as_array();

        return $result;
    }

    public static function query_getLastMessage($type, $request_id, $user_id) {
        $query = DB::select(
            't_dialog.msg',
            't_dialog.created'
        )
                   ->from([self::TABLE, 't_dialog'])
                   ->where('t_dialog.userID', '=', $user_id)
                   ->order_by('created', 'DESC')
                   ->limit(1);

        self::filterQueryRequest($type, $request_id, $query);

        $result = $query->execute()->as_array();

        if (count($result) > 0) {
            return $result[0];
        }
        else {
            return ['msg' => '', 'created' => ''];
        }
    }

    public static function query_markRead($type, $sender_type, $request_id, $user_id) {
        if ($sender_type == 'prop') {
            $condition = 'owner';
        }
        else {
            $condition = 'user';
        }

        try {
            if ($type == 'cargo') {
                $query = DB::select('ID')
                           ->from(Model_Frontend_User_Cargo_Application::TABLE)
                           ->where('cargoID', '=', $request_id);
            }
            else {
                $query = DB::select('ID')
                           ->from(Model_Frontend_User_Transport_Application::TABLE)
                           ->where('transportID', '=', $request_id);
            }
            $query_result = $query
                ->and_where('userID', '=', $user_id)
                ->execute()
                ->as_array();

            $app_id = $query_result[0]['ID'];

            $query_update = DB::update([self::TABLE, 't_dialog'])
                       ->set(['status' => DB::expr("'old'")])
                       ->where('userID', '=', $user_id)
                       ->and_where('owner', '=', $condition);

            if ($type == 'cargo') {
                $query_update
                    ->and_where('r_cargoID', '=', $app_id);
            }
            else {
                $query_update
                    ->and_where('r_transportID', '=', $app_id);
            }

            $query_update->execute();
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    public static function query_getMessages($type, $request_id, $user_id) {
        $query = DB::select(
            't_dialog.msg',
            't_dialog.userID',
            't_dialog.ownerID',
            ['t_uu_loc.name', 'userName'],
            ['t_ou_loc.name', 'ownerName'],
            't_dialog.created',
            't_dialog.msg',
            't_dialog.owner'
        )
                   ->from([self::TABLE, 't_dialog'])
                   ->join([Model_Frontend_User::TABLE, 't_uu'], 'INNER')
                   ->on('t_dialog.userID', '=', 't_uu.ID')
                   ->join([Model_Frontend_User_Locale::TABLE, 't_uu_loc'], 'INNER')
                   ->on('t_uu_loc.userID', '=', 't_uu.ID')
                   ->on('t_uu_loc.localeID', '=', 't_uu.primaryLocaleID')
                   ->join([Model_Frontend_User::TABLE, 't_ou'], 'INNER')
                   ->on('t_dialog.ownerID', '=', 't_ou.ID')
                   ->join([Model_Frontend_User_Locale::TABLE, 't_ou_loc'], 'INNER')
                   ->on('t_ou_loc.userID', '=', 't_ou.ID')
                   ->on('t_ou_loc.localeID', '=', 't_ou.primaryLocaleID')
                   ->where('t_dialog.userID', '=', $user_id)
                   ->order_by('created', 'DESC');

        self::filterQueryRequest($type, $request_id, $query);

        $result = $query->execute()->as_array();

        return $result;
    }

    public static function query_sendMessage($type, $request_id, $owner, $msg, $owner_id, $user_id) {
        if ($type == 'cargo') {
            $app_result = DB::select('ID')
                            ->from(Model_Frontend_User_Cargo_Application::TABLE)
                            ->where('cargoID', '=', $request_id)
                            ->and_where('userID', '=', $user_id)
                            ->execute()
                            ->as_array();

            $r_cargoID     = $app_result[0]['ID'];
            $r_transportID = null;
        }
        else {
            $app_result = DB::select('ID')
                            ->from(Model_Frontend_User_Transport_Application::TABLE)
                            ->where('transportID', '=', $request_id)
                            ->and_where('userID', '=', $user_id)
                            ->execute()
                            ->as_array();

            $r_cargoID     = null;
            $r_transportID = $app_result[0]['ID'];
        }

        DB::insert(self::TABLE, ['r_cargoID', 'r_transportID', 'owner', 'status', 'msg', 'ownerID', 'userID'])
          ->values([$r_cargoID, $r_transportID, $owner, 'new', $msg, $owner_id, $user_id])
          ->execute();
    }
}