<?php defined('SYSPATH') or die('No direct script access.');


class Model_Frontend_Complaints extends Model_Frontend_ORM
{
    const TABLE = 'Frontend_Complaints';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'company' => [
            'model' => 'Frontend_Company',
            'foreign_key' => 'companyID'
        ],
        'user' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'userID'
        ]
    ];

    public function rules(){
        $rules = [
            'message' => [
                [ 'not_empty' ]
            ]
        ];

        return $rules;
    }

//    public static function addMessage($id_from, $message, $complain_company_id) {
//        $query = DB::insert(self::TABLE, ['userID', 'message', 'complainCompanyID', 'status'])
//            ->values([$id_from, $message, $complain_company_id, 'unread']);
//
//        return $query->execute();
//    }

    public function getMessages($id_user) {
        $query = DB::select('*')
            ->from(self::TABLE)
            ->where('userID', '=', $id_user)
            ->order_by('created', 'DESC')
            ->execute();

        return $query;
    }

}