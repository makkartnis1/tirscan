<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Company
 *
 * @author Сергій Krid
 */
class Model_Frontend_Company extends Model_Frontend_ORM {

    const TABLE        = 'Frontend_Companies';
    const PARTNER_POSSIBLE_TO_BE                    = 'this is not not my partner';
    const PARTNER_NOT_COFIRMED_YET                  = 'this partner must confirm your request';
    const PARTNER_IS_MINE                           = 'this partner is already my partner';
    const PARTNER_WAIT_YOUR_CONFIRMATOIN            = 'this partner send you request. confirm it';

    const TABLE_TO_GEO = 'Frontend_Company_to_Geo_Relation';

    protected $_table_name = self::TABLE;

    protected $_belongs_to = [
        'primaryLocale' => [
            'model' => 'Frontend_Locale',
            'foreign_key' => 'primaryLocaleID'
        ],
        'type' => [
            'model' => 'Frontend_Company_Type',
            'foreign_key' => 'typeID'
        ],
        'ownershipType' => [
            'model' => 'Frontend_Company_Ownership_Type',
            'foreign_key' => 'ownershipTypeID'
        ],
        'phoneCode' => [
            'model' => 'Frontend_Phonecodes',
            'foreign_key' => 'phoneCodeID'
        ],
        'phoneStationaryCode' => [
            'model' => 'Frontend_Phonecodes',
            'foreign_key' => 'phoneStationaryCodeID'
        ]
    ];

    protected $_has_many = [
        'users' => [
            'model' => 'Frontend_User',
            'foreign_key' => 'companyID'
        ],
        'locales' => [
            'model' => 'Frontend_Company_Locale',
            'foreign_key' => 'companyID'
        ]
    ];

    /** @var null|Model_Frontend_User_Locale */
    protected $_locale = null;

    public function rules() {
        return [
            'phone' => [
                ['not_empty'],
                ['numeric']
            ],
            'phoneStationary' => [
                ['numeric']
            ],
            'url' => [
                ['regex', [':value', '/^[a-zA-Z0-9\-\/]+$/i']]
            ]
        ];
    }

    public function hasRights($right) {
        if (! $this->loaded()) // якщо користувач не підтянутий з бази, то доступу у нього немає
            return false;

        $group = Kohana::$config->load( 'frontend/groups.' . $this->type->ID );

        return ( is_null($group) ) ? false : in_array($right, $group);
    }

    public function addGeoRelationByIDs(array $ids) {
        foreach ($ids as $id) {
            DB::insert(self::TABLE_TO_GEO, ['geoID', 'companyID'])
              ->values([$id, $this->ID])
              ->execute();
        }
    }

    /**
     * @param Model_Frontend_Locale $site_locale
     * @return Model_Frontend_User_Locale
     */
    public function getLocale(Model_Frontend_Locale $site_locale) {
        if (is_null($this->_locale)) { // lazy load
            // CASE для сортування (шукана мова в пріоритеті)
            $case_str = sprintf('CASE WHEN `localeID` = %d THEN 2 ELSE 1 END', $site_locale->ID);

            $this->_locale = $this->locales
                ->and_where_open()
                ->where('localeID', '=', $site_locale->ID)
                ->or_where('localeID', '=', $this->primaryLocaleID)
                ->and_where_close()
                ->order_by(
                    DB::expr($case_str),
                    'ASC')
                ->find();
        }

        return $this->_locale;
    }

    public function getGeoFullAddress(Model_Frontend_Locale $locale) {
        $query = DB::select('geoID')
                   ->from(self::TABLE_TO_GEO)
                   ->where('companyID', '=', $this->ID);

        $geo_result = DB::select('ID')
                        ->from(Model_Frontend_Geo::TABLE)
                        ->where('ID', 'IN', $query)
                        ->order_by('level', 'DESC')
                        ->limit(1)
                        ->execute()
                        ->as_array();

        foreach ($geo_result as &$result) {
            $result = $result['ID'];
            unset($result);
        }

        $query_geo_locale = DB::select('fullAddress')
                              ->from(Model_Frontend_Geo_Locale::TABLE)
                              ->where('geoID', 'IN', $geo_result)
                              ->and_where('localeID', '=', $locale->ID)
                              ->execute()
                              ->as_array();

        // TODO: переклади якщо не існує

        return $query_geo_locale[0]['fullAddress'];
    }

    public function addGeoRelation(Model_Frontend_Geo $geo) {
        DB::insert(self::TABLE_TO_GEO, ['geoID', 'companyID'])
          ->values([$geo->ID, $this->ID])
          ->execute();
    }

    public static function query_getCompaniesCount() {
        $query_count = DB::select([DB::expr('COUNT(*)'), 'count'])
            ->from(self::TABLE)
            ->execute()
            ->as_array();

        return (int) $query_count[0]['count'];
    }

    public static function query_getCompaniesWithLocales($select_count, $localeID, $geoSearch, $limit, $offset, array $filters = [], array $sorts = []) {
        $query_companies = DB::select(
            ['t_comp.ID', 'ID'],
            'registered',

            'url',

            // вибираєм або потрібну локалізацію або (якщо вона не існує) локалізацію за замовчуванням
            // (на якій реєструвались - завжди існує)
            [DB::expr('IFNULL(`t_loc`.`name`, `t_loc_prim`.`name`)'), 'name'],
            [DB::expr('IFNULL(`t_loc`.`address`, `t_loc_prim`.`address`)'), 'address'],
            [DB::expr('IFNULL(`t_loc`.`realAddress`, `t_loc_prim`.`realAddress`)'), 'realAddress'],
            [DB::expr('IFNULL(`t_loc`.`info`, `t_loc_prim`.`info`)'), 'info'],

            [DB::expr('`t_comp`.`votesPositive` - `t_comp`.`votesNegative`'), 'votesDiff'],
            [DB::expr('`t_comp`.`votesPositive` + `t_comp`.`votesNegative`'), 'votesSumm'],

            // назва на локалізації за замовчуванням
            ['t_loc_prim.name', 'primaryName'],

            // місце розташування компанії
            [DB::expr('IFNULL(`t_geo_loc`.`fullAddress`, `t_geo_loc_prim`.`fullAddress`)'), 'fullAddress'],

            // тип компанії
            'typeID',
            ['t_comp_types.localeName', 'typeLocale'],

            // вибираємо номер телефону та код країни
            ['t_ph_codes.code', 'phoneCode'],
            't_comp.phone',

            // формуємо повний номер телефону (з кодом)
            [DB::expr('CONCAT(`t_ph_codes`.`code`, `t_comp`.`phone`)'), 'phoneFull'],

            // вибираємо placeID компаній (якщо фільтр по GEO вказаний, інакше не вибираєм
            ($geoSearch) ? 't_geo.placeID' : DB::expr('0')
        )
                             ->from([self::TABLE, 't_comp'])
                             ->join([Model_Frontend_Company_Locale::TABLE, 't_loc'], 'LEFT')
                             ->on('t_loc.companyID', '=', 't_comp.ID')
                             ->on('t_loc.localeID', '=', DB::expr($localeID))
                             ->join([Model_Frontend_Company_Locale::TABLE, 't_loc_prim'], 'LEFT')
                             ->on('t_loc_prim.companyID', '=', 't_comp.ID')
                             ->on('t_loc_prim.localeID', '=', 't_comp.primaryLocaleID')
                             ->join([Model_Frontend_Company_Type::TABLE, 't_comp_types'], 'LEFT')// join типу компанії
                             ->on('t_comp.typeID', '=', 't_comp_types.ID')
                             ->join([Model_Frontend_Phonecodes::TABLE, 't_ph_codes'], 'LEFT')// join коду основного номеру
                             ->on('t_comp.phoneCodeID', '=', 't_ph_codes.ID')
                             ->join([self::TABLE_TO_GEO, 't_comp_to_geo_max'], 'INNER')// join місця розташування найнижчого рівня (місто, GEO)
                             ->on('t_comp.ID', '=', 't_comp_to_geo_max.companyID')
                             ->join([Model_Frontend_Geo::TABLE, 't_geo_max'], 'INNER')
                             ->on('t_comp_to_geo_max.geoID', '=', 't_geo_max.ID')
                             ->join([Model_Frontend_Geo_Locale::TABLE, 't_geo_loc'], 'LEFT')// join локалізації цього рівня (GEO)
                             ->on('t_geo_max.ID', '=', 't_geo_loc.geoID')
                             ->on('t_geo_loc.localeID', '=', DB::expr($localeID))
                             ->join([Model_Frontend_Geo_Locale::TABLE, 't_geo_loc_prim', 'LEFT'])// join локалізації цього рівня на мові за замовчуванням (GEO)
                             ->on('t_geo_max.ID', '=', 't_geo_loc_prim.geoID')
                             ->on('t_geo_loc_prim.localeID', '=', 't_comp.primaryLocaleID')
                             ->order_by('t_comp.ID', 'ASC')
                             ->order_by('t_geo_max.level', 'DESC'); // і залишаєм лише запис з максимальним значенням level (місто, GEO)

        // якщо переданий фільтр по GEO
        if ($geoSearch) {
            $query_companies
                ->join([self::TABLE_TO_GEO, 't_comp_to_geo'], 'LEFT')// join місця розташування (GEO)
                ->on('t_comp.ID', '=', 't_comp_to_geo.companyID')
                ->join([Model_Frontend_Geo::TABLE, 't_geo'], 'LEFT')
                ->on('t_comp_to_geo.geoID', '=', 't_geo.ID');
        }

        $query = DB::select()
                   ->from([$query_companies, 't_join'])
                   ->group_by('ID'); // групуєм записи по ID

        // додаєм фільтри
        Model_Frontend_Helper::addFilterToQuery($query, $filters);

        // додаєм сортування
        Model_Frontend_Helper::addSortingToQuery($query, $sorts);

        if (!$select_count) {
            $query
                ->limit($limit)
                ->offset($offset);
            
            return $query
                ->execute()
                ->as_array();
        }

        return count($query
                         ->execute()
                         ->as_array());
    }

    public static function query_getCompanyRating($companyID) {
        $positive_comments = DB::select([DB::expr('COUNT(DISTINCT(t_coms.ID))'), 'count'])
                               ->from([Model_Frontend_Comment::TABLE, 't_coms'])
                               ->where('t_coms.companyID', '=', $companyID)
                               ->and_where('t_coms.rating', '=', DB::expr('"positive"'))
                               ->execute()
                               ->as_array();

        $result['positive'] = $positive_comments[0]['count'];

        $negative_comments = DB::select([DB::expr('COUNT(DISTINCT(t_coms.ID))'), 'count'])
                               ->from([Model_Frontend_Comment::TABLE, 't_coms'])
                               ->where('t_coms.companyID', '=', $companyID)
                               ->and_where('t_coms.rating', '=', DB::expr('"negative"'))
                               ->execute()
                               ->as_array();

        $result['negative'] = $negative_comments[0]['count'];

        return $result;
    }

    public static function query_getCompanyByID($localeID, $ID) {
        $query_companies = DB::select(
            ['t_comp.ID', 'ID'],

            'user.registered',
            ['user.ID', 'userID'],

            // вибираєм або потрібну локалізацію або (якщо вона не існує) локалізацію за замовчуванням
            // (на якій реєструвались - завжди існує)
            [DB::expr('IFNULL(`t_loc`.`name`, `t_loc_prim`.`name`)'), 'name'],
            [DB::expr('IFNULL(`t_loc`.`address`, `t_loc_prim`.`address`)'), 'address'],
            [DB::expr('IFNULL(`t_loc`.`realAddress`, `t_loc_prim`.`realAddress`)'), 'realAddress'],
            [DB::expr('IFNULL(`t_loc`.`info`, `t_loc_prim`.`info`)'), 'info'],

            // назва на локалізації за замовчуванням
            ['t_loc_prim.name', 'primaryName'],

            // місце розташування компанії
            [DB::expr('IFNULL(`t_geo_loc`.`fullAddress`, `t_geo_loc_prim`.`fullAddress`)'), 'fullAddress'],

            // тип компанії
            'typeID',
            ['t_comp_types.localeName', 'typeLocale'],

            // вибираємо номер телефону та код країни
            ['t_ph_codes.code', 'phoneCode'],
            't_comp.phone',

            // формуємо повний номер телефону (з кодом)
            [DB::expr('CONCAT(`t_ph_codes`.`code`, `t_comp`.`phone`)'), 'phoneFull']
        )
                             ->from([self::TABLE, 't_comp'])
                             ->join([Model_Frontend_Company_Locale::TABLE, 't_loc'], 'LEFT')
                             ->on('t_loc.companyID', '=', 't_comp.ID')
                             ->on('t_loc.localeID', '=', DB::expr($localeID))
                             ->join([Model_Frontend_Company_Locale::TABLE, 't_loc_prim'], 'LEFT')
                             ->on('t_loc_prim.companyID', '=', 't_comp.ID')
                             ->on('t_loc_prim.localeID', '=', 't_comp.primaryLocaleID')
                             ->join([Model_Frontend_Company_Type::TABLE, 't_comp_types'], 'LEFT')// join типу компанії
                             ->on('t_comp.typeID', '=', 't_comp_types.ID')
                             ->join([Model_Frontend_Phonecodes::TABLE, 't_ph_codes'], 'LEFT')// join коду основного номеру
                             ->on('t_comp.phoneCodeID', '=', 't_ph_codes.ID')
                             ->join([Model_Frontend_User::TABLE, 'user'], 'LEFT')// join id user
                             ->on('t_comp.ID', '=', 'user.companyID')
                             ->join([self::TABLE_TO_GEO, 't_comp_to_geo_max'], 'INNER')// join місця розташування найнижчого рівня (місто, GEO)
                             ->on('t_comp.ID', '=', 't_comp_to_geo_max.companyID')
                             ->join([Model_Frontend_Geo::TABLE, 't_geo_max'], 'INNER')
                             ->on('t_comp_to_geo_max.geoID', '=', 't_geo_max.ID')
                             ->join([Model_Frontend_Geo_Locale::TABLE, 't_geo_loc'], 'LEFT')// join локалізації цього рівня (GEO)
                             ->on('t_geo_max.ID', '=', 't_geo_loc.geoID')
                             ->on('t_geo_loc.localeID', '=', DB::expr($localeID))
                             ->join([Model_Frontend_Geo_Locale::TABLE, 't_geo_loc_prim', 'LEFT'])// join локалізації цього рівня на мові за замовчуванням (GEO)
                             ->on('t_geo_max.ID', '=', 't_geo_loc_prim.geoID')
                             ->on('t_geo_loc_prim.localeID', '=', 't_comp.primaryLocaleID')
                             ->order_by('t_comp.ID', 'ASC')
                             ->order_by('t_geo_max.level', 'DESC'); // і залишаєм лише запис з максимальним значенням level (місто, GEO)

        $query = DB::select()
                   ->from([$query_companies, 't_join'])
                   ->group_by('ID')// групуєм записи по ID
                   ->where('ID', '=', $ID)
                   ->limit(1);

        $result = $query
            ->execute()
            ->as_array();

        return $result[0];
    }

    public static function query_companyInfo_for_editor($userID) {

        $query = DB::select()
                   ->from(['Frontend_Users', 'fu'])
                   ->where('fu.ID', '=', $userID)
                   ->join(['Frontend_Companies', 'fc'])
                   ->on('fc.ID', '=', 'fu.companyID')
                   ->join(['Frontend_Locales', 'fl'], 'RIGHT')
                   ->on(DB::expr(1), '=', DB::expr(1))
                   ->join(['Frontend_Company_Locales', 'fcl'], 'LEFT')
                   ->on('fcl.companyID', '=', 'fc.ID')
                   ->on('fcl.localeID', '=', 'fl.ID')
                   ->join(['Frontend_User_to_Company_Ownership', 'fuco'], 'LEFT')
                   ->on('fuco.userID', '=', 'fu.ID')
                   ->on('fuco.companyID', '=', 'fu.companyID')
                   ->select(
                       ['fc.typeID', 'Frontend_Companies[typeID]'],
                       ['fc.url', 'Frontend_Companies[url]'],
                       ['fc.ipn', 'Frontend_Companies[ipn]'],
                       ['fc.ownershipTypeID', 'Frontend_Companies[ownershipTypeID]'],
                       ['fc.phone', 'Frontend_Companies[phone]'],
                       ['fc.phoneCodeID', 'Frontend_Companies[phoneCodeID]'],
                       ['fc.phoneStationary', 'Frontend_Companies[phoneStationary]'],
                       ['fc.phoneCodeID', 'Frontend_Companies[phoneStationaryCodeID]'],
                       ['fc.primaryLocaleID', 'Frontend_Companies[primaryLocaleID]']
                   )
                   ->select(
                       ['fcl.name', 'Frontend_Company_Locales[:%LOCALE-ID%:][name]'],
                       ['fcl.address', 'Frontend_Company_Locales[:%LOCALE-ID%:][address]'],
                       ['fcl.realAddress', 'Frontend_Company_Locales[:%LOCALE-ID%:][realAddress]'],
                       ['fcl.info', 'Frontend_Company_Locales[:%LOCALE-ID%:][info]']
                   )
                   ->select(['fl.ID', ':%LOCALE-ID%:'])
                   ->select(['fuco.ID', 'CAN_MODIFY_IF_NOT_NULL']);

        $query = $query->execute()
                       ->as_array();

        $result = [];

        $can_modify = false;

        foreach ($query as $row) {

            $can_modify = ($row['CAN_MODIFY_IF_NOT_NULL'] !== null) ? 'yes' : 'no';

            foreach ($row as $column => $value) {


                $strpos = strpos($column, ':%LOCALE-ID%:');

                if (($strpos === 0) OR $column == 'CAN_MODIFY_IF_NOT_NULL') {
                    continue;
                }


                if ($strpos) // якщо знайдено і це в 0 позиції
                {
                    $column = str_replace(':%LOCALE-ID%:', $row[':%LOCALE-ID%:'], $column);
                }

                $result[$column] = $value;

            }

        }

        return ['form' => $result, 'isOwner' => $can_modify];

    }


    //  -   -   -   -   -   -   -   LINKeR >>> запити по роботі із "партнерством" між компаніями

    protected static function query_partner_type($companyFromID, $companyToID = null){

        $sel = DB::select()

            ->select(['fcp_from.id','fid'],['fcp_from.company1ID','fid1'],['fcp_from.company2ID','fid2'])
            ->select(['fcp_to.id','tid'],['fcp_to.company1ID','tid1'],['fcp_to.company2ID','tid2'])

            ->from(['Frontend_Company_Partners', 'fcp_from'])

            ->join(['Frontend_Company_Partners', 'fcp_to'], 'LEFT')
            ->on('fcp_from.company2ID','=', 'fcp_to.company1ID')
;


        $sel
            ->and_where_open()
            ->where('fcp_from.company1ID',  '=', $companyFromID)
            ->where('fcp_from.company2ID',  '=', $companyToID)
            ->and_where_close();

        return $sel;

    }

    public static function query_who_is_this_company($myCompanyID, $anyOtherComanyID){
        
        $sel = DB::select()
            ->from(['Frontend_Company_Partners', 'fcp_from'])

            ->or_where_open()
            ->where('company1ID',       '=', $myCompanyID)
            ->and_where('company2ID',   '=', $anyOtherComanyID)
            ->or_where_close()

            ->or_where_open()
            ->where('company1ID',       '=', $anyOtherComanyID)
            ->and_where('company2ID',   '=', $myCompanyID)
            ->or_where_close()
            ->execute()
            ->as_array();
        ;
        
        if(empty($sel)){

            return self::PARTNER_POSSIBLE_TO_BE;

        }

        if(count($sel) == 2){

            return self::PARTNER_IS_MINE;

        }

        if( count($sel) == 1 ){

            if( $sel[0]['company1ID'] == $myCompanyID ){

                return self::PARTNER_NOT_COFIRMED_YET;

            }else{

                return self::PARTNER_WAIT_YOUR_CONFIRMATOIN;

            }

        }

//        return $sel->execute()->as_array();
//
//        $res = self::query_partner_type($myCompanyID, $anyOtherComanyID)->execute()->as_array();
//
//        if(empty($res))
//            return self::PARTNER_POSSIBLE_TO_BE;
//
//        if( empty($res[0]['tid']) )
//            return self::PARTNER_NOT_COFIRMED_YET;
//
//        return self::PARTNER_IS_MINE;

    }

    protected static function _query_my_partners_do_joinInfo(Database_Query_Builder_Select $select, $locale_ID, $targetTableAndColumn){
        
        $select
            ->join(['Frontend_Companies', 'fc'])
            ->on('fc.ID','=',$targetTableAndColumn)

            ->join(['Frontend_Locales', 'fl'], 'RIGHT')
            ->on(DB::expr(1),'=',DB::expr(1))

            ->join(['Frontend_Company_Locales','fcl'], 'LEFT')
            ->on('fcl.companyID','=','fc.ID')
            ->on('fcl.localeID','=','fl.ID')

            ->join(['Frontend_Company_Locales','fcl_primary'], 'LEFT')
            ->on('fcl_primary.companyID','=','fc.ID')
            ->on('fcl_primary.localeID','=','fc.primaryLocaleID')

            ->join(['Frontend_User_to_Company_Ownership', 'fuco'],'LEFT')
            ->on('fuco.companyID','=',$targetTableAndColumn)


            ->group_by($targetTableAndColumn)
            ->where('fl.ID','=', $locale_ID)

            ->select([$targetTableAndColumn, 'companyID'])
            ->select([DB::expr('IFNULL(`fcl`.`name`, `fcl_primary`.`name`)'), 'visibleName'])
            ->select(['fcl_primary.name', 'primaryName'])
            ->select(['fuco.userID','ownerID'])
        ;

        return $select;
    }

    /**
     * @param $ID
     * @param string $type
     * @return Database_Query_Builder_Select
     */
    protected static function _query_my_partners_($ID, $type = ''){

        return $sel = DB::select()

            ->from(['Frontend_Company_Partners', 'target'])

            ->join(['Frontend_Company_Partners', 'fcp_to'], 'LEFT')
            ->on('target.company2ID','=', 'fcp_to.company1ID')

            ->and_where_open()
            ->where('target.company1ID',  '=', $ID)
            ->where('fcp_to.company1ID',  "IS {$type}", NULL)
            ->and_where_close();

    }

    public static function query_my_partners_confirmed($myCompanyID, $if_False_ThenCount_ElsePutHereCurrentLocaleID = false){

        $if_False_ThenCount_ElsePutHereCurrentLocaleID = (int) $if_False_ThenCount_ElsePutHereCurrentLocaleID;

        if(!$if_False_ThenCount_ElsePutHereCurrentLocaleID){

            return self::_query_my_partners_($myCompanyID, 'NOT')
                ->select([DB::expr('COUNT(*)'),'count'])
                ->execute()
                ->as_array();

        }

        return self::_query_my_partners_do_joinInfo(

            self::_query_my_partners_($myCompanyID, 'NOT'),

            $if_False_ThenCount_ElsePutHereCurrentLocaleID,

            'target.company2ID'

        );//->execute()->as_array();

    }

    public static function query_my_partners_request_waiting_for_mine_confirmation($myCompanyID, $if_False_ThenCount_ElsePutHereCurrentLocaleID = false){

        $sel = DB::select()
//            ->select(['target.id','fid'],['target.company1ID','fid1'],['target.company2ID','fid2'])
//            ->select(['fcp_to.id','tid'],['fcp_to.company1ID','tid1'],['fcp_to.company2ID','tid2'])
//            ->select(['fcp_controlnyj.id','cid'],['fcp_controlnyj.company1ID','cid1'],['fcp_controlnyj.company2ID','cid2'])

            ->from(['Frontend_Company_Partners', 'target'])

            ->join(['Frontend_Company_Partners', 'fcp_to'], 'LEFT')
            ->on('target.company2ID','=', 'fcp_to.company1ID')
            ->on('target.company1ID','=', 'fcp_to.company2ID')

            ->where('target.company2ID',  '=', $myCompanyID)
            ->where('fcp_to.ID',  'IS', NULL);

        $if_False_ThenCount_ElsePutHereCurrentLocaleID = (int) $if_False_ThenCount_ElsePutHereCurrentLocaleID;

        if(!$if_False_ThenCount_ElsePutHereCurrentLocaleID){

            return $sel
                ->select([DB::expr('COUNT(*)'),'count'])
                ->execute()
                ->as_array();

        }

        return self::_query_my_partners_do_joinInfo(

            $sel,

            $if_False_ThenCount_ElsePutHereCurrentLocaleID,

            'target.company1ID'


        );//->execute()->as_array();

    }
    
    public static function query_my_partners_request_waiting_for_their_confirmation($myCompanyID, $if_False_ThenCount_ElsePutHereCurrentLocaleID = false){

        $if_False_ThenCount_ElsePutHereCurrentLocaleID = (int) $if_False_ThenCount_ElsePutHereCurrentLocaleID;

        if(!$if_False_ThenCount_ElsePutHereCurrentLocaleID){


            return self::_query_my_partners_($myCompanyID)
                ->select([DB::expr('COUNT(*)'),'count'])
                ->execute()
                ->as_array();

        }

        return self::_query_my_partners_do_joinInfo(

            self::_query_my_partners_($myCompanyID),

            $if_False_ThenCount_ElsePutHereCurrentLocaleID,

            'target.company2ID'


        );//->execute()->as_array();

    }



    public static function query_post_partner_request($from, $to){

        return DB::insert('Frontend_Company_Partners', ['company1ID', 'company2ID'])
            ->values([$from, $to])->execute();

    }

    public static function query_break_partner_request($oneCompanyID, $otherCompanyID){

        $delete = DB::delete('Frontend_Company_Partners')

            ->or_where_open()
            ->where('company1ID',       '=', $oneCompanyID)
            ->and_where('company2ID',   '=', $otherCompanyID)
            ->or_where_close()

            ->or_where_open()
            ->where('company1ID',       '=', $otherCompanyID)
            ->and_where('company2ID',   '=', $oneCompanyID)
            ->or_where_close();
        
        return $delete->execute();

    }

    public static function query_get_user_company_subscribe_request($userID, $companyID){
        return DB::select()
            ->from(['User_Company_Subscribe', 'ucs'])
            ->and_where_open()
            ->where('ucs.userID',  '=', $userID)
            ->where('ucs.companyID', '=',  $companyID)
            ->and_where_close()
            ->execute()
            ->as_array();
    }

    public static function query_user_company_subscribe_request($userID, $companyID){
        return DB::insert('User_Company_Subscribe', ['userID', 'companyID' ])
            ->values([$userID, $companyID])->execute();
    }

    public static function query_user_company_unsubscribe_request($userID, $companyID){
        return DB::delete('User_Company_Subscribe')
            ->where('userID', '=', $userID)
            ->where('companyID', '=', $companyID)
            ->execute();
    }

    public static function query_get_transports_cargo_subscribe_request($userID, $type = 'transport'){
        $sel = DB::select();
        if ($type == 'transport') {
            $sel->select(['t.ID', 'ID']);
        }
        if ($type == 'cargo') {
            $sel->select(['c.ID', 'ID']);
        }
        $sel->from(['User_Company_Subscribe', 'ucs'])
            ->join(['Frontend_Companies', 'fc'], 'INNER')->on('ucs.companyID','=', 'fc.ID')
            ->join(['Frontend_Users', 'u'], 'INNER')->on('fc.ID','=', 'u.companyID');
        if ($type == 'transport') {
            $sel->join(['Frontend_Transports', 't'], 'INNER')->on('u.ID', '=', 't.userID');
        }
        if ($type == 'cargo') {
            $sel->join(['Frontend_Cargo', 'c'], 'INNER')->on('u.ID', '=', 'c.userID');
        }
        $sel->where('ucs.userID',  '=', $userID);

        if ($type == 'transport') {
            $sel->where('t.status', 'LIKE', 'active%');
        }
        if ($type == 'cargo') {
            $sel->where('c.status', 'LIKE', 'active%');
        }

        return $sel->execute()->as_array();
    }

    //  -   -   -   -   -   -   -   запити по роботі із "партнерством" між компаніями <<<   LINKeR

}