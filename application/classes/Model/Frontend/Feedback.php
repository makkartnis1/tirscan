<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Feedback
 *
 * @author
 */
class Model_Frontend_Feedback extends Model_Frontend_ORM {

    const TABLE = 'Frontend_Feedback';

    protected $_table_name = self::TABLE;

    public function rules(){
        $rules = [
            'message' => [
                [ 'not_empty' ]
            ],
            'email' => [
                [ 'not_empty' ]
            ]
        ];

        return $rules;
    }

}