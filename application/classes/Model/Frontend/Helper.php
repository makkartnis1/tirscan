<?php

/**
 * Class Model_Frontend_Helper
 *
 * @author Сергій Krid
 */
class Model_Frontend_Helper {

    /**
     * Будує where умови для запиту на основі масиву фільтрів
     *
     * Приклад $filters:
     * [ ['id', '=', 1], ['created', '>', '2016-01-01'] ]   ->   WHERE (`id` = 1) AND (`created` > '2016-01-01)
     *
     * Якщо ключі елементу масиву $filters теж є масивами умов, то такі умови будуть погруповані через OR між собою
     *
     * @param Database_Query_Builder_Where $query
     * @param array $filters
     */
    public static function addFilterToQuery(Database_Query_Builder_Where $query, array $filters = []) {
        if (count($filters) > 0) {
            $query->and_where_open();
            foreach ($filters as $filter) {
                if ( is_array($filter[0]) ) { // якщо є підумови
                    $query->and_where_open(); // відкриваєм групу через AND
                    foreach ($filter as $or_filter) {
                        $query->or_where($or_filter[0], $or_filter[1], $or_filter[2]); // групуєм їх через OR
                    }
                    $query->and_where_close();
                }
                else { // інакше додаєм ще одну умову через AND
                    $query->and_where($filter[0], $filter[1], $filter[2]);
                }
            }
            $query->and_where_close();
        }
    }

    public static function addSortingToQuery(Database_Query_Builder_Where $query, array $sorts = []) {
        if (count($sorts) > 0) {
            foreach ($sorts as $sort) {
                $query->order_by($sort[0], $sort[1]);
            }
        }
    }

}