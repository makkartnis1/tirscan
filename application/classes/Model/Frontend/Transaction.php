<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Model_Frontend_Transaction
 *
 * @author Сергій Krid
 */
class Model_Frontend_Transaction {

    public static function start() {
        DB::query(null, 'START TRANSACTION')->execute();
    }

    public static function commit() {
        DB::query(null, 'COMMIT')->execute();
    }

    public static function rollback() {
        DB::query(null, 'ROLLBACK')->execute();
    }

}