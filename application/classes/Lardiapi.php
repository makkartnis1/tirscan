<?php defined('SYSPATH') or die('No direct script access.');

/**
 * класс для работы с api.lardi-trans.com
 * 
 * @author lardi-trans.com admin@lardi-trans.com
 * @version 1.0
 * @link http://api.lardi-trans.com/php/
 */
class Lardiapi {

	/**
     * url адрес api сервера
     * @access private
     * @var url 
     */
	private $apiurl;
	
	/**
     * Уникальный номер, аналог PHP сессии, выданный при авторизации методом {@link auth}
     * @access private 
     * @var string 
     */
	private $sig;
	
	/**
     * Основной номер фирмы
     * @access private
     * @var integer 
     */	
	private $uid;
	
	/**
     * Конструктор устанавливает переменную {@link $apiurl}
     */	
	function __construct(){		
		$this->apiurl = 'http://api.lardi-trans.com/api/';
	}

	/**
	 * Задает новый url-адрес сервера API
	 * @param URL $url URL-ссылка на сервер API 
	 */
	function setAPIUrl($url)
	{
		$this->apiurl = $url;
	}
	
	/**
	 * Получение MD5-суммы переданной строки
	 * @param mixed $str Строка, MD5-сумму которой нужно получить
	 * @return string MD5-сумма переданной строки  
	 */
	function getMD5($str)
	{
		return md5($str);
	}
	
	/**
	 * Получение внутренней переменной {@link $sig}
	 * @return sig {@link $sig}
	 */	
	function getSig()
	{
		return $this->sig;
	}
	
	/**
	 * Получение внутренней переменной {@link $uid}
	 * @return uid {@link $uid}
	 */	
	function getUid()
	{
		return $this->uid;
	}	

	/**
	 * Тестовая функция для проверки работоспособности, времени на сервере, кодировки
	 * @param string $str Слово, для проверки правильности восприятия русских букв
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/test
	 */
	function test($str)
	{
		$data='method=test&test_text='.$str;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<testfield>")===false) {
			throw new Exception("Test error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}	
	}	
	
	/**
	 * Функция для авторизации, если не произошло ошибки, то устанавливает {@link $sig} и {@link $uid}
	 * @param string $login Логин в системе lardi-trans.com
	 * @param string $passMD5 MD5 сумма пароля 
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/auth	   
	 */
	function auth($login,$passMD5)
	{
		$data='method=auth&login='.$login.'&password='.$passMD5;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<sig>")===false) {
			throw new Exception("Auth error!<br/>$resp");
		}
		else
		{			
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			$this->sig=$smplXml->sig;
			$this->uid=$smplXml->uid;
			return $smplXml;
		}			
	}	
	
	/**
	 * Получение информации о фирме по её номеру
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param intereg $uid 	Номер фирмы по которой нужно получить информацию
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/users.firm.info
	 */
	function usersFirmInfo($sig,$uid)
	{
		$data='method=users.firm.info&sig='.$sig.'&uid='.$uid;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<data>")===false) {
			throw new Exception("users.firm.info error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получить список стран и областей, если у страны есть связанные области то вместе с областями.
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.country
	 */	
	function baseCountry($sig)
	{
		$data='method=base.country&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.country error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получить список допустимых форм оплаты
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.forma
	 */
	function baseForma($sig)
	{
		$data='method=base.forma&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.forma error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получить список допустимых типов автомобилей
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.auto_tip 
	 */
	function baseAutoTip($sig)
	{
		$data='method=base.auto_tip&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.auto_tip error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получить список типов кузова
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.trucks	 
	 */
	function baseTrucks($sig)
	{
		$data='method=base.trucks&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.trucks error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получить список типов валюты
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.valuta	 
	 */
	function baseValuta($sig)
	{
		$data='method=base.valuta&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.valuta error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}

	/** 
	 * Получить список типов загрузки
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/base.zagruz	 
	 */
	function baseZagruz($sig)
	{
		$data='method=base.zagruz&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("base.zagruz error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Добавление заявки по грузам
	 * @param array $gruz массив параметров в виде "параметр=значение"
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.add
	 */	
	function myGruzAdd($gruz)
	{	
		$data='method=my.gruz.add';
		foreach ($gruz as $varname => $varvalue) {        	
			$data.='&'.$varname.'='.$varvalue;    
		}		
		
		$resp = $this->doRequest($data);
		if (strpos($resp, "<id>")===false) {
			throw new Exception("my.gruz.add error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Добавление заявки по транспорту
	 * @param array $trans массив параметров в виде "параметр=значение"
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.add
	 */	
	function myTransAdd($trans)
	{	
		$data='method=my.trans.add';
		foreach ($trans as $varname => $varvalue) {        	
			$data.='&'.$varname.'='.$varvalue;    
		}		
		
		$resp = $this->doRequest($data);
		if (strpos($resp, "<id>")===false) {
			throw new Exception("my.trans.add error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение списка "Мои грузы/транспорт"
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.list	
	 */	
	function myGruzList($sig)
	{	
		$data='method=my.gruz.list&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.list error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Удалить свой груз
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер груза, который нужно удалить
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.delete
	 */	
	function myGruzDelete($sig,$id)
	{	
		$data='method=my.gruz.delete&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.gruz.delete error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Удалить свой транспорт
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер транспорта, который нужно удалить
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.delete
	 */	
	function myTransDelete($sig,$id)
	{	
		$data='method=my.trans.delete&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.trans.delete error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение списка корзины "Мои грузы/транспорт"
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.trash.list
	 */	
	function myGruzTrashList($sig)
	{	
		$data='method=my.gruz.trash.list&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.trash.list error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Удалить свой груз из корзины
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер груза, который нужно удалить из корзины
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.trash.delete
	 */	
	function myGruzTrashDelete($sig,$id)
	{	
		$data='method=my.gruz.trash.delete&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.gruz.trash.delete error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Удалить свой транспорт из корзины
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер транспорта, который нужно удалить из корзины
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.trash.delete
	 */	
	function myTransTrashDelete($sig,$id)
	{	
		$data='method=my.trans.trash.delete&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.trans.trash.delete error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Извлечь груз из корзины по номеру
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер груза, который нужно восстановить из корзины
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.trash.return
	 */	
	function myGruzTrashReturn($sig,$id)
	{	
		$data='method=my.gruz.trash.return&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.gruz.trash.return error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Извлечь транспорт из корзины по номеру
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер транспорта, который нужно восстановить из корзины
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.trash.return
	 */	
	function myTransTrashReturn($sig,$id)
	{	
		$data='method=my.trans.trash.return&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.trans.trash.return error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Повтор заявки по грузам
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер заявки, которую нужно повторить
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.refresh 
	 */	
	function myGruzRefresh($sig,$id)
	{	
		$data='method=my.gruz.refresh&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.gruz.refresh error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Повтор заявки по транспорту
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param long $id Номер заявки, которую нужно повторить
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.refresh
	 */	
	function myTransRefresh($sig,$id)
	{	
		$data='method=my.trans.refresh&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<status>")===false) {
			throw new Exception("my.trans.refresh error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Расчет расстояния и предложение маршрута.
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param string $towns Города через запятую, между которыми нужно найти расстояние
	 * @param boolean $country_out Не выезжать из страны (true/false) 
	 * @param boolean $faster Минимальное время (true/false)
	 * @param boolean $reserved Использовать зарезервированные маршруты для грузового транспорта (true/false)
	 * @param boolean $exclude_parom Исключить паромы (true/false)
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/distance.calc
	 */
	function distanceCalc($sig, $towns, $country_out, $faster, $reserved, $exclude_parom)
	{
		$data='method=distance.calc&sig='.$sig.'&towns='.$towns.'&country_out='.$country_out.'&faster='.$faster.'&reserved='.$reserved.'&exclude_parom='.$exclude_parom;
		$resp = $this->doRequest($data);
		
		if ((strpos($resp, "<error>")!==false)) {
			throw new Exception("distance.calc error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение списка возможных городов по части его названия
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param string $name Город или часть его названия
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/distance.search
	 */	
	function distanceSearch($sig,$name)
	{
		$data='method=distance.search&sig='.$sig.'&name='.$name;
		$resp = $this->doRequest($data);		
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("distance.search error!<br/>$resp");
		}
		else
		{			
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение одной заявки по грузам
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param string $id id заявки
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.get
	 */	
	function myGruzGet($sig,$id)
	{	
		$data='method=my.gruz.get&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.get error!<br/>$resp");			
		}
		else
		{			
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение одной заявки по транспорту
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @param string $id id заявки
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.get
	 */	
	function myTransGet($sig,$id)
	{	
		$data='method=my.trans.get&sig='.$sig.'&id='.$id;
		$resp = $this->doRequest($data);
		
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.trans.get error!<br/>$resp");			
		}
		else
		{			
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение списка "Мои грузы/транспорт" в сокращенном варианте
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.list.short
	 */	
	function myGruzListShort($sig)
	{	
		$data='method=my.gruz.list.short&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.list.short error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Получение списка корзины "Мои грузы/транспорт" в сокращенном варианте
	 * @param string $sig Уникальный номер выданный при авторизации методом {@link auth}
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.trash.list.short
	 */	
	function myGruzTrashListShort($sig)
	{	
		$data='method=my.gruz.trash.list.short&sig='.$sig;
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.trash.list.short error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}	
	
	/**
	 * Универсальная функция для обращения к api.lardi-trans.com
	 * @param string $request Строка содержащая POST-запрос, который необходимо передать серверу API
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/
	 */	
	function callAPI($request)
	{		
		$resp = $this->doRequest($request);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("callAPI error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}
	}
	
	/**
	 * Редактирование заявки по грузам
	 * @param array $editGruz массив параметров в виде "параметр=значение"
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.gruz.edit
	 */	
	function myGruzEdit($editGruz)
	{
		$data='method=my.gruz.edit';
		foreach ($editGruz as $varname => $varvalue) {        	
			$data.='&'.$varname.'='.$varvalue;    
		}		
		
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.gruz.edit error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}	
	}
	
	/**
	 * Редактирование заявки по транспорту
	 * @param array $editTrans массив параметров в виде "параметр=значение"
	 * @return SimpleXML $smplXml Ответ сервера в формате SimpleXML
	 * @link http://api.lardi-trans.com/doc/my.trans.edit
	 */	
	function myTransEdit($editTrans)
	{
		$data='method=my.trans.edit';
		foreach ($editTrans as $varname => $varvalue) {        	
			$data.='&'.$varname.'='.$varvalue;    
		}		
		
		$resp = $this->doRequest($data);
		if (strpos($resp, "<error>")!==false) {
			throw new Exception("my.trans.edit error!<br/>$resp");
		}
		else
		{
			$smplXml = new SimpleXmlElement($resp, LIBXML_NOCDATA);
			return $smplXml;
		}	
	}
	
	/**
	 * Транспортная функция для обращения к серверу API методом POST 
	 * @param string $data Строка содержащая POST-запрос, который необходимо передать серверу API
	 * @return xml $rez Ответ сервера в формате XML
	 * @link http://api.lardi-trans.com/doc/ 
	 */	
	function doRequest($data) { 
		
		$ch = curl_init($this->apiurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);	
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST,1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
		$rez = curl_exec($ch); 
		curl_close($ch);
		return $rez;
				
	}
}
?>