<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller
 *
 * Переписано системний клас Controller (доповнено)
 * @author Сергій Krid
 */
class Controller extends Kohana_Controller {

    public function before() {
        parent::before();

//        if (strpos($_SERVER['SERVER_NAME'], 'www.') === 0) {
//            var_dump($_SERVER['REQUEST_URI']);
//            var_dump('http://' . str_replace('www.', '', $_SERVER['SERVER_NAME']) . $_SERVER['REQUEST_URI']);
//            die;
//            HTTP::redirect('http://' . str_replace('www.', '', $_SERVER['SERVER_NAME']) . $_SERVER['REQUEST_URI'], 301);
//        }

        // примусово додає / в кінці адрес
//        if ($this->request->initial() && $this->request->method() != Request::POST) {
//            $url_comp = parse_url($_SERVER['REQUEST_URI']);
//            if (substr($url_comp['path'], -1) != '/') {
//                if (array_key_exists('query', $url_comp)) {
//                    $url = $url_comp['path'] . '/?' . $url_comp['query'];
//                }
//                else {
//                    $url = $url_comp['path'] . '/';
//                }
//
//                header('Location: ' . $url);
//                exit;
//            }
//        }
    }

    /**
     * Додаємо вивід статистики для відналагодження сторінки
     *
     * Для відображення передати GET-параметр debug_stats=1
     * (доступно лише для статусу DEVELOPMENT)
     *
     * parent::after() у наслідуваних класах викликати в самому кінці!
     */
    public function after() {
        if ( (Kohana::$environment == Kohana::DEVELOPMENT) && $this->request->query('debug_stats'))
            echo View::factory('profiler/stats');
    }

}