<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Mapper Богдана
 */
class Mapper
{
    /**
     * @var array Дані для інсерту чи апдейту
     */
    protected $data     = array();
    /**
     * @var string  Назва таблиці
     */
    protected $table    = null;
    /**
     * @var array
     */
    protected $result   = array();
    /**
     * @var array
     */
    protected $where    = array();
    /**
     * @var запрос до бд
     */
    protected $_query;

    protected $execution = array();

    public static function factory($name)
    {
        $x = new Mapper();
        $x->table($name);
        return $x;
    }

    /**
     * @param array $data ассоціативний массив вхідних данних де ключ == назві колонки в БД, а значення == значенню колонки в БД
     * @return $this
     * в $data можна передавати массив що містить і лишні поля. Просто тоді треба буде вказати через extract() лише ті поля що будуть враховані
     */
    public function set(array $data){
        $this->data = $data;
        return $this;
    }

    /**
     * @param $table string Назва таблиці відносно якої будемо робити INSERT чи UPDATE
     * @return $this
     */
    public function table($table){
        $this->table = $table;
        return $this;
    }


    /**
     * @param mixed array|string значення, назви полів якими треба буде обмежити операцю INSERT чи UPDATE
     */
    public function useOnly(){

        $columns = func_get_args();

        foreach($columns as $name){
            $queryField = (is_array($name)) // назва поля для вставки
                ? $name[1]
                : $name;
            $value      = $this->data[ (is_array($name)) ? $name[0] : $name ];

            $this->result[$queryField] = $value;
        }
        return $this;

    }

    /**
     * @param $column   string назва поля
     * @param $value    string значення
     */
    public function append($column, $value){
        $this->result[$column] = $value;
        return $this;
    }

    /**
     * @param $col  string      назва кеолонки
     * @param $compare string   метод порівняння
     * @param $value string     значення
     * @return $this
     */
    public function where($col, $compare, $value){
        $this->where[] = array($col, $compare, $value);
        return $this;
    }
    //  -   -   -   -   -   -   -   -   -
    protected function _compile(){
        if(empty($this->result)){
            $this->result = $this->data;
        }
    }
    protected function _where($query){
        foreach($this->where as $statement){
            $query->where($statement[0],$statement[1],$statement[2]);
        }
        return $query;
    }
    //  -   -   -   -   -   -   -   -   -

    /**
     * використати класичний INSERT
     *
     * @return $this
     * @throws Kohana_Exception
     */
    public function insert(){
        $this->_compile();
        $this->_query = DB::insert($this->table, array_keys($this->result))
            ->values(array_values($this->result));

        return $this;
    }

    /**
     * використати класичний UPDATE
     *
     * @return $this
     */
    public function update(){
        $this->_compile();
        $upd = DB::update($this->table)
            ->set($this->result);
        $this->_query = $this->_where($upd);

        return $this;
    }

    /**
     * використати класичний INSERT, якщо при зпрацюванні відловилась помилка DUPLICATE - то UPDATE
     * @param mixed string колонки котрі повинні бути проігноровані для побудови UPDATE виразу
     * @return $this
     */
    public function save(){

        $this->insert();

        $onDuplicate = array();
        $parameters = array();
        $ignoreKeys = func_get_args();


        foreach($this->result as $field => $val){

            if(!in_array($field,$ignoreKeys)){

                $onDuplicate[] = "`{$field}`=[:{$field}:]";
                $parameters["[:{$field}:]"] = $val;

            }

        }

        $this->_query = DB::query(Database::INSERT,
            $this->_query . ' ON DUPLICATE KEY UPDATE ' . implode(', ', $onDuplicate)
        )
            ->parameters($parameters)
        ;

        return $this;

    }

    /**
     * @param $field string назва колонки в якій лежить ID,
     * якщо вона не пороожня в масиві значень що попадають під результат,
     * то поверне значення звідти, інакше поверне MYSQL_LAST_INSERT_ID
     */
    public function id($field = null){

        return ($field == null)
            ? $this->execution[0]
            : (empty($this->result[$field]))
                ? $this->execution[0]
                : $this->result[$field];
    }

    public function execute(){

        $this->execution = $this->_query->execute();

        return $this;

    }

    public function executionResult(){

        return $this->execution;

    }

    public function __toString(){
        return (string) $this->_query;
    }
}