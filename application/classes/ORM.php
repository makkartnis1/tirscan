<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class ORM
 *
 * @author Сергій Krid
 */
class ORM extends Kohana_ORM {

    protected $_primary_key = 'ID';

}