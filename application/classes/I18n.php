<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class I18n Богдана
 */
class I18n extends Kohana_I18n {

    protected static $cacheTime_sec = 0;

    public static $db_18in = false;
    public static $db_localeID = false;
    protected static $dir = 'i18n/db/locale-id-';

    public static function clearCache(){

        VarCache::clearTag(self::$dir . self::$db_localeID);

    }

    public static function loadDB($localeID){
        self::$db_localeID = $localeID;

        self::$db_18in = (VarCache::expired(self::$dir.$localeID))
            ? VarCache::set(self::$dir.$localeID, function() use($localeID){

                $locales = DB::select('i18n.id', 'i18n.const','i18n_locale.translate')
                    ->from('i18n')
                    ->join('i18n_locale', 'LEFT')
                    ->on('i18n.id','=','i18nID')
                    ->where('i18n_locale.localeID','=',$localeID)
                    ->execute()->as_array();

                $db_i18n = array();

                foreach($locales as $row){
                    $db_i18n[$row['const']] = array(
                        'translation'   => $row['translate'],
                        'id'            => $row['id'],
                    );
                }

                return $db_i18n;



            }, self::$cacheTime_sec)
            : VarCache::get(self::$dir.$localeID);

    }


}


if ( ! function_exists('__db'))
{

    function __db($string, array $values = NULL)
    {
        $string = ''.$string;

        if(empty($string)){
            return '';
        }

        if(!is_array(I18n::$db_18in)){

            return empty($values) ? ($string) : __($string, $values);

        }else{

            $res = $string;

            $str = Arr::get(I18n::$db_18in, $string, false);

            if($str === false){
                $db = Database::instance();

                try{
                    $db->begin();
                    try {
                        $ins = DB::insert('i18n', array('const'))
                            ->values(array($string))
                            ->execute();

                        $id = $ins[0];
                    }
                    catch (Exception $e) {
                        $sel = DB::select('id')
                            ->from('i18n')
                            ->where('const', '=', $string)
                            ->execute()
                            ->as_array();

                        $id = $sel[0]['id'];
                    }

                    Mapper::factory('i18n_locale')
                        ->set(
                            array(
                                'localeID' => I18n::$db_localeID,
                                'translate' => $string,
                                'i18nID'    => $id
                             )
                        )->save('localeID', 'i18nID')
                    ->where('localeID','=',I18n::$db_localeID)
                    ->where('i18nID','=',$id)
                    ->execute();


//                    DB::insert('i18n_locale', array('localeID', 'translate', 'i18nID'))
//                        ->values()
//                        ->execute();

                    $db->commit();

                    I18n::$db_18in[$string] = array(
                        'translation'   => $string,
                        'id'            => $id,
                    );

                    I18n::clearCache();

                }catch (Exception $e){

                    $db->rollback();

                    throw $e;

                }

            }else{
                if($str['translation'] === null){ // в таблиці перекладів відсутнє значення

                    Mapper::factory('i18n_locale')
                        ->set(array(
                            'id'            => $str['id'],
                            'localeID'      => I18n::$db_localeID,
                            'translate'     => $string,
                        ))
                        ->save('id');

                    I18n::clearCache();

                }else{

                    $res = $str['translation'];

                }

            }

            return (empty($values)) ? $res : strtr($res, $values);

        }

    }
}