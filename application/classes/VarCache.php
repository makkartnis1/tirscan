<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class VarCache Богдана
 */
class VarCache {

    public static function set($tag, $value, $time = 3600){

        $file_ = APPPATH.'varcache/'.$tag;

        $cache = array(
            'expiration'    => time() + abs($time),
            'data'          => (is_callable($value)) ? $value() : $value,
        );

        VarCache_Buffer::set($tag, $cache);

        $handle = fopen($file_, "w");
        fwrite($handle, json_encode($cache));
        fclose($handle);

        $res = VarCache_Buffer::get($tag);
        return $res['data'];

    }

    public static function clearTag($tag){

        if(is_file( APPPATH.'varcache/'.$tag)){

            unlink(APPPATH.'varcache/'.$tag);

        }

    }

    public static function get($tag){

        $res = VarCache_Buffer::get($tag);
        return $res['data'];

    }

    public static function expired($tag){


        $file_ = APPPATH.'varcache'.DIRECTORY_SEPARATOR.$tag;
        $path = explode(DIRECTORY_SEPARATOR, $file_);
        array_pop($path);
        $path = implode(DIRECTORY_SEPARATOR, $path);

        if(!is_dir($path))
            mkdir($path, 0750, true);


        if(!is_file($file_)){

            VarCache_Buffer::set($tag, false);
            return true;

        }

        $handle = fopen($file_, "r");
        $cache = json_decode(fread($handle, filesize($file_)), true);
        fclose($handle);

        VarCache_Buffer::set($tag, $cache);

        return ($cache['expiration'] < time());

    }

}