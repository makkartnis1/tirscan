<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Liqpay extends Controller {
    
    public function action_callback() {
        try {
            $data = json_decode(base64_decode($this->request->post('data')), true);

            $orm_transaction                  = ORM::factory('Frontend_Wallet_Transaction');
            $orm_transaction->order_id        = $data['order_id'];
            $orm_transaction->liqpay_order_id = $data['liqpay_order_id'];
            $orm_transaction->status          = $data['status'];
            $orm_transaction->userID          = $this->request->query('user_id');
            $orm_transaction->amount          = $data['amount'];
            $orm_transaction->raw             = print_r($data, true);

            $orm_transaction->save();

            if ($data['status'] == 'sandbox') {
                Model_Frontend_Wallet_History::query_insertHistory(
                    $this->request->query('user_id'),
                    'purchase',
                    'wallet.purchase',
                    $data['amount']
                );
            }
        }
        catch (Exception $e) {
//            $orm_transaction      = ORM::factory('Frontend_Wallet_Transaction');
//            $orm_transaction->userID          = $this->request->query('user_id');
//            $orm_transaction->raw = base64_decode($this->request->post('data'));
//            $orm_transaction->save();
        }

        $this->response->body();
    }

}
