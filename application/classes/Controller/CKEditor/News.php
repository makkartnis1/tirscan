<?php defined('SYSPATH') or die('No direct script access.');

class Controller_CKEditor_News  extends Controller_CKEditor_System_CKEditor {

    public static $dir = '/uploads/images/news';

    public function isValid($files){

        if(is_array(@getimagesize($_FILES["upload"]["tmp_name"]))){
            return true;
        } else {
            $this->error = 'uploaded file is not image!';
            return false;
        }

    }

}
