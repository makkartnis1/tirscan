<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_CKEditor_System_CKEditor extends Controller {

    /**
     * @var View
     */
    protected $content = 'ckeditor/upload-response' ;

    protected $callback;
    protected $path;
    protected $error;

    public static $dir = '/path/to/file/from/root/';

    public function before(){

        $this->content = View::factory($this->content);

        $this->content->bind('callback',    $this->callback);
        $this->content->bind('http_path',   $this->path);
        $this->content->bind('error',       $this->error);

        $get = $this->request->query();

        $this->callback = $get['CKEditorFuncNum'];

    }

    /**
     * @param array $files копія кглобального массиву $_FILES
     * @return bool , якщо повертає false === не валідний, то повинно віддавати повідомлення в $this->error, про стан помилки
     */
    abstract public function isValid($files);

    public function action_upload(){

        if($this->isValid($_FILES)){

            $this->save($_FILES['upload']['tmp_name'], $_FILES['upload']['name']);

        }

    }

    protected function save($tempFile, $fileNameOriginal){

        $ext = pathinfo($fileNameOriginal, PATHINFO_EXTENSION);

        $fileName = new Transliterate(substr($fileNameOriginal,0,-strlen($ext)));
        $fileName->lowercase()->replaceSpaces('-')->trim()->limit(200);
        $fileName = time() . '_' . $fileName.'.'.$ext;

        $_dirPath           = $file_new_name = $_SERVER['DOCUMENT_ROOT'] . static::$dir;
        $_fileTargetPath    = $_dirPath . $fileName;

        $this->path = static::$dir . $fileName;

        if( !move_uploaded_file($tempFile, $_fileTargetPath) ) {

            $this->error = __('error.on.file.save.action');

        }
    }

    public function after(){

        $this->response->body(

            $this->content->render()

        );

    }



} // End Welcome