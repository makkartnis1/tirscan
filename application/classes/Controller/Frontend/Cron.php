<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Cron extends Controller {

    public function action_index()
    {
        I18n::loadDB(1);
        $m = new Model_Frontend_Tenders();
        $m->updateStatusTenders(true);
    }

}