<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Frontend_Error
 *
 * Контроллер виводу помилок сайту (відображаються для версії PRODUCTION)
 */
class Controller_Frontend_Error extends Controller_Frontend_Authorized {

    /**
     * @throws HTTP_Exception_404
     */
    public function before() {
        // залишаємо доступ до відображення лише через субзапити (HMVC)
        if ($this->request->is_initial()) {
            throw new HTTP_Exception_404();
        }

        $this->blocks['header'] = 'frontend/blocks/header_page';

        parent::before();
    }

    public function action_index() {
        $this->l10n = ['title' => __db('site.error.title')];

        $code    = $this->request->param('code');
        $message = $this->request->query('message', null);
        if ($message == null)
            $message = __db('site.error.content.' . $code);

        $this->blocks['content'] = View::factory('frontend/content/error', compact('code', 'message'));
    }

}
