<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Transport extends Controller_Frontend_HMVC {


    public function action_index(){

        self::_addJS('/assets/js/frontend/linker/uploader.js');
        self::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR . 'transport/init.js');
        self::_addJS('/assets/js/frontend/linker/handlebars_helpers_extension.js');

        $this->response->body(View::factory('frontend/content/profile/tab/transport/layout',[
            'require_init'      => true,

            'modal_layout'      => View::factory('frontend/content/profile/tab/transport/blocks/modal-edit',[

                'modal_tab'         => [

                    'transport-modal-edit-files'          => [
                        'transport.tab.documents',
                        View::factory('frontend/content/profile/tab/transport/blocks/modal-edit/files')->render()
                    ],

                    'transport-modal-edit-info'           => [
                        'transport.tab.info',
                        View::factory('frontend/content/profile/tab/transport/blocks/modal-edit/info',[
                                'carTypes' => array_map(
                                    function($value){ $value['localeName'] = 'some.' . $value['localeName']; return $value; },
                                    DB::select('id','localeName')->from('Frontend_User_Car_Types')->execute()->as_array()
                                )
                            ]
                        )->render()
                    ],

                    'transport-modal-edit-gallery'        => [
                        'transport.tab.gallery',
                        View::factory('frontend/content/profile/tab/transport/blocks/modal-edit/gallery')->render()
                    ],
                ]

            ])->render()

        ]));

    }
    

}