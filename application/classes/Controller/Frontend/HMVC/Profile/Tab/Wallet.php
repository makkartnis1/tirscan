<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Wallet extends Controller_Frontend_HMVC {

    public function action_index() {
        self::_addJS(self::SCRIPT_DIR . 'wallet.js');

        $view_purchase = View::factory('frontend/content/profile/tab/wallet/purchase');
        $view_history = View::factory('frontend/content/profile/tab/wallet/history');
        
        $this->response->body(self::_compile_layout_tabs([
            'wallet-purchase' => [
                'caption' => __db('profile.wallet.purchase'),
                'content' => $view_purchase
            ],
            'wallet-history' => [
                'caption' => __db('profile.wallet.history'),
                'content' => $view_history
            ]
        ]));
    }

}