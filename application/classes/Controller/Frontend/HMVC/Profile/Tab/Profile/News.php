<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Profile_News extends Controller_Frontend_HMVC {

    public function action_index() {

        self::_addJS(self::SCRIPT_DIR . 'my-profile/news/init.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/profile/news/layout',[
                'handlebars_view' => View::factory('frontend/content/profile/tab/profile/news/handlebars/news_list'),
                'handlebars_view_one_news'   => View::factory('frontend/content/profile/tab/profile/news/handlebars/one_news')
            ])
        );

    }

}