<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Profile_Feedback extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(self::SCRIPT_DIR . 'my-profile/feedback/init.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/profile/feedback/layout',[
                'handlebars_view'   => View::factory('frontend/content/profile/tab/profile/feedback/layout/handlebars/review'),
                // пагінацію не підключаю, так як вона підключаєьться в транспорті автоматом
            ])
        );

    }


}