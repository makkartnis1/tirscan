<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Profile_Blacklist extends Controller_Frontend_HMVC {

    public function action_index() {

        self::_addJS(self::SCRIPT_DIR . 'my-profile/black_list/init.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/profile/black_list/layout', [
                'handlebars_view' => View::factory('frontend/content/profile/tab/profile/black_list/handlebars/black_list')
            ])
        );

    }

}