<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Ticket extends Controller_Frontend_HMVC {

    public        $modals;
    public static $info_linked = false;

    public function before() {
        parent::before();

        $this->modals = View::factory('frontend/content/profile/tab/ticket/modals');

        self::_addJS(self::SCRIPT_DIR . 'tickets/dialogs.js');

        if (!self::$info_linked) {
            self::_addJS(self::SCRIPT_DIR . 'tickets/info_modal.js');
            self::$info_linked = true;
        }
    }

    public function action_transport() {
        self::_addJS(self::SCRIPT_DIR . 'tickets/transport-tickets.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/transport-propositions.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/transport-subscriptions.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/transport-archive.js');

        $view_requests     = View::factory('frontend/content/profile/tab/ticket/transport-my', ['modals' => $this->modals]);
        $view_propositions = View::factory('frontend/content/profile/tab/ticket/transport-prop', ['modals' => $this->modals]);
        $view_subscriptions = View::factory('frontend/content/profile/tab/ticket/transport-subscriptions');
        $view_archive      = View::factory('frontend/content/profile/tab/ticket/transport-archive', ['modals' => $this->modals]);

        $this->response->body(self::_compile_layout_tabs([
            'requests-transport-my-requests' => [
                'caption' => __db('profile.requests.my-tickets'),
                'content' => $view_requests
            ],
            'requests-transport-my-applications' => [
                'caption' => __db('profile.requests.my-applications'),
                'content' => $view_propositions
            ],
            'requests-transport-my-subscriptions' => [
                'caption' => __db('profile.requests.my-subscriptions'),
                'content' => $view_subscriptions
            ],
            'requests-transport-my-archive' => [
                'caption' => __db('profile.requests.my-archive'),
                'content' => $view_archive
            ]
        ]));
    }

    public function action_cargo() {
        self::_addJS(self::SCRIPT_DIR . 'tickets/cargo-tickets.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/cargo-propositions.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/cargo-archive.js');
        self::_addJS(self::SCRIPT_DIR . 'tickets/transport-subscriptions.js');

        $view_requests     = View::factory('frontend/content/profile/tab/ticket/cargo-my', ['modals' => $this->modals]);
        $view_propositions = View::factory('frontend/content/profile/tab/ticket/cargo-prop', ['modals' => $this->modals]);
        $view_archive      = View::factory('frontend/content/profile/tab/ticket/cargo-archive', ['modals' => $this->modals]);
        $view_subscriptions = View::factory('frontend/content/profile/tab/ticket/cargo-subscriptions');

        $this->response->body(self::_compile_layout_tabs([
            'requests-cargo-my-requests' => [
                'caption' => __db('profile.requests.my-tickets'),
                'content' => $view_requests
            ],
            'requests-cargo-my-applications' => [
                'caption' => __db('profile.requests.my-applications'),
                'content' => $view_propositions
            ],
            'requests-cargo-my-subscriptions' => [
                'caption' => __db('profile.requests.my-subscriptions'),
                'content' => $view_subscriptions
            ],
            'requests-cargo-my-archive' => [
                'caption' => __db('profile.requests.my-archive'),
                'content' => $view_archive
            ]
        ]));
    }

}