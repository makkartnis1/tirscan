<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Company extends Controller_Frontend_HMVC {

    protected  $routeParams = [];

    public function before() {

        parent::before();

        $this->routeParams = array_merge( $this->request->param(), ['controller'=>$this->request->controller()] );

    }

    public function action_index()
    {

        $tabs = [

            'my-company-managers' => [
                'content'   => Request::factory(Route::url('hmvc/profile/tab', array_merge($this->routeParams, ['action'        => 'managers'])))->execute()->body(),
                'caption'   => __db('profile.tab.my-managers'),
            ],

        ];

        $tabs = self::_compile_layout_tabs($tabs);

        $layout = View::factory('frontend/content/profile/tab/my-company/layout',[
            'tabs'  => $tabs,
        ]);

        $this->response->body( $layout );

    }

    public function action_managers(){

        self::_addJS('/assets/js/frontend/page/profile/hmvc/company/managers.js');
        
        $layout = $layout = View::factory('frontend/content/profile/tab/my-company/managers/layout');
        
        $this->response->body($layout);

    }


}