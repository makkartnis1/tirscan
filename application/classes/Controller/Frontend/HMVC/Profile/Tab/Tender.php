<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Tender extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR . 'tender/init.js');
        self::_addJS('/assets/js/frontend/linker/handlebars_helpers_extension.js');


        $layout = View::factory('/frontend/content/tenders/list');
        $layout->lang_uri = self::$firs_request_params['lang'];

        $layout->right = 'trans';

        if (self::$current_user->hasRights('cargo'))
        {
            $layout->right = 'cargo';
        }

        if (self::$current_user->hasRights('cargo') and self::$current_user->hasRights('transport'))
        {
            $layout->right = 'exp';
        }

        $this->response->body($layout);

    }

}