<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Messenger extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(self::SCRIPT_DIR.'messenger.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/messenger/index').
            View::factory('frontend/content/profile/tab/messenger/hb-messenger').
            View::factory('frontend/content/profile/tab/messenger/hb-messenger-dialogs')
        );

    }

}