<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Subscription extends Controller_Frontend_HMVC {

    public function action_index() {

        self::_addJS(self::SCRIPT_DIR . 'subscription/init.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/subscription/layout', [
                'handlebars_view' => View::factory('frontend/content/profile/tab/subscription/handlebars/subscription_list')
            ])
        );

    }

}