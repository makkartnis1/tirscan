<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Statistic extends Controller_Frontend_HMVC {

    public function action_index()
    {
        
        $tabs = [

            'stat-feedback-linker'  =>
            [
                
                'caption'   => __db('profile.tab.statistic.feedback'),
                'content'   => View::factory('frontend/content/profile/tab/statistic/feedback/layout'),
                
            ],


            
        ];

        // перевірка чи користувач має право бачити поточну ТАБ-у !!! сюди не дійде, якщо користувач не авторизований
        $user = ORM::factory('Frontend_User', Frontend_Auth::userID());

        if( $user->hasRights('transport') ){

            $tabs['stat-transport-requests-linker'] =
            [

                'caption'   => __db('profile.tab.statistic.transport.requests'),
                'content'   => View::factory('frontend/content/profile/tab/statistic/requests/transport/layout'),

            ];

        }

        if( $user->hasRights('cargo') ){

            $tabs['stat-cargo-requests-linker'] =
                [

                    'caption'   => __db('profile.tab.statistic.cargo.requests'),
                    'content'   => View::factory('frontend/content/profile/tab/statistic/requests/cargo/layout'),

                ];

        }



        $toolbar = View::factory('frontend/content/profile/tab/statistic/requests/toolbar');

        
        self::_addJS('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.js');
        self::_addJS('/assets/js/frontend/page/profile/hmvc/statistic/feedback-booblik.js');
        self::_addJS('/assets/js/frontend/page/profile/hmvc/statistic/feedback-line.js');
        self::_addJS('/assets/js/frontend/page/profile/hmvc/statistic/tickets.js');



        self::_addJS('/assets/js/frontend/page/profile/hmvc/statistic/header-datepickers.js');

        // datePicker
        self::_addJS('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js');
        self::_addJS('https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js');
        self::_addCSS('https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css');

        $this->response->body(

            View::factory('frontend/content/profile/tab/statistic/layout', [
                'tabs'      => self::_compile_layout_tabs($tabs),
                'toolbar'   => $toolbar,
            ])

        );

//        $enum = ['active.have-no-proposition','active.have-confirmed','active.have-not-confirmed','completed','outdated'];
//        $i = 0;
//        while ($i<1000){
//
//            $i = array_rand($enum);
//
//            DB::update('Frontend_Transports')->set([
//
//                'status' => $enum[$i]
//
//            ])  //->where('status','=','')
//                ->order_by(DB::expr('RAND()'))
//                ->limit(10)
//                ->execute( );
//
//            $i++;
//
//        }



    }
    
    

}