<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Newsadd extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(self::SCRIPT_DIR . 'info/add_news.js');

        $input_groups = [];

        $input_groups['title'] = Frontend_FormElement::compile_input([
            'text',                                     # type
            'Frontend_News_Locales[%localeID%][title]',                              # name
            '',                                             # value
            __db('placeholder.profile.news.title'),         # placeholder
            __db('hint.profile.news.title'),                # caption
            'news-create-title',                              # id
            '%localeID%',                                          # localeDependent
            true,                                           # is necessary ("*")
        ]);

        $input_groups['image'] = View::factory('frontend/content/profile/tab/infoblock/news-add/image-block');


        $input_groups['preview'] = Frontend_FormElement::compile_input([
            'textarea',                                     # type
            'Frontend_News_Locales[%localeID%][preview]',                              # name
            '',                                             # value
            __db('placeholder.profile.news.preview'),         # placeholder
            __db('hint.profile.news.preview'),                # caption
            'news-create-preview',                              # id
            '%localeID%',                                          # localeDependent
            true,                                           # is necessary ("*")
        ]);


        $input_groups['text'] = Frontend_FormElement::compile_input([
            'textarea.cke-editor',                                     # type
            'Frontend_News_Locales[%localeID%][text]',                              # name
            '',                                             # value
            __db('placeholder.profile.news.text'),         # placeholder
            __db('hint.profile.news.text'),                # caption
            'news-create-preview-text',                              # id
            '%localeID%',                                          # localeDependent
            true,                                           # is necessary ("*")
        ]);

        $userID = Frontend_Auth::userID();

        $user_company_id = ORM::factory('Frontend_User')
            ->where('ID', '=', $userID)
            ->find();

//        images preview__________________________
        $widget = '';

        if( ($userCompanyID = $user_company_id->companyID) !== false) {
            $widget = Frontend_Uploader::init_as_newsPreviewImage($userCompanyID)
                ->widget('', 'img-preview')
                ->usingToken(true)
                ->download(false)
                ->delete(false)
                ->upload(true)
                ->render();
        }
//      ______________________________

        $view = View::factory('frontend/content/profile/tab/infoblock/news-add/layout',
            [
                'groups'    => $input_groups,
                'company_id'    => $user_company_id->companyID,
                'button_save'    => View::factory('frontend/content/profile/component/frm/button',
                    [
                        'type'          => 'submit',
                        'name'          => 'action',
                        'value'         => 'save[companyinfo]',
                        'id'            => '',
                        'btn_class'     => 'btn-primary',
                        'placeholder'   => __db('btn.save'),
                    ]
                ),
                'button_reset'    => View::factory('frontend/content/profile/component/frm/button',
                    [
                        'type'          => 'reset',
                        'name'          => 'action',
                        'value'         => 'create[news]',
                        'id'            => '',
                        'btn_class'     => 'btn-primary',
                        'placeholder'   => __db('btn.reset'),
                    ]
                ),

                'changeImg_widget'   => $widget
//                'avatar'             => Helper_User::avatar(Frontend_Auth::userID()),
            ]
        );

        $this->response->body( $view );

    }
}