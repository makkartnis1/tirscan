<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Newslist extends Controller_Frontend_HMVC {

    public function action_index() {

        self::_addJS(self::SCRIPT_DIR . 'info/list_news.js');

        $this->response->body(
            View::factory('frontend/content/profile/tab/infoblock/news-list/layout',[
                'handlebars_view' => View::factory('frontend/content/profile/tab/infoblock/news-list/handlebars/list'),
                'handlebars_view_one_news'   => View::factory('frontend/content/profile/tab/infoblock/news-list/handlebars/one_news')
            ])
        );

    }

}