<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Myinfo extends Controller_Frontend_HMVC {
    
    
    
    public function action_index(){
        
        self::_addJS(self::SCRIPT_DIR . 'info/myinfo.js');


        $data_to_show =
            [

                //[$type,       $name,                                                                  $value, $placeholder,                   $caption,   $id,                    $locale_dependent]
                ['text',        'Frontend_Users[icq]',                                                  '',     __db('placeholder.profile.icq'),                      __db('hint.profile.icq'),                 'profile-my-icq',       false,           false],
                ['text',        'Frontend_Users[skype]',                                                '',     __db('placeholder.profile.skype'),                    __db('hint.profile.skype'),               'profile-my-skype',     false,           false],
                ['email',       'Frontend_Users[email]',                                                '',     __db('placeholder.profile.email'),                    __db('hint.profile.email'),               'profile-my-email',     false,           true],
                ['phone+code',  'Frontend_Users[phone]/Frontend_Users[phoneCodeID]',                    '/',    __db('placeholder.profile.phone'),                    __db('hint.profile.phone'),               'profile-my-phone',     false,           true],
                ['phone+code',  'Frontend_Users[phoneStationary]/Frontend_Users[phoneStationaryCodeID]',      '/',    __db('placeholder.profile.phone.stationary'),   __db('hint.profile.phone.stationary'),    'profile-my-phoneStat', false,           false],
                ['text',        'Frontend_Users_Locale[%localeID%][name]',                              '',     __db('placeholder.profile.name'),                     __db('hint.profile.name'),                'profile-my-name',      '%localeID%',    true],

            ];
        $input_groups = [];

        foreach ($data_to_show as $input){

            $input_groups[] = Frontend_FormElement::compile_input($input);

        }

        $view = View::factory('frontend/content/profile/tab/infoblock/myinfo/layout',
            [
                'groups'    => $input_groups,
                'button'    => View::factory('frontend/content/profile/component/frm/button',
                    [
                        'type'          => 'submit',
                        'name'          => 'action',
                        'value'         => 'save[myinfo]',
                        'id'            => '',
                        'btn_class'     => 'btn-primary',
                        'placeholder'   => __db('btn.save'),
                    ]
                ),
            ]
        );

        $this->response->body( $view );
        
    }


}