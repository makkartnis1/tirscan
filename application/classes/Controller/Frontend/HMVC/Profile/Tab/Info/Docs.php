<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Docs extends Controller_Frontend_HMVC {

    protected static $todo_init = [

        'doc_reg'   => array(
            'company:certificate.of.registratoin.as.subject.of.business.activity',
            Frontend_Uploader::DIR_COMPANY_CERTIFICATE_DOCUMENTS
        ),

        'doc_tax'   => array(
            'company:tax.documents',
            Frontend_Uploader::DIR_COMPANY_TAX_DOCUMENTS,
        ),
        'doc_lic'   => array(
            'company:lecense.on.service.of.expeditor.and.transport',
            Frontend_Uploader::DIR_COMPANY_LICENSE_TRANSPORT_N_EXPEDITION,
        ),
        'doc_cert'  => array(
            'company:certificate.on.state.registration',
            Frontend_Uploader::DIR_COMPANY_CERT_STATE_REGISTRATION
        ),
    ];

    public function action_index(){

        $user = Controller_Frontend_Profile::$orm_cur_user;

        $layout = View::factory('frontend/content/profile/tab/infoblock/docs/layout');

        $widgets = self::widgets_company(
            Controller_Frontend_Profile::$orm_cur_user->companyID,
            $user->isOwnerOfProfileCompany()
        );

        foreach ($widgets as $var => $widget){

            $layout->set($var, $widget);

        }

        $layout->heading = View::factory('frontend/content/profile/tab/infoblock/docs/layout/heading');
        $layout->warning = __db('company.docs.warning.about.uploading');

        $this->response->body( $layout );
//        $this->response->body( '<pre>'. print_r(Controller_Frontend_Profile::$orm_cur_user->isOwnerOfProfileCompany(), true) .'</pre>' );
  
    }
    


    public static function widgets_company($companyID, $upload = false){

        $res = array();

        foreach (self::$todo_init as $view_var => $tmp){

            list($i18n_const, $dir) = $tmp;


            $widget = Frontend_Uploader::factory(

                Frontend_Uploader::DIR_VISIBLE_PRIVATE.$dir,

                $companyID,
                Frontend_Uploader::DIR_UPL      //      'uploads'

            )->widget(__db($i18n_const), $config = 'company-docs')
                ->showPreviewIfPossible(false)
                ->usingToken(true)

                ->download(true)
                ->delete(false)
                ->upload($upload)

                ->render();

            $res[$view_var] = $widget;

        }

        return $res;

    }

}