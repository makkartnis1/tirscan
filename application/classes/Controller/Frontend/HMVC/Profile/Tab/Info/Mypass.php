<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Mypass extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR . 'info/pass.js');


        $data_to_show =
            [

                [
                    'password',                                     # type
                    'mypass[current]',                              # name
                    'abcabc',                                             # value
                    __db('placeholder.profile.password.current'),         # placeholder
                    __db('hint.profile.password.current'),                # caption
                    'mypass-cur-pass',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],

                [
                    'password',                                     # type
                    'mypass[new]',                                  # name
                    'abcabc',                                             # value
                    __db('placeholder.profile.password.new'),             # placeholder
                    __db('hint.profile.password.new'),                    # caption
                    'mypass-new-pass',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],

                [
                    'password',                                     # type
                    'mypass[repeat]',                               # name
                    'abcabc',                                             # value
                    __db('placeholder.profile.password.repeat'),          # placeholder
                    __db('hint.profile.password.repeat'),                 # caption
                    'mypass-repeat-pass',                           # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],




            ];
        
        $input_groups = [];

        foreach ($data_to_show as $input){

            $input_groups[] = Frontend_FormElement::compile_input($input);

        }

        $view = View::factory('frontend/content/profile/tab/infoblock/mypass/layout',
            [
                'groups'    => $input_groups,
                'button'    => View::factory('frontend/content/profile/component/frm/button',
                    [
                        'type'          => 'submit',
                        'name'          => 'action',
                        'value'         => 'save[myinfo]',
                        'id'            => '',
                        'btn_class'     => 'btn-primary',
                        'placeholder'   => __db('btn.save'),
                    ]
                ),
            ]
        );

        $this->response->body( $view );

    }


}