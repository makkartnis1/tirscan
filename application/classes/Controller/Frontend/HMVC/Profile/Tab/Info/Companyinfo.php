<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info_Companyinfo extends Controller_Frontend_HMVC {

    public function action_index()
    {
        $data_to_show_col_md_6 =
            [
                [
                    'text',                                    # type
                    'Frontend_Companies[url]',                 # name
                    '',                                        # value
                    'placeholder.profile.company.url',         # placeholder
                    'profile.company.url',                     # caption
                    'company-url',                             # id
                    false,                                     # localeDependentset_form_uri
                    true,                                      # is necessary ("*")
                ],
                [
                    'text',                                     # type
                    'Frontend_Companies[ipn]',                              # name
                    '',                                             # value
                    __db('placeholder.profile.company.ipn'),         # placeholder
                    __db('hint.profile.company.ipn'),                # caption
                    'company-ipn',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'select',                                     # type
                    'Frontend_Companies[typeID]',                              # name
                    [Model_Frontend_Company_Type::getTypes(),'ID', 'localeName',/* 4й (3й ключ) елемент active typeID > не обовязковий параметр */],                                             # value
                    __db('placeholder.profile.company.type'),         # placeholder
                    __db('hint.profile.company.type'),                # caption
                    'company-type',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'select',                                     # type
                    'Frontend_Companies[ownershipTypeID]',                              # name
                    [Model_Frontend_Company_Ownership_Type::getTypes(),'ID', 'localeName',/* 4й (3й ключ) елемент active typeID > не обовязковий параметр */],                                             # value
                    __db('placeholder.profile.company.ownsership'),         # placeholder
                    __db('hint.profile.company.ownsership'),                # caption
                    'company-ownsership',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'phone+code',                                     # type
                    'Frontend_Companies[phone]/Frontend_Companies[phoneCodeID]',                              # name
                    '/',                                             # value
                    __db('placeholder.profile.company.phone'),         # placeholder
                    __db('hint.profile.company.phone'),                # caption
                    'company-phone',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'phone+code',                                     # type
                    'Frontend_Companies[phoneStationary]/Frontend_Companies[phoneStationaryCodeID]',                              # name
                    '/',                                             # value
                    __db('placeholder.profile.company.phone.stationary'),         # placeholder
                    __db('hint.profile.company.phone.stationary'),                # caption
                    'company-phone-stationary',                              # id
                    false,                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'text',                                     # type
                    'Frontend_Company_Locales[%localeID%][name]',                              # name
                    '',                                             # value
                    __db('placeholder.profile.company.name'),         # placeholder
                    __db('hint.profile.company.name'),                # caption
                    'company-name',                              # id
                    '%localeID%',                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'text',                                     # type
                    'Frontend_Company_Locales[%localeID%][address]',                              # name
                    '',                                             # value
                    __db('placeholder.profile.company.address'),         # placeholder
                    __db('hint.profile.company.address'),                # caption
                    'company-address',                              # id
                    '%localeID%',                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
                [
                    'text',                                     # type
                    'Frontend_Company_Locales[%localeID%][realAddress]',                              # name
                    '',                                             # value
                    __db('placeholder.profile.company.realAddress'),         # placeholder
                    __db('hint.profile.company.realAddress'),                # caption
                    'company-realAddress',                              # id
                    '%localeID%',                                          # localeDependent
                    true,                                           # is necessary ("*")
                ],
            ];

        $data_to_show_col_md_12 = [
            [
                'textarea',                                     # type
                'Frontend_Company_Locales[%localeID%][info]',   # name
                '',                                             # value
                __db('placeholder.profile.company.info'),             # placeholder
                __db('hint.profile.company.info'),                    # caption
                'company-info',                                 # id
                '%localeID%',                                   # localeDependent
                true,                                           # is necessary ("*")
            ],
        ];

        $input_groups = [];

        foreach ($data_to_show_col_md_6 as $input){
            $input_groups['col-md-6'][] = Frontend_FormElement::compile_input($input);
        }
        
        foreach ($data_to_show_col_md_12 as $input){
            $input_groups['col-md-12'][] = Frontend_FormElement::compile_input($input);
        }

        $view = View::factory('frontend/content/profile/tab/infoblock/companyinfo/layout',
            [
                'groups'    => $input_groups,
                'button'    => View::factory('frontend/content/profile/component/frm/button',
                    [
                        'type'          => 'submit',
                        'name'          => 'action',
                        'value'         => 'save[companyinfo]',
                        'id'            => '',
                        'btn_class'     => 'btn-blue',
                        'placeholder'   => __db('btn.save'),
                    ]
                ),
            ]
        );
        $this->response->body( $view );
    }
}