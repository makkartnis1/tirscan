<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Info extends Controller_Frontend_HMVC {
    
    public function action_index(){

        self::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR . 'init.js');
        self::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR . 'info.js');

        $layout = View::factory('frontend/content/profile/tab/infoblock/layout');

        $page_tabs = Kohana::$config->load('frontend/profile/tab/info/layout')->as_array();

        $tab_pills = $tab_contents = [];
        
        $active = true;
        
        foreach ($page_tabs as $tabID => $data){

            $tab_pills[]    = View::factory('frontend/content/profile/tab/infoblock/layout-tab-pill',[
                'id'            => $tabID,
                'active'        => $active,
                'caption'       => $data['caption']
            ]);

            unset($data['caption']); // щоб не конфігурувало в можливих параметрах роуту не потрібна для роута інфа

            $url = Route::url('hmvc/profile/tab/infoblock', array_merge($this->request->param(), $data));
            
            if(Kohana::$profiling === true)
                $benchmark = Profiler::start('Tab "Info" 2nd lvl hmvc', $tabID);

            $hmvc = Request::factory($url)->execute()->body();

            if(Kohana::$profiling === true)
                Profiler::stop($benchmark);
            
            $tab_contents[] = View::factory('frontend/content/profile/tab/infoblock/layout-tab-content',[
                'id'            => $tabID,
                'active'        => $active,
                'content'       => $hmvc,
            ]);
            
            if($active)
                $active = false;
            
        }

        $layout->pills      = implode(PHP_EOL, $tab_pills);
        $layout->contents   = implode(PHP_EOL, $tab_contents);

        $this->response->body( $layout );

    }
    
}