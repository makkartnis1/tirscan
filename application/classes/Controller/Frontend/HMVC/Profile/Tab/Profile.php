<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Tab_Profile extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS(self::SCRIPT_DIR . 'my-profile/init.js');

        $layout = $this->_compile_layout_tabs([

            'feedback-of-requests'  => [
                'caption'   => __db('profile.tab.my-profile.requests-feedback'),
                'content'   => Request::factory(
                    Route::url(
                        'hmvc/profile/tab/my-profile',
                        array_merge($this->request->param(), ['action' => 'index', 'controller'=>'feedback'])
                    )
                )->execute()->body(),
            ],

            'feedback-request-based-partners'  => [
                'caption'   => __db('profile.tab.my-profile.partners'),
                'content'   => Request::factory(
                    Route::url(
                        'hmvc/profile/tab/my-profile',
                        array_merge($this->request->param(), ['action' => 'index', 'controller'=>'partner'])
                    )
                )->execute()->body(),
            ],

            'news' => [
                'caption'   => __db('profile.tab.my-profile.news_partners'),
                'content'   => Request::factory(
                    Route::url(
                        'hmvc/profile/tab/my-profile',
                        array_merge($this->request->param(), ['action' => 'index', 'controller'=>'news'])
                    )
                )->execute()->body(),
            ],


            'managers' => [
                'caption' => __db('profile.tab.my-profile.add_managers'),
                'content' => Request::factory(
                    Route::url(
                        'hmvc/profile/tab/my-profile',
                        array_merge($this->request->param(), ['action' => 'index', 'controller' => 'managers'])
                    )
                )->execute()->body(),
            ],

            'black_list' => [
                'caption' => __db('profile.tab.my-profile.black_list'),
                'content' => Request::factory(
                    Route::url(
                        'hmvc/profile/tab/my-profile',
                        array_merge($this->request->param(), ['action' => 'index', 'controller' => 'blacklist'])
                    )
                )->execute()->body(),
            ],

        ]);


        $this->response->body( $layout );

    }

}