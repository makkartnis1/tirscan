<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_HMVC_Profile_Heading extends Controller_Frontend_HMVC {


    public function action_index(){
        
        $widget = '';
        
//        if( ($userID = Frontend_Auth::userID()) !== false)
//            $widget = Frontend_Uploader::init_as_userAvatar($userID)
//
//                ->widget('','user-avatar')
//
//                ->usingToken(true)
//                ->download(false)
//                ->delete(false)
//                ->upload(true)
//
//                ->render();


        $layout = View::factory('frontend/content/profile/head', [
            'changeAvatar_widget'   => $widget,
            'avatar'                => Helper_User::avatar(Frontend_Auth::userID()),
        ]);

        $this->response->body($layout);

    }

}