<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Company extends Controller_Frontend_Authorized {

    const PASS_REGEX      = "/^[a-zA-Z][a-zA-Z0-9!@#$%_]+$/";
    const PASS_MIN_LENGTH = 6;
    const PASS_MAX_LENGTH = 16;

    /** @var Model_Frontend_User_Locale */
    public $user_locale;

    public function before() {
        parent::before();

        $this->blocks['header'] = 'frontend/blocks/header_page';

        $this->user_locale = $this->user->getLocale($this->locale);
    }

    public function prepareCompanyFilters($query) {
        $filters = [
            [
                ['name', 'LIKE', "%{$query['f_text']}%"],
                ['info', 'LIKE', "%{$query['f_text']}%"],
                ['address', 'LIKE', "%{$query['f_text']}%"],
                ['realAddress', 'LIKE', "%{$query['f_text']}%"],
                ['phoneFull', 'LIKE', "%{$query['f_text']}%"],
            ]
        ];

        if ($query['f_type'] != null) {
            $filters[] = ['typeID', '=', $query['f_type']];
        }

        if ($query['f_rating'] == 'only_positive') {
            $filters[] = ['votesDiff', '>=', 0];
        }

        // перевіряєм чи заданий в нас фільтр пошуку по GEO
        $isGeoSearch = ($query['f_place_id'] != null);
        if ($isGeoSearch) {
            $filters[] = ['placeID', '=', $query['f_place_id']]; // якщо так, то додаєм відповідну умову до фільтрів запиту
        }

        return $filters;
    }

    public function prepareCompanySorts($query) {
        $sorts = [];
        if ($query['f_sort'] == 'by-reviews') {
            $sorts[] = ['votesSumm', 'DESC'];
        }
        if ($query['f_sort'] == 'by-rating') {
            $sorts[] = ['votesDiff', 'DESC'];
        }
        if ($query['f_sort'] == 'by-registration') {
            $sorts[] = ['registered', 'DESC'];
        }

        return $sorts;
    }

    public function getCompaniesCountFromQuery($query) {
        $filters     = $this->prepareCompanyFilters($query);
        $sorts       = $this->prepareCompanySorts($query);
        $isGeoSearch = ($query['f_place_id'] != null);

        return Model_Frontend_Company::query_getCompaniesWithLocales(true, $this->locale->ID, $isGeoSearch, 0, 0, $filters, $sorts);
    }

    public function getCompanyListFromQuery($query, $page_number, $page_size) {
        $filters     = $this->prepareCompanyFilters($query);
        $sorts       = $this->prepareCompanySorts($query);
        $isGeoSearch = ($query['f_place_id'] != null);

        // вибірка списку компаній з моделі
        $companies = Model_Frontend_Company::query_getCompaniesWithLocales(false, $this->locale->ID, $isGeoSearch, $page_size, $page_size * ($page_number - 1), $filters, $sorts);

        // встановлення відповідних локалізацій типу компанії з модулю I18n
        $result = [];
        foreach ($companies as $company) {
            $company['type'] = __db($company['typeLocale']);

            $params         = array_merge($this->request->param(), ['company_url_name' => $this->getCompanyUrlName($company)]);
            $company['db_url'] = $company['url'];
            $company['url'] = Route::url('frontend_site_company_page', $params);

            $company['image']    = $this->getCompanyImage($company['ID']);
            $company['rating']   = $this->getCompanyRating($company['ID']);
            $company['requests'] = $this->getCompanyRequestsAllCount($company['ID']);

            $result[] = $company;
        }

        return $result;
    }

    protected static function processCompanyRequest($request) {
        $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
        $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

        if ($request['dateTo'] != null) {
            $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
        }

        if ($request['json_sizeX'] != null) {
            $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
        }
        if ($request['json_sizeY'] != null) {
            $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
        }
        if ($request['json_sizeZ'] != null) {
            $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
        }

        return $request;
    }

    protected function getCompanyRequests($company_id) {
        $company = ORM::factory('Frontend_Company', $company_id);
        $result  = [
            'cargo' => [],
            'transport' => []
        ];

        if ($company->hasRights('cargo')) {
            $result['cargo'] = Model_Frontend_Cargo::query_getCargo($this->locale->ID, false, 10, 0, [
                ['t_comp.ID', '=', $company_id],
                [
                    ['t_cargo.status', '=', 'active.have-no-proposition'],
                    ['t_cargo.status', '=', 'active.have-not-confirmed']
                ]
            ]);
        }

        if ($company->hasRights('transport')) {
            $result['transport'] = Model_Frontend_Transport::query_getTransports($this->locale->ID, false, 10, 0, [
                ['t_comp.ID', '=', $company_id],
                [
                    ['t_trans.status', '=', 'active.have-no-proposition'],
                    ['t_trans.status', '=', 'active.have-not-confirmed']
                ]
            ]);
        }

        $result['transport'] = array_map('Controller_Frontend_Company::processCompanyRequest', $result['transport']);
        $result['cargo']     = array_map('Controller_Frontend_Company::processCompanyRequest', $result['cargo']);

        return $result;
    }

    protected function getCompanyRequestsCount($company_id) {
        $transport_count = Model_Frontend_Transport::query_getTransportCount([
            ['t_comp.ID', '=', $company_id],
            [
                ['t_trans.status', '=', 'active.have-no-proposition'],
                ['t_trans.status', '=', 'active.have-not-confirmed']
            ]
        ]);

        $cargo_count = Model_Frontend_Cargo::query_getCargoCount([
            ['t_comp.ID', '=', $company_id],
            [
                ['t_cargo.status', '=', 'active.have-no-proposition'],
                ['t_cargo.status', '=', 'active.have-not-confirmed']
            ]
        ]);

        return $transport_count + $cargo_count;
    }

    protected function getCompanyRequestsAllCount($company_id) {
        $transport_count = Model_Frontend_Transport::query_getTransportCount([
            ['t_comp.ID', '=', $company_id]
        ]);

        $cargo_count = Model_Frontend_Cargo::query_getCargoCount([
            ['t_comp.ID', '=', $company_id]
        ]);

        return $transport_count + $cargo_count;
    }

    protected function getCompanyComments($company_id) {
        $comments = Model_Frontend_Comment::query_getCommentsForCompany($company_id, $this->locale->ID);

        foreach ($comments as &$comment) {
            $company                = ORM::factory('Frontend_Company', $comment['authorCompanyID']);
            $comment['company_url'] = Route::url('frontend_site_company_page', [
                'lang' => $this->locale->uri,
                'company_url_name' => Controller_Frontend_Company::getCompanyUrlName([
                    'ID' => $comment['authorCompanyID'],
                    'primaryName' => 'company',
                    'url' => $company->url
                ])
            ]);
            $comment['image']       = $this->getCompanyImage($comment['authorCompanyID']);
            unset($comment);
        }

        return $comments;
    }

    protected function getCompanyRating($company_id) {
        return Model_Frontend_Company::query_getCompanyRating($company_id);
    }

    protected function getCompanyImage($company_id) {

        $avatar = DB::select('userID')
                    ->from('Frontend_User_to_Company_Ownership')
                    ->where('companyID', '=', $company_id)
                    ->limit(1)
                    ->execute()
                    ->as_array();

        return (empty($avatar))
            ? Helper_User::avatar('-')
            : Helper_User::avatar($avatar[0]['userID']);


        $no_image = Kohana::$config->load('frontend/site.company.images.no_logo');
        $path     = Kohana::$config->load('frontend/site.company.paths.images');
        if ($path === null) {
            return $no_image;
        }

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . sprintf($path, $company_id))) {
            return sprintf($path, $company_id);
        }
        else {
            return $no_image;
        }
    }

    public static function getCompanyUrlName(array $company) {
//        return $company['ID'] . '-' . Frontend_Helper_URL::title($company['primaryName']);
        return $company['ID'] . '-' . $company['url'];
    }

    /**
     * @param array $post_data
     * @param Model_Frontend_Phonecodes $phone_code
     * @param Model_Frontend_Phonecodes $stationary_phone_code
     * @return Model_Frontend_User
     */
    public function initUser(array $post_data,
                             Model_Frontend_Phonecodes $phone_code,
                             Model_Frontend_Phonecodes $stationary_phone_code) {

        $new_user = ORM::factory('Frontend_User')
                       ->set('primaryLocale', $this->locale)
                       ->set('emailApproved', 'pending')
                       ->set('hash', md5($post_data['password']))
                       ->set('phone', Arr::get($post_data, 'company_phone'))
                       ->set('phoneCode', $phone_code)
                       ->set('phoneStationary', Arr::get($post_data, 'stationary_phone'))
                       ->set('phoneStationaryCode', $stationary_phone_code)
                       ->set('email', Arr::get($post_data, 'company_email'))
                       ->set('icq', Arr::get($post_data, 'icq'))
                       ->set('skype', Arr::get($post_data, 'skype'));

        return $new_user;
    }

    public function initUserLocale(array $post_data) {
        $new_user_locale = ORM::factory('Frontend_User_Locale')
                              ->set('locale', $this->locale)
                              ->set('name', Arr::get($post_data, 'company_pib'));

        return $new_user_locale;
    }

    /**
     * @param array $post_data
     * @param Model_Frontend_Company_Type $type
     * @param Model_Frontend_Company_Ownership_Type $ownership_type
     * @param Model_Frontend_Phonecodes $phone_code
     * @param Model_Frontend_Phonecodes $stationary_phone_code
     * @return Model_Frontend_Company
     */
    public function initCompany(array $post_data,
                                Model_Frontend_Company_Type $type,
                                Model_Frontend_Company_Ownership_Type $ownership_type,
                                Model_Frontend_Phonecodes $phone_code,
                                Model_Frontend_Phonecodes $stationary_phone_code) {

        $new_company = ORM::factory('Frontend_Company')
                          ->set('primaryLocale', $this->locale)
                          ->set('phone', Arr::get($post_data, 'company_phone'))
                          ->set('phoneCode', $phone_code)
                          ->set('phoneStationary', Arr::get($post_data, 'stationary_phone'))
                          ->set('phoneStationaryCode', $stationary_phone_code)
                          ->set('registered', DB::expr('NOW()'))
                          ->set('type', $type)
                          ->set('ownershipType', $ownership_type)
                          ->set('url', Frontend_Helper_URL::title(Arr::get($post_data, 'company_name')));

        return $new_company;
    }

    public function initCompanyLocale(array $post_data) {
        $new_company_locale = ORM::factory('Frontend_Company_Locale')
                                 ->set('name', Arr::get($post_data, 'company_name'))
                                 ->set('address', Arr::get($post_data, 'company_address'));

        $new_company_locale->locale = $this->locale;

        return $new_company_locale;
    }

    public function handleCompanyGeo(Model_Frontend_Company $company, $geo_data) {
        if ($geo_data == null) {
            return;
        }

        $ids_str = Request::factory('/cms/geo/' . $this->locale->googleLang . '/add')
                          ->post($geo_data)
                          ->execute()
                          ->body();
        $ids     = explode(',', $ids_str);

        $company->addGeoRelationByIDs($ids);
    }

    public function handleRegistrationManagerPost(Model_Frontend_Registration_ManagerLink $ref_link, Model_Frontend_Company $company, $form_data) {
        $form_data['company_phone'] = $company->phone;
        $form_data['company_pib']   = 'Новий менеджер';

        $user        = $this->initUser($form_data, $company->phoneCode, $company->phoneStationaryCode);
        $user_locale = $this->initUserLocale($form_data);

        $validation_success = $this->validateORM([
            $user,
            $user_locale
        ]);

        // додатково валідація паролю
        $ext_validation = Validation::factory($form_data)
                                    ->rule('password', 'not_empty')
                                    ->rule('password', 'regex', [':value', self::PASS_REGEX])
                                    ->rule('password', 'min_length', [':value', self::PASS_MIN_LENGTH])
                                    ->rule('password', 'max_length', [':value', self::PASS_MAX_LENGTH]);

        if (!$ext_validation->check()) {
            $this->addErrors($ext_validation->errors(), 'user');
            $validation_success = false;
        }

        if (!$user->email_available()) {
            $this->addErrors([
                'email' => [
                    'email_available',
                    [$form_data['company_email']]
                ]
            ], 'user');
            $validation_success = false;
        }

//        var_dump($this->errors); die;

        if ($validation_success) {
            try {
                Model_Frontend_Transaction::start();

                $user->company = $company;
                $user->save();

                $user_locale->user = $user;
                $user_locale->save();

                // вставляємо тимчасове посилання для підтвердження
                $temp_link = ORM::factory('Frontend_Registration_Link');
                /** @var $temp_link Model_Frontend_Registration_Link */
                $temp_link->user = $user;
                $temp_link
                    ->initRegistrationLink('confirm_email')
                    ->save();

                Frontend_Helper_Mail::send_mail($user->email, __db('email.confirm_registration.title'), 'confirm-registration', [
                    'link' => 'http://tirscan.com' . Route::url('frontend_site_company_registration_confirmation',
                            [
                                'code' => $temp_link->temp,
                                'lang' => $this->request->param('lang')
                            ]),
                    'username' => $user_locale->name,
                    'email' => $user->email
                ]);

                /**
                 * якщо дійшли сюди, то все ок - виконуємо комміт транзакції
                 */
                Model_Frontend_Transaction::commit();

                // вставляємо бонус користувачу
                Model_Frontend_Wallet_History::query_insertHistory(
                    $user->ID,
                    'bonus',
                    'wallet.bonusForRegistration',
                    25 // TODO: читання бонуса за реєстрацію
                );

                $ref_link->delete();

                $this->addStoredNotification('success', 'Дякуємо за реєстрацію! На Вашу пошту було відправлено лист для підтвердження введених даних');
                HTTP::redirect(Route::url('frontend_index', $this->request->param()));
            }
            catch (HTTP_Exception_Redirect $e) {
                throw $e;
            }
            catch (Exception $e) {
                Model_Frontend_Transaction::rollback();

                throw new HTTP_Exception_500();
            }
        }
        else {
            $this->addNotification('error', "Правильно заповніть усі поля форми!");
        }
    }

    public function handleRegistrationPost($form_data) {
        $company_type           = ORM::factory('Frontend_Company_Type', Arr::get($form_data, 'company_type'));
        $company_ownership_type = ORM::factory('Frontend_Company_Ownership_Type', Arr::get($form_data, 'company_ownership_type'));
        $phone_code             = ORM::factory('Frontend_Phonecodes', Arr::get($form_data, 'company_phone_code'));
        $phone_stationary_code  = ORM::factory('Frontend_Phonecodes', Arr::get($form_data, 'stationary_phone_code'));

        if (!($company_type->loaded()
              && $company_ownership_type->loaded()
              && $phone_code->loaded()
              && $phone_stationary_code->loaded())
        ) {
            throw new HTTP_Exception_400;
        }

        $company        = $this->initCompany($form_data, $company_type, $company_ownership_type, $phone_code, $phone_stationary_code);
        $company_locale = $this->initCompanyLocale($form_data);

        $user        = $this->initUser($form_data, $phone_code, $phone_stationary_code);
        $user_locale = $this->initUserLocale($form_data);

        $validation_success = $this->validateORM([
            $company,
            $company_locale,
            $user,
            $user_locale
        ]);

        // додатково валідація паролю і geo
        $ext_validation = Validation::factory($form_data)
                                    ->rule('password', 'not_empty')
                                    ->rule('password', 'regex', [':value', self::PASS_REGEX])
                                    ->rule('password', 'min_length', [':value', self::PASS_MIN_LENGTH])
                                    ->rule('password', 'max_length', [':value', self::PASS_MAX_LENGTH])
                                    ->rule('place', 'not_empty')
                                    ->rule('place_data', 'not_empty');

        if (!$ext_validation->check()) {
            $this->addErrors($ext_validation->errors(), 'user');
            $validation_success = false;
        }

        if (!$user->email_available()) {
            $this->addErrors([
                'email' => [
                    'email_available',
                    [$form_data['company_email']]
                ]
            ], 'user');
            $validation_success = false;
        }

        if ($validation_success) {
            Session::instance()
                   ->delete('add_request_buffer');

            try {
                Model_Frontend_Transaction::start();

                $company->save();

                $company_locale->company = $company;
                $company_locale->save();

                $user->company = $company;
                $user->save();

                $user_locale->user = $user;
                $user_locale->save();

                // вставляємо тимчасове посилання для підтвердження
                $temp_link = ORM::factory('Frontend_Registration_Link');
                /** @var $temp_link Model_Frontend_Registration_Link */
                $temp_link->user = $user;
                $temp_link
                    ->initRegistrationLink('confirm_email')
                    ->save();

                // якщо була реєстрація з прив'язкою соцпрофілю
                if (isset($form_data['social_data'])) {
                    ORM::factory('Frontend_User_Social')
                       ->set('user', $user)
                       ->set('network', $form_data['social_data']['network'])
                       ->set('identity', $form_data['social_data']['identity'])
                       ->set('socialUID', $form_data['social_data']['uid'])
                       ->set('email', $form_data['social_data']['email'])
                       ->save();

                    Session::instance()
                           ->delete('social_auth_data');
                }

                // вставляємо дані geo компанії
                $place_data = json_decode($form_data['place_data'], true);
                $this->handleCompanyGeo($company, $place_data);

                // призначення власника
                $user->makeOwner($company);

                Frontend_Helper_Mail::send_mail($user->email, __db('email.confirm_registration.title'), 'confirm-registration', [
                    'link' => 'http://tirscan.com' . Route::url('frontend_site_company_registration_confirmation',
                            [
                                'code' => $temp_link->temp,
                                'lang' => $this->request->param('lang')
                            ]),
                    'username' => $user_locale->name,
                    'email' => $user->email
                ]);

                /**
                 * якщо дійшли сюди, то все ок - виконуємо комміт транзакції
                 */
                Model_Frontend_Transaction::commit();

                // вставляємо бонус користувачу
                Model_Frontend_Wallet_History::query_insertHistory(
                    $user->ID,
                    'bonus',
                    'wallet.bonusForRegistration',
                    25 // TODO: читання бонуса за реєстрацію
                );

                $this->addStoredNotification('success', 'Дякуємо за реєстрацію! На Вашу пошту було відправлено лист для підтвердження введених даних');
                HTTP::redirect(Route::url('frontend_index', $this->request->param()));
            }
            catch (HTTP_Exception_Redirect $e) {
                throw $e;
            }
            catch (Exception $e) {
                Model_Frontend_Transaction::rollback();

                throw new HTTP_Exception_500();
            }
        }
        else {
            $this->addNotification('error', "Правильно заповніть усі поля форми!");
        }
    }

    public function mergeAddRequestBufferToPost() {
        $result = $this->request->post();

        $request_buffer = Session::instance()
                                 ->get('add_request_buffer', []);
        if ($request_buffer !== []) {
            $result = array_merge($result, $request_buffer);
        }

        return $result;
    }

    public function action_registration() {
        if ($this->authorized) {
            HTTP::redirect(Route::url('frontend_index', $this->request->param()));
        }

        $company = null;
        $code    = $this->request->query('code', null);
        if ($code !== null) {
            $manager_link = ORM::factory('Frontend_Registration_ManagerLink')
                               ->where('hashLink', '=', $code)
                               ->and_where('availableUntil', '>', DB::expr('NOW()'))
                               ->find();

            if ($manager_link->loaded()) {
                $company = $manager_link->company;

                if (!$company->loaded()) {
                    throw new HTTP_Exception_500;
                }
            }
        }

        $post = $this->request->post();

        $social_data = Session::instance()
                              ->get('social_auth_data', null); // отримуєм дані соцпрофілю, якщо реєстрація через соцмережу
        if ($social_data !== null) {
            $post['company_email'] = $social_data['email'];
            $post['company_pib']   = $social_data['first_name'] . ' ' . $social_data['last_name'];

            $post['social_data'] = $social_data;
        }

        if ($this->request->method() == Kohana_HTTP_Request::POST) {
            if ($company != null) {
                $this->handleRegistrationManagerPost($manager_link, $company, $post);
            }
            else {
                $this->handleRegistrationPost($post);
            }
        }

        // додаємо js-скрипт сторінки реєстрації
        $this->addLocalScript('page/company/registration');

        $this->l10n = [
            'title' => __db('company.registration.title')
        ];

        // читаємо дані з БД для селектів форми реєстрації
        $phone_codes     = ORM::factory('Frontend_Phonecodes')
                              ->find_all();
        $company_types   = ORM::factory('Frontend_Company_Type')
                              ->find_all();
        $ownership_types = ORM::factory('Frontend_Company_Ownership_Type')
                              ->find_all();

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_company_registration', $this->request->param()));

        $this->blocks['content'] = View::factory('frontend/content/companies/registration', compact('company', 'phone_codes', 'company_types', 'ownership_types', 'post'));
    }

    public function action_confirm_registration() {
        $this->auto_render = false;

        // видаляємо застарілі (минув ttl) посилання
        Model_Frontend_Registration_Link::query_deleteOutdatedLinks();

        $code      = $this->request->param('code', '');
        $temp_link = ORM::factory('Frontend_Registration_Link', ['temp' => $code, 'type' => 'confirm_email',]);

        if (!$temp_link->loaded()) {
            throw new HTTP_Exception_404();
        }
        else {
            try {
                $temp_link->user
                    ->set('emailApproved', 'approved')
                    ->save();

                $temp_link->delete();
            }
            catch (Exception $e) {
                throw new HTTP_Exception_500();
            }

            $this->addStoredNotification('success', 'Ваш e-mail успішно підтверджений! Тепер ви можете увійти до системи');
            HTTP::redirect(Route::url('frontend_index', $this->request->param()));
        }
    }

    public function action_confirm_forgot_password() {
        $this->auto_render = false;

        Model_Frontend_Registration_Link::query_deleteOutdatedLinks();
        $code = $this->request->param('code', '');

        $temp_link = ORM::factory('Frontend_Registration_Link', [
            'temp' => $code,
            'type' => 'confirm_password'
        ]);

        $id   = $temp_link->user;
        $temp = $temp_link->temp;

        $password      = Model_Frontend_Registration_Link::getNewPassword($id, $temp);
        $hash_password = md5($password[0]['casual_password']);

        if (!$temp_link->loaded()) {
            throw new HTTP_Exception_404();
        }
        else {
            try {
                $temp_link->user
                    ->set('hash', $hash_password)
                    ->save();

                $temp_link->delete();
            }
            catch (Exception $e) {
                throw new HTTP_Exception_500();
            }

            $this->addStoredNotification('success', 'Ваш пароль успішно змінений');
            HTTP::redirect(Route::url('frontend_index', $this->request->param()));
        }
    }

    public function action_list() {

        $page_size = 12;

        // отримуємо номер сторінки
        $page_number = $this->request->param('page', null);
//        if (($page_number == 1) and (Route::name($this->request->route()) == 'frontend_site_company_list')) {
//            HTTP::redirect(Route::url('frontend_site_company_list_first_page', ['lang' => $this->language]), 301);
//        }
        if ($page_number === null) {
            $page_number = 1;
        }

        // агрегація отриманих даних запиту через GET
        $filter_form               = [];
        $filter_form['f_text']     = $this->request->query('f_text');
        $filter_form['f_place']    = $this->request->query('f_place');
        $filter_form['f_place_id'] = json_decode($this->request->query('f_place_id'), true)['place_id'];
        $filter_form['f_type']     = $this->request->query('f_type');
        $filter_form['f_rating']   = $this->request->query('f_rating');
        $filter_form['f_sort']     = $this->request->query('f_sort');

//        $companies = $this->getCompanyListFromQuery($filter_form, 1, 100000);
//        foreach ($companies as $company) {
//            $new_name = Frontend_Helper_URL::title($company['primaryName']);
//
//            if ($new_name == '') { $new_name = $company['ID']; }
////            echo $company['url'] . '<br>';
//            if ($company['db_url'] == '') {
//                $query = DB::update(Model_Frontend_Company::TABLE)
//                  ->set(['url' => $new_name])
//                  ->where('ID', '=', $company['ID']);
//
//                echo (string) $query . '<br>';
//                $query->execute();
//            }
//        }
//        die;

        $companies_count = $this->getCompaniesCountFromQuery($filter_form);

        // формуємо блок пагінації для списку компаній (після читання компаній з БД - щоб знати кількість)
        $pagination_manager = $this->preparePaginationBlock('pagination_companies', 'default', Route::get('frontend_site_company_list'), $companies_count, $page_number, $page_size);
        if ($page_number > $pagination_manager->getPageCount()) {
            $page_number = 1;
        }

        // читаємо компанії порталу
        $companies = $this->getCompanyListFromQuery($filter_form, $page_number, $page_size);

        $this->styles[]  = '/assets/css/fontawesome-stars.css';
        $this->scripts[] = '/assets/js/jquery.barrating.min.js';
        $this->addLocalScript('page/company/list');

        $this->l10n                    = [
            'title' => __db('SEO_company_title')
        ];
        $this->seo['title_for_header'] = __db('company.list.title');
        $this->seo['description']      = __db('SEO_company_description');
        $this->seo['content_text']     = __db('SEO_company_content');

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->seo['title_for_header'], 'frontend_site_company_list_first_page', $this->getRequestParams(['page' => null])));

        // якщо знайшли компанії
        if ($pagination_manager->getPageCount() > 0) {
            $this->blocks['companies'] = View::factory('frontend/content/companies/list/results', compact('companies'));
        }
        else { // інакше виводим повідомлення про відсутність компаній
            $back_link_url = Route::url('frontend_site_company_list_first_page', ['lang' => $this->language]);

            $this->blocks['companies'] = View::factory('frontend/content/companies/list/not_found', compact('back_link_url'));
        }

        $search_form_action      = Route::url('frontend_site_company_list', ['lang' => $this->language]);
        $this->blocks['content'] = View::factory('frontend/content/companies/list', compact('filter_form', 'search_form_action'));
    }

    /**
     * @param $company_id
     * @return array|bool
     */
    protected function getDocumentsForCompany($company_id) {
        $paths = Kohana::$config->load('frontend/site.company.paths.documents');
        if ($paths === null) {
            return false;
        }

        $result = [];
        foreach ($paths as $name => $path) {
            $full_paths = glob($_SERVER['DOCUMENT_ROOT'] . sprintf($path, $company_id));

            // опускаєм '.' та '..'
            foreach ($full_paths as $full_path) {
                if (($full_path != '.') && ($full_path != '..')) {
                    $result[$name][] = [
                        'url' => $full_path,
                        'title' => basename($full_path)
                    ];
                }
            }
        }

        return (count($result) > 0) ? $result : false;
    }

    /**
     * @param $documents
     * @return int
     */
    protected function getDocumentsCount($documents) {
        // документ визначається масивом з двома ключами (url і name), тому ділимо на 2
        return (int)(Frontend_Helper_Arr::countSub($documents) / 2);
    }

    public function action_page() {

        $post = $this->request->post();
        if ($post) {
            $companyID = $post['company'];
            $userID    = $this->user->ID;

            if ($post['action'] == 'subscribe') {
                Model_Frontend_Company::query_user_company_subscribe_request($userID, $companyID);
            }
            if ($post['action'] == 'unsubscribe') {
                Model_Frontend_Company::query_user_company_unsubscribe_request($userID, $companyID);
            }
            HTTP::redirect(Route::url('frontend_site_company_page', $this->request->param()));
        }

        $this->styles[]  = '/assets/css/fontawesome-stars.css';
        $this->scripts[] = '/assets/js/jquery.barrating.min.js';
        $this->scripts[] = '/assets/js/responsive-tabs.js';
        $this->addLocalScript('page/company/page');

        /**
         * company_url_name
         * Валідується регуляркою в роуті, має формат "число-довільний рядок",
         * тому explode завжди буде вертати масив з двох елементів (число і рядок)
         */
        $company_url_array = explode('-', $this->request->param('company_url_name'), 2);
        $company_id        = (int)$company_url_array[0];
        $company_name      = $company_url_array[1];

        $user_company_subscribe = Model_Frontend_Company::query_get_user_company_subscribe_request(
            $this->user->ID, $company_id);
        $subscribe              = count($user_company_subscribe);

        $CID = $company_id;

        $company_orm = $company = ORM::factory('Frontend_Company', $company_id);
        if (!$company->loaded()) // якщо в базі не існує компанія з таким ID
        {
            throw new HTTP_Exception_404(__db('error.404.companyNotFound'));
        }

        $documents_count = 0;
        $documents       = $this->getDocumentsForCompany($company->ID); // читаєм документи компанії з диску
        if ($documents !== false) {
            $documents_count = $this->getDocumentsCount($documents);
        }

        $company_image = $this->getCompanyImage($company->ID);

        $this->blocks['company/documents'] = 'frontend/content/companies/page/documents';


        /** @var $company_primary_locale Model_Frontend_Company_Locale */
        $company_primary_locale = $company->locales
            ->where('localeID', '=', $company->primaryLocaleID)
            ->find();

        /** @var $company_locale Model_Frontend_Company_Locale */
        $company_locale = $company->locales
            ->where('localeID', '=', $this->locale->ID)
            ->find();

        // якщо не знайдено перекладу компанії на поточній мові, то використовуємо переклад на мові реєстрації (завжди існує)
        if (!$company_locale->loaded()) {
            $company_locale = $company_primary_locale;
        }

        $company           = Model_Frontend_Company::query_getCompanyByID($this->locale->ID, $company_id);
        $company['rating'] = $this->getCompanyRating($company['ID']);

        $requests       = $this->getCompanyRequests($company['ID']);
        $requests_count = $this->getCompanyRequestsCount($company['ID']);
        $comments       = $this->getCompanyComments($company['ID']);

        if ($company_orm->url != $company_name) { // якщо назви не співпадають, то url не правильне
            if ($company_orm->oldUrl == $company_name) {
                $params                     = $this->request->param();
                $params['company_url_name'] = $company_id . '-' . $company_orm->url;

                // робимо редірект на правильний URL
                HTTP::redirect(Route::url('frontend_site_company_page', $params));
            }
            else {
                throw new HTTP_Exception_404(__db('error.404.companyNotFound'));
            }
        }
        //  -   -   -   -   -   -   -   -   -   -   -   - WIDGETS:


        View::set_global('invite_button',

            (!($is_owner = ($this->user->companyID == $CID)))
                ?
                Request::factory(
                    Route::url('@widget', ['controller' => 'partner', 'action' => 'invite_button', 'param' => $CID])
                )->post($this->request->post())->execute()->body()
                : ''
        );

        if ($this->request->post('action-partner') == 'make-partner') {
            HTTP::redirect(Route::url('frontend_site_company_page', $this->request->param()));
        }

        if (($userID = Frontend_Auth::userID()) !== false) {

            $this->aside_widgets[] = [
                'name' => __db('my.partners.list'),
                'content' => Request::factory(Route::url('@widget', ['controller' => 'partner', 'action' => 'list', 'param' => $CID]))
                                    ->execute()->body(),
            ];

        }
        //  -   -   -   -   -   -   -   -   -   -   -   -

        $this->l10n = [
            'title' => __db('company.page.title(:company)', [':company' => $company_locale->name])
        ];

        $last_news = Model_Frontend_News_NewsSite::getLastNewsByCompanyId(5, $company_id, $this->locale->ID);

        $transports = Model_Frontend_User_Car::query_getTransportByCompanyID($company_id);


        if ($this->authorized) {
            $documents_blocked =
                !(Model_Frontend_User_Transport_Application::query_applicationExist($company_id, $this->user->ID)
                  || Model_Frontend_User_Cargo_Application::query_applicationExist($company_id, $this->user->ID)
                  || ($this->user->company->ID == $company_id));
        }
        else {
            $documents_blocked = true;
        }

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(false, __db('company.list.title'), 'frontend_site_company_list_first_page', $this->request->param()));
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_company_page', $this->request->param()));

        $this->blocks['content'] = View::factory('frontend/content/companies/page', compact('transports', 'documents_blocked', 'company', 'comments', 'requests', 'requests_count', 'documents', 'documents_count', 'company_image', 'last_news', 'subscribe'));
    }

    public function action_page_news() {

        $news_id = $this->request->param('id');

        $company_news_data = Model_Frontend_News_NewsSite::getOneNewsById($news_id, $this->locale->ID);

        $this->blocks['content'] = View::factory('frontend/content/companies/news_list/page', compact('company_news_data'));
    }

    public function action_list_news() {

        $page_size   = 12;
        $page_number = $this->request->param('page', null);
        $company_id  = $this->request->param('company_id');
        $news_count  = Model_Frontend_News_NewsSite::countCompanyNews($company_id);

        if ($page_number === null) {
            $page_number = 1;
        }

        $pagination_manager = $this->preparePaginationBlock('pagination_company_news', 'default',
            Route::get('frontend_site_company_list_news'), $news_count, $page_number, $page_size);

        $this->l10n = [
            'title' => __db('news-company (:company)', [':company' => 'test1'])
        ];

        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_company_list_news', $this->getRequestParams(['page' => null])));

        if (!empty($company_id)) {
            $company_news_list = Model_Frontend_News_NewsSite::getNewsByCompanyId($company_id, $this->locale->ID, $page_size, $page_size * ($page_number - 1));
        }

        if ($pagination_manager->getPageCount() > 0) {
            $this->blocks['content'] = View::factory('frontend/content/companies/news_list/results', compact('company_news_list'));
        }

    }

}