<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Requests extends Controller_Frontend_Authorized {

    public function before() {
        parent::before();

        $this->addLocalScript('linker/handlebars_helpers_extension');

        $this->blocks['header'] = 'frontend/blocks/header_page';
    }

    public static function getUpdateCountForTemplate(Model_Frontend_Filter_Template $template, $google_lang) {
        $is_cargo = ($template->type == 'cargo');

        parse_str($template->query, $query);

        $filter_form = self::getFiltersFromQuery($query);
        $filters     = self::prepareFilters($google_lang, $filter_form, $is_cargo);

        if ($is_cargo) {
            $filters[] = [
                ['t_cargo.status', '=', 'active.have-no-proposition'],
                ['t_cargo.status', '=', 'active.have-not-confirmed']
            ];
        }
        else {
            $filters[] = [
                ['t_trans.status', '=', 'active.have-no-proposition'],
                ['t_trans.status', '=', 'active.have-not-confirmed']
            ];
        }

        $filters[] = ['created', '>', $template->lastUpdateTime];

        if ($is_cargo) {
            $count = (int)Model_Frontend_Cargo::query_getCargoCount($filters);
        }
        else {
            $count = (int)Model_Frontend_Transport::query_getTransportCount($filters);
        }

        return $count;
    }

    public static function getActiveTemplatesUpdates($type, $user_id, $google_lang) {
        $templates = ORM::factory('Frontend_Filter_Template')
                        ->where('type', '=', $type)
                        ->and_where('active', '=', 'yes')
                        ->and_where('userID', '=', $user_id)
                        ->find_all();

        $result = [];
        foreach ($templates as $template) {
            $count = self::getUpdateCountForTemplate($template, $google_lang);

            if ($count > 0) {
                $result[] = [
                    'template' => $template,
                    'raw' => [
                        'id' => $template->ID,
                        'name' => $template->name
                    ],
                    'count' => $count,
                ];
            }
        }

        return $result;
    }

    public function initCar(array $post_data) {
        $existing_car_id = $this->request->post('existing_car_id');
        if ($existing_car_id != null) {
            $new_car = ORM::factory('Frontend_User_Car', $existing_car_id);
        }
        else {
            $new_car = ORM::factory('Frontend_User_Car')
                          ->set('carTypeID', Arr::get($post_data, 'car_type'))
                          ->set('type', Arr::get($post_data, 'car_body_type'))
                          ->set('sizeX', Arr::get($post_data, 'size_X'))
                          ->set('sizeY', Arr::get($post_data, 'size_Y'))
                          ->set('sizeZ', Arr::get($post_data, 'size_Z'))
                          ->set('liftingCapacity', Arr::get($post_data, 'weight'))
                          ->set('volume', Arr::get($post_data, 'volume'));
        }

        return $new_car;
    }

    /**
     * @return Model_Frontend_Transport
     */
    public function initTransportRequest(array $post_data) {
        $new_request = ORM::factory('Frontend_Transport')
                          ->set('dateFrom', self::convert_dmY_to_Ymd(Arr::get($post_data, 'date_from')))
            // документи
                          ->set('docTIR', Arr::get($post_data, 'doc_TIR', 'no'))
                          ->set('docCMR', Arr::get($post_data, 'doc_CMR', 'no'))
                          ->set('docT1', Arr::get($post_data, 'doc_T1', 'no'))
                          ->set('docSanPassport', Arr::get($post_data, 'doc_sanpassport', 'no'))
                          ->set('docSanBook', Arr::get($post_data, 'doc_sanbook', 'no'))
            // умови завантаження
                          ->set('loadFromSide', Arr::get($post_data, 'load_side', 'no'))
                          ->set('loadFromTop', Arr::get($post_data, 'load_top', 'no'))
                          ->set('loadFromBehind', Arr::get($post_data, 'load_behind', 'no'))
                          ->set('loadTent', Arr::get($post_data, 'load_tent', 'no'))
                          ->set('condPlomb', Arr::get($post_data, 'cond_plomb', 'no'))
                          ->set('condLoad', Arr::get($post_data, 'cond_reload', 'no'))
                          ->set('condBelts', Arr::get($post_data, 'cond_belts', 'no'))
                          ->set('condRemovableStands', Arr::get($post_data, 'cond_removable_stands', 'no'))
                          ->set('condHardSide', Arr::get($post_data, 'cond_bort', 'no'))
                          ->set('condCollectableCargo', Arr::get($post_data, 'cond_collectable', 'no'));

        if (Arr::get($post_data, 'date_to') != null) {
            $new_request->set('dateTo', self::convert_dmY_to_Ymd(Arr::get($post_data, 'date_to')));
        }

        // додаткові умови (з числовою величиною - температура, кількість і т.п.)
        if (Arr::get($post_data, 't', 'no') == 'yes') {
            $new_request->set('condTemperature', Arr::get($post_data, 't_amount'));
            $new_request->set('condTemperatureTo', Arr::get($post_data, 't_amount_to'));
        }

        if (Arr::get($post_data, 'pallets', 'no') == 'yes') {
            $new_request->set('condPalets', Arr::get($post_data, 'pallets_amount'));
        }

        if (Arr::get($post_data, 'ADR', 'no') == 'yes') {
            $new_request->set('condADR', Arr::get($post_data, 'ADR_amount'));
        }

        return $new_request;
    }

    public static function convert_dmY_to_Ymd($date) {
        if ($date == null) {
            return null;
        }

        $date_from = DateTime::createFromFormat('d.m.Y', $date);
        $date_from->setTime(0, 0, 0);

        return $date_from->format('Y-m-d H:i:s');
    }

    /**
     * @return Model_Frontend_Cargo
     */
    public function initCargoRequest(array $post_data) {
        $new_request = ORM::factory('Frontend_Cargo')
                          ->set('dateFrom', self::convert_dmY_to_Ymd(Arr::get($post_data, 'date_from', '')))
            // дані вантажу
                          ->set('cargoType', Arr::get($post_data, 'cargo_type', ''))
                          ->set('weight', Arr::get($post_data, 'cargo_weight', null))
                          ->set('volume', Arr::get($post_data, 'cargo_volume', null))
                          ->set('sizeX', Arr::get($post_data, 'size_X', null))
                          ->set('sizeY', Arr::get($post_data, 'size_Y', null))
                          ->set('sizeZ', Arr::get($post_data, 'size_Z', null))
                          ->set('carCount', Arr::get($post_data, 'car_count', null))
                          ->set('transportTypeID', Arr::get($post_data, 'car_type', null))
                          ->set('info', Arr::get($post_data, 'info', ''))
            // документи
                          ->set('docTIR', Arr::get($post_data, 'doc_TIR', 'no'))
                          ->set('docCMR', Arr::get($post_data, 'doc_CMR', 'no'))
                          ->set('docT1', Arr::get($post_data, 'doc_T1', 'no'))
                          ->set('docSanPassport', Arr::get($post_data, 'doc_sanpassport', 'no'))
                          ->set('docSanBook', Arr::get($post_data, 'doc_sanbook', 'no'))
            // умови завантаження
                          ->set('loadFromSide', Arr::get($post_data, 'load_side', 'no'))
                          ->set('loadFromTop', Arr::get($post_data, 'load_top', 'no'))
                          ->set('loadFromBehind', Arr::get($post_data, 'load_behind', 'no'))
                          ->set('loadTent', Arr::get($post_data, 'load_tent', 'no'))
                          ->set('condPlomb', Arr::get($post_data, 'cond_plomb', 'no'))
                          ->set('condLoad', Arr::get($post_data, 'cond_reload', 'no'))
                          ->set('condBelts', Arr::get($post_data, 'cond_belts', 'no'))
                          ->set('condRemovableStands', Arr::get($post_data, 'cond_removable_stands', 'no'))
                          ->set('condHardSide', Arr::get($post_data, 'cond_bort', 'no'))
                          ->set('condCollectableCargo', Arr::get($post_data, 'cond_collectable', 'no'))
                          ->set('onlyTransporter', Arr::get($post_data, 'only_transporter', 'no'));

        if (Arr::get($post_data, 'date_to') != null) {
            $new_request->set('dateTo', self::convert_dmY_to_Ymd(Arr::get($post_data, 'date_to', '')));
        }

        // додаткові умови (з числовою величиною - температура, кількість і т.п.)
        if (Arr::get($post_data, 't', 'no') == 'yes') {
            $new_request->set('condTemperature', Arr::get($post_data, 't_amount'));
        }

        if (Arr::get($post_data, 'pallets', 'no') == 'yes') {
            $new_request->set('condPalets', Arr::get($post_data, 'pallets_amount'));
        }

        if (Arr::get($post_data, 'ADR', 'no') == 'yes') {
            $new_request->set('condADR', Arr::get($post_data, 'ADR_amount'));
        }

        return $new_request;
    }

    public function initPrice(array $post_data) {
        $price = Arr::get($post_data, 'price', null);
        if ($price === 0) {
            $price = null;
        }

        $new_price = ORM::factory('Frontend_Request_Price')
                        ->set('value', $price)
                        ->set('paymentType', Arr::get($post_data, 'payForm', null))
                        ->set('PDV', Arr::get($post_data, 'pdv', 'no'))
                        ->set('onLoad', Arr::get($post_data, 'on_load', 'no'))
                        ->set('onUnload', Arr::get($post_data, 'on_unload', 'no'))
                        ->set('onPrepay', Arr::get($post_data, 'advanced_payment', 'no'))
                        ->set('advancedPayment', Arr::get($post_data, 'advanced_payment_amount', null))
                        ->set('currencyID', 1)
                        ->set('specifiedPrice', 'yes');

        return $new_price;
    }

    public function initOrder(array $post_data) {
        if (Arr::get($post_data, 'service_type') != 'regular') {
            $new_order = ORM::factory('Frontend_Wallet_Order');
        }
        else {
            $new_order = false;
        }

        return $new_order;
    }

    /**
     * @param $place_data
     * @return Model_Frontend_Geo
     */
    public function getPlaceForRequest($place_data) {
        $geo_place = ORM::factory('Frontend_Geo', ['placeID' => $place_data['place_id']]);
        if (!$geo_place->loaded()) {
            // TODO: вставка ієрархії і перекладів

            $geo_place = ORM::factory('Frontend_Geo')
                            ->set('placeID', $place_data['place_id'])
                            ->set('lat', $place_data['lat'])
                            ->set('lng', $place_data['lat'])
                            ->set('parentID', null)
                            ->set('level', 1)
                            ->save();

            ORM::factory('Frontend_Geo_Locale')
               ->set('geo', $geo_place)
               ->set('locale', $this->locale)
               ->set('name', $place_data['name'])
               ->set('fullAddress', $place_data['full_address'])
               ->set('country', '')
               ->save();
        }

        return $geo_place;
    }

    public function handleRequestGeo(Model_Frontend_Transport $request, $geo_data, $type, $group) {
        if ($geo_data == null) {
            return;
        }

        $ids_str = Request::factory('/cms/geo/' . $this->locale->googleLang . '/add')
                          ->post($geo_data)
                          ->execute()
                          ->body();
        $ids     = explode(',', $ids_str);

        $request->addGeoRelationByIDs($ids, $type, $group);
    }

    public function handleCargoRequestGeo(Model_Frontend_Cargo $request, $geo_data, $type, $group) {
        if ($geo_data == null) {
            return;
        }

        $ids_str = Request::factory('/cms/geo/' . $this->locale->googleLang . '/add')
                          ->post($geo_data)
                          ->execute()
                          ->body();
        $ids     = explode(',', $ids_str);

        $request->addGeoRelationByIDs($ids, $type, $group);
    }

    public function handleNewTransportPost($existing_id = null) {

        if ($existing_id == null) {
            $new_car     = $this->initCar($this->request->post());
            $new_price   = $this->initPrice($this->request->post());
            $new_request = $this->initTransportRequest($this->request->post());
            $new_order   = $this->initOrder($this->request->post());
        }
        else {
            $new_car = 1;
            $new_price = 2;
            $new_request = 3;
            $new_order = null;
        }

        $validation_success = $this->validateORM([
            $new_car,
            $new_price,
            $new_request
        ]);

        if (($this->request->post('load_place') == null) || ($this->request->post('unload_place') == null)) {
            $validation_success = false;
        }

        $service      = $this->request->post('service_type');
        $calc_price   = null;
        $service_name = '';
        switch ($service) {
            case 'vip':
                $calc_price = $this->request->post('service_vip_duration') * 120;

                $new_request->isVip = 'yes';
                $service_name       = 'wallet.serviceVipRequest';
                break;
            case 'color':
                $calc_price = $this->request->post('service_color_duration') * 120;

                $new_request->isColored = 'yes';
                $service_name           = 'wallet.serviceColoredRequest';
                break;
        }

        if ($calc_price !== null) {
            if ($calc_price > $this->user->wallet) {
                $this->addStoredNotification('error', 'Вартість обраної послуги ' . $calc_price . ' грн перевищує поточний баланс Вашого гаманця (' . (float)$this->user->wallet . ' грн)');
                $validation_success = false;
            }
        }

        if ($validation_success) {
            try {
                if (!$this->authorized) {
                    // зберігаємо дані заявки у сесію
                    Session::instance()
                           ->set('add_request_buffer', $this->request->post());

                    $this->addStoredNotification('success', 'Успішно! Ваша заявка була збережена у системі. Завершіть процедуру реєстрації на порталі і Ваша заявка буде розміщена');
                    HTTP::redirect(Route::url('frontend_site_company_registration', $this->request->param())); // перехід на сторінку реєстрації
                }
                else {
                    Model_Frontend_Transaction::start();

                    if (!$new_car->loaded()) { // вставляємо машину, якщо нова
                        $new_car
                            ->set('userID', $this->user->ID)
                            ->save();
                    }

                    $new_price->save(); // вставляємо ціну

                    // вставляємо заявку
                    $new_request->userCar = $new_car;
                    $new_request->price   = $new_price;
                    $new_request->user    = $this->user;
                    $new_request->save();

                    // вставляємо дані geo заявки
                    $this->handleRequestGeo(
                        $new_request,
                        json_decode($this->request->post('load_place'), true),
                        'from',
                        1
                    );
                    $this->handleRequestGeo(
                        $new_request,
                        json_decode($this->request->post('unload_place'), true),
                        'to',
                        1
                    );

                    $load_places = $this->request->post('load_places');
                    $i           = 2;
                    if (is_array($load_places)) {
                        foreach ($load_places as $load_place) {
                            $this->handleRequestGeo(
                                $new_request,
                                json_decode($load_place, true),
                                'from',
                                $i++
                            );
                        }
                    }

                    $unload_places = $this->request->post('unload_places');
                    $i             = 2;
                    if (is_array($unload_places)) {
                        foreach ($unload_places as $unload_place) {
                            $this->handleRequestGeo(
                                $new_request,
                                json_decode($unload_place, true),
                                'to',
                                $i++
                            );
                        }
                    }

                    Model_Frontend_Transaction::commit();

                    // вставляємо бонус користувачу
                    Model_Frontend_Wallet_History::query_insertHistory(
                        $this->user->ID,
                        'bonus',
                        'wallet.bonusForAddingCargo',
                        1 // TODO: читання бонуса за реєстрацію
                    );

                    if ($service != 'regular') {
                        // знімаємо за послугу
                        Model_Frontend_Wallet_History::query_insertHistory(
                            $this->user->ID,
                            'service',
                            $service_name,
                            -$calc_price // TODO: читання вартості послуги?
                        );
                        $this->user->wallet = $this->user->wallet - $calc_price;
                        $this->user->save();
                    }

                    $this->addStoredNotification('success', 'Успішно! Ваша заявка була добавлена в систему');
                    HTTP::redirect(Route::url('frontend_site_request_transport_list', $this->request->param()));
                }
            }
            catch (HTTP_Exception_Redirect $e) { // якщо отримали редірект
                throw $e; // то прокидуємо його далі для обробкою Kohana
            }
            catch (Exception $e) {
                /*
                 * якщо ми тут, то значить була помилка при вставці заявки до БД
                 * дані пройшли валідацію, але не були вставлені, тому це захист від непередбачених проблем
                 * (наприклад, помилка зі структурою БД, неіснуючі ключі (підміна даних форм) і т.п.)
                 */

                Model_Frontend_Transaction::rollback(); // відкат усіх змін

                $this->addNotification('error', "Не вдалось розмістити заявку! Спробуйте, будь ласка, пізніше або зверніться до адміністраторів порталу");
            }
        }
        else {
            $this->addNotification('error', "Правильно заповніть усі обов'язкові поля форми!");
        }
    }

    public function action_addTransport() {
        if ($this->authorized && !$this->user->hasRights('transport')) {
            throw new HTTP_Exception_403();
        }

        $existing_id = $this->request->query('edit_id', null);
        if ($existing_id != null) {

        }

        if ($this->request->method() == Kohana_HTTP_Request::POST) {
            $this->handleNewTransportPost($existing_id);
        }

        $this->addLocalScript('page/request/add_transport');

        if ($existing_id !== null) {
            $this->l10n = [
                'title' => __db('transport.page.edit.title')
            ];
        }
        else {
            $this->l10n = [
                'title' => __db('transport.page.add.title')
            ];
        }

        // отримуємо інформацію для форми з БД
        $currencies = ORM::factory('Frontend_Currency')
                         ->find_all();
        $car_types  = ORM::factory('Frontend_User_Car_Type')
                         ->find_all();
        if (!$this->authorized) {
            $phone_codes = ORM::factory('Frontend_Phonecodes')
                              ->find_all();
        }
        else {
            $user_cars = $this->user->cars->find_all();
        }

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, __db('transport.page.list.title'), 'frontend_site_request_transport_list', $this->request->param()));
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_request_add_transport', $this->request->param()));

        $post = $this->request->post();

        // налаштування послуг
        $services = View::factory('frontend/content/services/request-add');

        $this->blocks['content'] = View::factory('frontend/content/requests/add-transport',
            compact('car_types', 'phone_codes', 'currencies', 'user_cars', 'post', 'services', 'existing_id')
        );
    }

    public function handleNewCargoPost($existing_id = null) {
        if ($existing_id == null) {
            $new_price   = $this->initPrice($this->request->post());
            $new_request = $this->initCargoRequest($this->request->post());
            $new_order   = $this->initOrder($this->request->post());
        }
        else {
            $new_price = 1;
            $new_request = 1;
            $new_order = null;
        }

        $validation_success = $this->validateORM([
            $new_price,
            $new_request
        ]);

        if (($this->request->post('load_place') == null) || ($this->request->post('unload_place') == null)) {
            $validation_success = false;
        }

        $service      = $this->request->post('service_type');
        $calc_price   = null;
        $service_name = '';
        switch ($service) {
            case 'vip':
                $calc_price = $this->request->post('service_vip_duration') * 120;

                $new_request->isVip = 'yes';
                $service_name       = 'wallet.serviceVipRequest';
                break;
            case 'color':
                $calc_price = $this->request->post('service_color_duration') * 120;

                $new_request->isColored = 'yes';
                $service_name           = 'wallet.serviceColoredRequest';
                break;
        }

        if ($calc_price !== null) {
            if ($calc_price > $this->user->wallet) {
                $this->addStoredNotification('error', 'Вартість обраної послуги ' . $calc_price . ' грн перевищує поточний баланс Вашого гаманця (' . (float)$this->user->wallet . ' грн)');
                $validation_success = false;
            }
        }

        if ($validation_success) {
            try {
                if (!$this->authorized) {
                    // зберігаємо дані заявки у сесію
                    Session::instance()
                           ->set('add_request_buffer', $this->request->post());

                    $this->addStoredNotification('success', 'Успішно! Ваша заявка була збережена у системі. Завершіть процедуру реєстрації на порталі і Ваша заявка буде розміщена');
                    HTTP::redirect(Route::url('frontend_site_company_registration', $this->request->param())); // перехід на сторінку реєстрації
                }
                else {
                    Model_Frontend_Transaction::start();

                    $new_price->save(); // вставляємо ціну

                    // вставляємо заявку
                    $new_request->price = $new_price;
                    $new_request->user  = $this->user;
                    $new_request->save();

                    // вставляємо дані geo заявки
                    $this->handleCargoRequestGeo(
                        $new_request,
                        json_decode($this->request->post('load_place'), true),
                        'from',
                        1
                    );
                    $this->handleCargoRequestGeo(
                        $new_request,
                        json_decode($this->request->post('unload_place'), true),
                        'to',
                        1
                    );

                    $load_places = $this->request->post('load_places');
                    $i           = 2;
                    if (is_array($load_places)) {
                        foreach ($load_places as $load_place) {
                            $this->handleCargoRequestGeo(
                                $new_request,
                                json_decode($load_place, true),
                                'from',
                                $i++
                            );
                        }
                    }

                    $unload_places = $this->request->post('unload_places');
                    $i             = 2;
                    if (is_array($unload_places)) {
                        foreach ($unload_places as $unload_place) {
                            $this->handleCargoRequestGeo(
                                $new_request,
                                json_decode($unload_place, true),
                                'to',
                                $i++
                            );
                        }
                    }

                    Model_Frontend_Transaction::commit();

                    // вставляємо бонус користувачу
                    Model_Frontend_Wallet_History::query_insertHistory(
                        $this->user->ID,
                        'bonus',
                        'wallet.bonusForAddingTransport',
                        1 // TODO: читання бонуса за реєстрацію
                    );

                    if ($service != 'regular') {
                        // знімаємо за послугу
                        Model_Frontend_Wallet_History::query_insertHistory(
                            $this->user->ID,
                            'service',
                            $service_name,
                            -$calc_price // TODO: читання вартості послуги?
                        );
                        $this->user->wallet = $this->user->wallet - $calc_price;
                        $this->user->save();
                    }

                    $this->addStoredNotification('success', 'Успішно! Ваша заявка була добавлена в систему');
                    HTTP::redirect(Route::url('frontend_site_request_cargo_list', $this->request->param()));
                }
            }
            catch (HTTP_Exception_Redirect $e) { // якщо отримали редірект
                throw $e; // то прокидуємо його далі для обробкою Kohana
            }
            catch (Exception $e) {
                /*
                 * якщо ми тут, то значить була помилка при вставці заявки до БД
                 * дані пройшли валідацію, але не були вставлені, тому це захист від непередбачених проблем
                 * (наприклад, помилка зі структурою БД, неіснуючі ключі (підміна даних форм) і т.п.)
                 */

                // TODO: можливо потрібно додати тут логування даних форм

//                throw $e;

                Model_Frontend_Transaction::rollback(); // відкат усіх змін

                $this->addNotification('error', "Не вдалось розмістити заявку! Спробуйте, будь ласка, пізніше або зверніться до адміністраторів порталу");
            }
        }
        else {
            $this->addNotification('error', "Правильно заповніть усі обов'язкові поля форми!");
        }
    }

    public function action_addCargo() {
        if ($this->authorized && !$this->user->hasRights('cargo')) {
            throw new HTTP_Exception_403();
        }

        $existing_id = $this->request->query('edit_id', null);
        if ($existing_id != null) {

        }

        if ($this->request->method() == Kohana_HTTP_Request::POST) {
            $this->handleNewCargoPost($existing_id);
        }

        $this->addLocalScript('page/request/add_cargo');

        if ($existing_id !== null) {
            $this->l10n = [
                'title' => __db('cargo.page.edit.title')
            ];
        }
        else {
            $this->l10n = [
                'title' => __db('cargo.page.add.title')
            ];
        }

        // отримуємо інформацію для форми з БД
        $currencies = ORM::factory('Frontend_Currency')
                         ->find_all();
        $car_types  = ORM::factory('Frontend_User_Car_Type')
                         ->find_all();
        if (!$this->authorized) {
            $phone_codes = ORM::factory('Frontend_Phonecodes')
                              ->find_all();
        }

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, __db('cargo.page.list.title'), 'frontend_site_request_cargo_list', $this->request->param()));
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_request_add_cargo', $this->request->param()));

        $post = $this->request->post();

        // налаштування послуг
        $services = View::factory('frontend/content/services/request-add');

        $this->blocks['content'] = View::factory('frontend/content/requests/add-cargo', compact('car_types', 'phone_codes', 'currencies', 'post', 'services', 'existing_id'));
    }

    public function getTransportListWithFilters($filters, $page_number, $page_size) {
        // вибірка списку транспортних заявок
        $requests = Model_Frontend_Transport::query_getTransports($this->locale->ID, false, $page_size, $page_size * ($page_number - 1), $filters);

        $result = [];
        foreach ($requests as $request) {
            if ($request['parser'] == null) {
                $request['info'] = Frontend_Helper_String::request_concat_info($request, false);
            }
            else {
                $request['info'] = Frontend_Helper_String::substr_if_more($request['info'], 20, true);
            }

            if ($request['json_sizeX'] != null) {
                $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
            }
            if ($request['json_sizeY'] != null) {
                $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
            }
            if ($request['json_sizeZ'] != null) {
                $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
            }

            $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
            $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

            if ($request['dateTo'] != null) {
                $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
            }

            $result[] = $request;
        }

        return $result;
    }

    public function getCargoListFromQuery($filters, $page_number, $page_size) {
        // вибірка списку вантажних заявок
        $requests = Model_Frontend_Cargo::query_getCargo($this->locale->ID, false, $page_size, $page_size * ($page_number - 1), $filters);

        $result = [];
        foreach ($requests as $request) {
            if ($request['parser'] == null) {
                $request['info'] = Frontend_Helper_String::request_concat_info($request, true);
            }
            else {
                $request['info'] = Frontend_Helper_String::substr_if_more($request['info'], 20, true);
            }

            if ($request['json_sizeX'] != null) {
                $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
            }
            if ($request['json_sizeY'] != null) {
                $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
            }
            if ($request['json_sizeZ'] != null) {
                $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
            }

            $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
            $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

            if ($request['dateTo'] != null) {
                $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
            }

            $result[] = $request;
        }

        return $result;
    }

    public static function processRequest($request) {
        $request['from_fullAddress'] = preg_replace('/^[^,]+,\s/', '', $request['from_fullAddress']);
        $request['to_fullAddress']   = preg_replace('/^[^,]+,\s/', '', $request['to_fullAddress']);

        $request['value'] = (float)$request['value'];

        return $request;
    }

    public static function prepareFilters($google_lang, array $query, $is_cargo = false) {
        $filters = [];

        // спільні фільтри
        if ($query['load_geo'] != null) {
            Request::factory('/cms/geo/' . $google_lang . '/add')
                   ->post($query['load_geo'])
                   ->execute();

            if ($query['load_radius'] != '') {
                $mylon = $query['load_geo']['longitude'];
                $mylat = $query['load_geo']['latitude'];

                $dist = $query['load_radius'];
                if ($dist == 0) {
                    $dist = 1;
                }

                $lon1 = $mylon - $dist / round(cos(deg2rad($mylat)) * 111.0, 6);
                $lon2 = $mylon + $dist / round(cos(deg2rad($mylat)) * 111.0, 6);

                $lat1 = $mylat - ($dist / 111.0);
                $lat2 = $mylat + ($dist / 111.0);

                $ids = Model_Frontend_Geo::query_getGeoIDsByLatLng($lon1, $lat1, $lon2, $lat2);

                $filters[] = [
                    't_geo_from.ID', 'IN', $ids
                ];
            }
            else {
                $filters[] = [
                    't_geo_from.placeID', '=', $query['load_geo']['place_id']
                ];
            }
        }
        if ($query['unload_geo'] != null) {
            Request::factory('/cms/geo/' . $google_lang . '/add')
                   ->post($query['unload_geo'])
                   ->execute();

            if ($query['unload_radius'] != '') {
                $mylon = $query['unload_geo']['longitude'];
                $mylat = $query['unload_geo']['latitude'];

                $dist = $query['unload_radius'];
                if ($dist == 0) {
                    $dist = 1;
                }

                $lon1 = $mylon - $dist / round(cos(deg2rad($mylat)) * 111.0, 6);
                $lon2 = $mylon + $dist / round(cos(deg2rad($mylat)) * 111.0, 6);

                $lat1 = $mylat - ($dist / 111.0);
                $lat2 = $mylat + ($dist / 111.0);

                $ids = Model_Frontend_Geo::query_getGeoIDsByLatLng($lon1, $lat1, $lon2, $lat2);

                $filters[] = [
                    't_geo_to.ID', 'IN', $ids
                ];
            }
            else {
                $filters[] = [
                    't_geo_to.placeID', '=', $query['unload_geo']['place_id']
                ];
            }
        }

        if (($query['dateFrom'] != null) && ($query['dateTo'] != null)) {
            $date_from = DateTime::createFromFormat('d.m.Y', $query['dateFrom']);
            $date_to   = DateTime::createFromFormat('d.m.Y', $query['dateTo']);
            if ($date_from && $date_to) {
                $filters[] = ['dateFrom', '<=', $date_to->format('Y-m-d')];
                $filters[] = ['dateTo', '>=', $date_from->format('Y-m-d')];
            }
        }
        elseif ($query['dateFrom'] != null) {
            $date_obj = DateTime::createFromFormat('d.m.Y', $query['dateFrom']);
            if ($date_obj) {
                $filters[] = ['dateTo', '>=', $date_obj->format('Y-m-d')];
            }
        }
        elseif ($query['dateTo'] != null) {
            $date_obj = DateTime::createFromFormat('d.m.Y', $query['dateTo']);
            if ($date_obj) {
                $filters[] = ['dateFrom', '<=', $date_obj->format('Y-m-d')];
            }
        }

        if ($query['carType'] != null) {
            if ($is_cargo) {
                $filters[] = ['t_cargo.transportTypeID', '=', $query['carType']];
            }
            else {
                $filters[] = ['t_user_car_type.ID', '=', $query['carType']];
            }
        }

        // фільтри по документам
        if ($query['docTIR'] != null) {
            $filters[] = ['docTIR', '=', 'yes'];
        }
        if ($query['docCMR'] != null) {
            $filters[] = ['docCMR', '=', 'yes'];
        }
        if ($query['docT1'] != null) {
            $filters[] = ['docT1', '=', 'yes'];
        }
        if ($query['docSanPassport'] != null) {
            $filters[] = ['docSanPassport', '=', 'yes'];
        }
        if ($query['docSanBook'] != null) {
            $filters[] = ['docSanBook', '=', 'yes'];
        }

        // фільтри по загрузці
        if ($query['loadFromSide'] != null) {
            $filters[] = ['loadFromSide', '=', 'yes'];
        }
        if ($query['loadFromTop'] != null) {
            $filters[] = ['loadFromTop', '=', 'yes'];
        }
        if ($query['loadFromBehind'] != null) {
            $filters[] = ['loadFromBehind', '=', 'yes'];
        }
        if ($query['loadTent'] != null) {
            $filters[] = ['loadTent', '=', 'yes'];
        }

        // фільтри по умовам
        if ($query['condPlomb'] != null) {
            $filters[] = ['condPlomb', '=', 'yes'];
        }
        if ($query['condLoad'] != null) {
            $filters[] = ['condLoad', '=', 'yes'];
        }
        if ($query['condBelts'] != null) {
            $filters[] = ['condBelts', '=', 'yes'];
        }
        if ($query['condRemovableStands'] != null) {
            $filters[] = ['condRemovableStands', '=', 'yes'];
        }
        if ($query['condHardSide'] != null) {
            $filters[] = ['condHardSide', '=', 'yes'];
        }
        if ($query['condCollectableCargo'] != null) {
            $filters[] = ['condCollectableCargo', '=', 'yes'];
        }
        if ($query['condPalets'] != null) {
            $filters[] = ['condPalets', '>', $query['condPaletsValue']];
        }
        if ($query['condADR'] != null) {
            $filters[] = ['condADR', '=', $query['condADRValue']];
        }

        // лише перевізник
        if ($query['condOnlyTransport'] != null) {
            $filters[] = ['t_comp.typeID', '=', 2];
        }
        if ($query['pricePDV'] != null) {
            $filters[] = ['t_price.PDV', '=', 'yes'];
        }
        if ($query['priceOnLoad'] != null) {
            $filters[] = ['t_price.onLoad', '=', 'yes'];
        }
        if ($query['priceOnUnload'] != null) {
            $filters[] = ['t_price.onUnload', '=', 'yes'];
        }
        if ($query['pricePayForm'] != null) {
            $filters[] = ['t_price.paymentType', '=', $query['pricePayForm']];
        }
        if ($query['priceBefore'] != null) {
            $filters[] = ['t_price.onPrepay', '=', 'yes'];
        }

        if ($is_cargo) {
            // фільтри вантажу
            if ($query['weight'] != null) {
                $filters[] = [
                    ['t_cargo.weight', '<=', $query['weight']],
                    ['t_cargo.weight', 'IS', null]
                ];
            }
            if ($query['volume'] != null) {
                $filters[] = [
                    ['t_cargo.volume', '<=', $query['volume']],
                    ['t_cargo.volume', 'IS', null]
                ];
            }
            if ($query['sizeX'] != null) {
                $filters[] = [
                    ['t_cargo.sizeX', '<=', $query['sizeX']],
                    ['t_cargo.sizeX', 'IS', null]
                ];
            }
            if ($query['sizeY'] != null) {
                $filters[] = [
                    ['t_cargo.sizeY', '<=', $query['sizeY']],
                    ['t_cargo.sizeY', 'IS', null]
                ];
            }
            if ($query['sizeZ'] != null) {
                $filters[] = [
                    ['t_cargo.sizeZ', '<=', $query['sizeZ']],
                    ['t_cargo.sizeZ', 'IS', null]
                ];
            }
            // по ціні
            if ($query['priceValue'] != null) {
                $filters[] = ['t_price.value', '>=', $query['priceValue']];
            }
            if ($query['priceBeforeAmount'] != null) {
                $filters[] = ['t_price.advancedPayment', '>', $query['priceBeforeAmount']];
            }
            // по температурі
            if ($query['condTemperature'] != null) {
                $filters[] = ['condTemperature', '>=', $query['condTemperatureValue']];
            }
        }
        else {
            // фільтри транспорту
            if ($query['weight'] != null) {
                $filters[] = [
                    ['t_user_car.liftingCapacity', '>=', $query['weight']],
                    ['t_user_car.liftingCapacity', 'IS', null]
                ];
            }
            if ($query['volume'] != null) {
                $filters[] = [
                    ['t_user_car.volume', '>=', $query['volume']],
                    ['t_user_car.volume', 'IS', null]
                ];
            }
            if ($query['sizeX'] != null) {
                $filters[] = [
                    ['t_user_car.sizeX', '>=', $query['SizeX']],
                    ['t_user_car.sizeX', 'IS', null]
                ];
            }
            if ($query['sizeY'] != null) {
                $filters[] = [
                    ['t_user_car.sizeY', '>=', $query['SizeY']],
                    ['t_user_car.sizeY', 'IS', null]
                ];
            }
            if ($query['sizeZ'] != null) {
                $filters[] = [
                    ['t_user_car.sizeZ', '>=', $query['SizeZ']],
                    ['t_user_car.sizeZ', 'IS', null]
                ];
            }
            // по ціні
            if ($query['priceValue'] != null) {
                $filters[] = ['t_price.value', '<=', $query['priceValue']];
            }
            if ($query['priceBeforeAmount'] != null) {
                $filters[] = ['t_price.advancedPayment', '<', $query['priceBeforeAmount']];
            }
            // по температурі
            if ($query['condTemperature'] != null) {
                $filters[] = ['condTemperature', '<=', $query['condTemperatureValue']];
                $filters[] = ['condTemperatureTo', '>=', $query['condTemperatureValue']];
            }
        }

        return $filters;
    }

    public static function getFiltersFromQuery($query) {
        $filter_form = [];

        $filter_form['dateTo']   = Arr::get($query, 'date_to');
        $filter_form['dateFrom'] = Arr::get($query, 'date_from');
        $filter_form['carType']  = Arr::get($query, 'car_type');

        $filter_form['load_geo']    = json_decode(Arr::get($query, 'load_geo'), true);
        $filter_form['load_radius'] = Arr::get($query, 'load_radius');

        $filter_form['unload_geo']    = json_decode(Arr::get($query, 'unload_geo'), true);
        $filter_form['unload_radius'] = Arr::get($query, 'unload_radius');

        $filter_form['weight'] = Arr::get($query, 'weight');
        $filter_form['volume'] = Arr::get($query, 'volume');
        $filter_form['sizeX']  = Arr::get($query, 'sizeX');
        $filter_form['sizeY']  = Arr::get($query, 'sizeY');
        $filter_form['sizeZ']  = Arr::get($query, 'sizeZ');

        $filter_form['docTIR']         = Arr::get($query, 'doc_TIR');
        $filter_form['docCMR']         = Arr::get($query, 'doc_CMR');
        $filter_form['docT1']          = Arr::get($query, 'doc_T1');
        $filter_form['docSanPassport'] = Arr::get($query, 'doc_sanpassport');
        $filter_form['docSanBook']     = Arr::get($query, 'doc_sanbook');

        $filter_form['loadFromSide']   = Arr::get($query, 'load_from_side');
        $filter_form['loadFromTop']    = Arr::get($query, 'load_from_top');
        $filter_form['loadFromBehind'] = Arr::get($query, 'load_from_behind');
        $filter_form['loadTent']       = Arr::get($query, 'load_tent');

        $filter_form['condPlomb']            = Arr::get($query, 'cond_plomb');
        $filter_form['condLoad']             = Arr::get($query, 'cond_load');
        $filter_form['condBelts']            = Arr::get($query, 'cond_stripes');
        $filter_form['condRemovableStands']  = Arr::get($query, 'cond_rem_stands');
        $filter_form['condHardSide']         = Arr::get($query, 'cond_bort');
        $filter_form['condCollectableCargo'] = Arr::get($query, 'cond_collectable');
        $filter_form['condTemperature']      = Arr::get($query, 'cond_t');
        $filter_form['condTemperatureValue'] = Arr::get($query, 'cond_t_value');
        $filter_form['condPalets']           = Arr::get($query, 'cond_pallets');
        $filter_form['condPaletsValue']      = Arr::get($query, 'cond_pallets_value');
        $filter_form['condADR']              = Arr::get($query, 'cond_ADR');
        $filter_form['condADRValue']         = Arr::get($query, 'cond_ADR_value');
        $filter_form['condOnlyTransport']    = Arr::get($query, 'cond_only_transport');

        $filter_form['priceValue']        = Arr::get($query, 'price_value');
        $filter_form['priceCurrency']     = Arr::get($query, 'price_currency');
        $filter_form['priceBefore']       = Arr::get($query, 'price_before');
        $filter_form['priceBeforeAmount'] = Arr::get($query, 'price_before_amount');

        $filter_form['pricePDV']      = Arr::get($query, 'price_PDV');
        $filter_form['priceOnLoad']   = Arr::get($query, 'price_on_load');
        $filter_form['priceOnUnload'] = Arr::get($query, 'price_on_unload');
        $filter_form['pricePayForm']  = Arr::get($query, 'pay_form');

        return $filter_form;
    }

    public function action_transport() {
        $template_updates = self::getActiveTemplatesUpdates('transport', $this->user->ID, $this->locale->googleLang);
        View::set_global('template_updates', $template_updates);

        View::set_global('last_transport_update_time', Session::instance()->get('last_transport_update_time'));
        Session::instance()->delete('last_transport_update_time');

        if (!is_null($this->request->query('t', null)) && $this->authorized) {
            $template = ORM::factory('Frontend_Filter_Template', [
                'ID' => $this->request->query('t'),
                'userID' => $this->user->ID,
                'type' => 'transport'
            ]);

            if ($template->loaded()) {
                if ($template->active == 'yes') {
                    Session::instance()->set('last_transport_update_time', $template->lastUpdateTime);
                }

                $template->lastUpdateTime = DB::expr("NOW()");
                $template->save();

                HTTP::redirect(Route::url('frontend_site_request_transport_list', $this->getRequestParams()) . '?' . $template->query);
            }
            else {
                HTTP::redirect(Route::url('frontend_site_request_transport_list', $this->getRequestParams()));
            }
        }

        $page_size = 20;

        // отримуємо номер сторінки
        $page_number = $this->request->param('page', null);
//        if (($page_number == 1) and (Route::name($this->request->route()) == 'frontend_site_request_transport_list')) {
//            HTTP::redirect(Route::url('frontend_site_request_transport_list_first_page', ['lang' => $this->language]) . URL::query(), 301);
//        }
        if ($page_number === null) {
            $page_number = 1;
        }

        $this->addLocalScript('page/request/transport');

        $this->l10n = [
            'title' => __db('SEO_transport_title')
        ];
        $this->seo['title_for_header'] = __db('transport.page.list.title');
        $this->seo['description'] = __db('SEO_transport_description');
        $this->seo['content_text'] = __db('SEO_transport_content');

        $query = $this->request->query();
        if ((Arr::get($query, 'load_geo', '') == '')) {
            $query['load'] = '';
        }
        if ((Arr::get($query, 'unload_geo', '') == '')) {
            $query['unload'] = '';
        }
        if ((Arr::get($query, 'load_geo', '') == '') || (Arr::get($query, 'load_radius', 0) < 0)) {
            $query['load_radius'] = '';
        }
        if ((Arr::get($query, 'unload_geo', '') == '') || (Arr::get($query, 'unload_radius', 0) < 0)) {
            $query['unload_radius'] = '';
        }

        // агрегація отриманих даних запиту через GET
        $filter_form = $this->getFiltersFromQuery($query);

        $filters   = self::prepareFilters($this->locale->googleLang, $filter_form, false);
        $filters[] = [
            ['t_trans.status', '=', 'active.have-no-proposition'],
            ['t_trans.status', '=', 'active.have-not-confirmed']
        ];

        $filters_for_vip = array_merge($filters, [['isVip', '=', 'yes']]);
        $filters[]       = ['isVip', '=', 'no'];

        $transport_count = Model_Frontend_Transport::query_getTransportCount($filters);

        // формуємо блок пагінації для списку заявок
        $pagination_manager = $this->preparePaginationBlock('pagination_transport', 'default', Route::get('frontend_site_request_transport_list'), $transport_count, $page_number, $page_size);
        if ($page_number > $pagination_manager->getPageCount()) {
            $page_number = 1;
        }

        $transport_requests = array_map('Controller_Frontend_Requests::processRequest', $this->getTransportListWithFilters($filters, $page_number, $page_size));
        $transport_vip      = array_map('Controller_Frontend_Requests::processRequest', $this->getTransportListWithFilters($filters_for_vip, 1, 10));

        $all_requests_count = count($transport_requests) + count($transport_vip);
        // якщо знайшли заявки
//        if ($pagination_manager->getPageCount() > 0) {
        if ($all_requests_count > 0) {
            $this->blocks['transports'] = View::factory('frontend/content/requests/transport/list/results', compact('transport_requests', 'transport_vip'));
        }
        else { // інакше виводим повідомлення про відсутність заявок
            $back_link_url = Route::url('frontend_site_request_transport_list', ['lang' => $this->language]);

            $this->blocks['transports'] = View::factory('frontend/content/requests/transport/list/not_found', compact('back_link_url'));
        }

        // отримуємо дані з БД для селектів форми
        $car_types = ORM::factory('Frontend_User_Car_Type')
                        ->find_all();

        $sticker_car_type_value = '';
        if (($q_car_id = $this->request->query('car_type')) != null) {
            $q_car                  = ORM::factory('Frontend_User_Car_Type', $q_car_id);
            $sticker_car_type_value = __db($q_car->localeName);
        }

        $sticker_config                      = Kohana::$config->load('frontend/requests/filter-stickers.transport');
        $sticker_config['car_type']['value'] = $sticker_car_type_value;

        $sticker_manager = new Frontend_Stickers_Manager((array)$sticker_config);
        $stickers        = $sticker_manager->get($query);

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->seo['title_for_header'], 'frontend_site_request_transport_list_first_page', $this->getRequestParams(['page' => null])));

        // результати пошуку
        if (count($this->request->query()) > 0) {
            if ($transport_count > 0) {
                $this->addNotification('success', 'За вашим запитом успішно знайдено ' . $transport_count . ' заявок');
            }
        }

        $templates               = View::factory('frontend/content/requests/templates/notifier');
        $filter_form             = View::factory('frontend/content/requests/filter-form', compact('query', 'stickers', 'car_types'));
        $this->blocks['content'] = View::factory('frontend/content/requests/transport', compact('filter_form', 'templates'));
    }

    public function action_cargo() {
        $template_updates = self::getActiveTemplatesUpdates('cargo', $this->user->ID, $this->locale->googleLang);
        View::set_global('template_updates', $template_updates);

        View::set_global('last_cargo_update_time', Session::instance()->get('last_cargo_update_time'));
        Session::instance()->delete('last_cargo_update_time');

        if (!is_null($this->request->query('t', null)) && $this->authorized) {
            $template = ORM::factory('Frontend_Filter_Template', [
                'ID' => $this->request->query('t'),
                'userID' => $this->user->ID,
                'type' => 'cargo'
            ]);

            if ($template->loaded()) {
                if ($template->active == 'yes') {
                    Session::instance()->set('last_cargo_update_time', $template->lastUpdateTime);
                }

                $template->lastUpdateTime = DB::expr("NOW()");
                $template->save();

                HTTP::redirect(Route::url('frontend_site_request_cargo_list', $this->getRequestParams()) . '?' . $template->query);
            }
            else {
                HTTP::redirect(Route::url('frontend_site_request_cargo_list', $this->getRequestParams()));
            }
        }

        $page_size = 20;

        // отримуємо номер сторінки
        $page_number = $this->request->param('page', null);
        if ($page_number === null) {
            $page_number = 1;
        }

        $this->addLocalScript('page/request/cargo');

        $this->l10n = [
            'title' => __db('SEO_cargo_title')
        ];
        $this->seo['title_for_header'] = __db('cargo.page.list.title');
        $this->seo['description'] = __db('SEO_cargo_description');
        $this->seo['content_text'] = __db('SEO_cargo_content');

        $query = $this->request->query();
        if ((Arr::get($query, 'load_geo', '') == '')) {
            $query['load'] = '';
        }
        if ((Arr::get($query, 'unload_geo', '') == '')) {
            $query['unload'] = '';
        }
        if ((Arr::get($query, 'load_geo', '') == '') || (Arr::get($query, 'load_radius', 0) < 1)) {
            $query['load_radius'] = '';
        }
        if ((Arr::get($query, 'unload_geo', '') == '') || (Arr::get($query, 'unload_radius', 0) < 1)) {
            $query['unload_radius'] = '';
        }

        // агрегація отриманих даних запиту через GET
        $filter_form = $this->getFiltersFromQuery($query);

        $filters   = self::prepareFilters($this->locale->googleLang, $filter_form, true);
        $filters[] = [
            ['t_cargo.status', '=', 'active.have-no-proposition'],
            ['t_cargo.status', '=', 'active.have-not-confirmed']
        ];

        $filters_for_vip = array_merge($filters, [['isVip', '=', 'yes']]);
        $filters[]       = ['isVip', '=', 'no'];

        $cargo_count = Model_Frontend_Cargo::query_getCargoCount($filters);

        // формуємо блок пагінації для списку заявок
        $pagination_manager = $this->preparePaginationBlock('pagination_cargo', 'default', Route::get('frontend_site_request_cargo_list'), $cargo_count, $page_number, $page_size);
        if ($page_number > $pagination_manager->getPageCount()) {
            $page_number = 1;
        }

        $cargo_requests = array_map('Controller_Frontend_Requests::processRequest', $this->getCargoListFromQuery($filters, $page_number, $page_size));
        $cargo_vip      = array_map('Controller_Frontend_Requests::processRequest', $this->getCargoListFromQuery($filters_for_vip, 1, 10));

        $all_requests_count = count($cargo_requests) + count($cargo_vip);
        // якщо знайшли заявки
//        if ($pagination_manager->getPageCount() > 0) {
        if ($all_requests_count > 0) {
            $this->blocks['cargo'] = View::factory('frontend/content/requests/cargo/list/results', compact('cargo_requests', 'cargo_vip'));
        }
        else { // інакше виводим повідомлення про відсутність заявок
            $back_link_url = Route::url('frontend_site_request_transport_list', ['lang' => $this->language]);

            $this->blocks['cargo'] = View::factory('frontend/content/requests/cargo/list/not_found', compact('back_link_url'));
        }

        // отримуємо дані з БД для селектів форми
        $car_types = ORM::factory('Frontend_User_Car_Type')
                        ->find_all();

        $sticker_car_type_value = '';
        if (($q_car_id = $this->request->query('car_type')) != null) {
            $q_car                  = ORM::factory('Frontend_User_Car_Type', $q_car_id);
            $sticker_car_type_value = __db($q_car->localeName);
        }

        $sticker_config                      = Kohana::$config->load('frontend/requests/filter-stickers.cargo');
        $sticker_config['car_type']['value'] = $sticker_car_type_value;

        $sticker_manager = new Frontend_Stickers_Manager((array)$sticker_config);
        $stickers        = $sticker_manager->get($query);

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->seo['title_for_header'], 'frontend_site_request_cargo_list_first_page', $this->getRequestParams(['page' => null])));

        // результати пошуку
        if (count($this->request->query()) > 0) {
            if ($cargo_count > 0) {
                $this->addNotification('success', 'За вашим запитом успішно знайдено ' . $cargo_count . ' заявок');
            }
        }

        $templates               = View::factory('frontend/content/requests/templates/notifier');
        $filter_form             = View::factory('frontend/content/requests/filter-form', compact('query', 'stickers', 'car_types'));
        $this->blocks['content'] = View::factory('frontend/content/requests/cargo', compact('filter_form', 'templates'));
    }

    public function getTenderListFromQuery($filters, $page_number, $page_size) {
        // вибірка списку вантажних заявок
        $requests = Model_Frontend_Tenders::query_getTenders($this->locale->ID, false, $page_size, $page_size * ($page_number - 1), $filters);

        $result = [];
        foreach ($requests as $request) {
            $request['datetimeFromTender'] = date('d.m.y G:i:s', strtotime($request['datetimeFromTender']));
            $request['datetimeToTender']   = date('d.m.y G:i:s', strtotime($request['datetimeToTender']));

            $request['created']      = date('d.m.y G:i:s', strtotime($request['created']));
            $request['dateFromLoad'] = date('d.m', strtotime($request['dateFromLoad']));

            if ($request['dateToLoad'] != null) {
                $request['dateToLoad'] = date('d.m', strtotime($request['dateToLoad']));
            }

            $result[] = $request;
        }

        return $result;
    }

    public function action_tender() {
        $page_size = 20;

        // отримуємо номер сторінки
        $page_number = $this->request->param('page', null);
//        if (($page_number == 1) and (Route::name($this->request->route()) == 'frontend_site_request_tender_list')) {
//            HTTP::redirect(Route::url('frontend_site_request_tender_list_first_page', ['lang' => $this->language]), 301);
//        }
        if ($page_number === null) {
            $page_number = 1;
        }

        $this->addLocalScript('page/request/tender');

        $this->l10n = [
            'title' => __db('SEO_tenders_title')
        ];
        $this->seo['title_for_header'] = __db('tender.page.list.title');
        $this->seo['description'] = __db('SEO_tenders_description');
        $this->seo['content_text'] = __db('SEO_tenders_content');

        $filters      = [];
        $tender_count = Model_Frontend_Tenders::query_getCargoCount($filters);

        // формуємо блок пагінації для списку заявок
        $pagination_manager = $this->preparePaginationBlock('pagination_tender', 'default', Route::get('frontend_site_request_tender_list'), $tender_count, $page_number, $page_size);
        if ($page_number > $pagination_manager->getPageCount()) {
            $page_number = 1;
        }

        $tender_requests = $this->getTenderListFromQuery($filters, $page_number, $page_size);


        // якщо знайшли заявки
        if ($pagination_manager->getPageCount() > 0) {
            $this->blocks['tenders'] = View::factory('frontend/content/requests/tender/list/results', compact('tender_requests'));
        }
        else { // інакше виводим повідомлення про відсутність заявок
            $back_link_url = Route::url('frontend_site_request_tender_list_first_page', ['lang' => $this->language]);

            $this->blocks['tenders'] = View::factory('frontend/content/requests/tender/list/not_found', compact('back_link_url'));
        }

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->seo['title_for_header'], 'frontend_site_request_tender_list_first_page', $this->getRequestParams(['page' => null])));

        $this->blocks['content'] = View::factory('frontend/content/requests/tender');
    }

    public function action_tender_page() {
        $this->l10n = [
            'title' => __db('tender.page.title')
        ];

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(false, __db('tender.page.list.title'), 'frontend_site_request_tender_list_first_page', $this->request->param()));
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_request_tender_page', $this->request->param()));

        $this->blocks['content'] = View::factory('frontend/content/requests/tender/page');
    }


}