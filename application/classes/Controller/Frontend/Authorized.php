<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Frontend_Authorized
 */
class Controller_Frontend_Authorized extends Controller_Frontend_Base {

    /** @var bool */
    public $authorized = false;

    /** @var Model_Frontend_User */
    public $user = null;


    public function before() {
        parent::before();

        $user_ID = Frontend_Auth::userID();
        if ($user_ID === false) {
            $this->authorized = false;
            $this->blocks['header/userbar'] = 'frontend/blocks/userbar/no-auth';

            $this->user = ORM::factory('Frontend_User');
        }
        else {
            $this->user = ORM::factory('Frontend_User', $user_ID);

            if (! $this->user->loaded()) {
                Frontend_Auth::logoutUser();
                HTTP::redirect( Route::url('frontend_index', $this->getRequestParams()) );
            }

            if ($this->user->deleted == 'y') {
                Frontend_Auth::logoutUser();
                $this->addStoredNotification('error', 'Ваш профіль було видалено управляючим вашої компанії!');
                HTTP::redirect( Route::url('frontend_index', $this->getRequestParams()) );
            }

            $this->authorized = true;
            $this->blocks['header/userbar'] = 'frontend/blocks/userbar/logged-in';

            $this->links['profile_index'] = Route::url('frontend_site_profile', [
                'lang' => $this->request->param('lang')
            ]);
            $this->links['profile_messages'] = Route::url('frontend_site_profile', [
                'lang' => $this->request->param('lang'),
                'profile_tab' => 'messages'
            ]);
            $this->links['profile_requests'] = Route::url('frontend_site_profile', [
                'lang' => $this->request->param('lang'),
                'profile_tab' => 'requests'
            ]);

            Frontend_Auth::prolongUserSession($this->user->ID);
        }
    }

    public function action_login() {
        $this->auto_render = false;

        $lang = Cookie::get('stored_lang', null);
        $locale = ORM::factory('Frontend_Locale')
             ->where('uri', '=', $lang)
             ->and_where('status', '=', 'active')
             ->find();

        if ($locale->loaded()) {
            I18n::loadDB($locale->ID); // ініціалізація модуля локалізації Богдана
        }

        $email = $this->request->post('login-email', null);
        $password = $this->request->post('login-password', null);
        $remember_me = ($this->request->post('remember-me', null) != null);

        $user = ORM::factory('Frontend_User')
            ->where('email', '=', $email)
            ->and_where('hash', '=', md5($password) )
            ->find();

        if ($user->loaded()) {
            if ($user->deleted == 'y') {
                $this->addStoredNotification('error', __db('notification.auth.deleted'));
            }//'Ваш профіль було видалено управляючим вашої компанії!'

            // перевірка чи був активований користувач через email
            if ($user->emailApproved != 'approved') {
                $this->addStoredNotification('warning', __db('notification.auth.confirm_email'), true);

                Model_Frontend_Registration_Link::query_deleteOutdatedLinks();

                // вставляємо нове тимчасове посилання для підтвердження
                $temp_link = ORM::factory('Frontend_Registration_Link');
                /** @var $temp_link Model_Frontend_Registration_Link */
                $temp_link->user = $user;
                $temp_link
                    ->initRegistrationLink('confirm_email')
                    ->save();

                Frontend_Helper_Mail::send_mail($user->email, __db('email.confirm_registration.title'), 'confirm-registration', [
                    'link' => 'http://tirscan.com/' . Route::url('frontend_site_company_registration_confirmation',
                            [
                                'code' => $temp_link->temp
                            ]),
                    'username' => $user->locales->where('localeID', '=', $user->primaryLocaleID)->find()->name,
                    'email' => $user->email
                ]);
            }
            else {
                // інакше авторизуєм його
                Frontend_Auth::authUser($user->ID, $remember_me);
            }
        }
        else {
            Session::instance()->set('show_auth_modal', true);
            Session::instance()->set('login_email', $email);

            $this->addStoredNotification('error', __db('notification.auth.fail'));
        }

        if ( isset($_SERVER['HTTP_REFERER']) )
            HTTP::redirect( $_SERVER['HTTP_REFERER'] );
        else
            throw new HTTP_Exception_404();
    }

    public function action_forgot_pass(){
        $email = $this->request->post('email-forgot', null);

        $user_identify = ORM::factory('Frontend_User')
            ->where('email', '=', $email)
            ->find();

        // Generate password
        $chars = "qazxswedcvfrtgbnhyujmkiolpQAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max = 8;
        $size = StrLen($chars)-1;
        $password = null;

        while($max--) {
            $password .= $chars[rand(0, $size)];
        }

        if($user_identify->loaded() && !empty($user_identify->email)){

            Model_Frontend_Registration_Link::query_deleteOutdatedLinks();

            // вставляємо нове тимчасове посилання для підтвердження
            $temp_link = ORM::factory('Frontend_Registration_Link'); /** @var $temp_link Model_Frontend_Registration_Link */
            $temp_link->user = $user_identify;
            $temp_link
                ->initRegistrationLink('confirm_password', $password)
                ->save();

            Frontend_Helper_Mail::send_mail($user_identify->email, __db('email.confirm_registration.title'),
                'confirm-forgot-pass', [
                    'password' => $password,
                    'link' => 'http://tirscan.com' . Route::url('frontend_site_company_password_confirmation',
                            [
                                'code' => $temp_link->temp,
                                'lang' => Cookie::get('stored_lang', 'ua')
                            ]),
                    'username' => $user_identify->locales->where('localeID', '=', $user_identify->primaryLocaleID)->find()->name,
                    'email' => $user_identify->email
                ]
            );

            $this->addStoredNotification('success', 'Лист відправлено на пошту');
        } else {
            Session::instance()->set('show_forgot_modal', true);
            $this->addStoredNotification('error', __db('notification.auth.fail'));
        }

        if ( isset($_SERVER['HTTP_REFERER']) )
            HTTP::redirect( $_SERVER['HTTP_REFERER'] );
        else
            throw new HTTP_Exception_404();
    }

    public function requestSocialData($token) {
        $request_url_format = Kohana::$config->load('frontend/site.uLogin.request_url', null);
        if (is_null($request_url_format)) {
            $this->addStoredNotification('error', __db('notification.auth.social_fail'));

            HTTP::redirect( Route::url('frontend_index') );
        }
        else {
            $request_url = sprintf($request_url_format, $token, $_SERVER['HTTP_HOST']);
            $data = Request::factory($request_url)
                ->execute()
                ->body();

            return json_decode($data, true);
        }
    }

    public function action_social() {
        $this->auto_render = false;

        $token = $this->request->post('token', null);
        if (is_null($token))
            HTTP::redirect( Route::url('frontend_index') );
        else {
            $social_data = $this->requestSocialData($token);

            // шукаєм соцпрофіль у БД
            $social = ORM::factory('Frontend_User_Social')
                ->where('socialUID', '=', $social_data['uid'])
                ->find();

            if ($social->loaded()) { // якщо знайшли
                Frontend_Auth::authUser($social->user->ID); // то авторизуєм

                if ( isset($_SERVER['HTTP_REFERER']) ) // і редиректим назад
                    HTTP::redirect( $_SERVER['HTTP_REFERER'] );
                else
                    HTTP::redirect( Route::url('frontend_index') );
            }
            else { // інакше соцпрофіль не закріплений за жодною компанією

                Session::instance()->set('social_auth_data', $social_data); // зберігаєм дані соцпрофілю

                $params = [];
                $lang = Cookie::get('stored_lang', null); // шукаємо збережену мову у cookie
                if (! is_null($lang))
                    $params = ['lang' => $lang]; // використовуєм мову користувача, якщо знайшли

                HTTP::redirect( Route::url( 'frontend_site_company_registration', $params) ); // пропонуєм реєстрацію
            }
        }
    }

    public function action_logout() {
        $this->auto_render = false;

        Frontend_Auth::logoutUser();

        if ( isset($_SERVER['HTTP_REFERER']) )
            HTTP::redirect($_SERVER['HTTP_REFERER']);
        else
            throw new HTTP_Exception_404();
    }

}