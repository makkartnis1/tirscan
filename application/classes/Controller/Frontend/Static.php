<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Static extends Controller_Frontend_Authorized {

    public function before() {
        $this->blocks['header'] = 'frontend/blocks/header_page';

        parent::before();
    }

    public function action_index() {
        $uri         = $this->request->param('uri');
        $static_page = ORM::factory('Frontend_Static_Page', [
            'localeID' => $this->locale->ID,
            'uri' => $uri
        ]);

        if (!$static_page->loaded()) {
            throw new HTTP_Exception_404;
        }

        $this->l10n = ['title' => htmlspecialchars($static_page->title)];

        $this->seo['keywords']    = htmlspecialchars($static_page->keywords);
        $this->seo['description'] = htmlspecialchars($static_page->description);

        // формуємо breadcrumbs
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_static_page', $this->getRequestParams()));

        $this->blocks['content'] = View::factory('frontend/content/static', [
            'title' => $this->l10n['title'],
            'content' => $static_page->content
        ]);
    }

}
