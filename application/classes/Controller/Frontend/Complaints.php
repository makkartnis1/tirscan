<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Complaints extends Controller_Frontend_HMVC {

    public function action_index(){

        self::_addJS('/assets/js/frontend/page/company/page.js');

        $this->response->body(
            View::factory('frontend/content/companies/page')
        );

    }

}