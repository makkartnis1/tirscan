<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_File extends Controller {

    /**
     * @var Frontend_Uploader
     */
    protected $file;

    public function action_download(){

        $this->file = Frontend_Uploader::factory($this->request->param('location'), $this->request->param('id'), $this->request->param('rootDir'));

        $file = $this->file->get_file_location(
            $this->request->param('token'),
            $this->request->param('file'),
            Frontend_Uploader::TOKEN_READ
        );

        if($file === false)
            throw HTTP_Exception::factory(403);

        $this->response->send_file($file);

    }

    public function action_preview(){

        $this->file = Frontend_Uploader::factory($this->request->param('location'), $this->request->param('id'), $this->request->param('rootDir'));

        $file = $this->file->get_file_location(
            $this->request->param('token'),
            $this->request->param('file'),
            Frontend_Uploader::TOKEN_READ
        );


        if($file === false)
            throw HTTP_Exception::factory(403);

        $this->response->preview_file($file);

    }

    public function action_delete(){

        if($this->request->method() !== Request::POST)
            return;

        $this->file = Frontend_Uploader::factory(
            $this->request->post('location'),
            $this->request->post('id'),
            $this->request->post('dir'));

        $file = $this->file->get_file_location(
            $this->request->post('token'),
            $this->request->post('file'),
            Frontend_Uploader::TOKEN_DELETE
        );

        if($file === false){

            $delete = false;

            //throw HTTP_Exception::factory(403);
        }else{

            $delete = unlink($file);

        }




        $widget = $this->file->widget('',''); // цю строчку не переносити, щоб вона була раніше віджета - так як, саме на цій строчці виконується читнання директорії
        $widget->params_apply($this->request->post('init'));

        list($callback, $widgetUID) = explode('|', $this->request->post('callback'));


        $response = json_encode([
            'html'      => $widget->render(),
            'delete'    => $delete,
        ]);

        $this->response->body("
            <script>
                window.onload = function() {
                    parent.{$callback}('{$widgetUID}', {$response} );
                };
            </script>
        ");


    }

    public function action_upload(){

        if($this->request->method() !== Request::POST)
            return;

        $this->file = Frontend_Uploader::factory(
            $this->request->post('location'),
            $this->request->post('id'),
            $this->request->post('dir'));


        list($ok, $messages) = $this->file->save(
            
            $this->request->post('token'),
            $this->request->post('config'),
            $this->request->post('config-token')
            
        );

        $widget = $this->file->widget('',''); // цю строчку не переносити,
        // щоб вона була раніше віджета - так як, саме на цій строчці виконується читнання директорії
        $widget->params_apply($this->request->post('init'));

        list($callback, $widgetUID) = explode('|', $this->request->post('callback'));


        $response = json_encode([
            'html'      => $widget->render(),
            'upload'    => $ok,
            'messages'  => $messages,
            'files'     => $this->file->scanPath(),
        ]);

        $this->response->body("
            <script>
                window.onload = function() {
                    parent.{$callback}('{$widgetUID}', {$response} );
                };
            </script>
        ");
        

    }

}