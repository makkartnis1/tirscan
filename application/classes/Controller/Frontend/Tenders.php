<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Tenders extends Controller_Frontend_Authorized {

    public function before() {
        parent::before();

        $this->styles[] = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css';
        $this->scripts[] = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js';
        $this->scripts[] = '/assets/js/moment-with-locales.min.js';
        $this->scripts[] = '/assets/js/countdown.min.js';
        $this->scripts[] = '/assets/js/moment-countdown.min.js';
        $this->blocks['header'] = 'frontend/blocks/header_page';
    }


    public function action_edit() {

        //якщо користувач не авторизований до тендера не допускаємо
        if (!$this->user->ID)
            throw new HTTP_Exception_403(__db('tenders.error.access.edit_only_authorized'));

        $user_id = $this->user->ID;
        $tender_id = $this->request->param('id');
        $m_tenders = new Model_Frontend_Tenders();
        $tender = $m_tenders->get_one($tender_id,$user_id);
        if (!$tender)
            throw new HTTP_Exception_404(__db('tenders.error.access.not_isset'));

        //якщо користувач не є власником тендера, то до редагування не допускаємо
        if ($user_id != $tender['info']['userID'])
            throw new HTTP_Exception_403(__db('tenders.error.access.not_owner'));


        if ($tender['info']['status'] != 'inactive')
            throw new HTTP_Exception_403(__db('tenders.error.access.no_edit_active'));

        $tender_info = $m_tenders->filter_for_edit($tender);

        $init_js_view = '';
        $errors = [];

        if ($this->request->method() == Kohana_HTTP_Request::POST)
        {

            $data = $this->request->post();

            $load_place = $data['load_place'];
            $unload_place = $data['unload_place'];

            foreach($data['load_place'] as $k => $v)
            {
                if (!$v) unset($load_place[$k]);
            }

            foreach($data['unload_place'] as $k => $v)
            {
                if (!$v) unset($unload_place[$k]);
            }

            $data['load_place'] = $load_place;
            $data['unload_place'] = $unload_place;

            $data['loads'] = [];
            $data['unloads'] = [];

            $m_cms_geo = new Model_Cms_Geo();

            foreach($load_place as $place)
            {
                $data['loads'][] = [
                    'name'  => $m_cms_geo->getNameByIDs(explode(',',$place)),
                    'value' => $place
                ];
            }

            foreach($unload_place as $place)
            {
                $data['unloads'][] = [
                    'name'  => $m_cms_geo->getNameByIDs(explode(',',$place)),
                    'value' => $place
                ];
            }

            $m_tenders = new Model_Frontend_Tenders();

            $errors = $m_tenders->validate($data);
            if (empty($errors))
            {
                $data = $m_tenders->filter($data);

                $update_status = $m_tenders->update($data,$tender_id);

                if (!$update_status)
                {
                    $this->addNotification('error', __db('tenders.error.publish.fields'));
                    $tender_info = $data;
                }
                else
                {
                    $this->addNotification('success', __db('tenders.success.publish.message'));
                    $tender = $m_tenders->get_one($tender_id,$user_id);
                    $tender_info = $m_tenders->filter_for_edit($tender);
                }

            }
            else
            {
                $this->addNotification('error', __db('tenders.error.publish.fields'));
                $tender_info = $data;
            }


        }

        $init_js_view = View::factory('frontend/js/tender_add_init', array(
            'post' => $tender_info,
            'errors' => $errors
        ));

        $this->addLocalScript('page/request/add_tender');

        $this->l10n = [
            'title' => __db( 'tenders.page.edit.title')
        ];

        // отримуємо інформацію для форми з БД
        $currencies = ORM::factory('Frontend_Currency')
            ->find_all();
        $car_types = ORM::factory('Frontend_User_Car_Type')
            ->find_all();
        if (! $this->authorized)
            $phone_codes = ORM::factory('Frontend_Phonecodes')
                ->find_all();


        // формуємо breadcrumbs
        $this->breadcrumbs->addLink( new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_tender_edit', $this->request->param()) );

        $post = $this->request->post();

        $this->blocks['content'] = View::factory('frontend/content/requests/mixed-tender', compact('car_types', 'phone_codes', 'currencies', 'post', 'init_js_view', 'errors'));
    }

    public function action_add() {

        //якщо користувач не авторизований до тендера не допускаємо
        if (!$this->user->ID)
            throw new HTTP_Exception_403(__db('tenders.error.access.add_only_authorized'));

        //якщо компанія є транспортною до тендера не допускаємо
        $company = DB::select('typeID')
            ->from('Frontend_Companies')
            ->where('ID','=',$this->user->companyID)
            ->execute()
            ->as_array();
        $company_type = $company[0]['typeID'];

        if ($company_type == 2)
            throw new HTTP_Exception_403(__db('tenders.error.access.add_no_transporter'));


        $init_js_view = '';
        $errors = [];
        $create_status = 0;

        if ($this->request->method() == Kohana_HTTP_Request::POST)
        {

            $data = $this->request->post();

            $load_place = $data['load_place'];
            $unload_place = $data['unload_place'];

            foreach($data['load_place'] as $k => $v)
            {
                if (!$v) unset($load_place[$k]);
            }

            foreach($data['unload_place'] as $k => $v)
            {
                if (!$v) unset($unload_place[$k]);
            }

            $data['load_place'] = $load_place;
            $data['unload_place'] = $unload_place;

            $data['loads'] = [];
            $data['unloads'] = [];

            $m_cms_geo = new Model_Cms_Geo();

            foreach($load_place as $place)
            {
                $data['loads'][] = [
                    'name'  => $m_cms_geo->getNameByIDs(explode(',',$place)),
                    'value' => $place
                ];
            }

            foreach($unload_place as $place)
            {
                $data['unloads'][] = [
                    'name'  => $m_cms_geo->getNameByIDs(explode(',',$place)),
                    'value' => $place
                ];
            }

            $m_tenders = new Model_Frontend_Tenders();

            $errors = $m_tenders->validate($data);
            if (empty($errors))
            {
                $data = $m_tenders->filter($data);

                $tender_id = $m_tenders->create($data,$this->user->ID);
                $data = [];

                if ($tender_id)
                {
                    HTTP::redirect( Route::url('frontend_site_tender', array('id'=>$tender_id)) );

                }
                else
                {
                    $this->addNotification('error', __db('tenders.error.publish.fields'));
                }

            }
            else
            {
                $this->addNotification('error', __db('tenders.error.publish.fields'));
            }

            $init_js_view = View::factory('frontend/js/tender_add_init', array(
                'post' => $data,
                'errors' => $errors
            ));
        }

        $this->addLocalScript('page/request/add_tender');

        $this->l10n = [
            'title' => __db( 'tenders.page.add.title')
        ];

        // отримуємо інформацію для форми з БД
        $currencies = ORM::factory('Frontend_Currency')
            ->find_all();
        $car_types = ORM::factory('Frontend_User_Car_Type')
            ->find_all();
        if (! $this->authorized)
            $phone_codes = ORM::factory('Frontend_Phonecodes')
                ->find_all();




        // формуємо breadcrumbs
        $this->breadcrumbs->addLink( new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_request_add_tender', $this->request->param()) );

        $post = $this->request->post();

        $title = $this->l10n['title'];

        $this->blocks['content'] = View::factory('frontend/content/requests/mixed-tender', compact('car_types', 'phone_codes', 'currencies', 'post', 'init_js_view', 'errors'));
    }

    public function action_one()
    {

        $user_id = $this->user->ID;
        $tender_id = $this->request->param('id');
        $m_tenders = new Model_Frontend_Tenders();
        $tender = $m_tenders->get_one($tender_id,$user_id);
        if (!$tender)
            throw new HTTP_Exception_404(__db('tenders.error.access.not_isset'));

        $conditions = [];
        //критерії участі в тендері

        //перевірка чи авторизований користувач
        if (!$user_id) $conditions['user'] = 1;


        //перевірка на участь у тендері тільки партнерів
        if ($user_id and $tender['info']['filterPartners'] == 'yes')
        {
            $partners = DB::select(DB::expr('count(ID) as c'))
                ->from('Frontend_Company_Partners')
                ->or_where_open()
                ->where('company1ID','=',$tender['company_id'])
                ->where('company2ID','=',$this->user->companyID)
                ->or_where_close()
                ->or_where_open()
                ->where('company2ID','=',$tender['company_id'])
                ->where('company1ID','=',$this->user->companyID)
                ->or_where_close()
                ->limit(1)
                ->execute()
                ->as_array();

            $count_partners = $partners[0]['c'];
            if ($count_partners == 0) $conditions['partner'] = 1;
        }

        //перевірка на термін діяльності компанії
        if ($tender['info']['filterCompanyYears'] and $user_id and $tender['info']['filterPartners'] == 'no')
        {
            $company_info = DB::select('foundationDate')
                ->from('Frontend_Companies')
                ->where('ID','=',$this->user->companyID)
                ->where('foundationDate','<',DB::expr('DATE_ADD(NOW(), INTERVAL - '.$tender['info']['filterCompanyYears'].' YEAR)'))
                ->execute()
                ->as_array();

            if (empty($company_info))
            {
                $conditions['company_time'] = 1;
            }
        }

        $company_info = DB::select()
            ->from('Frontend_Companies')
            ->where('ID','=',$this->user->companyID)
            ->execute()
            ->as_array();

        //перевірка на тип компанії: чи дозволено брати участь експедиторам
        if ($user_id and $tender['info']['filterPartners'] == 'no')
        {
            $filter_forwarder = $tender['info']['filterForwarder'];
            $company_type = $company_info[0]['typeID'];

            if ($filter_forwarder == 'yes')
            {
                if ($company_type == 3) $conditions['company_type'] = 1;
            }
            else
            {
                if ($company_type != 2) $conditions['company_type'] = 1;
            }
        }

        //перевірка на статус компанії: дозволено брати участь компаніям лише з підтвердженими платіжними реквізитами
        if ($user_id and $tender['info']['filterCompany'] == 'yes' and $tender['info']['filterPartners'] == 'no')
        {
            if ($company_info[0]['approvedByAdmin'] != 'yes') $conditions['company_status'] = 1;
        }


        $post = $this->request->post();
        if (isset($post['go_bet']))
        {
            $custom_bet = $post['custom_bet'];
            $hidden_bet_valid = $m_tenders->hidden_bet_valid($tender['info']['price'],$custom_bet,$user_id,$tender_id);
            if (!$hidden_bet_valid)
            {
                $res_hidden_bet = $m_tenders->hidden_bet($tender_id,$user_id,$custom_bet);
                if ($res_hidden_bet)
                {
                    $this->addNotification('success', __db('tenders.bet.success.message'));
                }
                else
                {
                    $this->addNotification('error', __db('tenders.bet.error.bet_ahead'));
                }
            }
            else
            {
                $this->addNotification('error', $hidden_bet_valid);
            }

            $tender = $m_tenders->get_one($tender_id,$user_id);
        }


        if (isset($post['go_auto_bet']))
        {
            $auto_bet = $post['auto_bet'];
            $auto_bet_res = $m_tenders->auto_bet($tender_id,$user_id,$auto_bet);

            if ($auto_bet_res)
            {
                $this->addNotification('error', $auto_bet_res);
            }
            else
            {
                $this->addNotification('success', 'Автоматична ставка успішно встановлена');
            }

            $tender = $m_tenders->get_one($tender_id,$user_id);

        }

        $this->breadcrumbs->addLink( new Frontend_Breadcrumbs_Link(true, __db('tender.page.list.title'), 'frontend_site_request_tender_list', $this->request->param()) );
        $this->breadcrumbs->addLink( new Frontend_Breadcrumbs_Link(true, __db('tender_page.tender').' №'.$tender_id, 'frontend_site_tender', $this->request->param()) );

        $this->blocks['content'] = View::factory('frontend/content/tenders/tender', compact('tender','user_id','conditions'));


    }

}