<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Fix_File_user extends Controller {
    
    public function action_avatar(){
        
        $file = Helper_User::NO_AVATAR;

        $this->response->preview_file($_SERVER['DOCUMENT_ROOT'].'/'.$file, [], [
            'headers'   => [
                'status'    => 404,
            ],

        ]);
        
    }
    

}