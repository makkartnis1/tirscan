<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Fix_File_Car extends controller {
    
    public function action_gallery(){
        
        $file = Model_Frontend_User_Car::NO_IMAGE;

        $this->response->preview_file($_SERVER['DOCUMENT_ROOT'].'/'.$file, [], [
            'headers'   => [
                'status'    => 404,
            ],

        ]);
        
    }
    

}