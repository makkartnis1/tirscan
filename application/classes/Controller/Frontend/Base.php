<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Frontend_Base
 */
class Controller_Frontend_Base extends Controller_Template {

    const LOAD_LOCALE_FROM_DB = 0;

    /** @var string */
    public $language;

    /** @var Model_Frontend_Locale */
    public $locale = null;

    /** @var Model_Frontend_Locale */
    public $locales;

    /** @var string|View базовий шаблон сторінки */
    public $template = 'frontend/templates/page';

    /** @var string шлях до локальних js-скриптів */
    public $local_script_path = 'assets/js/frontend';

    /** @var string[] */
    public $links = [];

    /** @var string[] */
    public $seo = [];

    /** @var Frontend_Breadcrumbs_Manager */
    public $breadcrumbs;

    /** @var array[] */
    public $notifications = [];

    /** @var array[] */
    public $errors = [];

    public $selectedMenuItem = '';

    public $aside_widgets = [
        /*
         
            $locale_const     => [
        
                'content'       => 'widget content';
            ]        
        
         */


    ];

    /** @var array блоки для передачі видам (можна редагувати у action) */
    public $blocks = [
        'header' => 'frontend/blocks/header',
        'header/common' => 'frontend/blocks/header/common',
        'footer' => 'frontend/blocks/footer',
        'js/init' => 'frontend/js/init'
    ];

    /** @var array масив js-скриптів сторінки для підключення (можна редагувати у action) */
    public $scripts = [
        '//ulogin.ru/js/ulogin.js',
        '/assets/js/handlebars.min-latest.js',
        '/assets/js/handlebars_extended.js',
        '/assets/js/jquery.easy-overlay.js',
        '/assets/js/form2json.js',
        '/assets/js/common-js.js'
    ];

    /** @var array масив css-стилів сторінки для підключення (можна редагувати у action) */
    public $styles = [

    ];

    /** @var array масив локалізації для сторінки (можна редагувати у action) */
    public $l10n = [
        'title' => 'TIRSCAN'
    ];

    public function addNotification($type, $message) {
        $this->notifications[] = [
            'type' => $type,
            'message' => $message
        ];
    }

    public function addStoredNotification($type, $message, $locale_key = false) {
        $buffer   = Session::instance()->get('stored_notifications', []);
        $buffer[] = [
            'type' => $type,
            'message' => $message,
            'locale_key' => $locale_key
        ];

        Session::instance()->set('stored_notifications', $buffer);
    }

    public function addErrors(array $errors, $group = 'common') {
        if (array_key_exists($group, $this->errors)) {
            $this->errors[$group] = array_merge($this->errors[$group], $errors);
        }
        else {
            $this->errors[$group] = $errors;
        }
    }

    public function hasError($field_name, $group = 'common') {
        if (array_key_exists($group, $this->errors)) {
            if (array_key_exists($field_name, $this->errors[$group])) {
                return true;
            }
        }

        return false;
    }

    public function classIfError($field_name, $group = 'common') {
        if ($this->hasError($field_name, $group)) {
            return ' errorForm';
        }
        else {
            return '';
        }
    }

    public function echoErrors($field_name, $group = 'common') {
        if ($this->hasError($field_name, $group)) {
            $error_params = [];
            foreach ($this->errors[$group][$field_name][1] as $key => $value)
                $error_params[':' . $key] = $value;
            $this->errors[$group][$field_name][1] = $error_params;

            $view = View::factory('frontend/error/form-input', ['errors' => $this->errors[$group][$field_name]])
                        ->render();
            echo $view;
        }
    }

    /**
     * Добавляет локальный скрипт к подключаемым js-скриптам на странице
     * (путь к js-файлу скрипта с этим именем генерируется автоматически)
     *
     * @param string $name имя скрипта
     */
    public function addLocalScript($name) {
        $this->scripts[] = "/{$this->local_script_path}/{$name}.js";
    }

    /**
     * Вивід блоку на сторінці
     *
     * @param string|View|mixed $block
     */
    public function echoBlock($block, array $view_defs = []) {
        if (array_key_exists($block, $this->blocks)) {
            if ($this->blocks[$block] instanceof View) // якщо файл виду, то рендерим його
            {
                echo $this->blocks[$block]->render();
            }
            elseif (is_string($this->blocks[$block])) // якщо це строка, то шукаєм по ній файл виду і рендерим його
            {
                echo View::factory($this->blocks[$block], $view_defs)->render();
            }
            else {
                echo $block;
            }
        } // інакше виводим "в лоб"
    }

    /**
     * @param $block_name string
     * @param $view string
     * @param $route Route
     * @param $objects_count int
     * @param $page_number int
     * @param $page_size int
     * @return Frontend_Pagination_Manager
     */
    public function preparePaginationBlock($block_name, $view, $route, $objects_count, $page_number, $page_size) {
        $group_length = (int)Kohana::$config->load('frontend/site.pagination.group_length');

        // отримуєм шаблон посилань для сторінок пагінації
        $params              = array_merge($this->request->param(), ['page' => 'xPAGEx']);
        $this_route_template = Route::url(Route::name($route), $params);

        // формуєм пагінацію через менеджер
        $manager    = new Frontend_Pagination_Manager($objects_count, $this_route_template, $page_size, $group_length);
        $pagination = $manager->getPage($page_number);

        // створення відповідного файлу виду і передача його до публічних блоків контролеру
        $view_pagination             = View::factory('frontend/blocks/pagination/' . $view);
        $view_pagination->pagination = $pagination;
        $this->blocks[$block_name]   = $view_pagination;

        return $manager;
    }

    protected function _validateORM(Model_Frontend_ORM $orm) {
        try {
            $orm->check();
        }
        catch (ORM_Validation_Exception $e) {
            $this->addErrors($e->errors(), $orm->getPostName());

            return false;
        }
        catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $orm Model_Frontend_ORM|Model_Frontend_ORM[]
     * @return bool true якщо без помилок валідації
     */
    public function validateORM($orm) {
        if (is_array($orm)) {
            $success = true;
            foreach ($orm as $item)
                if (!$this->_validateORM($item)) // якщо валідація пройшла з помилкою, то зберігаємо помилку для всіх
                {
                    $success = false;
                }

            return $success;
        }
        else {
            return $this->_validateORM($orm);
        }
    }

    public function getCurrentUrl($lang) {
        // отримуєм url поточного роута + вказуєм uri мови до параметрів
        $params = array_merge($this->request->param(), ['lang' => $lang]);

        return Route::url(Route::name($this->request->route()), $params);
    }

    protected function redirectToDefaultLanguage() {
        $default_uri = 'ru';

        // читаєм з бази першу мову (мова по замовчуванню)
//        $default_locale = ORM::factory('Frontend_Locale')
//                             ->order_by('ID', 'ASC')
//                             ->find();
//
//        HTTP::redirect($this->getCurrentUrl($default_locale->uri));

        HTTP::redirect($this->getCurrentUrl($default_uri));
    }

    public function getRequestParams(array $ext = []) {
        return array_merge($this->request->param(), $ext);
    }

    /**
     * Дії по замовчуванню до action
     */
    public function before() {
        parent::before();

        View::set_global('controller', $this);

        // отримуємо параметр мови і додатково фільтруєм його
        $lang = $this->request->param('lang', null);
        if ($lang !== false) { // якщо мова потрібна

            if ($lang == null) { // якщо мова не вказана (порівняння не строге, бо значення false ми вже відсіяли вище)
                // пробуємо знайти її в cookie
                $lang = Cookie::get('stored_lang', null);

                if ($lang == null) // якщо немає в cookie
                {
                    $this->redirectToDefaultLanguage();
                } // робимо редірект на мову за замовчуванням
                else {
                    HTTP::redirect($this->getCurrentUrl($lang));
                } // інакше робимо редірект на збережену в cookie
            }

            $this->language = $lang;
            $locale         = ORM::factory('Frontend_Locale')
                                 ->where('uri', '=', $lang)
                                 ->and_where('status', '=', 'active')
                                 ->find();

            if (!$locale->loaded()) // якщо мови не існує в БД
            {
                throw new HTTP_Exception_404();
            }
            else {
                $this->locale = $locale;
                I18n::loadDB($this->locale->ID); // ініціалізація модуля локалізації Богдана
                Cookie::set('stored_lang', $lang); // оновлюєм cookie

                $this->locales = ORM::factory('Frontend_Locale')
                                    ->where('status', '=', 'active')
                                    ->find_all();
            }
        }

        // скрипти
        $this->scripts[] = '/assets/js/jquery.krid-select.js';
        $this->scripts[] = Ajax::$dependent_from_toastr_js;
        $this->scripts[] = Ajax::$ajax_pligin_js;
        $this->scripts[] = Ajax::$ajax_pligin_init;
        $this->addLocalScript('main/config');

        $this->styles[] = Ajax::$dependent_from_toastr_css;
        $this->styles[] = '/assets/css/style_main.css';
        $this->styles[] = '/assets/css/file/style.css';

        $this->styles[]  = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css';
        $this->scripts[] = 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js';

        // сповіщення
        $this->notifications = Session::instance()->get('stored_notifications', []);
        Session::instance()->set('stored_notifications', []);

        // модальне вікно авторизації
        $email_value = '';
        if (Session::instance()->get('show_auth_modal') === true) {
            $email_value = Session::instance()->get('login_email');

            Session::instance()->delete('login_email');
            Session::instance()->delete('show_auth_modal');

            $this->addLocalScript('page/auth/fail_handle');
        }

        $this->blocks['footer/login-modal'] = View::factory('frontend/blocks/footer/login-modal', compact('email_value'));

        if (Session::instance()->get('show_forgot_modal') === true) {
            $email_value = Session::instance()->get('login_email');

            Session::instance()->delete('login_email');
            Session::instance()->delete('show_forgot_modal');

            $this->addLocalScript('page/auth/fail_email_forgot');
        }

        // посилання
        $this->links['to_main_page']       = Route::url('frontend_index', $this->request->param());
        $this->links['companies_page']     = Route::url('frontend_site_company_list_first_page', $this->getRequestParams(['page' => null]));
        $this->links['registration_page']  = Route::url('frontend_site_company_registration', $this->request->param());
        $this->links['login']              = Route::url('frontend_site_auth', ['action' => 'login']);
        $this->links['forgot_pass']        = Route::url('frontend_site_auth', ['action' => 'forgot_pass']);
        $this->links['logout']             = Route::url('frontend_site_auth', ['action' => 'logout']);
        $this->links['transport']          = Route::url('frontend_site_request_transport_list_first_page', $this->getRequestParams(['page' => null]));
        $this->links['transport_add_page'] = Route::url('frontend_site_request_add_transport', $this->request->param());
        $this->links['cargo']              = Route::url('frontend_site_request_cargo_list_first_page', $this->getRequestParams(['page' => null]));
        $this->links['cargo_add_page']     = Route::url('frontend_site_request_add_cargo', $this->request->param());
        $this->links['tender_add_page']    = Route::url('frontend_site_request_add_tender', $this->request->param());
        $this->links['tenders']            = Route::url('frontend_site_request_tender_list_first_page', $this->getRequestParams(['page' => null]));

        $this->links['news'] = Route::url('frontend_site_news_list', $this->getRequestParams(['uri' => 'news']));
        $this->links['contacts'] = Route::url('frontend_static_page', $this->getRequestParams(['uri' => 'contacts']));

        // визначаємо активний пункт меню
        $controller_action = $this->request->controller() . '/' . $this->request->action();
        switch ($controller_action) {
            case "Requests/addCargo":
            case "Requests/cargo": $this->selectedMenuItem = 'cargo'; break;
            case "Requests/addTransport":
            case "Requests/transport": $this->selectedMenuItem = 'transport'; break;
            case "Tenders/one":
            case "Requests/tender": $this->selectedMenuItem = 'tenders'; break;
            case "Company/page":
            case "Company/list": $this->selectedMenuItem = 'companies'; break;
            case "NewsSite/page":
            case "NewsSite/list": $this->selectedMenuItem = 'news'; break;
            case "Static/index": if ($this->request->param('uri') == 'contacts') {
                    $this->selectedMenuItem = 'contacts';
                }
                break;
        }

        $this->breadcrumbs = new Frontend_Breadcrumbs_Manager();
        $this->breadcrumbs->addLink(new Frontend_Breadcrumbs_Link(false, __db('main_page.title'), 'frontend_index', $this->request->param()));
        $this->blocks['breadcrumbs'] = 'frontend/blocks/breadcrumbs/in-the-heading';
    }

    protected function assignSeoIfNull(array $meta) {
        foreach ($meta as $key) {
            if (! isset($this->seo[$key]))
                $this->seo[$key] = '';
        }
    }

    /**
     * Дії по замовчуванюю після action
     */
    public function after() {
        // якщо ключ локалізації не заданий явно, то читаєм його з плагіну Богдана
        foreach ($this->l10n as $key => &$locale) {
            if ($locale === self::LOAD_LOCALE_FROM_DB) {
                $locale = __db($key);
            }
        }

        $email                                 = $this->user->email;
        $this->blocks['footer/feedback-modal'] = View::factory('frontend/blocks/footer/feedback-modal', compact('email'));

        $count_messages = Model_Frontend_Messages_Messages::getUnreadMessages(Frontend_Auth::userID());

        // google скрипт підключаємо останнім
        $this->addLocalScript('autocomplete_google');
        if ($this->locale !== null) { // якщо вказана мова, то підключаємо google API на ній
            $key             = Kohana::$config->load('frontend/site.google_API.key');
            $this->scripts[] = "http://maps.googleapis.com/maps/api/js?libraries=places&language={$this->locale->googleLang}&key={$key}";
        }

        $this->scripts = array_merge($this->scripts, Controller_Frontend_HMVC::$js_required);
        $this->styles  = array_merge($this->styles, Controller_Frontend_HMVC::$css_required);

        $this->seo['title'] = __db('site.title(:page_title)', [':page_title' => $this->l10n['title']]);
        $this->assignSeoIfNull(['keywords', 'description']);
        
        View::set_global('count_messages', $count_messages);

        parent::after();
    }

}