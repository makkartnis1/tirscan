<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Frontend_Ajax
 *
 * Базовий контролер для обробки ajax-запитів
 */
class Controller_Frontend_Ajax extends Controller {

    public $result = [];

    public $user;

    public $locale;

    public $auth_needed = false;

    public $lang_needed = false;

    public $errors = [];

    public function before() {
        if ($this->auth_needed) {
            $user_ID = Cookie::get('user', false);
            if ($user_ID === false) {
                throw new HTTP_Exception_403();
            }
            else {
                $this->user = ORM::factory('Frontend_User', $user_ID); // завантажуємо користувача

                if (! $this->user->loaded())
                    throw new HTTP_Exception_403();
            }
        }

        if ($this->lang_needed) {

        }

        $this->response->headers('Content-Type', 'application/json');
    }

    public function after() {
        $this->response->body( json_encode($this->result) );

        parent::after();
    }

    public function hasError($field_name, $group = 'common') {
        if (array_key_exists($group, $this->errors))
            if (array_key_exists($field_name, $this->errors[$group]))
                return true;

        return false;
    }

    public function addErrors(array $errors, $group = 'common') {
        if (array_key_exists($group, $this->errors))
            $this->errors[$group] = array_merge($this->errors[$group], $errors);
        else
            $this->errors[$group] = $errors;
    }

    public function classIfError($field_name, $group = 'common') {
        if ($this->hasError($field_name, $group))
            return ' errorForm';
        else
            return '';
    }

    public function echoErrors($field_name, $group = 'common') {
        if ($this->hasError($field_name, $group)) {
            $error_params = [];
            foreach ($this->errors[$group][$field_name][1] as $key => $value)
                $error_params[':' . $key] = $value;
            $this->errors[$group][$field_name][1] = $error_params;

            $view = View::factory( 'frontend/error/form-input', [ 'errors' => $this->errors[$group][$field_name] ] )->render();
            echo $view;
        }
    }

}
