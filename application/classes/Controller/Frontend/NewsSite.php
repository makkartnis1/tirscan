<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_NewsSite extends Controller_Frontend_Authorized {

    public function before() {
        parent::before();

        $this->blocks['header'] = 'frontend/blocks/header_page';

        $this->user_locale = $this->user->getLocale( $this->locale );
    }

    public function action_list(){

        $this->l10n = [
            'title' => __db('press-center-title')
        ];

        $page_size   = 12;
        $page_number = $this->request->param('page', null);
        $news_count  = Model_Frontend_News_NewsSite::countNews();
        $language    = $this->language;

        if ($page_number === null) {
            $page_number = 1;
        }

        $pagination_manager = $this->preparePaginationBlock('pagination_news', 'default',
            Route::get('frontend_site_news_list'), $news_count, $page_number, $page_size);

        $news = Model_Frontend_News_NewsSite::getAllNews($this->locale->ID, $page_size, $page_size*($page_number-1));

        $this->breadcrumbs->addLink( new Frontend_Breadcrumbs_Link(true, $this->l10n['title'], 'frontend_site_news_list', $this->getRequestParams(['page' => null])) );

        if ($pagination_manager->getPageCount() > 0) {
            $this->blocks['content'] = View::factory('frontend/content/press_center/list', compact('news', 'language'));
        }

    }

    public function action_page(){

        $news_id = $this->request->param('id');

        $news_data = Model_Frontend_News_NewsSite::getOneNewsById($news_id, $this->locale->ID);

        $this->l10n = [
            'title' => $news_data['title']
        ];

        $this->blocks['content'] = View::factory('frontend/content/press_center/page', compact('news_data'));
    }
}
