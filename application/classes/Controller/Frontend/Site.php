<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Site extends Controller_Frontend_Authorized {

    public $template = 'frontend/templates/index';

    public function before() {
        parent::before();
    }

    public function getLastCargo($count) {
        // вибірка списку останніх вантажних заявок
        $requests = array_map('Controller_Frontend_Requests::processRequest', Model_Frontend_Cargo::query_getCargo($this->locale->ID, false, $count, 0, [
            [
                ['t_cargo.status', '=', 'active.have-no-proposition'],
                ['t_cargo.status', '=', 'active.have-not-confirmed']
            ]
        ]));

        $result = [];
        foreach ($requests as $request) {
            if ($request['parser'] == null) {
                $request['info'] = Frontend_Helper_String::request_concat_info($request, false);
            }
            else {
                $request['info'] = Frontend_Helper_String::substr_if_more($request['info'], 20, true);
            }

            if ($request['json_sizeX'] != null) {
                $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
            }
            if ($request['json_sizeY'] != null) {
                $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
            }
            if ($request['json_sizeZ'] != null) {
                $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
            }

            $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
            $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

            if ($request['dateTo'] != null) {
                $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
            }

            $result[] = $request;
        }

        return $result;
    }

    public function getLastTransport($count) {
        // вибірка списку останніх транспортних заявок
        $requests = array_map('Controller_Frontend_Requests::processRequest', Model_Frontend_Transport::query_getTransports($this->locale->ID, false, $count, 0, [
            [
                ['t_trans.status', '=', 'active.have-no-proposition'],
                ['t_trans.status', '=', 'active.have-not-confirmed']
            ]
        ]));

        $result = [];
        foreach ($requests as $request) {
            if ($request['parser'] == null) {
                $request['info'] = Frontend_Helper_String::request_concat_info($request, false);
            }
            else {
                $request['info'] = Frontend_Helper_String::substr_if_more($request['info'], 20, true);
            }

            if ($request['json_sizeX'] != null) {
                $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
            }
            if ($request['json_sizeY'] != null) {
                $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
            }
            if ($request['json_sizeZ'] != null) {
                $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
            }

            $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
            $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

            if ($request['dateTo'] != null) {
                $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
            }

            $result[] = $request;
        }

        return $result;
    }

    public function getLastNews($count, $localeID) {
        return Model_Frontend_News_NewsSite::getLastNews($count, $localeID);
    }

    public function _getStatsForMainPage() {
        $transport_count           = Model_Frontend_Transport::query_getTransportCount([
            ['t_trans.status', 'LIKE', 'active.%']
        ]);
        $cargo_count               = Model_Frontend_Cargo::query_getCargoCount([
            ['t_cargo.status', 'LIKE', 'active.%']
        ]);
        $completed_transport_count = Model_Frontend_Transport::query_getTransportCount([
            ['t_trans.status', '=', 'completed']
        ]);
        $completed_cargo_count     = Model_Frontend_Cargo::query_getCargoCount([
            ['t_cargo.status', '=', 'completed']
        ]);

        $stats['completed_positions'] = $completed_transport_count + $completed_cargo_count;
        $stats['active_positions']    = $transport_count + $cargo_count;
        $stats['companies']           = Model_Frontend_Company::query_getCompaniesCount();
        $stats['countries']           = Model_Frontend_Geo::query_getUniqueGeoCount();

        return $stats;
    }

    public function getStatsForMainPage() {
        $tag     = 'db/site-stats';
        $context = $this;

        return (VarCache::expired($tag))
            ? VarCache::set($tag, function () use ($context) {
                return $this->_getStatsForMainPage();
            }, 3600)
            : VarCache::get($tag);
    }

    public function action_index() {
        $this->addLocalScript('page/index');

        $this->l10n['title'] = __db('SEO_main_title');
        $this->seo['description'] = __db('SEO_main_description');

        if ($this->request->method() == 'POST') {
            $params = $this->request->post();
            unset($params['f_request_type']);
            $query = http_build_query($params);

            if ($this->request->post('f_request_type')) {
                HTTP::redirect(Route::url('frontend_site_request_transport_list', $this->getRequestParams()) . '?' . $query);
            }
            else {
                HTTP::redirect(Route::url('frontend_site_request_cargo_list', $this->getRequestParams()) . '?' . $query);
            }
        }

        // отримуємо дані з БД для селектів форми
        $car_types = ORM::factory('Frontend_User_Car_Type')
                        ->find_all();

        $last_transport_requests = $this->getLastTransport(8);
        $last_cargo_requests     = $this->getLastCargo(8);
        $last_news               = $this->getLastNews(4, $this->locale->ID);

        $query       = [];
        $filter_form = View::factory('frontend/blocks/header/filter-on-main', compact('query', 'car_types'));

        $stats = $this->getStatsForMainPage();

        View::set_global('car_types', $car_types);
        View::set_global('filter_form', $filter_form);
        $this->blocks['content'] = View::factory('frontend/content/index', compact('stats', 'last_transport_requests', 'last_cargo_requests', 'last_news'));
    }

}