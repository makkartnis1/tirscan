<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Frontend_HMVC extends Controller {

    const SCRIPT_DIR = '/assets/js/frontend/page/profile/hmvc/';

    protected $_default_tab_layout          = 'frontend/content/profile/tab/infoblock/layout';
    protected $_default_tab_pill_layout     = 'frontend/content/profile/tab/infoblock/layout-tab-pill';
    protected $_default_tab_content_layout  = 'frontend/content/profile/tab/infoblock/layout-tab-content';

    public static $js_required      = [
        '/assets/js/frontend/linker/addons.js', // Мої допіли <<< LINKeRxUA
    ];
    public static $css_required     = [];

    public static $firs_request_params;
    public static $current_user;

    public static function _addJS($url){
        if(!in_array($url, self::$js_required))
            self::$js_required[] = $url;
    }

    public static function _addCSS($url){
        if(!in_array($url, self::$css_required))
            self::$css_required[] = $url;
    }

    public function before(){

        if($this->request->is_initial())
            throw HTTP_Exception::factory(403);

    }

    public function action_index(){
        $this->response->body($this->request->controller() . '/' . $this->request->action());
    }

    /**
     * @param $page_tabs array Дані для генерації таб
     *
     * $page_tabs = [
     *      // #my-tab
     *      // $tab_id           =>      $tab_data
     *      'my-tab'            => [
     *          'caption'       => 'my.tab.title-name',
     *          'content'       => '<div class="...     ...     ...</div>
     *      ],
     *      ...
     * ]
     *
     * @return View
     */
    protected function _compile_layout_tabs($page_tabs){

        $layout = View::factory($this->_default_tab_layout);

        $tab_pills = $tab_contents = [];

        $active = true;

        foreach ($page_tabs as $tabID => $data) {

            $tab_pills[] = View::factory($this->_default_tab_pill_layout, [
                'id' => $tabID,
                'active' => $active,
                'caption' => $data['caption']
            ]);

            $tab_contents[] = View::factory($this->_default_tab_content_layout,[
                'id'            => $tabID,
                'active'        => $active,
                'content'       => $data['content'],
            ]);

            if($active)
                $active = false;

        }

        $layout->pills      = implode(PHP_EOL, $tab_pills);
        $layout->contents   = implode(PHP_EOL, $tab_contents);

        return $layout;

    }

}