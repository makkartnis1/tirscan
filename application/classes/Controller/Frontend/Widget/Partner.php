<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Widget_Partner extends Controller_Frontend_HMVC {

    public function action_list(){

//        $s = DB::select('c.ID')->from(['Frontend_Companies', 'c'])->where('ID','!=', 634)->execute()->as_array();
//
//        $i = 1;
//
//        foreach ($s as $r){
//
//            $ins = DB::insert('Frontend_Company_Partners',['company1ID','company2ID']);
//
//            if($i == 1){
//                $ins->values([$r['ID'], 634])->execute();
//            }
//
//            if($i == 2){
//                $ins
//                    ->values([$r['ID'], 634])
//                    ->values([634, $r['ID']])
//                    ->execute();
//
//            }
//
//            if($i == 3){
//                $ins
//                    ->values([634, $r['ID']])
//                    ->execute();
//
//            }
//
//            $i++;
//
//            if($i == 4){
//                $i = 1;
//            }
//
//
//
//        }


        $companyID = (int) trim($this->request->param('param'));
        
        if( ($userID = Frontend_Auth::userID()) === false )
            return;

        $user = ORM::factory('Frontend_User', $userID);

        if(!$user->loaded())
            return;
        
        $list = Model_Frontend_Company::query_my_partners_confirmed($companyID, I18n::$db_localeID)
//            ->limit($limit)
//            ->offset($offset)
            ->execute()
            ->as_array()
        ;

//        var_dump($list);

        foreach ($list as &$row){

            $x = View::factory('frontend/content/companies/widget/list/element',[

//                ->select([$targetTableAndColumn, 'companyID'])
//                ->select([DB::expr('IFNULL(`fcl`.`name`, `fcl_primary`.`name`)'), 'visibleName'])
//                ->select(['fcl_primary.name', 'primaryName'])
//                ->select(['fuco.userID','ownerID'])

                'name'      => $row['visibleName'],
                'url'       => Controller_Frontend_Company::getCompanyUrlName([
                    'ID'                    => $row['companyID'],
                    'primaryName'           => $row['primaryName'],
                ]),
                'avatar'    => Helper_User::avatar($row['ownerID']),

            ]);

            $row = $x;

            unset($row);

        }


        $this->response->body(View::factory('frontend/content/companies/widget/list', ['list'=> implode(PHP_EOL, $list)]));
        
    }
    
    public function action_invite_button(){

        if( ($userID = Frontend_Auth::userID()) === false )
            return;

        $companyID = (int) trim($this->request->param('param'));
        $user = ORM::factory('Frontend_User', $userID);

        if(!$user->loaded())
            return;
        
        if($this->request->post('action-partner') == 'make-partner'){
            $result = Model_Frontend_Company::query_post_partner_request($user->companyID, $companyID);
        }

        $relation = Model_Frontend_Company::query_who_is_this_company($user->companyID, $companyID);

        $view = 'frontend/content/companies/widget/invite_button/';

        switch ($relation){
            
            case(Model_Frontend_Company::PARTNER_IS_MINE):
                $relation = View::factory($view.'is-mine'); break;

            case(Model_Frontend_Company::PARTNER_NOT_COFIRMED_YET):
                $relation = View::factory($view.'not-confirmed'); break;

            case(Model_Frontend_Company::PARTNER_POSSIBLE_TO_BE):
                $relation = View::factory($view.'button'); break;

            case(Model_Frontend_Company::PARTNER_WAIT_YOUR_CONFIRMATOIN):
                $relation = View::factory($view.'button-to-accept'); break;

        }
        
        $this->response->body($relation);

        
    }

}
