<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Frontend_Ajax
 *
 * Базовий контролер для обробки ajax-запитів
 */
class Controller_Frontend_Profile_Ajax extends Controller_Frontend_Ajax {

    public function action_update_user() {
        $this->result = [
            ['test'=>1,
            'sds' => [1, 5, 3]],
            ['test'=>2],
        ];
    }

    public function action_update_company() {
        $this->result = [
            'test'=>1
        ];
    }

//    //Скарга на компанію
//    public function action_complains() {
//        $post = $this->request->post();
//
//        if ($this->request->method() == Kohana_HTTP_Request::POST) {
//            $this->handleComplainsPost($post);
//        }
//    }
//
//    //Повідомлення компанії
//    public function action_send_message() {
//        $post = $this->request->post();
//
//        if ($this->request->method() == Kohana_HTTP_Request::POST) {
//            $this->handleMessagePost($post);
//        }
//    }
//
//    public function handleMessagePost($form_data){
//        $user_from = Arr::get($form_data, 'user_from');
//        $user_to   = Arr::get($form_data, 'user_to');
//        $theme     = Arr::get($form_data, 'theme');
//        $message   = Arr::get($form_data, 'message');
//        $response_array = [];
//
//        $validation = Validation::factory($form_data)
//            ->rule('message', 'not_empty')
//            ->rule('theme', 'not_empty');
//
//        if (!$validation->check()) {
//            $validation = false;
//            $response_array['error'] = 'Заповніть поле форми';
//        }
//
//        if($validation) {
//            $db = Database::instance();
//
//            $db->begin();
//            try {
//                Model_Frontend_Messages_Dialogs::createDialog($theme, $user_to, $user_from);
//
//                $id_dialog = Model_Frontend_Messages_Dialogs::getLastDialog();
//                Model_Frontend_Messages_Messages::addMessage($id_dialog, $message, $user_from);
//
//                $last_message = Model_Frontend_Messages_Messages::getLastMessage();
//                Model_Frontend_Messages_Messages::addStatus($user_to, $id_dialog, $last_message);
//
//                Model_Frontend_Messages_Dialogs::addStatus($user_to, $id_dialog);
//
//                $db->commit();
//
//                $response_array['success'] = 'Success';
//
//            } catch (HTTP_Exception_Redirect $e) {
//
//                $db->rollback();
//
//                throw $e;
//            }
//        }
//
//        echo json_encode($response_array); die();
//    }
//
//
//    public function handleComplainsPost($form_data) {
//        $user_id = ORM::factory('Frontend_User', Arr::get($form_data, 'user-id'));
//        $message = Arr::get($form_data, 'message');
//        $complain_company_id = ORM::factory('Frontend_Company', Arr::get($form_data, 'complain-company-id'));
//        $response_array = [];
//
//        $validation = Validation::factory($form_data)
//            ->rule('message', 'not_empty');
//
//        if (!$validation->check()) {
//            $validation = false;
//            $response_array['error'] = 'Заповніть поле форми';
//        }
//
//        if($validation) {
//            try {
//                Model_Frontend_Complaints::addMessage($user_id, $message, $complain_company_id);
//                $response_array['success'] = 'Success';
//            } catch (HTTP_Exception_Redirect $e) {
//                throw $e;
//            }
//        }
//
//        echo json_encode($response_array); die();
//    }
}