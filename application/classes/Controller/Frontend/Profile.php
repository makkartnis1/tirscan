<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Profile extends Controller_Frontend_Authorized {

    /**
     * @var Model_Frontend_User
     */
    public static $orm_cur_user;

    public function before() {
        parent::before();
        
        if (! $this->authorized)
            HTTP::redirect( Route::url( 'frontend_index', $this->request->param() ) );
        
        self::$orm_cur_user = $this->user;

        $this->blocks['header'] = 'frontend/blocks/header_page';
        $this->blocks['content/heading'] = 'frontend/content/profile/heading';
    }

    

    public function action_index() {

        Controller_Frontend_HMVC::_addJS(Controller_Frontend_HMVC::SCRIPT_DIR.'head.js');



//        if (Kohana::$profiling === TRUE)
//        {
//            // Start a new benchmark
//            $benchmark = Profiler::start('Your Category', __FUNCTION__);
//        }


//        $this->blocks['content'] =
//            $page_layout
//            .
//            View::factory('profiler/stats')->render()
//        ;

//        if (isset($benchmark))
//        {
//            // Stop the benchmark
//            Profiler::stop($benchmark);
//        }
//
//        return;

        $this->l10n = [ 'title' => __db('profile.title') ];

        $routeParam = ['localeID' => $this->locale->ID];

        Controller_Frontend_HMVC::$firs_request_params = $this->request->param();
        Controller_Frontend_HMVC::$current_user = $this->user;

        // локальні скрипти
        $this->addLocalScript('page/profile/profile_my_transport');
        $this->addLocalScript('page/profile/profile_my_requests');


        $page_layout = View::factory('frontend/content/profile');

        // Верхній ряд (шапка блоку "контент")
        $page_layout->profile_head = Route::url('hmvc/profile', array_merge(
            $routeParam,
            ['controller'=>'heading']
        ));
        $page_layout->profile_head = Request::factory($page_layout->profile_head)->execute()->body();

        // Таби сторінки
        $tab_shell = Kohana::$config->load('frontend/profile/tab/1-menu')->as_array();

        $tab_nav = $tab_contend = [];

        $active = true;

        foreach ($tab_shell as $tab_id => $tab_data){

            // перевірка чи користувач має право бачити поточну ТАБ-у

            if(

                # не пусте поле,    не перевірка на "власника компанії",    має право
                (!empty($tab_data['enabled']) AND ($tab_data['enabled'] != 'owner')  and ($this->user->hasRights($tab_data['enabled'])))

                OR
                # не пусте поле,    перевірка на "власника компанії",       є власником
                (!empty($tab_data['enabled']) AND ($tab_data['enabled'] == 'owner') AND ($this->user->isOwnerOfProfileCompany()))

                OR

                (empty($tab_data['enabled']))
            ){

                $tab_nav[] = View::factory('frontend/content/profile/tab/1.menu/nav-tab', [
                   'id'         => $tab_id,
                   'active'     => $active,
                   'icon'       => $tab_data['tab']['icon'],
                   'caption'    => __db($tab_data['tab']['caption']),
                ]);

                $url = Route::url($tab_data['content-hmvc']['route']['name'], array_merge(
                    $routeParam,
                    $tab_data['content-hmvc']['route']['parameters'] )
                );

                if(Kohana::$profiling === true)
                    $benchmark = Profiler::start('Tabs 1st lvl hmvc', $tab_id);

                $hmvc = Request::factory($url)->execute()->body();

                if(Kohana::$profiling === true)
                    Profiler::stop($benchmark);

                $tab_contend[] = View::factory('frontend/content/profile/tab/1.menu/nav-content',[
                    'id'        => $tab_id,
                    'active'    => $active,
                    'content'   => $hmvc
                ]);

                if($active == true)
                    $active = false;

            }

        }
        
        

        $page_layout->tab_nav       = implode(PHP_EOL, $tab_nav);
        $page_layout->tab_content   = implode(PHP_EOL, $tab_contend);


        $this->blocks['content'] = $page_layout;

    }

}