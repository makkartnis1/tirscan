<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Base extends Controller {

    protected      $auth;
    protected      $user;
    protected      $role;
    protected      $logged_in = null;
    public         $template;
    public         $controller;
    public         $action;
    public         $param;
    public         $lang;
    public         $lang_id;
    public         $site_lang;
    public         $site_lang_id;
    public         $m_plugin;
    public         $plugin_info;
    public         $config;

    public function before()
    {
        parent::before();
        $this->template = View::factory('cms/v_base');
        $this->controller = $this->request->controller();
        $this->action = $this->request->action();
        $this->param = $this->request->param('param');

        $this->m_plugin = new Model_Cms_Plugin();

        $this->lang = $this->request->param('lang');
        if (!$this->lang or !$this->m_plugin->issetLangByPref($this->lang)) $this->lang = $this->m_plugin->getDefaultPrefixLang();
        $this->lang_id = $this->m_plugin->getIdByPref($this->lang);


        View::set_global('controller',$this->controller);
        View::set_global('action',$this->action);
        View::set_global('param',$this->param);
        View::set_global('lang',$this->lang);
        View::set_global('lang_id',$this->lang_id);


        $config = Kohana::$config->load('cms/main');
        $this->config['browser_key']  = $config->get('browser_key');
        $this->config['server_key']  = $config->get('server_key');
        $this->config['date_to']  = $config->get('date_to');

        View::set_global('config',$this->config);

        View::bind_global('logged_in',$this->logged_in);

        if (!isset($_SESSION['collapse'])) $_SESSION['collapse'] = '0';

        if (Helper_Myauth::isLogin(1))
        {
            if ($this->controller=='Login' and $this->action!='logout')
            {
                $this->redirect_to_plugin();
            }

            $this->user = Helper_Myauth::getUserInfo(1);
            $this->logged_in = true;
            $this->plugin_info = $this->m_plugin->getPluginInfo($this->controller,$this->action,$this->lang_id);
            View::set_global('logged_in',true);
            View::set_global('user_info',$this->user);
            View::set_global('plugin_info',$this->plugin_info);

              $m_complaints = new Model_Component_Cms_Complaints();
              View::set_global('count_complaints',$m_complaints->countNew());

              $m_feedback = new Model_Component_Cms_Feedback();
              View::set_global('count_feedback',$m_feedback->countNew());

        }
        else
        {
            if ($this->controller!='Login')
                HTTP::redirect(Route::get('cms')->uri(array('controller'=>'login','lang'=>$this->lang)));
        }

    }

    public function after()
    {
       $this->response->body($this->template);
    }

    public function redirect_to_plugin()
    {
        $plugin = $this->m_plugin->getFirst($this->lang_id);
        HTTP::redirect(Route::get('cms')->uri(array('controller'=>$plugin['controller'],'action'=>$plugin['action'],'lang'=>$this->lang)));
    }


    public function get_access_denied_view()
    {
        return 'cms/errors/access_denied';
    }



}