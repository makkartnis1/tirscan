<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Redactor extends Controller {

    public function action_index()
    {


        $dir = $_SERVER['DOCUMENT_ROOT'].'/public/cms/files/redactor/';
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg')
        {
            $file = md5(date('YmdHis')).'.jpg';

            move_uploaded_file($_FILES['file']['tmp_name'], $dir.$file);

            $array = array(
                'filelink' => '/public/cms/files/redactor/'.$file
            );

            echo stripslashes(json_encode($array));
            die;
        }
    }

}