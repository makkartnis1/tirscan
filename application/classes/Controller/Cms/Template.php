<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Template extends Controller_Cms_Base {


    public function before()
    {
        parent::before();

        $top = View::factory('cms/blocks/v_top');
        $footer = View::factory('cms/blocks/v_footer');

        $top->plugins = $this->m_plugin->getViewPlugins($this->lang_id,$this->user['group_id']);

        $this->template->block_top = array($top);
        $this->template->block_center = null;
        $this->template->block_footer = array($footer);


    }

}