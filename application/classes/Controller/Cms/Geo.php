<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Geo extends Controller {

    private $lang_id;
    private $google_lang;
    private $server_key;

    public function before()
    {
        $this->google_lang = $this->request->param('google_lang');
        $m_lang = new Model_Cms_Plugin_Lng();
        $lang = $m_lang->get_one_by_google_lang($this->google_lang);
        $this->lang_id = $lang['ID'];

        $config = Kohana::$config->load('cms/main');
        $this->server_key = $config->get('server_key');
    }

    //методи які викликаються по URL

    //отримання масива ідентифікаторів таблиці Frontend_Geo по назві місця
    public function action_get_geo_ids()
    {
        if (!isset($_GET['name']))
        {
            Helper_Logs_Geo::error('перевірка існування GET-змінної name для метода get_geo_ids','','змінна відсутня');
            die;
        }

        if (!$_GET['name'])
        {
            Helper_Logs_Geo::error('перевірка чи не пуста GET-змінна name для метода get_geo_ids','','змінна пуста');
            die;
        }

        $ids = $this->getGeoIdsFromDbByAddress($_GET['name'],$this->lang_id);
        if ($ids)
        {
            echo implode(',',array_unique($ids));
            die;
        }


        $ids = $this->getFromGoogleAllPlacesByAddress($_GET['name'],false);
        if (!$ids) { echo '3379'; die;}
        echo implode(',',array_unique($ids));
        die;
    }

    //отримання масива ідентифікаторів таблиці Frontend_Geo по place_id місця
    public function action_add()
    {
        $place_id = $this->request->post('place_id');

        $status = 1;

        if (is_null($place_id))
        {
            Helper_Logs_Geo::error('перевірка існування POST-змінної place_id для метода add','','змінна відсутня');
            $status = 0;
        }

        if (!$place_id)
        {
            Helper_Logs_Geo::error('перевірка чи не пуста POST-змінна place_id для метода add','','змінна пуста');
            $status = 0;
        }

        if ($status)
        {
            $ids = $this->getGeoIdsFromDbByPlaceID($place_id);
            if ($ids)
            {
                $this->response->body(implode(',',array_unique($ids)));
            }
            else
            {
                $ids = $this->getFromGoogleAllPlacesByAddress(false,$place_id);
                if ($ids) {
                    $this->response->body(implode(',',array_unique($ids)));
                }
            }
        }


    }

    //методи для внутрішнього використання

    //метод для отримання повних даних про місце і загальних даних про місця які до нього прив'язані по ієрархії
    private function getFromGoogleAllPlacesByAddress($address,$place_id)
    {
        $ch = curl_init();

        if ($address)
        {
            $address = urlencode($address);
            $link = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$this->server_key.'&language='.$this->google_lang;
            curl_setopt($ch, CURLOPT_URL, $link);

        }elseif($place_id)
        {
            $link = 'https://maps.googleapis.com/maps/api/geocode/json?place_id='.$place_id.'&key='.$this->server_key.'&language='.$this->google_lang;
            curl_setopt($ch, CURLOPT_URL, $link);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result,true);

        $status = $result['status'];

        $result = $result['results'];

        if ($status != 'OK') {

            Helper_Logs_Geo::error('CURL запит метода getFromGoogleAllPlacesByAddress',$link,'результат запита пустий');
            return false;
        }

        Helper_Logs_Geo::success('CURL запит метода getFromGoogleAllPlacesByAddress',$link);


        $country_code = '';
        $places = [];
        $lvls = 0;

        $available_types = [
            'locality'=>'city',
            'administrative_area_level_1'=>'region',
            'country'=>'country'
        ];

        foreach($result[0]['address_components'] as $place)
        {
            if (empty($place['types'])) continue;
            $type = $place['types'][0];
            if (!array_key_exists($type,$available_types)) continue;
            $lvls++;

            $data = [
                'name' => $place['long_name'],
                'type' => $available_types[$type],
                'parent' => NULL
            ];

            if ($lvls == 1)
                $data['details'] = [
                    'place_id'=>$result[0]['place_id'],
                    'lat'=>$result[0]['geometry']['location']['lat'],
                    'lng'=>$result[0]['geometry']['location']['lng'],
                    'full_name'=>$result[0]['formatted_address']
                ];

            $places[] = $data;

            if ($type == 'country') $country_code = $place['short_name'];
        }

        if (empty($places))
        {
            Helper_Logs_Geo::error('перевірка на пустоту обробленого масива місць після отримання його із CURL запита',json_encode($result),'масив місць пустий');
            return false;
        }

        //присвоєння значення 'level' і 'country'
        foreach($places as &$p)
        {
            $p['country']=$country_code;
            $p['level'] = $lvls;
            $lvls--;
        }

        return $this->insertGooglePlacesIntoDB($places);
    }


    //метод для отримання повних даних про місце
    private function getFromGooglePlaceDetailsByAddress($address)
    {

        $address = urlencode($address);

        $link = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.$this->server_key.'&language='.$this->google_lang;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($result,true);

        $status = $result['status'];
        $result = $result['results'];

        if ($status != 'OK')
        {
            Helper_Logs_Geo::error('CURL запит метода getFromGooglePlaceDetailsByAddress',$link,'результат запита пустий');
            return false;
        }

        Helper_Logs_Geo::success('CURL запит метода getFromGooglePlaceDetailsByAddress',$link);

        $details = [
            'place_id'=>$result[0]['place_id'],
            'lat'=>$result[0]['geometry']['location']['lat'],
            'lng'=>$result[0]['geometry']['location']['lng'],
            'full_name'=>$result[0]['formatted_address']
        ];

        return $details;

    }


    private function insertGooglePlacesIntoDB(array $google_places)
    {

        $m_geo = new Model_Cms_Geo();

        foreach($google_places as &$place)
        {
            $geo_id = $m_geo->get_geo_id_by_name($place['name'],$this->lang_id);
            if (!$geo_id)
            {
                //для першого значення дані уже присутні, не потрібно робити зайвий запит до сервіса гугла
                if (array_key_exists('details',$place))
                    continue;

                $details = $this->getFromGooglePlaceDetailsByAddress($place['name']);
                if (!$details)
                {
                    Helper_Logs_Geo::error('отримання деталей нового місця за допомогою метода getFromGooglePlaceDetailsByAddress',$place['name'],'дані не отримані');
                    return false;
                }

                $place['details'] = $details;
            }
            else
            {
                $place['geo_id'] = $geo_id;
                if (array_key_exists('details',$place))
                    unset($place['details']);
            }
        }


        //збереження в базу нових місць, якщо присутній ключ-мітка 'details' і отримання geo_id
        $ids = [];
        foreach($google_places as &$p)
        {
            if (array_key_exists('geo_id',$p)) $ids[] = $p['geo_id'];
            if (!array_key_exists('details',$p) and array_key_exists('geo_id',$p)) continue;
            $p['geo_id'] = $m_geo->save(
                $p['details']['place_id'],
                $this->lang_id,
                $p['details']['lat'],
                $p['details']['lng'],
                $p['name'],
                $p['details']['full_name'],
                $p['country'],
                $p['type'],
                $p['level']
            );
            if ($p['geo_id'])
            {
                $ids[] = $p['geo_id'];
            }
            else
            {
                return false;
            }

        }

        if (empty($ids))
        {
            Helper_Logs_Geo::error('перевірка масива результуючих ідентифікаторів на пустоту у методі insertGooglePlacesIntoDB','','масив пустий');
            return false;
        }

        //встановлення значення 'parent'
        for( $i=count($google_places)-1; $i>0; $i-- )
            if (array_key_exists('details',$google_places[$i-1]))
                $google_places[$i-1]['parent'] = $google_places[$i]['geo_id'];

        //збереження в базу параметра 'parent'
        foreach($google_places as $place)
            if (array_key_exists('details',$place))
                $m_geo->set_parent($place['geo_id'],$place['parent']);

        return $ids;
    }

    private function getGeoIdsFromDbByAddress($address,$lng_id)
    {
        $m_geo = new Model_Cms_Geo();
        $id = $m_geo->get_geo_id_by_name($address,$lng_id);
        if (!$id) return false;
        $ids = $m_geo->get_ids_by_id($id,array());
        if (!$ids) return false;
        return $ids;
    }


    private function getGeoIdsFromDbByPlaceID($place_id)
    {
        $m_geo = new Model_Cms_Geo();
        $id = $m_geo->get_id_by_place_id_and_lang($place_id,$this->lang_id);
        if (!$id) return false;
        $ids = $m_geo->get_ids_by_id($id,array());
        if (!$ids) return false;
        return $ids;
    }
}