<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Admin extends Controller_Cms_Template {

    public $access = 1;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 2) $this->access = 0;
    }

    public function action_index()
    {
        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_PluginLng();
        $lang = $m_lang->get_one_by_pref($lang_pref);
        $view = View::factory('cms/plugin/admin/v_index');
        $m_group = new Model_Cms_Plugin_AdminGroup();
        $view->groups = $m_group->get_all($lang['id'],$this->user['group_id']);
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_add()
    {
        $view = View::factory('cms/plugin/admin/v_add');
        $m_admin = new Model_Cms_Plugin_Admin();

        if (isset($_POST['go']) and $this->access)
        {
            $info = $m_admin->valid($_POST);
            if (empty($info['errors']))
            {
                $id = $m_admin->add($info['data']);

                if (isset($_FILES['img']))
                {
                    if ($_FILES['img']['size']!=0)
                    {
                        $uploaddir = '/uploads/images/admins/';
                        $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                        if (!file_exists($dir)) mkdir($dir);
                        move_uploaded_file($_FILES['img']['tmp_name'],$dir.$id.'.jpg');
                    }
                }

                $view->add_ok = 1;
            }
            else
            {
               $view->errors = $info['errors'];
            }
        }

        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_PluginLng();
        $lang = $m_lang->get_one_by_pref($lang_pref);

        $m_group = new Model_Cms_Plugin_AdminGroup();
        $view->groups = $m_group->get_all($lang['id'],$this->user['group_id']);

        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_edit()
    {
        $view = View::factory('cms/plugin/admin/v_edit');
        $m_admin = new Model_Cms_Plugin_Admin();

        $this->param = (int) $this->param;
        $admin_info = $m_admin->get_one($this->param);
        if (!$admin_info) throw new Kohana_HTTP_Exception_404;
        $view->admin_info = $admin_info;

        if (isset($_POST['go']) and $this->access)
        {
            $change_pass = (isset($_POST['change_pass']))?1:0;
            $info = $m_admin->valid($_POST,$this->param,$change_pass);
            if (empty($info['errors']))
            {
                $m_admin->save($info['data'],$this->param,$change_pass);
                if (isset($_FILES['img']))
                {
                    if ($_POST['file_exist'] == 1)
                    {
                        if ($_FILES['img']['size']!=0)
                        {
                            $uploaddir = '/uploads/images/admins/';
                            $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                            if (!file_exists($dir)) mkdir($dir);
                            move_uploaded_file($_FILES['img']['tmp_name'],$dir.$this->param.'.jpg');
                        }
                    }
                    else
                    {
                        $uploaddir = '/uploads/images/admins/';
                        $file=$_SERVER['DOCUMENT_ROOT'].$uploaddir.$this->param.'.jpg';
                        if (file_exists($file)) unlink($file);
                    }
                }
                $view->save_ok = 1;
                $admin_info = $m_admin->get_one($this->param);
                $view->admin_info = $admin_info;
            }
            else
            {
                $view->errors = $info['errors'];
            }
        }

        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_PluginLng();
        $lang = $m_lang->get_one_by_pref($lang_pref);

        $m_group = new Model_Cms_Plugin_AdminGroup();
        $view->groups = $m_group->get_all($lang['id'],$this->user['group_id']);

        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);

    }

    public function action_db()
    {

        if (!$this->access) die;

        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_PluginLng();
        $lang = $m_lang->get_one_by_pref($lang_pref);

        $_dt = new Model_Cms_Dt_Admin();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id',$this->user['group_id']);
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id',$this->user['group_id']);
        $res['data'] = $_dt->get($_POST,$lang['id'],$this->user['group_id']);
        echo json_encode($res);
        die;
    }


    public function action_ajax()
    {
        if (!$this->access) die;

        $m_admin = new Model_Cms_Plugin_Admin();
        switch ($this->param) {
            case 'del':
                $m_admin->del_by_id($_POST['id']);
                $img = $_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$_POST['id'].'.jpg';
                if (file_exists($img)) unlink($img);
            break;
            case 'del_all':
                $m_admin->del_by_ids($_POST['ids']);
                foreach($_POST['ids'] as $id)
                {
                    $img = $_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$id.'.jpg';
                    if (file_exists($img)) unlink($img);
                }
            break;
            case 'active_all':
                $m_admin->active_by_ids($_POST['ids']);
            break;
            case 'blocked_all':
                $m_admin->blocked_by_ids($_POST['ids']);
            break;
        }
        die;
    }


    public function action_group()
    {
        $view = View::factory('cms/plugin/admin/v_group');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_group_add()
    {
        $view = View::factory('cms/plugin/admin/v_group_add');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_group_edit()
    {
        $view = View::factory('cms/plugin/admin/v_group_edit');
        $view->id = (int) $this->param;
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

}