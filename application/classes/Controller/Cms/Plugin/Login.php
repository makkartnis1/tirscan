<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Login extends Controller_Cms_Template {

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {


        $view = View::factory('cms/plugin/v_login');

        if (isset($_POST['email']))
        {
                $log_in = $this->valid();
                if ($log_in=='ok') $this->redirect_to_plugin();
                if ($log_in=='blocked') $view->error_blocked = true;
                if ($log_in=='not_isset') $view->error_not_isset = true;
        }

        $this->template->block_center = array($view);
        $this->template->block_top = array();
        $this->template->block_footer = array();
    }

    public function action_logout()
    {
        Helper_Myauth::logOut(1);
        HTTP::redirect(Route::get('cms')->uri(array('controller'=>'login','lang'=>$this->lang)));
    }

    public function action_valid()
    {
        echo json_encode($this->valid());
        die;
    }

    private function valid()
    {
        if (strpos($_POST['email'],'@') == false)
        {
            return 'not_isset';
        }
        else
        {
            return Helper_Myauth::logIn(1,$_POST['email'],$_POST['pass'],$_POST['rememb']);
        }
    }
}