<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Messages extends Controller_Cms_Template {

    public $access = 1;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 3) $this->access = 0;
    }

    public function action_user()
    {
        $view = View::factory('cms/plugin/messages/v_user');
        $view->dialog_id = $this->request->param('param');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

}