<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Analytics extends Controller_Cms_Template {

    public $access = 1;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 3) $this->access = 0;
    }

    public function action_index()
    {
        $view = View::factory('cms/plugin/analytics/v_index');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $view->main_analytics = $this->test_main();
        $view->services = $this->test_services();
        $view->user_activities = $this->test_users();
        $view->arr_time = ['hour','day','week','month','all'];

        $view->arr_help_1 = [
            'transports'=>'Транспортні заявки',
            'loads'=>'Вантажні заявки',
            'tenders'=>'Тендери'
        ];

        $view->arr_help_2 = [
            'carriers'=>'Перевізники',
            'cargo_owners'=>'Власники вантажів',
            'expeditors'=>'Експедитори'
        ];

        $view->arr_help_3 = [
            'managers'=>'Менеджери',
            'own_transports'=>'Транспорт',
            'company_news'=>'Новини компаній',
            'services_money'=>'Куплено валюти'
        ];

        $view->arr_help_4 = [
            'top'=>'Топ',
            'vip'=>'VIP',
            'selected'=>'Виділені',
            'urgently'=>'Термінові',
            'up'=>'Підняті',
        ];

        $view->arr_help_5 = [
            'online'=>'Онлайн',
            'visits'=>'Відвідування',
            'unique'=>'Унікальних'
        ];

        $this->template->block_center = array($view);
    }

    private function test_main()
    {

        $result = [
            'transports',
            'loads',
            'tenders',
            'comments',
            'carriers',
            'cargo_owners',
            'expeditors',
            'managers',
            'own_transports',
            'company_news',
            'services_money'
        ];

        $time_arr_1 = [
            'hour'=>['0','10'],
            'day'=>['30','100'],
            'week'=>['150','700'],
            'month'=>['1500','5000'],
            'all'=>['10000','20000']
        ];

        $time_arr_2 = [
            'hour'=>['0','1'],
            'day'=>['1','5'],
            'week'=>['5','10'],
            'month'=>['10','50'],
            'all'=>['50','200']
        ];

        $time_arr_3 = [
            'hour'=>['0','3'],
            'day'=>['10','30'],
            'week'=>['50','250'],
            'month'=>['500','1700'],
            'all'=>['3000','9000']
        ];

        foreach($result as $r)
        {
            $f_arr = $time_arr_3;
            if ($r == 'transports' or $r == 'loads' or $r == 'tenders') $f_arr = $time_arr_1;
            if ($r == 'tenders') $f_arr = $time_arr_2;
            if ($r == 'carriers' or $r == 'cargo_owners' or $r == 'expeditors') $f_arr = $time_arr_3;

            foreach($f_arr as $key=>$val)
            {
                $result[$r][$key] = [];

                if ($r == 'transports' or $r == 'loads' or $r == 'tenders')
                {
                    $closed = rand($val[0],$val[1]);
                    $process = rand($val[0],$val[1]);
                    $all = rand($val[0],$val[1])+$closed+$process;

                    $result[$r][$key]['closed'] = '<span class="no_data">---</span>';
                    $result[$r][$key]['process'] = '<span class="no_data">---</span>';
                    $result[$r][$key]['all'] = '<span class="no_data">---</span>';

                    if ($closed > 0)
                        $result[$r][$key]['closed'] = '<a href="#" class="badge badge-success">'.$closed.'</a>';

                    if ($process > 0)
                        $result[$r][$key]['process'] = '<a href="#" class="badge badge-primary">'.$process.'</a>';

                    if ($all > 0)
                        $result[$r][$key]['all'] = '<a href="#" class="badge">'.$all.'</a>';

                }
                elseif($r == 'carriers' or $r == 'cargo_owners' or $r == 'expeditors')
                {
                    $approved = rand($val[0],$val[1]);
                    $pending = rand($val[0],$val[1]);

                    $result[$r][$key]['approved'] = '<span class="no_data">---</span>';
                    $result[$r][$key]['pending'] = '<span class="no_data">---</span>';

                    if ($approved > 0)
                        $result[$r][$key]['approved'] = '<a href="#" class="badge badge-success">'.$approved.'</a>';

                    if ($key == 'hour' or $key == 'day')
                    {
                        if ($pending > 0)
                            $result[$r][$key]['pending'] = '<a href="#" class="badge badge-important">'.$pending.'</a>';
                    }

                }
                else
                {
                    $class = 'success';
                    if ($r == 'comments') $class = 'primary';
                    $temp = rand($val[0],$val[1]);
                    $result[$r][$key] = '<span class="no_data">---</span>';
                    if ($temp > 0)
                        $result[$r][$key] = '<a href="#" class="badge badge-'.$class.'">'.$temp.'</a>';
                }

            }
        }

        return $result;
    }

    private function test_services()
    {
        $result = [
            'top',
            'vip',
            'selected',
            'urgently',
            'up'
        ];

        $time_arr = [
            'hour'=>['0','3'],
            'day'=>['10','30'],
            'week'=>['50','250'],
            'month'=>['500','1700'],
            'all'=>['3000','9000']
        ];


        foreach($result as $r)
        {
            foreach($time_arr as $key=>$val)
            {
                $temp = rand($val[0],$val[1]);
                $result[$r][$key] = '<span class="no_data">---</span>';
                if ($temp > 0)
                    $result[$r][$key] = '<a href="#" class="badge badge-success">'.$temp.'</a>';
            }
        }

        return $result;
    }

    private function test_users()
    {

        $result = [
            'online',
            'visits',
            'unique'
        ];

        $time_arr = [
            'hour'=>['250','500'],
            'day'=>['500','1000'],
            'week'=>['1000','5000'],
            'month'=>['5000','10000'],
            'all'=>['10000','20000']
        ];

        foreach($result as $r)
        {
            foreach($time_arr as $key=>$val)
            {
                if ($r == 'visits')
                {

                    $result[$r][$key] = [];

                    $auths = rand($val[0],$val[1]);
                    $anonims = rand($val[0],$val[1]);

                    $result[$r][$key]['auths'] = '<span class="no_data">---</span>';
                    $result[$r][$key]['anonims'] = '<span class="no_data">---</span>';

                    if ($auths > 0)
                        $result[$r][$key]['auths'] = '<a href="#" class="badge badge-success">'.$auths.'</a>';

                    if ($anonims > 0)
                        $result[$r][$key]['anonims'] = '<a href="#" class="badge badge-primary">'.$anonims.'</a>';

                }
                else
                {
                    $temp = rand($val[0],$val[1]);
                    $result[$r][$key] = '<span class="no_data">---</span>';
                    if ($temp > 0)
                        $result[$r][$key] = '<a href="#" class="badge badge-success">'.$temp.'</a>';
                }
            }
        }

        return $result;
    }
}