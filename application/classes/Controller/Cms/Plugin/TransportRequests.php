<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_TransportRequests extends Controller_Cms_Template {

    public $access = 1;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 3) $this->access = 0;
    }

    public function action_index()
    {
        $view = View::factory('cms/plugin/transport_requests/v_index');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_add()
    {
        $view = View::factory('cms/plugin/transport_requests/v_add');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_edit()
    {
        $view = View::factory('cms/plugin/transport_requests/v_edit');
        $view->id = (int) $this->param;
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }


}