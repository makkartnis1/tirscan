<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Settings extends Controller_Cms_Template {

    public $access = 1;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 3) $this->access = 0;
    }

    public function action_locales()
    {
        $view = View::factory('cms/plugin/locales/v_locales');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_locale_edit()
    {
        $view = View::factory('cms/plugin/locales/v_locale_edit');
        $view->id = (int) $this->param;
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_currencies()
    {
        $view = View::factory('cms/plugin/currencies/v_currencies');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_currencies = new Model_Cms_Plugin_Currencies();
        if (isset($_POST['add_new']))
        {
            $m_currencies->add($_POST);
        }

        if (isset($_POST['save']))
        {
            $m_currencies->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_currencies()
    {
        $_dt = new Model_Cms_Dt_Currencies();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id');
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }


    public function action_ajax_currencies()
    {

        $_dt = new Model_Cms_Plugin_Currencies();

        switch ($this->param) {
            case 'del':
                $_dt->del($_POST['id']);
                break;
            case 'del_all':
                $_dt->del_all($_POST['ids']);
                break;
        }

        die;
    }

    public function action_car_types()
    {
        $view = View::factory('cms/plugin/car_types/v_car_types');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_car_types = new Model_Cms_Plugin_Cartypes();
        if (isset($_POST['add_new']))
        {
            $m_car_types->add($_POST);
        }

        if (isset($_POST['save']))
        {
            $m_car_types->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_car_types()
    {
        $_dt = new Model_Cms_Dt_Cartypes();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id');
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }


    public function action_ajax_car_types()
    {

        $_dt = new Model_Cms_Plugin_Cartypes();

        switch ($this->param) {
            case 'del':
                $_dt->del($_POST['id']);
                break;
            case 'del_all':
                $_dt->del_all($_POST['ids']);
                break;
        }

        die;
    }


    public function action_bonuses()
    {
        $view = View::factory('cms/plugin/bonuses/v_bonuses');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_bonuses = new Model_Cms_Plugin_Bonuses();

        if (isset($_POST['save']))
        {
            $m_bonuses->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_bonuses()
    {
        $_dt = new Model_Cms_Dt_Bonuses();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id');
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }

    public function action_services()
    {
        $view = View::factory('cms/plugin/services/v_services');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_services = new Model_Cms_Plugin_Services();

        if (isset($_POST['save']))
        {
            $m_services->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_services()
    {
        $_dt = new Model_Cms_Dt_Services();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id');
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }


    public function action_parser()
    {
        $view = View::factory('cms/plugin/parser/v_parser');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m = new Model_Cms_Plugin_Parser();

        if (isset($_POST['save']))
        {
            $m->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_parser()
    {
        $_dt = new Model_Cms_Dt_Parser();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered();
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }
}