<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Notification extends Controller_Cms_Template {


    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $view = View::factory('cms/plugin/notification/v_index');
        $this->template->block_center = array($view);
    }

    public function action_add()
    {
        $view = View::factory('cms/plugin/notification/v_add');
        $this->template->block_center = array($view);
    }

    public function action_edit()
    {
        $view = View::factory('cms/plugin/notification/v_edit');
        $view->id = (int) $this->param;
        $this->template->block_center = array($view);
    }

    public function action_config()
    {
        $view = View::factory('cms/plugin/notification/v_config');
        $this->template->block_center = array($view);
    }
}