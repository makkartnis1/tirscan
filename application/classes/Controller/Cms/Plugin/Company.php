<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cms_Plugin_Company extends Controller_Cms_Template {

    public $access = 1;
    private $langs = null;

    public function before()
    {
        parent::before();
        if ($this->user['group_id'] > 3) $this->access = 0;
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_comments()
    {
        $view = View::factory('cms/plugin/company/v_comments');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_index()
    {
        $view = View::factory('cms/plugin/company/v_index');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_types = new Model_Cms_Plugin_CompanyTypes();
        $m_owner_types = new Model_Cms_Plugin_CompanyOwnerTypes();

        $view->types = $m_types->get_all();
        $view->owner_types = $m_owner_types->get_all();

        $view->register_times = [
            '<1m'=>'менше місяця',
            '>1m'=>'більше 1 місяця',
            '>2m'=>'більше 2 місяців',
            '>3m'=>'більше 3 місяців',
            '>6m'=>'більше 6 місяців',
            '>1y'=>'більше 1 року',
            '>2y'=>'більше 2 років',
            '>3y'=>'більше 3 років',
            '>5y'=>'більше 5 років'
        ];

        $view->foundation_times = [
            '<1m'=>'менше місяця',
            '>1m'=>'більше 1 місяця',
            '>2m'=>'більше 2 місяців',
            '>3m'=>'більше 3 місяців',
            '>6m'=>'більше 6 місяців',
            '>1y'=>'більше 1 року',
            '>2y'=>'більше 2 років',
            '>3y'=>'більше 3 років',
            '>5y'=>'більше 5 років',
            '>10y'=>'більше 10 років',
            '>15y'=>'більше 15 років',
            '>20y'=>'більше 20 років'
        ];

        $view->rating = [
            '1-2'=>'від 1 до 2',
            '2-3'=>'від 2 до 3',
            '3-4'=>'від 3 до 4',
            '4-5'=>'від 4 до 5',
            '5'=>'5'
        ];

        $filter = [];
        if (isset($_GET['goFilter']))
        {

            if (isset($_GET['ID']))
            {
                $ID = (int) $_GET['ID'];
                if ($ID)
                {
                    $filter['ID'] = $ID;
                }
            }

            if (isset($_GET['name']))
            {
                $name = $_GET['name'];
                if ($name)
                {
                    $filter['name'] = $name;
                }
            }


            if (isset($_GET['IPN']))
            {
                $IPN = $_GET['IPN'];
                if ($IPN)
                {
                    $filter['IPN'] = $IPN;
                }
            }

            if (isset($_GET['phone']))
            {
                $phone = $_GET['phone'];
                if ($phone)
                {
                    $filter['phone'] = $phone;
                }
            }


            if (isset($_GET['approvedByAdmin']))
            {
                $approvedByAdmin = $_GET['approvedByAdmin'];
                if ($approvedByAdmin !== '')
                {
                    $filter['approvedByAdmin'] = $approvedByAdmin;
                }
            }

            if (isset($_GET['typeID']))
            {
                $typeID = $_GET['typeID'];
                if ($typeID)
                {
                    $filter['typeID'] = $typeID;
                }
            }

            if (isset($_GET['ownershipTypeID']))
            {
                $ownershipTypeID = $_GET['ownershipTypeID'];
                if ($ownershipTypeID)
                {
                    $filter['ownershipTypeID'] = $ownershipTypeID;
                }
            }


            if (isset($_GET['registered']))
            {
                $registered = $_GET['registered'];
                if ($registered)
                {
                    $filter['registered'] = $registered;
                }
            }

            if (isset($_GET['foundationDate']))
            {
                $foundationDate = $_GET['foundationDate'];
                if ($foundationDate)
                {
                    $filter['foundationDate'] = $foundationDate;
                }
            }

            if (isset($_GET['rating']))
            {
                $rating = $_GET['rating'];
                if ($rating)
                {
                    $filter['rating'] = $rating;
                }
            }

            if (isset($_GET['dateCreate1']))
            {
                $dateCreate1 = $_GET['dateCreate1'];
                if ($dateCreate1)
                {
                    $filter['dateCreate1'] = $dateCreate1;
                }
            }

            if (isset($_GET['dateCreate2']))
            {
                $dateCreate2 = $_GET['dateCreate2'];
                if ($dateCreate2)
                {
                    $filter['dateCreate2'] = $dateCreate2;
                }
            }

            if (isset($_GET['places_from']))
            {
                if (!empty($_GET['places_from']))
                {
                    if (is_array($_GET['places_from']))
                    {
                        if (isset($_GET['places_from'][0]))
                        {
                            if ($_GET['places_from'][0])
                            {
                                $filter['places_from'] = $_GET['places_from'];
                                $filter['places_from_groups'] = [];
                                $m_geo = new Model_Cms_Geo();
                                foreach($filter['places_from'] as $place_group_ids)
                                {
                                    $filter['places_from_groups'][] = [
                                        'ids'=>explode(',',$place_group_ids),
                                        'name'=>$m_geo->getNameByIDs(explode(',',$place_group_ids))
                                    ];
                                }
                            }
                        }
                    }
                }
            }


        }

        $view->filter = $filter;

        $view->langs = $this->langs;
        $this->template->block_center = array($view);
    }


    public function action_db()
    {
        $_dt = new Model_Cms_Dt_Company();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,$_POST['filter']);
        $res['data'] = $_dt->get($_POST,$_POST['filter']);
        echo json_encode($res);
        die;
    }

    public function action_add()
    {
        $view = View::factory('cms/plugin/company/v_add');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }

    public function action_edit()
    {
        $view = View::factory('cms/plugin/company/v_edit');
        $view->id = (int) $this->param;
        if (!$this->access) $view = View::factory($this->get_access_denied_view());
        $this->template->block_center = array($view);
    }



    public function action_owner_types()
    {
        $view = View::factory('cms/plugin/company/v_owner_types');
        if (!$this->access) $view = View::factory($this->get_access_denied_view());

        $m_company_owner_types = new Model_Cms_Plugin_CompanyOwnerTypes();
        if (isset($_POST['add_new']))
        {
            $m_company_owner_types->add($_POST);
        }

        if (isset($_POST['save']))
        {
            $m_company_owner_types->save($_POST,$_POST['id']);
        }

        $this->template->block_center = array($view);
    }



    public function action_db_owner_types()
    {
        $_dt = new Model_Cms_Dt_CompanyOwnerTypes();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countFiltered($_POST,'id');
        $res['data'] = $_dt->get($_POST);
        echo json_encode($res);
        die;
    }


    public function action_ajax_owner_types()
    {

        $_dt = new Model_Cms_Plugin_CompanyOwnerTypes();

        switch ($this->param) {
            case 'del':
                $_dt->del($_POST['id']);
                break;
            case 'del_all':
                $_dt->del_all($_POST['ids']);
                break;
        }

        die;
    }

    public function action_ajax()
    {
        $m = new Model_Cms_Plugin_Company();
        switch ($_POST['action']) {
            case 'delete':
                $m->delete($_POST['id']);
                break;
            case 'multi_delete':
                foreach($_POST['ids'] as $id)
                {
                    $m->delete($id);
                }
                break;
        }

        die;
    }


}