<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Parser_Search extends Controller_Parser_Base {

//    public $model;
    public $lardiHost = 'https://lardi-trans.com';

    protected $company_arr = array(
        'Наименование:' => 'name',
        'Найменування:' => 'name',
        'Код фирмы:' => 'companyID',
        'Код фірми:' => 'companyID',
        'Страна:' => 'country',
        'Країна:' => 'country',
        'Область:' => 'region',
        'Область:' => 'region',
        'Город:' => 'town',
        'Місто:' => 'town',
        'Адрес:' => 'address',
        'Адреса:' => 'address',
        'Псевдоним:' => 'nameInternational',
        'Стан:' => 'nick',
        'Веб-сторінка:' => 'webPage',
        'Веб-страница:' => 'webPage',
        'Псевдоним:' => 'nameInternational',
        'Псевдонім:' => 'nameInternational',
        'Профиль:' => 'profile',
        'Профіль:' => 'profile',
        'Ліцензія:' => 'license',
        'Лицензия:' => 'license',
        'Состояние:' => 'stan',
        'Стан:' => 'stan',
        'Членство АсМАП:' => 'license',
        'Членство АсМАП:' => 'license',
        'Рейтинг надежности:' => 'rating',
        'Рейтинг надійності:' => 'rating',
        '"Бал надійності":' => 'rating',
        '"Балл надежности":' => 'rating',
        'Від імені фірми дано:' => 'rating',
        'От имени фирмы дано:' => 'rating',
        'На фирму дано:' => 'rating',
        'На фірму дано:' => 'rating',
        'По грузам:' => 'rating',
        'По вантажам:' => 'rating',
        'По транспорту:' => 'rating',
        'По транспорту:' => 'rating',
        'Сообщения:' => 'rating',
        'Повідомлення:' => 'rating',
        'Темы:' => 'rating',
        'Теми:' => 'rating',
        'Дата регистрации на Ларди-Транс:' => 'dateRegistration',
        'Дата реєстрації на Ларді-Транс:' => 'dateRegistration',
        'Дата последнего посещения:' => 'dateLastLogin',
        'Дата останнього візиту:' => 'dateLastLogin',
        'Клиент Ларди-Транс:' => 'lardyClient',
        'Клієнт Ларді-Транс:' => 'lardyClient',
        ' ' => 'space',
    );

    protected $gruz_arr = array(
        '№ заявки' => 'id',
        'Дата размещения:' => 'add_date',
        'Дата повтора:' => 'update_date',
        'Комментариев:' => 'comments',
        'Откуда' => 'from',
        'Куда' => 'to',
        'Расстояние' => 'distance',
        'Дата загрузки' => 'dateFrom',
        'Тип кузова' => 'transportTypeID',
        'Груз' => 'cargo',
        'Масса' => 'weigth',
        'Оплата' => 'cost',
        'Объем' => 'volume',
        'Загрузка' => 'loadFrom',
        'Примечание' => 'info',
        'Габариты груза' => 'size',
        'Количество авто' => 'autoCount',
        'ADR' => 'adr',//
        'Разрешения' => '',//мед книжка 217628515
    );

    protected $trans_arr = array(
        '№ заявки' => 'id',
        'Дата размещения:' => 'add_date',
        'Дата повтора:' => 'update_date',
        'Комментариев:' => 'comments',
        'Откуда' => 'from',
        'Куда' => 'to',
        'Дата загрузки' => 'dateFrom',
        'Тип кузова' => 'transportTypeID',//Зерновоз
        'Количество авто' => 'autoCount',
        'Примечание' => 'info',//Самосвал
        'Грузоподъемность' => 'weigth', //54324051
        'Загрузка' => 'loadFrom', //53715693
        'Габариты кузова' => 'carSize', //53762043
        'Разрешения' => '', //54198313, CMR
        'Оплата' => 'cost', //28756632, нал.
        'Расстояние' => 'distance', //54315697
        'ADR' => 'adr',//54003823
    );

    protected $typeID = array(
        'Складські та експедиторські послуги' => 1,
        'Транспортні послуги' => 2,
        'Складские и экспедиторские услуги' => 1,
        'Транспортные услуги' => 2,
    );

    protected $prymary_locale = array(
        'Росія' => 2,
        'Россия' => 2,
    );

    protected $ru_month = array( 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря', 'Cегодня' );

    protected $ua_month = array( 'січня', 'лютого', 'березня', 'квітня', 'травня', 'червня', 'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня', 'Сьогодні' );

    protected $en_month = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'today' );

    protected $sizeArray = array(
        'sizeX' => 'д:',
        'sizeY' => 'ш:',
        'sizeZ' => 'в:',
    );

    protected $loadFrom = array(
        'боковая' => 'loadFromSide',
        'задняя' => 'loadFromBehind',
        'верхняя' => 'loadFromTop',
        'со снятием поперечин' => 'loadTent',
        'со снятием стоек' => 'loadTent',
        'с полной растентовкой' => 'loadTent',
        'без ворот' => 'loadTent',
    );

    protected $any_city = ['любой', 'любий'];

    protected $dateTo = 30;//date to = date from + dateTo

    public $defaultFrom = 'Україна';

    public $defaultTo = 'Україна';

    public function before()
    {
        parent::before();
//        $model = new Model_Parser_Parser();
//        die;
    }

    public function action_index()
    {
		header('Content-Type: text/html; charset=utf-8');
//    	die;
    	set_time_limit(300);
//        if($_SERVER['REMOTE_ADDR'] !== $_SERVER['SERVER_ADDR']) {
//        if($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']) {
			$model = new Model_Parser_Parser();
			$countries = $model->getCountryPars();
			echo '<pre>';
			foreach ($countries as $countryFrom) {
				foreach ($countries as $countryTo) {
					var_dump('start ' . time());
//					echo 'country ' . $countryFrom['short_name'] . '-' . $countryTo['short_name'] . PHP_EOL;
					$this->searchGruz($countryFrom['short_name'], $countryTo['short_name']);
					$this->searchTrans($countryFrom['short_name'], $countryTo['short_name']);
					var_dump('finish ' . time());
				}
			}
	//        $this->getGruz(217572047);
	//        var_dump($this->ru_date('18 июля 2012 18:21'));
	//        var_dump($this->ru_date('10 апреля 88:16'));
	//        var_dump($this->ru_date('Cегодня 09:04 + 5 days'));
	//        var_dump($this->ru_date(explode('-', '17 апреля ')[0]));
	//        var_dump($this->ru_date(explode('-', '17 апреля - 18 апреля')[0]));
	//        $this->getTrans(54075935);
	//        $this->auth();
	//        $this->getCompanyInfo('https://uk.lardi-trans.com/u/tm-trans');
	//        $this->getCompanyInfo('https://uk.lardi-trans.com/user/10426957091');
	//        var_dump($this->getCompanyId('/user/15381867702'));
	//        var_dump($api->usersFirmInfo($this->sig, '16882522359'));
	//        var_dump($this->loadPageContent('http://api.lardi-trans.com/api/?method=users.firm.info&sig='.$this->sig.'&uid=34543'));
			echo '</pre>';
//        } else {
//            throw new Kohana_HTTP_Exception_404;
//        }
    }

    public function action_test()
	{
		echo '<pre>';
//		$filename = DOCROOT . 'test.html';
//		var_dump($filename);
//		$file = fopen($filename, 'r');
//		$content = fread($file, filesize($filename));
//		fclose($file);
//		$content = $this->loadPageContent('https://lardi-trans.com/gruz/?predl_tip=1&countryfrom=UA&countryto=UA&areafrom=0&areato=0&bt_chbs_slc=&dayfrom=&monthfrom=&yearfrom=&mass2=&value2=&startSearch=%D0%9F%D0%BE%D0%B8%D1%81%D0%BA')['content'];
//		$content = $this->loadPageContent('https://lardi-trans.com/gruz/?predl_tip=1&countryfrom=UA&countryto=UA&areafrom=0&areato=0&bt_chbs_slc=&dayfrom=&monthfrom=&yearfrom=&mass2=&value2=&startSearch=%D0%9F%D0%BE%D0%B8%D1%81%D0%BA')['content'];
//		$content = $this->loadPageContent('https://lardi-trans.com/trans/?countryfrom=UA&countryto=UA&areafrom=-1&areato=-1&cityFrom=&bodyType=0&adr=-1&dayfrom=-1&monthfrom=-1&yearfrom=-1&dayto=-1&monthto=-1&yearto=-1&mass=0.0&mass2=0.0&value=0.0&value2=0.0&refresh_time=&userInputCaptchaText=yUctF&userInputCaptchaId=Xz0O9aHp&enterCaptcha=%D0%9F%D1%80%D0%BE%D0%B4%D0%BE%D0%BB%D0%B6%D0%B8%D1%82%D1%8C')['content'];
		$content = $this->loadPageContent('https://lardi-trans.com/trans/?countryfrom=UA&countryto=UA&areafrom=-1&areato=-1&cityFrom=&bodyType=0&adr=-1&dayfrom=-1&monthfrom=-1&yearfrom=-1&dayto=-1&monthto=-1&yearto=-1&mass=0.0&mass2=0.0&value=0.0&value2=0.0&refresh_time=&startSearch=1')['content'];
		var_dump($content);
		$contentDom = $this->toDOMXPath($content);
		$captchaDom = $contentDom->query('//*[@id="predlFilterCont"]/table/tbody/tr[11]/td[1]/span/div/table/tbody/tr[2]/td/img');
		var_dump('captcha length = ' . $captchaDom->length);
//		var_dump('captcha length = ' . $captchaDom->item(0)->getAttribute('src'));
//		var_dump('captcha length = ' . $captchaDom->item(1)->getAttribute('src'));
//		var_dump('captcha length = ' . $captchaDom->item(2)->getAttribute('src'));


		echo '</pre>';
		return false;
	}

    public function searchGruz($country_from = 'UA', $country_to = 'UA', $pages = 5)
    {
        for($page = 1; $page <= $pages; $page++) {
            $pageHTML = $this->loadPageContent($this->lardiHost . '/gruz/?countryfrom=' . $country_from . '&countryto=' . $country_to . '&areafrom=-1&areato=0&cityFrom=&cityTo=&truck=0&tir=-1&custom_control=-1&dayfrom=-1&monthfrom=-1&yearfrom=-1&dayto=-1&monthto=-1&yearto=-1&mass=0.0&mass2=0.0&value=0.0&value2=0.0&showType=all&refresh_time=&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&page=1&p='.$page, true);
//            var_dump($pageHTML);
            $pageDom = $this->toDOMXPath($pageHTML['content']);
            $xpath = '//*[@id="gtsearch_result_table"]/tbody/tr/td[1]/input';
            $xpath = '//tr/td[2]/span/input';
            $gruzDom = $pageDom->query($xpath);
            $xpath = '//*[@id="gtsearch_result_table"]/tbody/tr/td[7]/a[1]';
            $xpath = '//tr/td[10]/table/tbody/tr/td[2]/div[2]/a';

			////////////*[@id="page_container"]/tbody/tr[2]/td/div[6]/table/tbody/tr/td[10]/table/tr/td[2]/div[2]/a

            $xpath = '//table/tr[2]/td[1]/div[6]/table/tbody/tr/td[10]/table/tr/td[2]/div[1]/a';
            $xpath = '//*[@id="page_container"]/tbody/tr[2]/td/div[6]/table/tbody/tr/td[10]/table/tr/td[2]/div[1]/a';
            $xpath = '//tr/td/table/tr/td[2]/div[1]/a[1]'; // непровірені
            $xpath = '//tr/td/table/tr/td[2]/div[count(*)=1 and not(@id)]/a[1]'; // провірені
            $companyDom = $pageDom->query($xpath);
            var_dump('gruz count: ');
            var_dump($gruzDom->length);
            var_dump('company count: ');
            var_dump($companyDom->length);
			if($gruzDom->length == 0 && $companyDom->length == 0)
			{
				var_dump($pageHTML);
				die;
			}
			if($companyDom->length < 1)
			{
				var_dump($pageHTML);
				$this->action_auth();
				die;
				break;
			} else {
//				die('dkcdjc');
			}
//			die('here');
			for($company  = 0; $company < $companyDom->length; $company++)
            {
                $companyId = $this->getCompanyId($companyDom->item($company)->getAttribute('href'));
                if($companyId) {
                    var_dump('gruzzzz: ');
                    var_dump($gruzDom->item($company)->getAttribute('value'));
                    $gruzId = $this->getGruz($gruzDom->item($company)->getAttribute('value'));

					if(!$gruzId){
						return false;
					}
                }
            }

        }
        return true;
    }

    public function getGruz($gruzId)
    {
        $model = new Model_Parser_Parser();
        try {
            if(count($model->getGruzByParser($gruzId)))
            {
                return false;
            } else {
                $gruz = $this->loadPageContent($this->lardiHost . '/gruz/view/' . $gruzId);
                $gruz_dom = $this->toDOMXPath($gruz['content']);
                $xpath = '//div/table/tr/td/div/img';
                $uId = $gruz_dom->query($xpath);
				if($uId->length) {
					$uId = $uId->item(0)->getAttribute('src');
				} else {
					return false;
					var_dump($gruz);
					die;
				}
                $uId = explode('/', $uId)[3];
                var_dump($uId);
                $xpath = '//*[@id]/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td[2]';
                $gruzValue = $gruz_dom->query($xpath);
                $xpath = '//*[@id]/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td[1]';
                $gruzKey = $gruz_dom->query($xpath);
                $gruzInfo = [];
                for ($item = 0; $item < $gruzValue->length; $item++) {
                    $key = $this->gruz_arr[$gruzKey->item($item)->nodeValue];
                    if ($key == 'weigth') {
                        if (strpos($gruzValue->item($item)->nodeValue, 'м')) {
                            $gruzInfo['volume'] = floatval($gruzValue->item($item)->nodeValue);
                        } else {
                            $gruzInfo[$key] = floatval($gruzValue->item($item)->nodeValue);
                        }
                    } elseif ($key == 'add_date' || $key == 'update_date') {
                        $gruzInfo[$key] = $this->ru_date($gruzValue->item($item)->nodeValue);
                    } elseif ($key == 'dateFrom') {
                        $value = explode('-', $gruzValue->item($item)->nodeValue)[0];
                        $gruzInfo[$key] = $this->ru_date($value);
                        $gruzInfo['dateTo'] = $this->ru_date($value . '+ ' . $this->dateTo . ' days');
                    } elseif ($key == 'loadFrom') {
                        $gruzInfo[$key] = $this->getLoadType($gruzValue->item($item)->nodeValue);
                    } else {
                        $gruzInfo[$key] = $gruzValue->item($item)->nodeValue;
                    }
                }
                if (!Arr::get($gruzInfo, 'loadFrom', false)) {
                    $gruzInfo['loadFrom'] = [];
                }
                if (Arr::get($gruzInfo, 'size', false)) {
                    $gruzInfo['size'] = $this->getSize(Arr::get($gruzInfo, 'size'));
                }
                $userTransportId = $model->getUserTransportType(Arr::get($gruzInfo, 'transportTypeID', 'Тент'));
                if (!count($userTransportId)) {
                    $userTransportTypeId = $model->insertUserTransportType(Arr::get($gruzInfo, 'transportTypeID', 'Тент'))[0];
                } else {
                    $userTransportTypeId = $userTransportId[0]['ID'];
                }
                $gruzInfo['transportTypeID'] = $userTransportTypeId;
                $gruzInfo['priceId'] = $model->insertPrice()[0];
                $gruzInfo['id'] = $gruzId;
                $userId = $model->findUserByParser($uId);
                var_dump($userId);
                if (count($userId)) {
                    $userId = $userId[0]['ID'];
                } else {
                    return false;
                }
                $gruzReturn = $model->insertGruz($gruzInfo, $userId);
                if ($gruzReturn) {
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => explode('(', $gruzInfo['from'])[0]]));
                    if(mb_strlen($idsFrom['content']) == 0) {
                        $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => $this->defaultFrom]));
                    }
                    var_dump('ids from');
                    var_dump($idsFrom);
                    $model->insertGeoToGruz($idsFrom['content'], $gruzReturn[0]);
                    $idsTo = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => explode('(', $gruzInfo['to'])[0]]));
                    if(mb_strlen($idsTo['content']) == 0) {
                        $idsTo = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => $this->defaultTo]));
                    }
                    var_dump('ids to');
                    var_dump($idsTo);
                    $model->insertGeoToGruz($idsTo['content'], $gruzReturn[0], 'to');
                    return $gruzReturn;
                } else {
                    return false;
                }
            }
        } catch(ErrorException $e){
            return false;
        }
    }

    public function searchTrans($country_from = 'UA', $country_to = 'UA', $pages = 5)
    {
        for($page = 1; $page <= $pages; $page++) {
            $pageHTML = $this->loadPageContent($this->lardiHost . '/trans/?countryfrom='.$country_from.'&countryto='.$country_to.'&areafrom=0&areato=0&cityFrom=&bodyType=0&adr=-1&dayfrom=-1&monthfrom=-1&yearfrom=-1&dayto=-1&monthto=-1&yearto=-1&mass=0.0&mass2=0.0&value=0.0&value2=0.0&refresh_time=&startSearch=Сделать+выборку&page=1&p='.$page);
//			var_dump($pageHTML);
            $pageDom = $this->toDOMXPath($pageHTML['content']);
            $xpath = '//*[@id="gtsearch_result_table"]/tbody/tr/td[1]/input';
			$xpath = '//tr/td[2]/span/input';
            $transDom = $pageDom->query($xpath);
            $xpath = '//*[@id="gtsearch_result_table"]/tbody/tr/td[7]/a[1]';
			$xpath = '//tr/td/table/tr/td[2]/div[count(*)=1 and not(@id)]/a[1]'; // провірені
            $companyDom = $pageDom->query($xpath);
            var_dump('transport count: ');
            var_dump($transDom->length);
			var_dump('company count');
			var_dump($companyDom->length);
			if($transDom->length == 0 && $companyDom->length == 0)
			{
				var_dump($pageHTML);
				die;
			}
			if($companyDom->length < 1)
			{
				var_dump($pageHTML);
				$this->action_auth();
				die;
				break;
			}
//			die;
            for($company = 0; $company < $companyDom->length; $company++)
            {
                $companyId = $this->getCompanyId($companyDom->item($company)->getAttribute('href'));
                if($companyId) {
                    var_dump('gruzzzz: ');
                    var_dump($transDom->item($company)->getAttribute('value'));
                    $transId = $this->getTrans($transDom->item($company)->getAttribute('value'));
					if(!$transId) return false;
                } else {
//                    return '';
                }
            }

        }
        return true;
    }

    public function getTrans($transId, $country_from = 'UA', $country_to = 'UA')
    {
        $model = new Model_Parser_Parser();
        if(count($model->getTransportByParser($transId)))
        {
            return false;
        } else {
            $trans = $this->loadPageContent($this->lardiHost . '/trans/view/' . $transId);
            $trans_dom = $this->toDOMXPath($trans['content']);
            $xpath = '//div/table/tr/td/div/img';
            $uId = $trans_dom->query($xpath);
			if($uId->length) {
				$uId = $uId->item(0)->getAttribute('src');
			} else {
				return false;
				var_dump($gruz);
				die;
			}
            $uId = explode('/', $uId)[3];
            var_dump($uId);
            $xpath = '//*[@id]/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td[2]';
            $transValue = $trans_dom->query($xpath);
            $xpath = '//*[@id]/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td[1]';
            $transKey = $trans_dom->query($xpath);
            $transInfo = [];
            for ($item = 0; $item < $transValue->length; $item++) {
                $key = $this->trans_arr[$transKey->item($item)->nodeValue];
                if ($key == 'weigth') {
                    if (strpos($transValue->item($item)->nodeValue, 'м')) {
                        $transInfo['volume'] = floatval($transValue->item($item)->nodeValue);
                    } else {
                        $transInfo[$key] = floatval($transValue->item($item)->nodeValue);
                    }
                } elseif ($key == 'add_date' || $key == 'update_date') {
                    $transInfo[$key] = $this->ru_date($transValue->item($item)->nodeValue);
                } elseif ($key == 'dateFrom') {
                    $value = explode('-', $transValue->item($item)->nodeValue)[0];
                    $transInfo[$key] = $this->ru_date($value);
                    $transInfo['dateTo'] = $this->ru_date($value . '+ ' . $this->dateTo . ' days');
                } elseif ($key == 'loadFrom') {
                    $transInfo[$key] = $this->getLoadType($transValue->item($item)->nodeValue);
                } else {
                    $transInfo[$key] = $transValue->item($item)->nodeValue;
                }
            }
            if (!Arr::get($transInfo, 'loadFrom', false)) {
                $transInfo['loadFrom'] = [];
            }
            if (Arr::get($transInfo, 'carSize', false)) {
                $transInfo['carSize'] = $this->getSize(Arr::get($transInfo, 'carSize'));
            }
            $transInfo['id'] = $transId;
            var_dump($uId);
            $userId = $model->findUserByParser($uId);
            var_dump($userId);
            if (count($userId)) {
                $userId = $userId[0]['ID'];
                $userTransport = $model->findUserTransport($userId);
                if (count($userTransport)) {
                    $transInfo['userCarId'] = $userTransport[0]['ID'];
                } else {
                    $userTransportId = $model->getUserTransportType(Arr::get($transInfo, 'transportTypeID', 'Тент'));
                    if (!count($userTransportId)) {
                        $userTransportTypeId = $model->insertUserTransportType(Arr::get($transInfo, 'transportTypeID', 'Тент'))[0];
                    } else {
                        $userTransportTypeId = $userTransportId[0]['ID'];
                    }
                    $transInfo['userCarId'] = $model->insertUserTransport($transInfo, $userId, $userTransportTypeId)[0];
                }
                $transInfo['priceId'] = $model->insertPrice()[0];
                $transportReturn = $model->insertTransport($transInfo, $userId);
                if($transportReturn)
                {
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => explode('(', $transInfo['from'])[0]]));
                    if(strlen($idsFrom['content']) === 0) {
                        $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => $this->defaultFrom]));
                    }
                    $model->insertGeoToTransport($idsFrom['content'], $transportReturn[0]);
                    $idsTo = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => explode('(', $transInfo['to'])[0]]));
                    if(strlen($idsTo['content']) === 0) {
                        $idsTo = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => $this->defaultTo]));
                    }
                    $model->insertGeoToTransport($idsTo['content'], $transportReturn[0], 'to');
                    return $transportReturn;
                } else {
                    return false;
                }


            } else {
                return false;
            }
        }

        return false;
    }

    public function getSize($sizeString)
    {
        $size = [];
        foreach($this->sizeArray as $key => $value)
        {
            $strPos = strpos($sizeString, $value);
            if($strPos !== false)
            {
                $strTmp = substr($sizeString, $strPos + 3);
                $size[$key] = floatval($strTmp);
            }
        }
        return $size;
    }

    public function getCompanyId($companyUrl, $country_to = 'UA')
    {
        $model = new Model_Parser_Parser();
        try {
            $companyParsID = explode('/', $companyUrl);
            $companyParsID = end($companyParsID);
            $companyID = $model->get_company_by_url($companyParsID);
            if (!count($companyID)) {
                $company_ru = $this->getCompanyInfo('https://ru.lardi-trans.com' . $companyUrl, 'ru');
                $company_ru['lang'] = 2;
                $company_ru['companyParsID'] = $companyParsID;
                $company_ua = $this->getCompanyInfo('https://uk.lardi-trans.com' . $companyUrl, 'ua');
                $company_ua['lang'] = 1;
                $company_ua['companyParsID'] = $companyParsID;
                var_dump($company_ru);
                var_dump($company_ua);
                if ($company_ua['primaryLangID'] == 1) {
                    $companyID = $model->save_company($company_ua)[0];
                    $this->save_contact($company_ua, $company_ru, $companyID);
                } else {
                    $companyID = $model->save_company($company_ru)[0];
                    $this->save_contact($company_ru, $company_ua, $companyID);
                }
                $model->save_company_locale($company_ua, $companyID);
                $model->save_company_locale($company_ru, $companyID);
                if(Arr::get($company_ru, 'town', false))
                {
                    var_dump('town');
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => Arr::get($company_ru, 'town', false)]));
                    var_dump($idsFrom);
                    $model->insertGeoToCompany($idsFrom['content'], $companyID);
                } elseif(Arr::get($company_ru, 'region', false)) {
                    var_dump('region');
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => Arr::get($company_ru, 'region', false)]));
                    var_dump($idsFrom);
                    $model->insertGeoToCompany($idsFrom['content'], $companyID);
                } elseif(Arr::get($company_ru, 'country', false)) {
                    var_dump('country');
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => Arr::get($company_ru, 'country', false)]));
                    var_dump($idsFrom);
                    $model->insertGeoToCompany($idsFrom['content'], $companyID);
                } else {
                    var_dump('else');
                    $idsFrom = $this->loadPageContent('http://tirscan.com/cms/geo/uk/get_geo_ids?' . http_build_query(['name' => $this->defaultFrom]));
                    $model->insertGeoToCompany($idsFrom['content'], $companyID);
                }

            } else {
                $companyID = $companyID[0]['id'];
            }

            return $companyID;
        } catch (ErrorException $e) {
            return false;
        }
    }

    public function getCompanyInfo($url, $lang = 'ua')
    {
        var_dump($url);
        $model = new Model_Parser_Parser();
        $count = 0;
        do {
            if($count > 5) return false;
            $count++;
            $company_html = $this->loadPageContent($url);
        }while($company_html['http_code'] == 302 || $company_html['http_code'] == 0);
        //довбу поки не получу дані п.с. часто буває шо редірект непонятно куди, або взагалі 0
        //follow redirect у нас на сервері не канає, там шота адмін нашаманив, уже з пів року не робить
//        if($company_html['http_code'] == 302)
//        {
//            echo '302';
//            return NULL;
//        }
        $company_dom = $this->toDOMXPath($company_html['content']);
        $xpath = '//*[@id="page_container"]/tr[2]/td/table/tr/td[2]/table[2]/tr/td[@class="profile_container profile_container_l"]/table[1]/tbody/tr/td/div/table/tbody/tr/td';
        $t_rows = $company_dom->query($xpath);
        $company_info = array();
        for($i = 0; $i < $t_rows->length; $i = $i+2)
        {
            //якщо тут помилка то нада додати ключ якого не знайшло в $company_arr, на двох мовах, по аналогії як уже є
            $company_info[$this->company_arr[$t_rows->item($i)->nodeValue]] = $t_rows->item($i+1)->nodeValue;
        }
        $company_info['dateRegistration'] = $this->ru_date($company_info['dateRegistration'], $lang);
        $company_info['primaryLangID'] = Arr::get($this->prymary_locale, $company_info['country'], 1);
        if(!isset($company_info['name']))
        {
            var_dump($company_html);
            echo $company_html['content'];
        }
        var_dump($company_info);
        $company_info['owner_type'] = explode(' ', $company_info['name'])[0];
        $company_info['owner_type_id'] = $model->get_owner_type_id($company_info['owner_type']);
        if(count($company_info['owner_type_id']))
        {
            $company_info['owner_type_id'] = $company_info['owner_type_id'][0]['id'];
        } else {
            $company_info['owner_type_id'] = $model->insert_owner_type($company_info['owner_type'])[0];
        }
        $xpath = '//*/div[@class="profile_info"]/span';
        $profile_dom = $company_dom->query($xpath);
        unset($company_info['profile']);
        for($i = 0; $i < $profile_dom->length; $i++)
        {
            $company_info['profile'][] = $profile_dom->item($i)->nodeValue;
//            $model->insert_ignore_company_type($profile_dom->item($i)->nodeValue, $lang);
        }
        foreach($this->typeID as $key => $value)
        {
            if(isset($company_info['profile'])) {
                if (in_array($key, $company_info['profile'])) {
                    $company_info['typeID'] = $value;
                    break;
                }
            } else {
                $company_info['typeID'] = 3;
            }
        }
        $xpath = '//*/div[@class="widgetDivDescription"]/text()';
        $description_dom = $company_dom->query($xpath);
        $company_info['description'] = '';
        for($i = 0; $i < $description_dom->length; $i++)
        {
            $company_info['description'] .= $description_dom->item($i)->nodeValue . '<br>';
        }
        $xpath = '//*[@id="page_container"]/tr[2]/td/table/tr/td[2]/table[2]/tr[2]/td[1]/div/table[1]/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr[not(@class="contInfoOtdelRow")]/td';

        $t_rows = $company_dom->query($xpath);
        for($i = 0; $i < ($t_rows->length - 3);$i+=6)
        {
            $contact = array();
            $contact['name'] = $t_rows->item($i)->nodeValue;
            $uIdXpath = $t_rows->item($i)->getNodePath() . '/table/tr/td[1]/div/div/div/div/img';
            $uId = $company_dom->query($uIdXpath);
            $uId = $uId->item(0)->getAttribute('src');
            $contact['parser'] = explode('/', $uId)[3];
            $xpath = $t_rows->item($i+2)->getNodePath() . '/div/img';
            $email_dom = $company_dom->query($xpath);
            if($email_dom->length) {
                $contact['email'] = $email_dom->item(0)->getAttribute('src');
            } else {
                $contact['email'] = '';
            }
            $contact['icq'] = $t_rows->item($i+3)->nodeValue;
            $contact['skype'] = $t_rows->item($i+4)->nodeValue;
            $phones = explode('+', $t_rows->item($i+1)->nodeValue);
            unset($phones[0]);
//            var_dump($phones);die;
            foreach($phones as &$phone)
            {
                $phone = str_replace('(', '', $phone);
                $phone = str_replace(')', '', $phone);

//                $ph['cod'] = '+' . substr($phone, 0, 3);
//                $ph['phone'] = substr($phone, 3);
//
//                var_dump($ph);
//
//                var_dump($phone);die;
//                $ph['cod'] = '+' . $this->parseThis($phone, '+', '(');
                $ph['cod'] = '+' . substr($phone, 0, 3);
                $ph['codID'] = $model->phone_code_id($ph['cod']);
                if(!count($ph['codID']))
                {
                    $ph['codID'] = $model->insert_phone_codes($ph['cod'])[0];
                } else {
                    $ph['codID'] = $ph['codID'][0]['id'];
                }
//                $ph['phone'] = '(' . $this->parseThis($phone, '(', '.');
                $ph['phone'] = substr($phone, 3);
                $contact['phone'][] = $ph;
            }
            $company_info['contact'][] = $contact;
        }
//        var_dump($company_info['contact']);

        return $company_info;
    }

    public function action_getGruzTemp()
    {
        $model = new Model_Parser_Parser();
        $gruz = $this->loadPageContent('https://lardi-trans.com/gruz/?foi=&filter_marker=new&countryfrom=UA&countryto=UA&cityFrom=&cityTo=&dateFrom=&dateTo=&bt_chbs_slc=&mass=&mass2=&value=&value2=&gabDl=&gabSh=&gabV=&zagruzFilterId=&adr=-1&showType=all&startSearch=%D0%A1%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C+%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D1%83&page=1&fi=4f6eb2b3-fb6e-415c-bf6c-c55a6e1fc742');
        $gruz_dom = $this->toDOMXPath($gruz['content']);
        $xpath = '//*/table/tbody/tr/td/span';
        $t_rows = $gruz_dom->query($xpath);

        $arr = array();

        for($i = 48; $i<$t_rows->length; $i++)
        {
            $arr['countries'] = $t_rows->item($i)->nodeValue;
            $arr['transport'] = $t_rows->item($i+1)->nodeValue;
            $arr['time'] = $t_rows->item($i+2)->nodeValue;
            $arr['date'] = $t_rows->item($i+3)->nodeValue;
            $countries = explode('–',$t_rows->item($i+4)->nodeValue);
            $arr['country_from'] = Arr::get($countries,0);
            $arr['country_where'] = Arr::get($countries,1);
            $arr['gruz'] = $t_rows->item($i+5)->nodeValue;
            $arr['payment'] = $t_rows->item($i+6)->nodeValue;
            $arr['contacts'] = $t_rows->item($i+7)->nodeValue;

            echo'<br>'."[i = $i]";
//            echo $t_rows->item($i)->nodeValue;
            print_r($arr);
            echo'<br>'.'db insert status:  ';
            print_r($model->insertGruzTEMP($arr));
            break;
        }
//        var_dump($t_rows);
    }

    public function get_place_id($city)
    {
        $request = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' . $city . '&key=' . $this->map_api_key;
        $data_region = $this->loadPageContent($request);
        $data_region = json_decode($data_region, true);
        return $data_region['predictions'][0]['place_id'];
    }

    /**
     * @param string $date
     * @param string $lang
     * @return string
     */
    public function ru_date($date, $lang = 'ru')
    {
        $lang = $lang.'_month';
        $date = preg_replace('/(.\d):(.\d)/i', '', $date);

        return Date::formatted_time(str_replace( $this->$lang, $this->en_month, $date ), 'Y-m-d');
    }

    public function save_contact($primary_lang, $another_lang, $companyID)
    {
        $model = new Model_Parser_Parser();
        $primary_lang_id = $primary_lang['primaryLangID'];
        $first_lang_id = $primary_lang['lang'];
        $second_lang_id = $another_lang['lang'];
        foreach($primary_lang['contact'] as $key => $contact)
        {
            if($key == 0)
            {
                $vlasnick = $model->save_contact($contact, $primary_lang_id, $companyID);
                $userID = $vlasnick[0];
            } else {
                $userID = $model->save_contact($contact, $primary_lang_id, $companyID)[0];
            }
            $model->save_contact_locale($contact, $first_lang_id, $userID);
            $model->save_contact_locale($primary_lang['contact'][$key], $second_lang_id, $userID);
        }

        $model->user_to_company($vlasnick[0], $companyID);
    }

    private function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }

    public function action_auth()
    {
        //auth
		echo '<pre>';
		echo 'auth';
        $post = 'loginData='.parent::AUTH_USER.'&password='.parent::AUTH_PASS_NOT_HASH.'&remember=true&_remember=on&deviceInfo=';
        $auth = $this->loadPageContent('https://lardi-trans.com/accounts/login', true, $post);
        var_dump($auth);
		$url = Arr::get($auth, 'redirect_url', false);
		var_dump($url);
		if($url)
			var_dump($this->loadPageContent($url));
		echo '</pre>';
        //end auth
    }

    public function getLoadType($load)
    {
        $returnTypes = [];
        $loadTypes = explode(',', $load);
        foreach($loadTypes as $key => $loadType)
        {
            $returnTypes[$this->loadFrom[trim($loadType)]] = 'yes';
        }
        return $returnTypes;
    }

}
