<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Parser_Api працює з апі, конвертує хмл та об'єкти з результатом в масиви.
 */
class Controller_Parser_Api extends Controller_Parser_Base {


    public function before()
    {
        parent::before();
    }

    /**
     * http://api.lardi-trans.com/doc/get.payment.valuta.ref
     * Возвращает список допустимых валют
     * @return array
     */
    public function action_get_payment_valuta_ref()
    {
        $api = new Lardiapi();
        $page = $this->loadPageContent("http://api.lardi-trans.com/api/?method=get.payment.valuta.ref&sig=$this->sig");

        $xmlobj = simplexml_load_string($page['content']);
        $arr = $this->object_to_array($xmlobj);
        var_dump($arr);

        return $arr;

    }

    /**
     * http://api.lardi-trans.com/doc/base.country
     * Возвращает список стран, если у страны есть связанные области то вместе с областями.
     * @return array
     */
    public function action_base_country()
    {
        $api = new Lardiapi();
        $arr = $this->object_to_array($api->baseCountry($this->sig));
        var_dump($arr);

        return $arr;
    }

    /**
     * http://api.lardi-trans.com/doc/distance.search
     * Возвращает список городов с подробной информациейname,name2,name3,name4 - Варианты названия
     * country - Страна
     * country_sign - Сокращение страны.area - Область.area_id - № области.
     * @return array
     */
    public function action_distance_search($name)
    {
        $api = new Lardiapi();
        $arr = $this->object_to_array($api->distanceSearch($this->sig,$name));
        var_dump($arr);
        return $arr;
    }

    /**
     * http://api.lardi-trans.com/doc/base.auto_tip
     * Возвращает список допустимых типов автомобилей.
     * @return array
     */
    public function action_base_auto_tip()
    {
        $api = new Lardiapi();
        $arr = $this->object_to_array($api->baseAutoTip($this->sig));
        var_dump($arr);

        return $arr;
    }

    /**
     * http://api.lardi-trans.com/doc/base.trucks
     * Возвращает список типов кузовов.
     * @return array
     */
    public function action_base_trucks()
    {
        $api = new Lardiapi();
        $arr = $this->object_to_array($api->baseTrucks($this->sig));
        var_dump($arr);

        return $arr;
    }

    /**
     * http://api.lardi-trans.com/doc/base.zagruz
     * Возвращает список типов загрузки.
     * @return array
     */
    public function action_base_zagruz()
    {
        $api = new Lardiapi();
        $arr = $this->object_to_array($api->baseZagruz($this->sig));
        var_dump($arr);

        return $arr;
    }


    private function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }



}
