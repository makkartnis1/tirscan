<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Parser_Base extends Controller {

//    const AUTH_USER = "Trek11";
    const AUTH_USER = "olena12305@ukr.net";
//    const AUTH_PASS_NOT_HASH = "NhtrByaj11";//pass is md5 hash of NhtrByaj11
    const AUTH_PASS_NOT_HASH = "123123";//pass is md5 hash of NhtrByaj11
    const AUTH_PASS = "29296cd8fb4cd3bbe767a0daaafea3c9";//pass is md5 hash of NhtrByaj11
    public $sig; //required token for any api method, lifetime: 24h
    public $map_api_key = 'AIzaSyBBypFCkEyQKMzKj6vac1ZbFM5vp9gg9sI'; //required token for any api method, lifetime: 24h

    public function before()
    {
//        $page = $this->loadPageContent("http://api.lardi-trans.com/api/?method=auth&login=".self::AUTH_USER."&password=".self::AUTH_PASS);
//        $page_dom = $this->toDOMXPathXML($page['content']);
//        $this->sig = $page_dom->query('/response/sig')->item(0)->nodeValue;
    }

    public function loadPageContent($url, $redirect = false, $post = '', $max_redirect = 5){

        $user_agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36';
		$headers = [
			'Host:lardi-trans.com',
			'Origin:https://lardi-trans.com',
			'Referer:https://lardi-trans.com/accounts/login',
			'Upgrade-Insecure-Requests:1',
			'Accept-Encoding:gzip, deflate, br',
			'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Language:ru,en-US;q=0.8,en;q=0.6,uk;q=0.4',
		];

        $options = array(
            CURLOPT_HEADER => true,
            CURLOPT_VERBOSE => true,
            CURLOPT_CUSTOMREQUEST  => "GET",        //set request type post or get
            CURLOPT_POST           => false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     => DOCROOT."cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      => DOCROOT."cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
//            CURLOPT_FOLLOWLOCATION => $redirect,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => $max_redirect,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,
        );

		if($redirect)//not redirect, it's header
		{
			$options[CURLOPT_HTTPHEADER] = $headers;
		}

        if(strlen($post))
        {
            $options[CURLOPT_REFERER] = 'https://lardi-trans.com/';
            $options[CURLOPT_CUSTOMREQUEST] = 'POST';
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $post;
			$headers[] = 'Content-Type:application/x-www-form-urlencoded';
            $options[CURLOPT_HTTPHEADER] = $headers;
        }

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

		$needles = Model_Parser_Parser::getCaptchaTypes();

		foreach ($needles as $key => $needl)
			if(strpos($url, $needl))
				$content = $this->checkIfNeedCaptcha($content, $url, $key);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;

    }

    public function checkIfNeedCaptcha($content, $url, $type)
	{
		$model = new Model_Parser_Parser();
		$contentDom = $this->toDOMXPath($content);
		$captchaDom = $contentDom->query('//*[@id="predlFilterCont"]/table/tbody/tr[11]/td[1]/span/div/table/tbody/tr[2]/td/img');
		$captchaDom = $contentDom->query('//td//tr//span//div//form//table//tr//td//img');
		$captchaDom = $contentDom->query('//td//tr//span//div//form//table//tr//td//img');
		$captchaDom = $contentDom->query('//html/body/table/tr/td/div/div/form/table/tr/td/img');
		var_dump($model->getCaptchaType($type));
		var_dump($captchaDom);
		var_dump('captcha length');
		var_dump($captchaDom->length);
		if($captchaDom->length)
		{
			var_dump('current captcha');
			$currentCaptcha = $captchaDom->item(0)->getAttribute('src');
			var_dump($currentCaptcha);
			$captcha = $model->getCaptcha($type);
			if(count($captcha)){
				$captcha = $captcha[0];
				var_dump($captcha);
				$path = parse_url($captcha['url'])['path'];
				$path = str_replace('/dinamix/captcha/', '', $path);
				$path = str_replace('.png', '', $path);
				var_dump($path);
				if($captcha['status'] == "1")
				{
					$this->loadPageContent($url . '&userInputCaptchaText=' . $captcha["captcha"] . '&userInputCaptchaId=' . $path . '&enterCaptcha=%D0%9F%D1%80%D0%BE%D0%B4%D0%BE%D0%BB%D0%B6%D0%B8%D1%82%D1%8C');
					$model->setCaptchaStatus(Model_Parser_Parser::CAPTCHA_STATUS_USED, $type);
					return $this->loadPageContent($url)['content'];
				} elseif ($captcha['status'] == 0)
				{
					$to      = 'rikalsky@gmail.com';
					$subject = 'Потрібно поновити капчу';
					$message = 'Потрібно поновити капчу для ' . $model->getCaptchaName($type);
					$headers = 'From: info@tirscan.com' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();

					Helper_Customemail::send($to, $subject, $message);
					$model->setCaptchaStatus(Model_Parser_Parser::CAPTCHA_STATUS_NEED_NEW, $type);
					die('mail send');
				} else {
					$model->setCurrentCaptcha($currentCaptcha, $type);
//					die('captcha executed');
				}
//				die('wrong');
				//&userInputCaptchaText=yUctF&userInputCaptchaId=Xz0O9aHp&enterCaptcha=%D0%9F%D1%80%D0%BE%D0%B4%D0%BE%D0%BB%D0%B6%D0%B8%D1%82%D1%8C

			} else {
				die('captcha not found');
			}
//			$model->setCaptchaStatus(Model_Parser_Parser::CAPTCHA_STATUS_USED, $type);
		} else {
			unset($contentDom);
			unset($captchaDom);
			return $content;
		}
	}

    /**
     * @param $html string
     * @return DOMXPath
     */
    public function toDOMXPath($html){

        $dom_doc = new DOMDocument("1.0", "utf-8");

//        @$dom_doc->loadHTML( $html );
        $dom_doc->preserveWhiteSpace    = false;
        $dom_doc->formatOutput          = true;
        $dom_doc->validateOnParse       = true;

        @$dom_doc->loadHTML( mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8') );

        $page_ = new DOMXPath($dom_doc);

        $page_->document->encoding = 'UTF-8';

        return $page_;

    }

    public function toDOMXPathXML($html){

        $dom_doc = new DOMDocument("1.0", "utf-8");

//        @$dom_doc->loadHTML( $html );
        $dom_doc->preserveWhiteSpace    = false;
        $dom_doc->formatOutput          = true;
        $dom_doc->validateOnParse       = true;

        @$dom_doc->loadXML( mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8') );

        $page_ = new DOMXPath($dom_doc);

        $page_->document->encoding = 'UTF-8';

        return $page_;

    }


    public function parseThis($_text, $start, $end, $rm = FALSE)
    {
        $text = $_text;
        if (( $startpos = strpos($text, $start) ) !== FALSE)
        {
            $text = substr($text, $startpos + strlen($start), strlen($text));
            if (( $endpos = strpos($text, $end) ) !== FALSE)
            {
                $text = substr($text, 0, $endpos);
                if ($rm == TRUE)
                {
                    $_text = substr($_text, 0, $startpos).
                        substr(
                            $_text, $endpos + $startpos + strlen($end) + strlen($start), strlen($_text)
                        );
                }
            }
            else
            {
                return '';
            }
        }
        if ($text == $_text)
        {
            return '';
        }
        return $text;
    }


    public function parseAll($_text, $start, $end, $include=true){
        $first_pos = 0;
        $result = array();
        while( ($start_pos = strpos($_text, $start,$first_pos)) !== false){
            $first_pos = $start_pos +1;
            $end_pos = strpos($_text, $end,$first_pos);
            if($include){
                $result[] = substr($_text, $start_pos, $end_pos - $start_pos+strlen($end));
            }else{
                $result[] = substr($_text, $start_pos+strlen($start), $end_pos - $start_pos  - strlen($start));
            }
        }
        return $result;
    }

    public static function rus2translit($string) {

        $converter = array(

            'а' => 'a',   'б' => 'b',   'в' => 'v',

            'г' => 'g',   'д' => 'd',   'е' => 'e',

            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

            'и' => 'i',   'й' => 'y',   'к' => 'k',

            'л' => 'l',   'м' => 'm',   'н' => 'n',

            'о' => 'o',   'п' => 'p',   'р' => 'r',

            'с' => 's',   'т' => 't',   'у' => 'u',

            'ф' => 'f',   'х' => 'h',   'ц' => 'c',

            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',

            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',



            'А' => 'A',   'Б' => 'B',   'В' => 'V',

            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

            'И' => 'I',   'Й' => 'Y',   'К' => 'K',

            'Л' => 'L',   'М' => 'M',   'Н' => 'N',

            'О' => 'O',   'П' => 'P',   'Р' => 'R',

            'С' => 'S',   'Т' => 'T',   'У' => 'U',

            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',

            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

        );

        return strtr($string, $converter);

    }

    public static function str2url($str) {

        // переводим в транслит

        $str = self::rus2translit($str);

        // в нижний регистр

        $str = strtolower($str);

        // заменям все ненужное нам на "-"

        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);

        // удаляем начальные и конечные '-'

        $str = trim($str, "-");

        return $str;

    }



}
