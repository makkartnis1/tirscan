<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Doc extends Controller {

    public $fields = [];

    public function addField($text, $value, $group, $key = '') {
        if ($value == null) {
            return;
        }
        $this->fields[$group][] = [$text, $value];
    }

    public function action_index() {
        try {
            $type = $this->request->param('type');
            $id   = $this->request->param('id');

            $view = null;
            if ($type == 'transport') {
                $request = Model_Frontend_Transport::query_getTransports(
                    1, false, 1, 0,
                    [['t_trans.ID', '=', $id]]
                );

                if (empty($request)) {
                    throw new HTTP_Exception_404;
                }
                else {
                    $request = $request[0];
                }

                $this->addField('Дата розміщення', date('d.m.y H:i:s', strtotime($request['created'])), 0);
                if ($request['dateTo'] == null) {
                    $this->addField('Завантаження', date('d.m.y', strtotime($request['dateFrom'])), 0);
                }
                else {
                    $this->addField('Завантаження', date('d.m.y', strtotime($request['dateFrom'])) . ' - ' . date('d.m.y', strtotime($request['dateTo'])), 0);
                }
                $this->addField('Замовник', $request['username'], 1);
                $this->addField('Компанія', $request['company_name'], 1);
                $this->addField('Контактний телефон', $request['phoneCode'] . $request['phone'], 1);

                $this->addField('Додаткова інформація', $request['info'], 2);
                $this->addField('Інформація для водія', '<br><hr><br><hr><br><hr>', 3);

//                $image_file = fopen($_SERVER['DOCUMENT_ROOT'] . '/assets/img/logo@2x.png', 'rb');
//                $image_bin = '';
//                while (!feof($image_file)) {
//                    $image_bin .= fread($image_file, 8192);
//                }
//                fclose($image_file);
//                $image = 'data:image/png;base64,' . base64_encode($image_bin);
                $image = 'http://tirscan.com/assets/img/logo@2x.png';

                $view = View::factory('frontend/doc_transport', [
                    'request' => $request,
                    'id' => $id,
                    'image' => $image
                ]);
            }
            elseif ($type == 'cargo') {
                $request = Model_Frontend_Cargo::query_getCargo(
                    1, false, 1, 0,
                    [['t_cargo.ID', '=', $id]]
                );

                if (empty($request)) {
                    throw new HTTP_Exception_404;
                }
                else {
                    $request = $request[0];
                }

                $view = View::factory('frontend/doc_cargo', [
                    'request' => $request,
                    'id' => $id
                ]);
            }

            $view->fields = $this->fields;

            header("Content-Type: application/vnd.ms-word");
            header("Content-Disposition: attachment; filename={$type}-{$id}.doc");

            $this->response->body($view);
        }
        catch (HTTP_Exception $e) {
            throw $e;
        }
        catch (Exception $e) {
            throw $e;
            throw new HTTP_Exception_500;
        }
    }

    /**
     * @throws Exception
     * @throws HTTP_Exception
     * @throws HTTP_Exception_500
     */
    public function action_doc2() {
        try {
            $type = $this->request->param('type');
            $relatiomId = $this->request->param('id');

            $view = null;
            if ($type == 'transport') {
                $userTransport = ORM::factory('Frontend_User_Transport_Application', $relatiomId);
                $user = ORM::factory('Frontend_User', $userTransport->userID);
                $company = Model_Frontend_Company::query_getCompanyByID(1, $user->companyID);
                $request = Model_Frontend_Transport::query_getTransports(
                    1, false, 1, 0,
                    [['t_trans.ID', '=', $userTransport->transportID]]
                );
                if (empty($request)) {
                    throw new HTTP_Exception_404;
                }
                else {
                    $request = $request[0];
                }
            }
            elseif ($type == 'cargo') {
                $userCargo = ORM::factory('Frontend_User_Cargo_Application', $relatiomId);
                $user = ORM::factory('Frontend_User', $userCargo->userID);
                $company = Model_Frontend_Company::query_getCompanyByID(1, $user->companyID);
                $request = Model_Frontend_Cargo::query_getCargo(
                    1, false, 1, 0,
                    [['t_cargo.ID', '=', $userCargo->cargoID]]
                );

                if (empty($request)) {
                    throw new HTTP_Exception_404;
                }
                else {
                    $request = $request[0];
                }
            }
            echo '<pre>', var_dump($user->ID, $company, $request); exit;

//            header("Content-Type: application/vnd.ms-word");
//            header("Content-Disposition: attachment; filename={$type}-{$id}.doc");
//            $this->response->body($view);
        }
        catch (HTTP_Exception $e) {
            throw $e;
        }
        catch (Exception $e) {
            throw $e;
            throw new HTTP_Exception_500;
        }
    }

}
