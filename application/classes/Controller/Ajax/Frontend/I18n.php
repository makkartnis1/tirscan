<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_I18n extends Ajax
{

    public function create(){

        $vars = Arr::get($this->post, 'i18n',[]);

        if(empty($vars))
            return;

        I18n::loadDB($this->post['loc_id']);

        foreach ($vars as $var){

            __db($var);

        }

    }

}