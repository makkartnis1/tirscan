<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Complaints extends Ajax
{
    const DB_LIMIT = 10;
    const PAGINATION_STEP = 9;

    public function before()
    {
        parent::before();
        $this->debug = true;
//        $this->debug = false;
    }

    public function send_complaint(){

        try {
            $user_id = $this->post['userID'];
            $message = $this->post['message'];
            $to_company = $this->post['toCompany'];

            $complaint = ORM::factory('Frontend_Complaints');

            $complaint->userID = $user_id;
            $complaint->message = $message;
            $complaint->complainCompanyID = $to_company;

            $complaint->save();

            $this->add_success('complaint.send.success');

        } catch (ORM_Validation_Exception $e) {

            $this->add_error('complaint.error');

            $this->data['validation'] = $e->errors();
        }

    }

    public function add_to_black_list(){

        try {
            $user_id = $this->post['userID'];
            $to_company = $this->post['toCompany'];

            $black_list_count = ORM::factory('Frontend_BlackList')
                ->where('companyID', '=', $to_company)
                ->and_where('fromUserID', '=',$user_id)
                ->find();

            if ($black_list_count->companyID == $to_company && $black_list_count->fromUserID == $user_id) {

                $this->add_error('added.to.black.list.now');

            } else {

                $black_list = ORM::factory('Frontend_BlackList');
                $black_list->fromUserID = $user_id;
                $black_list->companyID = $to_company;

                $black_list->save();

                $this->add_success('add.to.black.list.success');
            }

        } catch (ORM_Validation_Exception $e) {

            $this->add_error('add.to.black.list.error');

            $this->data['validation'] = $e->errors();
        }
    }

    public function del_with_black_list(){

        $user_id = Frontend_Auth::userID();
        $to_company = $this->post['companyID'];

        Model_Frontend_BlackList::del_company_with_blacklist($to_company, $user_id);

        $this->add_success('delete.with.black.list.success');

    }

    public function get_black_list(){

        $locale_id = $this->post['lang_id'];
        $user_id = Frontend_Auth::userID();

        $black_list_count = ORM::factory('Frontend_BlackList')
            ->where('fromUserID', '=', $user_id)
            ->count_all();

        if (!empty($black_list_count)) {
            $this->data['count'] = $black_list_count;
        } else {
            $this->data['count'] = 0;
        }

        list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

        if ($this->data['count'] > 0) {

            $get_black_list = Model_Frontend_BlackList::get_list($user_id, $locale_id, $limit, $offset);

            $this->data['black_list'] = $get_black_list;

        } else {
            $this->data['black_list'] = [];
        }

    }

}