<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Profile extends Ajax {

    /** @var Model_Frontend_User */
    public $user;

    /** @var Model_Frontend_News_NewsSite */
    public $news;

    public function before()
    {
        $this->debug = true;
//        $this->debug = false;
        parent::before();

        if( ($userID = Frontend_Auth::userID()) === false){

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID); /** @var $user Model_Frontend_User */

        if (! $this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }
    }

    public function info(){

        $ownerEmail = DB::select('u.email')
            ->from(['Frontend_User_to_Company_Ownership','own'])
            ->where('own.companyID', '=', $this->user->companyID)
            ->join(['Frontend_Users', 'u'],'INNER')
            ->on('u.ID','=','own.userID')->execute()->as_array();

        $ownerEmail = $ownerEmail[0]['email'];

        $this->data['user'] = [

            'form'  => [

                'Frontend_Users[icq]'                   => $this->user->icq,
                'Frontend_Users[skype]'                 => $this->user->skype,
                'Frontend_Users[email]'                 => $this->user->email,

                'Frontend_Users[phone]'                 => $this->user->phone,
                'Frontend_Users[phoneCodeID]'           => $this->user->phoneCodeID,

                'Frontend_Users[phoneStationary]'       => $this->user->phoneStationary,
                'Frontend_Users[phoneStationaryCodeID]' => $this->user->phoneStationaryCodeID,

            ],

            'hidden' => ['primaryLocaleID'                       => $this->user->primaryLocaleID],
            

        ];
        
        $this->data['company'] = Model_Frontend_Company::query_companyInfo_for_editor($this->user->ID);
        $this->data['company']['ownerEmail'] = $ownerEmail;
            

        $locales = $this->user->locales->find_all();

        foreach ($locales as $loc){

            $this->data['user']['form']['Frontend_Users_Locale['.$loc->localeID.'][name]'] = $loc->name;
        }

    }
    
    public function save(){

        $this->post = json_decode($this->post['post'], true);
        $this->data['post'] = $this->post;

//        var_dump(Model_Frontend_Filters::onlyDigits('(12313) 90-09-20 dec')); die;

        try {
            $this->user->icq                    = $this->post['Frontend_Users']['icq'];
            $this->user->skype                  = $this->post['Frontend_Users']['skype'];
            //$this->user->email = $this->post['Frontend_Users']['email'];

            $this->user->phone                  = $this->post['Frontend_Users']['phone'];
            $this->user->phoneCodeID            = $this->post['Frontend_Users']['phoneCodeID'];

            $this->user->phoneStationary        = $this->post['Frontend_Users']['phoneStationary'];
            $this->user->phoneStationaryCodeID  = $this->post['Frontend_Users']['phoneStationaryCodeID'];

            $this->user->save();

            foreach ($this->post['Frontend_Users_Locale'] as $loc_ID => $loc) {
                if ($loc_ID == 0)
                    continue;

                if ( ($loc_ID != $this->user->primaryLocaleID) && ($loc['name'] == '')) {
                    $locale = ORM::factory('Frontend_User_Locale', ['localeID' => $loc_ID, 'userID' => $this->user->ID]);

                    if ($locale->loaded())
                        $locale->delete();

                    continue;
                }

                $locale = ORM::factory('Frontend_User_Locale', ['localeID' => $loc_ID, 'userID' => $this->user->ID]);

                if (! $locale->loaded()) {
                    $locale->localeID = $loc_ID;
                    $locale->userID = $this->user->ID;
                }

                $locale->name = $loc['name'];
                $locale->save();
            }

            $this->info(); // перечитуємо інфу

            $this->add_success('profile.info.saved');

        }
        catch (ORM_Validation_Exception $e) {

            $this->add_error('profile.info.form.error');

            $this->data['validation'] = $e->errors();

        }
        
    }

    public function save_my_company(){

        $this->post = json_decode($this->post['post'], true);

        $this->data['post'] = $this->post;
//        return;

        $company = Model_Frontend_Company::query_companyInfo_for_editor( $this->user->ID );

        if($company['isOwner'] != 'yes'){

            $this->add_error('profile.auth.not.authorized');
            return;

        }

        $company = $this->user->company;
        
        $this->data['primaryLocaleID'] = $company->primaryLocaleID;
        
        /**
         * @var $company Model_Frontend_Company
         */

        try{

            $db = Database::instance();

            $db->begin();


            $company->set('ipn',                    $this->post['Frontend_Companies']['ipn']);
            $company->set('ownershipTypeID',        $this->post['Frontend_Companies']['ownershipTypeID']);
            $company->set('phoneCodeID',            $this->post['Frontend_Companies']['phoneCodeID']);
            $company->set('phoneStationary',        $this->post['Frontend_Companies']['phoneStationary']);
            $company->set('phone',                  $this->post['Frontend_Companies']['phone']);
            //$company->set('typeID',                 $this->post['Frontend_Companies']['typeID']);
            $company->set('oldUrl',                 $company->url);
            $company->set('url',                    $this->post['Frontend_Companies']['url']);
            $company->set('approvedByAdmin',        'no');

            foreach ($this->post['Frontend_Company_Locales'] as $loc_ID => $loc ){

                if ($loc_ID == 0)
                    continue;

                $locale = ORM::factory('Frontend_Company_Locale', ['localeID' => $loc_ID, 'companyID' => $company->ID]);

                if ( ($loc_ID != $company->primaryLocaleID) && (Frontend_Helper_Arr::array_has_all_empty_fields($loc))) {

                    if ($locale->loaded())
                        $locale->delete();

                    continue;

                }


                if (! $locale->loaded()) {
                    $locale->localeID = $loc_ID;
                    $locale->companyID = $company->ID;
                }

                $locale->address        = $loc['address'];
                $locale->info           = $loc['info'];
                $locale->name           = $loc['name'];
                $locale->realAddress    = $loc['realAddress'];

                $locale->save();
                $company->save();

            }

            $this->add_success('company.info.save.success');

            $db->commit();

        }catch (ORM_Validation_Exception $e){

            $db->rollback();

            $this->add_error('profile.info.form.error');
            
            $this->data['orm'] = $e->errors();

        }catch (Exception $e){

            $db->rollback();
            
//            throw  $e;
            
        }

    }


    public function add_news(){

        $company_id = $this->post['company_id'];
        $image = $this->post['image'];

        $this->post = json_decode($this->post['post'], true);
        $this->data['post'] = $this->post;

        $this->data['user'] = [
            'primaryLocaleID'  => $this->user->primaryLocaleID
        ];

        try {
            $db = Database::instance();
            $db->begin();

            $this->news = ORM::factory('Frontend_News_NewsSite');

            $this->news->userID    = $this->user->ID;
            $this->news->image     = $image;
            $this->news->companyID = $company_id;
            $this->news->url       = Frontend_Helper_URL::title($this->post['Frontend_News_Locales'][1]['title']);

            $this->news->save();

            foreach ($this->post['Frontend_News_Locales'] as $loc_ID => $loc) {
                if ($loc_ID == 0) continue;

                if (($loc_ID != $this->user->primaryLocaleID) && ($loc['title'] == '')) {
                    $locale = ORM::factory('Frontend_News_Locale', ['localeID' => $loc_ID, 'newsID' => $this->news->companyID]);

                    if ($locale->loaded()) {
                        $locale->delete();
                    }

                    continue;
                }

                $locale = ORM::factory('Frontend_News_Locale');

                if (!$locale->loaded()) {
                    $locale->localeID = $loc_ID;
                    $locale->newsID = $this->news->companyID;
                }

                $locale->localeID = $loc_ID;
                $locale->newsID   = $this->news->ID;
                $locale->title    = $loc['title'];
                $locale->preview  = $loc['preview'];
                $locale->content  = $loc['text'];

                $locale->save();
            }

            $this->data['news_id'] = $this->news->ID;

            $this->add_success('success');

            $db->commit();

        } catch (ORM_Validation_Exception $e) {

            $db->rollback();

            $this->add_error('error');

            $this->data['validation'] = $e->errors();

        } catch (Exception $e){

            $db->rollback();

//            throw  $e;

        }

    }

    public function list_news(){

        $locale_id = $this->post['lang_id'];

        $list_news = Model_Frontend_News_NewsSite::getMyNews($this->user->ID, $locale_id);

        $this->data['list_news'] = $list_news;
    }

    public function one_news(){

        $news_id = $this->post['news_id'];
        $locale_id = $this->post['lang_id'];

        $get_one_news = Model_Frontend_News_NewsSite::getMyOneNews($this->user->ID, $news_id, $locale_id);

        $this->data['one_news'] = $get_one_news;
    }

    public function generate_managers(){

        $is_ownership_company = ORM::factory('Frontend_Company_Ownership_CompanyOwnership')
            ->where('userID', '=', $this->user->ID)
            ->find();

        if ($is_ownership_company->loaded()){

            $chars = "qazxswedcvfrtgbnhyujmkiolp";
            $max = 5;
            $size = StrLen($chars)-1;
            $temp_link = null;

            while($max--) {
                $temp_link .= $chars[rand(0, $size)];
            }

            $hash_temp_link = md5($temp_link);

            $link = 'http://tirscan.com' . Route::url('frontend_site_company_registration', ['lang' => $this->post['lang_uri']]) . '?code=' . $hash_temp_link;

            $this->data['registration_manager_link'] = $link;

            $manager_link = ORM::factory('Frontend_Registration_ManagerLink');

            $manager_link->hashLink = $hash_temp_link;
            $manager_link->companyID = $is_ownership_company->companyID;

            $link_datetime = new DateTime('now');
            $link_datetime->add(
                DateInterval::createFromDateString('24 hours')
            );

            $manager_link->availableUntil = $link_datetime->format('Y-m-d H:i:s');

            $manager_link->save();

        }
    }

    public function upload_image()
    {
        $error = false;

        $upload_dir = 'uploads/files/public/news/images/';
        $files = [];

        // Создадим папку если её нет
        if (!is_dir($upload_dir)) mkdir($upload_dir, 0777, true);

        // переместим файлы из временной директории в указанную

        foreach ($_FILES as $file) {
            if (move_uploaded_file($file['tmp_name'], $upload_dir . basename($file['name']))) {
                $files[] = '/'.$upload_dir . $file['name'];
            } else {
                $error = true;
            }
        }

        $this->data['files'] = $error ? ['error' => 'Ошибка загрузки файлов.'] : $files;
    }

}