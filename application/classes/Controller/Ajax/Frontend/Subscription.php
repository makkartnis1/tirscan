<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Subscription extends Ajax
{
    public $user;
    public $userID;
    const DB_LIMIT = 12;
    const PAGINATION_STEP = 9;

    public function before(){
        parent::before();

        if( ($this->userID = Frontend_Auth::userID()) === false){

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $this->userID); /** @var $user Model_Frontend_User */

        if (! $this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }
    }

    public function send_subscribe(){
        $user_id    = $this->userID;
        $company_id = $this->post['companyID'];

        try {

            $subscribe = ORM::factory('Frontend_Subscription')
                ->where('companyID', '=', $company_id)
                ->and_where('userID', '=',$user_id)
                ->find();

            if ($subscribe->companyID == $company_id && $subscribe->userID == $user_id) {

                $this->add_error('signed.up');

            } else {

                $subscribe = ORM::factory('Frontend_Subscription');

                $subscribe->companyID= $company_id;
                $subscribe->userID = $user_id;

                $subscribe->save();

                $this->add_success('subscribe.success');

            }


        } catch (ORM_Validation_Exception $e) {

            $this->add_error('subscribe.error');

            $this->data['validation'] = $e->errors();
        }


    }

    public function get_sub_company(){

        $locale_id = $this->post['lang_id'];
        $user_id = $this->userID;

        $subscribe = ORM::factory('Frontend_Subscription')
            ->where('userID', '=', $user_id)
            ->count_all();

        if (!empty($subscribe)) {
            $this->data['count'] = $subscribe;
        } else {
            $this->data['count'] = 0;
        }

        list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

        if ($this->data['count'] > 0) {

            $get_company_list = Model_Frontend_Subscription::get_company_list($user_id, $locale_id, $limit, $offset);

            $this->data['company_list'] = $get_company_list;

        }

    }

    public function un_subscribe(){
        $user_id = $this->userID;
        $to_company = $this->post['companyID'];

        Model_Frontend_Subscription::del_company_with_subscribe($to_company, $user_id);

        $this->add_success('delete.with.sub.list.success');
    }

}