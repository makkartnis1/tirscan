<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Feedback extends Ajax
{

    public function before(){
        parent::before();
        $this->debug = true;
//        $this->debug = false;
    }

    public function send_feedback(){

        $this->post = json_decode($this->post['post'], true);
        $this->data['post'] = $this->post;

        try {
            $name = $this->data['post']['name'];
            $email = $this->data['post']['email'];
            $message = $this->data['post']['message'];

            $feedback = ORM::factory('Frontend_Feedback');

            $feedback->message = $message;
            $feedback->email= $email;
            $feedback->user_name =$name;

            $feedback->save();

            $this->add_success('feedback.send.success');

        } catch (ORM_Validation_Exception $e) {

            $this->add_error('feedback.error');

            $this->data['validation'] = $e->errors();
        }
    }

    public function send_subscribe(){
        $user_id    = $this->post['userID'];
        $company_id = $this->post['companyID'];

        try {

            $subscribe = ORM::factory('Frontend_Subscription')
                ->where('companyID', '=', $company_id)
                ->and_where('userID', '=',$user_id)
                ->find();

            if ($subscribe->companyID == $company_id && $subscribe->userID == $user_id) {

                $this->add_error('signed.up');

            } else {

                $subscribe = ORM::factory('Frontend_Subscription');

                $subscribe->companyID= $company_id;
                $subscribe->userID = $user_id;

                $subscribe->save();

                $this->add_success('subscribe.success');

            }


        } catch (ORM_Validation_Exception $e) {

            $this->add_error('subscribe.error');

            $this->data['validation'] = $e->errors();
        }


    }

}