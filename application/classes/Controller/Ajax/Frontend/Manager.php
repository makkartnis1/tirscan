<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Manager extends Ajax {

    const DB_LIMIT = 20;
    const PAGINATION_STEP = 9;

    public $debug = true;
    
    /** @var Model_Frontend_User */
    public $user;

    public function before() {
        parent::before();

        if( ($userID = Frontend_Auth::userID()) === false){

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID); /** @var $user Model_Frontend_User */

        if (! $this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }
    }

    public function read(){

        $this->data['count'] = DB::select( [DB::expr('COUNT(*)'), 'count'] )
            ->from([Model_Frontend_User::TABLE, 'u'])
            ->where('u.companyID','=', $this->user->companyID)
            ->where('u.ID','<>',$this->user->ID)
            ->where('u.deleted', '=','n')
            ->execute()
            ->as_array();

        $this->data['count'] = (int) $this->data['count'][0]['count'];
        $this->data['table'] = [];
        $this->data['limit'] = self::DB_LIMIT;

        if($this->data['count'] == 0)
            return;

        list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

        $query = $this->getQuery();

        $query->limit($limit)->offset($offset)->where('u.deleted', '=','n');

        $this->data['table'] = $query->execute()->as_array();
        
    }

    /**
     * @return Database_Query_Builder_Select
     */
    protected function getQuery(){

        return DB::select()

            ->select(['u.ID', 'userID'])
            //->select([ 'ul_p.name', 'primaryName' ])
            ->select([DB::expr('IFNULL(`ul`.`name`, `ul_p`.`name`)'), 'username'])

            ->from([Model_Frontend_User::TABLE, 'u'])

            ->where('u.companyID','=', $this->user->companyID)
            ->where('u.ID','<>',$this->user->ID)

            ->join([Model_Frontend_Locale::TABLE, 'l'], 'RIGHT')
            ->on(DB::expr('1'), '=', DB::expr('1'))

            ->join([Model_Frontend_User_Locale::TABLE, 'ul'], 'LEFT')
            ->on('l.ID','=', 'ul.localeID')
            ->on('ul.userID','=', 'u.ID')

            ->join([Model_Frontend_User_Locale::TABLE, 'ul_p'], 'LEFT')
            ->on('ul_p.localeID','=', 'u.primaryLocaleID')
            ->on('ul.userID','=', 'u.ID')

            ->where('l.ID', '=', $this->post['loc_id'])

            ->group_by('u.ID')
            ->order_by('u.ID', 'DESC')
        ;


    }

    public function delete(){

        // TODO :: MIGRATE_DB >>> ALTER TABLE `Frontend_Users` ADD `deleted` ENUM('y','n') NOT NULL DEFAULT 'n' COMMENT 'on manager delete, makes it ''Y''' , ADD INDEX (`deleted`) ;

        if( !$this->user->isOwnerOfProfileCompany() ){
            $this->add_error('permission.denied');
            return;
        }

        $query = $this->getQuery();

        $is_manager_mine = $query->where('u.ID','=', (int) $this->post['userID'] )->limit(1)->execute()->as_array();

        if(empty($is_manager_mine)){

            $this->add_error('permission.denied');

            return;

        }

        DB::update( Model_Frontend_User::TABLE )
            ->set([
                'deleted'   => 'y',
            ])->where( 'ID','=', (int) $this->post['userID'] )
            ->execute();

        $this->read();

        $this->data['post'] = $this->post;

    }

}