<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Wallet extends Ajax {

    public $debug = true;

    /** @var Model_Frontend_User */
    public $user;

    public $localeID;
    public $localeUri;

    public function before() {
        parent::before();

        if (($userID = Frontend_Auth::userID()) === false) {
            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID);
        /** @var $user Model_Frontend_User */

        if (!$this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }

        $this->localeID = Arr::get($this->post, 'localeID', null);
        if ($this->localeID != null) {
            $orm_locale = ORM::factory('Frontend_Locale', $this->localeID);
            if (!$orm_locale->loaded()) {
                $this->add_error('invalid.data');
                $this->run = false;
            }
            else {
                $this->localeUri = $orm_locale->uri;
            }
        }
    }

    public static function processHistory($item) {
        $item['created']    = date('d.m.y G:i:s', strtotime($item['created']));
        $item['typeLocale'] = __db('wallet.history.' . $item['type']);
        $item['descriptionLocale'] = __db($item['description']);

        return $item;
    }

    public function get_history() {
        I18n::loadDB($this->localeID); // ініціалізація модуля локалізації Богдана
        $events = array_map('Controller_Ajax_Frontend_Wallet::processHistory', Model_Frontend_Wallet_History::query_get($this->user->ID));

        $this->data['items'] = $events;
    }

    public function get_liqpay_form() {
        $amount = Arr::get($this->post, 'amount');

        if ($amount < 10) {
            $this->add_error('user.permission.denied');
            return;
        }

        $form = App::component('LiqPay')->cnb_form([
            'version' => '3',
            'action' => 'pay',
            'amount' => $amount,
            'currency' => 'UAH',
            'description' => 'Поповнення гаманця в системі',
            'order_id' => rand(1, 9999),
            'language' => 'ru',
            'type' => 'buy',
            'sandbox' => 1,
            'server_url' => 'http://tirscan.com/liqpay/callback?' . http_build_query([
                    'user_id' => $this->user->ID
                ]),
            'result_url' => 'http://tirscan.com/profile/#wallet'
        ]);

        $this->data['form'] = $form;
    }

}