<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Pass extends Ajax {



    public function before()
    {
        $this->debug = true;
//        $this->debug = false;
//        sleep(1);
        parent::before();

        $this->data['was-posted'] = $this->post;
    }

    public function change(){

        $user = ORM::factory('Frontend_User', Frontend_Auth::userID()); /** @var $user Model_Frontend_User */
        if (! $user->loaded()) {
            $this->add_error('invalid.data');
            return;
        }

        if (! $user->checkPasswordEqual($this->post['current'])) {
            $this->data['validation'] = [
                'curr_path'  => ['correct', [$this->post['current']]]
            ];
            
            $this->add_error('invalid.form');
            // викид ошибки про те, що поточний пароль не співпадає
            return;
        }

        if( $this->post['new_1'] !== $this->post['new_2']){

            $this->data['validation'] = [
                'hash'  => ['matches', [$this->post['new_1'], $this->post['new_2']]]
            ];

            $this->add_error('invalid.form');
            return;
        }


        try {
            $user->password = $this->post['new_1'];
            $user->save();

            $this->add_success('profile.pass.change.success');
        }
        catch (ORM_Validation_Exception $e) {
            $this->data['validation'] = $e->errors();
            $this->add_error('invalid.form');

        }
    }


}