<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Transport extends Ajax {

    const DB_LIMIT = 16;
    const PAGINATION_STEP = 9;

    protected $userID = null;

    public function before() {
        $this->debug = true;
        parent::before();

        if( ($this->userID = Frontend_Auth::userID()) === false ){
            $this->add_error('user.not.authorized.permisson.denied');
            return;
        }
    }

    public function show_mine(){


        $this->data['count']    = Model_Frontend_User_Car::query_getUserTransport_no_execute($this->userID)->where('car.status','=','active');
        $this->data['result']   = clone $this->data['count'];

        $this->data['count'] = $this->data['count']
            ->select([DB::expr('COUNT(*)'), 'count'])
            ->execute()
            ->as_array();

        $this->data['count'] = (int) $this->data['count'][0]['count'];



        if($this->data['count']){

            $noCarImage = Model_Frontend_User_Car::NO_IMAGE;

            $page       = (int) Arr::get($this->post,'page',1);
            $limit      = self::DB_LIMIT;
            $offset     = $offset = ($page - 1) * $limit;

            $this->data['result'] = $this->data['result']

                ->join(['Frontend_Transports', 'ft'], 'LEFT')
                ->on('ft.userCarID','=', 'car.ID')
                ->group_by('car.ID')

                ->select('type.localeName', 'type.name', 'car.*')

                ->select([DB::expr("'{$noCarImage}'"), 'no_avatar'])

                ->select([DB::expr("IF(`ft`.`userCarID` IS NULL, 'yes', 'no')"), 'hasRequests'])

                ->limit($limit)
                ->offset($offset)
                ->order_by('car.id', 'DESC')
                ->execute()
                ->as_array();

            $this->data['pages']    = ceil( $this->data['count'] / $limit );
            $this->data['current']  = $page;
            $this->data['step']     = self::PAGINATION_STEP;


            foreach ($this->data['result'] as &$row){

                $row = self::_prepareDB_row($row, true);

                unset($row);

            }

        }else{

            $this->data['pages']    = 0;

            $this->data['result'] = [];
        }

    }

    protected static function _prepareDB_row($row, $emptyAs_MINUS = false){
        $row['minus'] = $emptyAs_MINUS;

        $row['cssIcon']                 = Model_Frontend_User_Car_Type::cssIcon($row['type']);
        $row['sizeX']                   = Model_Frontend_Filters::decimal($row['sizeX'], 2);
        $row['sizeY']                   = Model_Frontend_Filters::decimal($row['sizeY'], 2);
        $row['sizeZ']                   = Model_Frontend_Filters::decimal($row['sizeZ'], 2);
        $row['volume']                  = Model_Frontend_Filters::decimal($row['volume'], 2);

        $row['liftingCapacity']         =
            !($row['liftingCapacity'] == null)
            ? $row['liftingCapacity']
            : (($emptyAs_MINUS)
                ? '-'
                : NULL);
        $row['sizeX']                   =
            !($row['sizeX'] == '')
            ? $row['sizeX']
            : (($emptyAs_MINUS)
                ? '-'
                : NULL);

        $row['sizeY']                   =
            !($row['sizeY'] == '')//!(empty($row['sizeY']))
            ? $row['sizeY']
            : (($emptyAs_MINUS)
                ? '-'
                : NULL);

        $row['sizeZ']                   =
            !($row['sizeZ'] == '')//!(empty($row['sizeZ']))
            ? $row['sizeZ']
            : (($emptyAs_MINUS)
                ? '-'
                : NULL);

        $row['volume']                  =
            !($row['volume'] == '')//!(empty($row['volume']))
            ? $row['volume']
            : (($emptyAs_MINUS)
                ? '-'
                : NULL);

        

        $row['title']                   = Model_Frontend_User_Car::title($row['localeName'], $row['number']);
        $row['info']                    = htmlspecialchars($row['info']); // це для того щоб в атрибут можна було вказати

        return $row;

    }

    public function details_mine(){

        $info = $this->x__getCarRow($this->post['id']);

        if(empty($info))
            return;

        $can_manage = (bool) ($info[0]['hasRequests'] == 'yes');


        //$this->data['x'] = $can_manage;
        
        


        //                                          '/files/private/cars/docs',
        $widgetDocs = Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PRIVATE.Frontend_Uploader::DIR_CAR_DOCS,
            $this->post['id'],
            Frontend_Uploader::DIR_UPL      //      'uploads'

        )
            ->widget('file.widget.car.docs')

            ->showPreviewIfPossible(true)
            ->usingToken(true)

            ->download(true)
            ->delete($can_manage)
            ->upload($can_manage)

            ->render();

        $widgetNumbers = Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PRIVATE.Frontend_Uploader::DIR_CAR_NUMBERS,
            $this->post['id'],
            Frontend_Uploader::DIR_UPL      //      'uploads'

        )
            ->widget('file.widget.car.numbers')

            ->showPreviewIfPossible(true)
            ->usingToken(true)

            ->download(true)
            ->delete($can_manage)
            ->upload($can_manage)

            ->render();
        
        $widgetCarGallery = Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PUBLIC.Frontend_Uploader::DIR_CAR_GALLERY,
            $this->post['id'],
            Frontend_Uploader::DIR_UPL      //      'uploads'

        )
            ->widget('file.widget.car.photoes')

            ->showPreviewIfPossible(true)
            ->usingToken(true)

            ->download(true)
            ->delete($can_manage)
            ->upload($can_manage)

            ->render();

        $this->data['widget'] = [
            'docs'      => $widgetDocs,
            'numbers'   => $widgetNumbers,
            'gallery'   => $widgetCarGallery,
        ];

        $this->data['info'] = self::_prepareDB_row( $info[0], false );
        

    }

    public function delete_mine(){

        $info = $this->x__getCarRow($this->post['id'], false);

        if(empty($info))
            return;
            

        DB::update(Model_Frontend_User_Car::TABLE)->set([
            'status'    => 'deleted',
        ])
            ->where(Model_Frontend_User_Car::TABLE.'.id','=',$this->post['id'])
            ->execute();

    }

    public function update_mine(){

        $info = $this->x__getCarRow($this->post['id'], true);

        if(empty($info))
            return;

        if($info[0]['hasRequests'] == 'yes'){

            $this->x_notEnoughRights();

            return;

        }



        $orm = ORM::factory('Frontend_User_Car', $this->post['id']);

        $posted = [];

        parse_str($this->post['query'], $posted);

        try{

            $orm
                ->set('volume', $posted['volume'])
                ->set('sizeX', $posted['sizeX'])
                ->set('sizeY', $posted['sizeY'])
                ->set('sizeZ', $posted['sizeZ'])
                ->set('liftingCapacity', $posted['liftingCapacity'])

                ->set('info', $posted['info'])

                ->set('carTypeID', $posted['car_type'])

                ->set('type', $posted['car_body_type'])

                ->save()

            ;

        }catch (ORM_Validation_Exception $e){

            $this->add_error('form.data.invalid');

            $this->data['errors'] = $e->errors();

            return;

        }


        $this->add_success('transport.info.saved');


    }
    
    public function update_car_main_image(){
        
        $file   = basename(urldecode($this->post['url']));
        $carID  = $this->post['id'];

//        $this->data['f'] = $file;

        $info = $this->x__getCarRow($carID);

        if(empty($info))
            return;

        $widgetCarGallery = Frontend_Uploader::factory( Frontend_Uploader::DIR_VISIBLE_PUBLIC.Frontend_Uploader::DIR_CAR_GALLERY,
            $carID,
            Frontend_Uploader::DIR_UPL      //      'uploads'

        );

        if( !$widgetCarGallery->hasFile($file) ){

            $this->add_error('file.not.exists');

            return;
        }

        $orm = ORM::factory('Frontend_User_Car', $carID);

        $orm->set('avatar', '/'.

            Frontend_Uploader::DIR_UPL .
            Frontend_Uploader::DIR_VISIBLE_PUBLIC .
            Frontend_Uploader::DIR_CAR_GALLERY . "/{$carID}/" .
            $file

        )->save();

        $this->add_success('car.gallery.avatar.change.success');




    }

    //  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -

    protected function x_notEnoughRights(){
        $this->add_error('not.enough.rights');
    }

    protected function x__getCarRow($rowID, $join_requests = true){

        $info = Model_Frontend_User_Car::query_getUserTransport_no_execute($this->userID);

        if($join_requests){

            $info

                ->join(['Frontend_Transports', 'ft'], 'LEFT')
                ->on('ft.userCarID','=', 'car.ID')
                ->group_by('car.ID')

                ->select([DB::expr("IF(`ft`.`userCarID` IS NULL, 'no', 'yes')"), 'hasRequests']);

        }

        $info =

            $info

                ->select('type.localeName', 'type.name', 'car.*')

                ->where('car.status','=','active')
                ->where('car.id','=',$rowID)
                ->limit(1)
                ->execute()
                ->as_array();


        if( empty($info) ) {// це не є транспортом поточного користувача

            $this->x_notEnoughRights();

        }

        return $info;

    }


}