<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Applications extends Ajax {

    public $userID = false;

    public function before() {
        parent::before();

        $this->run = (($this->userID = Frontend_Auth::userID()) !== false);

        if (!$this->run) {
            $this->add_error('user.not.authorized.permission.denied');
        }
    }

    private function _get_model_by_type($type) {
        return ($type == 'cargo') ? 'Frontend_User_Cargo_Application' : 'Frontend_User_Transport_Application';
    }

    private function _get_request_model_by_type($type) {
        return ($type == 'cargo') ? 'Frontend_Cargo' : 'Frontend_Transport';
    }

    private function _get_fk_by_type($type) {
        return ($type == 'cargo') ? 'cargoID' : 'transportID';
    }

    private function _application_exists($type, $requestID) {
        $model      = $this->_get_model_by_type($type);
        $fk_request = $this->_get_fk_by_type($type);

        $applications = ORM::factory($model, [
            'userID' => $this->userID,
            $fk_request => $requestID
        ]);

        return $applications->loaded();

    }

    public function get() {
        $type      = Arr::get($this->post, 'type');
        $requestID = Arr::get($this->post, 'requestID');

        $model      = $this->_get_model_by_type($type);
        $fk_request = $this->_get_fk_by_type($type);

        $application = ORM::factory($model)
                          ->where('userID', '=', $this->userID)
                          ->and_where($fk_request, '=', $requestID)
                          ->find();

        $this->data['request_id'] = $requestID;
        $this->data['type']       = $type;
        if ($this->data['loaded'] = $application->loaded()) {
            $this->data['status'] = $application->status;
        }
        else {
            $request = ORM::factory($this->_get_request_model_by_type($type), $requestID);
            $user    = ORM::factory('Frontend_User', $this->userID);

            if ($request->loaded() && $user->loaded()) {
                $this->data['not_mine'] = ($request->user->companyID != $user->companyID);
            }
        }
    }

    public function send() {
        $type      = Arr::get($this->post, 'type');
        $requestID = Arr::get($this->post, 'requestID');

        $model         = $this->_get_model_by_type($type);
        $fk_request    = $this->_get_fk_by_type($type);
        $request_model = $this->_get_request_model_by_type($type);

        if ($this->_application_exists($model, $requestID)) {
            $this->add_error('user.application.error.app_already_exists');
            return;
        }

        try {
            $request = ORM::factory($request_model, $requestID);
            if (!$request->loaded()) {
                throw new Exception('no request ' . $requestID . ', ' . $request_model);
            }

            if (($request->status != 'active.have-no-proposition') && ($request->status != 'active.have-not-confirmed')) {
                throw new Exception('wrong status');
            }

            Model_Frontend_Transaction::start();

            ORM::factory($model)
               ->set('userID', $this->userID)
               ->set($fk_request, $requestID)
               ->set('status', 'pending')
               ->save();

            ORM::factory($request_model, $requestID)
               ->set('status', 'active.have-not-confirmed')
               ->save();

            $this->add_success('Пропозиція успішно надіслана');

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            $this->add_error('Помилка при надсиланні пропозиції! Спробуйте пізніше');
        }
    }

}