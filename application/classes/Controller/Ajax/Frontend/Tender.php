<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Tender extends Ajax {

    const DB_LIMIT = 16;
    const PAGINATION_STEP = 9;

    protected $userID = null;

    public $debug = true;

    public function before() {
        $this->debug = true;
        parent::before();

        if( ($this->userID = Frontend_Auth::userID()) === false ){
            $this->add_error('user.not.authorized.permisson.denied');
            return;
        }

    }


    public function getMy(){

        $m_tender = new Model_Frontend_Tenders();
        $this->data['was-posted'] = $this->post;

        $count = $m_tender->countAllByUser($this->userID);

        $page       = (int) Arr::get($this->post,'page',1);
        $limit      = self::DB_LIMIT;
        $offset     = $offset = ($page - 1) * $limit;

        $this->data['count'] = $count;
        $this->data['pages']    = ceil( $this->data['count'] / $limit );
        $this->data['current']  = $page;
        $this->data['step']     = self::PAGINATION_STEP;
        $this->data['result'] = $m_tender->getAllByUser($this->userID,$offset,self::DB_LIMIT);

    }

    public function get(){

        $m_tender = new Model_Frontend_Tenders();
        $this->data['was-posted'] = $this->post;

        $count = $m_tender->countAllOwnerByUser($this->userID);

        $page       = (int) Arr::get($this->post,'page',1);
        $limit      = self::DB_LIMIT;
        $offset     = $offset = ($page - 1) * $limit;

        $this->data['count'] = $count;
        $this->data['pages']    = ceil( $this->data['count'] / $limit );
        $this->data['current']  = $page;
        $this->data['step']     = self::PAGINATION_STEP;
        $this->data['result'] = $m_tender->getAllOwnerByUser($this->userID,$offset,self::DB_LIMIT);

    }


    public function delete()
    {
        $m_tender = new Model_Frontend_Tenders();
        $this->data['result'] =  $m_tender->delete($this->post['id']);
    }

    public function stop()
    {
        $m_tender = new Model_Frontend_Tenders();
        $this->data['result'] =  $m_tender->stop($this->post['id'],$this->post['stop_reason']);
    }

    public function changes()
    {
        $this->post = $_POST;
        $m_tender = new Model_Frontend_Tenders();
        $this->data['result'] =  $m_tender->check_changes($this->post['id'],$this->post['status'],$this->post['count_requests']);
    }

}