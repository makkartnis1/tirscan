<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Messenger extends Ajax{

    protected $userID = null;

    public $addMessage;
    public $addDialog;

    public function before(){
        parent::before();
//        $this->debug = true;
        $this->debug = false;

        if( $this->run = (($this->userID = Frontend_Auth::userID()) !== false )){
            $this->orm = ORM::factory('Frontend_User', $this->userID);
        } else {
            $this->add_error('user.not.authorized.permission.denied');
        }

    }

    protected function check_block(){
        $get_company_id = ORM::factory('Frontend_User')
            ->where('ID', '=', $this->userID)
            ->find();

        $company_in_black_list = ORM::factory('Frontend_BlackList')
            ->where('companyID', '=', $get_company_id->companyID)
            ->find_all();

        $who_blocked = [];

        foreach ($company_in_black_list as $val) {
            $who_blocked[] = $val->fromUserID;
        }

        return $who_blocked;
    }

    public function send_message(){

        $dialog_id = $this->post['dialog_id'];
        $user_to   = $this->post['user_to'];
        $message   = $this->post['message'];
        $avatar = Helper_User::avatar($this->userID);

        if (in_array($user_to ,$this->check_block())){
            $this->add_error('user.has.block.your');
        } else {

            try {
                $this->addMessage = ORM::factory('Frontend_Messages_Messages');
                $this->addMessage->dialogID = $dialog_id;
                $this->addMessage->message = $message;
                $this->addMessage->userMessageID = $this->userID;
                $this->addMessage->save();
                $last_message = $this->addMessage->ID;

                Model_Frontend_Messages_Messages::addStatus($user_to, $dialog_id, $last_message);
                Model_Frontend_Messages_Dialogs::addStatus($user_to, $dialog_id);

                $this->add_success('message.send.success');

                $messages = Model_Frontend_Messages_Messages::getMessagesByDialog($dialog_id, $this->post['lang_id']);
                $dialog_data = [];

                foreach ($messages as $key => $value) {
                    $ava_users[]['ava'] = Helper_User::avatar($value['userMessageID']);

                    $dialog_data[] = $ava_users[$key] + $value;
                }

                $this->data['dialog'] = $dialog_data;
                $this->data['userID'] = $this->userID;
                $this->data['dialogID'] = $dialog_id;
                $this->data['toUserID'] = $user_to;
                $this->data['logged_user_avatar'] = $avatar;

            } catch (ORM_Validation_Exception $e) {
                $this->add_error('enter.message');

                $this->data['validation'] = $e->errors();
            }
        }

    }

    public function get_dialog(){
        $dialog_id = $this->post['dialog_id'];
        $logged_user_avatar = Helper_User::avatar($this->userID);

        if(!empty($dialog_id) && !empty($this->userID)) {
            $db = Database::instance();
            $db->begin();

            try {
                Model_Frontend_Messages_Messages::delStatusMessage($dialog_id, $this->userID);
                Model_Frontend_Messages_Dialogs::updateStatus($dialog_id, $this->userID);

                $messages = Model_Frontend_Messages_Messages::getMessagesByDialog($dialog_id, $this->post['lang_id']);
                $dialog_data = [];

                foreach ($messages as $key => $value) {
                    $avatar[]['ava'] = Helper_User::avatar($value['userMessageID']);

                    $dialog_data[] = $avatar[$key]+$value;
                }

                $this->data['dialog'] = $dialog_data;
                $this->data['userID'] = $this->userID;
                $this->data['logged_user_avatar'] = $logged_user_avatar;

                $db->commit();

            } catch (HTTP_Exception_Redirect $e) {
                $db->rollback();
//                throw $e;
            }
        }
    }

    public function get_dialogs(){

        $dialogs = Model_Frontend_Messages_Dialogs::getDialogsByUser($this->post['lang_id'], $this->userID);

        $avatar      = [];
        $dialog_data = [];

        foreach ($dialogs as $key => $value) {
            if ($this->userID == $value['fromUserID']) {
                $avatar[]['ava'] = Helper_User::avatar($value['toUserID']);
            } else {
                $avatar[]['ava'] = Helper_User::avatar($value['fromUserID']);
            }

            $dialog_data[] = $avatar[$key]+$value;
        }

        $this->data['dialogs'] = $dialog_data;
        $this->data['userID'] = $this->userID;
    }

    public function del_message(){
        $message_id = $this->post['message_id'];

        if(!empty($message_id)) {
            Model_Frontend_Messages_Messages::delMessage($message_id);

            $this->add_success('messages.deleted');
            $this->get_dialog();
        }
    }

    public function del_dialog(){
        $dialog_id  = $this->post['dialog_id'];

        if(!empty($dialog_id)) {
            Model_Frontend_Messages_Dialogs::delDialog($dialog_id);

            $this->add_success('dialog.deleted');
            $this->get_dialogs();
        }
    }

    public function change_status(){
        $dialog_id  = $this->post['dialog_id'];

        if(!empty($dialog_id)) {
            Model_Frontend_Messages_Messages::changeStatusMessage($dialog_id, $this->userID);
        }
    }

    public function get_new_data(){

        $this->data['count'] = Model_Frontend_Messages_Messages::getUnreadMessages($this->userID);
    }

    public function new_dialog_and_message(){
        $user_to   = $this->post['user_to'];
        $user_from = $this->post['user_from'];
        $theme     = $this->post['theme'];
        $message   = $this->post['message'];

        if (in_array($user_to ,$this->check_block())){
            $this->add_error('user.has.block.your');
        } else {

            try {
                $db = Database::instance();
                $db->begin();

                $this->addDialog = ORM::factory('Frontend_Messages_Dialogs');
                $this->addDialog->theme = $theme;
                $this->addDialog->toUserID = $user_to;
                $this->addDialog->fromUserID = $user_from;
                $this->addDialog->save();
                $generated_dialog = $this->addDialog->ID;

                $this->addMessage = ORM::factory('Frontend_Messages_Messages');
                $this->addMessage->dialogID = $generated_dialog;
                $this->addMessage->message = $message;
                $this->addMessage->userMessageID = $user_from;
                $this->addMessage->save();
                $generated_message = $this->addMessage->ID;

                Model_Frontend_Messages_Messages::addStatus($user_to, $generated_dialog, $generated_message);
                Model_Frontend_Messages_Dialogs::addStatus($user_to, $generated_dialog);

                $this->add_success('message.send.success');

                $db->commit();

            } catch (ORM_Validation_Exception $e) {

                $db->rollback();

                $this->add_error('enter.message');

                $this->data['validation'] = $e->errors();
            } catch (Exception $e) {

                $db->rollback();

//                throw  $e;

            }
        }

    }

}