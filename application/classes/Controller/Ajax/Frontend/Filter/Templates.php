<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Filter_Templates extends Ajax {

    public $debug = true;

    /** @var Model_Frontend_User */
    public $user;

    public function before() {
        parent::before();

        if (($userID = Frontend_Auth::userID()) === false) {

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID);
        /** @var $user Model_Frontend_User */

        if (!$this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }
    }

    public function check_new() {
        $user_id   = Frontend_Auth::userID();
        $type      = Arr::get($this->post, 'type', null);
        $locale_id = Arr::get($this->post, 'locale_id', null);

        try {
            $locale = ORM::factory('Frontend_Locale', $locale_id);
            if (!$locale->loaded()) {
//                $this->add_error(__db('filter.template.error'));
                return;
            }

            $this->data['templates'] = Controller_Frontend_Requests::getActiveTemplatesUpdates($type, $user_id, $locale->googleLang);
        }
        catch (Exception $e) {
//            $this->add_error(__db('filter.template.error'));
        }
    }

    /**
     * Вертає зміни по новим заявкам для шаблонів
     */
    public function check() {
        $user_id   = Frontend_Auth::userID();
        $locale_id = Arr::get($this->post, 'locale_id', null);

        try {
            $templates = ORM::factory('Frontend_Filter_Template')
                            ->where('userID', '=', $user_id)
                            ->find_all();

            $locale = ORM::factory('Frontend_Locale', $locale_id);
            if (!$locale->loaded()) {
                $this->add_error(__db('filter.template.error'));
                return;
            }

            foreach ($templates as $template) {
                parse_str($template->query, $query_arr);
                if ($template->type == 'cargo') {
                    $filters           = Controller_Frontend_Requests::prepareFilters($locale->googleLang, $query_arr, true);
                    $new_request_count = Model_Frontend_Cargo::query_getNewCargoCountSince($filters, $template->lastUpdateTime);
                }
                else {
                    $filters           = Controller_Frontend_Requests::prepareFilters($locale->googleLang, $query_arr, false);
                    $new_request_count = Model_Frontend_Cargo::query_getNewCargoCountSince($filters, $template->lastUpdateTime);
                }

                $template->bufferCount    = $template->bufferCount + $new_request_count;
                $template->lastUpdateTime = DB::expr('NOW()');
                $template->save();
            }
        }
        catch (Exception $e) {
            $this->add_error(__db('filter.template.error'));
        }
    }

    public function remove() {
        $user_id = Frontend_Auth::userID();
        $item_id = Arr::get($this->post, 'itemID', null);

        try {
            ORM::factory('Frontend_Filter_Template')
               ->where('userID', '=', $user_id)
               ->and_where('ID', '=', $item_id)
               ->find()
               ->delete();

            $this->add_success(__db('filter.template.deleted'));
        }
        catch (Exception $e) {
            $this->add_error(__db('filter.template.error'));
        }
    }

    public function activate_semafor() {
        $user_id = Frontend_Auth::userID();
        $id      = Arr::get($this->post, 'id', null);
        $type    = Arr::get($this->post, 'type', null);

        $template = ORM::factory('Frontend_Filter_Template')
                       ->where('userID', '=', $user_id)
                       ->and_where('type', '=', $type)
                       ->and_where('ID', '=', $id)
                       ->find();

        if ($template->loaded()) {
            $template->active = 'yes';
            $template->save();

            $this->add_success('Семафор для шаблона активовано');
        }
        else {
            $this->add_error(__db('filter.template.error'));
        }
    }

    /**
     * Змінює/вставляє шаблон
     */
    public function update() {
        $user_id = Frontend_Auth::userID();
        $id      = Arr::get($this->post, 'id', null);
        $name    = Arr::get($this->post, 'name', null);
        $query   = Arr::get($this->post, 'query', null);
        $type    = Arr::get($this->post, 'type', null);

        $this->data = $this->post;

        // чистим не потрібні поля
        parse_str($query, $query_arr);
        unset($query_arr['date_to']);
        unset($query_arr['date_from']);
//        unset($query_arr['load']); TODO: прибирати локалізації geo (?)
//        unset($query_arr['unload']);
        $query = http_build_query($query_arr);

        if ($name == '%force_default_name%') {
            $load_geo   = json_decode(Arr::get($query_arr, 'load_geo'), true);
            $unload_geo = json_decode(Arr::get($query_arr, 'unload_geo'), true);

            $load_name_str = '?';
            $point_1       = '?';
            if ($load_geo != null) {
                $load_name_str = $load_geo['country'];
                $point_1       = $load_geo['name'];
            }

            $unload_name_str = '?';
            $point_2         = '?';
            if ($unload_geo != null) {
                $unload_name_str = $unload_geo['country'];
                $point_2         = $unload_geo['name'];
            }

            $name_part = $point_1 . ' - ' . $point_2;
            $name      = "{$name_part} ({$load_name_str} - {$unload_name_str})";
        }

        try {
            $template = ORM::factory('Frontend_Filter_Template');

            if ($id != null) {
                $template
                    ->where('ID', '=', $id)
                    ->find();
            }
            else {
                $existing = DB::select()
                              ->from(Model_Frontend_Filter_Template::TABLE)
                              ->where('userID', '=', $user_id)
                              ->and_where('type', '=', $type)
                              ->and_where('query', '=', $query)
                              ->execute()
                              ->as_array();

                if (count($existing) > 0) {
                    $this->add_error(__db('filter.template.already_exists'));
                    return;
                }

                $template->userID = $user_id;
                $template->type   = $type;
            }

            if ($name != null) {
                $template->name = $name;
            }
            if ($query != null) {
                $template->query = $query;
            }

            $template->save();
            $this->add_success(__db('filter.template.added'));
        }
        catch (Exception $e) {
            $this->add_error(__db('filter.template.error'));

//            throw $e;
        }
    }

    /**
     * Вертає список шаблонів для користувача
     */
    public function get() {
        try {
            $templates = ORM::factory('Frontend_Filter_Template')
                            ->where('userID', '=', Frontend_Auth::userID())
                            ->find_all();

            foreach ($templates as $template) {
                if ($template->type == Arr::get($this->post, 'type')) {
                    $this->data[] = [
                        'id' => $template->ID,
                        'name' => $template->name,
                        'query' => $template->query,
                        'active_semafor' => $template->active
                    ];
                }
            }
        }
        catch (Exception $e) {
            $this->add_error(__db('filter.template.error'));
        }
    }

}