<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Request extends Ajax {

    const DB_LIMIT          = 16;
    const PAGINATION_STEP   = 9;

    protected $userID       = null;

    public $debug = true;

    public function before()
    {
        parent::before();
        
        $this->run = ( ($this->userID = Frontend_Auth::userID()) !== false );

        if(!$this->run)
            $this->add_error('user.not.authorized.permisson.denied');

    }

    protected function _apply_where(Database_Query_Builder_Select $query, $sort){

        switch ($sort){

            case ('negative'):  $query = $query->where('comments.rating', '=', 'negative'); break;
            case ('positive'):  $query = $query->where('comments.rating', '=', 'positive'); break;

        }

        return $query;

    }

    public  function get_feedback(){

        $this->data['count'] = $this->data['count'] =
            $this->_apply_where( Model_Frontend_Comment::query_feedbacks($this->userID) , Arr::get($this->post,'sort'))
            ->select([DB::expr('COUNT(*)'), 'count'])->execute()->as_array();

        $this->data['count'] = (int) $this->data['count'][0]['count'];

        $this->data['UID'] = $this->userID;




        if(!$this->data['count']){

            $this->data['pages'] = 0;

            $this->data['result'] = [];

            return;

        }

        $page       = (int) Arr::get($this->post,'page',1);
        $limit      = self::DB_LIMIT;
        $offset     = $offset = ($page - 1) * $limit;

        $this->data['pages']    = ceil( $this->data['count'] / $limit );
        $this->data['current']  = $page;
        $this->data['step']     = self::PAGINATION_STEP;

        $todoSelect = [

            'comments'  => [
                'text',
                'companyID',
                'userID',
                'createDate',
                'cargoID',
                'transportID',
                'rating',
            ],
//            'users' => [
//                'id',
//            ],
        ];


        $this->data['result'] = Model_Frontend_Comment::query_feedbacks_additional($this->userID, $this->post['locale_id']);

        $this->data['result'] = $this->_apply_where($this->data['result'], Arr::get($this->post,'sort'));

        foreach ($todoSelect as $table => $columns){

            foreach ($columns as $column){
                $this->data['result']->select($table.'.'.$column);
            }

        }



        $this->data['result']->select([DB::expr('IF(`ful_2`.`name` IS NULL, `ful_1`.`name`, `ful_2`.`name`)'),'userName']);
        $this->data['result']->select(['company_locale.name','companyPrimaryLocaleName']);
        $this->data['result']->select(['company_locale2.name','companyName']);




        $this->data['result'] = $this->data['result']

            ->select(['users.id','userID'])

            ->limit($limit)
            ->offset($offset)

            ->order_by('comments.ID','DESC')
            ->execute()
            ->as_array();

        foreach ($this->data['result'] as &$row){

            $row['avatar'] = Helper_User::avatar($row['userID']);

            $row['companyURL'] = Route::url('frontend_site_company_page', [
                'company_url_name'  => $row['companyID']. '-' . Frontend_Helper_URL::title($row['companyPrimaryLocaleName']),
                'lang'              => $this->post['locale_uri'],
            ]);

            unset($row);
        }



        
        
        /*
        
        
        // сторінка компанії
            Route::set('frontend_site_company_page', '(<lang>/)company/<company_url_name>', ['company_url_name' => '[1-9]\d*[-].+'])
            ->defaults([
                'directory'  => 'frontend',
                'controller' => 'company',
                'action' => 'page'
            ]); 
        
        
         
         */


    }

}