<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Partner extends Ajax {

    const DB_LIMIT = 10;
    const PAGINATION_STEP = 9;

    protected $userID = null;
    
    protected $orm;

    public function before()
    {

        parent::before();

        $this->debug = true;

        if( $this->run = ( ($this->userID = Frontend_Auth::userID()) !== false ) ){

            $this->orm = ORM::factory('Frontend_User', $this->userID);
            
        }else{

            $this->add_error('user.not.authorized.permisson.denied');
            
        }
            
    }

    protected function _init_type(){

        /*

        TRUNCATE TABLE `Frontend_Company_Partners`;

        INSERT INTO `Frontend_Company_Partners` (`ID`, `company1ID`, `company2ID`) VALUES
        (10, 536, 634),
        (15, 600, 634),
        (9, 634, 536),
        (12, 634, 537),
        (13, 634, 538),
        (14, 634, 539),
        (16, 634, 541),
        (17, 634, 542),
        (19, 634, 544),
        (21, 634, 546);

        $this->post
                companyID:  companyID,
                type:       type,
                page:       page

        type:

         */

        switch ($this->post['type']){

            case ('wait'):      $this->post['type'] = 'i-must-wait';    break;
            case ('confirm'):   $this->post['type'] = 'i-must-confirm'; break;
            case ('unlink'):    $this->post['type'] = 'partners';       break;

            default: throw HTTP_Exception::factory(500);

        }

    }

    public function load_list(){

        $this->data['count_i_must_confirm'] = Model_Frontend_Company::query_my_partners_request_waiting_for_mine_confirmation($this->orm->companyID);
        $this->data['count_i_must_confirm'] = $this->data['count_i_must_confirm'][0]['count'];

        $this->data['partners'] = [];

        $this->data['x'] = $this->post['type'];

        switch ($this->post['type']){

            case ('partners'):

                $this->data['count']    = Model_Frontend_Company::query_my_partners_confirmed($this->orm->companyID);
                $this->data['count']    = $this->data['count'][0]['count'];

                list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

                if($this->data['count'] > 0){

                    $this->data['partners'] = Model_Frontend_Company::query_my_partners_confirmed($this->orm->companyID, $this->post['lang_id'])
                        ->limit($limit)
                        ->offset($offset)
                        ->execute()
                        ->as_array()
                    ;
                }

                $class = 'confirmed';
                break;

            case ('i-must-confirm'):

                $this->data['count']    = $this->data['count_i_must_confirm'];

                list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

                if($this->data['count'] > 0){

                    $this->data['partners'] = Model_Frontend_Company::query_my_partners_request_waiting_for_mine_confirmation($this->orm->companyID, $this->post['lang_id'])
                        ->limit($limit)
                        ->offset($offset)
                        ->execute()
                        ->as_array()
                    ;

                }

                $class = 'not-confirmed';
                break;

            case ('i-must-wait'):

                $this->data['count'] = Model_Frontend_Company::query_my_partners_request_waiting_for_their_confirmation($this->orm->companyID);
                $this->data['count']    = $this->data['count'][0]['count'];

                list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

                if($this->data['count'] > 0){

                    $this->data['partners'] = Model_Frontend_Company::query_my_partners_request_waiting_for_their_confirmation($this->orm->companyID,$this->post['lang_id'])
                        ->limit($limit)
                        ->offset($offset)
                        ->execute()
                        ->as_array()
                    ;
                }

                $class = 'wait';
                break;

            default: $this->add_error('undefined.type'); return;
            
        }

        foreach ($this->data['partners'] as &$row) {

            $row['url'] = Route::url('frontend_site_company_page',[
                'lang'                  => $this->post['lang'],
                'company_url_name'      => Controller_Frontend_Company::getCompanyUrlName([
                    'ID'                    => $row['companyID'],
                    'primaryName'           => $row['primaryName'],
                ]),
            ]);

            $row['avatar']  = Helper_User::avatar($row['ownerID']);

            $row['type'] = $this->post['type'];
            $row['class'] = $class;

            unset($row);
            
        }

    }

    public function decline(){

        $this->_init_type();

        try{

            Database::instance()->begin();

            Model_Frontend_Company::query_break_partner_request($this->orm->companyID, $this->post['companyID']);

            Database::instance()->commit();

        }catch (Exception $e){

            Database::instance()->rollback();

//            throw $e;

        }

        $this->load_list();

    }

    public function confirm(){

        $this->_init_type();

        Model_Frontend_Company::query_post_partner_request($this->orm->companyID, $this->post['companyID']);

        $this->load_list();

    }

    public function get_news() {

        $companyID = Model_Frontend_News_PartnersNews::getCompanyISigned($this->userID);

        $company_is_signed = [];

        foreach ($companyID as $value) {
            $company_is_signed[] = $value['companyID'];
        }

        if (!empty($company_is_signed)) {
            $this->data['count'] = (int) Model_Frontend_News_NewsSite::countCompanyNewsISigned($company_is_signed, $this->post['lang_id']);
        } else {
            $this->data['count'] = 0;
        }

        list($limit, $offset) = $this->_init_paging(self::DB_LIMIT, self::PAGINATION_STEP);

        if($this->data['count'] > 0){

            $this->data['news_list'] = Model_Frontend_News_NewsSite::getCompanyNewsISigned($company_is_signed, $this->post['lang_id'])
                ->limit($limit)
                ->offset($offset)
                ->execute()
                ->as_array();
        }

    }

    public function get_one_news() {

        $this->data['one_news'] =  Model_Frontend_News_NewsSite::getOneNewsById($this->post['id'], $this->post['lang_id']);

    }

    public function update_news() {

        $this->post = json_decode($this->post['post'], true);

        $data = [
          'title'  => $this->post['title'],
          'preview'  => $this->post['preview'],
          'content'  => $this->post['text']
        ];

        Model_Frontend_News_NewsSite::updateNews($this->post['id'], $data);

    }

}