<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Messaging extends Ajax {

    public $debug = true;

    /** @var Model_Frontend_User */
    public $user;

    public $localeID;
    public $localeUri;

    public function before() {
        parent::before();

        if (($userID = Frontend_Auth::userID()) === false) {

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID);
        /** @var $user Model_Frontend_User */

        if (!$this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }

        $this->localeID = Arr::get($this->post, 'localeID', null);
        if ($this->localeID != null) {
            $orm_locale = ORM::factory('Frontend_Locale', $this->localeID);
            if (!$orm_locale->loaded()) {
                $this->add_error('invalid.data');
                $this->run = false;
            }
            else {
                $this->localeUri = $orm_locale->uri;
            }
        }
    }

    public function get_all_dialogs() {
        $request_type = Arr::get($this->post, 'request_type');
        $request_id   = Arr::get($this->post, 'request_id');

        $dialogs = Model_Frontend_Request_Dialog::query_getDialogs($request_type, $request_id);

        foreach ($dialogs as &$dialog) {
            $dialog['avatar'] = Helper_User::avatar($dialog['userID']);

            $last_message      = Model_Frontend_Request_Dialog::query_getLastMessage($request_type, $request_id, $dialog['userID']);
            $dialog['msg']     = $last_message['msg'];
            if ($last_message['created'] != '') {
                $dialog['created'] = date('d.m.y H:i:s', strtotime($last_message['created']));
            }
            else {
                $dialog['created'] = '';
            }

            unset($dialog);
        }

        $this->data['dialogs'] = $dialogs;
    }

    public function get_messages() {
        $request_type = Arr::get($this->post, 'request_type');
        $request_id   = Arr::get($this->post, 'request_id');
        $sender_type  = Arr::get($this->post, 'sender_type');
        $user         = Arr::get($this->post, 'user');
        $user2        = Arr::get($this->post, 'user2');

        if (($user !== $this->user->ID) && ($user2 !== $this->user->ID)) {
            throw new Exception('permission denied');
        }
        
        $messages = Model_Frontend_Request_Dialog::query_getMessages($request_type, $request_id, $user2);

        foreach ($messages as &$message) {
            $message['avatar'] = ($message['owner'] == 'owner')
                ? Helper_User::avatar($message['ownerID'])
                : Helper_User::avatar($message['userID']);

            $message['created'] = date('d.m.y H:i:s', strtotime($message['created']));
            unset($message);
        }
        
        Model_Frontend_Request_Dialog::query_markRead($request_type, $sender_type, $request_id, $user2);

        $this->data['messages'] = $messages;
    }

    public function send_message() {
        $request_type = Arr::get($this->post, 'request_type');
        $request_id   = Arr::get($this->post, 'request_id');
        $sender_type  = Arr::get($this->post, 'sender_type');
        $user         = Arr::get($this->post, 'user');
        $user2        = Arr::get($this->post, 'user2');
        $message      = Arr::get($this->post, 'message');

        if ($sender_type == 'prop') {
            $owner = 'user';
        }
        else {
            $owner = 'owner';
        }

        Model_Frontend_Request_Dialog::query_sendMessage($request_type, $request_id, $owner, $message, $user, $user2);
    }

}