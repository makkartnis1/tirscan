<?php
/**
 * LINKeR
 */

defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Statistic extends Ajax {

    protected $userID;

    protected static $i18n_transport    = 'stat.request.transport';
    protected static $i18n_cargo        = 'stat.request.cargo';

    protected static $cargo_labels = [
        'stat.request.cargo.outdated',
        'stat.request.cargo.completed',
        'stat.request.cargo.have.confirmed.proposition',
        'stat.request.cargo.have.not.yet.confirmed.proposition',
        'stat.request.cargo.have.not.proposition',
    ];

    protected static $transport_labels = [
        'stat.request.transport.outdated',
        'stat.request.transport.completed',
        'stat.request.transport.have.confirmed.proposition',
        'stat.request.transport.have.not.yet.confirmed.proposition',
        'stat.request.transport.have.not.proposition',
    ];

    protected static $months = [

        'nominative'    => [
            'january.nominative',
            'february.nominative',
            'march.nominative',
            'april.nominative',
            'may.nominative',
            'june.nominative',
            'july.nominative',
            'august.nominative',
            'september.nominative',
            'october.nominative',
            'november.nominative',
            'december.nominative'
        ],

        'genitive'      => [
            'january.genitive',
            'february.genitive',
            'march.genitive',
            'april.genitive',
            'may.genitive',
            'june.genitive',
            'july.genitive',
            'august.genitive',
            'september.genitive',
            'october.genitive',
            'november.genitive',
            'december.genitive'
        ]

    ];


    public function before()
    {
        $this->debug = true;

        parent::before();

        $this->data['posted'] = $this->post;

        $this->run = ( ($this->userID = Frontend_Auth::userID()) !== false );

        $this->data['u'] = $this->userID;

        if(!$this->run)
            $this->add_error('user.not.authorized.permisson.denied');

    }

    public function feedback(){

        $this->data['callback'] = 'feedback';

        # читаємо дані для пропорції за весь час

        $query = Model_Frontend_Comment::query_feedbacks($this->userID, false)
            ->select([ DB::expr('COUNT(*)'), 'count' ], 'comments.rating')->group_by('comments.rating');

        #!!! Вся статисктика по відгукам за всю історію компанії
        $total = $query->execute()->as_array();

        $this->data['proportion'] = [
            'labels'    => ['stat.feedback.chart.positive', 'stat.feedback.chart.negative'],
            'data'      => [0,0]
        ];

        foreach ($total as $row){

            $idx = (int) !!($row['rating'] == 'negative');

            $this->data['proportion']['data'][$idx] = $row['count'];
        }

        # групуємо по дням поточного місяця

        if($this->post['curGroup'] == 'month'){

            list($year, $month) = explode('/', $this->post['curMonth']);

            #!!! Вся статисктика по відгукам компанії ДО ОБРАНОЇ ДАТИ
            $_before_range = $query

                ->and_where_open()

                    ->or_where_open()
                        ->where(DB::expr('YEAR(`comments`.`createDate`)'),      '=', (int) $year)
                        ->where(DB::expr('MONTH(`comments`.`createDate`)'),     '<', (int) $month)
                    ->or_where_close()

                ->or_where(DB::expr('YEAR(`comments`.`createDate`)'),      '<', (int) $year)


                ->and_where_close()

                ->execute()
                ->as_array();

            $query = Model_Frontend_Comment::query_feedbacks($this->userID, false)
                ->select(

                    [ DB::expr('COUNT(*)'), 'count' ],
                    'comments.rating',
                    [DB::expr('DAY(`comments`.`createDate`)'), 'day']
                )
                ->where(DB::expr('YEAR(`comments`.`createDate`)'),      '=', (int) $year)
                ->where(DB::expr('MONTH(`comments`.`createDate`)'),     '=', (int) $month)
                ->group_by(DB::expr('DAY(`comments`.`createDate`)'))
                ->group_by('comments.rating')
            ;

            #!!! Вся статисктика по відгукам компанії ПО ОБРАНІЙ ДАТІ
            $_current_range = $query->execute()->as_array();

            $days = cal_days_in_month(CAL_GREGORIAN, (int) $month, (int) $year); // 31

            $labels = array_keys( array_fill(1, $days, 0) );

            $_cr_tmp = [];

            foreach ($_current_range as $row){

                $row['day'] = (int) $row['day'];

                if( !array_key_exists($row['day'], $_cr_tmp) )
                    $_cr_tmp[$row['day']] = [];

                $_cr_tmp[ $row['day'] ][ $row['rating'] ] = $row['count'];

            }

            ksort($_cr_tmp); // сортуємо щоб зпочатку була менша дата

            $min_negative       = 0;
            $min_positive       = 0;
            $before_negative    = 0;
            $before_positive    = 0;

            foreach ($_before_range as $row){
                if($row['rating'] == 'positive')
                    $before_positive    = (int) $row['count'];

                if($row['rating'] == 'negative')
                    $before_negative    = (int) $row['count'];
            }

            $result = [

                'current' => [                  // заповнюємо всі дані для графіка ПОТОЧНОЇ ДИНАМІКИ нулями
                    'positive'  => array_fill(0, $days, $min_positive),
                    'negative'  => array_fill(0, $days, $min_negative),
                ],
                'current_and_earlier' => [      // заповнюємо всі дані для графіка ПОТОЧНОЇ ДИНАМІКИ РАЗОМ ІЗ ПОПЕРЕДНІМИ ПОКАЗНИКАМИ нулями
                    'positive'  => array_fill(0, $days, $before_positive),
                    'negative'  => array_fill(0, $days, $before_negative),
                ],

            ];

            foreach ($_cr_tmp as $day => $row){

                $min_negative = (empty($row['negative'])) ? $min_negative: $min_negative + $row['negative'];
                $min_positive = (empty($row['positive'])) ? $min_positive: $min_positive + $row['positive'];

                $negative = array_fill($day - 1, $days - $day + 1, $min_negative);
                $positive = array_fill($day - 1, $days - $day + 1, $min_positive);

                $result['current']['positive'] =  Helper_Array::array_merge_indexed( $result['current']['positive'], $positive ) ;
                $result['current']['negative'] =  Helper_Array::array_merge_indexed( $result['current']['negative'], $negative ) ;

            }

            $result['current_and_earlier']['positive'] =  [];
            $result['current_and_earlier']['negative'] = [];

            $positive = $negative = $b_positive = $b_negative = [];

            foreach ($result['current']['positive'] as $key => $val){

                $positive[] = (int) $val;
                $negative[] = (int) ( $result['current']['negative'][$key] );
                $b_positive[] = (int) ( $before_positive + $val );
                $b_negative[] = (int) ( $before_negative + $result['current']['negative'][$key] );

            }


            $this->data['chart'] = [

                'labels'    => [
                    'i18n' => false,
                    'list' => $labels,
                ],

                'hint' => [
                    'i18n' => false,
                    'before'  => $year.'/'.$month.'/',
                    'from'  => '01',
                    'to'    => $days,
                ],

                'dataset' => [

                    'positive'  =>
                    [
                        'i18n_name' => 'stat.feedback.chart.positive',
                        'data'      => $positive,
                    ],

                    'negative' =>
                    [
                        'i18n_name' => 'stat.feedback.chart.negative',
                        'data'      => $negative,
                    ],

                ],

                'dataset_with_before' => [
                    
                    'positive' => ['data' => $b_positive ],
                    'negative' => ['data' => $b_negative ],
                    
                ]
            ];

            

        }else{

            $year = $this->post['curYear'];

            $_before_range = $query // загальна статистика до поточного року, групована по типам
                ->where(DB::expr('YEAR(`comments`.`createDate`)'),      '<',   (int) $year)
                ->execute()
                ->as_array();

            $_cur_range = $query = Model_Frontend_Comment::query_feedbacks($this->userID, false)
                ->select(

                    [ DB::expr('COUNT(*)'), 'count' ],
                    'comments.rating',
                    [DB::expr('MONTH(`comments`.`createDate`)'), 'month']
                )
                ->where(DB::expr('YEAR(`comments`.`createDate`)'),      '=', (int) 2016 )
                ->group_by( DB::expr('MONTH(`comments`.`createDate`)') )
                ->group_by('comments.rating')
                ->execute()
                ->as_array();
            ;

            $before_negative = $before_positive = 0;

            foreach ($_before_range as $row){

                if($row['type'] == 'positive')
                    $before_positive = (int) $row['count'];

                if($row['type'] == 'negative')
                    $before_negative = (int) $row['count'];

            }

            $cur_sorted = [];

            foreach ($_cur_range as $row){

                $k  = (int) $row['month'];

                if( !array_key_exists($k, $cur_sorted) )
                    $cur_sorted[$k] = [];

                $cur_sorted[$k][ $row['rating'] ] = (int) $row['count'];

            }

            $positive_cur_year = $negative_cur_year = $positive_cur_year_and_earlier = $negative_cur_year_and_earlier =
                array_fill(0, 12, 0);

            $positive_cur = 0;
            $negative_cur = 0;

            foreach($cur_sorted as $month => $feedbacks ){

                if(array_key_exists('positive', $feedbacks))
                    $positive_cur = $feedbacks['positive'];

                if(array_key_exists('negative', $feedbacks))
                    $negative_cur = $feedbacks['negative'];

                $positive_cur_year = Helper_Array::array_merge_indexed($positive_cur_year, array_fill($month - 1, 12, $positive_cur));
                $negative_cur_year = Helper_Array::array_merge_indexed($negative_cur_year, array_fill($month - 1, 12, $negative_cur));

                $positive_cur_year_and_earlier = Helper_Array::array_merge_indexed($positive_cur_year_and_earlier, array_fill($month - 1, 12, $before_positive + $positive_cur));
                $negative_cur_year_and_earlier = Helper_Array::array_merge_indexed($negative_cur_year_and_earlier, array_fill($month - 1, 12, $before_negative + $negative_cur));

            }


            $this->data['chart'] = [

                'labels'    => [
                    'i18n' => true,
                    'list' => self::$months['nominative'],
                ],

                'hint' => [
                    'i18n' => true,
                    'before'  => $year,
                    'from'  => 'january.genitive',
                    'to'    => 'december.genitive',
                ],

                'dataset' => [

                    'positive'  =>
                        [
                            'i18n_name' => 'stat.feedback.chart.positive',
                            'data'      => $positive_cur_year,
                        ],

                    'negative' =>
                        [
                            'i18n_name' => 'stat.feedback.chart.negative',
                            'data'      => $negative_cur_year,
                        ],

                ],
                'dataset_with_before' => [

                    'positive' => ['data' => $positive_cur_year_and_earlier ],
                    'negative' => ['data' => $positive_cur_year_and_earlier ],

                ]
            ];


        }


    }

    public function cargo(){
        $this->data['callback'] = 'cargo';

        $this->_stat_requests('Frontend_Cargo', self::$cargo_labels, self::$i18n_cargo);
    }

    public function transport(){

        $this->data['callback'] = 'transport';

        $this->_stat_requests('Frontend_Transports', self::$transport_labels, self::$i18n_transport);

    }

    protected function _stat_requests($table, $labels, $chart_name){

        $companyID = DB::select('companyID')->from('Frontend_Users')->where('ID','=',$this->userID)->limit(1)->execute()->as_array();
        $companyID = (int) $companyID[0]['companyID'];

        $query = DB::select()

            ->select([DB::expr('COUNT(*)'),'count'])
            ->select('req.status')

            ->from([$table,'req'])
            ->join(['Frontend_Users','fu'],'INNER')
            ->on('fu.ID','=','req.userID')
            ->where('fu.companyID','=', $companyID)

        ;

        $propotion = clone $query;

        $_propotion = $propotion

            ->group_by('req.status')
            ->execute()
            ->as_array();

        $propotion = array_fill(0,5,0);

        $key = 0;

        foreach ($_propotion as $row){

            switch ($row['status']){
                case('active.have-no-proposition'):         $key = 4; break;
                case('active.have-not-confirmed'):          $key = 3; break;
                case('active.have-confirmed'):              $key = 2; break;
                case('completed'):                          $key = 1; break;
                case('outdated'):                           $key = 0; break;
            }

            $propotion[$key]        = (int) $row['count'];

        }

        $this->data['proportion'] = [
            'labels'    => $labels,
            'data'      => $propotion,
        ];

        # групуємо по дням поточного місяця

        if($this->post['curGroup'] == 'month'){

            list($year, $month) = explode('/', $this->post['curMonth']);

            $days = cal_days_in_month(CAL_GREGORIAN, (int) $month, (int) $year); // 31

            $current = clone $query;

            $current_ = $current
                ->select([DB::expr('DAY(`req`.`created`)'), 'day'])
                ->and_where_open()
                ->where(DB::expr('YEAR(`req`.`created`)'),      '=', (int) $year)
                ->where(DB::expr('MONTH(`req`.`created`)'),     '=', (int) $month)
                ->and_where_close()
                ->group_by(DB::expr('DAY(`req`.`created`)'));

            $current_ = $current->execute()->as_array();


            $before = clone $query;
            $before = $before

                ->and_where_open()

                    ->or_where_open()
                        ->where(DB::expr('YEAR(`req`.`created`)'),      '=', (int) $year)
                        ->where(DB::expr('MONTH(`req`.`created`)'),     '<', (int) $month)
                    ->or_where_close()

                    ->or_where(DB::expr('YEAR(`req`.`created`)'),      '<', (int) $year)


                ->and_where_close()
            ;

            $before = $before->execute()->as_array();
            $before = (int) $before[0]['count'];

            $current = [];

            foreach ($current_ as $row){

                $k = (int) $row['day'];

                if(!array_key_exists($k, $current))
                    $current[$k] = [];

                $current[$k] = (int) $row['count'];

            }

            ksort($current);

            $result = [
                'range'             => array_fill(0,$days, 0),
                'rangeANDbefore'    => array_fill(0,$days, 0),
            ];

            foreach ($current as $day => $count){

                $result['range']            = Helper_Array::array_merge_indexed( $result['range'],              array_fill($day - 1, $days, $count) );
                $result['rangeANDbefore']   = Helper_Array::array_merge_indexed( $result['rangeANDbefore'],     array_fill($day - 1, $days, $count + $before) );

            }

            $this->data['chart'] = [

                'labels'    => [
                    'i18n' => false,
                    'list' => array_keys( array_fill(1, $days,null) ),
                ],

                'hint' => [
                    'i18n' => false,
                    'before'  => $year.'/'.$month.'/',
                    'from'  => '01',
                    'to'    => $days,
                ],

                'dataset' => [

                    [
                        'i18n_name' => $chart_name,
                        'data'      => $result['range'],
                    ],

                ],
                'dataset_with_before' => [

                    [
                        'data'      => $result['rangeANDbefore'],
                    ]

                ]

            ];

        }else{

            $year = $this->post['curYear'];

            $current = clone $query;

            $current_ = $current
                ->select([DB::expr('MONTH(`req`.`created`)'), 'month'])
                ->where(DB::expr('YEAR(`req`.`created`)'),      '=', (int) $year)
                ->group_by(DB::expr('MONTH(`req`.`created`)'));

            $current_ = $current->execute()->as_array();


            $before = clone $query;
            $before = $before->where(DB::expr('YEAR(`req`.`created`)'),      '<', (int) $year);

            $before = $before->execute()->as_array();
            $before = (int) $before[0]['count'];

            $current = [];

            foreach ($current_ as $row){

                $current[ (int) $row['month'] ] = (int) $row['count'];

            }

            ksort($current);

            $result = ['c'=>array_fill(0, 12, null), 'b'=>array_fill(0, 12, null)]; // current and before;

            foreach ($current as $month => $count){

                $result['c']   = Helper_Array::array_merge_indexed( $result['c'],     array_fill($month-1, 12 - $month, $count) );
                $result['b']   = Helper_Array::array_merge_indexed( $result['b'],     array_fill($month-1, 12 - $month, $count + $before) );

            }

            $this->data['chart'] = [

                'labels'    => [
                    'i18n' => true,
                    'list' => self::$months['nominative'],
                ],

                'hint' => [
                    'i18n' => true,
                    'before'  => $year,
                    'from'  => self::$months['genitive'][0],
                    'to'    => self::$months['genitive'][11],
                ],

                'dataset' => [

                        [
                            'i18n_name' => $chart_name,
                            'data'      => $result['c'],
                        ],

                ],
                'dataset_with_before' => [

                    [
                        'data'      => $result['b'],
                    ]

                ]
            ];


        }

    }

}