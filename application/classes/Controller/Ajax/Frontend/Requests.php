<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Frontend_Requests extends Ajax {

    public $debug = true;

    /** @var Model_Frontend_User */
    public $user;

    public $localeID;
    public $localeUri;

    public function before() {
        parent::before();

        if (($userID = Frontend_Auth::userID()) === false) {

            $this->add_error('profile.auth.not.authorized');
            $this->run = false;
        }

        $this->user = ORM::factory('Frontend_User', $userID);
        /** @var $user Model_Frontend_User */

        if (!$this->user->loaded()) {
            $this->add_error('invalid.data');
            $this->run = false;
        }

        $this->localeID = Arr::get($this->post, 'localeID', null);
        if ($this->localeID != null) {
            $orm_locale = ORM::factory('Frontend_Locale', $this->localeID);
            if (!$orm_locale->loaded()) {
                $this->add_error('invalid.data');
                $this->run = false;
            }
            else {
                $this->localeUri = $orm_locale->uri;
            }
        }
    }

    protected function formInfoJson($request) {
        $json = [];
        foreach ($request as $key => $value) {
            if (strpos($key, 'json_') === 0) {
                $json[substr($key, strlen('json_'))] = $value;
            }
        }

        if (!empty($request['_json_advancedPayment'])) {
            $unit = trim($request['_json_advancedPayment']);
        }
        else {
            $unit = $request['_json_currency_code'];
        }

        $json['price'] = $request['_json_price'] . '|' . $unit;


        return '<script type="application/json">'. json_encode($json) . '</script>';

    }

    protected function getCompanyImage($company_id) {
        $avatar = DB::select('userID')
                    ->from('Frontend_User_to_Company_Ownership')
                    ->where('companyID', '=', $company_id)
                    ->limit(1)
                    ->execute()
                    ->as_array();

        return (empty($avatar))
            ? Helper_User::avatar('-')
            : Helper_User::avatar($avatar[0]['userID']);
    }

    protected function processCargoRequest($request) {
        $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
        $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

        if ($request['dateTo'] != null) {
            $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
        }

        if ($request['json_sizeX'] != null) {
            $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
        }
        if ($request['json_sizeY'] != null) {
            $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
        }
        if ($request['json_sizeZ'] != null) {
            $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
        }

        $actions = $this->getRequestActions('cargo', $request['ID']);

        $request['js_actions'][] = $actions['message'];
        $request['js_actions'][] = $actions['get_doc'];
        $request['props_count'] = 0;
        // підготовка даних для js
        if ($request['status'] == 'active.have-no-proposition') {
            $request['classStatus'] = 'success';
            $request['js_status']   = 'нова';

            $request['js_actions'][] = $actions['edit'];
            $request['js_actions'][] = $actions['services'];
            $request['js_actions'][] = $actions['delete'];
        }
        elseif ($request['status'] == 'active.have-not-confirmed') {
            $request['classStatus'] = 'warning';

            $request['js_actions'][] = $actions['edit'];
            $request['js_actions'][] = $actions['services'];
            $request['js_actions'][] = $actions['delete'];
            $request['js_actions'][] = $actions['view_props'];

            $query_count = DB::select([DB::expr('COUNT(*)'), 'c'])
                             ->from(Model_Frontend_User_Cargo_Application::TABLE)
                             ->where('cargoID', '=', $request['ID'])
                             ->and_where('status', '=', DB::expr("'pending'"))
                             ->execute()
                             ->as_array();

            $request['props_count'] = $query_count[0]['c'];
//            $request['js_status']   = 'є пропозиції ' . '<div class="numb tablePropsCount"><sup>' . $request['props_count'] . '</sup></div>';
            $request['js_status']   = 'є пропозиції ' . '<a class="additionalInfo cargo-ticket-props" data-id="' . $request['ID'] . '" href="javascript:;"><i class="fa fa-files-o" aria-hidden="true"></i> '. $request['props_count'] .'</a>';
        }
        elseif ($request['status'] == 'active.have-confirmed') {
            $request['classStatus'] = 'danger';
            $request['js_status']   = 'у роботі';

            $request['js_actions'][] = $actions['close'];
        }
        elseif ($request['status'] == 'completed') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'завершена';
        }
        elseif ($request['status'] == 'outdated') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'просрочена';
        }

        if ($request['new_views'] > 0) {
            $request['js_status'] .= ' <a href="javascript:;" class="additionalInfo"><i class="fa fa-eye" aria-hidden="true"></i> +' . $request['new_views'] . '</a>';
            Model_Frontend_Cargo::query_dropViews($request['ID'], $request['views']);
        }

        $request['js_date']   = $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '');
        $request['js_from']   = $request['from_name'] . '(' . $request['from_country'] . ')';
        $request['js_to']     = $request['to_name'] . '(' . $request['to_country'] . ')';
        $request['js_z']      = Frontend_Helper_String::none_if_null($request['sizeZ'], (float)$request['sizeZ'] . ' м');
        $request['js_weight'] = Frontend_Helper_String::none_if_null($request['weight'], (float)$request['weight'] . ' т');
        $request['js_type']   = __db($request['car_type_localeName']);
        $request['js_price']  = Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']);

        if (array_key_exists('appCompID', $request)) {
            $request['js_action'] = ($request['appCompID'] != null)
                ? Route::url('frontend_site_company_page', [
                    'lang' => $this->localeUri,
                    'company_url_name' => Controller_Frontend_Company::getCompanyUrlName([
                        'ID' => $request['appCompID'],
                        'primaryName' => 'company',
                        'url' => ORM::factory('Frontend_Company', $request['appCompID'])->url
                    ])
                ])
                : null;
        }

        $request['json_info'] = $this->formInfoJson($request);

        return $request;
    }

    protected function processProposition($request) {
//        $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
//        $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));
//
//        if ($request['dateTo'] != null) {
//            $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
//        }

        $actions = $this->getPropositionActions('transport', $request['ID']);

        $request['js_prop_actions'][] = $actions['message'];

        // підготовка даних для js
        if ($request['status'] == 'active.have-not-confirmed') {
            $request['classStatus'] = 'warning';
            $request['js_status']   = 'розглядається';
            $request['js_prop_actions'][] = $actions['delete'];
        }
        elseif ($request['status'] == 'active.have-confirmed') {
            $request['classStatus'] = 'danger';
            $request['js_status']   = 'у роботі';
        }
        elseif ($request['status'] == 'completed') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'завершена';
        }
        elseif ($request['status'] == 'outdated') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'просрочена';
        }

        $request['user2ID'] = $this->user->ID;

        $request['js_date']   = $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '');
        $request['js_from']   = $request['from_name'] . '(' . $request['from_country'] . ')';
        $request['js_to']     = $request['to_name'] . '(' . $request['to_country'] . ')';
        $request['js_z']      = Frontend_Helper_String::none_if_null($request['car_sizeZ'], (float)$request['car_sizeZ'] . ' м');
        $request['js_weight'] = Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float)$request['car_liftingCapacity'] . ' т');
        $request['js_type']   = __db($request['car_type_localeName']);
        $request['js_price']  = Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']);

        if (array_key_exists('appCompID', $request)) {
            $request['js_action'] = ($request['appCompID'] != null)
                ? Route::url('frontend_site_company_page', [
                    'lang' => $this->localeUri,
                    'company_url_name' => Controller_Frontend_Company::getCompanyUrlName([
                        'ID' => $request['appCompID'],
                        'primaryName' => 'company',
                        'url' => ORM::factory('Frontend_Company', $request['appCompID'])->url
                    ])
                ])
                : null;
        }

        return $request;
    }

    protected function processCargoProposition($request) {
//        $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
//        $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

//        if ($request['dateTo'] != null) {
//            $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
//        }

        $actions = $this->getPropositionActions('cargo', $request['ID']);

        $request['js_prop_actions'][] = $actions['message'];

        // підготовка даних для js
        if ($request['status'] == 'active.have-not-confirmed') {
            $request['classStatus'] = 'warning';
            $request['js_status']   = 'розглядається';
            $request['js_prop_actions'][] = $actions['delete'];
        }
        elseif ($request['status'] == 'active.have-confirmed') {
            $request['classStatus'] = 'danger';
            $request['js_status']   = 'у роботі';
        }
        elseif ($request['status'] == 'completed') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'завершена';
        }
        elseif ($request['status'] == 'outdated') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'просрочена';
        }

        $request['user2ID'] = $this->user->ID;

        $request['js_date']   = $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '');
        $request['js_from']   = $request['from_name'] . '(' . $request['from_country'] . ')';
        $request['js_to']     = $request['to_name'] . '(' . $request['to_country'] . ')';
        $request['js_z']      = Frontend_Helper_String::none_if_null($request['sizeZ'], (float)$request['sizeZ'] . ' м');
        $request['js_weight'] = Frontend_Helper_String::none_if_null($request['weight'], (float)$request['weight'] . ' т');
        $request['js_type']   = __db($request['car_type_localeName']);
        $request['js_price']  = Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']);

        if (array_key_exists('appCompID', $request)) {
            $request['js_action'] = ($request['appCompID'] != null)
                ? Route::url('frontend_site_company_page', [
                    'lang' => $this->localeUri,
                    'company_url_name' => Controller_Frontend_Company::getCompanyUrlName([
                        'ID' => $request['appCompID'],
                        'primaryName' => 'company',
                        'url' => ORM::factory('Frontend_Company', $request['appCompID'])->url
                    ])
                ])
                : null;
        }

        return $request;
    }

    protected function getPropositionActions($type, $proposition_ID) {
        $new_msg_count = Model_Frontend_Request_Dialog::query_getNewForPropositionCount($type, $proposition_ID);

        return [
            'message' => [
                'html' => '<a href="javascript:;" data-id="' . $proposition_ID . '" class="' . $type . '-prop-message"><i class="fa fa-envelope" aria-hidden="true"></i>'.(($new_msg_count > 0) ? '+'.$new_msg_count : '').'</a>'
            ],
            'delete' => [
                'html' => '<a href="javascript:;" data-id="' . $proposition_ID . '" class="' . $type . '-prop-delete"><span class="stop">stop</span></a>'
//                'html' => '<a class="' . $type . '-prop-delete" data-id="' . $proposition_ID . '" href="javascript:;">відмовитись</a>',
            ]
        ];
    }

    protected function getRequestActions($type, $request_ID) {
        $new_msg_count = Model_Frontend_Request_Dialog::query_getNewForRequestCount($type, $request_ID);

        return [
            'edit' => [
                'html' => '<a href="'. Route::url('frontend_site_request_add_' . $type, ['lang' => $this->localeUri]) . '?edit_id=' . $request_ID . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'
            ],
            'message' => [
                'html' => '<a href="javascript:;" data-id="' . $request_ID . '" class="' . $type . '-ticket-message"><i class="fa fa-envelope" aria-hidden="true"></i>'.(($new_msg_count > 0) ? '+'.$new_msg_count : '').'</a>'
            ],
            'delete' => [
                'html' => '<a href="javascript:;" data-id="' . $request_ID . '" class="' . $type . '-ticket-delete"><span class="delete"><i class="fa fa-times" aria-hidden="true"></i></span></a>'
//                'html' => '<a class="' . $type . '-ticket-delete" data-id="' . $request_ID . '" href="javascript:;">видалити</a>',
            ],
            'get_doc' => [
                'html' => '<a href="'. Route::url('frontend_request_doc', ['type' => $type, 'id' => $request_ID]) .'"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>',
            ],
            'view_props' => [
                'html' => '',
//                'html' => '<a class="' . $type . '-ticket-props" data-id="' . $request_ID . '" href="javascript:;">пропозиції</a>',
            ],
            'close' => [
                'html' => '',
//                'html' => '<a class="' . $type . '-ticket-close" data-id="' . $request_ID . '" href="javascript:;">закрити</a>',
            ],
            'services' => [
                'html' => '<a href="javascript:;" data-id="' . $request_ID . '" class="' . $type . '-ticket-order"><i class="fa fa-money" aria-hidden="true"></i></a>',
//                'html' => '<a class="' . $type . '-ticket-order" data-id="' . $request_ID . '" href="javascript:;">послуги</a>',
            ]
        ];
    }

    protected function processRequest($request) {
        $request['created']  = date('d.m.y G:i:s', strtotime($request['created']));
        $request['dateFrom'] = date('d.m', strtotime($request['dateFrom']));

        if ($request['dateTo'] != null) {
            $request['dateTo'] = date('d.m', strtotime($request['dateTo']));
        }

        if ($request['json_sizeX'] != null) {
            $request['json_sizeX'] = (float)$request['json_sizeX'] . ' м';
        }
        if ($request['json_sizeY'] != null) {
            $request['json_sizeY'] = (float)$request['json_sizeY'] . ' м';
        }
        if ($request['json_sizeZ'] != null) {
            $request['json_sizeZ'] = (float)$request['json_sizeZ'] . ' м';
        }

        $actions = $this->getRequestActions('transport', $request['ID']);

        $request['js_actions'][] = $actions['message'];
        $request['js_actions'][] = $actions['get_doc'];
        $request['props_count'] = 0;
        // підготовка даних для js
        if ($request['status'] == 'active.have-no-proposition') {
            $request['classStatus'] = 'success';
            $request['js_status']   = 'нова';

            $request['js_actions'][] = $actions['edit'];
            $request['js_actions'][] = $actions['services'];
            $request['js_actions'][] = $actions['delete'];
        }
        elseif ($request['status'] == 'active.have-not-confirmed') {
            $request['classStatus'] = 'warning';

            $request['js_actions'][] = $actions['edit'];
            $request['js_actions'][] = $actions['services'];
            $request['js_actions'][] = $actions['delete'];
            $request['js_actions'][] = $actions['view_props'];

            $query_count = DB::select([DB::expr('COUNT(*)'), 'c'])
                ->from(Model_Frontend_User_Transport_Application::TABLE)
                ->where('transportID', '=', $request['ID'])
                ->and_where('status', '=', DB::expr("'pending'"))
                ->execute()
                ->as_array();

            $request['props_count'] = $query_count[0]['c'];
//            $request['js_status']   = 'є пропозиції ' . '<div class="numb tablePropsCount"><sup>' . $request['props_count'] . '</sup></div>';
            $request['js_status']   = 'є пропозиції ' . '<a class="additionalInfo transport-ticket-props" data-id="' . $request['ID'] . '" href="javascript:;"><i class="fa fa-files-o" aria-hidden="true"></i> '. $request['props_count'] .'</a>';
        }
        elseif ($request['status'] == 'active.have-confirmed') {
            $request['classStatus'] = 'danger';
            $request['js_status']   = 'у роботі';

            $request['js_actions'][] = $actions['close'];
        }
        elseif ($request['status'] == 'completed') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'завершена';
        }
        elseif ($request['status'] == 'outdated') {
            $request['classStatus'] = 'info';
            $request['js_status']   = 'просрочена';
        }

        if ($request['new_views'] > 0) {
            $request['js_status'] .= ' <a href="javascript:;" class="additionalInfo"><i class="fa fa-eye" aria-hidden="true"></i> +' . $request['new_views'] . '</a>';
            Model_Frontend_Transport::query_dropViews($request['ID'], $request['views']);
        }

        $request['js_date']   = $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '');
        $request['js_from']   = $request['from_name'] . '(' . $request['from_country'] . ')';
        $request['js_to']     = $request['to_name'] . '(' . $request['to_country'] . ')';
        $request['js_z']      = Frontend_Helper_String::none_if_null($request['car_sizeZ'], (float)$request['car_sizeZ'] . ' м');
        $request['js_weight'] = Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float)$request['car_liftingCapacity'] . ' т');
        $request['js_type']   = __db($request['car_type_localeName']);
        $request['js_price']  = Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']);

        if (array_key_exists('appCompID', $request)) {
            $request['js_action'] = ($request['appCompID'] != null)
                ? Route::url('frontend_site_company_page', [
                    'lang' => $this->localeUri,
                    'company_url_name' => Controller_Frontend_Company::getCompanyUrlName([
                        'ID' => $request['appCompID'],
                        'primaryName' => 'company',
                        'url' => ORM::factory('Frontend_Company', $request['appCompID'])->url
                    ])
                ])
                : null;
        }

        $request['json_info'] = $this->formInfoJson($request);

        return $request;
    }

    public function get_transport_propositions() {
        $limit  = 30;
        $offset = 0;

        $query = Model_Frontend_Cargo::query_getCargoInfoSelect($this->localeID, false, $limit, $offset, []);
        $query
            ->join([Model_Frontend_User_Cargo_Application::TABLE, 't_user_app'], 'LEFT')
            ->on('t_user_app.cargoID', '=', 't_cargo.ID')
            ->and_where('t_user_app.userID', '=', DB::expr($this->user->ID))
            ->and_where('t_user_app.status', '<>', DB::expr("'rejected'"))
            ->select(['t_user_app.status', 'appStatus']);

        $requests = $query
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        $requests = array_map('Controller_Ajax_Frontend_Requests::processCargoRequest', Model_Frontend_Cargo::assignPlacesToCargo($this->localeID, $requests));

        $this->data['requests'] = array_map('Controller_Ajax_Frontend_Requests::processCargoProposition', $requests);
    }

    public function get_cargo_propositions() {
        $limit  = 30;
        $offset = 0;

        $query = Model_Frontend_Transport::query_getTransportInfoSelect($this->localeID, false, $limit, $offset, []);
        $query
            ->join([Model_Frontend_User_Transport_Application::TABLE, 't_user_app'], 'LEFT')
            ->on('t_user_app.transportID', '=', 't_trans.ID')
            ->and_where('t_user_app.userID', '=', DB::expr($this->user->ID))
            ->and_where('t_user_app.status', '<>', DB::expr("'rejected'"))
            ->select(['t_user_app.status', 'appStatus']);

        $requests = $query
            ->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        $requests               = array_map('Controller_Ajax_Frontend_Requests::processRequest', Model_Frontend_Transport::assignPlacesToTransport($this->localeID, $requests));

        $this->data['requests'] = array_map('Controller_Ajax_Frontend_Requests::processProposition', $requests);
    }


    public function close_transport_ticket() {
        $requestID = Arr::get($this->post, 'requestID');
        $rating    = Arr::get($this->post, 'rating');
        $feedback  = Arr::get($this->post, 'feedback');

        try {
            Model_Frontend_Transaction::start();

            $orm_comment         = ORM::factory('Frontend_Comment');
            $orm_comment->text   = $feedback;
            $orm_comment->rating = ($rating == 'positive') ? 'positive' : 'negative';

            $orm_comment->transportID = $requestID;
            $orm_comment->cargoID     = null;

            // шукаєм виконавця
            $orm_app  = ORM::factory('Frontend_User_Transport_Application')
                           ->where('transportID', '=', $requestID)
                           ->and_where('status', '=', DB::expr("'accepted'"))
                           ->find();
            $orm_user = ORM::factory('Frontend_User', $orm_app->userID);

            $orm_comment->userID    = $this->user->ID; // наш відгук
            $orm_comment->companyID = $orm_user->companyID; // на компанію виконавця
            $orm_comment->save();

            $orm_request         = ORM::factory('Frontend_Transport', $requestID);
            $orm_request->status = 'completed';
            $orm_request->save();

            $this->add_success('Заявку було успішно закрито!');

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            $this->add_error('user.permission.denied');
        }
    }

    public function close_cargo_ticket() {
        $requestID = Arr::get($this->post, 'requestID');
        $rating    = Arr::get($this->post, 'rating');
        $feedback  = Arr::get($this->post, 'feedback');

        try {
            Model_Frontend_Transaction::start();

            $orm_comment         = ORM::factory('Frontend_Comment');
            $orm_comment->text   = $feedback;
            $orm_comment->rating = ($rating == 'positive') ? 'positive' : 'negative';

            $orm_comment->cargoID = $requestID;
            $orm_comment->transportID = null;

            // шукаєм виконавця
            $orm_app  = ORM::factory('Frontend_User_Cargo_Application')
                           ->where('cargoID', '=', $requestID)
                           ->and_where('status', '=', DB::expr("'accepted'"))
                           ->find();
            $orm_user = ORM::factory('Frontend_User', $orm_app->userID);

            $orm_comment->userID    = $this->user->ID; // наш відгук
            $orm_comment->companyID = $orm_user->companyID; // на компанію виконавця
            $orm_comment->save();

            $orm_request         = ORM::factory('Frontend_Cargo', $requestID);
            $orm_request->status = 'completed';
            $orm_request->save();

            $this->add_success('user.ticket.closed');

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            $this->add_error('user.permission.denied');
        }
    }

    public function get_transport_requests() {
        $this->localeID = Arr::get($this->post, 'localeID', 0);
        $type           = Arr::get($this->post, 'type');

        $filters = [
            ['t_trans.userID', '=', $this->user->ID]
        ];
        if ($type != null) {
            $filters[] = ['t_trans.status', '=', $type];
        }

        $requests = array_map('Controller_Ajax_Frontend_Requests::processRequest', Model_Frontend_Transport::query_getTransportTickets($this->localeID, false, 30, 0, $filters));

        $this->data['requests'] = $requests;
    }

    public function get_cargo_requests() {
        $type = Arr::get($this->post, 'type');

        $filters = [
            ['t_cargo.userID', '=', $this->user->ID]
        ];
        if ($type != null) {
            $filters[] = ['t_cargo.status', '=', $type];
        }

        $requests = array_map('Controller_Ajax_Frontend_Requests::processCargoRequest', Model_Frontend_Cargo::query_getCargoTickets($this->localeID, false, 30, 0, $filters));

        $this->data['requests'] = $requests;
    }

    public function set_transport_proposition() {
        $appID  = Arr::get($this->post, 'appID', 0);
        $action = Arr::get($this->post, 'action');

        try {
            Model_Frontend_Transaction::start();

            $orm_app = ORM::factory('Frontend_User_Transport_Application', $appID);

            $message = null;
            switch ($action) {
                case 'accept':
                    $message = 'user.application.prop.accepted';

                    // відхиляєм усі інші пропозиції
                    $orm_other_apps = ORM::factory('Frontend_User_Transport_Application')
                                         ->where('transportID', '=', $orm_app->transportID)
                                         ->and_where('ID', '<>', $orm_app->ID)
                                         ->find_all();
                    foreach ($orm_other_apps as $app) {
                        $app->status = 'rejected';
                        $app->save();
                    }

                    // зберігаємо цю пропозицію як прийняту
                    $orm_app->status = 'accepted';
                    $orm_app->save();

                    // і міняємо статус заявки на заявку в роботі
                    $request         = ORM::factory('Frontend_Transport', $orm_app->transportID);
                    $request->status = 'active.have-confirmed'; // міняємо статус заявки
                    $request->save();

                    break;
                case 'reject':
                    $orm_app->status = 'rejected';
                    $orm_app->save();

                    $message        = 'user.application.prop.rejected';
                    $check_for_apps = ORM::factory('Frontend_User_Transport_Application', [
                        'transportID' => $orm_app->transportID,
                        'status' => DB::expr("'pending'")
                    ])->loaded();

                    $this->data = $check_for_apps;
                    if (!$check_for_apps) { // якщо це була остання пропозиція
                        $request         = ORM::factory('Frontend_Transport', $orm_app->transportID);
                        $request->status = 'active.have-no-proposition'; // міняємо статус заявки
                        $request->save();
                    }

                    break;
                default:
                    throw new Exception('wrong input data');
            }

            if ($message != null) {
                $this->add_success($message);
            }

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            $this->add_error('user.permission.denied');
        }
    }

    public function set_cargo_proposition() {
        $appID  = Arr::get($this->post, 'appID', 0);
        $action = Arr::get($this->post, 'action');

        try {
            Model_Frontend_Transaction::start();

            $orm_app = ORM::factory('Frontend_User_Cargo_Application', $appID);

            $message = null;
            switch ($action) {
                case 'accept':
                    $message = 'user.application.prop.accepted';

                    // відхиляєм усі інші пропозиції
                    $orm_other_apps = ORM::factory('Frontend_User_Cargo_Application')
                                         ->where('cargoID', '=', $orm_app->cargoID)
                                         ->and_where('ID', '<>', $orm_app->ID)
                                         ->find_all();
                    foreach ($orm_other_apps as $app) {
                        $app->status = 'rejected';
                        $app->save();
                    }

                    // зберігаємо цю пропозицію як прийняту
                    $orm_app->status = 'accepted';
                    $orm_app->save();

                    // і міняємо статус заявки на заявку в роботі
                    $request         = ORM::factory('Frontend_Cargo', $orm_app->cargoID);
                    $request->status = 'active.have-confirmed'; // міняємо статус заявки
                    $request->save();

                    break;
                case 'reject':
                    $orm_app->status = 'rejected';
                    $orm_app->save();

                    $message        = 'user.application.prop.rejected';
                    $check_for_apps = ORM::factory('Frontend_User_Cargo_Application', [
                        'cargoID' => $orm_app->cargoID,
                        'status' => DB::expr("'pending'")
                    ])->loaded();

                    $this->data = $check_for_apps;
                    if (!$check_for_apps) { // якщо це була остання пропозиція
                        $request         = ORM::factory('Frontend_Cargo', $orm_app->cargoID);
                        $request->status = 'active.have-no-proposition'; // міняємо статус заявки
                        $request->save();
                    }

                    break;
                default:
                    throw new Exception('wrong input data');
            }

            if ($message != null) {
                $this->add_success($message);
            }

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();

            $this->add_error('user.permission.denied');
        }
    }

    public function get_transport_ticket_companies() {
        $request_ID = Arr::get($this->post, 'requestID');
        I18n::loadDB($this->localeID); // ініціалізація модуля локалізації Богдана

        $companies_temp    = Model_Frontend_User_Transport_Application::query_getCompaniesIdsByRequest($this->user->ID, $request_ID);
        $companies_ids     = [];
        $companies_id_apps = [];
        foreach ($companies_temp as $company) {
            $companies_ids[]                   = $company['ID'];
            $companies_id_apps[$company['ID']] = $company['appID'];
        }

        if (count($companies_ids) > 0) {
            $filters = [
                ['ID', 'IN', $companies_ids]
            ];
            $sorts   = [];

            $companies = Model_Frontend_Company::query_getCompaniesWithLocales(false, $this->localeID, false, 4, 0, $filters, $sorts);
            $result    = [];
            foreach ($companies as $company) {
                $company['type'] = __db($company['typeLocale']);

                $params         = array_merge($this->request->param(), ['lang' => $this->localeUri, 'company_url_name' => Controller_Frontend_Company::getCompanyUrlName($company)]);
                $company['url'] = Route::url('frontend_site_company_page', $params);

                $company['image'] = $this->getCompanyImage($company['ID']);
                $company['appID'] = $companies_id_apps[$company['ID']];

                $result[] = $company;
            }

            $this->data['companies'] = $result;
        }
        else {
            $this->data['companies'] = [];
        }
    }

    public function get_cargo_ticket_companies() {
        $request_ID = Arr::get($this->post, 'requestID');
        I18n::loadDB($this->localeID); // ініціалізація модуля локалізації Богдана

        $companies_temp    = Model_Frontend_User_Cargo_Application::query_getCompaniesIdsByRequest($this->user->ID, $request_ID);
        $companies_ids     = [];
        $companies_id_apps = [];
        foreach ($companies_temp as $company) {
            $companies_ids[]                   = $company['ID'];
            $companies_id_apps[$company['ID']] = $company['appID'];
        }

        if (count($companies_ids) > 0) {
            $filters = [
                ['ID', 'IN', $companies_ids]
            ];
            $sorts   = [];

            $companies = Model_Frontend_Company::query_getCompaniesWithLocales(false, $this->localeID, false, 4, 0, $filters, $sorts);
            $result    = [];
            foreach ($companies as $company) {
                $company['type'] = __db($company['typeLocale']);

                $params         = array_merge($this->request->param(), ['lang' => $this->localeUri, 'company_url_name' => Controller_Frontend_Company::getCompanyUrlName($company)]);
                $company['url'] = Route::url('frontend_site_company_page', $params);

                $company['image'] = $this->getCompanyImage($company['ID']);
                $company['appID'] = $companies_id_apps[$company['ID']];

                $result[] = $company;
            }

            $this->data['companies'] = $result;
        }
        else {
            $this->data['companies'] = [];
        }
    }

    public function delete_transport_proposition() {
        $request_ID = Arr::get($this->post, 'requestID');

        $app = ORM::factory('Frontend_User_Cargo_Application')
                  ->where('cargoID', '=', $request_ID)
                  ->and_where('userID', '=', $this->user->ID)
                  ->and_where('status', '=', 'pending')
                  ->find();


        if ($app->loaded()) {
            try {
                Model_Frontend_Transaction::start();

                $app->delete();
                $other_propositions = DB::select()
                                        ->from(Model_Frontend_User_Cargo_Application::TABLE)
                                        ->where('cargoID', '=', $request_ID)
                                        ->and_where('status', '=', 'pending')
                                        ->execute()
                                        ->as_array();

                $prop_count = count($other_propositions);
                if ($prop_count == 0) {
                    $orm_request         = ORM::factory('Frontend_Cargo', $request_ID);
                    $orm_request->status = 'active.have-no-proposition';
                    $orm_request->save();
                }

                Model_Frontend_Transaction::commit();
            }
            catch (Exception $e) {
                Model_Frontend_Transaction::rollback();
            }
        }
    }

    public function delete_cargo_proposition() {
        $request_ID = Arr::get($this->post, 'requestID');

        $app = ORM::factory('Frontend_User_Transport_Application')
                  ->where('transportID', '=', $request_ID)
                  ->and_where('userID', '=', $this->user->ID)
                  ->find();

        if ($app->loaded()) {
            try {
                Model_Frontend_Transaction::start();

                $app->delete();
                $other_propositions = DB::select()
                                        ->from(Model_Frontend_User_Transport_Application::TABLE)
                                        ->where('transportID', '=', $request_ID)
                                        ->and_where('status', '=', 'pending')
                                        ->execute()
                                        ->as_array();

                $prop_count = count($other_propositions);
                if ($prop_count == 0) {
                    $orm_request         = ORM::factory('Frontend_Transport', $request_ID);
                    $orm_request->status = 'active.have-no-proposition';
                    $orm_request->save();
                }

                Model_Frontend_Transaction::commit();
            }
            catch (Exception $e) {
                Model_Frontend_Transaction::rollback();
            }

        }
    }

    public function delete_transport_request() {
        $request_ID = Arr::get($this->post, 'requestID');

        $request = ORM::factory('Frontend_Transport')
                      ->where('ID', '=', $request_ID)
                      ->and_where('userID', '=', $this->user->ID)
                      ->and_where_open()
                      ->where('status', '=', 'active.have-no-proposition')
                      ->or_where('status', '=', 'active.have-not-confirmed')
                      ->and_where_close()
                      ->find();

        if ($request->loaded()) {
            $request->delete();

            $this->add_success('Заявку було успішно видалено!');
        }
    }

    public function delete_cargo_request() {
        $request_ID = Arr::get($this->post, 'requestID');

        $request = ORM::factory('Frontend_Cargo')
                      ->where('ID', '=', $request_ID)
                      ->and_where('userID', '=', $this->user->ID)
                      ->and_where_open()
                      ->where('status', '=', 'active.have-no-proposition')
                      ->or_where('status', '=', 'active.have-not-confirmed')
                      ->and_where_close()
                      ->find();

        if ($request->loaded()) {
            $request->delete();

            $this->add_success('Заявку було успішно видалено!');
        }
    }

    public function request_add_view() {
        $request_ID = Arr::get($this->post, 'id');
        $type = Arr::get($this->post, 'type');

        try {
            if ($type == 'transport') {
                $model = ORM::factory('Frontend_Transport', $request_ID);
            }
            else {
                $model = ORM::factory('Frontend_Cargo', $request_ID);
            }

            $model->views = $model->views + 1;
            $model->save();
        }
        catch (Exception $e) {
        }
    }

    public function request_order() {
        $request_ID = Arr::get($this->post, 'requestID');
        $type = Arr::get($this->post, 'type');
        $order_type = Arr::get($this->post, 'orderType');
        $duration = Arr::get($this->post, 'duration');

        try {
            Model_Frontend_Transaction::start();

            if ($type == 'cargo') {
                $request = ORM::factory('Frontend_Cargo', $request_ID);
            }
            else {
                $request = ORM::factory('Frontend_Transport', $request_ID);
            }

            $calc_price = 0;
            $service_name = '';
            switch ($order_type) {
                case 'vip':
                    $calc_price = $duration * 120;

                    $request->isVip = 'yes';
                    $service_name       = 'wallet.serviceVipRequest';
                    break;
                case 'color':
                    $calc_price = $duration * 120;

                    $request->isColored = 'yes';
                    $service_name       = 'wallet.serviceColoredRequest';
                    break;
                case 'to_top':
                    $calc_price = 9;

                    $request->created = date('Y-m-d H:i:s');
                    $service_name = 'wallet.serviceToTopRequest';
                    break;
            }

            if ($calc_price > $this->user->wallet) {
                $this->add_error('Вартість обраної послуги ' . $calc_price . ' грн перевищує поточний баланс Вашого гаманця (' . (float)$this->user->wallet . ' грн)');
                throw new Exception();
            }

            // знімаємо за послугу
            Model_Frontend_Wallet_History::query_insertHistory(
                $this->user->ID,
                'service',
                $service_name,
                -$calc_price // TODO: читання вартості послуги?
            );
            $this->user->wallet = $this->user->wallet - $calc_price;
            $this->user->save();
            $request->save();

            $this->add_success('Послугу успішно активовано');

            Model_Frontend_Transaction::commit();
        }
        catch (Exception $e) {
            Model_Frontend_Transaction::rollback();
        }
    }

    public function get_subscriptions() {
        $type = Arr::get($this->post, 'type'); // 'cargo' or 'transport'
        $subscribe = Model_Frontend_Company::query_get_transports_cargo_subscribe_request($this->user->ID, $type);
        $subscribe = array_map(function($n){ return $n['ID']; }, $subscribe);
        $subscribe[] = 0;
        $this->data['subscribe'] = $subscribe;
        $this->data['type'] = $type;
        $this->data['status'] = 'ok';

        $this->localeID = Arr::get($this->post, 'localeID', 0);
        $type           = Arr::get($this->post, 'type');

        if ($type == 'cargo') {
            $filters = [
                ['t_cargo.ID', 'IN', $subscribe]
            ];

            $requests = array_map('Controller_Ajax_Frontend_Requests::processCargoRequest', Model_Frontend_Cargo::query_getCargoTickets($this->localeID, false, 30, 0, $filters));

            $this->data['requests'] = $requests;
        }
        else {
            $filters = [
                ['t_trans.ID', 'IN', $subscribe]
            ];

            $requests = array_map('Controller_Ajax_Frontend_Requests::processRequest', Model_Frontend_Transport::query_getTransportTickets($this->localeID, false, 30, 0, $filters));

            $this->data['requests'] = $requests;
        }
    }

}