<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Base extends Controller {

    public $config;

    public function before()
    {
        parent::before();
        $config = Kohana::$config->load('cms/main');
        $this->config['browser_key']  = $config->get('browser_key');
        $this->config['server_key']  = $config->get('server_key');
        $this->config['date_to']  = $config->get('date_to');
        View::set_global('config',$this->config);
    }

}