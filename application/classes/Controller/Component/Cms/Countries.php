<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Countries extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $add_url = null;
    private $edit_url = null;
    private $case_edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
        $this->lang = $this->request->param('lang');
    }


    public function action_db_analytics()
    {
        $_dt = new Model_Component_Cms_Dt_Countries();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered();
        $res['data'] = $_dt->get($this->post);
        echo json_encode($res);
        die;
    }

}