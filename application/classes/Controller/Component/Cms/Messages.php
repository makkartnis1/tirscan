<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Messages extends Controller{

    private $base_title = null;
    private $dialog_id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/messages/v_index');
        $this->base_title = $this->post['base_title'];
        $this->dialog_id = $this->post['dialog_id'];

        $m_mess = new Model_Component_Cms_Messages();
        $checkDialog = $m_mess->checkDialogID($this->dialog_id);
        $this->view->checkDialog = $checkDialog;

        if ($checkDialog)
            $this->view->theme = $m_mess->getTheme($this->dialog_id);

        $this->view->base_title = $this->base_title;
        $this->view->id = $this->dialog_id;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Messages();
        $res = array();

        $id = $this->post['id'];

        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($id);
        $res['recordsFiltered'] = $_dt->countFiltered($id);
        $res['data'] = $_dt->get($this->post,$id);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_model = new Model_Component_Cms_Messages();
        switch ($this->post['action']) {
            case 'delete':
                $m_model->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_model->delete($id);
                }
                break;
        }
        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}