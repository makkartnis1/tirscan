<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_CargoRequests extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $add_url = null;
    private $edit_url = null;
    private $base_title = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/cargo_requests/index');
        $this->add_url = $this->post['add_url'];
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];

        $this->view->add_url = $this->add_url;
        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_price_cur = new Model_Component_Cms_Currencies();
        $this->view->price_cur = $m_price_cur->getAll();

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->type_transports = $m_transport_types->get_all();

        $filter = [];

        if (isset($_GET['related_ids']))
        {
            $filter['related_ids'] = $_GET['related_ids'];
            $filter['date_from'] = $_GET['date_from'];
            $m_transport = new Model_Component_Cms_TransportRequests();
            $this->view->req_info = $m_transport->get_one($_GET['transport_id']);

        }

        if (isset($_GET['goFilter']))
        {

            if (isset($_GET['dateCreate1']))
            {
                $dateCreate1 = $_GET['dateCreate1'];
                if ($dateCreate1)
                {
                    $filter['dateCreate1'] = $dateCreate1;
                }
            }

            if (isset($_GET['dateCreate2']))
            {
                $dateCreate2 = $_GET['dateCreate2'];
                if ($dateCreate2)
                {
                    $filter['dateCreate2'] = $dateCreate2;
                }
            }

            if (isset($_GET['dateFrom1']))
            {
                $dateFrom1 = $_GET['dateFrom1'];
                if ($dateFrom1)
                {
                    $filter['dateFrom1'] = $dateFrom1;
                }
            }

            if (isset($_GET['dateFrom2']))
            {
                $dateFrom2 = $_GET['dateFrom2'];
                if ($dateFrom2)
                {
                    $filter['dateFrom2'] = $dateFrom2;
                }
            }

            if (isset($_GET['dateTo1']))
            {
                $dateTo1 = $_GET['dateTo1'];
                if ($dateTo1)
                {
                    $filter['dateTo1'] = $dateTo1;
                }
            }

            if (isset($_GET['dateTo2']))
            {
                $dateTo2 = $_GET['dateTo2'];
                if ($dateTo2)
                {
                    $filter['dateTo2'] = $dateTo2;
                }
            }

            if (isset($_GET['status']))
            {
                $status = $_GET['status'];
                if ($status)
                {
                    $filter['status'] = $status;
                }
            }

            if (isset($_GET['userID']))
            {
                $userID = (int) $_GET['userID'];
                if ($userID > 0)
                {
                    $filter['userID'] = $userID;
                    $m_user = new Model_Component_Cms_User();
                    $filter['userName'] = $m_user->get_name($userID);
                }
            }

            if (isset($_GET['userCompanyID']))
            {
                $userCompanyID = (int) $_GET['userCompanyID'];
                if ($userCompanyID > 0)
                {
                    $filter['userCompanyID'] = $userCompanyID;
                    $m_company = new Model_Component_Cms_Company();
                    $filter['companyName'] = $m_company->get_name($userCompanyID);
                }
            }

            if (isset($_GET['carTypeID']))
            {
                $carTypeID = (int) $_GET['carTypeID'];
                if ($carTypeID > 0)
                {
                    $filter['carTypeID'] = $carTypeID;
                }
            }

            if (isset($_GET['places_from']))
            {
                if (!empty($_GET['places_from']))
                {
                    if (is_array($_GET['places_from']))
                    {
                        if (isset($_GET['places_from'][0]))
                        {
                            if ($_GET['places_from'][0])
                            {
                                $filter['places_from'] = $_GET['places_from'];
                                $filter['places_from_groups'] = [];
                                $m_geo = new Model_Cms_Geo();
                                foreach($filter['places_from'] as $place_group_ids)
                                {
                                    $filter['places_from_groups'][] = [
                                        'ids'=>explode(',',$place_group_ids),
                                        'name'=>$m_geo->getNameByIDs(explode(',',$place_group_ids))
                                    ];
                                }
                            }
                        }
                    }
                }
            }


            if (isset($_GET['places_to']))
            {
                if (!empty($_GET['places_to']))
                {
                    if (is_array($_GET['places_to']))
                    {
                        if (isset($_GET['places_to'][0]))
                        {
                            if ($_GET['places_to'][0])
                            {
                                $filter['places_to'] = $_GET['places_to'];
                                $filter['places_to_groups'] = [];
                                $m_geo = new Model_Cms_Geo();
                                foreach($filter['places_to'] as $place_group_ids)
                                {
                                    $filter['places_to_groups'][] = [
                                        'ids'=>explode(',',$place_group_ids),
                                        'name'=>$m_geo->getNameByIDs(explode(',',$place_group_ids))
                                    ];
                                }
                            }
                        }
                    }
                }
            }

            if (isset($_GET['radius_from']))
            {
                $radius_from = (int) $_GET['radius_from'];
                if ($radius_from > 0)
                {
                    $filter['radius_from'] = $radius_from;
                }
            }

            if (isset($_GET['radius_to']))
            {
                $radius_to = (int) $_GET['radius_to'];
                if ($radius_to > 0)
                {
                    $filter['radius_to'] = $radius_to;
                }
            }
        }

        $this->view->filter = $filter;

        $this->render();
    }

    public function action_add()
    {
        $this->view = View::factory('component/cms/cargo_requests/add');
        $this->view->langs = $this->langs;

        $m_cargo_req = new Model_Component_Cms_CargoRequests();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_cargo_req->valid($data);
            if (empty($info['errors']))
            {
                $m_cargo_req->add($info['data']);
                $this->view->change_ok = 1;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }


        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;

        $m_price_cur = new Model_Component_Cms_Currencies();
        $this->view->price_cur = $m_price_cur->getAll();

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->car_types = $m_transport_types->get_all();

        $this->render();
    }


    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/cargo_requests/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_cargo_req = new Model_Component_Cms_CargoRequests();

        $info = $m_cargo_req->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_cargo_req->valid($data);
            if (empty($info['errors']))
            {
                $m_cargo_req->save($this->id,$info['data']);
                $this->view->change_ok = 1;
                $info = $m_cargo_req->get_one($this->id);
                $this->view->info = $info;


                $info = $m_cargo_req->get_one($this->id);
                $this->view->info = $info;

            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_price_cur = new Model_Component_Cms_Currencies();
        $this->view->price_cur = $m_price_cur->getAll();

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->car_types = $m_transport_types->get_all();

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_CargoRequests();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered($this->post['filter']);
        $res['data'] = $_dt->get($this->post,'uk',$this->post['filter']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_cargo_req = new Model_Component_Cms_CargoRequests();
        switch ($this->post['action']) {
            case 'delete':
                $m_cargo_req->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_cargo_req->delete($id);
                }
                break;
        }
        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

    function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1)
            foreach($value1 as $key2 => $value2)
                $result[$key2][$key1] = $value2;
        return $result;
    }
}