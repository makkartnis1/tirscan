<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_SimpleComments extends Controller{

    private $edit_url = null;
    private $base_title = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }


    public function action_index()
    {
        $this->view = View::factory('component/cms/simplecomments/index');
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];

        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->view->get = $this->post['get'];
        $this->render();
    }


    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/simplecomments/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_simpleComments = new Model_Component_Cms_SimpleComments();

        $info = $m_simpleComments->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_simpleComments->valid($data);
            if (empty($info['errors']))
            {
                $m_simpleComments->save($this->id,$info['data']);
                $this->view->change_ok = 1;
                $info = $m_simpleComments->get_one($this->id);
                $this->view->info = $info;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_SimpleComments();
        $res = array();
        if (!isset($this->post['get'])) $this->post['get'] = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($this->post['get']);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->post['get']);
        $res['data'] = $_dt->get($this->post,$this->post['get']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_simpleComments = new Model_Component_Cms_SimpleComments();
        switch ($this->post['action']) {
            case 'delete':
                $m_simpleComments->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_simpleComments->delete($id);
                }
                break;
        }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}