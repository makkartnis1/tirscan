<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Transport extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $add_url = null;
    private $edit_url = null;
    private $case_edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_widget()
    {
        $this->view = View::factory('component/cms/transport/widget');
        $this->callback_functon_btn = $this->post['callback_function_btn'];
        $this->callback_functon_view = $this->post['callback_function_view'];
        $this->view->callback_function_btn = $this->callback_functon_btn;
        $this->view->callback_function_view = $this->callback_functon_view;
        $this->view->langs = $this->langs;
        $this->view->lang = $this->request->param('lang');

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->types = $m_transport_types->get_all();

        $this->render();
    }

    public function action_index()
    {

        $this->view = View::factory('component/cms/transport/index');
        $this->add_url = $this->post['add_url'];
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();

        $this->view->add_url = $this->add_url;
        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->view->types = $m_transport_types->get_all();
        $this->view->get = $this->post['get'];
        $this->render();
    }

    public function action_add()
    {
        $this->view = View::factory('component/cms/transport/add');
        $this->view->langs = $this->langs;

        $m_car = new Model_Component_Cms_Transport();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_car->valid($data);
            if (empty($info['errors']))
            {
                $id =  $m_car->add($info['data']);

                $files_arr = ['docs','numbers','gallery'];

                foreach($files_arr as $f_a)
                {

                    $type = 'private';
                    if ($f_a == 'gallery')
                        $type = 'public';

                    if (isset($_FILES[$f_a]))
                    {
                        $files = $this->diverse_array($_FILES[$f_a]);
                        foreach($files as $file)
                        {
                            if ($file['size']!=0)
                            {

                                $uploaddir = '/uploads/files/'.$type.'/car/'.$f_a.'/'.$id.'/';

                                $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                                if (!file_exists($dir)) mkdir($dir);
                                move_uploaded_file($file['tmp_name'],$dir.$file['name']);
                            }
                        }
                    }
                }


                $this->view->change_ok = 1;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }



        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->types = $m_transport_types->get_all();

        $this->render();
    }


    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/transport/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_car = new Model_Component_Cms_Transport();

        $info = $m_car->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_car->valid($data);
            if (empty($info['errors']))
            {
                $m_car->save($this->id,$info['data']);
                $this->view->change_ok = 1;
                $info = $m_car->get_one($this->id);
                $this->view->info = $info;

                $files_arr = ['docs','numbers','gallery'];

                foreach($files_arr as $f_a)
                {

                    $type = 'private';
                    if ($f_a == 'gallery')
                        $type = 'public';

                    if ($info['files'][$f_a])
                    {
                        foreach($info['files'][$f_a] as $isset_file)
                        {

                            if (isset($data['isset_'.$f_a]))
                            {
                                if (!in_array($isset_file,$data['isset_'.$f_a]))
                                {
                                    $file_del=$_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$type.'/car/'.$f_a.'/'.$this->id.'/'.$isset_file;
                                    if (file_exists($file_del)) unlink($file_del);
                                }
                            }
                            else
                            {
                                $file_del=$_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$type.'/car/'.$f_a.'/'.$this->id.'/'.$isset_file;
                                if (file_exists($file_del)) unlink($file_del);
                            }
                        }
                    }


                    if (isset($_FILES[$f_a]))
                    {
                        $files = $this->diverse_array($_FILES[$f_a]);
                        foreach($files as $file)
                        {
                            if ($file['size']!=0)
                            {
                                $uploaddir = '/uploads/files/'.$type.'/car/'.$f_a.'/'.$this->id.'/';
                                $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                                if (!file_exists($dir)) mkdir($dir);
                                move_uploaded_file($file['tmp_name'],$dir.$file['name']);
                            }
                        }
                    }

                }


                $info = $m_car->get_one($this->id);
                $this->view->info = $info;

            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_transport_types = new Model_Cms_Plugin_TransportTypes();
        $this->view->types = $m_transport_types->get_all();

        $this->render();
    }

    public function action_db_widget()
    {
        $_dt = new Model_Component_Cms_Dt_TransportWidget();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($this->post['get']);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->langs,$this->post['get']);
        $res['data'] = $_dt->get($this->post,$this->langs,$this->post['get']);
        echo json_encode($res);
        die;
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Transport();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($this->post['get']);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->langs,$this->post['get']);
        $res['data'] = $_dt->get($this->post,$this->langs,$this->post['get']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_user = new Model_Component_Cms_Transport();
        switch ($this->post['action']) {
            case 'delete':
                $m_user->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_user->delete($id);
                }
                break;
        }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

    function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1)
            foreach($value1 as $key2 => $value2)
                $result[$key2][$key1] = $value2;
        return $result;
    }
}