<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Complaints extends Controller{

    private $base_title = null;
    private $langs = null;
    private $post;
    private $get;

    private $companyID = null;
    private $userID = null;
    private $filter = [];

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/complaints/v_index');
        $this->base_title = $this->post['base_title'];
        $this->get = $this->post['get'];

        
        if (isset($this->get['companyID']))
        {
            if ($this->get['companyID'])
            {
                $m_company = new Model_Component_Cms_Company();
                $this->companyID = $this->get['companyID'];
                $this->filter['companyID'] = $this->companyID;
                $this->view->companyID = $this->companyID;
                $this->view->companyName = $m_company->get_name($this->companyID);
            }
        }

        if (isset($this->get['userID']))
        {
            if ($this->get['userID'])
            {
                $m_user = new Model_Component_Cms_User();
                $this->userID = $this->get['userID'];
                $this->filter['userID'] = $this->userID;
                $this->view->userID = $this->userID;
                $this->view->userName = $m_user->get_name($this->userID);
            }
        }

        if (isset($this->get['text']))
        {
            if ($this->get['text'])
            {
                $this->text = $this->get['text'];
                $this->filter['text'] = $this->text;
                $this->view->text = $this->text;
            }
        }


        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->view->filter = $this->filter;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Complaints();
        if (!isset($this->post['get'])) $this->post['get'] = array();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($this->post['get']);
        $res['recordsFiltered'] = $_dt->countAll($this->post['get']);
        $res['data'] = $_dt->get($this->post,$this->post['get']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_model = new Model_Component_Cms_Complaints();
        switch ($this->post['action']) {
            case 'delete':
                $m_model->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_model->delete($id);
                }
                break;
        }
        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}