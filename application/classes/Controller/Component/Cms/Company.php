<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Company extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $add_url = null;
    private $edit_url = null;
    private $case_edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;
    private $company_type;


    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
        $this->lang = $this->request->param('lang');
    }


    public function action_add()
    {
        $this->view = View::factory('component/cms/company/add');
        $this->view->langs = $this->langs;

        $m_company = new Model_Component_Cms_Company();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_company->valid($data,$this->langs);
            if (empty($info['errors']))
            {
                $id =  $m_company->add($info['data'],$this->langs);

                $info = $m_company->get_one($id);

                if (isset($info['userID']))
                {
                    if (isset($_FILES['avatar']))
                    {
                        if ($_FILES['avatar']['size']!=0)
                        {
                            $uploaddir = '/uploads/images/users/';
                            $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                            if (!file_exists($dir)) mkdir($dir);
                            move_uploaded_file($_FILES['avatar']['tmp_name'],$dir.$info['userID'].'.jpg');
                        }
                    }
                }

                if (isset($_FILES['img']))
                {
                    if ($_FILES['img']['size']!=0)
                    {
                        $uploaddir = '/uploads/images/company/';
                        $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                        if (!file_exists($dir)) mkdir($dir);
                        move_uploaded_file($_FILES['img']['tmp_name'],$dir.$id.'.jpg');
                    }
                }



                $files_arr = ['sertifikaty','podatkovi','license-transport-and-expedition','cert-state-reg'];

                foreach($files_arr as $f_a)
                {
                    if (isset($_FILES[$f_a]))
                    {
                        $files = $this->diverse_array($_FILES[$f_a]);
                        foreach($files as $file)
                        {
                            if ($file['size']!=0)
                            {
                                $uploaddir = '/uploads/files/private/company/'.$f_a.'/'.$id.'/';
                                $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                                if (!file_exists($dir)) mkdir($dir);
                                move_uploaded_file($file['tmp_name'],$dir.$file['name']);
                            }
                        }
                    }
                }


                $this->view->change_ok = 1;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_company_types = new Model_Cms_Plugin_CompanyTypes();
        $this->view->types = $m_company_types->get_all();

        $m_company_owner_types = new Model_Cms_Plugin_CompanyOwnerTypes();
        $this->view->owner_types = $m_company_owner_types->get_all();

        $m_phonecodes = new Model_Cms_Plugin_Phonecodes();
        $this->view->phone_codes = $m_phonecodes->get_all();

        $this->render();
    }

    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/company/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_company = new Model_Component_Cms_Company();

        $info = $m_company->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $change_pass = (isset($data['change_pass']))?1:0;
            $info = $m_company->valid($data,$this->langs,$this->id,$change_pass);
            if (empty($info['errors']))
            {
                $m_company->save($this->id,$info['data'],$this->langs);
                $this->view->change_ok = 1;
                $info = $m_company->get_one($this->id);
                $this->view->info = $info;


                if (isset($info['userID']))
                {
                    if (isset($_FILES['avatar']))
                    {
                        if ($_POST['avatar_exist'] == 1)
                        {
                            if ($_FILES['avatar']['size']!=0)
                            {
                                $uploaddir = '/uploads/images/users/';
                                $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                                if (!file_exists($dir)) mkdir($dir);
                                move_uploaded_file($_FILES['avatar']['tmp_name'],$dir.$info['userID'].'.jpg');
                            }
                        }
                        else
                        {
                            $uploaddir = '/uploads/images/users/';
                            $file=$_SERVER['DOCUMENT_ROOT'].$uploaddir.$info['userID'].'.jpg';
                            if (file_exists($file)) unlink($file);
                        }
                    }
                }


                if (isset($_FILES['img']))
                {
                    if ($_POST['file_exist'] == 1)
                    {
                        if ($_FILES['img']['size']!=0)
                        {
                            $uploaddir = '/uploads/images/company/';
                            $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                            if (!file_exists($dir)) mkdir($dir);
                            move_uploaded_file($_FILES['img']['tmp_name'],$dir.$this->id.'.jpg');
                        }
                    }
                    else
                    {
                        $uploaddir = '/uploads/images/company/';
                        $file=$_SERVER['DOCUMENT_ROOT'].$uploaddir.$this->id.'.jpg';
                        if (file_exists($file)) unlink($file);
                    }
                }


                $files_arr = ['sertifikaty','podatkovi','license-transport-and-expedition','cert-state-reg'];


                foreach($files_arr as $f_a)
                {

                    if ($info['files'][$f_a])
                    {
                        foreach($info['files'][$f_a] as $isset_file)
                        {

                            if (isset($data['isset_'.$f_a]))
                            {
                                if (!in_array($isset_file,$data['isset_'.$f_a]))
                                {
                                    $file_del=$_SERVER['DOCUMENT_ROOT'].'/uploads/files/private/company/'.$f_a.'/'.$this->id.'/'.$isset_file;
                                    if (file_exists($file_del)) unlink($file_del);
                                }
                            }
                            else
                            {
                                $file_del=$_SERVER['DOCUMENT_ROOT'].'/uploads/files/private/company/'.$f_a.'/'.$this->id.'/'.$isset_file;
                                if (file_exists($file_del)) unlink($file_del);
                            }
                        }
                    }


                    if (isset($_FILES[$f_a]))
                    {
                        $files = $this->diverse_array($_FILES[$f_a]);
                        foreach($files as $file)
                        {
                            if ($file['size']!=0)
                            {
                                $uploaddir = '/uploads/files/private/company/'.$f_a.'/'.$this->id.'/';
                                $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                                if (!file_exists($dir)) mkdir($dir);
                                move_uploaded_file($file['tmp_name'],$dir.$file['name']);
                            }
                        }
                    }

                }


                $info = $m_company->get_one($this->id);
                $this->view->info = $info;

            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_company_types = new Model_Cms_Plugin_CompanyTypes();
        $this->view->types = $m_company_types->get_all();

        $m_company_owner_types = new Model_Cms_Plugin_CompanyOwnerTypes();
        $this->view->owner_types = $m_company_owner_types->get_all();

        $m_phonecodes = new Model_Cms_Plugin_Phonecodes();
        $this->view->phone_codes = $m_phonecodes->get_all();

        $this->render();
    }



    public function action_widget()
    {
        $this->view = View::factory('component/cms/company/widget');
        $this->callback_functon_btn = $this->post['callback_function_btn'];
        $this->callback_functon_view = $this->post['callback_function_view'];
        $this->view->callback_function_btn = $this->callback_functon_btn;
        $this->view->callback_function_view = $this->callback_functon_view;

        $filter = [];

        if (isset($this->post['company_type']))
        {
            $filter['company_type'] = $this->post['company_type'];
        }

        $this->view->langs = $this->langs;
        $this->view->lang = $this->lang;
        $this->view->filter = $filter;
        $this->render();
    }



    public function action_db()
    {
        $_dt = new Model_Cms_Dt_Company();
        $res = array();

        $company_type = (isset($this->post['filter']['company_type']))?$this->post['filter']['company_type']:null;

        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($company_type);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->langs,$company_type);
        $res['data'] = $_dt->get($this->post,$this->langs,$company_type);
        echo json_encode($res);
        die;
    }

    public function action_db_analytics()
    {
        $_dt = new Model_Component_Cms_Dt_Company();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered();
        $res['data'] = $_dt->get($this->post);
        echo json_encode($res);
        die;
    }


    private function render()
    {
        echo $this->response->body($this->view);
    }

    function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1)
            foreach($value1 as $key2 => $value2)
                $result[$key2][$key1] = $value2;
        return $result;
    }

}