<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_User extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $add_url = null;
    private $edit_url = null;
    private $case_edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;
    private $user_type;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_Lng();
//        $this->lang = $m_lang->get_one_by_pref($lang_pref);
        $this->langs = $m_lang->get_all();
    }

    public function action_widget()
    {
        $this->view = View::factory('component/cms/user/widget');
        $this->callback_functon_btn = $this->post['callback_function_btn'];
        $this->callback_functon_view = $this->post['callback_function_view'];
        $this->view->callback_function_btn = $this->callback_functon_btn;
        $this->view->callback_function_view = $this->callback_functon_view;
        $this->view->langs = $this->langs;
        $this->view->lang = $this->request->param('lang');

        if (isset($this->post['company_id']))
        {
            $this->view->company_id = $this->post['company_id'];
        }

        if (isset($this->post['isset_car']))
        {
            $this->view->isset_car = 1;
        }

        if (isset($this->post['user_type']))
        {
            $this->view->user_type = $this->post['user_type'];
        }

        $this->render();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/user/index');
        $this->add_url = $this->post['add_url'];
        $this->edit_url = $this->post['edit_url'];
        $this->case_edit_url = $this->post['case_edit_url'];
        $this->base_title = $this->post['base_title'];

        $m_types = new Model_Cms_Plugin_CompanyTypes();
        $this->view->company_types = $m_types->get_all();

        $this->view->register_times = [
            '<1m'=>'менше місяця',
            '>1m'=>'більше 1 місяця',
            '>2m'=>'більше 2 місяців',
            '>3m'=>'більше 3 місяців',
            '>6m'=>'більше 6 місяців',
            '>1y'=>'більше 1 року',
            '>2y'=>'більше 2 років',
            '>3y'=>'більше 3 років',
            '>5y'=>'більше 5 років'
        ];


        $filter = [];

        if (isset($_GET['company_id']))
        {
            $company_id = (int) $_GET['company_id'];
            if ($company_id)
            {
                $filter['company_id'] = $company_id;
            }
        }

        if (isset($_GET['goFilter']))
        {

            if (isset($_GET['ID']))
            {
                $ID = (int) $_GET['ID'];
                if ($ID)
                {
                    $filter['ID'] = $ID;
                }
            }

            if (isset($_GET['name']))
            {
                $name = $_GET['name'];
                if ($name)
                {
                    $filter['name'] = $name;
                }
            }


            if (isset($_GET['email']))
            {
                $email = $_GET['email'];
                if ($email)
                {
                    $filter['email'] = $email;
                }
            }

            if (isset($_GET['phone']))
            {
                $phone = $_GET['phone'];
                if ($phone)
                {
                    $filter['phone'] = $phone;
                }
            }


            if (isset($_GET['emailApproved']))
            {
                $emailApproved = $_GET['emailApproved'];
                if ($emailApproved !== '')
                {
                    $filter['emailApproved'] = $emailApproved;
                }
            }


            if (isset($_GET['group']))
            {
                $group = $_GET['group'];
                if ($group !== '')
                {
                    $filter['group'] = $group;
                }
            }


            if (isset($_GET['companyType']))
            {
                $companyType = $_GET['companyType'];
                if ($companyType !== '')
                {
                    $filter['companyType'] = $companyType;
                }
            }


            if (isset($_GET['registered']))
            {
                $registered = $_GET['registered'];
                if ($registered !== '')
                {
                    $filter['registered'] = $registered;
                }
            }

            if (isset($_GET['dateCreate1']))
            {
                $dateCreate1 = $_GET['dateCreate1'];
                if ($dateCreate1)
                {
                    $filter['dateCreate1'] = $dateCreate1;
                }
            }

            if (isset($_GET['dateCreate2']))
            {
                $dateCreate2 = $_GET['dateCreate2'];
                if ($dateCreate2)
                {
                    $filter['dateCreate2'] = $dateCreate2;
                }
            }
        }

        $this->view->filter = $filter;


        $this->view->add_url = $this->add_url;
        $this->view->edit_url = $this->edit_url;
        $this->view->case_edit_url = $this->case_edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->view->get = $this->post['get'];
        $this->render();
    }

    public function action_add()
    {
        $this->view = View::factory('component/cms/user/add');
        $this->view->langs = $this->langs;

        $m_user = new Model_Component_Cms_User();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_user->valid($data,$this->langs);
            if (empty($info['errors']))
            {
                $id =  $m_user->add($info['data'],$this->langs);

                if (isset($_FILES['img']))
                {
                    if ($_FILES['img']['size']!=0)
                    {
                        $uploaddir = '/uploads/files/public/user/avatar/'.$id.'/';
                        $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                        if (!file_exists($dir)) mkdir($dir);
                        move_uploaded_file($_FILES['img']['tmp_name'],$dir.$id.'.jpg');
                    }
                }

                $this->view->change_ok = 1;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_phonecodes = new Model_Cms_Plugin_Phonecodes();
        $this->view->phone_codes = $m_phonecodes->get_all();

        $this->render();
    }

    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/user/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_user = new Model_Component_Cms_User();

        $info = $m_user->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $change_pass = (isset($data['change_pass']))?1:0;
            $info = $m_user->valid($data,$this->langs,$this->id,$change_pass);
            if (empty($info['errors']))
            {
                $m_user->save($this->id,$info['data'],$this->langs);
                $this->view->change_ok = 1;
                $info = $m_user->get_one($this->id);
                $this->view->info = $info;


                if (isset($_FILES['img']))
                {
                    if ($_POST['file_exist'] == 1)
                    {
                        if ($_FILES['img']['size']!=0)
                        {
                            $uploaddir = '/uploads/files/public/user/avatar/'.$this->id.'/';
                            $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;
                            if (!file_exists($dir)) mkdir($dir);
                            move_uploaded_file($_FILES['img']['tmp_name'],$dir.$this->id.'.jpg');
                        }
                    }
                    else
                    {
                        $uploaddir = '/uploads/files/public/user/avatar/'.$this->id.'/';

                        $dir=$_SERVER['DOCUMENT_ROOT'].$uploaddir;

                        if (file_exists($dir))
                        {
                            if ($handle = opendir($dir)) {
                                while (false !== ($entry = readdir($handle))) {
                                    if ($entry != "." && $entry != "..") {
                                        $img = $entry;
                                    }
                                }
                                closedir($handle);
                            }
                        }

                        if (isset($img))
                        {
                            $file=$_SERVER['DOCUMENT_ROOT'].$uploaddir.$img;
                            if (file_exists($file)) unlink($file);
                        }

                    }
                }

            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $m_phonecodes = new Model_Cms_Plugin_Phonecodes();
        $this->view->phone_codes = $m_phonecodes->get_all();
        $this->render();
    }


    public function action_db_widget()
    {
        $_dt = new Model_Component_Cms_Dt_UserWidget();
        $res = array();

        $company_id = (isset($this->post['company_id']))?$this->post['company_id']:null;
        $isset_car = (isset($this->post['isset_car']))?$this->post['isset_car']:null;
        $user_type = (isset($this->post['user_type']))?$this->post['user_type']:null;

        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll('id',$company_id,$isset_car,$user_type);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->langs,$company_id,$isset_car,$user_type);
        $res['data'] = $_dt->get($this->post,$this->langs,$company_id,$isset_car,$user_type);
        echo json_encode($res);
        die;
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_User();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll($this->post['filter']);
        $res['recordsFiltered'] = $_dt->countFiltered($this->post['filter']);
        $res['data'] = $_dt->get($this->post,$this->post['filter']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_user = new Model_Component_Cms_User();
        switch ($this->post['action']) {
            case 'delete':
                $m_user->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_user->delete($id);
                }
                break;
            
        }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}
