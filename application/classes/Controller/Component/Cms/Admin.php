<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Admin extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $lang = null;


    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->lang = $m_lang->get_one_by_pref($lang_pref);
    }

    public function action_widget()
    {
        $this->view = View::factory('component/cms/admin/widget');
        $this->callback_functon_btn = $this->post['callback_function_btn'];
        $this->callback_functon_view = $this->post['callback_function_view'];
        $this->view->callback_function_btn = $this->callback_functon_btn;
        $this->view->callback_function_view = $this->callback_functon_view;
        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Admin();
        $res = array();
        $res['draw'] = $_POST['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countAll('id');
        $res['data'] = $_dt->get($_POST,$this->lang['id']);
        echo json_encode($res);
        die;
    }


    private function render()
    {
        echo $this->response->body($this->view);
    }

}