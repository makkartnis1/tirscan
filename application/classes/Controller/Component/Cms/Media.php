<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Media extends Controller{

    private $default_site_lng_id = null;
    private $is_files = 1;
    private $is_albums = 0;
    private $plugin_id = null;
    private $all_file_types = array();
    private $all_file_type_ids = array();
    private $post = array();
    private $view;
    private $table_name = null;
    private $col_name = null;
    private $id = null;
    private $session_id = null;


    function before()
    {
        parent::before();
        $this->post = $this->request->post();

    }

    public function init()
    {
        $this->is_files = $this->post['is_files'];
        $this->is_albums = $this->post['is_albums'];
        $this->plugin_id = $this->post['plugin_id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id'])) $this->id = (int) $this->post['id'];



        //створюємо сесію для роботи з файлами, якщо це не редагування
        if (!$this->id)
        {
            $session_info = DB::insert('x_file_sessions')->values(array())->execute();
            $this->session_id = $session_info[0];
        }

        //чистимо старі сесії і файли
        $this->clear_old_sessions();

        //вибірка мови сайту по замовчуванні
        $default_lang_info = DB::select('id')
                    ->from('x_lngs')
                    ->order_by('active','desc')
                    ->order_by('def','desc')
                    ->execute()
                    ->as_array();

        $this->default_site_lng_id = $default_lang_info[0]['id'];

        //вибірка всіх доступних типів файлів
        $all_file_type_info = DB::select('x_file_types.id','x_file_type_locales.title')
                    ->from('x_file_types')
                    ->join('x_file_type_locales','left')->on('x_file_types.id','=','x_file_type_locales.file_type_id')
                    ->where('x_file_type_locales.lng_id','=',$this->default_site_lng_id)
                    ->execute()
                    ->as_array();

        if (!empty($all_file_type_info))
        {
            $this->all_file_types = $all_file_type_info;
            $this->all_file_type_ids = Helper_Array::get_array_by_key($all_file_type_info,'id');
        }

        $this->view = View::factory('component/cms/media/index');
    }

    public function action_send_file() {
        Helper_Sendfile::send($_GET['file']);
    }

    //функції для роботи з файлами
    public function action_get_files()
    {
        $this->default_site_lng_id = $this->post['default_site_lng_id'];
        $file_type_id = $this->post['file_type_id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }
        echo $this->get_files($this->table_name,$this->col_name,$this->default_site_lng_id,$this->id,$this->session_id,$file_type_id);
    }

    public function action_get_files_by_album_id()
    {
        $this->default_site_lng_id = $this->post['default_site_lng_id'];
        $album_id = $this->post['album_id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }
        echo $this->get_files($this->table_name,$this->col_name,$this->default_site_lng_id,$this->id,$this->session_id,null,$album_id);
    }

    public function action_add_files()
    {
        $file_type_id = $this->post['file_type_id'];
        if (!$file_type_id) $file_type_id = null;
        $album_id = $this->post['album_id'];
        if (!$album_id) $album_id = null;
        $type = $this->post['type'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }

        if (isset($_FILES['files']))
        {
            if (!empty($_FILES['files']['name']))
            {
               $res_add = $this->add_files($_FILES,$file_type_id,$type,$this->table_name,$this->col_name,$this->id,$this->session_id,$album_id);
               echo ($res_add == 1)?json_encode(array("file_type_id"=>$file_type_id,"album_id"=>$album_id)):json_encode('');
            }
        }
        else
        {
            $titles = $this->post['titles'];
            $is_main = isset($this->post['is_main'])?1:0;
            $res_add = $this->create_file($titles,$is_main,$this->post['url'],$this->post['code'],$file_type_id,$type,$this->table_name,$this->col_name,$this->id,$this->session_id,$album_id);
            echo ($res_add == 1)?json_encode(array("file_type_id"=>$file_type_id,"album_id"=>$album_id)):json_encode('');
        }

    }

    public function action_up_srt_file()
    {
        $file_type_id = $this->post['file_type_id'];
        $album_id = $this->post['album_id'];
        $file_id = $this->post['file_id'];
        Helper_Srt::up('x_files',$file_id);
        echo json_encode(array('file_type_id'=>$file_type_id,'album_id'=>$album_id));
    }

    public function action_down_srt_file()
    {
        $file_type_id = $this->post['file_type_id'];
        $album_id = $this->post['album_id'];
        $file_id = $this->post['file_id'];
        Helper_Srt::down('x_files',$file_id);
        echo json_encode(array('file_type_id'=>$file_type_id,'album_id'=>$album_id));
    }

    public function action_apply_files_to_album()
    {
        $album_id = $this->post['album_id'];
        $file_ids = $this->post['file_ids'];
        foreach($file_ids as $file_id)
        {
            DB::insert('x_files_to_albums',array('file_id','album_id'))
                ->values(array($file_id,$album_id))
                ->execute();
        }

        echo json_encode('1');
    }

    public function action_add_main_file()
    {
        $file_type_id = $this->post['file_type_id'];
        $album_id = $this->post['album_id'];
        $file_id = $this->post['file_id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }
        DB::update('x_files')
                    ->set(array(
                        'is_main'=>1,
                    ))
                    ->where('id','=',$file_id)
                    ->execute();

        if ($this->id)
        {
            $files_info = DB::select('file_id')
                        ->from('x_files_to_'.$this->table_name)
                        ->where($this->col_name,'=',$this->id)
                        ->execute()
                        ->as_array();
            if (!empty($files_info))
            {
                $file_ids = Helper_Array::get_array_by_key($files_info,'file_id');
                DB::update('x_files')
                    ->set(array(
                        'is_main'=>0,
                    ))
                    ->where('id','!=',$file_id)
                    ->where('id','in',DB::expr('('.implode(',',$file_ids).')'))
                    ->execute();
            }
        }
        else
        {
            DB::update('x_files')
                ->set(array(
                    'is_main'=>0,
                ))
                ->where('id','!=',$file_id)
                ->where('session_id','=',$this->session_id)
                ->execute();
        }

        echo json_encode(array('file_type_id'=>$file_type_id,'album_id'=>$album_id));

    }

    public function action_delete_main_file()
    {
        $file_id = $this->post['file_id'];
        DB::update('x_files')
            ->set(array(
                'is_main'=>0,
            ))
            ->where('id','=',$file_id)
            ->execute();

        echo json_encode('1');
    }

    public function action_edit_file()
    {
        $file_id = $this->post['file_id'];
        $titles = $this->post['titles'];

        foreach($titles as &$t)
        {
            $t = trim($t);
            $t = htmlspecialchars($t,ENT_QUOTES);
        }

        $m_lang = new Model_Cms_Plugin_Lng();
        $langs = $m_lang->get_all_active();

        foreach($langs as $l)
        {
            DB::update('x_file_locales')
                ->set(array(
                    'title'=>$titles[$l['id']],
                ))
                ->where('lng_id','=',$l['id'])
                ->where('file_id','=',$file_id)
                ->execute();
        }

        $url = (isset($this->post['url']))?$this->post['url']:'';
        $code = (isset($this->post['code']))?$this->post['code']:'';

        DB::update('x_files')
                    ->set(array(
                        'url'=>$url,
                        'code'=>$code
                    ))
                    ->where('id','=',$file_id)
                    ->execute();

        echo json_encode(array("file_type_id"=>$this->post['file_type_id'],"album_id"=>$this->post['album_id']));
    }

    public function action_delete_plugin_files_and_albums()
    {

        $this->id = $this->post['id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];


        $albums = DB::select('album_id')
                    ->from('x_albums_to_'.$this->table_name)
                    ->where($this->col_name,'=',$this->id)
                    ->execute()
                    ->as_array();



        $table_files = DB::select('file_id')
                    ->from('x_files_to_'.$this->table_name)
                    ->where($this->col_name,'=',$this->id)
                    ->execute()
                    ->as_array();

        if (!empty($albums))
        {
            $album_ids = Helper_Array::get_array_by_key($albums,'album_id');
            $album_files = DB::select('x_files.id','x_files.year','x_files.month','x_files.day','x_files.ext')
                ->from('x_files')
                ->join('x_files_to_albums','inner')->on('x_files.id','=','x_files_to_albums.file_id')
                ->where('x_files_to_albums.album_id','in',DB::expr('('.implode(',',$album_ids).')'))
                ->where('x_files.only_in_album','=',1)
                ->execute()
                ->as_array();
            $this->delete_files($album_files);
        }

        if (!empty($table_files))
        {
            $file_ids = Helper_Array::get_array_by_key($table_files,'file_id');
            $files = DB::select('id','year','month','day','ext')
                ->from('x_files')
                ->where('id','in',DB::expr('('.implode(',',$file_ids).')'))
                ->execute()
                ->as_array();
            $this->delete_files($files);
        }


    }


    public function action_delete_files()
    {
        $file_ids = $this->post['file_ids'];
        $file_type_id = $this->post['file_type_id'];
        $album_id = $this->post['album_id'];

        if ($file_type_id)
        {
            $files = DB::select('id','year','month','day','ext')
                ->from('x_files')
                ->where('id','in',DB::expr('('.implode(',',$file_ids).')'))
                ->execute()
                ->as_array();

            $this->delete_files($files);
        }
        elseif($album_id)
        {
            $album_files = DB::select('x_files.id','x_files.year','x_files.month','x_files.day','x_files.ext')
                ->from('x_files')
                ->join('x_files_to_albums','inner')->on('x_files.id','=','x_files_to_albums.file_id')
                ->where('x_files_to_albums.album_id','=',$album_id)
                ->where('x_files.id','in',DB::expr('('.implode(',',$file_ids).')'))
                ->where('x_files.only_in_album','=',1)
                ->execute()
                ->as_array();

            $this->delete_files($album_files);

        }


        echo json_encode(array('file_type_id'=>$file_type_id,'album_id'=>$album_id));
    }

    public function action_delete_files_from_album()
    {
        $file_ids = $this->post['file_ids'];
        $album_id = $this->post['album_id'];

        $album_files = DB::select('x_files.id','x_files.year','x_files.month','x_files.day','x_files.ext')
            ->from('x_files')
            ->join('x_files_to_albums','inner')->on('x_files.id','=','x_files_to_albums.file_id')
            ->where('x_files_to_albums.album_id','=',$album_id)
            ->where('x_files.id','in',DB::expr('('.implode(',',$file_ids).')'))
            ->where('x_files.only_in_album','=',1)
            ->execute()
            ->as_array();

        if (!empty($album_files))
        {
            $this->delete_files($album_files);
        }

        echo json_encode('1');

    }

    public function action_get_file_full_info()
    {
        $file_id = $this->post['file_id'];
        $file_info = DB::select('id','date','ext','url','code','size','isset_file','type','size_types','year','month','day')
            ->from('x_files')
            ->where('id','=',$file_id)
            ->execute()
            ->as_array();
        if (empty($file_info))
        {
            echo json_encode('');
            die;
        }

        $file_info_locales = DB::select('title','lng_id')
            ->from('x_file_locales')
            ->where('file_id','=',$file_id)
            ->execute()
            ->as_array();

        $file_info_locales = Helper_Array::change_keys($file_info_locales,'lng_id');
        $file_info = $file_info[0];
        $file_info['locales'] = $file_info_locales;

        echo json_encode($file_info);

    }

    public function action_get_file_types()
    {
        $this->plugin_id = $this->post['plugin_id'];
        $this->default_site_lng_id = $this->post['default_site_lng_id'];
        $file_types_info = DB::select('x_file_types.id','x_file_type_locales.title')
                    ->from('x_file_types')
                    ->join('x_file_type_locales','left')->on('x_file_type_locales.file_type_id','=','x_file_types.id')
                    ->join('x_file_types_to_plugins','inner')->on('x_file_types.id','=','x_file_types_to_plugins.file_type_id')
                    ->where('x_file_type_locales.lng_id','=',$this->default_site_lng_id)
                    ->where('x_file_types_to_plugins.plugin_id','=',$this->plugin_id)
                    ->execute()
                    ->as_array();
        if (!empty($file_types_info))
        {
            echo json_encode($file_types_info);
        }
        else
        {
            echo json_encode('');
        }

    }

    public function action_set_file_types()
    {
        $this->plugin_id = $this->post['plugin_id'];
        $file_types = $this->post['file_types'];

        foreach($file_types as $file_type_id=>$val)
        {
            $file_type_count = DB::select(DB::expr('count(file_type_id) as c'))
                ->from('x_file_types_to_plugins')
                ->where('plugin_id','=',$this->plugin_id)
                ->where('file_type_id','=',$file_type_id)
                ->execute()
                ->as_array();

            $file_type_count = $file_type_count[0]['c'];

            if ($val == '1')
            {
                if ($file_type_count == 0)
                {
                    DB::insert('x_file_types_to_plugins',array('plugin_id','file_type_id'))
                                ->values(array($this->plugin_id,$file_type_id))
                                ->execute();
                }
            }
            else
            {
                if ($file_type_count == 1)
                {
                    DB::delete('x_file_types_to_plugins')
                                ->where('plugin_id','=',$this->plugin_id)
                                ->where('file_type_id','=',$file_type_id)
                                ->execute();
                }
            }
        }


        echo json_encode('1');
    }

    //функції для роботи з альбомами
    public function action_create_album()
    {
        $titles = $this->post['titles'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }

        $albums_max_srt = Helper_Srt::get_max_srt('x_albums');


        $new_album = DB::insert('x_albums',array('srt','session_id'))
                    ->values(array($albums_max_srt+1,$this->session_id))
                    ->execute();
        $album_id = $new_album[0];

        $m_lang = new Model_Cms_Plugin_Lng();
        $langs = $m_lang->get_all_active();

        foreach($langs as $l)
        {
            $titles[$l['id']] = htmlspecialchars($titles[$l['id']],ENT_QUOTES);

            DB::update('x_album_locales')
                ->set(array(
                    'title'=>$titles[$l['id']]
                ))
                ->where('lng_id','=',$l['id'])
                ->where('album_id','=',$album_id)
                ->execute();
        }

        if ($this->id)
        {
            DB::insert('x_albums_to_'.$this->table_name,array('album_id',$this->col_name))
                        ->values(array($album_id,$this->id))
                        ->execute();
        }

        echo json_encode(array($album_id));

    }

    public function action_edit_album()
    {
        $titles = $this->post['titles'];
        $album_id = $this->post['album_id'];
        $m_lang = new Model_Cms_Plugin_Lng();
        $langs = $m_lang->get_all_active();
        foreach($langs as $l)
        {
            $titles['title_'.$l['pref']] = htmlspecialchars($titles['title_'.$l['pref']],ENT_QUOTES);

            DB::update('x_album_locales')
                ->set(array(
                    'title'=>$titles['title_'.$l['pref']]
                ))
                ->where('lng_id','=',$l['id'])
                ->where('album_id','=',$album_id)
                ->execute();
        }
        echo json_encode('1');

    }

    public function action_up_srt_album()
    {
        $album_id = $this->post['album_id'];
        Helper_Srt::up('x_albums',$album_id);
        echo json_encode('1');

    }

    public function action_down_srt_album()
    {
        $album_id = $this->post['album_id'];
        Helper_Srt::down('x_albums',$album_id);
        echo json_encode('1');

    }

    public function action_delete_album()
    {
        $album_id = $this->post['album_id'];
        $album_files = DB::select('x_files.id','x_files.year','x_files.month','x_files.day','x_files.ext')
                    ->from('x_files')
                    ->join('x_files_to_albums','inner')->on('x_files.id','=','x_files_to_albums.file_id')
                    ->where('x_files_to_albums.album_id','=',$album_id)
                    ->where('x_files.only_in_album','=',1)
                    ->execute()
                    ->as_array();
        if (!empty($album_files))
        {
            $this->delete_files($album_files);
        }

        DB::delete('x_albums')
                    ->where('id','=',$album_id)
                    ->execute();

        echo json_encode('1');

    }

    public function action_get_albums()
    {
        $this->default_site_lng_id = $this->post['default_site_lng_id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];
        if (isset($this->post['id']))
        {
            $this->id = $this->post['id'];
        }
        else
        {
            $this->session_id = $this->post['session_id'];
        }

        $albums = DB::select('x_albums.id','x_album_locales.title')
                    ->from('x_albums')
                    ->join('x_album_locales','left')->on('x_albums.id','=','x_album_locales.album_id');
        if ($this->id)
        {
            $albums = $albums->join('x_albums_to_'.$this->table_name,'inner')->on('x_albums.id','=','x_albums_to_'.$this->table_name.'.album_id')
                             ->where('x_albums_to_'.$this->table_name.'.'.$this->col_name,'=',$this->id);
        }
        else
        {
            $albums = $albums->where('x_albums.session_id','=',$this->session_id);
        }
                    $albums = $albums->where('x_album_locales.lng_id','=',$this->default_site_lng_id)
                    ->order_by('x_albums.srt')
                    ->execute()
                    ->as_array();

        if (!empty($albums))
        {
            echo json_encode($albums);
        }
        else
        {
            echo json_encode('');
        }

    }

    public function action_get_album_full_info()
    {
        $album_id = $this->post['album_id'];

        $album_info = DB::select('id','date')
                    ->from('x_albums')
                    ->where('id','=',$album_id)
                    ->execute()
                    ->as_array();
        if (empty($album_info))
        {
            echo json_encode(array());
            die;
        }

        $album_info_locales = DB::select('title','lng_id')
                    ->from('x_album_locales')
                    ->where('album_id','=',$album_id)
                    ->execute()
                    ->as_array();

        $album_info_locales = Helper_Array::change_keys($album_info_locales,'lng_id');
        $album_info = $album_info[0];
        $album_info['locales'] = $album_info_locales;

        echo json_encode($album_info);

    }


    public function action_index()
    {
        $m_langs = new Model_Cms_Plugin_Lng();
        $langs = $m_langs->get_all_active();
        $this->init();
        $this->view->all_file_types = $this->all_file_types;
        $this->view->all_file_type_ids = $this->all_file_type_ids;
        $this->view->default_site_lng_id = $this->default_site_lng_id;
        $this->view->is_files = $this->is_files;
        $this->view->is_albums = $this->is_albums;
        $this->view->table_name = $this->table_name;
        $this->view->col_name = $this->col_name;
        $this->view->id = $this->id;
        $this->view->session_id = $this->session_id;
        $this->view->plugin_id = $this->plugin_id;
        $this->view->langs = $langs;
        $this->render();
    }




    private function render()
    {
        echo $this->response->body($this->view);
    }

    public function action_end_sessions()
    {
        $this->session_id = $this->post['session_id'];
        $this->id = $this->post['id'];
        $this->table_name = $this->post['table_name'];
        $this->col_name = $this->post['col_name'];

        $files = DB::select('id')
                    ->from('x_files')
                    ->where('session_id','=',$this->session_id)
                    ->execute()
                    ->as_array();

        $albums = DB::select('id')
            ->from('x_albums')
            ->where('session_id','=',$this->session_id)
            ->execute()
            ->as_array();


        if (!empty($files))
        {
            foreach($files as $file)
            {
                DB::insert('x_files_to_'.$this->table_name,array('file_id',$this->col_name))
                            ->values(array($file['id'],$this->id))
                            ->execute();
            }
        }

        if (!empty($albums))
        {
            foreach($albums as $album)
            {
                DB::insert('x_albums_to_'.$this->table_name,array('album_id',$this->col_name))
                    ->values(array($album['id'],$this->id))
                    ->execute();
            }
        }

        DB::delete('x_file_sessions')
            ->where('id','=',$this->session_id)
            ->execute();
    }

    private function clear_old_sessions()
    {
        $last_day = date("Y-m-d H:i:s",time() - 86400);
        $old_sessions = DB::select('id')
                    ->from('x_file_sessions')
                    ->where('date','<',$last_day)
                    ->execute()
                    ->as_array();
        if (!empty($old_sessions))
        {
            $session_ids = Helper_Array::get_array_by_key($old_sessions,'id');
            //видаляємо файли які підвязані до сесій
            $session_files = DB::select('id','year','month','day','ext')
                        ->from('x_files')
                        ->where('session_id','in',DB::expr('('.implode(',',$session_ids).')'))
                        ->execute()
                        ->as_array();

            if (!empty($session_files))
            {
                $this->delete_files($session_files);
            }


            //видаляємо альбоми які підвязані до сесій
            $session_albums = DB::select('id')
                ->from('x_albums')
                ->where('session_id','in',DB::expr('('.implode(',',$session_ids).')'))
                ->execute()
                ->as_array();

            if (!empty($session_albums))
            {
                $session_album_ids = Helper_Array::get_array_by_key($session_albums,'id');
                $file_ids_info = DB::select('file_id')
                    ->from('x_files_to_albums')
                    ->where('album_id','in',DB::expr('('.implode(',',$session_album_ids).')'))
                    ->execute()
                    ->as_array();
                if (!empty($file_ids_info))
                {
                    $file_ids = Helper_Array::get_array_by_key($file_ids_info,'file_id');
                    $session_files = DB::select('id','year','month','day','ext')
                        ->from('x_files')
                        ->where('session_id','in',DB::expr('('.implode(',',$session_ids).')'))
                        ->where('id','in',DB::expr('('.implode(',',$file_ids).')'))
                        ->execute()
                        ->as_array();

                    if (!empty($session_files))
                    {
                        $this->delete_files($session_files);
                    }

                }
            }

            //видаляемо старі сесії
            DB::delete('x_file_sessions')
                        ->where('id','in',DB::expr('('.implode(',',$session_ids).')'))
                        ->execute();


        }
    }



    private function get_files($table_name, $col_name, $lng_id, $id = null, $session_id = null, $file_type_id = null, $album_id = null)
    {

            $files = DB::select(
                'x_files.id',
                'x_files.date',
                'x_files.url',
                'x_files.code',
                'x_files.year',
                'x_files.month',
                'x_files.day',
                'x_files.size',
                'x_files.ext',
                'x_files.is_main',
                'x_files.isset_file',
                'x_files.type',
                'x_files.size_types',
                'x_file_locales.title'
            )
                        ->from('x_files')
                        ->join('x_file_locales')->on('x_files.id','=','x_file_locales.file_id');

            if ($album_id)
            {
                $files = $files->join('x_files_to_albums','inner')->on('x_files.id','=','x_files_to_albums.file_id')
                    ->where('x_files_to_albums.album_id','=',$album_id);
            }

            if ($session_id)
            {
                $files = $files->where('session_id','=',$session_id);
            }
            elseif($id)
            {
                $files = $files->join('x_files_to_'.$table_name,'inner')->on('x_files.id','=','x_files_to_'.$table_name.'.file_id')
                               ->where('x_files_to_'.$table_name.'.'.$col_name,'=',$id);
            }

            if ($file_type_id) $files = $files->where('x_files.file_type_id','=',$file_type_id);
            $files = $files->where('x_file_locales.lng_id','=',$lng_id)
            ->order_by('x_files.srt')
            ->execute()
            ->as_array();

        if (!empty($files))
        {
            return json_encode(array("files"=>$files,"file_type_id"=>$file_type_id,"album_id"=>$album_id));
        }
        else
        {
            return json_encode(array("file_type_id"=>$file_type_id,"album_id"=>$album_id,"files"=>''));
        }

    }

    private function delete_files($files)
    {
        foreach($files as $file)
        {
            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);

            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'_s.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);

            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'_m.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);

            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'_w.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);

            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'_s_w.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);

            $file_link = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$file['year'].'/'.$file['month'].'/'.$file['day'].'/'.$file['id'].'/'.$file['id'].'_m_w.'.$file['ext'];
            if (file_exists($file_link)) unlink($file_link);
        }

        if (!empty($files))
        {
            $file_ids = Helper_Array::get_array_by_key($files,'id');
            DB::delete('x_files')->where('id','in',DB::expr('('.implode(',',$file_ids).')'))->execute();
        }
    }

    private function add_files($files,$file_type_id,$type,$table_name,$col_name,$id,$session_id,$album_id = null)
    {
        $only_in_album = ($album_id)?1:0;

        $year = (int) date("Y");
        $month = (int) date("m");
        $day = (int) date("d");

        $files_copy = $files;
        foreach($files['files']['name'] as $key => $val)
        {
            $exp_val = explode('.',$val);
            $ext = mb_strtolower($exp_val[count($exp_val)-1]);
            $size = $files_copy['files']['size'][$key];
            $size = round($size / 1024,2);

            $ins_file_info = DB::insert('x_files',
                array(
                    'date',
                    'ext',
                    'srt',
                    'size',
                    'file_type_id',
                    'year',
                    'month',
                    'day',
                    'session_id',
                    'only_in_album',
                    'isset_file',
                    'type'
                )
            )
                ->values(
                    array(
                        date("Y-m-d H:i:s"),
                        $ext,
                        Helper_Srt::get_max_srt('x_files')+1,
                        $size,
                        $file_type_id,
                        $year,
                        $month,
                        $day,
                        $session_id,
                        $only_in_album,
                        '1',
                        $type
                    )
                )
                ->execute();

            $file_id = $ins_file_info[0];

            $dir = $_SERVER['DOCUMENT_ROOT'].'/uploads/files/'.$year.'/';
            if (!file_exists($dir)) mkdir($dir);
            $dir .= $month.'/';
            if (!file_exists($dir)) mkdir($dir);
            $dir .= $day.'/';
            if (!file_exists($dir)) mkdir($dir);
            $dir .= $file_id.'/';
            if (!file_exists($dir)) mkdir($dir);

            move_uploaded_file($files_copy['files']['tmp_name'][$key],$dir.$file_id.'.'.$ext);

            if ($type == '1')
            {
                $size_types = array();
                $original_file = $dir.$file_id.'.'.$ext;
                $image_size = getimagesize($original_file);
                if ($image_size)
                {
                    $width = (int) $image_size[0];
                    $height = (int) $image_size[1];
                    $image=Image::factory($original_file);
                    if ($width > 1500 or $height > 1500)
                    {
                        $image->resize(1500, 1500);
                        $image->save($dir.$file_id.'_l.'.$ext,100);
                        $image->resize(500, 500);
                        $image->save($dir.$file_id.'_m.'.$ext,100);
                        $image->resize(200, 200);
                        $image->save($dir.$file_id.'_s.'.$ext,100);
                        $size_types[] = 'l';
                        $size_types[] = 'm';
                        $size_types[] = 's';
                    }
                    elseif($width > 500 or $height > 500)
                    {
                        $image->resize(500, 500);
                        $image->save($dir.$file_id.'_m.'.$ext,100);
                        $image->resize(200, 200);
                        $image->save($dir.$file_id.'_s.'.$ext,100);
                        $size_types[] = 'm';
                        $size_types[] = 's';
                    }
                    elseif($width > 200 or $height > 200)
                    {
                        $image->resize(200, 200);
                        $image->save($dir.$file_id.'_s.'.$ext,100);
                        $size_types[] = 's';
                    }

                    if (!empty($size_types))
                    {
                        $size_types = implode('.',$size_types);
                        DB::update('x_files')
                                    ->set(array(
                                        'size_types'=>$size_types,
                                    ))
                                    ->where('id','=',$file_id)
                                    ->execute();

                    }

                }
            }


            if ($id)
            {
                DB::insert('x_files_to_'.$table_name,array('file_id',$col_name))
                    ->values(array($file_id,$id))
                    ->execute();
            }

            if ($album_id)
            {
                DB::insert('x_files_to_albums',array('file_id','album_id'))
                    ->values(array($file_id,$album_id))
                    ->execute();
            }

        }

        return 1;

    }

    private function create_file($titles,$is_main,$url,$code,$file_type_id,$type,$table_name,$col_name,$id,$session_id,$album_id = null)
    {
        $only_in_album = ($album_id)?1:0;

        $year = (int) date("Y");
        $month = (int) date("m");
        $day = (int) date("d");

        $m_lang = new Model_Cms_Plugin_Lng();
        $langs = $m_lang->get_all_active();
        foreach($titles as &$t)
        {
            $t = trim($t);
            $t = htmlspecialchars($t,ENT_QUOTES);
        }

        $file_info = DB::insert('x_files',
            array(
                'date',
                'srt',
                'url',
                'code',
                'file_type_id',
                'year',
                'month',
                'day',
                'is_main',
                'session_id',
                'only_in_album',
                'isset_file',
                'type'
            )
        )
            ->values(
                array(
                    date("Y-m-d H:i:s"),
                    Helper_Srt::get_max_srt('x_files')+1,
                    $url,
                    $code,
                    $file_type_id,
                    $year,
                    $month,
                    $day,
                    $is_main,
                    $session_id,
                    $only_in_album,
                    '0',
                    $type
                )
            )
            ->execute();

        $file_id = $file_info[0];

        foreach($langs as $l)
        {
            DB::update('x_file_locales')
                ->set(array(
                    'title'=>$titles[$l['id']],
                ))
                ->where('lng_id','=',$l['id'])
                ->where('file_id','=',$file_id)
                ->execute();
        }

        if ($id)
        {
            DB::insert('x_files_to_'.$table_name,array('file_id',$col_name))
                ->values(array($file_id,$id))
                ->execute();
        }

        if ($album_id)
        {
            DB::insert('x_files_to_albums',array('file_id','album_id'))
                ->values(array($file_id,$album_id))
                ->execute();
        }

        return 1;
    }
}