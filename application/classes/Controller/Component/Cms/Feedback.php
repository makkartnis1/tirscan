<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Feedback extends Controller{

    private $base_title = null;
    private $langs = null;
    private $post;
    private $get;


    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/feedback/v_index');
        $this->base_title = $this->post['base_title'];
        $this->get = $this->post['get'];

        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Feedback();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countAll();
        $res['data'] = $_dt->get($this->post);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_model = new Model_Component_Cms_Feedback();
        switch ($this->post['action']) {
            case 'delete':
                $m_model->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_model->delete($id);
                }
                break;
        }
        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}