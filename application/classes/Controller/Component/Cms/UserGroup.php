<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_UserGroup extends Controller{

    private $add_url = null;
    private $edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $lang_pref = $this->request->param('lang');
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->lang = $m_lang->get_one_by_pref($lang_pref);
        $this->langs = $m_lang->get_all_active();
    }


    public function action_index()
    {
        $this->view = View::factory('component/cms/user_group/index');
        $this->add_url = $this->post['add_url'];
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];
        $this->view->add_url = $this->add_url;
        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->render();
    }

    public function action_add()
    {
        $this->view = View::factory('component/cms/user_group/add');
        $this->view->langs = $this->langs;
        $m_user_group = new Model_Component_Cms_UserGroup();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $m_user_group->create($data,$this->langs);
            $this->view->change_ok = 1;
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;

        $this->render();
    }

    public function action_edit()
    {
        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/user_group/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_user_group = new Model_Component_Cms_UserGroup();

        $info = $m_user_group->get($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $m_user_group->update($this->id,$data,$this->langs);
            $this->view->change_ok = 1;
            $info = $m_user_group->get($this->id);
            $this->view->info = $info;
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_UserGroup();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll('id');
        $res['recordsFiltered'] = $_dt->countAll('id');
        $res['data'] = $_dt->get($this->post,$this->lang['id']);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_user = new Model_Component_Cms_UserGroup();
        switch ($this->post['action']) {
            case 'delete':
                $m_user->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_user->delete($id);
                }
                break;
        }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}