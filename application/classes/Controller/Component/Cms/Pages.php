<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Pages extends Controller{

    private $add_url = null;
    private $edit_url = null;
    private $base_title = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }


    public function action_index()
    {
        $this->view = View::factory('component/cms/pages/index');
        $this->add_url = $this->post['add_url'];
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];

        $this->view->add_url = $this->add_url;
        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->view->get = $this->post['get'];
        $this->render();
    }

    public function action_add()
    {
        $this->view = View::factory('component/cms/pages/add');
        $this->view->langs = $this->langs;

        $m_pages = new Model_Component_Cms_Pages();

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_pages->valid($data,$this->langs);
            if (empty($info['errors']))
            {
                $m_pages->add($info['data'],$this->langs);
                $this->view->change_ok = 1;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $this->render();
    }

    public function action_edit()
    {

        $this->id = $this->post['id'];

        $this->view = View::factory('component/cms/pages/edit');
        $this->view->langs = $this->langs;

        $this->view->id = $this->id;

        $m_pages = new Model_Component_Cms_Pages();

        $info = $m_pages->get_one($this->id);
        if (!$info) throw new Kohana_HTTP_Exception_404;
        $this->view->info = $info;

        $data = $this->post['data'];

        if (isset($data['go']))
        {
            $info = $m_pages->valid($data,$this->langs);
            if (empty($info['errors']))
            {
                $m_pages->save($this->id,$info['data'],$this->langs);
                $this->view->change_ok = 1;
                $info = $m_pages->get_one($this->id);
                $this->view->info = $info;
            }
            else
            {
                $this->view->errors = $info['errors'];
            }
        }

        $this->base_title = $this->post['base_title'];
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Pages();
        $res = array();
        if (!isset($this->post['get'])) $this->post['get'] = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered();
        $res['data'] = $_dt->get($this->post);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_pages = new Model_Component_Cms_Pages();
        switch ($this->post['action']) {
            case 'delete':
                $m_pages->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_pages->delete($id);
                }
                break;
        }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}