<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Admindialogs extends Controller{

    private $show_url = null;
    private $base_title = null;
    private $langs = null;
    private $id = null;
    private $post;
    private $get;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }

    public function action_index()
    {
        $this->view = View::factory('component/cms/dialogs/admin/v_index');
        $this->show_url = $this->post['show_url'];
        $this->base_title = $this->post['base_title'];

        $this->view->show_url = $this->show_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;

        $this->render();
    }

//    public function action_show()
//    {
//        $this->view = View::factory('component/cms/dialogs/admin/v_messages');
//        $this->get = $this->post['get'];
//
//        if (!isset($this->get['id'])) throw new Kohana_HTTP_Exception_404;
//
//        $this->id = (int) $this->get['id'];
//
//        if ($this->id <= 0) throw new Kohana_HTTP_Exception_404;
//
//        $this->view->id = $this->id;
//
//        $this->render();
//    }

    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_AdminDialogs();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered();
        $res['data'] = $_dt->get($this->post);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_model = new Model_Component_Cms_AdminDialogs();
        switch ($this->post['action']) {
            case 'delete':
                $m_model->delete($this->post['id']);
                break;
            case 'multi_delete':
                foreach($this->post['ids'] as $id)
                {
                    $m_model->delete($id);
                }
                break;
        }
        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }

}