<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Component_Cms_Locales extends Controller{

    private $callback_functon_btn = null;
    private $callback_functon_view = null;
    private $edit_url = null;
    private $base_title = null;
    private $lang = null;
    private $langs = null;
    private $id = null;
    private $post;

    function before()
    {
        parent::before();
        $this->post = $this->request->post();
        $m_lang = new Model_Cms_Plugin_Lng();
        $this->langs = $m_lang->get_all();
    }


    public function action_index()
    {
        $this->view = View::factory('component/cms/locales/index');
        $this->edit_url = $this->post['edit_url'];
        $this->base_title = $this->post['base_title'];

        $this->view->edit_url = $this->edit_url;
        $this->view->base_title = $this->base_title;
        $this->view->langs = $this->langs;
        $this->render();
    }


    public function action_db()
    {
        $_dt = new Model_Component_Cms_Dt_Locales();
        $res = array();
        $res['draw'] = $this->post['draw'];
        $res['recordsTotal'] = $_dt->countAll();
        $res['recordsFiltered'] = $_dt->countFiltered($this->post,$this->langs);
        $res['data'] = $_dt->get($this->post,$this->langs);
        echo json_encode($res);
        die;
    }

    public function action_ajax()
    {
        $m_locales = new Model_Component_Cms_Locales();
        switch ($this->post['action']) {
            case 'edit':
                $m_locales->edit($this->post['id'],$this->post['lng_id'],$this->post['translate']);
                break;
            }

        die;
    }

    private function render()
    {
        echo $this->response->body($this->view);
    }
}