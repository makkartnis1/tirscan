<?
include_once($_SERVER['DOCUMENT_ROOT'].'/application/views/cms/main_functions.php');
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title><?=(isset($seo_title))?''.$seo_title:''?></title>
    <meta name="keywords" content="<?=(isset($seo_key))?$seo_key:''?>">
    <meta name="description" content="<?=(isset($seo_desc))?$seo_desc:''?>">

    <link href="/adminex/css/style.css" rel="stylesheet">
    <link href="/adminex/css/style-responsive.css" rel="stylesheet">

    <link rel="shortcut icon" href="/public/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/public/images/favicon.ico" type="image/x-icon">

    <link rel="shortcut icon" href="/public/images/favicon.ico">
    <link rel="icon" href="/public/images/favicon.ico">



    <!--[if lt IE 9]>
    <script src="/adminex/js/html5shiv.js"></script>
    <script src="/adminex/js/respond.min.js"></script>
    <![endif]-->


    <script type="text/javascript" src="/public/cms/js/jquery-2.1.4.min.js"></script>
    <script src="/adminex/js/bootstrap.min.js"></script>
    <script src="/adminex/js/modernizr.min.js"></script>

    <script type="text/javascript" src="/public/cms/vendor/daterangepicker/moment.js"></script>
    <script type="text/javascript" src="/public/cms/vendor/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/public/cms/vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/public/cms/vendor/daterangepicker/daterangepicker-bs3.css" />

</head>

<? if (isset($block_top))
{
    foreach($block_top as $btop) echo $btop;
}
?>

<? if (isset($block_center))
{
    foreach($block_center as $bcenter) echo $bcenter;
}
?>

<? if (isset($block_footer))
{
    foreach($block_footer as $bfooter) echo $bfooter;
}
?>


</html>
