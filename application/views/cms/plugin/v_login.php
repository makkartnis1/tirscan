
<body class="login-body">

<div class="container">

    <form class="form-signin" action="" method="post">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">ABTON CMS</h1>
            <!--            <img src="/adminex/images/login-logo.png" alt=""/>-->
        </div>
        <div class="login-wrap">
            <div id="error_not_isset" class="alert alert-danger" style="<?=(!isset($error_not_isset))?'display: none;':''?>">Неправильний адрес ел. пошти або пароль</div>
            <div id="error_blocked" class="alert alert-danger" style="<?=(!isset($error_blocked))?'display: none;':''?>">Профіль заблоковано. Зверніться до адміністрації</div>
            <input type="text" name="email" class="form-control" placeholder="Эл. почта" autofocus>
            <input type="password" name="pass" class="form-control" placeholder="Пароль">

            <button class="btn btn-lg btn-login btn-block" type="submit">
                <i class="fa fa-check"></i>
            </button>


            <label class="checkbox">
                <input type="checkbox" name="rememb" value="remember-me"> Запам'ятати мене
                <span class="pull-right">
<!--                    <a data-toggle="modal" href="#myModal"> Забыли пароль?</a>-->
                </span>
            </label>

        </div>
    </form>

</div>

</body>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Забыли пароль?</h4>
            </div>
            <div class="modal-body">
                <p>Введите ваш адрес эл. почты на которую будет отправлена инструкция с далнейшими действиями</p>
                <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Отмена</button>
                <button class="btn btn-primary" type="button">Отправить</button>
            </div>
        </div>
    </div>
</div>
<!-- modal -->

<script type="text/javascript">
    $(document).on( "click", ".btn-login", function(e){
        e.preventDefault();
        var email = $("[name=email]").val();
        var pass = $("[name=pass]").val();
        var rememb = $("[name=rememb]").prop('checked');
        $.post("/cms/<?=$lang?>/login/valid/",{email:email,pass:pass,rememb:rememb},backCheck,"json");
    });

    function backCheck(data)
    {
        if (data=='ok')
        {
            $(".form-signin").submit();
        }

        if (data=='not_isset')
        {
            $("#error_blocked").hide();
            $("#error_not_isset").show();
        }

        if (data=='blocked')
        {
            $("#error_not_isset").hide();
            $("#error_blocked").show();
        }

    }
</script>