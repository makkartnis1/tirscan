<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$plugin_info['title']?>
                    <span id="add_modal" class="btn btn-default"><i class="fa fa-plus"></i> Додати валюту</span>
                    <div style="float: right; position: relative; z-index: 100;" class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Дії <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu" style="left:-55px;">
                            <li><a href="#" id="del_all">Видалити</a></li>
                        </ul>
                    </div>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>Назва</th>
                                <th>Код</th>
                                <th>Системне поле</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>Назва</th>
                                <th>Код</th>
                                <th>Системне поле</th>
                                <th>Дії</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/cms/<?=$lang?>/settings/db_currencies/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one" data-id="'+row.ID+'">';
                    }
                },
                {
                    className: "center",data:"name"
                },
                {
                    className: "center",data:"code"
                },
                {
                    className: "center",data:"localeName"
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<div style="min-width: 80px;"><button style="margin-right: 2px;" class="btn btn-info edit" data-id="'+row.ID+'" data-name="'+row.name+'" data-code="'+row.code+'" data-localeName="'+row.localeName+'"><i class="fa fa-edit"></i></button>'+
                            '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-title="'+row.name+'" data-toggle="button"><i class="fa fa-times"></i></button></div>';
                    }
                },
            ],
            "order": [[1, 'asc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

    });


    function callback_action()
    {
        window.table.draw(false);
    }

    $(document).on( "click", ".select_one", function(){
        var c_count = 0;
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one").prop('checked',true);
            $(".select_all").prop('checked',true);
        }
        else
        {
            $(".select_one").prop('checked',false);
            $(".select_all").prop('checked',false);
        }
    });

</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення валюти</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення валюти "<span id="title_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#title_del").html(title);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/cms/<?=$lang?>/currencies/ajax_currencies/del/",{id:id},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення валют</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних валют?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#del_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/cms/<?=$lang?>/currencies/ajax_currencies/del_all/",{ids:window.ids},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Додавання валюти</h4>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                Назва
                            </td>
                            <td>
                                <input type="text" name="name" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Код
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" name="code" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ключ
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" name="localeName" class="form-control">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                    <button type="submit" name="add_new" class="btn btn-success">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#add_modal", function(){
        $("#addModal").modal('show');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Редагування валюти</h4>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                Назва
                            </td>
                            <td>
                                <input type="text" id="name" name="name" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Код
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" id="code" name="code" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ключ
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" id="localeName" name="localeName" class="form-control">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                    <button type="submit" name="save" class="btn btn-success">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".edit", function(){

        $("#name").val($(this).data('name'));
        $("#localeName").val($(this).data('localename'));
        $("#code").val($(this).data('code'));

        $("[name=id]").val($(this).data('id'));
        $("#editModal").modal('show');
    });
</script>