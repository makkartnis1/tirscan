<style type="text/css">
    .text-success
    {
        color: #25af25 !important;
    }
    .text-danger
    {
        color: red !important;
    }
    .text-primary
    {
        color: #0000ed !important;
    }
    .text-orange
    {
        color: #ff9900 !important;
    }

    .general-table a
    {
        color: white !important;
        text-decoration: none !important;
        display: inherit;
    }
    .general-table td
    {
        vertical-align: middle !important;
    }
    .general-table th
    {
        text-align: left !important;
        font-size: 11px !important;
    }
    .clear_br
    {
        height: 3px;
    }
    .no_data
    {
        display: inherit;
        min-width: 25px;
        text-align: center;
    }
    .table_data td
    {
        font-size: 14px !important;
    }

    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        display: none;
    }
    .sorting_desc
    {
        background: none !important;
    }
    td.center
    {
        text-align: left !important;
    }


</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading" style="background: #eff0f4; padding: 0px 15px; border: none;">
    <?=$plugin_info['title']?>
</header>
<div class="panel-body" style="background: #eff0f4;">


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Загальний аналіз</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover general-table">
            <thead>
                <tr>
                    <th>Тип</th>
                    <th>За годину</th>
                    <th>Сьогодні</th>
                    <th>Тиждень</th>
                    <th>Місяць</th>
                    <th>Весь період</th>
                    <th>Опис</th>
                </tr>
            </thead>
            <tbody>
                <?

                    foreach($arr_help_1 as $key=>$val)
                    {
                        ?>
                        <tr>
                            <td><?=$val?></td>
                            <?
                            foreach($arr_time as $time)
                            {
                                ?>
                                <td>
                                    <?=$main_analytics[$key][$time]['closed']?>
                                    <div class="clear_br"></div>
                                    <?=$main_analytics[$key][$time]['process']?>
                                    <div class="clear_br"></div>
                                    <?=$main_analytics[$key][$time]['all']?>
                                </td>
                                <?
                            }
                            ?>
                            <td><i class="text-success">Успішні</i><br><i class="text-primary">В процесі</i><br><i class="text-muted">Всього</i></td>
                        </tr>
                        <?
                    }

                ?>

                <tr>
                    <td>Оцінки компаній</td>
                    <?
                        foreach($arr_time as $time)
                        {
                            ?>
                            <td>
                                <?=$main_analytics['comments'][$time]?>
                            </td>
                            <?
                        }
                    ?>
                    <td><i class="text-primary">Оцінок</i></td>
                </tr>

                <?

                    foreach($arr_help_2 as $key=>$val)
                    {
                        ?>
                        <tr>
                            <td><?=$val?></td>

                            <?
                                foreach($arr_time as $time)
                                {
                                    ?>
                                    <td>
                                        <?=$main_analytics[$key][$time]['approved']?>
                                        <div class="clear_br"></div>
                                        <?=$main_analytics[$key][$time]['pending']?>
                                    </td>
                                    <?
                                }
                            ?>
                            <td><i class="text-success">Верифіковані</i><br><i class="text-danger">Очікують перевірки</i></td>
                        </tr>
                        <?
                    }

                    foreach($arr_help_3 as $key=>$val)
                    {
                        ?>
                        <tr>
                            <td><?=$val?></td>
                            <?
                                foreach($arr_time as $time)
                                {
                                    ?>
                                    <td>
                                        <?=$main_analytics[$key][$time]?>
                                    </td>
                                    <?
                                }
                            ?>
                            <td><i class="text-success">Додано</i></td>
                        </tr>
                        <?
                    }
                ?>

            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Активність компаній</h3>
    </div>
    <div class="panel-body">
        <table class="table table_data general-table" id="company_analytics">
            <thead>
            <tr>
                <th>Рейтинг</th>
                <th>Назва</th>
                <th>Оцінки</th>
                <th>Транспортні заявки</th>
                <th>Вантажні заявки</th>
                <th>Тендери</th>
                <th>Менеджери</th>
                <th>Транспорт</th>
                <th>Новини</th>
                <th>Валюта</th>
            </tr>
            </thead>
        </table>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Аналітика покупки валюти</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover general-table">
            <thead>
            <tr>
                <th>Послуга</th>
                <th>За годину</th>
                <th>Сьогодні</th>
                <th>Тиждень</th>
                <th>Місяць</th>
                <th>Всього</th>
                <th>Опис</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?
                foreach($arr_help_4 as $key=>$val)
                {
                ?>
                    <tr>
                        <td><?=$val?></td>
                        <?
                        foreach($arr_time as $time)
                        {
                            ?>
                            <td>
                                <?=$services[$key][$time]?>
                            </td>
                        <?
                        }
                        ?>
                        <td><i class="text-success">Куплено валюти</i></td>
                    </tr>
                <?
                }
            ?>

            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Активність користувачів</h3>
    </div>
    <div class="panel-body">
        <table class="table general-table">
            <thead>
            <tr>
                <th>Тип</th>
                <th>За годину</th>
                <th>Сьогодні</th>
                <th>Тиждень</th>
                <th>Місяць</th>
                <th>Всього</th>
                <th>Опис</th>
            </tr>
            </thead>
            <tbody>

            <?
                foreach($arr_help_5 as $key=>$val)
                {
                    ?>
                    <tr>
                        <td><?=$val?></td>

                        <?
                            foreach($arr_time as $time)
                            {
                                ?>
                                <td>
                                    <?
                                        if ($key == 'visits')
                                        {
                                            echo $user_activities[$key][$time]['auths'];
                                            echo '<div class="clear_br"></div>';
                                            echo $user_activities[$key][$time]['anonims'];
                                        }
                                        else
                                        {
                                           echo $user_activities[$key][$time];
                                        }
                                    ?>
                                </td>
                                <?
                            }
                        ?>


                        <td>
                            <?
                                if ($key == 'online' or $key == 'unique')
                                {
                                    ?>
                                        <i class="text-success">Користувачів</i>
                                    <?
                                }
                                elseif($key == 'visits')
                                {
                                    ?>
                                        <i class="text-success">Авторизовані</i>
                                        <div class="clear_br"></div>
                                        <i class="text-primary">Неавторизовані</i>
                                    <?
                                }
                            ?>
                        </td>
                    </tr>
                    <?
                }
            ?>

            </tbody>
        </table>
    </div>
</div>



<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Географічна аналітика по країнах</h3>
    </div>
    <div class="panel-body">
        <table class="table table_data general-table" id="country_analytics">
            <thead>
            <tr>
                <th>Країна</th>
                <th>Транспорт з країни</th>
                <th>Транспорт в країну</th>
                <th>Вантажі з країни</th>
                <th>Вантажі в країну</th>
                <th>Зареєстрованих компаній</th>
            </tr>
            </thead>
        </table>
    </div>
</div>


</div>


</div>

</section>
</div>
</div>
</div>



<script type="text/javascript">
    $(function(){

        //аналітика компаній
        window.company_analytics = $('#company_analytics').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/company/db_analytics/",
                type: "POST"
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            "iDisplayLength": 10,
            columns: [
                {
                    className: "center",data:"rating","orderable": false
                },
                {
                    className: "center",data:"name","orderable": false
                },
                {
                    className: "center",data:"marks","orderable": false
                },
                {
                    className: "center",data:"transports","orderable": false,
                    render: function (val, type, row) {
                        return row.transports.closed+'<div class="clear_br"></div>'+row.transports.process+'<div class="clear_br"></div>'+row.transports.all;
                    }
                },
                {
                    className: "center",data:"loads","orderable": false,
                    render: function (val, type, row) {
                        return row.loads.closed+'<div class="clear_br"></div>'+row.loads.process+'<div class="clear_br"></div>'+row.loads.all;
                    }
                },
                {
                    className: "center",data:"tenders","orderable": false,
                    render: function (val, type, row) {
                        return row.tenders.closed+'<div class="clear_br"></div>'+row.tenders.process+'<div class="clear_br"></div>'+row.tenders.all;
                    }
                },
                {
                    className: "center",data:"managers","orderable": false
                },
                {
                    className: "center",data:"own_transports","orderable": false
                },
                {
                    className: "center",data:"company_news","orderable": false
                },
                {
                    className: "center",data:"services_money","orderable": false
                }
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


        //географічна аналітика по країнам
        window.country_analytics = $('#country_analytics').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/countries/db_analytics/",
                type: "POST"
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"name","orderable": false
                },
                {
                    className: "center",data:"transports_from","orderable": false,
                    render: function (val, type, row) {
                        return row.transports_from.closed+'<div class="clear_br"></div>'+row.transports_from.process+'<div class="clear_br"></div>'+row.transports_from.all;
                    }
                },
                {
                    className: "center",data:"transports_to","orderable": false,
                    render: function (val, type, row) {
                        return row.transports_to.closed+'<div class="clear_br"></div>'+row.transports_to.process+'<div class="clear_br"></div>'+row.transports_to.all;
                    }
                },
                {
                    className: "center",data:"loads_from","orderable": false,
                    render: function (val, type, row) {
                        return row.loads_from.closed+'<div class="clear_br"></div>'+row.loads_from.process+'<div class="clear_br"></div>'+row.loads_from.all;
                    }
                },
                {
                    className: "center",data:"loads_to","orderable": false,
                    render: function (val, type, row) {
                        return row.loads_to.closed+'<div class="clear_br"></div>'+row.loads_to.process+'<div class="clear_br"></div>'+row.loads_to.all;
                    }
                },
                {
                    className: "center",data:"company_count","orderable": false
                }
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

    });

</script>





