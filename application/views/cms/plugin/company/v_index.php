<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">

    <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Список компаній
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <div class="filter-block">
                        <form class="form-horizontal adminex-form" enctype="multipart/form-data">

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;ID</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <input style="min-width: 140px;margin-bottom:3px" name="ID" type="text" class="form-control" value="<?=(isset($filter['ID']))?$filter['ID']:''?>" placeholder="введіть ID">
                                </div>
                                <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;назві&nbsp;компанії</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <input style="min-width: 140px;margin-bottom:3px" name="name" type="text" class="form-control" value="<?=(isset($filter['name']))?$filter['name']:''?>" placeholder="введіть назву">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label" style="text-align: left;">Пошук по адресі</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <div id="places_from_block">


                                        <?
                                        if (isset($filter['places_from']))
                                        {
                                            $i_from_group = 0;
                                            foreach($filter['places_from_groups'] as $geo_group)
                                            {
                                                $i_from_group++;
                                                ?>
                                                <div class="one_place_block">
                                                    <input placeholder="введіть адресу" style="min-width: 250px" type="text" id="place_from_<?=$i_from_group?>" class="form-control dynamic_place_inputs" value="<?=$geo_group['name']?>">
                                                    <input name="places_from[]" id="place_from_<?=$i_from_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                                </div>
                                            <?
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            <div class="one_place_block">
                                                <input placeholder="введіть адресу" style="min-width: 250px" type="text" id="place_from_1" class="form-control dynamic_place_inputs">
                                                <input name="places_from[]" id="place_from_1_places" type="hidden">
                                            </div>
                                        <?
                                        }
                                        ?>

                                    </div>

                                </div>

                                <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;IPN</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <input style="min-width: 140px;margin-bottom:3px" name="IPN" type="text" class="form-control" value="<?=(isset($filter['IPN']))?$filter['IPN']:''?>" placeholder="введіть IPN">
                                </div>

                            </div>


                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label" style="text-align: left;">Пошук&nbsp;по&nbsp;телефону</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <input style="min-width: 140px;margin-bottom:3px" name="phone" type="text" class="form-control" value="<?=(isset($filter['phone']))?$filter['phone']:''?>" placeholder="введіть телефон">
                                </div>
                                <label class="col-sm-12 control-label" style="text-align: left;">Статус</label>
                                <div class="col-sm-12" style="text-align: left;">
                                    <select name="approvedByAdmin" class="form-control">
                                        <option value="">---</option>
                                        <option <?=(isset($filter['approvedByAdmin']))?(($filter['approvedByAdmin']=='1')?'selected':''):''?> value="1">Підтверджена</option>
                                        <option <?=(isset($filter['approvedByAdmin']))?(($filter['approvedByAdmin']=='0')?'selected':''):''?> value="0">Не підтверджена</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Датa&nbsp;реєстрації</label>
                                <div class="col-sm-7" style="text-align: left;">
                                    <input style="min-width: 140px;margin-bottom:3px" name="dateCreate1" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate1']))?$filter['dateCreate1']:''?>" placeholder="від">
                                </div>
                                <div class="col-sm-7" style="text-align: left;">
                                    <input style="min-width: 140px" name="dateCreate2" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate2']))?$filter['dateCreate2']:''?>" placeholder="до">
                                </div>
                                <div class="col-sm-7" style="text-align: left;">
                                    <div class="btn btn-link btn_select_widget" id="clearDateCreate">Очистити</div>
                                    <script type="text/javascript">
                                        $(document).on( "click", "#clearDateCreate", function(){
                                            $("[name=dateCreate1]").val('');
                                            $("[name=dateCreate2]").val('');
                                        });
                                    </script>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">

                                <div class="col-sm-12">
                                    <label class="col-sm-12 control-label" style=" text-align: left; padding-left: 0;">Тип&nbsp;компанії</label>
                                    <select name="typeID" class="form-control">
                                        <option value="">---</option>
                                        <?
                                        foreach($types as $c_t)
                                        {
                                            ?>
                                            <option <?=(isset($filter['typeID']))?(($filter['typeID']==$c_t['ID'])?'selected':''):''?> value="<?=$c_t['ID']?>"><?=$c_t['name']?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="col-sm-12 control-label" style=" text-align: left;padding-left: 0;">Форма&nbsp;власності</label>
                                    <select name="ownershipTypeID" class="form-control">
                                        <option value="">---</option>
                                        <?
                                        foreach($owner_types as $o_t)
                                        {
                                            ?>
                                            <option <?=(isset($filter['ownershipTypeID']))?(($filter['ownershipTypeID']==$o_t['ID'])?'selected':''):''?> value="<?=$o_t['ID']?>"><?=$o_t['name']?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="col-sm-3">

                                <div class="col-sm-12">
                                    <label class="col-sm-12 control-label" style=" text-align: left; padding-left: 0;">На&nbsp;сайті</label>
                                    <select name="registered" class="form-control">
                                        <option value="">---</option>
                                        <?
                                        foreach($register_times as $k => $v)
                                        {
                                            ?>
                                            <option <?=(isset($filter['registered']))?(($filter['registered']==$k)?'selected':''):''?> value="<?=$k?>"><?=$v?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="col-sm-12 control-label" style=" text-align: left;padding-left: 0;">На&nbsp;ринку</label>
                                    <select name="foundationDate" class="form-control">
                                        <option value="">---</option>
                                        <?
                                        foreach($foundation_times as $k => $v)
                                        {
                                            ?>
                                            <option <?=(isset($filter['foundationDate']))?(($filter['foundationDate']==$k)?'selected':''):''?> value="<?=$k?>"><?=$v?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>



                            </div>




                            <div class="col-sm-3">

                                <div class="col-sm-12">
                                    <label class="col-sm-12 control-label" style=" text-align: left; padding-left: 0;">Рейтинг</label>
                                    <select name="rating" class="form-control">
                                        <option value="">---</option>
                                        <?
                                        foreach($rating as $k => $v)
                                        {
                                            ?>
                                            <option <?=(isset($filter['rating']))?(($filter['rating']==$k)?'selected':''):''?> value="<?=$k?>"><?=$v?></option>
                                        <?
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-sm-12" style="margin-top: 23px">
                                    <div class="col-sm-6" style="width: 80px">
                                        <button style="margin-left: 0 !important;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Застосувати</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="/cms/<?=$lang?>/company" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Скинути фільтри</a>
                                    </div>
                                </div>

                            </div>



                        </div>

                        <script type="text/javascript">

                            $(function(){

                                <?
                                $num_place_from = 0;
                                $num_place_to = 0;

                                if (isset($filter['places_from']))
                                {
                                    foreach($filter['places_from_groups'] as $geo_group)
                                    {
                                        $num_place_from++;
                                        ?>
                                google_place_init('place_from_<?=$num_place_from?>','uk');
                                <?
                            }
                        }

                        if (isset($filter['places_to']))
                        {
                            foreach($filter['places_to_groups'] as $geo_group)
                            {
                                $num_place_to++;
                                ?>
                                google_place_init('place_to_<?=$num_place_to?>','uk');
                                <?
                            }
                        }

                        ?>

                                window.num_place_from = '<?=$num_place_from?>';
                                window.num_place_to = '<?=$num_place_to?>';

                                if (window.num_place_from == 0)
                                {
                                    window.num_place_from = 1;
                                    google_place_init('place_from_1','uk');
                                }
                                if (window.num_place_to == 0)
                                {
                                    window.num_place_to = 1;
                                    google_place_init('place_to_1','uk');
                                }


                            });

                            $(document).on( "click", "#add_place_from", function(){
                                window.num_place_from++;
                                var text = '<div class="one_place_block">' +
                                    '<input type="text" id="place_from_'+window.num_place_from+'" class="form-control dynamic_place_inputs"><input name="places_from[]" id="place_from_'+window.num_place_from+'_places" type="hidden">' +
                                    '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                                    '</div>';
                                $("#places_from_block").append(text);
                                google_place_init('place_from_'+window.num_place_from,'uk');
                            });

                            $(document).on( "click", "#add_place_to", function(){
                                window.num_place_to++;
                                var text = '<div class="one_place_block">' +
                                    '<input type="text" id="place_to_'+window.num_place_to+'" class="form-control dynamic_place_inputs"><input name="places_to[]" id="place_to_'+window.num_place_to+'_places" type="hidden">' +
                                    '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                                    '</div>';
                                $("#places_to_block").append(text);
                                google_place_init('place_to_'+window.num_place_to,'uk');
                            });

                            $(document).on( "click", ".delete_place", function(){
                                $(this).parent(".one_place_block").remove();
                            });

                        </script>

                        </form>
                        </div>


                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="example">
                            <thead>
                            <tr>
                                <th>ID<br>--<br>IPN</th>
                                <th style="min-width: 100px">Рейтинг<br>--<br>Назва</th>
                                <th style="min-width: 100px">Адрес<br>--<br>Телефон(и)</th>
                                <th>Тип<br>--<br>Форма&nbsp;власності</th>
                                <th>Транспорт/Вантажі/Тендери</th>
                                <th>Відгуки<br>(позитивні/нейтральні/негативні)</th>
                                <th>Менеджери<br>--<br>Власник</th>
                                <th>Власний транспорт</th>
                                <th>Новини</th>
                                <th>Куплено валюти</th>
                                <th style="min-width: 100px">На сайті<br>--<br>На ринку</th>
                                <th>Статус</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function(){

    window.table = $('#example').DataTable( {
        ajax: {
            url: "/cms/<?=$lang?>/company/db/",
            type: "POST",
            data : {
                filter: <?=(!empty($filter))?json_encode($filter):'1'?>
            }
        },
        drawCallback: function() {
            $(".select_all").prop('checked',false);
        },
        deferRender : true,
        serverSide: true,
        processing: true,
        columns: [
            {
                className: "center",data:"ID",
                render: function (val, type, row) {
                    return row.ID+'<br>--<br>'+row.ipn;
                }
            },
            {
                className: "center",data:"rating",
                render: function (val, type, row) {
                    return row.rating+'<br>--<br><a target="_blank" class="link_name" href="/cms/uk/company/edit/'+row.ID+'">'+row.name+'</a>';
                }
            },
            {
                className: "center",data:"address","orderable": false,
                render: function (val, type, row) {
                    var phone = (row.phone)?row.phone:'';
                    var phone2 = (row.phone2)?',<br>'+row.phone2:'';
                    var phones = phone+''+phone2;
                    if (!phones)
                    {
                        phones = '<i>не вказано</i>';
                    }

                    return row.address+'<br>--<br>'+phones;
                }
            },
            {
                className: "center",data:"type","orderable": false,
                render: function (val, type, row) {
                    return '<span class="label label-primary">'+row.type+'</span><br>--<br>'+row.ownershipType;
                }
            },
            {
                className: "center",data:"requests","orderable": false,
                render: function (val, type, row) {
                    return row.transport_req+row.cargo_req+row.tenders;
                }
            },
            {
                className: "center",data:"comments","orderable": false,
                render: function (val, type, row) {
                    return row.positive+row.neutral+row.negative;
                }
            },
            {
                className: "center",data:"users","orderable": false,
                render: function (val, type, row) {
                    return row.users+'<br>--<br>'+row.owner;
                }
            },
            {
                className: "center",data:"transports","orderable": false,
                render: function (val, type, row) {
                    return row.transports;
                }
            },
            {
                className: "center",data:"news","orderable": false,
                render: function (val, type, row) {
                    return row.news;
                }
            },
            {
                className: "center",data:"valuta","orderable": false,
                render: function (val, type, row) {
                    return row.valuta;
                }
            },
            {
                className: "center",data:"dates","orderable": false,
                render: function (val, type, row) {
                    return row.register_date+'<br>--<br>'+row.founding_date;
                }
            },
            {
                className: "center",data:"approvedByAdmin","orderable": false,
                render: function (val, type, row) {
                    var apr = row.approvedByAdmin;
                    var apr_text = (apr=='yes')?'<span class="label label-success">Підтверджена</span>':'<span class="label label-warning">Не підтверджена</span>';
                    return apr_text;
                }
            },
            {
                className: "center",
                "orderable": false,
                render: function (val, type, row) {
                    var res = '<div style="min-width: 80px;">';
                    res += '<a style="margin-right:2px;" href="/cms/<?=$lang?>/company/edit/'+row.ID+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                    res += '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-name="'+row.name+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                    return res+'</div>';
                }
            },
        ],
        "order": [[0, 'desc']],
        language: {
            processing:     "Завантажую..",
            search:         "Пошук&nbsp;:",
            lengthMenu:     "Записів на сторінці: _MENU_ ",
            info:           "Показано записів з _START_ по _END_ із _TOTAL_",
            infoEmpty:      "Записів немає",
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Даних немає",
            emptyTable:     "Даних немає",
            paginate: {
                first:      "Перша",
                previous:   "Назад",
                next:       "Вперед",
                last:       "Остання"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
            }
        }

    } );


});


function callback_action()
{
    window.table.draw(false);
}


$(document).on( "click", ".select_one", function(){
    var c_count = 0;
    $(".select_one").each(function(){
        if ($(this).prop('checked'))
        {
            c_count++;
        }
    });

});

$(document).on( "click", ".select_all", function(){
    if ($(this).prop('checked'))
    {
        $(".select_one").prop('checked',true);
        $(".select_all").prop('checked',true);
    }
    else
    {
        $(".select_one").prop('checked',false);
        $(".select_all").prop('checked',false);
    }
});

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення компанії</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення компанії "<span id="name_del"></span>"? <i>Компанія не буде видалена якщо за нею закріплений власник або хоча б один користувач</i>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("#name_del").html(name);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/cms/<?=$lang?>/company/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення вибраних компаній</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних компаній?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/cms/<?=$lang?>/company/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

