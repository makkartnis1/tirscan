<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$plugin_info['title']?>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example">
                            <thead>
                            <tr>
                                <th>Тип</th>
                                <th>URL</th>
                                <th>Captcha</th>
                                <th>Статус</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/cms/<?=$lang?>/settings/db_parser/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"type","orderable": false,
                    render: function (val, type, row) {
                        var t = 'Транспорт';
                        if (row.type == '2') t = 'Вантажі';
                        if (row.type == '3') t = 'Компанії';
                        return t;
                    }
                },
                {
                    className: "center",data:"url","orderable": false,
                },
                {
                    className: "center",data:"captcha","orderable": false,
                },
                {
                    className: "center",data:"status","orderable": false,
                    render: function (val, type, row) {
                        var s = 'Нова';
                        if (row.status == '0') s = 'Використана';
                        if (row.status == '2') s = 'Потрібна нова';
                        return s;
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<div style="min-width: 80px;">' +
                            '<button style="margin-right: 2px;" class="btn btn-info edit" data-id="'+row.id+'" data-url="'+row.url+'" data-captcha="'+row.captcha+'" data-status="'+row.status+'" data-type="'+row.type+'"><i class="fa fa-edit"></i></button>'+
                            '</div>';
                    }
                },
            ],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

    });


    function callback_action()
    {
        window.table.draw(false);
    }

</script>


<script type="text/javascript">
    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Редагування значення парсера</h4>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                Тип
                            </td>
                            <td>
                                <span id="type"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                URL
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" id="url" name="url" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Current captcha image
                            </td>
                            <td>
                                <img src="" alt="current captcha" id="currentCaptcha">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Captcha
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" id="captcha" name="captcha" class="form-control">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                    <button type="submit" name="save" class="btn btn-success">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".edit", function(){

        var t = $(this).data('type');
        var ty = 'Транспорт';
        if (t == '2') ty = 'Вантажі';
        if (t == '3') ty = 'Компанії';

        $("#type").html(ty);
        $("#url").val($(this).data('url'));
        $("#currentCaptcha").attr('src', $(this).data('url'));
        $("#captcha").val($(this).data('captcha'));

        $("[name=id]").val($(this).data('id'));
        $("#editModal").modal('show');
    });
</script>