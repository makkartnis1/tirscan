<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$plugin_info['title']?>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example">
                            <thead>
                            <tr>
                                <th>Ключ</th>
                                <th>Опис</th>
                                <th>Сума (грн)</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/cms/<?=$lang?>/settings/db_bonuses/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"key"
                },
                {
                    className: "center",data:"name"
                },
                {
                    className: "center",data:"amount"
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<div style="min-width: 80px;"><button style="margin-right: 2px;" class="btn btn-info edit" data-id="'+row.ID+'" data-amount="'+row.amount+'" data-key="'+row.key+'" data-name="'+row.name+'"><i class="fa fa-edit"></i></button>'+
                            '</div>';
                    }
                },
            ],
            "order": [[1, 'asc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

    });


    function callback_action()
    {
        window.table.draw(false);
    }

</script>


<script type="text/javascript">
    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Редагування значення бонуса</h4>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                Ключ
                            </td>
                            <td>
                                <span id="key_edit"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Опис
                            </td>
                            <td>
                                <span id="name"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Сума (грн)
                            </td>
                            <td>
                                <input style="margin-top: 10px" type="text" id="amount" name="amount" class="form-control">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                    <button type="submit" name="save" class="btn btn-success">Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".edit", function(){

        $("#key_edit").html($(this).data('key'));
        $("#name").html($(this).data('name'));
        $("#amount").val($(this).data('amount'));

        $("[name=id]").val($(this).data('id'));
        $("#editModal").modal('show');
    });
</script>