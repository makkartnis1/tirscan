<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$plugin_info['title']?>
                </header>
                <div class="panel-body">
                    <div class="module_desc_div">
                        <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
                        <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
                    </div>
                    <?
                    if (isset($save_ok))
                    {
                        ?>
                        <div class="alert alert-success fade in">
                            Профіль адміністратора успішно оновлено
                        </div>
                    <?
                    }

                    if (isset($errors))
                    {
                        extract($errors,EXTR_PREFIX_ALL,"error");
                    }

                    if (isset($error_name))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Ім'я може містити букви, пробіл і символ "-"
                        </div>
                    <?
                    }

                    if (isset($error_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Довжина пароля повинна бути не меншою ніж 6 символів. Пароль може містити цифри, букви латинського алфавіту і наступні символи "_-.,;"
                        </div>
                    <?
                    }

                    if (isset($error_conf_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Пароль і підтвердження пароля повинні співпадати
                        </div>
                    <?
                    }

                    if (isset($error_email_isset))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Вказана електронна пошта уже зареєстрований в системі
                        </div>
                        <?
                    }
                    ?>

                    <form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ім'я&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="name" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=$admin_info['name']?>">
                            </div>
                        </div>

                        <?
                            if ($admin_info['group_id'] == '1')
                            {
                                ?>
                                <input type="hidden" name="group_id" value="1">
                                <?
                            }
                            else
                            {
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Тип&nbsp;<span class="required_span">*</span></label>
                                    <div class="col-sm-4">
                                        <select name="group_id" class="form-control">
                                            <?
                                            foreach($groups as $g)
                                            {
                                                ?>
                                                <option <?=($g['id']==$admin_info['group_id'])?'selected':''?> value="<?=$g['id']?>"><?=$g['title']?></option>
                                            <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?
                            }
                        ?>



                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="email" type="email" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів" required="" value="<?=$admin_info['email']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label abton-form-checkbox-title">Змінити пароль</label>
                            <div class="col-sm-1 abton-form-checkbox">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" name="change_pass">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Пароль</label>
                            <div class="col-sm-7">
                                <input name="pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Повтор пароля</label>
                            <div class="col-sm-7">
                                <input name="conf_pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Телефон(и)</label>
                            <div class="col-sm-7">
                                <input name="phone" type="tel" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів" value="<?=$admin_info['phone']?>">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="control-label col-sm-2">Фото</label>


                                <div class="col-sm-3">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">

                                    <?
                                    $file=$_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$admin_info['id'].'.jpg';
                                    if (file_exists($file))
                                    {
                                        $img = '/uploads/images/admins/'.$admin_info['id'].'.jpg';
                                    }
                                    ?>

                                    <div id="no_image" class="fileupload-new thumbnail" style="width: 200px; height: 150px; <?=(isset($img))?'display:none;':''?>">
                                        <img src="/public/cms/img/no_image_small.png">
                                    </div>
                                    <div id="image_ex" class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px; <?=(isset($img))?'display:block;':''?>">
                                        <?
                                        if (isset($img))
                                        {
                                            ?>
                                            <img src="<?=$img?>" style="max-height: 150px;">
                                        <?
                                        }
                                        ?>
                                    </div>
                                    <div style="width: 203px;">
                                                   <span id="reload_file" class="btn btn-default btn-file">
                                                   <span id="load_file" style="display:none;" class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати</span>
                                                   <span style="display:inline-block;" class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img" onchange="setStatus()">
                                                   </span>

                                        <a id="del_file" style="display:inline-block;" class="btn btn-danger fileupload-exists"><i class="fa fa-trash"></i> Видалити</a>
                                    </div>
                                </div>
                                </div>

                        </div>
                        <?
                        $is_file = (file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$admin_info['id'].'.jpg'))?1:0;
                        ?>
                        <input type="hidden" name="file_exist" value="<?=$is_file?>">
                        <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#del_file", function(){
        $("#no_image").show();
        $("#image_ex").hide();
        $("[name=file_exist]").val('0');

    });

    $(document).on( "click", "#reload_file", function(){
        $("#image_ex").show();
    });
</script>
<script type="text/javascript">
    function setStatus()
    {
        $("[name=file_exist]").val('1');
    }
</script>
