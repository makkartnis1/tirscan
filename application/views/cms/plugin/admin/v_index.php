<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Адміністрація
                    <a href="/cms/<?=$lang?>/admin/add/"><span class="btn btn-default"><i class="fa fa-plus"></i> Створити адміністратора</span></a>
                    <div style="float: right; position: relative; z-index: 100;" class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Дії <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu" style="left:-105px;">
                            <li><a href="#" id="del_all">Видалити</a></li>
                        </ul>
                    </div>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="example">
                            <thead>
                            <tr id="row_for_search_admin">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>Ім'я</th>
                                <th>Тип</th>
                                <th>Електронна пошта</th>
                                <th>Телефон(и)</th>
                                <th>Дата створення</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(function(){

        $('#row_for_search_admin th').each( function () {

            var ind = $(this).index();

            if (ind >= 1 && ind < 6 && ind!=2 && ind!=5)
            {
                $(this).append( '<input index="'+ind+'" type="text" style="width:95%" />' );
            }
            else if(ind == 2)
            {
                $(this).append( '<select index="'+ind+'" style="width:95%;" class="search_select"><option value="">---</option><?
                    foreach($groups as $g)
                    {
                        ?><option value="<?=$g['id']?>"><?=$g['title']?></option><?
                    }
                ?></select>' );
            }
            else if(ind == 5)
            {
                $(this).append( '<input index="'+ind+'" class="date_range" type="text" style="width:200px;">' );
            }
        } );


        window.table = $('#example').DataTable( {
            ajax: {
                url: "/cms/<?=$lang?>/admin/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one" data-id="'+row.id+'">';
                    }
                },
                {
                    className: "center",data:"name",
                    render: function (val, type, row) {
                        var avatar = '<img class="avatar-img" src="'+row.avatar+'"><br>';
                        return avatar+'<div class="user-name">'+row.name+'</div>';
                    }
                },
                {
                    className: "center",data:"group_id",
                    render: function (val, type, row) {
                        return '<span class="label label-primary">'+row.group_title+'</span>';
                    }
                },
                {
                    className: "center",data:"email",
                    render: function (val, type, row) {
                        return (row.email)?row.email:'-';
                    }
                },
                {
                    className: "center",data:"phone",
                    render: function (val, type, row) {
                        return (row.phone)?row.phone:'-';
                    }
                },
                {
                    className: "center",data:"register_date"
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="/cms/<?=$lang?>/admin/edit/'+row.id+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        if (row.id!=1)
                        {
                            res += '<button class="btn btn-danger delete" data-id="'+row.id+'" data-name="'+row.name+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        }
                        return res+'</div>';
                    }
                },
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

        var cb = function(start, end, label) {
        };
        var optionSet1 = {
            startDate: moment(),
            endDate: moment(),
            showDropdowns: true,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            ranges: {
                'Сьогодні': [moment().startOf('day'), moment().endOf('day')],
                'Вчора': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                '7 днів': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                '30 днів': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'Поточний місяць': [moment().startOf('month'), moment().endOf('month')],
                'Попередній місяць': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm',
            format: 'YYYY-MM-DD HH:mm',
            separator: ' - ',
            locale: {
                applyLabel: 'Застосувати',
                cancelLabel: 'Очистити',
                fromLabel: 'Від',
                toLabel: 'До',
                customRangeLabel: 'Налаштувати',
                daysOfWeek: ['Нд','Пн','Вт','Ср','Чт','Пт','Сб'],
                monthNames: ['Січень','Лютий','Березень','Квітень','Травень','Червень','Липень','Серпень','Вересень','Жовтень','Листопад','Грудень'],
                firstDay: 1
            }
        };


        $('.date_range').daterangepicker(optionSet1, cb).data('daterangepicker');

        $(document).on( "click", ".cancelBtn", function(){
            $('.date_range').val('').focus().trigger('keyup');
            $('.date_range').daterangepicker(optionSet1, cb);
        });


        <?
           for($k=1;$k<=5;$k++)
           {
               ?>
                $("#row_for_search_admin th input[index=<?=$k?>], #row_for_search_admin th select[index=<?=$k?>]").on( 'keyup change', function () {
                    window.table
                        .column(<?=$k?>)
                        .search(this.value)
                        .draw();
                } );
                <?
            }
        ?>


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one", function(){
        var c_count = 0;
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one").prop('checked',true);
            $(".select_all").prop('checked',true);
        }
        else
        {
            $(".select_one").prop('checked',false);
            $(".select_all").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення адміністратора</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення адміністратора "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалення</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("#name_del").html(name);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/cms/<?=$lang?>/admin/ajax/del/",{id:id},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення групи адміністраторів</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних адміністраторів?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#blocked_all", function(){
        set_all_ids();
        if (window.ids.length > 0)
        $.post("/cms/<?=$lang?>/admin/ajax/blocked_all/",{ids:window.ids},callback_itemAll,"json");
    });

    $(document).on( "click", "#active_all", function(){
        set_all_ids();
        if (window.ids.length > 0)
            $.post("/cms/<?=$lang?>/admin/ajax/active_all/",{ids:window.ids},callback_itemAll,"json");
    });

    $(document).on( "click", "#del_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/cms/<?=$lang?>/admin/ajax/del_all/",{ids:window.ids},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

