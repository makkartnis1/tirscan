<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Створення адміністратора
                </header>
                <div class="panel-body">
                    <div class="module_desc_div">
                        <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
                        <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
                    </div>
                    <?
                    if (isset($add_ok))
                    {
                        ?>
                        <div class="alert alert-success fade in">
                            Адміністратор успішно створений
                        </div>
                    <?
                    }

                    if (isset($errors))
                    {
                        extract($errors,EXTR_PREFIX_ALL,"error");
                    }

                    if (isset($error_name))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Ім'я може містити букви, пробіл і символ "-"
                        </div>
                        <?
                    }

                    if (isset($error_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Довжина пароля повинна бути не меншою ніж 6 символів. Пароль може містити цифри, букви латинського алфавіту і наступні символи "_-.,;"
                        </div>
                    <?
                    }

                    if (isset($error_conf_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Пароль і підтвердження пароля повинні співпадати
                        </div>
                    <?
                    }

                    if (isset($error_email_isset))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Вказана електронна пошта уже зареєстрований в системі
                        </div>
                        <?
                    }
                    ?>

                    <form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ім'я&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="name" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Тип&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-4">
                                <select name="group_id" class="form-control">
                                    <?
                                        foreach($groups as $g)
                                        {
                                            ?>
                                            <option value="<?=$g['id']?>"><?=$g['title']?></option>
                                            <?
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="email" type="email" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Пароль&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Повтор пароля&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="conf_pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Телефон(и)</label>
                            <div class="col-sm-7">
                                <input name="phone" type="tel" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="control-label col-sm-2">Фото</label>

                                <div class="col-sm-3">

                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="/public/cms/img/no_image_small.png" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div style="width: 203px;">
                                                   <span class="btn btn-default btn-file" style="margin-left: 0px;">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати</span>
                                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img">
                                                   </span>

                                            <a href="#" class="btn btn-danger fileupload-exists" style="margin-left: 0px;" data-dismiss="fileupload"><i class="fa fa-trash"></i> Видалити</a>
                                        </div>
                                    </div>

                                </div>

                        </div>

                        <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
