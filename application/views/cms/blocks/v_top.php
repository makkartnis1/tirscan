<link href="/public/cms/css/base.css" rel="stylesheet">
<!--gritter css-->
<link rel="stylesheet" type="text/css" href="/adminex/js/gritter/css/jquery.gritter.css" />

<!--pickers css-->
<link rel="stylesheet" type="text/css" href="/adminex/js/bootstrap-datepicker/css/datepicker-custom.css" />
<link rel="stylesheet" type="text/css" href="/adminex/js/bootstrap-timepicker/css/timepicker.css" />
<link rel="stylesheet" type="text/css" href="/adminex/js/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="/adminex/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="/adminex/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />

<!--ios7-->
<link rel="stylesheet" type="text/css" href="/adminex/js/ios-switch/switchery.css" />

<!--icheck-->
<link href="/adminex/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/minimal/red.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/minimal/green.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/minimal/blue.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/minimal/yellow.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/minimal/purple.css" rel="stylesheet">

<link href="/adminex/js/iCheck/skins/square/square.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/square/red.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/square/green.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/square/blue.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/square/yellow.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/square/purple.css" rel="stylesheet">

<link href="/adminex/js/iCheck/skins/flat/grey.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/flat/red.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/flat/blue.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/flat/yellow.css" rel="stylesheet">
<link href="/adminex/js/iCheck/skins/flat/purple.css" rel="stylesheet">

<!--multi-select-->
<link rel="stylesheet" type="text/css" href="/adminex/js/jquery-multi-select/css/multi-select.css" />

<!--file upload-->
<link rel="stylesheet" type="text/css" href="/adminex/css/bootstrap-fileupload.min.css" />

<!--tags input-->
<link rel="stylesheet" type="text/css" href="/adminex/js/jquery-tags-input/jquery.tagsinput.css" />

<script type="text/javascript" src="/adminex/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/adminex/js/jquery-multi-select/js/jquery.quicksearch.js"></script>

<body class="sticky-header <?=($_SESSION['collapse'])?'left-side-collapsed':''?>">

<section>
<!-- left side start-->
<div class="left-side sticky-left-side">


    <!--logo and iconic logo start-->
    <div class="logo">
        <div style="color: white; float: left; padding-right: 20px; border-right: 2px solid white;">
            <a style="font-size: 13px;" href="/<?=Route::get('cms')->uri(array('lang'=>$lang))?>">СИСТЕМА<br>УПРАВЛІННЯ<br>КОНТЕНТОМ</a>
        </div>
        <a style="font-size: 13px; position: relative; top:20px;" href="/<?=Route::get('cms')->uri(array('lang'=>$lang))?>">ABTON CMS</a>
        <div style="clear: both;"></div>
    </div>

    <div class="logo-icon text-center">
    </div>
    <!--logo and iconic logo end-->


    <div class="left-side-inner">


        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">

            <?


            foreach($plugins as $plugin)
            {
//                 if ($plugin['content']['id']==59 && $user_info['id']!=1) continue;

                if (!empty($plugin['child']) and $plugin['content']['controller']=='')
                {
                    ?>
                    <li class="menu-list <?=($plugin['content']['id']==$plugin_info['sub'])?'nav-active':''?>">
                        <a href="#" style="<?=($plugin['content']['id']==$plugin_info['sub'])?'':'color: #fff'?>">
                            <i style="<?=($plugin['content']['id']==$plugin_info['sub'])?'':'color: #fff'?>" class="fa <?=($plugin['content']['icon'])?$plugin['content']['icon']:'fa-bars'?>"></i>
                             <span><?=$plugin['content']['title']?>

                                 <?
                                 $counts = 0;

                                 foreach($plugin['child'] as $sub)
                                 {

                                     if (isset(${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']}))
                                     {
                                         if (${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']} > 0)
                                         {
                                             $counts += ${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']};
                                         }
                                     }
                                     else
                                     {
                                         if (isset(${'count_'.$sub['content']['controller']}))
                                         {
                                             if (${'count_'.$sub['content']['controller']} > 0)
                                             {
                                                 $counts += ${'count_'.$sub['content']['controller']};
                                             }
                                         }
                                     }
                                 }

                                 if ($counts > 0)
                                 {
                                     ?>
                                     <count class="badge badge-important"><?=$counts?></count>
                                 <?
                                 }
                                 ?>



                             </span>
                        </a>
                        <ul class="sub-menu-list">
                            <?

                            foreach($plugin['child'] as $sub)
                            {
                                ?>
                                <li class="<?=($sub['content']['id']==$plugin_info['id'])?'active':''?>">
                                    <a href="/<?=Route::get('cms')->uri(array('lang'=>$lang,'controller'=>$sub['content']['controller'],'action'=>$sub['content']['action']))?>">
                                        <?=$sub['content']['title']?>

                                        <?
                                        if (isset(${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']}))
                                        {
                                            if (${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']} > 0)
                                            {
                                                ?>
                                                <count class="badge badge-important"><?=${'count_'.$sub['content']['controller'].'_'.$sub['content']['action']}?></count>
                                            <?
                                            }
                                        }
                                        else
                                        {
                                            if (isset(${'count_'.$sub['content']['controller']}))
                                            {
                                                if (${'count_'.$sub['content']['controller']} > 0)
                                                {
                                                    ?>
                                                    <count class="badge badge-important"><?=${'count_'.$sub['content']['controller']}?></count>
                                                <?
                                                }
                                            }
                                        }
                                        ?>

                                    </a>
                                </li>
                            <?
                            }

                            ?>
                        </ul>
                    </li>
                <?

                }
                else
                {
                    ?>
                    <li class="<?=($plugin['content']['id']==$plugin_info['id'])?'active':''?>">
                        <a href="/<?=Route::get('cms')->uri(array('lang'=>$lang,'controller'=>$plugin['content']['controller'],'action'=>$plugin['content']['action']))?>"><i class="fa <?=($plugin['content']['icon'])?$plugin['content']['icon']:'fa-bars'?>"></i>
                             <span><?=$plugin['content']['title']?>

                                 <?
                                 if (isset(${'count_'.$plugin['content']['controller'].'_'.$plugin['content']['action']}))
                                 {
                                     if (${'count_'.$plugin['content']['controller'].'_'.$plugin['content']['action']} > 0)
                                     {
                                         ?>
                                         <count class="badge badge-important"><?=${'count_'.$plugin['content']['controller'].'_'.$plugin['content']['action']}?></count>
                                     <?
                                     }
                                 }
                                 else
                                 {
                                     if (isset(${'count_'.$plugin['content']['controller']}))
                                     {
                                         if (${'count_'.$plugin['content']['controller']} > 0)
                                         {
                                             ?>
                                             <count class="badge badge-important"><?=${'count_'.$plugin['content']['controller']}?></count>
                                         <?
                                         }
                                     }
                                 }
                                 ?>

                             </span>
                        </a>
                    </li>
                <?
                }
            }
            ?>
        </ul>

        <!--sidebar nav end-->

    </div>
</div>
<!-- left side end-->

<!-- main content start-->
<div class="main-content"  style="<?=($_SESSION['collapse'])?'width:96%;':'width:82%;'?>">

    <!-- header section start-->
    <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn" id="view_mode" collapse="<?=$_SESSION['collapse']?>"><i id="collapse_i" class="fa <?=($_SESSION['collapse'])?'fa-caret-square-o-right':'fa-caret-square-o-left'?>"></i></a>
        <!--toggle button end-->

        <!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <img height="30" src="<?=(file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads/images/admins/'.$user_info['id'].'.jpg'))?'/uploads/images/admins/'.$user_info['id'].'.jpg':'/public/cms/img/user.png'?>" alt="" />
                        <?=$user_info['name']?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        <li><a href="/<?=Route::get('cms')->uri(array('lang'=>$lang,'controller'=>'login','action'=>'logout'))?>"><i class="fa fa-sign-out"></i> <span>Вийти</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--notification menu end -->

    </div>
    <!-- header section end-->

