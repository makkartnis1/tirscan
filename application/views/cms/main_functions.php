<?php
function view_item_rub_list($rub,$del = '')
{
    if (empty($rub)) return;

    foreach($rub as $r)
    {
        if ($r['content']['count_items_all'] != $r['content']['count_items'])
        {
            echo '<option value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].' (в рубрике '.$r['content']['count_items'].', всего '.$r['content']['count_items_all'].')'.'</option>';
        }
        else
        {
            echo '<option value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].' ('.$r['content']['count_items'].')'.'</option>';
        }

        if (!empty($r['child']))
        {
            view_item_rub_list($r['child'],$del.'-');
        }
    }
}

function view_mats_rub_list($rub,$del = '')
{
    if (empty($rub)) return;

    foreach($rub as $r)
    {
        if ($r['content']['count_mats_all'] != $r['content']['count_mats'])
        {
            echo '<option value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].' (в рубрике '.$r['content']['count_mats'].', всего '.$r['content']['count_mats_all'].')'.'</option>';
        }
        else
        {
            echo '<option value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].' ('.$r['content']['count_mats'].')'.'</option>';
        }

        if (!empty($r['child']))
        {
            view_mats_rub_list($r['child'],$del.'-');
        }
    }
}

function view_rub_list($rub,$del = '')
{
    if (empty($rub)) return;

    foreach($rub as $r)
    {
        if (isset($GLOBALS['edit_rub_id']))
        {
            $selected = '';
            if ($GLOBALS['edit_rub_id'] == $r['content']['id']) $selected = 'selected';
            echo '<option '.$selected.' value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].'</option>';

        }
        else
        {
            echo '<option value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].'</option>';
        }


        if (!empty($r['child']))
        {
            view_rub_list($r['child'],$del.'-');
        }
    }
}

function view_list($res,$pr_key,$title_key,$is_group = 0,$group_key = '',$del_type,$del = '')
{
    if (empty($res)) return;

    foreach($res as $r)
    {

        $title = trim($del.' '.$r['content'][$title_key]);
        if ($is_group)
        {
            if (!isset($group_val)) $group_val = '';
            if ($group_val!=$r['content'][$group_key]) echo '<option disabled>['.$r['content'][$group_key].']</option>';
            $group_val = $r['content'][$group_key];
        }

        $selected = '';

        if (isset($GLOBALS['edit_sub']))
        {
            if ($GLOBALS['edit_sub'] == $r['content'][$pr_key]) $selected = 'selected';
        }

        if (isset($GLOBALS['edit_id']))
        {
            if ($GLOBALS['edit_id'] == $r['content'][$pr_key]) continue;
        }

        echo '<option '.$selected.' value="'.$r['content'][$pr_key].'">'.$title.'</option>';

        if (!empty($r['child']))
        {
            view_list($r['child'],$pr_key,$title_key,0,'',$del_type,$del.$del_type);
        }
    }
}

function view_rubs($rub,$del = '')
{
    if (empty($rub)) return;

    foreach($rub as $r)
    {
        if (isset($GLOBALS['checked_rubs']))
        {
            $checked = '';
            if (in_array($r['content']['id'],$GLOBALS['checked_rubs'])) $checked = 'checked';
            echo '<div class="flat-grey single-row icheck form_checkbox"><input type="checkbox" name="rubs[]" multiple value="'.$r['content']['id'].'" '.$checked.'>'.$del.' '.$r['content']['title'].'</div>';
        }
        else
        {
            echo '<div class="flat-grey single-row icheck form_checkbox"><input type="checkbox" name="rubs[]" multiple value="'.$r['content']['id'].'">'.$del.' '.$r['content']['title'].'</div>';

        }

        if (!empty($r['child']))
        {
            view_rubs($r['child'],$del.'-');
        }
    }
}
