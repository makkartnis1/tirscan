<?  
/**
 * Created by
 * User: LINKeR
 * Date: 25.04.16
 * Time: 12:38
 *
 * Суть док-блока - показати список можливих змінних
 * 
 * Обовязкові:
 * @var $name           string      ім"я інпута
 * @var $type           string      тип інпута
 * @var $value          string      значення інпута
 * @var $caption        string      Надпис над формою
 * @var $id             string      ID елемента на сторінці, важливо для розмітки
 *
 * Необовязкові:
 * @var $error_text     string      Якщо передавати як не пусту строку - то input підсвітиться помилкою
 * @var $necessarily    boolean     Чи виводити "*"
 * @var $placeholder    string      placeholder для інпута
 *
 *  Якщо тип "textarea":
 *      Необовязкові 
 *      @var $cols          int     
 *      @var $rows          int
 *
 *  Якщо тип "button"|"submit":
 *      @var $btn_class           string  class кнопки btn-xs | btn-success etc..
 * tirscan/application/config/frontend/profile/form-component/input-err.php
 * 
 */

$type = strtolower($type);
$placeholder = isset($placeholder) ? $placeholder : null;

?><div class="form-group <?= (isset($error_text) || !empty($error_text)) ? 'errorForm' : null ?>">
    <label for="<?= $id ?>"><?= $caption ?> <? if(isset($necessarily) AND $necessarily) { ?><span>*</span> <? } ?></label>
    <div class="input-group">
        
        <?
            $input_data = [
                'placeholder'   => $placeholder,
                'name'          => $name,
                'id'            => $id,
                'value'         => $value,
            ];
        
        ?>

        <? switch ($type) {

            case ('textarea'):
            case ('textarea.cke-editor'):
                $input = View::factory('frontend/content/profile/component/frm/textarea',['ck' => (boolean) (strpos($type, 'cke-editor') !== false)]);

                if (isset($cols))
                    $input_data['cols'] = $cols;

                if (isset($rows))
                    $input_data['rows'] = $rows;

                break;

            case ('button'):
            case ('submit'):
            case ('reset'):
                if (isset($btn_class))
                    $input_data['btn_class'] = $btn_class;
                $input = View::factory('frontend/content/profile/component/frm/button');
                break;
            
            case ('select'):
                $input = View::factory('frontend/content/profile/component/frm/select');
                $input_data['value']        = $value[0];
                $input_data['valueKEY']     = $value[1];
                $input_data['hintKEY']      = $value[2];
                $input_data['active']       = Arr::get($value, 3);
                //$input_data['option'] =
                break;
            case ('email'):
            case ('text'):
            case ('number'):
            case ('password'):

                $input_data['type'] = $type;

                $input = View::factory('frontend/content/profile/component/frm/input');
                break;

            case ('phone+code'):
                $input_data['name']     = explode('/', $input_data['name']);
                $input_data['value']    = explode('/', $input_data['value']);
                $input = View::factory('frontend/content/profile/component/frm/phone-n-code');
                break;

            default:
                $input = 'undefined input type!';
        }

            if($input instanceof View)
                $input->set($input_data);

        ?>

        <?= $input ?>
                    
    </div>

    <? if(isset($error_text) || !empty($error_text)) { ?>
    <ul class="errorText">
        <li><?= $error_text ?></li>
    </ul>
    <? } ?>
</div>