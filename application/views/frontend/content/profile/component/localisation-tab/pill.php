<?php
/**
 * Created by
 * User: LINKeR
 * Date: 25.04.16
 * Time: 17:06
 */
?>
<li role="presentation" class="<? if($active) { ?>active<? } ?>">
    <a href=".<?= $class ?>" aria-controls="settings" role="tab" data-toggle="tab">
        <?= $caption ?>
    </a>
</li>