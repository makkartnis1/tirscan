<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 25.04.16
 * Time: 17:06
 */
?>
<div role="tabpanel" class="<?= $class ?> tab-pane fade input-tab-pane <? if($active) { ?>in active <? } ?>">
    <?= $content ?>
</div>