<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 26.04.16
 * Time: 16:00
 */?>

<select name="<?= $name ?>" id="<?= $id ?>" class="form-control">

    <? foreach ($value as $i => $option) { ?>

        <?
            if($active === null) {

                $selected = (boolean) ($i == 0);

            }else{

                $selected = (boolean) ($option[$valueKEY] == $active);

            }
        ?>

    <option value="<?= $option[$valueKEY] ?>"> <?= $option[$hintKEY] ?> </option>
    <? } ?>

</select>
