<?php
/**
 * Created by
 * User: LINKeR
 * Date: 25.04.16
 * Time: 14:21
 */?>
<div class="row">
    <div class="col-md-3">
        <select class="form-control" name="<?= $name[1] ?>" id="<?= $id ?>-code"><? $codes = Model_Frontend_Phonecodes::phoneCodes(); ?>

            <? foreach ($codes as $key => $code) { ?>

                <?
                    if(!empty($value[1])){

                        $selected = (boolean) ($code['ID'] == $value[1]);

                    }else{

                        $selected = (boolean) ($key == 0);

                    }

                ?>

                <option <?= ($selected) ? 'selected' : null ?> value="<?= $code['ID'] ?>"><?= $code['code'] ?></option>

            <? } ?>
        </select>
    </div>
    <div class="col-md-9">

        <?= View::factory('frontend/content/profile/component/frm/input',[
            'name'          => $name[0],
            'id'            => $id.'-phone',
            'type'          => 'text',
            'value'         => $value[1],
            'placeholder'   => '',
        ])?>

    </div>
</div>