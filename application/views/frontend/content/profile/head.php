<?
/**
 * @var Controller_Frontend_Profile $controller
 */
?>

    <div class="userInfo">
        <div class="row">
            <div class="col-sm-6">
                <div class="clearfix">
                    <div class="imgHolder">
                        <div class="img" data-change-avatar style="background-image: url('<?= $avatar ?>')"></div>
<!--                        <div style="display: none;">
                            <?= $changeAvatar_widget ?>
                        </div>-->
                        <div class="status online"></div>
<!--                        <a class="messagesQnt" href="#"></a>-->
                    </div>
                    <div class="caption">
                        <p class="userName"><?= $controller->user->company->locales->find()->name ?></p>
                        <div class="userOptions">
                            <span><i class="fa fa-money" aria-hidden="true"></i> <a href="javascript:;" class="go-to-wallet"><?= $controller->user->wallet ?></a> грн</span>
                            <span><i class="fa fa-sliders" aria-hidden="true"></i> <a href="javascript:;" class="go-to-infoediting"><? __dbt('Налаштування профілю') ?></a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="buttonHolder">
                    <ul class="text-right">
                        <? if ($controller->user->hasRights('transport')): ?>
                        <li>
                            <a class="btn btn-blue" href="<?= $controller->links['transport_add_page'] ?>"><? __dbt('Додати транспорт') ?></a>
                        </li>
                        <? endif; ?>
                        <? if ($controller->user->hasRights('cargo')): ?>
                        <li>
                            <a class="btn btn-blue" href="<?= $controller->links['cargo_add_page'] ?>"><? __dbt('Додати вантаж') ?></a>
                        </li>
                        <li>
                            <a class="btn btn-blue" href="<?= $controller->links['tender_add_page'] ?>"><? __dbt('Додати тендер') ?></a>
                        </li>
                        <? endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<!--    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-2">-->
<!--        <div class="profile-img-box">-->
<!--            <div class="img-block">-->
<!--                <div class="img-box">-->
<!--                    <img class="img-responsive" src="--><?//= $avatar ?><!--">-->
<!--                    <a href="#" class="fadeInDown" data-change-avatar>-->
<!--                        <i class="fa fa-arrow-circle-up fa-2x"></i>-->
<!--                    </a>-->
<!--                    -->
<!--                </div>-->
<!---->
<!--                <div style="display: none;">-->
<!--                    --><?//= $changeAvatar_widget ?>
<!--                </div>-->
<!--                -->
<!--            </div>-->
<!--            <div class="img-block-middle">-->
<!--                <span class="green">Online</span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-10">-->
<!--        <div class="top-profile">-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-7 col-md-6">-->
<!--                    <p class="profile-name">-->
<!--                        --><?//= $controller->user->company->locales->find()->name ?>
<!--                        <a href="--><?//= Route::url('frontend_site_profile', $controller->getRequestParams()) . '#wallet' ?><!--">-->
<!--                            <i class="fa fa-credit-card"></i>--><?//= $controller->user->wallet ?><!-- грн-->
<!--                        </a>-->
<!--                    </p>-->
<!--                    --><?// if ($controller->user->isOwnerOfProfileCompany()) { ?>
<!--                    <p class="sub">-->
<!--                        <a href="javascript:;" class="go-to-infoediting">--><?// __dbt('Редагувати ПІБ або назву компанії') ?><!--</a>-->
<!--                    </p>-->
<!--                    --><?// } ?>
<!--                </div>-->
<!--                --><?// if ($controller->user->hasRights('transport')): ?>
<!--                <div class="col-xs-6 col-sm-3 col-md-3">-->
<!--                    <p class="padding first">-->
<!--                        <a href="--><?//= $controller->links['transport_add_page'] ?><!--" type="button" class="btn btn-primary"><b>--><?// __dbt('Додати транспорт') ?><!--</b></a>-->
<!--                    </p>-->
<!--                </div>-->
<!--                --><?// endif; ?>
<!--                --><?// if ($controller->user->hasRights('cargo')): ?>
<!--                <div class="col-xs-6 col-sm-3 col-md-3">-->
<!--                    <p class="padding">-->
<!--                        <a href="--><?//= $controller->links['cargo_add_page'] ?><!--" type="button" class="btn btn-primary"><b>--><?// __dbt('Додати вантаж') ?><!--</b></a>-->
<!--                    </p>-->
<!--                </div>-->
<!--                --><?// endif; ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->


<?= View::factory('frontend/content/profile/tab/ticket/modals') ?>