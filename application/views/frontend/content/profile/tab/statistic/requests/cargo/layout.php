<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 31.05.16
 * Time: 09:46
 */?>

<div class="row">
<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= __db('stat.cargo.proportion[:duration:]',[ '[:duration:]' => __db('stat.duration.whole.time') ]) ?></h3>
        </div>
        <div class="panel-body">
            <script id="cargo-request-booblik" type="application/json"><?/*        README:: буде читатись через EVAL! такий підхід дозволить динамічно міняти графіки      */?></script>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= __db('stat.cargo.chart') ?>: <span data-from></span>-<span data-to></span></h3>
        </div>
        <div class="panel-body">
            <script id="cargo-request-chart" type="application/json"><?/*        README:: буде читатись через EVAL! такий підхід дозволить динамічно міняти графіки      */?></script>
        </div>
    </div>
</div>
</div>