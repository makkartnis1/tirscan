<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 31.05.16
 * Time: 15:29
 */?>
<form>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <label>
                    <input type="radio"     name="group" value="year">
                    <?= __db('stat.show.range.year') ?>
                </label>
                <input type="text" class="form-control" data-this-is="datepicker" name="year" value="<?= date('Y') ?>">
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <label>
                    <input type="radio"     name="group" value="month" checked>
                    <?= __db('stat.show.range.month') ?>
                </label>
                <input type="text" class="form-control" data-this-is="datepicker" name="month" value="<?= date('Y').'/'.date('m') ?>">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">

                <label class="pull-left">
                    <input type="radio"     name="show" value="dataset" checked>
                    <?= __db('stat.show.chart.range.diff') ?>
                </label>

                <label class="pull-right">
                    <input type="radio"     name="show" value="dataset_with_before">
                    <?= __db('stat.show.chart.range.all') ?>
                </label>

            </div>
        </div>
    </div>

    <input type="hidden" name="curYear">
    <input type="hidden" name="curMonth">
    <input type="hidden" name="curGroup">

</form>