<?
/**
 * @var string $liqpay_form
 */
?>

<div class="row line-profile transparent">
    <div class="col-md-12">
        <div class="info-profile">
            <p class="balance-p"><b>На вашому рахунку <span><?= Controller_Frontend_HMVC_Profile_Tab_Wallet::$current_user->wallet ?></span> грн</b></p>
        </div>
        <div class="info-profile">
            <div class="row balance">
                <div class="col-xs-12 col-sm-3">
                    <input class="form-control" name="purchase_amount" type="number" min="10" value="10">
                </div>
                <div class="col-xs-12 col-sm-2">
                    <p>грн</p>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a type="button" id="walletPurchaseMoneyBtn" class="btn btn-primary">Поповнити</a>
                    <div id="walletPurchaseHiddenForm" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <div class="info-profile">
            <p class="balance-p bottom"><b>Мінімальна сумма <span>10</span> грн</b></p>
        </div>
    </div>
</div>