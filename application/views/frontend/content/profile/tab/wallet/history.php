<div class="row">
    <div class="col-md-12">
        <section class="table-section-holder profile table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th><? __dbt('Дата') ?></th>
                    <th><? __dbt('Тип') ?></th>
                    <th><? __dbt('Опис') ?></th>
                    <th><? __dbt('Зміна балансу') ?></th>
                </tr>
                </thead>
                <tbody id="table-wallet-history">
                </tbody>
            </table>
        </section>
    </div>
</div>

<script id="template_walletMyHistory" type="text/x-handlebars-template">
    {{#each items}}
    <tr class="transport-ticket-row {{classStatus}}" data-status="{{status}}" data-id="{{ID}}">
        <td>
            {{created}}
        </td>
        <td>
            {{typeLocale}}
        </td>
        <td>
            {{descriptionLocale}}
        </td>
        <td>
            {{amount}} UAH
        </td>
    </tr>
    {{else}}
    <tr>
        <td colspan="4"><? __dbt('Історії не знайдено') ?></td>
    </tr>
    {{/each}}
</script>