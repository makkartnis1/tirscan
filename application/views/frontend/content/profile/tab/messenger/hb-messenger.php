<script type="text/x-handlebars-template" id="hb-messenger">
    <div class="row line-profile transparent messages">
        <div class="col-md-12">
            <div style="margin-bottom: 5px;" class="row">
                <div class="col-md-12">
                    <button onclick="get_dialog('{{dialogID}}', '{{toUserID}}', '{{themeDialog}}');" class="btn btn-default pull-right"><i class="fa fa-refresh" aria-hidden="true"></i> Оновити</button>
                    <button onclick="dialogList()" class="btn btn-default pull-right" style="margin-right: 10px;"><i class="fa fa-reply" aria-hidden="true"></i> Назад</button>
                    <p id="theme-dialog" style="float: left;">{{themeDialog}}</p>
                </div>
            </div>
            <div class="info-profile">
                <div class="title">
                    <div class="row inner-tab">
                        <div class="message-box">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2">
                                    <div class="profile-f-img message dialog active">
                                        <img src="{{avatar}}">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-10">
                                    <div class="m-line">
                                        <textarea class="form-control" rows="3" id="message-text" placeholder="Ваше повідомлення"></textarea>
                                    </div>
<!--                                    <div class="m-line">-->
<!--                                        <button type="button" class="btn btn-primary btn-sm load"><i class="fa fa-download"></i>Завантажити</button>-->
<!--                                        <span class="file-c">Файл не вибрано</span>-->
<!--                                    </div>-->
                                    <div class="m-line">
                                        <div class="row">
                                            <div class="col-sm-8">
<!--                                                <p>-->
<!--                                                    Ви можете прикріпити файл до повідомлення обсягом не більше 4 Мб. Файл може мати формати: gif, jpeg, png, swf, zip, rar, doc, docx, psd, pdf, xls, xlsx, rtr, txt, ogg-->
<!--                                                </p>-->
                                            </div>
                                            <div class="col-sm-4" style="margin-bottom: 20px;">
                                                <button type="button" onclick="send_message()" class="btn btn-primary pull-right send">Надіслати</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <input type="hidden" id="dialog-id" value="{{dialogID}}">
        <input type="hidden" id="to-user-id" value="{{toUserID}}">
        {{#each dialog}}
        <div class="info-profile">
            <div class="title">
                <div class="row inner-tab">
                    <div class="message-profile dialog">
                        <div class="col-xs-1 col-ms-1 col-sm-1 {{#if_eq userMessageID ../userID}}my-message{{/if_eq}}">
                            <div class="profile-f">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="profile-f-img message active">
                                            <img src="{{ava}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-ms-11 col-sm-11">
                            <div class="profile-f-p {{#if_eq userMessageID ../userID}}dialog r{{else}}dialog l{{/if_eq}}" {{#if_eq status "unread"}}id="unread-message"{{/if_eq}}>
                            <span class="profile-f-top">
                                <p><a href="#">{{name}}</a></p>
                            </span>
                            <span class="profile-f-bottom message">
                                <p>{{created}}</p>
                            </span>
                            <span class="profile-f-center message">
                                <p>{{message}}</p>
                            </span>
                                <a href="#" class="profile-f-del" id="del-message-button" onclick="del_message('{{ID}}')"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{/each}}
    </div>
</script>