<div class="myStatistics">
    <div class="tab-pane" id="messages">
        <div>
            <ul class="nav nav-tabs profile responsive" role="tablist">
                <li role="presentation" class="active"><a href="#dialog-list" aria-controls="dialog-list" role="tab" data-toggle="tab"><? __dbt("Список діалогів") ?></a></li>
    <!--            <li role="presentation"><a href="#dialogs" aria-controls="dialogs" role="tab" data-toggle="tab">Діалоги</a></li>-->
            </ul>
            <div class="tab-content responsive">
                <div role="tabpanel" class="tab-pane active" id="dialogs">
                    <div class="row" data-inside="dialog"></div>
                </div>
            </div>
        </div>
    </div>
</div>