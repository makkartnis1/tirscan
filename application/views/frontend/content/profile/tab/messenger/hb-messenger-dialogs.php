<script type="text/x-handlebars-template" id="hb-messenger-dialogs">
    <div class="col-md-12">
        {{#if_eq dialogs ''}}<? __dbt("Діалогів ще не створено") ?>{{else}}
        {{#each dialogs}}
            <div class="info-profile" id="dialog-{{id}}">
                <div class="title">
                    <div class="row inner-tab">
                        <div class="message-profile {{#if_eq status 'unread'}}unread-dialog{{/if_eq}}">
                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-4" onclick="get_dialog('{{id}}', '{{#if_eq fromUserID ../userID}}{{toUserID}}{{else}}{{fromUserID}}{{/if_eq}}', '{{theme}}')">
                                <div class="profile-f">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <div class="profile-f-img message">
                                                <img src="{{ava}}">
                                            </div>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="profile-f-p">
                                                <span class="profile-f-top"><p>
                                                    <a href="#">
                                                        {{#if_eq fromUserID ../userID}}
                                                            {{name_to}}
                                                        {{else}}
                                                            {{name_from}}
                                                        {{/if_eq}}
                                                    </a></p>
                                                </span>
                                                <span class="profile-f-bottom message"><p>{{created}}</p></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-sm-5 col-md-7">
                                <div class="profile-f-p">
                                    <span class="profile-f-center message">
                                        <p><? __dbt("Тема:") ?></p>
                                        <p>{{theme}}</p>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-sm-1 col-md-1">
                                <a href="#" id="del-dialog-button" class="profile-f-del" onclick="del_dialog('{{id}}')"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{/each}}
        {{/if_eq}}
    </div>
</script>