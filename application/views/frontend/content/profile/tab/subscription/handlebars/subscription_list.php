<script id="profile-company-list" type="text/x-handlebars-template">
    <div>
        <h4><?= __db('sub.company.list') ?></h4>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th><?= __db('sub.company.list.num') ?></th>
            <th><?= __db('sub.company.list.company') ?></th>
            <th><?= __db('sub.company.list.actions') ?></th>
        </tr>
        </thead>
        <tbody>

        {{#each company_list}}

        <tr>
            <th>{{id}}</th>
            <td>{{name}}</td>
            <td>
                <button class="del-sub-list btn btn-default" data-company="{{companyID}}"><i class="fa fa-times" aria-hidden="true" style="color: red"></i> <?= __db('sub.company.list.del') ?></button>
            </td>
        </tr>

        {{/each}}

        </tbody>
    </table>

</script>