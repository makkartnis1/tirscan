<ul class="nav nav-tabs profile responsive" role="tablist">
    <li role="presentation" class="active"><a href="#info-form" aria-controls="info-form" role="tab" data-toggle="tab">Інформаційний блок</a></li>
    <li role="presentation"><a href="#archive" aria-controls="archive" role="tab" data-toggle="tab"><? __dbt("Архів новин") ?></a></li>
    <li role="presentation"><a href="#add-news-form" aria-controls="add-news-form" role="tab" data-toggle="tab"><? __dbt("Додати новину") ?></a></li>
</ul>
<div>
    <div class="tab-content responsive">
        <div role="tabpanel" class="tab-pane" id="archive">
            <div class="row line-profile transparent news-a">
                <div class="col-md-4">
                    <div class="info-profile padding-t">
                        <p><b><? __dbt("Архів новин") ?></b></p>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
                <div class="col-md-3">
                    <a type="button" class="btn btn-primary transparent"><? __dbt("Сортувати") ?></a>
                </div>
                <div class="col-md-2">
                    <div class="dropdown">
                        <button class="dropdown-toggle filter" type="button" id="dropdownMenu5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = "По даті
                            ";     $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/ ?><i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">
                            <li><a href="#">- не вказано -</a></li>
                            <li><a href="#">- не вказано -</a></li>
                            <li><a href="#">- не вказано -</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-box">
                <div class="newsItem row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="newsItemDate">
                            <p class="dateNumber">15</p>
                            <p class="dateMonth">січ</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <a href="#"><h3><? __dbt("Нові автомобілі у флоті - бачити наші вантажівки Scania нові") ?></h3></a>
                        <p class="timeYear">09:00, 15.01.2016</p>
                        <p class="anons">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam culpa dignissimos eveniet, magnam minus natus necessitatibus non quibusdam quis saepe, tempora totam vel voluptatum? Adipisci cumque hic nemo provident! Quis.
                        </p>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary pull-right"><? __dbt("Детальніше...") ?></button>
                        </div>

                    </div>
                    <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div class="newsItem row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="newsItemDate">
                            <p class="dateNumber">13</p>
                            <p class="dateMonth">січ</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <a href="#"><h3><? __dbt("Нові автомобілі у флоті - бачити наші вантажівки Scania нові") ?></h3></a>
                        <p class="timeYear">09:00, 13.01.2016</p>
                        <p class="anons">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam culpa dignissimos eveniet, magnam minus natus necessitatibus non quibusdam quis saepe, tempora totam vel voluptatum? Adipisci cumque hic nemo provident! Quis.
                        </p>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary pull-right"><? __dbt("Детальніше...") ?></button>
                        </div>

                    </div>
                    <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div class="newsItem row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="newsItemDate">
                            <p class="dateNumber">13</p>
                            <p class="dateMonth">січ</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <a href="#"><h3><? __dbt("Нові автомобілі у флоті - бачити наші вантажівки Scania нові") ?></h3></a>
                        <p class="timeYear">09:00, 13.01.2016</p>
                        <p class="anons">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam culpa dignissimos eveniet, magnam minus natus necessitatibus non quibusdam quis saepe, tempora totam vel voluptatum? Adipisci cumque hic nemo provident! Quis.
                        </p>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary pull-right"><? __dbt("Детальніше...") ?></button>
                        </div>

                    </div>
                    <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div class="newsItem row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="newsItemDate">
                            <p class="dateNumber">11</p>
                            <p class="dateMonth">січ</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <a href="#"><h3><? __dbt("Нові автомобілі у флоті - бачити наші вантажівки Scania нові") ?></h3></a>
                        <p class="timeYear">09:00, 11.01.2016</p>
                        <p class="anons">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam culpa dignissimos eveniet, magnam minus natus necessitatibus non quibusdam quis saepe, tempora totam vel voluptatum? Adipisci cumque hic nemo provident! Quis.
                        </p>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary pull-right"><? __dbt("Детальніше...") ?></button>
                        </div>

                    </div>
                    <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div class="newsItem row">
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="newsItemDate">
                            <p class="dateNumber">11</p>
                            <p class="dateMonth">січ</p>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10">
                        <a href="#"><h3><? __dbt("Нові автомобілі у флоті - бачити наші вантажівки Scania нові") ?></h3></a>
                        <p class="timeYear">09:00, 11.01.2016</p>
                        <p class="anons">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam culpa dignissimos eveniet, magnam minus natus necessitatibus non quibusdam quis saepe, tempora totam vel voluptatum? Adipisci cumque hic nemo provident! Quis.
                        </p>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-primary pull-right"><? __dbt("Детальніше...") ?></button>
                        </div>

                    </div>
                    <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div class="info-profile center">
                    <ul class="pagination center">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane active" id="info-form">
            <div class="tab-box">
                <form method="post" action="#infoblock">
                    <div class="row line-profile">
                        <div class="col-md-4">
                            <div class="title-line"><? __dbt("Старий пароль ") ?><span class="hidden">*</span></div>
                            <input type="password" name="info_old_password" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <div class="title-line"><? __dbt("Новий пароль ") ?><span class="hidden">*</span></div>
                            <input type="password" name="info_new_password" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <div class="title-line"><? __dbt("Підтвердження ") ?><span class="hidden">*</span></div>
                            <input type="password" name="info_new_password2" class="form-control">
                        </div>
                    </div>
                    <div class="row line-profile">
                        <div class="col-md-12">
                            <div class="title-line">E-mail <span class="hidden">*</span></div>
                            <input type="text" class="form-control" placeholder="" value="blonda@gmail.com" disabled="">
                        </div>
                    </div>
                    <div class="row line-profile">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 pull-right">
                                    <button type="submit" name="action_info_change_password" class="btn btn-primary"><b><? __dbt("ЗБЕРЕГТИ ЗМІНИ") ?></b></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="post" action="#infoblock">
                    <div class="row line-profile transparent">
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("ПІБ відповідальної особи") ?></div>
                            <input type="text" class="form-control" placeholder="" name="info_user_name" value="mfafahlfh">

                        </div>
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("Юридична адреса ") ?><span>*</span></div>
                            <input type="text" class="form-control" name="info_company_address" placeholder="" value="hghghgghg">
                        </div>
                    </div>
                    <div class="row line-profile transparent">
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("Тип користувача на порталі") ?></div>
                            <select class="form-control" disabled="">
                                <option value="1" selected=""><? __dbt("Експедитор") ?></option>
                                <option value="2"><? __dbt("Транспорт") ?></option>
                                <option value="3"><? __dbt("Вантажотримач") ?></option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("Реальна адреса ") ?><span>*</span></div>
                            <input type="text" class="form-control" placeholder="якщо відмінна від юридичної">
                        </div>
                    </div>
                    <div class="row line-profile transparent">
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("Компанія ") ?><span>*</span></div>
                            <input type="text" class="form-control" placeholder="" name="info_company_name" value="ggggggg">

                        </div>
                        <div class="col-md-6">
                            <div class="title-line"><? __dbt("Країна реєстрації ") ?><span>*</span></div>
                            <input type="text" class="form-control" placeholder="" value="Україна">
                        </div>
                    </div>
                    <div class="row line-profile transparent">
                        <div class="col-md-2">
                            <div class="title-line"><? __dbt("Телефон ") ?><span>*</span></div>
                            <select class="form-control" name="company_phone_code">
                                <option value="1"> </option>
                                <option value="15">+00</option>
                                <option value="14" selected="">+359</option>
                                <option value="16">+372</option>
                                <option value="3">+38</option>
                                <option value="4">+380</option>
                                <option value="7">+48</option>
                                <option value="2">+7</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="title-line"><span></span></div>
                            <input type="text" class="form-control" placeholder="" name="info_user_phone" value="">
                        </div>
                        <div class="col-md-6">
                            <div class="title-line">ICQ</div>
                            <input type="text" class="form-control" placeholder="" name="info_user_icq" value="">
                        </div>
                    </div>
                    <div class="row line-profile transparent">
                        <div class="col-md-2">
                            <div class="title-line"><? __dbt("Стаціон. ") ?><span>*</span></div>
                            <select class="form-control" name="company_phone_code">
                                <option value="1" selected=""> </option>
                                <option value="15">+00</option>
                                <option value="14">+359</option>
                                <option value="16">+372</option>
                                <option value="3">+38</option>
                                <option value="4">+380</option>
                                <option value="7">+48</option>
                                <option value="2">+7</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="title-line"><span></span></div>
                            <input type="text" class="form-control" placeholder="" name="info_user_phonestationary" value="">
                        </div>
                        <div class="col-md-6">
                            <div class="title-line">Skype</div>
                            <input type="text" class="form-control" placeholder="" name="info_user_skype" value="">
                        </div>
                    </div>
                    <div class="row line-profile transparent">
                        <div class="col-md-12">
                            <div class="title-line">ІПН-фірми</div>
                            <input type="text" class="form-control" placeholder="" name="info_company_ipn" value="">
                        </div>
                    </div>
                    <!--<div class="row line-profile transparent">-->
                    <!--    <div class="col-md-12">-->
                    <!--        <div class="title-line">Свідоцтво про реєстрацію, як суб’єкта підприємницької діяльності<span class="load"><i class="fa fa-paperclip"></i> <a href="#">Прикріпити файл</a></span></div>-->
                    <!--        <div class="docs-p">-->
                    <!--            <div class="row">-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div class="row line-profile transparent">-->
                    <!--    <div class="col-md-12">-->
                    <!--        <div class="title-line">Податкові документи <span class="load"><i class="fa fa-paperclip"></i> <a href="#">Прикріпити файл</a></span></div>-->
                    <!--        <div class="docs-p">-->
                    <!--            <div class="row">-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div class="row line-profile">-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="title-line">Тип місця</div>-->
                    <!--        <div class="dropdown">-->
                    <!--            <button class="dropdown-toggle filter" type="button" id="dropdownMenu5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">-->
                    <!--                - не вказано --->
                    <!--                <i class="fa fa-angle-down"></i>-->
                    <!--            </button>-->
                    <!--            <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">-->
                    <!--                <li><a href="#">- не вказано -</a></li>-->
                    <!--                <li><a href="#">- не вказано -</a></li>-->
                    <!--                <li><a href="#">- не вказано -</a></li>-->
                    <!--            </ul>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="title-line">-->
                    <!--            Адреса-->
                    <!--                                                            <span class="label--checkbox line">-->
                    <!--                                                             <input type="checkbox" class="checkbox" checked>-->
                    <!--                                                            <span class="gr line">Координати на карті </span>-->
                    <!--                                                            </span>-->
                    <!--        </div>-->
                    <!--        <input type="text" class="form-control" placeholder="">-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div class="row line-profile">-->
                    <!--    <div class="col-md-12">-->
                    <!--        <div class="title-line">Сертифікати професійної діяльності<span class="load"><i class="fa fa-paperclip"></i> <a href="#">Прикріпити файл</a></span></div>-->
                    <!--        <div class="docs-p">-->
                    <!--            <div class="row">-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                    <div class="doc-inner">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-xs-4">-->
                    <!--                                <div class="img-doc"></div>-->
                    <!--                            </div>-->
                    <!--                            <div class="col-xs-8">-->
                    <!--                                                                                <span>Свідоцтво<br>-->
                    <!--про держ. реєстрацію</span>-->
                    <!--                                <span><a href="#"><i class="fa fa-times"></i>Видалити </a></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                </div>-->
                    <!--                <div class="col-md-3">-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="row line-profile">
                        <div class="col-md-12">
                            <div class="title-line"><? __dbt("Додаткова інформація") ?></div>
        <textarea class="form-control" name="info_company_info" placeholder="" rows="3">Разнообразный и богатый опыт новая модель организационной деятельности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития. Разнообразный и богатый опыт реализация намеченных плановых заданий позволяет выполнять важные задания по разработке новых предложений. Значимость этих проблем настолько очевидна, что новая модель организационной деятельности требуют определения и уточнения системы обучения кадров, соответствует насущным потребностям.

Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности требуют определения и уточнения дальнейших направлений развития. Таким образом дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки соответствующий условий активизации. Идейные соображения высшего порядка, а также сложившаяся структура организации позволяет оценить значение форм развития. Равным образом рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития. Равным образом дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке новых предложений.</textarea>
                        </div>
                    </div>
                    <!--<div class="row line-profile">-->
                    <!--    <div class="col-md-12">-->
                    <!--        <div class="title-line">Завантажити логотип компанії-->
                    <!--            <span class="load"><i class="fa fa-paperclip"></i> <a href="#">Прикріпити файл</a></span>-->
                    <!--            <span class="break">/</span>-->
                    <!--            <a href="#" class="delete red"><i class="fa fa-times"></i>Видалити</a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="container-fluid">-->
                    <!--        <div class="row">-->
                    <!--            <div class="col-xs-12 col-md-6">-->
                    <!--                <div class="load-box-company">-->
                    <!--                    <a href="#"></a>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="row line-profile">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-4 pull-right">
                                    <button type="submit" name="action_info_update_info" class="btn btn-primary"><b>ЗБЕРЕГТИ ЗМІНИ</b></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="add-news-form">
            <div class="row line-profile transparent">
                <div class="col-md-12">
                    <div class="title-line"><b>Додати новину</b></div>
                </div>
            </div>
            <div class="row line-profile transparent">
                <div class="col-md-12">
                    <div class="info-profile add-news">
                        <p><b>Заголовок новини</b></p>
                        <p>
                            <input class="form-control" rows="1">
                        </p>

                    </div>
                    <div class="info-profile padding-text add-news">
                        <p><a href="#">Завантажити</a> головне зображення. <span>Розмір зображення не менше 30 Кб і не більше 10 Мб</span></p>
                    </div>
                    <div class="info-profile add-news">
                        <p><b>Текст анонсу</b></p>
                        <p>
                            <textarea class="form-control" rows="5"></textarea>
                        </p>
                    </div>
                    <div class="info-profile add-news">
                        <p><b>Зміст новини</b></p>
                        <form method="post">
                            <textarea id="mytextarea"></textarea>
                        </form>
                    </div>
                    <div class="info-profile add-news">
                        <a type="button" class="btn btn-primary public">Опублікувати</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>