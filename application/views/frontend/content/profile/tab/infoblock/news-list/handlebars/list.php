<script id="info-block-list-news" type="text/x-handlebars-template">

    <p><b><?= __db('archive.news')?></b></p>
    <div class="tab-box">

        {{#each list_news}}
        <div class="newsItem row">
<!--            <div class="col-lg-2 col-md-2 col-sm-2">-->
<!--                <div class="newsItemDate">-->
<!--                    <p class="dateNumber">15</p>-->
<!--                    <p class="dateMonth">січ</p>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-lg-12 col-md-12 col-sm-12">
                <a href="#"><h3>{{title}}</h3></a>
                <p class="timeYear">{{createDate}}</p>
                <p class="anons">{{preview}}</p>
                <div class="clearfix">
                    <button type="submit" data-id="{{ID}}" class="btn btn-primary pull-right edit-news"><? __dbt("Редагувати") ?></button>
                </div>
            </div>
            <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
        </div>
        {{/each}}

    </div>

</script>