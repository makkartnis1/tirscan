<script id="info-block-one-news" type="text/x-handlebars-template">
    <form id="update_news" method="post">
    <div class="col-lg-12 newsItemText">
        <div class="form-group">
            <label for="news-create-title"><?= __db('hint.profile.news.title')?> <span>*</span> </label>
            <div class="input-group">
                <input type="text" name="title" value="{{title}}" class="form-control" placeholder="placeholder.profile.news.title">
                <input type="hidden" name="id" value="{{id}}">
            </div>
        </div>

        <div class="form-group ">
            <label for="news-create-preview"><?= __db('hint.profile.news.preview')?> <span>*</span> </label>
            <div class="input-group">
                <textarea class="form-control" placeholder="<?= __db('placeholder.profile.news.preview')?>" name="preview">{{preview}}</textarea>
            </div>
        </div>

        <div class="form-group ">
            <label>content</label>
            <div class="input-group">
                <textarea class="form-control" name="text" id="editor1">{{{content}}}</textarea>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <button id="save" class="btn btn-primary pull-right"><?= __db('btn.save')?></button>
        </div>
    </div>
    </form>
</script>