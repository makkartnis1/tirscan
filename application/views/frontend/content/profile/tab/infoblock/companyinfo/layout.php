<?

/**
 * форма редагування профілю компанії
 */

?>
<div class="row line-profile transparent">
    <form method="post">

        <div class="col-md-12">
            <?= Frontend_LocaleTab::getPills(); ?>
        </div>

        <? foreach ($groups as $class => $group_block){ ?>

            <? foreach ($group_block as $element){ ?>

                <div class="<?= $class ?>">

                    <?= $element ?>

                </div>

            <? } ?>

        <? } ?>

        <div class="col-md-2 col-md-offset-10">

            <?= $button ?>

        </div>

    </form>
</div>