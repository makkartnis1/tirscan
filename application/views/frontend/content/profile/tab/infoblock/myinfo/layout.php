<div class="row profile-content">
    <form method="post">
        <div class="col-md-12">
            <?= Frontend_LocaleTab::getPills() ?>
        </div>

        <? foreach ($groups as $group){ ?>
            <div class="col-md-6">
                <?= $group ?>
            </div>
        <? } ?>

        <div class="col-md-2 col-md-offset-10">
            <?= $button ?>
        </div>
    </form>
</div>