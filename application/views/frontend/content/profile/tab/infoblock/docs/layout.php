
<?= $heading ?>
<div class="row">

    <div class="col-xs-12">
        <div class="alert alert-warning">
            <?= __db('company.docs.warning.about.uploading') ?>
        </div>
    </div>


    <div class="col-xs-12">
        <?= $doc_reg ?>
    </div>

    <div class="col-xs-12">
        <?= $doc_tax ?>
    </div>

    <div class="col-xs-12">
        <?= $doc_lic ?>
    </div>

    <div class="col-xs-12">
        <?= $doc_cert ?>
    </div>

</div>
