<?

/**
 * Форма добалення новини
 */

?>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>


<script type="text/javascript">

    window.CKEDITOR.editorConfig = function( config ) {
        <? if(isset($urlUploader)) { ?>
            config.filebrowserUploadUrl = '<?= $urlUploader ?>';
        <? } ?>
        config.configlanguage = '<?= (isset($ckeLang)) ? $ckeLang : 'ru'; ?>';
        config.toolbarCanCollapse = true;
    };

    function initCKE(){
        $( 'textarea.cke-editor:not([style*="hidden"])').each( function() {
            CKEDITOR.replace(this);
        });
    }

    $(function(){
        initCKE();
    });

</script>

<div class="tab-box">
    <form class="row" method="post" enctype="multipart/form-data">

        <div class="col-md-12">
            <?= Frontend_LocaleTab::getPills() ?>
        </div>

        <? foreach ($groups as $group){ ?>

        <div class="col-md-12">

            <?= $group ?>

        </div>

        <? } ?>

        <div style="display: none;">
            <?= $changeImg_widget ?>
        </div>

        <input type="hidden" id="company_id" value="<?= $company_id ?>">

<!--        <div class="col-md-12">-->
<!--            <div class="form-group">-->
<!--                <label>--><?//= __db('seo.url')?><!--</label>-->
<!--                <div class="input-group">-->
<!--                    <input class="form-control" type="text" id="url" name="url">-->

<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

    <!--    <div class="col-md-12">-->
    <!--        <div class="form-group">-->
    <!--            <label>Сповістити підписників</label>-->
    <!--            <input type="checkbox" id="subscribe" style="margin: 0;">-->
    <!--        </div>-->
    <!--    </div>-->

        <div class="col-md-12">
            <div class="row">

                <div class="col-md-4">

                    <?= $button_save ?>

                </div>

                <div class="col-md-4">

                    <?= $button_reset ?>

                </div>

            </div>
        </div>

    </form>
</div>