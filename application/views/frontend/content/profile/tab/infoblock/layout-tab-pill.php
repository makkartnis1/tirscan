<li role="presentation" class="<? if($active) { ?>active<? } ?>">
    <a href="#<?= $id ?>" aria-controls="info-block" role="tab" data-toggle="tab">
        <?= $caption ?>
    </a>
</li>