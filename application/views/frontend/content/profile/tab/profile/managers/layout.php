<div>

    <div class="info-profile reviews">
        <div class="row"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-default" id="generate-link"><?= __db('generate.link') ?></button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p id="link"></p>
        </div>
    </div>

</div>
<?= $handlebars_view; ?>