<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 24.05.16
 * Time: 14:49
 */

?>

<div>

    <div class="row">
        <div class="col-md-4">
            <div class="sorting-b pull-left">
                <p><b><?= __db('profile.partners.list') ?></b></p>
            </div>
        </div>

        <div class="col-md-2 col-md-offset-1">
            <span class="sorting-b"><?= __db('frm.sort') ?> :</span>
        </div>
        <div class="col-md-3">

            <select class="form-control" name="sort">
                <option value="partners"><?=                __db('partners.show.partners');                     ?></option>
                <option value="i-must-confirm"><?=          __db('partners.sort.i.must.confirm');               ?></option>
                <option value="i-must-wait"><?=             __db('partners.sort.i.must.wait.confirmation');     ?></option>
            </select>

        </div>
        <div class="col-md-1">
            <button type="button" name="refresh" class="btn btn-default pull-right"><i class="fa fa-refresh"></i></button>
        </div>

        <div class="col-md-1">
            <button type="button" name="partner-request" class="btn btn-warning pull-right" value="">
                <i class="fa fa-reply"></i>
                <span>1</span>
            </button>
        </div>
    </div>


    <div class="info-profile reviews">

        <div class="oneCompanyMainFigureContent">
            <div class="row">

            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="pagination center"></ul>
        </div>
    </div>
</div>
<?= $handlebars_view ?>

