<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 27.05.16
 * Time: 12:10
 */
?>
<script id="profile-partners-list-blocks" type="text/x-handlebars-template">
    {{# each companies}}
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
        <div class="partner">

            <img src="{{avatar}}" alt="">

            <div class="manage-linker {{class}}">

                <div class="manage-panel-linker">

                    <div class="company-name-linker">
                        <a href="{{url}}" class="profile-link" target="_blank">
                            {{visibleName}}
                        </a>

                    </div>

                    {{#if_equal_soft type "partners"}}

                        <a href="{{url}}" class="toggle-button button-left" target="_blank">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="#" class="toggle-button button-right" data-switch="confirmation" data-confirmation="off" data-id="{{companyID}}">
                            <i class="fa fa-times"></i>
                        </a>

                    {{/if_equal_soft}}

                    {{#if_equal_soft type "i-must-confirm"}}

                        <a href="#" class="toggle-button button-left" data-switch="confirmation" data-confirmation="on" data-id="{{companyID}}">

                            <i class="fa fa-check"></i>
                        </a>

                        <a href="#" class="toggle-button button-right" data-switch="confirmation" data-confirmation="off" data-id="{{companyID}}">
                            <i class="fa fa-times"></i>
                        </a>

                    {{/if_equal_soft }}


                    {{#if_equal_soft type "i-must-wait"}}

                    <a href="{{url}}" class="toggle-button button-left" target="_blank" data-id="{{companyID}}">

                            <i class="fa fa-clock-o"></i>
                    </a>

                    <a href="#" class="toggle-button button-right" data-switch="confirmation" data-confirmation="off" data-id="{{companyID}}">
                            <i class="fa fa-times"></i>
                    </a>

                    {{/if_equal_soft }}

                </div>

            </div>

        </div>
    </div>
    {{/each}}
</script>

