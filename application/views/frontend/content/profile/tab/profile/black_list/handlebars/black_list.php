<script id="profile-partners-black-list" type="text/x-handlebars-template">

    <h4 style="margin-bottom: 10px;" ><?= __db('profile.black.list') ?></h4>

    <div>
        {{#if_eq black_list ''}} <? __dbt('Компанії у списку відсутні') ?>
        {{else}}
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><?= __db('profile.black.list.num') ?></th>
                <th><?= __db('profile.black.list.company') ?></th>
                <th><?= __db('profile.black.list.actions') ?></th>
            </tr>
            </thead>
            <tbody>

            {{#each black_list}}

            <tr>
                <th>{{id}}</th>
                <td>{{name}}</td>
                <td>
                    <button class="del-with-list btn btn-default" data-company="{{companyID}}"><i class="fa fa-times" aria-hidden="true" style="color: red"></i> <?= __db('profile.black.list.del') ?></button>
                </td>
            </tr>

            {{/each}}

            </tbody>
        </table>
        {{/if_eq}}
    </div>

</script>