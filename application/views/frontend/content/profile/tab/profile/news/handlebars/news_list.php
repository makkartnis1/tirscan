<script id="profile-partners-news-list" type="text/x-handlebars-template">
    <div class="col-md-12 line-profile transparent tr-margin-right reviews">
        <h4><?= __db('profile.partners.news.list') ?></h4>
    </div>

    {{#each news_list}}
<!--        <div class="col-lg-2 col-md-2 col-sm-2">-->
<!--            <div class="newsItemDate">-->
<!--                <p class="dateNumber">15</p>-->
<!--                <p class="dateMonth">січ</p>-->
<!--            </div>-->
<!--        </div>-->
        <div class="newsItem col-lg-12 col-md-12 col-sm-12">
            <a href="#"><h3>{{title}}</h3></a>
            <p class="timeYear">{{createDate}}</p>
            <p class="anons">{{preview}}</p>
            <div class="clearfix">
                <button id="{{ID}}" class="open-news btn btn-primary pull-right"><? __dbt('Детальніше...') ?></button>
            </div>
            <div class="bottomBorderHolder" style="margin-right: 0; margin-left: 0; width: 100%"></div>
        </div>
    {{/each}}

    <div class="col-md-12">
        <ul class="pagination center"></ul>
    </div>
</script>
