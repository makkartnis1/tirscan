<script id="profile-partners-one-news" type="text/x-handlebars-template">

    <div class="col-md-12" style="margin-bottom: 15px;">
        <button id="back-to-list" class="btn btn-default pull-right"><i class="fa fa-reply" aria-hidden="true"></i><? __dbt('Назад') ?></button>
    </div>
    <div class="col-lg-12 newsItemText">
        <h1>{{title}}</h1>
        <p class="date">{{createDate}}</p>
        <p class="text">{{{content}}}</p>
    </div>

</script>
