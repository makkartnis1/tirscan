<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 18.05.16
 * Time: 11:50
 */?>
<script id="hb-profile-feedback-review" type="text/x-handlebars-template">
    {{#each elements}}
    <div class="reviews-box">
            <div class="row inner-tab reviews rew">
                <div class="col-xs-12 col-ms-8 col-sm-6 col-md-5">
                    <div class="profile-f reviews">
                        <div class="row">
                            <div class="col-xs-12 col-ms-12 col-sm-12 col-md-3">
                                <div class="profile-f-img">
                                    <img src="{{avatar}}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-sm-12 col-md-9">
                                <div class="profile-f-p">
                                                                                    <span class="profile-f-top">
                                                                                        <p><a href="{{companyURL}}">{{companyName}}</a>/{{userName}}
                                                                                        </p>
<!--                                                                                        <div class="stars">-->
<!--                                                                                            <i class="fa fa-star active"></i>-->
<!--                                                                                            <i class="fa fa-star active"></i>-->
<!--                                                                                            <i class="fa fa-star active"></i>-->
<!--                                                                                            <i class="fa fa-star"></i>-->
<!--                                                                                            <i class="fa fa-star"></i>-->
<!--                                                                                        </div>-->
                                                                                    </span>
                                    <p></p>


                                                                                    <span class="profile-f-bottom">
                                                                                         <p>{{createDate}}</p>
                                                                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-ms-4 col-sm-6 col-md-7">
                    <p class="pull-right {{#if_equal_soft rating 'positive'}} green {{else}} red {{/if_equal_soft}} <?// red || green ?> reviews">
                        {{#if_equal_soft rating 'positive'}}
                        <i class="fa fa-thumbs-up"></i>
                        {{else}}
                        <i class="fa fa-thumbs-down"></i>
                        {{/if_equal_soft}}
                    </p>
                </div>
            </div>
            <div class="row inner-tab reviews">
                <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">
                    <p class="reviews-p">
                        {{text}}
                    </p>
                </div>
            </div>
        </div>
    {{/each}}
</script>
