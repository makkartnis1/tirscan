<div>
    <div class="row">
        <div class="col-md-4">
            <div class="sorting-b pull-left">
                <p><b><?= __db('profile.feedback.history') ?></b></p>
            </div>
        </div>

        <div class="col-md-2 col-md-offset-1">
            <span class="sorting-b"><?= __db('frm.sort') ?> :</span>
        </div>
        <div class="col-md-4">

            <select class="form-control" name="sort">
                <option value="all"><?=             __db('feedback.sort.all');          ?></option>
                <option value="positive"><?=        __db('feedback.sort.positive');     ?></option>
                <option value="negative"><?=        __db('feedback.sort.negative');     ?></option>
            </select>

        </div>
        <div class="col-md-1">
            <button type="button" name="refresh" class="btn btn-default pull-right"><i class="fa fa-refresh"></i></button>
        </div>
    </div>


    <div class="info-profile reviews">


    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="pagination center"></ul>
        </div>
    </div>
</div>
<?= $handlebars_view ?>