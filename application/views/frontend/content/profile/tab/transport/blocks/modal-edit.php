<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 13.05.16
 * Time: 14:50
 */?>
<div class="modal fade transportOrderModal" id="modal_edit_my_transport" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content transport">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title">
                    <i></i>
                    <span class="ttl">b</span></h4>
            </div>
            <div class="row">
                <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">
                    <div class="transport-box"></div>
                </div>
            </div>
            <div class="modal-body">

                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">

                        <? $active = true; foreach ($modal_tab as $id => $data){ list($name, $content) = $data?>

                            <li role="presentation" <? if($active === true) { ?>class="active"<? $active = false;} ?>>
                                <a href="#<?= $id ?>" aria-controls="<?= $id ?>" role="tab" data-toggle="tab"><?= __db($name) ?></a>
                            </li>

                        <? } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <? $active = true; foreach ($modal_tab as $id => $data){ list($name, $content) = $data?>

                            <div role="tabpanel" class="tab-pane fade in <? if($active === true) { ?> active <? $active = false;} ?>" id="<?= $id ?>">
                                <?= $content ?>
                            </div>

                        <? } ?>

                    </div>

                </div>
                
            </div>
        </div>
    </div>
</div>
