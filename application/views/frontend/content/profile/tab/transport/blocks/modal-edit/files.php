<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 16.05.16
 * Time: 15:40
 */?>
<div class="row">

    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">
        <h3 class="col-xs-12"><?= __db('file.car.docs') ?></h3>
        <div class="row"
             data-caption-atatch="<?=       htmlspecialchars(__db('file.block.attach.car.docs')) ?>"
             data-caption-delete="<?=       htmlspecialchars(__db('file.block.delete.car.docs')) ?>"
             data-caption-download="<?=     htmlspecialchars(__db('file.block.download.car.docs')) ?>"
             data-title="<?=                htmlspecialchars(__db('file.block.title.car.docs')) ?>"
             data-block="car-documents">
        </div>
    </div>

    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">
        <h3 class="col-xs-12"><?= __db('file.car.number.photo') ?></h3>
        <div class="row"
             data-caption-atatch="<?=       htmlspecialchars(__db('file.block.attach.car.number')) ?>"
             data-caption-delete="<?=       htmlspecialchars(__db('file.block.delete.car.number')) ?>"
             data-caption-download="<?=     htmlspecialchars(__db('file.block.download.car.number')) ?>"
             data-title="<?=                htmlspecialchars(__db('file.block.title.car.number')) ?>"
             data-block="car-photo"></div>

    </div>

</div>
