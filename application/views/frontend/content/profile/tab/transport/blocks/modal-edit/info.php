<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 16.05.16
 * Time: 15:41
 */?>
<form class="row" method="post">
    <div class="col-md-12">

        <div class="whiteFluidContainer">
            <div class="container-fluid ">
                <div>
                    <div class="row">

                        <div class="formHolder" id="formSelectedCar" style="display: none;"><? __dbt('Обрана машина: ') ?><span id="textSelectedCar"></span> <a href="javascript:;" id="linkRemoveSelectedCar">[X]</a>
                        </div>
                        <div class="formHolder" id="formAddCar">
                            <div class="form-group truckType col-lg-6 col-md-6">
                                <h2><?= __db('profile.my-transport.modal-edit.block.car-info') ?>:</h2>
                                <label for="carType"><?= __db('profile.my-transport.modal-edit.block.transport-type') ?></label>
                                <select name="car_type" class="form-control" type="text" id="carType">
                                    <? foreach ($carTypes as $db_row) { ?>
                                        <option value="<?= $db_row['id'] ?>" data-translate="<?= htmlspecialchars($db_row['localeName']) ?>"><?= __db($db_row['localeName']); ?></option>
                                    <? } ?>
                                </select>
                                <div class="radioTruck">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="car_body_type" value="vantazhivka">
                                            <span><?= __db('car.body.vantazhivka') ?></span><span class="carIconSm"></span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="car_body_type" value="napiv prychip">
                                            <span><?= __db('car.body.napiv prychip') ?></span><span class="carIconMd"></span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="car_body_type" value="z prychipom">
                                            <span><?= __db('car.body.z prychipom') ?></span><span class="carIconLg"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group truckType col-lg-6 col-md-6">
                                <h2><?= __db('profile.my-transport.modal-edit.block.car-technical-info') ?></h2>
                                <div class="row truckWeight">
                                    <label for="maxWeight" class="col-lg-5"><?= __db('profile.my-transport.modal-edit.lifting-possibilities') ?></label>
                                    <div class="col-lg-7">
                                        <div class="form-group ">
                                            <div class="input-group">

                                                <input type="text" name="liftingCapacity" value="" class="form-control" id="maxWeight">
                                                <div class="input-group-addon"><?= __db('units.tone') ?></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row truckContent">
                                    <label for="maxContent" class="col-lg-5"><?= __db('profile.my-transport.modal-edit.block.volume-sizes') ?></label>
                                    <div class="col-lg-7">
                                        <div class="form-group ">
                                            <div class="input-group">

                                                    <input type="text" name="volume" value="" class="form-control" id="maxContent">
                                                    <div class="input-group-addon"><?= __db('units.meter') ?><sup>3</sup></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="truckMeasures">
                                    <label><?= __db('profile.my-transport.modal-edit.block.XYZ-sizes') ?></label>
                                    <div class="measuresInput">
                                        <div class="form-group ">
                                            <div class="input-group">

                                                    <input type="text" name="sizeZ" value="" class="form-control" id="maxLength" placeholder="<?= __db('unit-label.length') ?>">
                                                    <div class="input-group-addon"><?= __db('units.meter') ?></div>

                                            </div>
                                        </div>
                                    </div><div class="measuresInput">
                                        <div class="form-group ">
                                            <div class="input-group">

                                                    <input type="text" name="sizeX" value="" class="form-control" id="maxWidth" placeholder="<?= __db('unit-label.width') ?>">
                                                    <div class="input-group-addon"><?= __db('units.meter') ?></div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="measuresInput">
                                        <div class="form-group ">
                                            <div class="input-group">

                                                <input type="text" name="sizeY" value="" class="form-control" id="maxHeight" placeholder="<?= __db('unit-label.height') ?>">
                                                <div class="input-group-addon"><?= __db('units.meter') ?></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?= __db('profile.my-transport.modal-edit.block.text-info') ?></h3>
                            <textarea name="info" class="form-control" placeholder="" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-default btn-primary pull-right" type="button" name="action" value="save"><?= __db('btn.save') ?></button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</form>