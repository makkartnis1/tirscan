<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 16.05.16
 * Time: 10:09
 */?>
<script id="hb-bs-pagination" type="text/x-handlebars-template">

    {{#pagination currentPage pageCount size}}

        {{#unless startFromFirstPage}}
        <li>
        <a href="#" data-page="1"><i class="fa fa-angle-left"></i></a>
        </li>
        {{/unless}}

        {{#each pages}}
        {{#if isCurrent}}
        <li class="active">
        <a href="#" data-page="{{page}}">{{page}}</a>
        </li>
        {{/if}}
        {{#unless isCurrent}}
        <li>
        <a href="#" data-page="{{page}}">{{page}}</a>
        </li>
        {{/unless}}
        {{/each}}

        {{#unless endAtLastPage}}
        <li>
        <a href="#" data-page="{{lastPage}}"><i class="fa fa-angle-right"></i></a>
        </li>
        {{/unless}}

    {{/pagination}}

</script>