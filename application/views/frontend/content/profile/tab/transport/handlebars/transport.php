<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 13.05.16
 * Time: 12:51
 */?>
<script id="hb-template-my-transport-items" type="text/x-handlebars-template">

    {{#if_eq cars ''}}<? __dbt('Ви ще не додали свій транспорт') ?>{{else}}
    {{#each cars}}

    <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">
        <div class="transport-box" data-id="{{ID}}" data-info="{{info}}">
            <div class="thumb transport">
                <div class="thumb-inner transport">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="center-inner img">
                                {{#car_avatar avatar}}
                                <img src="{{avatar}}" data-avatar>
                                {{else}}
                                <img src="{{no_avatar}}" data-no-avatar>
                                {{/car_avatar}}
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="center-inner-second">
                                <p class="yellow-inner"></p>
                                <p class="top-inner">{{title}}
                                    <span class="back-img{{cssIcon}}">
                                </p>
                                <span class="tr-bottom">
                                    <p class="middle-inner">
                                        {{sizeX}}
                                        х
                                        {{sizeY}}
                                        х
                                        {{sizeZ}}
                                    </p>
                                    <p class="bottom-inner">
                                        {{liftingCapacity}} т
                                        /
                                        {{volume}} м3
                                    </p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="editing-block">

                <a href="#" data-toggle="modal" data-target="#modal_edit_my_transport">
                    {{#if_equal_soft hasRequests "no"}}
                        <i class="fa fa-pencil"></i> {{else}} <i class="fa fa-eye"></i>
                    {{/if_equal_soft}}
                </a>

                <a href="#" data-action="delete">
                    <i class="fa fa-times fa-lg red"></i>
                </a>
            </span>
        </div>

    </div>

    {{/each}}
    {{/if_eq}}

</script>