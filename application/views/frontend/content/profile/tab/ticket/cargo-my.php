<?
/**
 * @var View $modals
 */
?>

<div class="heading">
    <div class="row">
        <div class="col-sm-5">
            <h2><? __dbt('Мої заявки по вантажу') ?> <a href="#">(<span class="cargo-ticket-count"></span>)</a></h2>
        </div>
        <div class="col-sm-7">
            <div class="text-right">
                <a class="btn btn-blue" href="<?= $controller->links['cargo'] ?>"><?= __dbt('Пошук вантажу') ?></a>
                <a class="btn btn-blue" href="<?= $controller->links['transport'] ?>"><?= __dbt('Пошук транспорту') ?></a>
                <div class="selectGroup">
                    <label for="status"><? __dbt('Статус') ?>:</label>
                    <select name="" id="status" class="form-control">
                        <option value=""><? __dbt('Показати усі') ?></option>
                    </select>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tableHolder">
    <table>
        <thead>
            <tr>
                <th><? __dbt('Дата') ?></th>
                <th><? __dbt('Звідки') ?></th>
                <th><? __dbt('Куди') ?></th>
                <th><? __dbt('Ціна') ?></th>
                <th><? __dbt('Статус') ?></th>
                <th><? __dbt('Виконавець') ?></th>
                <th><? __dbt('Дії') ?></th>
            </tr>
        </thead>
        <tbody class="table-cargo-my-requests">
        </tbody>
    </table>
</div>

<script id="template_cargoCompanyProps" type="text/x-handlebars-template">
    {{#each companies}}
    <figure class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="companyFigure">
            <div class="figureImgHolder">
                <a href="javascript:;">
                    <img src="{{image}}" alt="" />
                </a>
            </div>
            <div class="figureCaption">
                <a href="{{url}}"><h3>{{name}}</h3></a>
                <p class="companyTypeName">{{type}}</p>
                <div class="companyDescription">
                    <p>{{info}}</p>
                </div>
                <div class="companyContacts">
                    <p class="contactType">Телефон</p>
                    <p><i class="fa fa-mobile fa-2x"></i><span class="phoneNumber">{{phoneFull}}</span></p>
                    <p class="contactType">Розташування</p>
                    <p><span class="flag"></span><span class="adress">{{fullAddress}}</span></p>
                </div>
                <div class="orderInProcessTwoButtonsHolder">
                    <button class="btn btn-primary accept-cargo-company-prop" data-id="{{appID}}">Прийняти</button>
                    <button class="btn btn-primary reject-cargo-company-prop btnCancel" data-id="{{appID}}">Відхилити</button>
                </div>
            </div>
        </div>
    </figure>
    {{/each}}
</script>

<script id="template_cargoMyRequests" type="text/x-handlebars-template">
    {{#each requests}}
    <tr class="cargo-ticket-row {{classStatus}}" data-status="{{status}}" data-id="{{ID}}"
        data-click="showTransportModal"
        data-from-placeid="{{'from_placeID'}}"
        data-to-placeid="{{to_placeID}}"
        data-request-id="{{ID}}"
        data-request-user-id="{{userID}}"
    >
        <td>
            {{js_date}}
            {{{json_info}}}
        </td>
        <td>
            {{js_from}}
        </td>
        <td>
            {{js_to}}
        </td>
<!--        <td>-->
<!--            {{js_z}}-->
<!--        </td>-->
<!--        <td>-->
<!--            {{js_weight}}-->
<!--        </td>-->
<!--        <td>-->
<!--            {{js_type}}-->
<!--        </td>-->
        <td>
            {{js_price}}
        </td>
        <td>
            {{{js_status}}}
        </td>
        <td>
            {{#if_equal_soft js_action null}}
            -
            {{else}}
            <a href="{{js_action}}" class="go_to_company_page">на сторінку компанії</a>
            {{/if_equal_soft}}
        </td>
        <td class="icoList">
            {{#each js_actions}}
            {{{html}}}
            {{else}}
            -
            {{/each}}
        </td>
    </tr>
    {{else}}
    <tr>
        <td colspan="9"><? __dbt('Заявок не знайдено') ?></td>
    </tr>
    {{/each}}
</script>

<div class="modal fade orderInProcessModalWindow" id="orderCargoInProcessModalWindow" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl">Заявка №<span class="cargo-ticket-apps-number"></span></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h3>Пропозиції по заявці</h3>
                        <div class="companyThumbnailsHolder">
                            <div class="row" id="orderCargoCompaniesProps">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="CargoRatingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Закриття заявки №<span class="cargo-ticket-close-number"></span></h4>
            </div>
            <div class="modal-body rating-modal">
                <div class="radio">
                    <label>
                        <input type="radio" name="closeCargoTicketRating" id="optionsRadios1" value="positive" checked="">
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="closeCargoTicketRating" id="optionsRadios2" value="negative">
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                    </label>
                </div>
                <textarea class="form-control" name="closeCargoTicketText"></textarea>
                <div class="row">
                    <div class="col-xs-3 col-xs-offset-9">
                        <button type="button" id="submitCargoCloseTicket" class="btn btn-primary">Надіслати</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>