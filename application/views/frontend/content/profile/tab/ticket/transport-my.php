<?
/**
 * @var View $modals
 */
?>

<div class="table-transport-ticket-slide1">
    <div class="heading">
        <div class="row">
            <div class="col-sm-5">
                <h2><? __dbt('Мої заявки по транспорту') ?> <a href="#">(<span class="transport-ticket-count"></span>)</a></h2>
            </div>
            <div class="col-sm-7">
                <div class="text-right">
                    <a class="btn btn-blue" href="<?= $controller->links['cargo'] ?>"><?= __dbt('Пошук вантажу') ?></a>
                    <a class="btn btn-blue" href="<?= $controller->links['transport'] ?>"><?= __dbt('Пошук транспорту') ?></a>
                    <div class="selectGroup">
                        <label for="status"><? __dbt('Статус') ?>:</label>
                        <select name="" id="status" class="form-control">
                            <option value=""><? __dbt('Показати усі') ?></option>
                        </select>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tableHolder">
        <table>
            <thead>
                <tr>
                    <th><? __dbt('Дата') ?></th>
                    <th><? __dbt('Звідки') ?></th>
                    <th><? __dbt('Куди') ?></th>
                    <th><? __dbt('Ціна') ?></th>
                    <th><? __dbt('Статус') ?></th>
                    <th><? __dbt('Виконавець') ?></th>
                    <th><? __dbt('Дії') ?></th>
                </tr>
            </thead>
            <tbody class="table-transport-my-requests">
            </tbody>
        </table>
    </div>
</div>

<div class="table-transport-ticket-slide2" style="display: none;">
    <div class="heading">
        <div class="row">
            <div class="col-sm-5">
                <h2>Заявка <a href="javascript:;">№<span id="table-transport-ticket-request-id"></span></a></h2>
            </div>
            <div class="col-sm-7">
                <div class="text-right">
                    <a class="btn btn-blue" href="<?= $controller->links['cargo'] ?>"><?= __dbt('Пошук вантажу') ?></a>
                    <a class="btn btn-blue" href="<?= $controller->links['transport'] ?>"><?= __dbt('Пошук транспорту') ?></a>
                    <a class="backLink transport-ticket-go-back" href="javascript:;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Повернутися назад</a>
                </div>
            </div>
        </div>
    </div>
    <div class="tableHolder">
        <table>
            <thead>
            <tr>
                <th><? __dbt('Дата') ?></th>
                <th><? __dbt('Звідки') ?></th>
                <th><? __dbt('Куди') ?></th>
                <th><? __dbt('Ціна') ?></th>
                <th><? __dbt('Статус') ?></th>
                <th><? __dbt('Виконавець') ?></th>
                <th><? __dbt('Дії') ?></th>
            </tr>
            </thead>
            <tbody id="table-transport-ticket-body-row">
            </tbody>
        </table>
        <div class="orderWatchInfo">
            <div class="clearfix">
                <table class="pull-right">
                    <tbody><tr>
                        <td>Просмотров заявки</td>
                        <td class="watch"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>2</a></td>
                    </tr>
                    <tr>
                        <td>Получено предложений</td>
                        <td class="proposition"><a href="#"><i class="fa fa-files-o" aria-hidden="true"></i>0</a></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
    </div>
    <div class="orderInfoTablesHolder">
        <div class="row">
            <div class="col-xs-6">
                <table class="orderInfoTable">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <h2 class="heading">Дополнительная информация по заявке</h2>
                        </th>
                    </tr>
                    </thead>
                </table>
                <div id="table-transport-ticket-info">
                </div>
<!--                    <tbody id="table-transport-ticket-info">-->
<!--                    <tr>-->
<!--                        <td>Довантаження</td>-->
<!--                        <td><span class="check"><i class="fa fa-check" aria-hidden="true"></i></span></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Тип оплати</td>-->
<!--                        <td>б/г</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>При розвантаженні</td>-->
<!--                        <td><span class="check"><i class="fa fa-check" aria-hidden="true"></i></span></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Примітка</td>-->
<!--                        <td>Крихкий вантаж</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Відсоток</td>-->
<!--                        <td>10%</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>К-сть машин</td>-->
<!--                        <td>12</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Ширина</td>-->
<!--                        <td>3.20 м</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Висота</td>-->
<!--                        <td>3 м</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Довжина</td>-->
<!--                        <td>2.30 м</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Тип вантажу</td>-->
<!--                        <td>ТНП</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Ціна</td>-->
<!--                        <td>25 400 грн</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Передоплата</td>-->
<!--                        <td><span class="check"><i class="fa fa-check" aria-hidden="true"></i></span></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Карта</td>-->
<!--                        <td><a href="#">Показати на карті</a></td>-->
<!--                    </tr>-->
<!--                    </tbody>-->
<!--                </table>-->
            </div>
            <div class="col-xs-6">
                <table class="orderSteps">
                    <thead>
                    <tr>
                        <th>
                            <h2 class="heading">Статус заявки</h2>
                        </th>
                        <th>
                            <h2 class="heading">Дії</h2>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
<!--                    <tr class="stepEnded">-->
<!--                        <td>Підтвердження надіслане, чекаю на "ОК" від контрагента</td>-->
<!--                        <td>-->
<!--                            <ul>-->
<!--                                <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>ПП Техно-Люкс</a></li>-->
<!--                            </ul>-->
<!--                            +38(095) 2541584, +38(063) 2554152 Микола Степанович-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    <tr class="stepEnded">-->
<!--                        <td>Контрагент підтвердив</td>-->
<!--                        <td>-->
<!--                            <ul>-->
<!--                                <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Форма письмової заявки з можливістю редагувати</a></li>-->
<!--                            </ul>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    <tr class="stepEnded">-->
<!--                        <td>Заявка відправлена</td>-->
<!--                        <td>-->
<!--                            <a href="#"><i class="fa fa-check" aria-hidden="true"></i></a>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    <tr class="stepInProcess">-->
<!--                        <td>Заявка підтверджена, в процесі виконання</td>-->
<!--                        <td>-->
<!--                            <ul>-->
<!--                                <li class="stepSuccess"><a href="#"><i class="fa fa-check" aria-hidden="true"></i>Перевезення виконане</a></li>-->
<!--                                <li><a href="#"><i class="fa fa-compass" aria-hidden="true"></i>Відслідковувати</a></li>-->
<!--                                <li><a href="#"><i class="fa fa-print" aria-hidden="true"></i>Надрукувати заявку</a></li>-->
<!--                                <li class="stepNotStarted">-->
<!--                                    <a href="#"><i class="fa fa-comment" aria-hidden="true"></i>Оцінка партнеру і коментар</a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                    <tr class="newStep">-->
<!--                        <td>Заявка успішно завершена</td>-->
<!--                        <td>-->
<!--                            <ul>-->
<!--                                <li><a href="#"><i class="fa fa-archive" aria-hidden="true"></i>В архів</a></li>-->
<!--                            </ul>-->
<!--                        </td>-->
<!--                    </tr>-->
                    </tbody>
                </table>
                <div class="stopCaption clearfix">
                    <a href="#" class="stopIco"></a>
                    <p>Припинити виконання та відкритиформу з поясненням
                        контрагенту причини “ STOP ”</p>
                </div>
                <div class="stepsDescription">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="imgHolder">
                                <div class="step stepEnded"></div>
                            </div>
                            <p>Пройдений етап</p>
                        </div>
                        <div class="col-xs-4">
                            <div class="imgHolder">
                                <div class="step stepInProcess"></div>
                            </div>
                            <p>Етап в процесі виконання</p>
                        </div>
                        <div class="col-xs-4">
                            <div class="imgHolder">
                                <div class="step newStep"></div>
                            </div>
                            <p>Етап не пройдений</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="template_transportCompanyProps" type="text/x-handlebars-template">
    {{#each companies}}
    <figure class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="companyFigure">
            <div class="figureImgHolder">
                <a href="javascript:;">
                    <img src="{{image}}" alt="" />
                </a>
            </div>
            <div class="figureCaption">
                <a href="{{url}}"><h3>{{name}}</h3></a>
                <p class="companyTypeName">{{type}}</p>
                <div class="companyDescription">
                    <p>{{info}}</p>
                </div>
                <div class="companyContacts">
                    <p class="contactType">Телефон</p>
                    <p><i class="fa fa-mobile fa-2x"></i><span class="phoneNumber">{{phoneFull}}</span></p>
                    <p class="contactType">Розташування</p>
                    <p><span class="flag"></span><span class="adress">{{fullAddress}}</span></p>
                </div>
                <div class="orderInProcessTwoButtonsHolder">
                    <button class="btn btn-primary accept-transport-company-prop" data-id="{{appID}}">Прийняти</button>
                    <button class="btn btn-primary reject-transport-company-prop btnCancel" data-id="{{appID}}">Відхилити</button>
                </div>
            </div>
        </div>
    </figure>
    {{/each}}
</script>

<script id="template_transportMyRequests" type="text/x-handlebars-template">
    {{#each requests}}
    <tr class="transport-ticket-row {{classStatus}}" data-status="{{status}}" data-id="{{ID}}"
        data-click="showTransportModal"
        data-from-placeid="{{'from_placeID'}}"
        data-to-placeid="{{to_placeID}}"
        data-request-id="{{ID}}"
        data-request-user-id="{{userID}}"
    >
        <td>
            {{js_date}}
            {{{json_info}}}
        </td>
        <td>
            {{js_from}}
        </td>
        <td>
            {{js_to}}
        </td>
<!--        <td>-->
<!--            {{js_z}}-->
<!--        </td>-->
<!--        <td>-->
<!--            {{js_weight}}-->
<!--        </td>-->
<!--        <td>-->
<!--            {{js_type}}-->
<!--        </td>-->
        <td>
            {{js_price}}
        </td>
        <td>
            {{{js_status}}}
        </td>
        <td>
            {{#if_equal_soft js_action null}}
            -
            {{else}}
            <a href="{{js_action}}" class="go_to_company_page">на сторінку компанії</a>
            {{/if_equal_soft}}
        </td>
        <td class="icoList">
            {{#each js_actions}}
            {{{html}}}
            {{else}}
            -
            {{/each}}
        </td>
    </tr>
    {{else}}
    <tr>
        <td colspan="9"><? __dbt('Заявок не знайдено') ?></td>
    </tr>
    {{/each}}
</script>

