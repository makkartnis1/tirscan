<div class="modal fade paidServicesModalWindow" id="paidServicesModalWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl">Платні послуги</span></h4>
            </div>
            <div class="modal-body">
                <div class="serviceFiguresHolder">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure colorHeading">
                                <div class="serviceFigureHeader">
                                    <div class="radio">
                                        <label>
                                            <span><? __dbt("Виділення кольором") ?></span>
                                        </label>
                                    </div>
                                    <div class="serviceIcon1"></div>
                                </div>
                                <div class="serviceFigureBody">
                                    <p><? __dbt("Опублікувати на період:") ?></p>
                                    <a href="#" class="info"><i class="fa fa-question"></i></a>
                                    <div class="form-group">
                                        <select class="form-control" name="service_color_duration">
                                            <option value="1">1 день</option>
                                            <option value="7">7 днів</option>
                                            <option value="14">14 днів</option>
                                        </select>
                                    </div>
                                    <div class="serviceDefinition">
                                        <p><? __dbt("Заявка буде виділена фоном на тлі інших заявок") ?></p>
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input checked type="radio" name="service_type" value="color" id="price1">За <span class="daysQnt">1</span> день <span class="sumQnt">120</span> грн.</label>
                                        </div>
                                    </div>
                                    <div class="subDefText">
                                        <p>* Послуга буде підключена після оплати</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure vipBlock">
                                <div class="serviceFigureHeader">
                                    <div class="radio">
                                        <label>
                                            <span>VIP - блок</span>
                                        </label>
                                    </div>
                                    <div class="serviceIcon2"></div>
                                </div>
                                <div class="serviceFigureBody">
                                    <p><? __dbt("Опублікувати на період:") ?></p>
                                    <a href="#" class="info"><i class="fa fa-question"></i></a>
                                    <div class="form-group">
                                        <select class="form-control"name="service_vip_duration">
                                            <option value="1">1 день</option>
                                            <option value="7">7 днів</option>
                                            <option value="14">14 днів</option>
                                        </select>
                                    </div>
                                    <div class="serviceDefinition">
                                        <p><? __dbt("Заявка буде розміщена у VIP блоці") ?></p>
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="service_type" value="vip" id="price2">За <span class="daysQnt">1</span> день <span class="sumQnt">120</span> грн.</label>
                                        </div>
                                    </div>
                                    <div class="subDefText">
                                        <p>* Послуга буде підключена після оплати</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure upInSearch">
                                <div class="serviceFigureHeader">
                                    <div class="radio">
                                        <label for="upInSearch">
                                            <span>Підняття в пошуку</span>
                                        </label>
                                    </div>
                                    <div class="serviceIcon3"></div>
                                </div>
                                <div class="serviceFigureBody">
                                    <div class="serviceDefinition">
                                        <p>Заявка піднімається на перше місце в результатах пошуку</p>
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="service_type" value="to_top" id="price3"> <span class="sumQnt">9</span> грн.</label>
                                        </div>
                                    </div>
                                    <div class="subDefText">
                                        <p>* Послуга буде підключена після оплати</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right payRequestService">Оплатити</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade orderInProcessModalWindow" id="orderInProcessModalWindow" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl">Заявка №<span class="transport-ticket-apps-number"></span></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h3>Пропозиції по заявці</h3>
                        <div class="companyThumbnailsHolder">
                            <div class="row" id="orderCompaniesProps">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="RatingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Закриття заявки №<span class="transport-ticket-close-number"></span></h4>
            </div>
            <div class="modal-body rating-modal">
                <div class="radio">
                    <label>
                        <input type="radio" name="closeTicketRating" id="optionsRadios1" value="positive" checked="">
                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="closeTicketRating" id="optionsRadios2" value="negative">
                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                    </label>
                </div>
                    <textarea class="form-control" name="closeTicketText"></textarea>
                <div class="row">
                    <div class="col-xs-3 col-xs-offset-9">
                        <button type="button" id="submitCloseTicket" class="btn btn-primary">Надіслати</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade appSelectDialogModal" id="appSelectDialogModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                            href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Список діалогів по пропозиціям") ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row" data-inside="dialog">
                    <div class="col-md-12" id="appSDM_dialogs">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade appDialogModal" id="appDialogModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                            href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Переписка") ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="dialogueHolder">
                    <div class="heading">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2><? __dbt('Діалог по заявці') ?> №<span id="appDM_requestID"></span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="textInputHolder">
                        <div class="clearfix">
                            <div class="userInfo">
                                <div class="imgHolder">
                                    <div class="img" style="background-image: url('<?= Helper_User::avatar($controller->user->ID); ?>')"></div>
                                    <div class="status online"></div>
                                </div>
                            </div>
                            <div class="textareaHolder">
                                <textarea class="form-control" rows="5" id="appDM_message"></textarea>
                            </div>
                        </div>
                        <div class="buttonHolder">
                            <button class="btn btn-primary" id="appDM_send"><? __dbt('Надіслати') ?></button>
                        </div>
                    </div>
                    <div class="dialogueHistoryHolder" id="appDM_dialogMessages">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade transportOrderModal" id="transportOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                            href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Заявка") ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" data-table="here">
                        <?/* Динамічно грзутиься рядок із таблиці */?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><? __dbt("Додаткова інформація про заявку") ?></h3>
                            </div>
                            <div class="col-md-12" data-info >

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="orderModalMapHolder">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d162757.72582793544!2d30.392609105722805!3d50.402170238254264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2z0JrQuNGX0LIsIDAyMDAw!5e0!3m2!1suk!2sua!4v1453978363646"
                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="modal-apply-area">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= View::factory('frontend/content/requests/modal/request-additional-info'); ?>