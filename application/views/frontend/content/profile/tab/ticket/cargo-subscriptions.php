<div class="heading">
    <div class="row">
        <div class="col-sm-5">
            <h2><? __dbt('Заявки вибраних фірм') ?> <a href="#">(<span class="cargo-subscriptions-count"></span>)</a></h2>
        </div>
        <div class="col-sm-7">
            <div class="text-right">
                <a class="btn btn-blue" href="<?= $controller->links['cargo'] ?>"><?= __dbt('Пошук вантажу') ?></a>
                <a class="btn btn-blue" href="<?= $controller->links['transport'] ?>"><?= __dbt('Пошук транспорту') ?></a>
                <div class="selectGroup">
                    <label for="status"><? __dbt('Статус') ?>:</label>
                    <select name="" id="status" class="form-control">
                        <option value=""><? __dbt('Показати усі') ?></option>
                    </select>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="tableHolder">
    <table>
        <thead>
        <tr>
            <th><? __dbt('Дата') ?></th>
            <th><? __dbt('Звідки') ?></th>
            <th><? __dbt('Куди') ?></th>
            <th><? __dbt('Ціна') ?></th>
            <th><? __dbt('Статус') ?></th>
        </tr>
        </thead>
        <tbody class="table-cargo-my-subscriptions">
        </tbody>
    </table>
</div>

<script id="template_cargoMySubscriptions" type="text/x-handlebars-template">
    {{#each requests}}
    <tr class="transport-ticket-row {{classStatus}}" data-status="{{status}}" data-id="{{ID}}"
        data-click="showTransportModal"
        data-from-placeid="{{'from_placeID'}}"
        data-to-placeid="{{to_placeID}}"
        data-request-id="{{ID}}"
        data-request-user-id="{{userID}}"
    >
        <td>
            {{js_date}}
            {{{json_info}}}
        </td>
        <td>
            {{js_from}}
        </td>
        <td>
            {{js_to}}
        </td>
        <!--        <td>-->
        <!--            {{js_z}}-->
        <!--        </td>-->
        <!--        <td>-->
        <!--            {{js_weight}}-->
        <!--        </td>-->
        <!--        <td>-->
        <!--            {{js_type}}-->
        <!--        </td>-->
        <td>
            {{js_price}}
        </td>
        <td>
            {{{js_status}}}
        </td>
    </tr>
    {{else}}
    <tr>
        <td colspan="9"><? __dbt('Заявок не знайдено') ?></td>
    </tr>
    {{/each}}
</script>
