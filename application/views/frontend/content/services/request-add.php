<div class="greyFluidContainer">
    <div class="container-fluid">
        <div class="container center">
            <div class="pagePaidServices">
                <div class="serviceFiguresHolder">
                    <div class="title-line"><? __dbt("Платні послуги") ?></div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure colorHeading">
                                <div class="serviceFigureHeader">
                                    <div class="radio">
                                        <label>
                                            <span><? __dbt("Виділення кольором") ?></span>
                                        </label>
                                    </div>
                                    <div class="serviceIcon1"></div>
                                </div>
                                <div class="serviceFigureBody">
                                    <p><? __dbt("Опублікувати на період:") ?></p>
                                    <a href="#" class="info"><i class="fa fa-question"></i></a>
                                    <div class="form-group">
                                        <select class="form-control" name="service_color_duration">
                                            <option value="1">1 день</option>
                                            <option value="7">7 днів</option>
                                            <option value="14">14 днів</option>
                                        </select>
                                    </div>
                                    <div class="serviceDefinition">
                                        <p><? __dbt("Заявка буде виділена фоном на тлі інших заявок") ?></p>
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="service_type" value="color" id="price1">За <span class="daysQnt">1</span> день <span class="sumQnt">120</span> грн.</label>
                                        </div>
                                    </div>
                                    <div class="subDefText">
                                        <p>* Послуга буде підключена після оплати</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure vipBlock">
                                <div class="serviceFigureHeader">
                                    <div class="radio">
                                        <label>
                                            <span>VIP - блок</span>
                                        </label>
                                    </div>
                                    <div class="serviceIcon2"></div>
                                </div>
                                <div class="serviceFigureBody">
                                    <p><? __dbt("Опублікувати на період:") ?></p>
                                    <a href="#" class="info"><i class="fa fa-question"></i></a>
                                    <div class="form-group">
                                        <select class="form-control"name="service_vip_duration">
                                            <option value="1">1 день</option>
                                            <option value="7">7 днів</option>
                                            <option value="14">14 днів</option>
                                        </select>
                                    </div>
                                    <div class="serviceDefinition">
                                        <p><? __dbt("Заявка буде розміщена у VIP блоці") ?></p>
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="service_type" value="vip" id="price2">За <span class="daysQnt">1</span> день <span class="sumQnt">120</span> грн.</label>
                                        </div>
                                    </div>
                                    <div class="subDefText">
                                        <p>* Послуга буде підключена після оплати</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="serviceFigure">
                                <div class="serviceFigureHeader simpleOrder">
                                    <div class="radio">
                                        <label>
                                            <span><? __dbt("Звичайна заявка") ?></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="serviceFigureBody">
                                    <p><? __dbt("Звичайна заявка") ?></p>
                                    <a href="#" class="info"><i class="fa fa-question"></i></a>
                                    <div class="serviceDefinition">
                                    </div>
                                    <div class="payForService">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" checked name="service_type" value="regular" id="price4"><? __dbt("Безкоштовно") ?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>