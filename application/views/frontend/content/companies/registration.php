<?
/**
 * @var $controller Controller_Frontend_Company
 * @var $phone_codes Model_Frontend_Phonecodes
 * @var $company_types Model_Frontend_Company_Type
 * @var $ownership_types Model_Frontend_Company_Ownership_Type
 * @var $post array
 * @var $company
 */
?>

<div class="content">
    <form id="registration-form" action="" method="post">
        <div class="container-fluid registration first">
            <div class="container center">
                <div class="row line">
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('email', 'user') ?>">
                            <label for="company_email">E-mail <span>*</span></label>
                            <div class="input-group">
                                <input id="company_email" type="text" name="company_email" value="<?= Arr::get($post, 'company_email', '') ?>" class="form-control" placeholder="">
                            </div>
                            <? $controller->echoErrors('email', 'user') ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('password', 'user') ?>">
                            <label for="user_password"><? __dbt("Пароль ") ?><span>*</span></label>
                            <div class="input-group">
                                <input id="user_password" type="password" name="password" class="form-control" placeholder="">
                            </div>
                            <? $controller->echoErrors('password', 'user') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? if ($company == null) { ?>
        <div class="container-fluid registration second">
            <div class="container center">
                <div class="row line">
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('name', 'company_locale') ?>">
                            <label for="company_name"><? __dbt("Компанія ") ?><span>*</span></label>
                            <div class="input-group">
                                <input id="company_name" type="text" name="company_name" value="<?= Arr::get($post, 'company_name', '') ?>" class="form-control" placeholder="">
                            </div>
                            <? $controller->echoErrors('name', 'company_locale') ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('place_data', 'user') ?>">
                            <label for="input-place"><? __dbt("Місце реєстрації ") ?><span>*</span></label>
                            <div class="input-group">
                                <input type="text" name="place" value="<?= Arr::get($post, 'place', '') ?>" id="input-place" class="form-control searchGeo" placeholder="">
                                <input type="hidden" name="place_data" value="<?= htmlspecialchars(Arr::get($post, 'place_data', '')) ?>">
                            </div>
                            <? $controller->echoErrors('place_data', 'user') ?>
                        </div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="company_type"><? __dbt("Тип компанії на порталі ") ?><span>*</span></label>
                            <div class="input-group">
                                <select id="company_type" class="form-control" name="company_type">
                                    <? foreach ($company_types as $type): ?>
                                        <option value="<?= $type->ID ?>" <?= ($type->ID == Arr::get($post, 'company_type', '')) ? 'selected' : '' ?>><?= __db($type->localeName) ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="company_ownership_type"><? __dbt("Форма ") ?><span>*</span></label>
                            <div class="input-group">
                                <select id="company_ownership_type" class="form-control" name="company_ownership_type">
                                    <? foreach ($ownership_types as $type): ?>
                                        <? if (__db($type->localeName) == '') { continue; } ?>
                                        <option value="<?= $type->ID ?>" <?= ($type->ID == Arr::get($post, 'company_ownership_type', '')) ? 'selected' : '' ?>><?= __db($type->localeName) ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('address', 'company_locale') ?>">
                            <label for="company_address"><? __dbt("Юридична адреса ") ?><span>*</span></label>
                            <div class="input-group">
                                <input id="company_address" type="text" name="company_address" value="<?= Arr::get($post, 'company_address', '') ?>" class="form-control" placeholder="">
                            </div>
                            <? $controller->echoErrors('address', 'company_locale') ?>
                        </div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('name', 'user_locale') ?>">
                            <label for="company_pib"><? __dbt("ПІБ відповідальної особи ") ?><span>*</span></label>
                            <div class="input-group">
                                <input id="company_pib" type="text" name="company_pib" value="<?= Arr::get($post, 'company_pib', '') ?>" class="form-control" placeholder="">
                            </div>
                            <? $controller->echoErrors('name', 'user_locale') ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= $controller->classIfError('realAddress', 'company_locale') ?>">
                            <label for="real_address"><? __dbt("Фактична адреса") ?></label>
                            <div class="input-group">
                                <input id="real_address" type="text" name="real_address" value="<?= Arr::get($post, 'real_address', '') ?>" class="form-control" placeholder="<? __dbt('вкажіть, якщо не співпадає з юридичною') ?>">
                            </div>
                            <? $controller->echoErrors('realAddress', 'company_locale') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid registration first">
            <div class="container center">
                <div class="row line">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="company_phone_code"><? __dbt("Телефон ") ?><span>*</span></label>
                            <div class="input-group">
                                <select id="company_phone_code" class="form-control" name="company_phone_code">
                                    <? foreach ($phone_codes as $code): ?>
                                        <option value="<?= $code->ID ?>" <?= ($type->ID == Arr::get($post, 'company_phone_code', '')) ? 'selected' : '' ?>><?= $code->code ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?= $controller->classIfError('phone', 'company') ?>">
                            <label for="company_phone"></label>
                            <div class="input-group">
                                <input id="company_phone" type="text" class="form-control allownumericwithdecimal" name="company_phone" value="<?= Arr::get($post, 'company_phone', '') ?>" placeholder="">
                            </div>
                            <? $controller->echoErrors('phone', 'company') ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="icq">ICQ</label>
                            <div class="input-group">
                                <input id="icq" type="text" class="form-control allownumericwithdecimal" name="icq" value="<?= Arr::get($post, 'icq', '') ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row line">
                    <div class="col-md-2">
                        <label for="stationary_phone_code"><? __dbt("Стаціонарний") ?></label>
                        <div class="input-group">
                            <select id="stationary_phone_code" class="form-control" name="stationary_phone_code">
                                <? foreach ($phone_codes as $code): ?>
                                    <option value="<?= $code->ID ?>" <?= ($type->ID == Arr::get($post, 'stationary_phone_code', '')) ? 'selected' : '' ?>><?= $code->code ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group <?= $controller->classIfError('phoneStationary', 'company') ?>">
                            <label for="stationary_phone"></label>
                            <div class="input-group">
                                <input id="stationary_phone" type="text" class="form-control allownumericwithdecimal" name="stationary_phone" value="<?= Arr::get($post, 'stationary_phone', '') ?>" placeholder="">
                            </div>
                            <? $controller->echoErrors('phoneStationary', 'company') ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="skype">Skype</label>
                            <div class="input-group">
                                <input id="skype" type="text" class="form-control" name="skype" value="<?= Arr::get($post, 'skype', '') ?>" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>
        <div class="container-fluid registration second bottom">
            <div class="container center">
                <? if ($company == null) { ?>
                <div class="row line">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="company_info"><? __dbt("Додаткова інформація") ?></label>
                            <div class="input-group">
                                <textarea id="company_info" class="form-control" placeholder="" name="company_info" rows="3"><?= Arr::get($post, 'company_info', '') ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <? } ?>
                <div class="row line">
                    <div class="col-md-9">
                        <p class="rules">
                            <label class="label--checkbox">
                                <input type="checkbox" name="terms_accepted" class="checkbox" checked>
                                <span class="gr"><? __dbt('Я ознайомився і згідний з ') ?> <a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'public-offer'])) ?>"><? __dbt('правилами') ?></a> <? __dbt('на сайті') ?></span>
                            </label>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <div type="button" id="registration-submit" class="btn btn-primary"><b><? __dbt("Реєстрація") ?></b></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>