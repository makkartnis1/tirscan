<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 27.05.16
 * Time: 18:17
 */?>

<div class="col-lg-6 col-md-3 col-sm-4 col-xs-6">
    <div class="partner">
        <a href="<?= $url ?>"><img src="<?= $avatar ?>" alt="<?= $name ?>"></a>
    </div>
</div>
