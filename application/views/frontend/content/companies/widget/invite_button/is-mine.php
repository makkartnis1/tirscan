<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 25.05.16
 * Time: 17:16
 */
?>
<div class="well well-sm linker-partner">
    <i class="fa fa-lock"></i> <?= __db('partner.is.mine') ?>
</div>
