<?
/**
 * @var $back_link_url string
 */
?>
<div class="t-block">
    <div class="t-title">
        <p><?= __db('company.list.notFound') ?></p>
    </div>
    <div class="t-content">
        <p><a href="<?= $back_link_url ?>"><?= __db('company.list.notFoundBackLink') ?></a></p>
    </div>
</div>