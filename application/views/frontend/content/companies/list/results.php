<?
/**
 * @var $controller Controller_Frontend_Authorized
 * @var $companies array[]
 */
?>

    <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--        <img style="position: absolute; left: -175px; top: 40px" src="http://placehold.it/160x600" />-->
<!--        <img style="position: absolute; right: -175px; top: 40px" src="http://placehold.it/160x600" />-->
    </div>
    <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--        <img style="position: absolute; left: -175px; top: 645px" src="http://placehold.it/160x850" />-->
<!--        <img style="position: absolute; right: -175px; top: 645px" src="http://placehold.it/160x850" />-->
    </div>

<div class="companyThumbnailsHolder">
<!--    <img class="visible-lg visible-sm" src="http://placehold.it/970x90" />-->
    <? $row_c = 0; foreach ($companies as $company): ?>
        <? if ($row_c == 0) { ?>
        <div class="row">
        <? } $row_c++; ?>
            <figure class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="companyFigure">
                    <div class="figureImgHolder">
                        <a href="<?= $company['url'] ?>">
                            <img src="<?= $company['image'] ?>" alt=""/>
                        </a>
                    </div>
                    <div class="figureCaption">
                        <a href="<?= $company['url'] ?>"><h3><?= $company['name'] ?></h3></a>
                        <p class="companyTypeName"><?= $company['type'] ?></p>
                        <div class="companyDescription">
                            <p><?= ($company['info'] != null) ? Frontend_Helper_String::substr_if_more($company['info'], 100) : __dbt('Опис не вказано') ?></p>
                        </div>
                        <div class="companyContacts">
                            <p class="contactType"><? __dbt("Телефон") ?></p>
                            <p><i class="fa fa-mobile fa-2x"></i>
                                <span class="phoneNumber">
                                    <? if ($controller->authorized) { ?>
                                        <?= $company['phoneCode'] . $company['phone'] ?>
                                    <? } else { ?>
                                        <? __dbt('(телефон приховано)') ?>
                                    <? } ?>
                                </span>
                            </p>
                            <p class="contactType"><? __dbt("Розташування") ?></p>
                            <p><span class="flag"></span><span class="adress">
                                    <? if ($company['address'] != '') {
                                        echo $company['address'] . ' (' . $company['fullAddress'] . ')';
                                    }
                                    else {
                                        echo $company['fullAddress'];
                                    }
                                    ?>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="figureRating">
                        <p>
                            <a href="<?= $company['url'] ?>" class="hasQtip"><i class="fa fa-file"></i><?= $company['requests'] ?></a>
                            <a href="<?= $company['url'] ?>"><i class="fa fa-comment"></i><span class="positive-rating">+<?= $company['rating']['positive'] ?></span> / <span class="negative-rating">-<?= $company['rating']['negative'] ?></span></a>
                        </p>
                    </div>
                </div>
            </figure>
        <? if ($row_c == 4) { $row_c = 0; ?>
        </div>
        <? } ?>
    <? endforeach; ?>
</div>

<? $controller->echoBlock('pagination_companies') ?>