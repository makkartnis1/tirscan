<div class="content">
    <div class="container-fluid profile backgroundContainer">
        <div class="container center profile">
            <div class="newsMainHolder">
                <div class="row">
                    <div class="col-lg-9 col-md-12">
                        <?php if(isset($company_news_list)) { ?>
                            <?php foreach ($company_news_list as $value) { ?>
                            <div class="newsItem row">
<!--                                <div class="col-lg-2 col-md-2 col-sm-2">-->
<!--                                    <div class="newsItemDate">-->
<!--                                        <p class="dateNumber">15</p>-->
<!--                                        <p class="dateMonth">січ</p>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <a href="<?= $value['ID']?>-<?= $value['url']?>"><h3><?= $value['title']?></h3></a>
                                    <p class="timeYear"><?= $value['createDate']?></p>
                                    <p class="anons"><?= $value['preview']?></p>
                                    <div class="clearfix">
                                        <a href="<?= $value['ID']?>-<?= $value['url']?>" class="btn btn-primary pull-right" style="width: 120px; font-size: 12px!important;"><? __dbt("Детальніше...") ?></a>
                                    </div>
                                </div>
                                <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                            </div>
                            <?php } ?>
                        <?php } else { ?>
                            <p><? __dbt("Пусто") ?></p>
                        <?php } ?>
                        <nav class="text-center">

                            <? $controller->echoBlock('pagination_company_news') ?>

                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="row">
                            <div class="newsCategoriesMenu col-lg-12 col-md-6 col-sm-6">
                                <h2><? __dbt("Дивіться також") ?></h2>
                                <ul>
                                    <li><a href="#"><span>&#187;</span><? __dbt("Новини компаній") ?></a></li>
                                    <li><a href="/profile"><span>&#187;</span><? __dbt("Профіль компанії") ?></a></li>
                                </ul>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-6 popularNewsHolder">
                                <h2><? __dbt("Популярні новини") ?></h2>
                                <div class="row popularNewsItem">
                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">
                                        <div class="newsMinImgHolder">
                                            <a href="#"><img src="/assets/img/newsMin1.jpg" alt=""/></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                        <a href="#"><h3><? __dbt("На що звернути увагу при страхуванні") ?></h3></a>
                                        <p class="popularNewsDate">Січ 12, 2016</p>
                                    </div>
                                </div>
                                <div class="row popularNewsItem">
                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">
                                        <div class="newsMinImgHolder">
                                            <a href="#"><img src="/assets/img/newsMin2.jpg" alt=""/></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                        <a href="#"><h3><? __dbt("Хто повинен пройти технічний контроль") ?></h3></a>
                                        <p class="popularNewsDate">Січ 12, 2016</p>
                                    </div>
                                </div>
                                <div class="row popularNewsItem">
                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">
                                        <div class="newsMinImgHolder">
                                            <a href="#"><img src="/assets/img/newsMin3.jpg" alt=""/></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                        <a href="#"><h3><? __dbt("Чому бояться європротоколу?") ?></h3></a>
                                        <p class="popularNewsDate">Січ 12, 2016</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>