<div class="content">
    <div class="container-fluid profile backgroundContainer">
        <div class="container center profile">
            <div class="newsMainHolder">
                <div class="row">
                    <div class="col-lg-9 col-md-12 newsItem">
                        <div class="row">
                            <div class="col-lg-12 newsItemMainImg">
                                <img class="img-responsive" src="<?=$company_news_data['image']?>" alt=""/>
<!--                                <div class="newsItemDate col-sm-3 hidden-xs">-->
<!--                                    <p class="dateNumber">15</p>-->
<!--                                    <p class="dateMonth">січ</p>-->
<!--                                </div>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 newsItemText">
                                <h1><?= $company_news_data['title']?></h1>
                                <p class="date"><?= $company_news_data['createDate']?></p>
                                <div class="text"><?= $company_news_data['content']?></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-12">
                        <div class="row">
                            <div class="newsCategoriesMenu col-lg-12 col-md-6 col-sm-6">
                                <h2><? __dbt("Дивіться також") ?></h2>
                                <ul>
                                    <li><a href="<?= $controller->links['profile_index'] ?>"><span>&#187;</span><? __dbt("Профіль компанії") ?></a></li>
                                </ul>
                            </div>
<!--                            <div class="col-lg-12 col-md-6 col-sm-6 popularNewsHolder">-->
<!--                                <h2>Популярні новини</h2>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin1.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>На що звернути увагу при страхуванні</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin2.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>Хто повинен пройти технічний контроль</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin3.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>Чому бояться європротоколу?</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-lg-12 col-md-6 col-sm-6">
                                <a href="https://www.priceline.com" target="_blank">
                                    <img src="/uploads/b/2.png" style="width: 100%; border: 1px solid;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>