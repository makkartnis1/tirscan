<?
/**
 * @var $controller Controller_Frontend_Authorized
 * @var $companies
 * @var $filter_form
 * @var $search_form_action
 */
?>

<div class="content companies">
    <div class="greyFluidContainer">
        <div class="container-fluid">
            <div class="container center">
                <div class="formHolder">
                    <form id="filter-form" class="companyInputHolder" method="get" action="<?= $search_form_action ?>">
                        <div class="row">
                            <div class="form-group col-lg-5 col-md-5 col-sm-12 companyInput">
                                <input type="text" name="f_text" value="<?= $filter_form['f_text'] ?>" class="form-control" placeholder="<? __dbt('Пошук по компаніям') ?>"/>
                            </div>
                            <div class="form-group col-lg-5 col-md-5 col-sm-12 companyInput">
                                <input type="text" name="f_place" value="<?= $filter_form['f_place'] ?>" id="filter-place" class="form-control searchGeo" placeholder="<? __dbt('Пошук за місцезнаходженням') ?>"/>
                                <input type="hidden" name="f_place_id" value="<?= $filter_form['f_place_id'] ?>">
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 companyInput">
                                <button class="btn btn-primary"><i class="fa fa-search"></i><? __dbt('Знайти') ?></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 companyInput">
                                <div class="dropdown krid-select" ks-name="f_type" ks-placeholder="<? __dbt('Тип діяльності компанії') ?>" ks-default-value="<?= $filter_form['f_type'] ?>">
                                    <div class="dropdown-toggle filter" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="dropdown-select"><? __dbt('Тип діяльності компанії') ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">
                                        <li ks-value="2"><a href="javascript:;"><? __dbt('транспортна') ?></a></li>
                                        <li ks-value="1"><a href="javascript:;"><? __dbt('експедиція') ?></a></li>
                                        <li ks-value="3"><a href="javascript:;"><? __dbt('вантажотримач') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 companyInput">
                                <div class="dropdown krid-select" ks-name="f_rating" ks-placeholder="<? __dbt('Рейтинг') ?>" ks-default-value="<?= $filter_form['f_rating'] ?>">
                                    <div class="dropdown-toggle filter" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="dropdown-select"><? __dbt('Рейтинг') ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">
                                        <li ks-value="only_positive"><a href="javascript:;"><? __dbt("лише позитивний") ?></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 companyInput">
                                <div class="dropdown krid-select" ks-name="f_sort" ks-placeholder="<? __dbt('Сортувати') ?>" ks-default-value="<?= $filter_form['f_sort'] ?>">
                                    <div class="dropdown-toggle filter" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="dropdown-select"><? __dbt("Сортувати") ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">
                                        <li ks-value="by-rating"><a href="javascript:;"><? __dbt("за рейтингом") ?></a></li>
                                        <li ks-value="by-reviews"><a href="javascript:;"><? __dbt("за відгуками") ?></a></li>
                                        <li ks-value="by-registration"><a href="javascript:;"><? __dbt("за реєстрацією") ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="whiteFluidContainer">
        <div class="container-fluid ">
            <div class="container center">
                <? $controller->echoBlock('companies') ?>
            </div>
        </div>
    </div>
</div>
<div class="container center">
    <div class="seo_block_list">
        <?= $controller->seo['content_text']; ?>
    </div>
</div>