<?php
/**
 * @var $controller Controller_Frontend_Authorized
 * @var $company array
 * @var $documents array[]
 * @var $documents_count int
 * @var $comments array[]
 * @var $requests array[]
 * @var $requests_count int
 * @var $company_image string
 * @var $last_news
 * @var $transports
 * @var $documents_blocked
 */
?>
<?
    $total_doc_count = 0;
    if ($controller->authorized) {
        $widgets    = Controller_Frontend_HMVC_Profile_Tab_Info_Docs::widgets_company($company['ID']);
        $widget_str = '';

        foreach ($widgets as $widget) {

            if ($count = substr_count($widget, 'docName transport')) {
                $widget_str .= $widget;

                $total_doc_count += $count;

            }
        }
    }
?>

<div class="content">
    <div class="container-fluid profile backgroundContainer">
        <div class="container center profile">
            <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--                <img style="position: absolute; left: -175px; top: 110px" src="http://placehold.it/160x600" />-->
<!--                <img style="position: absolute; right: -175px; top: 110px" src="http://placehold.it/160x600" />-->
            </div>
            <div class="newsMainHolder">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="oneCompanyFigure">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="thumbnailImgHolder">
<!--                                                <a href="#">-->
                                                    <img src="<?= $company_image ?>" alt=""/>
<!--                                                </a>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="thumbnailCaption">
                                                <div class="companyRatingHolder">
                                                    <p><?= __db('company.page.rating') ?>
                                                        <span class="positive-rating">+<?= $company['rating']['positive'] ?></span>
                                                        /
                                                        <span class="negative-rating">-<?= $company['rating']['negative'] ?></span>
                                                    </p>
                                                </div>
                                                <div class="companyDescription">
                                                    <p><?= ($company['info'] != null) ? Frontend_Helper_String::substr_if_more($company['info'], 200) : __dbt('Опис не вказано') ?></p>
                                                </div>
                                                <div class="companyType">
                                                    <p class="companyTypeHeader"><?= __db('company.page.activity') ?></p>
                                                    <p><?= __db($company['typeLocale']) ?></p>
                                                </div>
                                                <div class="address">
                                                    <p class="addressHeader"><?= __db('company.page.location') ?></p>
                                                    <p>
                                                        <span class="flag"></span><span class="adress">
                                                            <? if ($company['address'] != '') {
                                                                echo $company['address'] . ' (' . $company['fullAddress'] . ')';
                                                            }
                                                            else {
                                                                echo $company['fullAddress'];
                                                            }
                                                            ?>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 oneCompanyMainInfo">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs responsive" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= __db('company.page.tab.infromation') ?></a>
                                    </li>
                                    <? if ($controller->authorized) { ?>
                                    <li role="presentation">
                                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?= __db('company.page.tab.requests(:count)', [':count' => $requests_count]) ?></a>
                                    </li>
                                    <? } ?>
                                    <li role="presentation">
                                        <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?= __db('company.page.tab.documents(:count)', [':count' => $total_doc_count]) ?></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><?= __db('company.page.tab.transport(:count)', [':count' => count($transports)]) ?></a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content responsive oneCompanyTabContent">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <p class="partHeading"><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = "Дата реєстрації на
                                            TIRSCAN:";     $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/ ?><span class="date"><?= date('d.m.Y G:i:s', strtotime($company['registered'])); ?></span></p>
                                        <!--                                    <p><span class="blue">Увага!</span> Перегляд поштової адреси і документів можливий тільки для співробітників Вашої компанії і оператора TIRSCAN</p>-->
                                        <p><?= ($company['info'] != null) ? $company['info'] : 'Опис не вказано' ?></p>

                                        <? $controller->echoBlock('company/documents', compact('documents')) ?>
                                    </div>
                                    <? if ($controller->authorized) { ?>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                    <? if ($requests_count > 0) { ?>
                                        <? if (count($requests['cargo']) > 0) { echo "Вантажні заявки:<p>"; ?>
                                        <section class="container-fluid table-section">
                                            <div class="table-responsive">
                                                <section class="table-section-holder">
                                                    <table class="table table-striped table-condensed table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th><? __dbt("Дата") ?></th>
                                                            <th><? __dbt("Звідки") ?></th>
                                                            <th><? __dbt("Куди") ?></th>
                                                            <th><? __dbt("Довжина") ?></th>
                                                            <th><? __dbt("Вага") ?></th>
                                                            <th><? __dbt("Тип") ?></th>
                                                            <th><? __dbt("Ціна") ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <? foreach ($requests['cargo'] as $request) { ?>
                                                            <tr
                                                                data-id="<?= $request['ID'] ?>"
                                                                data-request-type="cargo"
                                                                data-click="showTransportModal"
                                                                data-from-placeid="<?= $request['from_placeID'] ?>"
                                                                data-to-placeid="<?= $request['to_placeID'] ?>"
                                                                data-request-id="<?= $request['ID']?>"
                                                            >
                                                                <td>
                                                                    <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
                                                                    <? $json = [] ?>
                                                                    <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

                                                                    <?

                                                                    if( !empty($request['_json_advancedPayment']) ){
                                                                        $unit = trim($request['_json_advancedPayment']);
                                                                    }else{
                                                                        $unit = $request['_json_currency_code'];
                                                                    }

                                                                    $json['price'] = $request['_json_price'].'|'.$unit;

                                                                    ?>

                                                                    <script type="application/json"><?= json_encode($json) ?></script>
                                                                </td>
                                                                <td>
                                                                    <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
                                                                </td>
                                                                <td>
                                                                    <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
                                                                </td>
                                                                <td>
                                                                    <?= Frontend_Helper_String::none_if_null($request['sizeZ'], (float) $request['sizeZ'] . ' м') ?>
                                                                </td>
                                                                <td>
                                                                    <?= Frontend_Helper_String::none_if_null($request['weight'], (float) $request['weight'] . ' т') ?>
                                                                </td>
                                                                <td>
                                                                    <?= Frontend_Helper_String::none_if_null($request['car_type_localeName'], __db($request['car_type_localeName'])) ?>
                                                                </td>
                                                                <td>
                                                                    <?= Frontend_Helper_String::none_if_null($request['value'],  $request['value'] . ' ' . $request['valueCode']) ?>
                                                                </td>
                                                            </tr>
                                                        <? } ?>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </section>
                                        <? } ?>
                                        <? if (count($requests['transport']) > 0) { echo "Транспортні заявки:<p>"; ?>
                                            <section class="container-fluid table-section">
                                                <div class="table-responsive">
                                                    <section class="table-section-holder">
                                                        <table class="table table-striped table-condensed table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th><? __dbt("Дата") ?></th>
                                                                <th><? __dbt("Звідки") ?></th>
                                                                <th><? __dbt("Куди") ?></th>
                                                                <th><? __dbt("Довжина") ?></th>
                                                                <th><? __dbt("Вага") ?></th>
                                                                <th><? __dbt("Кузов") ?></th>
                                                                <th><? __dbt("Ціна") ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <? foreach ($requests['transport'] as $request) { ?>
                                                                <tr
                                                                    data-id="<?= $request['ID'] ?>"
                                                                    data-request-type="transport"
                                                                    data-click="showTransportModal"
                                                                    data-from-placeid="<?= $request['from_placeID'] ?>"
                                                                    data-to-placeid="<?= $request['to_placeID'] ?>"
                                                                    data-request-id="<?= $request['ID']?>"
                                                                >
                                                                    <td>
                                                                        <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
                                                                        <? $json = [] ?>
                                                                        <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

                                                                        <?

                                                                        if( !empty($request['_json_advancedPayment']) ){
                                                                            $unit = trim($request['_json_advancedPayment']);
                                                                        }else{
                                                                            $unit = $request['_json_currency_code'];
                                                                        }

                                                                        $json['price'] = $request['_json_price'].'|'.$unit;

                                                                        ?>

                                                                        <script type="application/json"><?= json_encode($json) ?></script>
                                                                    </td>
                                                                    <td>
                                                                        <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
                                                                    </td>
                                                                    <td>
                                                                        <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
                                                                    </td>
                                                                    <td>
                                                                        <?= Frontend_Helper_String::none_if_null($request['car_sizeZ'], (float) $request['car_sizeZ'] . ' м') ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float) $request['car_liftingCapacity'] . ' т') ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= Frontend_Helper_String::none_if_null($request['car_type'], __db('car.body.' . $request['car_type'])) ?>
                                                                    </td>
                                                                    <td>
                                                                        <?= Frontend_Helper_String::none_if_null($request['value'],  $request['value'] . ' ' . $request['valueCode']) ?>
                                                                    </td>
                                                                </tr>
                                                            <? } ?>
                                                            </tbody>
                                                        </table>
                                                    </section>
                                                </div>
                                            </section>
                                        <? } ?>
                                    <? } else { ?>
                                        <span><? __dbt("Заявок не знайдено") ?></span>
                                    <? } ?>
                                    </div>
                                    <? } ?>
                                    <div role="tabpanel" class="tab-pane" id="messages">
                                        <? if ($documents_blocked) { ?>
                                            <span>Ви не можете переглядати документи цієї компанії</span>
                                        <? }
                                        else { ?>
                                            <? if ($total_doc_count == 0) { ?>
                                                <span><? __dbt("За компанією не закріплено документів") ?></span>
                                            <? } else {
                                                echo $widget_str;
                                            }} ?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="settings">
                                        <? if (empty($transports)) { ?>
                                        <span><? __dbt("Транспорт не знайдено") ?></span>
                                        <? } else { ?>
                                            <section class="container-fluid table-section">
                                                <div class="table-responsive">
                                                    <section class="table-section-holder">
                                                        <table class="table table-striped table-condensed table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Номер</th>
                                                                <th>Тип</th>
                                                                <th>Кузов</th>
                                                                <th>Об'єм</th>
                                                                <th>В/п</th>
                                                                <th>Довжина</th>
                                                                <th>Ширина</th>
                                                                <th>Висота</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <? foreach ($transports as $transport) { ?>
                                                                <tr>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['number'], $transport['number']) ?></td>
                                                                    <td><?= __db($transport['type']) ?></td>
                                                                    <td><?= __db($transport['localeName']) ?></td>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['volume'], (float) $transport['volume'] . ' м<sup>3</sup>') ?></td>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['liftingCapacity'], (float) $transport['liftingCapacity'] . ' т') ?></td>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['sizeZ'], (float) $transport['sizeZ'] . ' м') ?></td>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['sizeX'], (float) $transport['sizeX'] . ' м') ?></td>
                                                                    <td><?= Frontend_Helper_String::none_if_null($transport['sizeY'], (float) $transport['sizeY'] . ' м') ?></td>
                                                                </tr>
                                                            <? } ?>
                                                            </tbody>
                                                        </table>
                                                    </section>
                                                </div>
                                            </section>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="oneCompanyMainFigure companyComments">
                                    <h3><? __dbt("Відгуки про компанію") ?> (<?= $company['rating']['positive'] + $company['rating']['negative'] ?>)</h3>
                                    <div class="oneCompanyMainFigureContent">
                                        <div class="row">
                                            <div class="col-lg-12">
                                            <? if (count($comments) > 0) { ?>
                                                <div class="info-profile reviews">
                                                <? foreach ($comments as $comment) { ?>
                                                    <div class="reviews-box">
                                                        <div class="row inner-tab reviews rew">
                                                            <div class="col-xs-12 col-ms-8 col-sm-6 col-md-5">
                                                                <div class="profile-f reviews">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-ms-12 col-sm-12 col-md-3">
                                                                            <div class="profile-f-img">
                                                                                <img src="<?= $comment['image'] ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-ms-12 col-sm-12 col-md-9">
                                                                            <div class="profile-f-p">
                                                                                <span class="profile-f-top">
                                                                                    <p><a href="<?= $comment['company_url'] ?>"><?= $comment['name'] ?></a>
                                                                                </span>
                                                                                <p></p>
                                                                                <span class="profile-f-bottom">
                                                                                     <p><?= $comment['createDate'] ?></p>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-ms-4 col-sm-6 col-md-7">
                                                            <? if ($comment['rating'] == 'positive') { ?>
                                                                <p class="pull-right green reviews"><? __dbt("Позитивний відгук") ?></p>
                                                            <? } else { ?>
                                                                <p class="pull-right red reviews"><? __dbt("Негативний відгук") ?></p>
                                                            <? } ?>
                                                            </div>
                                                        </div>
                                                        <div class="row inner-tab reviews">
                                                            <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">
                                                                <p class="reviews-p">
                                                                    <?= $comment['text'] ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? } ?>
                                                </div>
                                            <? } else { ?>
                                                <span><? __dbt("У компанії наразі немає жодного відгуку") ?></span>
                                            <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="oneCompanyMainFigure">
                                    <h3><? __dbt("Зворотній зв'язок") ?></h3>
                                    <div class="oneCompanyMainFigureContent">
                                        <div class="figurePart">
                                            <h4><? __dbt("Менеджер компанії") ?></h4>
                                            <p>
                                                <? if ($controller->authorized) { ?>
                                                    <?= $company['phoneFull'] ?>
                                                <? } else { ?>
                                                    <? __dbt('(телефон приховано)') ?>
                                                <? } ?>
                                            </p>
                                        </div>

                                        <? if ($controller->authorized) { ?>
                                            <a id="create-message" class="btn btn-primary" href="#">
                                                 <i class="fa fa-envelope"></i><?= __db('send.message.company')?>
                                            </a>
                                            <button id="complaint" class="btn btn-primary"><i class="fa fa-envelope"></i><?= __db('add.complaints.on.company')?></button>
                                            <button class="btn btn-primary" type="submit" id="add-black-list"><i class="fa fa-user-times" aria-hidden="true"></i><?= __db('add.company.in.black.list')?></button>
                                            <?= (isset($invite_button)) ? $invite_button : '' ?>
                                            <form action="<?= $_SERVER['REQUEST_URI'] ?>" method='post'>
                                                <input type="hidden" name="company" value="<?= $company['ID']; ?>">
                                                <? if(isset($subscribe) && $subscribe): ?>
                                                <input type="hidden" name="action" value="unsubscribe">
                                                <input type="submit"  id="user_company_subscribe" class="btn btn-primary" value="<? __dbt('відписатись')?>">
                                                <? else: ?>
                                                <input type="hidden" name="action" value="subscribe">
                                                <input type="submit"  id="user_company_subscribe" class="btn btn-primary" value="<? __dbt('підписатись')?>">
                                                <? endif; ?>
                                            </form>
                                        <? } ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="oneCompanyMainFigure">
                                    <h3><? __dbt("Новини компанії") ?></h3>
                                    <div class="oneCompanyMainFigureContent popularNewsHolder">
                                        <?php if (empty($last_news)) { ?>
                                            <p><? __dbt("Новини відсутні") ?></p>
                                        <?php } else { ?>
                                            <?php foreach ($last_news as $value) { ?>
                                                <div class="row popularNewsItem">
                                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">
                                                        <div class="newsMinImgHolder">
                                                            <a href="<?= Route::url('frontend_site_company_page_news', $controller->getRequestParams(['company_id' => $company['ID'], 'id' => $value['ID'], 'url' => $value['url']])) ?>"><img src="<?= $value['image']?>" alt=""></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                                        <a href="<?= Route::url('frontend_site_company_page_news', $controller->getRequestParams(['company_id' => $company['ID'], 'id' => $value['ID'], 'url' => $value['url']])) ?>">
                                                            <h4><?= $value['title']; ?></h4></a>
                                                        <p class="popularNewsDate"><?= $value['createDate']; ?></p>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <a href="<?= Route::url('frontend_site_company_list_news', $controller->getRequestParams(['company_id' => $company['ID']])) ?>" class="allItems"><? __dbt("Всі новини компанії") ?></a>
                                            <a href="#" class="allItems" style="float: right;" id="subscribe-to-news" ><? __dbt("Підписатись на новини") ?></a>

                                        <?php } ?>
                                    </div>

                                    <input type="hidden" id="user_id" value="<?= $controller->user->ID; ?>">
                                    <input type="hidden" id="company_id" value="<?= $company['ID']; ?>">

                                </div>

                                <div style="margin-top: 20px;" class="visible-lg visible-sm">
<!--                                    <a href="https://www.markmonitor.com" target="_blank">-->
<!--                                        <img src="/uploads/b/1.png" style="width: 100%; border: 1px solid;">-->
<!--                                    </a>-->
<!--                                    <img src="http://placehold.it/283x283" />-->
                                </div>
                            </div>
                        </div>
                    </div>
                    
<!--                    --><?// if(!empty($controller->aside_widgets)) { ?>
<!--                    --><?// foreach ($controller->aside_widgets as $data) { ?>
<!--                    <div class="row">-->
<!--                        <div class="col-lg-12">-->
<!--                            <div class="oneCompanyMainFigure">-->
<!--                                <h3>--><?//=         $data['name']       ?><!--</h3>-->
<!--                                <div class="oneCompanyMainFigureContent popularNewsHolder">-->
<!--                                    --><?//=         $data['content']    ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    --><?// } ?>
<!--                    --><?// } ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-complaint" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Скарга на компанію") ?></span></h4>
            </div>
            <form method="post" id="form-complaint">
                <div class="modal-body">
                    <div class="form-title"><? __dbt("Ваш e-mail") ?></div>
                    <div class="form-group">
                        <input type="email"
                            name="user-email"
                            class="form-control"
                            value="<?= $controller->user->email; ?>"
                            disabled="disabled">
                    </div>

                    <div class="form-title">Ім'я</div>
                    <div class="form-group">
                        <input type="hidden" name="user-id" value="<?= $controller->user->ID; ?>">
                        <input type="text"
                            name="user-name"
                            class="form-control"
                            value="<?= $controller->user_locale->name ?>"
                            disabled="disabled">
                    </div>

                    <div class="form-title"><? __dbt("Скарга") ?></div>
                    <div class="form-group">
                        <input type="hidden" name="complain-company-id" value="<?= $company['ID'] ?>">
                        <textarea type="text"
                            name="message"
                            id="message-textarea"
                            class="form-control <?= $controller->classIfError('message', 'complaint') ?>"></textarea>
                    </div>
                    <ul id="error-msg" class="errorText"></ul>

                    <div class="row">
                        <div class="offset-md-7 col-sm-5">
                            <button type="submit"  id="send-complaint" class="btn btn-primary"><? __dbt("Відправити") ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-message" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Повідомлення компанії") ?></span></h4>
            </div>
            <form method="post" id="form-messages">
                <div class="modal-body">
                    <div class="form-title">Ім'я</div>
                    <div class="form-group">
                        <input type="hidden" name="user_from" value="<?= $controller->user->ID; ?>">
                        <input type="text"
                            name="user-name"
                            class="form-control"
                            value="<?= $controller->user_locale->name ?>"
                            disabled="disabled">
                    </div>

                    <div class="form-title"><? __dbt("Тема повідомлення") ?></div>
                    <div class="form-group">
                        <input type="text"
                            name="theme"
                            class="form-control">
                    </div>

                    <div class="form-title"><? __dbt("Повідомлення") ?></div>
                    <div class="form-group">
                        <input type="hidden" name="user_to" value="<?= $company['userID'] ?>">
                        <textarea type="text"
                            name="message"
                            id="message-textarea"
                            class="form-control <?= $controller->classIfError('message', 'complaint') ?>"></textarea>
                    </div>
                    <ul id="error-msg" class="errorText"></ul>

                    <div class="row">
                        <div class="offset-md-7 col-sm-5">
                            <button type="submit" id="send-message" class="btn btn-primary"><? __dbt("Відправити") ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade transportOrderModal" id="transportOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                            href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Заявка") ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 table-section-holder" data-table="here">
                        <?/* Динамічно грзутиься рядок із таблиці */?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><? __dbt("Додаткова інформація про заявку") ?></h3>
                            </div>
                            <div class="col-md-12" data-info >

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="orderModalMapHolder">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d162757.72582793544!2d30.392609105722805!3d50.402170238254264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2z0JrQuNGX0LIsIDAyMDAw!5e0!3m2!1suk!2sua!4v1453978363646"
                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="modal-apply-area">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="template_modalRequestApply" type="text/x-handlebars-template">
    {{#if loaded}}
    <span class="request-text"><? __dbt("Вами вже було відправлено пропозицію по даній заявці") ?></span>
    {{else}}
    {{#if not_mine}}
    <button id="btn-send-application" class="btn btn-primary pull-right" data-request-type="{{type}}" data-request-id="{{request_id}}"><? __dbt("Відправити пропозицію") ?></button>
    {{else}}
    <span class="request-text"><? __dbt("Це заявка розміщена вашою компанією") ?></span>
    {{/if}}
    {{/if}}
</script>

<?= View::factory('frontend/content/requests/modal/request-additional-info'); ?>