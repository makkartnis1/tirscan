<?
/**
 * @var $documents array[]
 */
?>

<? if ($documents !== false): ?>
    <? foreach ($documents as $doc_name => $docs): ?>
        <div class="companyDocsHolder">
            <p class="partHeading"><?= __db('company.page.documents.' . $doc_name) ?>:</p>
            <div class="row">
                <? foreach ($docs as $doc): ?>
                    <div class="col-xs-6">
                        <div class="row companyDocs">
                            <div class="col-xs-4 col-sm-3">
                                <div class="imgDoc"></div>
                            </div>
                            <div class="col-xs-8 col-sm-9 docName">
                                <p><span class="blue"><?= $doc['title'] ?></span></p>
                                <p><a class="download" href="<?= $doc['url'] ?>"><i class="fa fa-floppy-o"></i><? __dbt("Скачати ") ?></a></p>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>