<div class="content">
    <div class="container-fluid profile backgroundContainer">
        <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 160px" src="http://placehold.it/160x355" />-->
<!--            <img style="position: absolute; right: -175px; top: 160px" src="http://placehold.it/160x355" />-->
        </div>
        <div class="container center profile">
            <div class="newsMainHolder">
                <div class="row">
                    <div class="col-lg-9 col-md-12">
                        <?php foreach ($news as $value) { ?>
                        <div class="newsItem row">
<!--                            <div class="col-lg-2 col-md-2 col-sm-2">-->
<!--                                <div class="newsItemDate">-->
<!--                                    <p class="dateNumber">15</p>-->
<!--                                    <p class="dateMonth">січ</p>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <a href="<?= $value['ID']?>-<?= $value['url']?>"><h3><?= $value['title']; ?></h3></a>
                                <p class="timeYear"><?= date('d.m.y G:i:s', strtotime($value['createDate'])); ?></p>
                                <p class="anons"><?= $value['preview']?></p>
                                <div class="clearfix">
                                    <a href="<?= $value['ID']?>-<?= $value['url']?>" class="btn btn-primary pull-right" style="width: 120px; font-size: 12px!important;"><? __dbt("Детальніше...") ?></a>
                                </div>

                            </div>
                            <div class="bottomBorderHolder col-lg-12 col-md-12 col-sm-12"></div>
                        </div>
                        <?php } ?>
                        <nav class="text-center">
                            <? $controller->echoBlock('pagination_news'); ?>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="row">
                            <div class="newsCategoriesMenu col-lg-12 col-md-6 col-sm-6">
                                <h2><? __dbt("Категорії") ?></h2>
                                <ul>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'our-services'])) ?>"><span>&#187;</span><? __dbt("Наші послуги") ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'about'])) ?>"><span>&#187;</span><? __dbt("Про проект") ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'system-description'])) ?>"><span>&#187;</span><? __dbt("Опис системи") ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'public-offer'])) ?>"><span>&#187;</span><? __dbt("Публічна оферта") ?></a></li>
                                    <li><a href="javascript:;" data-toggle="modal" data-target="#feedback"><span>&#187;</span><? __dbt("Зворотній зв'язок") ?></a></li>
                                </ul>
                            </div>
<!--                            <div class="col-lg-12 col-md-6 col-sm-6 popularNewsHolder">-->
<!--                                <h2>Популярні новини</h2>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin1.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>На що звернути увагу при страхуванні</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin2.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>Хто повинен пройти технічний контроль</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row popularNewsItem">-->
<!--                                    <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2">-->
<!--                                        <div class="newsMinImgHolder">-->
<!--                                            <a href="#"><img src="/assets/img/newsMin3.jpg" alt=""/></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">-->
<!--                                        <a href="#"><h3>Чому бояться європротоколу?</h3></a>-->
<!--                                        <p class="popularNewsDate">Січ 12, 2016</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-lg-12 col-md-6 col-sm-6 visible-lg visible-sm">
<!--                                <img src="http://placehold.it/200x200" />-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>