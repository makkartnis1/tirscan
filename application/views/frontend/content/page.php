<div class="content">
    <div class="container-fluid">
        <div class="container center">
            <div class="t-block">
                <div class="t-title">
                    <p><?= $title ?></p>
                </div>
                <div class="t-content">
                    <p><?= $content ?></p>
                </div>
            </div>
        </div>
    </div>
</div>