<?
/**
 * @var $controller Controller_Frontend_Requests
 * @var View $filter_form
 * @var View $templates
 */
?>


<?= $filter_form ?>
    <div class="content">
        <? $controller->echoBlock('transports') ?>
    </div>
    <div class="container center">
        <div class="seo_block_list">
            <?= $controller->seo['content_text']; ?>
        </div>
    </div>

    <div class="modal fade transportOrderModal" id="transportOrderModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                                href="#"></a></span></button>
                    <h4 class="modal-title"><span class="ttl"><? __dbt("Заявка") ?></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 table-section-holder" data-table="here">
                            <?/* Динамічно грзутиься рядок із таблиці */?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><? __dbt("Додаткова інформація про заявку") ?></h3>
                                </div>
                                <div class="col-md-12" data-info >
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            <div class="orderModalMapHolder">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d162757.72582793544!2d30.392609105722805!3d50.402170238254264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2z0JrQuNGX0LIsIDAyMDAw!5e0!3m2!1suk!2sua!4v1453978363646"
                                    width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" id="modal-apply-area">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-message" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #27324b;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span></button>
                    <h4 class="modal-title"><span class="ttl"><? __dbt("Повідомлення менеджеру заявки") ?></span></h4>
                </div>
                <form method="post" id="form-messages">
                    <div class="modal-body">
                        <input type="hidden" id="user_from" value="<?= $controller->user->ID; ?>">
                        <div class="form-title"><? __dbt("Тема повідомлення") ?></div>
                        <div class="form-group">
                            <input type="text"
                                   name="theme"
                                   class="form-control">
                        </div>

                        <div class="form-title"><? __dbt("Повідомлення") ?></div>
                        <div class="form-group">
                            <input type="hidden"
                                   name="user_to">
                        <textarea type="text"
                                  name="message"
                                  id="message-textarea"
                                  class="form-control <?= $controller->classIfError('message', 'complaint') ?>"></textarea>
                        </div>
                        <ul id="error-msg" class="errorText"></ul>

                        <div class="row">
                            <div class="offset-md-7 col-sm-5">
                                <button type="button" id="send-message" class="btn btn-primary"><? __dbt("Відправити") ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script id="template_modalRequestApply" type="text/x-handlebars-template">
        {{#if loaded}}
        <span class="request-text"><? __dbt("Вами вже було відправлено пропозицію по даній заявці") ?></span>
        {{else}}
        {{#if not_mine}}
        <button id="btn-send-application" class="btn btn-primary pull-right" data-request-id="{{request_id}}"><? __dbt("Відправити пропозицію") ?></button>
        {{else}}
        <span class="request-text"><? __dbt("Це заявка розміщена вашою компанією") ?></span>
        {{/if}}
        {{/if}}
    </script>

<? if ($controller->authorized) echo $templates; ?>

<?= View::factory('frontend/content/requests/modal/request-additional-info'); ?>