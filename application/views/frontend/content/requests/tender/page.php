<div class="content">
    <div class="container-fluid transport bottom">
        <div class="container center">
            <div class="tender-box">
                <div class="modal-content">
                    <div class="tender-head">
                        <div class="row">
                            <div class="col-xs-6"><h4 class="title"><span class="ttl">Тендер №125478</span></h4></div>
                            <div class="col-xs-6 text-right"><a href="/ukr/tender"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>повернутися назад</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="thumb-block tender">
                                <div class="thumb-inner-box">
                                    <div class="row">
                                        <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">
                                            <div class="border-bottom-mobile">
                                                <div class="row">
                                                    <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">
                                                        <a href="#" class="tender-link">
                                                            <div class="inner-box green">
                                                                <span class="gray-top">14.12 16:33:34</span>
                                                                <span class="gray-top lh-tender">-</span>
                                                                <span class="gray-top">14.12 16:33:34</span>
                                                                <span class="gray-bottom">RU-UA</span>
                                                                <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /
                                                                <span class="tender-ico">100$</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">
                                                        <div class="box-border">
                                                            <div class="center-inner-block">
                                                                <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>
                                                                <p>Малопольське</p>
                                                            </div>
                                                            <div class="center-inner-block bottom">
                                                                <p>Бокситогорськ, <a href="#" id="q">...</a></p>
                                                                <p>Тарностав-Люб<a href="#">...</a>
                                                                    <a href="#"></a>
                                                                </p>
                                                            </div>
                                                            <i class="fa fa-map-marker fa-2x abs"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">
                                            <div class="row thumb-bottom">
                                                <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">
                                                    <div class="right-inner">
                                                        <p class="yellow-span"></p>
                                                        <div class="descr-tr">
                                                            <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>
                                                            <p class="count">Кількість автомобілів: <b>20</b></p>
                                                            <p class="middle-inner">2.42 х 3 х 13.6</p>
                                                            <p class="bottom-inner">22 т / 86 м3</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">
                                                    <div class="right-inner-last">
                                                        <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)
                                                            <i class="fa fa-envelope fa-2x pull-right"></i>
                                                            <span class="radio-online active pull-right"></span>
                                                            <i class="fa fa-asterisk client-abs"></i>
                                                        </a>

                                                        <p class="p-top last">Михаил</p>
                                                        <p class="p-middle last">+7(912)744-02-93</p>
                                                        <div class="p-rating last">
                                                            <div class="stars">
                                                                <i class="fa fa-star active"></i>
                                                                <i class="fa fa-star active"></i>
                                                                <i class="fa fa-star active"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <span class="ratings">3.38</span>
                                                            </div>
                                                            <div class="comments">
                                                                <i class="fa fa-comment-o fa-flip-horizontal"></i> 12
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body tender">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-xs-8 container-helper">
                                        <h3>Критерії для участі у тендері</h3>
                                        <ul class="tender-list">
                                            <li><i class="fa fa-check" aria-hidden="true"></i> Термін діяльності компанії на ринку: <b>3 роки</b></li>
                                            <li><i class="fa fa-check" aria-hidden="true"></i> Мінімально допустимий рейтинг компаній: <b>не вказаний.</b></li>
                                            <li><i class="fa fa-check" aria-hidden="true"></i> Компанії лише з підтвердженими платіжними реквізитами</li>
                                            <li><i class="fa fa-check" aria-hidden="true"></i> Дозволено приймати участь експедиторам</li>
                                            <li><i class="fa fa-check" aria-hidden="true"></i> Дозволено приймати участь компаніям, що присутня у чорних списках</li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-4  container-helper bg-grey">
                                        <h3>Поточна ставка</h3>
                                        <ul class="tender-list cash">
                                            <li>1400 <i class="fa fa-usd" aria-hidden="true"></i></li>
                                        </ul>
                                        <h3>Ваша остання ставка</h3>
                                        <ul class="tender-list cash">
<!--                                            <li>- <i class="fa fa-usd" aria-hidden="true"></i></li>-->
                                            <li>-</li>
                                        </ul>
                                        <h3>До закінчення</h3>
                                        <ul class="tender-list">
                                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> 12 год 49 хв 31сек</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8 container-helper">
                                        <h3>Додаткова інформація</h3>
                                        <p class="descr">Тестова інформація про даний тендер</p>
                                    </div>
                                    <div class="col-xs-4 container-helper bg-grey">
                                        <h3>Зробити ставку</h3>
                                        <ul class="tender-list">
                                            <li> Ваша ставка складатиме: 1300 <i class="fa fa-usd" aria-hidden="true"></i></li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-xs-6 col-xs-offset-6">
                                                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#bet">Прийняти участь</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>