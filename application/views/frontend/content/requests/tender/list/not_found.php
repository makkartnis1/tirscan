<?
/**
 * @var $back_link_url string
 */
?>
<div class="whiteFluidContainer">
    <div class="container-fluid ">
        <div class="container center">
            <div class="t-block">
                <div class="t-title">
                    <p><?= __db('tender.list.notFound') ?></p>
                </div>
                <div class="t-content">
                    <p><?= __db('tender.list.notFoundInfo') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>