<?
/**
 * @var $controller Controller_Frontend_Requests
 * @var $tender_requests array
 */
?>

<section class="container-fluid table-section" style="min-height: 650px">

    <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--        <img style="position: absolute; left: -175px; top: 160px" src="http://placehold.it/160x490" />-->
<!--        <img style="position: absolute; right: -175px; top: 160px" src="http://placehold.it/160x490" />-->
    </div>

    <div class="container center table-responsive">
        <section class="table-section-holder">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                    <tr>
                        <th><? __dbt('Дата') ?></th>
                        <th><? __dbt('Звідки') ?></th>
                        <th><? __dbt('Куди') ?></th>
                        <th><? __dbt('Тип вантажу') ?></th>
                        <th><? __dbt('Вага і об\'єм') ?></th>
                        <th><? __dbt('Початкова ціна') ?></th>
                        <th><? __dbt('Ставки') ?></th>
                        <th><? __dbt('Компанія') ?></th>
                        <th><? __dbt('Статус') ?></th>
<!--                        <th>--><?//     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Дії';     $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/ ?><!--</th>-->
                    </tr>
                </thead>
                <tbody>
                <? foreach ($tender_requests as $request) { ?>
                    <?
                    __db('tender.startingAt');__db('tender.stopped');
                    switch ($request['status']) {
                        case 'active':
                            $status_text = __db('tender.endingAt') . ' <br>' . $request['datetimeToTender'];
                            $status_class = 'success';
                            break;
                        case 'inactive':
                            $status_text = __db('tender.startingAt') . ' <br>' . $request['datetimeFromTender'];
                            $status_class = '';
                            break;
                        case 'stopped':
                            $status_text = __db('tender.stopped');
                            $status_class = 'danger';
                            break;
                    }
                    ?>
                    <tr data-id="<?= $request['ID'] ?>" class="<?= $status_class ?>">
                        <? $link = Route::url('frontend_site_tender', $controller->getRequestParams(['id' => $request['ID']])); ?>
                        <td>
                            <a href="<?= $link ?>"><?= $request['dateFromLoad'] . (($request['dateToLoad'] != null) ? ' - ' . $request['dateToLoad'] : '') ?></a>
                        </td>
                        <td class="from-country"><a href="<?= $link ?>"><?= $request['from_name'] ?> (<?= $request['from_country'] ?>)</a></td>
                        <td class="to-country"><a href="<?= $link ?>"><?= $request['to_name'] ?> (<?= $request['to_country'] ?>)</a></td>
                        <td>
                            <a href="<?= $link ?>"><?= $request['cargoType'] ?></a>
                        </td>
                        <td>
                            <a href="<?= $link ?>"><?= Frontend_Helper_String::none_if_null($request['weight'], (float) $request['weight'] . ' т') . ' / ' . Frontend_Helper_String::none_if_null($request['volume'], (float) $request['volume'] . ' м. куб.') ?></a>
                        </td>
                        <td>
                            <a href="<?= $link ?>">
                            <? if ($request['priceHide'] == 'yes') { echo 'приховано'; }
                            else {
                                echo (float) $request['price'] . ' ' . $request['priceCurrency'];
                            } ?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= $link ?>"><?= Frontend_Helper_String::none_if_null($request['bid_count'], $request['bid_count']) ?></a>
                        </td>
                        <td class="contact" data-user-id="<?= $request['userID'] ?>" data-id="<?= $request['ID'] ?>" data-click="modal-message">
                            <? if ($controller->authorized) { ?>
                            <a href="<?= Route::url('frontend_site_company_page', $controller->getRequestParams(['company_url_name' => $request['companyID'] . '-company' ])) ?>"><?= Frontend_Helper_String::substr_if_more(htmlspecialchars($request['company_name']), 26) ?>
                            <br><?= $request['phoneCode'] . $request['phone'] ?>
                            <? }
                            else { ?>
                                <? __dbt('Дані про компанію<br>приховано') ?>
                            <? } ?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= $link ?>"><?= $status_text ?></a>
                        </td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        </section>
    </div>
</section>

<? $controller->echoBlock('pagination_tender') ?>