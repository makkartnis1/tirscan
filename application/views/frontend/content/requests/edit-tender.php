<div class="content">
<div class="container-fluid transport bottom">
<div class="container center">
<div class="tender-box">
<div class="modal-content">
<div class="tender-head">
    <div class="row">
        <div class="col-xs-6">
            <h4 class="title"><span class="ttl"><?=__db('tenders.page.edit.title')?> <?=$title?></span></h4></div>
        <div class="col-xs-6 text-right"><a href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><?=__db('tenders.page.back')?></a></div>
    </div>
</div>
<div class="modal-body tender add">
<form class="apps-red" id="add-request-form" action="" method="post">
<div class="row transparent">
    <div class="col-md-12">
        <div class="content addTransportContent cargoContent">
            <div class="greyFluidContainer">
                <div class="container-fluid ">
                    <div class=" center ">
                        <label>
                            <span class="req_field">*</span> - Обов'язкові до заповнення поля <?=__db('tenders.page.required')?>
                        </label>
                        <br>
                        <label>
                            <span class="req_field_options">*</span> - Обов'язкове до заповнення одне із полів <?=__db('tenders.page.required_options')?>
                        </label>
                        <div class="formHolder">
                            <div class="row">
                                <div class="form-group uploadDate col-lg-6 col-md-6">
                                    <label for="dateFromLoad"><? __dbt("Дата завантаження від ") ?><?=__db('tenders.page.date_load_from')?><span class="req_field">*</span></label>
                                    <input type="text" name="dateFromLoad" class="form-control datepicker">
                                </div>
                                <div class="form-group uploadDate col-lg-6 col-md-6">
                                    <label for="dateToLoad"><? __dbt("Дата завантаження до ") ?><?=__db('tenders.page.date_load_to')?><span class="req_field">*</span></label>
                                    <input type="text" name="dateToLoad" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group uploadPlace col-lg-6 col-md-6">
                                    <label for="startPointTender"><? __dbt("Місце завантаження ") ?><?=__db('tenders.page.place_load')?><span class="req_field">*</span></label>
                                    <div id="start">
                                        <div class="uploadPlaceInputHolder uploadPlaceInputHolderFrom">
                                            <input type="text" class="form-control startPointTender" id="startPointTender1" name="load_1" placeholder="Пункт завантаження <?=__db('tenders.page.load_point')?>">
                                            <input type="hidden" name="load_place[]">
                                        </div>
                                    </div>
                                    <p><a id="addLoadPlace" href="javascript:;"><? __dbt("Додати місце завантаження ") ?><?=__db('tenders.page.add_place_load')?></a></p>
                                </div>
                                <div class="form-group uploadPlace col-lg-6 col-md-6">
                                    <label for="endPointTender"><? __dbt("Місце розвантаження ") ?><?=__db('tenders.page.place_unload')?><span class="req_field">*</span></label>
                                    <div id="end">
                                        <div class="uploadPlaceInputHolder">
                                            <input type="text" class="form-control endPointTender" id="endPointTender1" name="unload_1" placeholder="Пункт розвантаження <?=__db('tenders.page.unload_point')?>">
                                            <input type="hidden" name="unload_place[]">
                                        </div>
                                    </div>
                                    <p><a id="addUnloadPlace" href="javascript:;"><? __dbt("Додати місце розвантаження ") ?><?=__db('tenders.page.add_place_unload')?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whiteFluidContainer">
                <div class="container-fluid ">
                    <div class=" center ">
                        <div class="formHolder cargoType">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="typeCargo"><? __dbt("Тип вантажу ") ?><?=__db('tenders.page.cargo_type')?></label>
                                        <input type="text" name="typeCargo" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="label_block_validation" for="weightVolumeCargo"></label>
                                        <label for="weightCargo"><? __dbt("Вага вантажу ") ?><?=__db('tenders.page.weight_cargo')?><span class="req_field_options">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control weightVolumeCargo" name="weightCargo">
                                            <div class="input-group-addon"><? __dbt("Т") ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="volumeCargo"><? __dbt("Об'єм вантажу ") ?><?=__db('tenders.page.volume_cargo')?><span class="req_field_options">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control weightVolumeCargo" name="volumeCargo">
                                            <div class="input-group-addon"><? __dbt("М") ?><sup>3</sup></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="carType"><? __dbt("Тип транспорту ") ?><?=__db('tenders.page.transport_type')?></label>
                                        <select name="carType" class="form-control" type="text">
                                            <option value="">- не вказано <?=__db('tenders.page.not_specified')?> -</option>
                                            <? foreach ($car_types as $type): ?>
                                                <option value="<?= $type->ID ?>"><?= __db($type->localeName) ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="truckQnt"><? __dbt("Кількість машин ") ?><?=__db('tenders.page.count_car')?></label>
                                        <input type="number" name="truckQnt" class="form-control" min="1" max="1000"/>
                                    </div>
                                    <div class="cargoMeasures">
                                        <label><? __dbt("Габарити транспортного засобу ") ?><?=__db('tenders.page.dimensions_car')?></label>
                                        <div class="measuresInput">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="carLength" placeholder="Довжина <?=__db('tenders.page.length')?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                        <div class="measuresInput">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="carWidth" placeholder="Ширина <?=__db('tenders.page.width')?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                        <div class="measuresInput">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="carHeight" placeholder="Висота <?=__db('tenders.page.height')?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="line clearfix">
                                    <div class="col-md-12">
                                        <p class="additionalInfoHeading"><? __dbt("Додаткова інформація ") ?><?=__db('tenders.page.specify')?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row transparent">
    <div class="col-md-12">
        <div class="content addTransportContent">
            <div class="greyFluidContainer">
                <div class="container-fluid">
                    <div class="center">
                        <div class="formHolder">
                            <div class="form-group row">
                                <div class="col-xs-4">
                                    <label for="dateFromTender"><? __dbt("Дата початку торгів ") ?><?=__db('tenders.page.date_from_tender')?><span class="req_field">*</span></label>
                                    <input type="text" name="dateFromTender" class="form-control datepicker">
                                </div>
                                <div class="col-xs-4">
                                    <label for="timeFromTender"><? __dbt("Час початку торгів ") ?><?=__db('tenders.page.time_from_tender')?></label>
                                    <input type="time" name="timeFromTender" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4">
                                    <label for="dateToTender"><? __dbt("Дата закінчення торгів ") ?><?=__db('tenders.page.date_to_tender')?><span class="req_field">*</span></label>
                                    <input type="text" name="dateToTender" class="form-control datepicker">
                                </div>
                                <div class="col-xs-4">
                                    <label for="timeToTender"><? __dbt("Час закінчення торгів ") ?><?=__db('tenders.page.time_to_tender')?></label>
                                    <input type="time" name="timeToTender" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="startPrice"><? __dbt("Рекомендована ціна ") ?><?=__db('tenders.page.recommended_price')?><span class="req_field">*</span></label>
                                    <input type="number" name="startPrice" class="form-control" min="50" max="100000">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="currency"><? __dbt("Валюта ") ?><?=__db('tenders.page.currency')?></label>
                                    <select name="currency" class="form-control">
                                        <? foreach ($currencies as $curr): ?>
                                            <option value="<?= $curr->ID ?>"><?= $curr->name ?></option>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group check-block">
                                        <input type="checkbox" name="priceHide">
                                        <label for="priceHide"><? __dbt("Приховати рекомендовану ціну ") ?><?=__db('tenders.page.hide_price')?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="companyYears">Tермін діяльності компанії на ринку <?=__db('tenders.page.term_company')?></label>
                                    <select name="companyYears" class="form-control">
                                        <option>-не вказано <?=__db('tenders.page.not_specified')?>-</option>
                                        <option value="3"><? __dbt("від 3 років ") ?><?=__db('tenders.page.from[:count:]year',['[:count:]'=>3])?></option>
                                        <option value="5"><? __dbt("від 5 років ") ?><?=__db('tenders.page.from[:count:]year',['[:count:]'=>5])?></option>
                                        <option value="10"><? __dbt("від 10 років ") ?><?=__db('tenders.page.from[:count:]years',['[:count:]'=>10])?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <input type="checkbox" name="filterCompanyYears">
                                    <label for="filterCompanyYears"><? __dbt("Дозволено брати участь компаніям лише з підтвердженими платіжними реквізитами ") ?><?=__db('tenders.page.filter.company')?></label>
                                </div>
                                <div class="form-group col-lg-12">
                                    <input type="checkbox" name="filterForwarder">
                                    <label for="filterForwarder"><? __dbt("Дозволено брати участь експедиторам ") ?><?=__db('tenders.page.filter.forwarder')?></label>
                                </div>
                                <div class="form-group col-lg-12">
                                    <input type="checkbox" name="filterPartners">
                                    <label for="filterPartners"><? __dbt("Дозволено брати участь лише партнерам ") ?><?=__db('tenders.page.filter.partners')?>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="whiteFluidContainer">
                <div class="container-fluid ">
                    <div class=" center ">
                        <div class="formHolder">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group rulesAccept">
                                        <div class="checkbox">
                                            <input type="checkbox" name="rules" checked>
                                            <label for="rules"><? __dbt("Я ознайомився і згідний з ") ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary"><b><? __dbt("ЗБЕРЕГТИ") ?></b></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade additionalInfoModal" id="additionalInfoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Додаткова інформація") ?></span></h4>
            </div>
            <div class="modal-body">
                <h2><? __dbt("Відмітьте необхідні поля") ?></h2>
                <div class="additionalInfoItem">
                    <h3><? __dbt("Документи") ?></h3>
                    <div class="row">
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="doc_TIR" type="checkbox">TIR
                            </label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="doc_CMR" type="checkbox">CMR
                            </label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="doc_T1" type="checkbox">T1
                            </label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="doc_sanpassport" type="checkbox"><? __dbt("Санпаспорт") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="doc_sanbook" type="checkbox"><? __dbt("Санкнижка") ?></label>
                        </div>
                    </div>
                </div>
                <div class="additionalInfoItem">
                    <h3><? __dbt("Завантаження") ?></h3>
                    <div class="row">
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="load_side" type="checkbox"><? __dbt("Збоку") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="load_top" type="checkbox"><? __dbt("Зверху") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="load_behind" type="checkbox"><? __dbt("Ззаду") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input value="load_tent" type="checkbox"><? __dbt("Розтентування") ?></label>
                        </div>
                    </div>
                </div>
                <div class="additionalInfoItem">
                    <h3><? __dbt("Умови") ?></h3>
                    <div class="row">
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="cond_plomb" type="checkbox"><? __dbt("Пломба") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="cond_reload" type="checkbox"><? __dbt("Довантаження") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="cond_belts" type="checkbox"><? __dbt("Ремені") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                            <label>
                                <input name="cond_removable_stands" type="checkbox"><? __dbt("З'ємні стійки") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                            <label>
                                <input name="cond_bort" type="checkbox"><? __dbt("Жорсткий борт") ?></label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                            <label>
                                <input name="cond_collectable" type="checkbox"><? __dbt("Збірний вантаж") ?></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                            <label>
                                <input name="t" type="checkbox">t&#176;
                                <input name="t_amount" type="text" class="form-control additionalTextInfo"/>
                            </label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                            <label>
                                <input name="pallets" type="checkbox"><? __dbt("Палети") ?><input name="pallets_amount" type="text" class="form-control additionalTextInfo"/>
                            </label>
                        </div>
                        <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                            <label>
                                <input name="ADR" type="checkbox">ADR
                                <input name="ADR_amount" type="text" class="form-control additionalTextInfo"/>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3><? __dbt("Примітка") ?></h3>
                        <textarea name="info" class="form-control" placeholder="" rows="5"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary pull-right additionalInfoButton" type="button"><? __dbt("Зберегти") ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


</form>
</div>
</div>
</div>
</div>
</div>
</div>


<!--end-->
<!--additional info modal window-->

<?= $init_js_view ?>
