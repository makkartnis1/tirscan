<?
/**
 * @var $controller Controller_Frontend_Requests
 * @var $car_types Model_Frontend_User_Car_Type
 * @var $phone_codes Model_Frontend_Phonecodes
 * @var $currencies Model_Frontend_Request_Price_Currency
 * @var $user_cars Model_Frontend_User_Car
 * @var $post array
 * @var $errors array
 * @var View $services
 */
?>

<form id="add-request-form" action="" method="post">

<div class="content addTransportContent">
    <div class="greyFluidContainer">
        <div class="container-fluid ">
            <div class="container center">
                <div class="formHolder">
                        <label for="uploadBeginDate"><? __dbt("Завантаження:") ?></label>
                        <div class="form-group uploadDate">
                            <input type="text" name="date_from" value="<?= Arr::get($post, 'date_from', '') ?>" class="form-control datepicker" placeholder="Дата з">
                            <input type="text" name="date_to" value="<?= Arr::get($post, 'date_to', '') ?>" class="form-control datepicker" placeholder="Дата по">
                        </div>
                        <div class="form-group uploadPlace col-lg-6 col-md-6">
                            <label><? __dbt("Місце завантаження:") ?></label>
                            <div class="uploadPlaceInputHolder">
                                <input type="text" class="form-control searchGeo startPointCargo" id="startPointCargo1" name="load_1" value="<?= Arr::get($post, 'load_1', '') ?>" placeholder="Пункт (країна) завантаження">
                                <input type="hidden" value="<?= htmlspecialchars(Arr::get($post, 'load_place', '')) ?>" name="load_place">
                            </div>
                            <?
                            $i = 1;
                            foreach (Arr::get($post, 'load_places', []) as $key => $place) {
                                $i++;
                            ?>
                                <div class="uploadPlaceInputHolder">
                                    <input type="text" class="form-control searchGeo startPointCargo" value="<?= Arr::get($post, 'load_' . $i ) ?>" id="startPointCargo<?= $i ?>" name="load_<?= $i ?>" placeholder="Пункт (країна) розвантаження" autocomplete="off">
                                    <input type="hidden" name="load_places[<?= $i ?>]" value="<?= htmlspecialchars($place) ?>">
                                    <a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>
                                </div>
                            <? } ?>
                            <p><a id="addLoadPlace" href="javascript:;"><? __dbt("Додати місце завантаження") ?></a></p>
                        </div>
                        <div class="form-group uploadPlace col-lg-6 col-md-6">
                            <label><? __dbt("Місце розвантаження:") ?></label>
                            <div class="uploadPlaceInputHolder">
                                <input type="text" class="form-control searchGeo endPointCargo" id="endPointCargo1" name="unload_1" value="<?= Arr::get($post, 'unload_1', '') ?>" placeholder="Пункт (країна) розвантаження">
                                <input type="hidden" name="unload_place" value="<?= Arr::get($post, 'unload_place', '') ?>">
                            </div>
                            <?
                            $i = 1;
                            foreach (Arr::get($post, 'unload_places', []) as $place) {
                            $i++;
                            ?>
                                <div class="uploadPlaceInputHolder">
                                    <input type="text" class="form-control searchGeo endPointCargo" value="<?= Arr::get($post, 'unload_' . $i ) ?>" id="endPointCargo<?= $i ?>" name="unload_<?= $i ?>" placeholder="Пункт (країна) розвантаження" autocomplete="off">
                                    <input type="hidden" name="unload_places[<?= $i ?>]" value="<?= htmlspecialchars($place) ?>">
                                    <a href="javascript:;" class="deletePlace"><i class="fa fa-times"></i></a>
                                </div>
                            <? } ?>
                            <p><a id="addUnloadPlace" href="javascript:;"><? __dbt("Додати місце розвантаження") ?></a></p>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="whiteFluidContainer">
        <div class="container-fluid ">
            <div class="container center">
                <? if ($controller->authorized && (count($user_cars) > 0) ): ?>
                <div class="row">
                    <div class="col-md-4">
                        <div type="button" class="btn btn-primary addTransportExists gradient-top" data-toggle="modal" data-target="#addExistingTransport"><b><? __dbt("Додати вже існуючий транспорт") ?></b></div>
                        <input type="hidden" name="existing_car_id" value="">
                    </div>
                </div>
                <? endif; ?>
                <div class="formHolder" id="formSelectedCar" style="display: none;"><? __dbt("Обрана машина: ") ?><span id="textSelectedCar"></span> <a href="javascript:;" id="linkRemoveSelectedCar">[X]</a>
                </div>
                <div class="formHolder" id="formAddCar">
                        <div class="form-group truckType col-lg-6 col-md-6">
                            <h2><? __dbt("Відомості про автомобіль:") ?></h2>
                            <label for="carType"><? __dbt("Тип транспорту") ?></label>
                            <select name="car_type" class="form-control" type="text" id="carType">
                                <? $car_type = Arr::get($post, 'car_type', 1); ?>
                                <? foreach ($car_types as $type): ?>
                                    <option value="<?= $type->ID ?>" <?= ($car_type == $type->ID) ? 'checked' : '' ?>><?= __db($type->localeName) ?></option>
                                <? endforeach; ?>
                            </select>
                            <div class="radioTruck">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="car_body_type" id="carTypeRadio1" value="vantazhivka" checked>
                                        <span><? __dbt("Вантажівка") ?></span><span class="carIconSm"></span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="car_body_type" id="carTypeRadio2" value="napiv prychip">
                                        <span><? __dbt("Напівпричіп") ?></span><span class="carIconMd"></span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="car_body_type" id="carTypeRadio3" value="z prychipom">
                                        <span><? __dbt("З причепом") ?></span><span class="carIconLg"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group truckType col-lg-6 col-md-6">
                            <h2><? __dbt("Характеристики транспортного засобу") ?></h2>
                            <div class="row truckWeight">
                                <label for="maxWeight" class="col-lg-5"><? __dbt("Вантажопідйомність") ?></label>
                                <div class="col-lg-7">
                                    <div class="input-group <?= $controller->classIfError('liftingCapacity', 'car') ?>">
                                        <input type="text" name="weight" value="<?= Arr::get($post, 'weight', '') ?>" class="form-control" id="maxWeight">
                                        <div class="input-group-addon"><? __dbt("Т") ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row truckContent">
                                <label for="maxContent" class="col-lg-5"><? __dbt("Об'єм") ?></label>
                                <div class="col-lg-7">
                                    <div class="input-group <?= $controller->classIfError('volume', 'car') ?>">
                                        <input type="text" name="volume" value="<?= Arr::get($post, 'volume', '') ?>" class="form-control" id="maxContent">
                                        <div class="input-group-addon"><? __dbt("М") ?><sup>3</sup></div>
                                    </div>
                                </div>
                            </div>
                            <div class="truckMeasures">
                                <label ><? __dbt("Габарити транспортного засобу") ?></label>
                                <div class="measuresInput">
                                    <div class="input-group <?= $controller->classIfError('sizeZ', 'car') ?>">
                                        <input type="text" name="size_Z" value="<?= Arr::get($post, 'size_Z', '') ?>" class="form-control" id="maxLength" placeholder="Довжина">
                                        <div class="input-group-addon"><? __dbt("М") ?></div>
                                    </div>
                                </div><div class="measuresInput">
                                    <div class="input-group <?= $controller->classIfError('sizeX', 'car') ?>">
                                        <input type="text" name="size_X" value="<?= Arr::get($post, 'size_X', '') ?>" class="form-control" id="maxWidth" placeholder="Ширина">
                                        <div class="input-group-addon"><? __dbt("М") ?></div>
                                    </div>
                                </div><div class="measuresInput">
                                    <div class="input-group <?= $controller->classIfError('sizeY', 'car') ?>">
                                        <input type="text" name="size_Y" value="<?= Arr::get($post, 'size_Y', '') ?>" class="form-control" id="maxHeight" placeholder="Висота">
                                        <div class="input-group-addon"><? __dbt("М") ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row line">
                    <div class="col-md-12">
                        <p><? __dbt("Вартість перевезення: ") ?><a href="javascript:;" data-toggle="modal" data-target="#changePriceModal">вказати</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? if (! $controller->authorized): ?>
    <div class="greyFluidContainer">
        <div class="container-fluid ">
            <div class="container center ">
                <div class="formHolder contactInfoHolder">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="companyName"><? __dbt("Назва компанії:") ?></label>
                                    <input type="text" name="company_name" value="<?= Arr::get($post, 'company_name', '') ?>" class="form-control" placeholder="" id="companyName"/>
                                </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="companyEmail">E-mail:</label>
                                    <input type="email" name="company_email" value="<?= Arr::get($post, 'company_email', '') ?>" class="form-control" placeholder="" id="companyEmail"/>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="contactName"><? __dbt("Контактна особа:") ?></label>
                                    <input type="text" name="company_pib" value="<?= Arr::get($post, 'company_pib', '') ?>" class="form-control" placeholder="Ім'я або ПІБ" id="contactName"/>
                                </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="companyPhone"><? __dbt("Телефон:") ?></label>
                                    <select name="company_phone_code" id="telCode" class="phoneCodeNumber">
                                        <? foreach ($phone_codes as $code): ?>
                                            <option value="<?= $code->ID ?>"><?= $code->code ?></option>
                                        <? endforeach; ?>
                                    </select>
                                    <input type="tel" name="company_phone" value="<?= Arr::get($post, 'company_phone', '') ?>" class="form-control pull-right" placeholder="Номер" id="companyPhone"/>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? endif; ?>
    <div class="container-fluid registration second bottom">
        <div class="container center">
            <div class="row line">
                <div class="col-md-12">
                    <p><? __dbt("Додаткова інформація: ") ?><a href="javascript:;" data-toggle="modal" data-target="#additionalInfoModal">вказати</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                        <div class="form-group rulesAccept">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms_accepted" checked><? __dbt("Я ознайомився і згідний з ") ?>
                                    <a href="#"><? __dbt("правилами") ?></a>
                                </label>
                            </div>
                        </div>
                </div>
                <div class="col-md-3">
                    <? if ($existing_id == null) { ?>
                        <div type="button" id="add-request-submit" class="btn btn-primary"><b><? __dbt("РОЗМІСТИТИ") ?></b></div>
                    <? } else { ?>
                        <div type="button" id="add-request-submit" class="btn btn-primary"><b><? __dbt("ЗБЕРЕГТИ") ?></b></div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
    <? if ($controller->authorized && ($existing_id == null)) { echo $services; } ?>
</div>

<!--Price modal window-->
<div class="modal fade changePriceModal" id="changePriceModal" tabindex="-1" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt('Вартість перевезення') ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <div class="form-group price">
                            <input name="price" type="number" value="<?= (Arr::get($post, 'price') !== null) ? Arr::get($post, 'price') : '0' ?>" class="form-control"/> грн.
                            <!--                                    <select name="currency" class="form-control col-md-6">-->
                            <!--                                        --><?// foreach ($currencies as $curr): ?>
                            <!--                                            <option value="--><?////= $curr->ID ?><!--<!--">--><?////= __db($curr->localeName) ?><!--<!--</option>-->
                            <!--                                            <option value="--><?//= $curr->ID ?><!--">--><?//= $curr->name ?><!--</option>-->
                            <!--                                        --><?// endforeach; ?>
                            <!--                                    </select>-->
                        </div>
                        <div class="row payFormDiv">
                            <div class="col-lg-3 col-md-4 col-sm-4 payFormDivItem">
                                <div class="radio">
                                    <label>
                                        <input value="b/g" type="radio" name="payForm" <?= ((Arr::get($post, 'payForm') == 'b/g') || (Arr::get($post, 'payForm') == null)) ? 'checked' : '' ?>  />
                                        <span><? __dbt('б/г') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 payFormDivItem">
                                <div class="radio">
                                    <label>
                                        <input value="gotivka" type="radio" name="payForm" <?= (Arr::get($post, 'payForm') == 'gotivka') ? 'checked' : '' ?> />
                                        <span><? __dbt('готівка') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 payFormDivItem">
                                <div class="radio">
                                    <label>
                                        <input value="combin" type="radio" name="payForm" <?= (Arr::get($post, 'payForm') == 'combin') ? 'checked' : '' ?> />
                                        <span><? __dbt('комбінована') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 payFormDivItem">
                                <div class="radio">
                                    <label>
                                        <input value="card" type="radio" name="payForm" <?= (Arr::get($post, 'payForm') == 'card') ? 'checked' : '' ?> />
                                        <span><? __dbt('на картку') ?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row payFormDiv">
                            <div class="col-lg-3 col-md-7 col-sm-7 payFormDivItem">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="pdv" value="yes" <?= (Arr::get($post, 'pdv') == 'yes') ? 'checked' : '' ?>> <span><? __dbt('ПДВ') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-5 payFormDivItem">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="on_unload" value="yes" <?= (Arr::get($post, 'on_unload') == 'yes') ? 'checked' : '' ?>> <span><? __dbt('При розвантаженні') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-7 col-sm-7 payFormDivItem">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="on_load" value="yes" <?= (Arr::get($post, 'on_load') == 'yes') ? 'checked' : '' ?>> <span><? __dbt('При завантаженні') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 payFormDivItem">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="advanced_payment" value="yes" <?= (Arr::get($post, 'advanced_payment') == 'yes') ? 'checked' : '' ?>> <span><? __dbt('Передоплата') ?></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8 payFormDivItem">
                                <div class="input-group prePaid">
                                    <input type="text" name="advanced_payment_amount" class="form-control" value="<?= (Arr::get($post, 'advanced_payment_amount') !== null) ? Arr::get($post, 'advanced_payment_amount') : '' ?>">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modalButtonHolder text-right">
                    <a class="btn btn-primary" data-dismiss="modal">ОК</a>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!--end-->
<!--additional info modal window-->
<div class="modal fade additionalInfoModal" id="additionalInfoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Додаткова інформація") ?></span></h4>
            </div>
            <div class="modal-body">
                <h2><? __dbt("Відмітьте необхідні поля") ?></h2>
                    <div class="additionalInfoItem">
                        <h3><? __dbt("Документи") ?></h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="doc_TIR" type="checkbox" value="yes" <?= (Arr::get($post, 'doc_TIR') == 'yes') ? 'checked' : '' ?>>TIR
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="doc_CMR" type="checkbox" value="yes" <?= (Arr::get($post, 'doc_CMR') == 'yes') ? 'checked' : '' ?>>CMR
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="doc_T1" type="checkbox" value="yes" <?= (Arr::get($post, 'doc_T1') == 'yes') ? 'checked' : '' ?>>T1
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="doc_sanpassport" type="checkbox" value="yes" <?= (Arr::get($post, 'doc_sanpassport') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Санпаспорт';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="doc_sanbook" type="checkbox" value="yes" <?= (Arr::get($post, 'doc_sanbook') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Санкнижка';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                        </div>
                    </div>
                    <div class="additionalInfoItem">
                        <h3><? __dbt("Завантаження") ?></h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="load_side" type="checkbox" value="yes" <?= (Arr::get($post, 'load_side') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Збоку';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="load_top" type="checkbox" value="yes" <?= (Arr::get($post, 'load_top') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Зверху';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="load_behind" type="checkbox" value="yes" <?= (Arr::get($post, 'load_behind') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Ззаду';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="load_tent" type="checkbox" value="yes" <?= (Arr::get($post, 'load_tent') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Розтентування';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                        </div>
                    </div>
                    <div class="additionalInfoItem">
                        <h3><? __dbt("Умови") ?></h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="cond_plomb" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_plomb') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Пломба';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="cond_reload" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_reload') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Довантаження';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="cond_belts" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_belts') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Ремені';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input name="cond_removable_stands" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_removable_stands') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'З\'ємні стійки';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                                <label>
                                    <input name="cond_bort" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_bort') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Жорсткий борт';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                                <label>
                                    <input name="cond_collectable" type="checkbox" value="yes" <?= (Arr::get($post, 'cond_collectable') == 'yes') ? 'checked' : '' ?>><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Збірний вантаж';      $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input name="pallets" type="checkbox" value="yes" <?= (Arr::get($post, 'pallets', 'no') == 'yes') ? 'checked' : '' ?>><? __dbt("Палети") ?>
                                    <input name="pallets_amount" type="number" class="form-control additionalTextInfo" value="<?= Arr::get($post, 'pallets_amount', '') ?>"/>
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input name="ADR" type="checkbox" value="yes" <?= (Arr::get($post, 'ADR', 'no') == 'yes') ? 'checked' : '' ?>>ADR
                                    <input name="ADR_amount" type="number" class="form-control additionalTextInfo" value="<?= Arr::get($post, 'ADR_amount', '') ?>"/>
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input name="t" type="checkbox" value="yes" <?= (Arr::get($post, 't', 'no') == 'yes') ? 'checked' : '' ?>>t&#176;
                                    <input name="t_amount" type="number" class="form-control additionalTextInfo" value="<?= Arr::get($post, 't_amount', '') ?>"/>
                                    <span class="temperatureSeparatorDivider">-</span>
                                    <input name="t_amount_to" type="number" class="form-control additionalTextInfo" value="<?= Arr::get($post, 't_amount_to', '') ?>"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><? __dbt("Примітка") ?></h3>
                            <textarea name="info" class="form-control" placeholder=""><?= Arr::get($post, 'info', '') ?></textarea>
                        </div>
                    </div>
                <div class="modalButtonHolder text-right">
                    <a class="btn btn-primary" data-dismiss="modal">ОК</a>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<? if ($controller->authorized): ?>
    <!--Add existing transport-->
    <div class="modal fade addExistingTransport" id="addExistingTransport" tabindex="-1" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                    <h4 class="modal-title"><span class="ttl"><? __dbt("Додати вже існуючий транспорт") ?></span></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group clearfix">
                        <label for="chooseTransport"><? __dbt("Оберіть автомобіль") ?></label>
                        <div class="dropdown">
                            <select id="select-existing-car" class="form-control">
                            <? foreach ($user_cars as $car): ?>
                                <option value="<?= $car->ID ?>"><?= $car->getTitle() ?></option>
                            <? endforeach; ?>
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary pull-right" id="button-add-extisting-transport"><? __dbt("Додати") ?></button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<? endif; ?>


</form>