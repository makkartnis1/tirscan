<?
/**
 * @var $controller Controller_Frontend_Requests
 * @var $transport_requests array
 * @var $transport_vip array
 */
?>

    <section class="container-fluid table-section" style="min-height: 650px">

        <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 18px" src="http://placehold.it/160x600" />-->
<!--            <img style="position: absolute; right: -175px; top: 18px" src="http://placehold.it/160x600" />-->
        </div>
        <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 640px" src="http://placehold.it/160x400" />-->
<!--            <img style="position: absolute; right: -175px; top: 640px" src="http://placehold.it/160x400" />-->
        </div>

        <div class="container center table-responsive">
            <section class="table-section-holder">
                <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th><? __dbt('Дата') ?></th>
                    <th><? __dbt('Звідки') ?></th>
                    <th><? __dbt('Куди') ?></th>
                    <th class="col-additional-info"><? __dbt('Дод. інформація') ?></th>
                    <th><? __dbt('Вага') ?></th>
                    <th><? __dbt('Кузов') ?></th>
                    <th><? __dbt('Ціна') ?></th>
                    <th><? __dbt('Контакти') ?></th>
                </tr>
                </thead>
                <tbody>
<?
foreach ($transport_vip as $request) { ?>
    <tr data-id="<?= $request['ID'] ?>"
        data-click="showTransportModal"
        data-from-placeid="<?= $request['from_placeID'] ?>"
        data-to-placeid="<?= $request['to_placeID'] ?>"
        data-request-id="<?= $request['ID']?>"
        class="warning cargo-row"
    >
        <td>
            <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
            <? $json = [] ?>
            <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

            <?

            if( !empty($request['_json_advancedPayment']) ){
                $unit = trim($request['_json_advancedPayment']);
            }else{
                $unit = $request['_json_currency_code'];
            }

            $json['price'] = $request['_json_price'].'|'.$unit;

            ?>

            <script type="application/json"><?= json_encode($json) ?></script>
        </td>
        <td class="from-country">
            <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
            <? foreach ($request['from_otherGeo'] as $geo): ?>
                <br><?= $geo['from_name'] ?> (<?= $geo['from_country'] ?>)
            <? endforeach; ?>
        </td>
        <td class="to-country">
            <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
            <? foreach ($request['to_otherGeo'] as $geo): ?>
                <br><?= $geo['to_name'] ?> (<?= $geo['to_country'] ?>)
            <? endforeach; ?>
        </td>
        <td class="col-additional-info">
            <?= Frontend_Helper_String::none_if_null($request['info'], $request['info'])  ?>
        </td>
        <td>
            <?= Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float) $request['car_liftingCapacity'] . ' т') ?>
        </td>
        <td>
            <?= __db($request['car_type_localeName']) ?>
        </td>
        <td>
            <?= Frontend_Helper_String::none_if_null($request['value'],  $request['value'] . ' ' . $request['valueCode']) ?>
        </td>
        <td data-user-id="<?= $request['userID'] ?>" data-id="<?= $request['ID'] ?>" data-click="modal-message">
            <? if ($controller->authorized) { ?>
            <? if ($request['companyTypeID'] == 1) { ?>
                <b class="expeditor-mark" style="color: red;">Е</b>
            <? } ?>
            <a href="<?= Route::url('frontend_site_company_page', $controller->getRequestParams(['company_url_name' => $request['companyID'] . '-company' ])) ?>">
            <?= htmlspecialchars(Frontend_Helper_String::substr_if_more($request['company_name'], 23)) ?>
                <br>
                <?= $request['phoneCode'] . $request['phone'] ?>
            <? }
            else { ?>
                <? __dbt('Дані про компанію<br>приховано') ?>
            <? } ?>
            </a>
            <i class="fa fa-envelope table-ico contact" aria-hidden="true"></i>
        </td>
    </tr>
    <?
} ?>
<!--<tr><td colspan="8" align="center"><img style="margin-top: 4px;" class="visible-lg visible-sm" src="http://placehold.it/728x90" /></td></tr>-->
<? $iter = 0; foreach ($transport_requests as $request) { $iter++;
    if (($iter == 10) && (count($transport_requests) > 15)) { ?>
<!--        <tr><td colspan="8" align="center"><img style="margin-top: 4px;" class="visible-lg visible-sm" src="http://placehold.it/728x90" /></td></tr>-->
    <? }

    ?>

    <tr data-id="<?= $request['ID'] ?>" 
        data-click="showTransportModal" 
        data-from-placeid="<?= $request['from_placeID'] ?>" 
        data-to-placeid="<?= $request['to_placeID'] ?>" 
        data-request-id="<?= $request['ID']?>"
        <?= ($request['isColored'] == 'yes') ? 'class="info cargo-row"' : ((($last_transport_update_time != false) && ($request['createdRaw'] > $last_transport_update_time)) ? 'class="cargo-row danger"' : 'class="cargo-row"') ?>
        >
        <td>
            <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
            <? $json = [] ?>
            <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

            <?

            if( !empty($request['_json_advancedPayment']) ){
                $unit = trim($request['_json_advancedPayment']);
            }else{
                $unit = $request['_json_currency_code'];
            }

            $json['price'] = $request['_json_price'].'|'.$unit;

            ?>

            <script type="application/json"><?= json_encode($json) ?></script>
        </td>
        <td class="from-country">
            <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
            <? foreach ($request['from_otherGeo'] as $geo): ?>
                <br><?= $geo['from_name'] ?> (<?= $geo['from_country'] ?>)
            <? endforeach; ?>
        </td>
        <td class="to-country">
            <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
            <? foreach ($request['to_otherGeo'] as $geo): ?>
                <br><?= $geo['to_name'] ?> (<?= $geo['to_country'] ?>)
            <? endforeach; ?>
        </td>
        <td class="col-additional-info">
            <?= Frontend_Helper_String::none_if_null($request['info'], $request['info'])  ?>
        </td>
        <td>
            <?= Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float) $request['car_liftingCapacity'] . ' т') ?>
        </td>
        <td>
            <?= __db($request['car_type_localeName']) ?>
        </td>
        <td>
            <?= Frontend_Helper_String::none_if_null($request['value'],  $request['value'] . ' ' . $request['valueCode']) ?>
        </td>
        <td data-user-id="<?= $request['userID'] ?>" data-id="<?= $request['ID'] ?>" data-click="modal-message">
            <? if ($controller->authorized) { ?>
            <? if ($request['companyTypeID'] == 1) { ?>
                <b class="expeditor-mark" style="color: red;">Е</b>
            <? } ?>
            <a href="<?= Route::url('frontend_site_company_page', $controller->getRequestParams(['company_url_name' => $request['companyID'] . '-company' ])) ?>">
            <?= htmlspecialchars(Frontend_Helper_String::substr_if_more($request['company_name'], 23)) ?>
                <br>
                <?= $request['phoneCode'] . $request['phone'] ?>
            <? }
            else { ?>
                <? __dbt('Дані про компанію<br>приховано') ?>
            <? } ?>
            </a>
            <i class="fa fa-envelope table-ico contact" aria-hidden="true"></i>
        </td>
    </tr>
    <?
}
?>

                </tbody>
            </table>
        </section>
    </div>
</section>

<? $controller->echoBlock('pagination_transport') ?>