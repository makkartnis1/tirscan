<?
/**
 * @var Controller_Frontend_Requests $controller
 * @var array $query
 * @var Model_Frontend_User_Car_Type[] $car_types
 * @var $stickers
 */
?>

<div class="container-fluid filter-page">
    <div class="container center">
        <section class="filter-section">
            <div class="row filter-line">
                <? if ($controller->user->hasRights('cargo') || !$controller->authorized) { ?>
                <div class="col-sm-3 col-md-3">
                    <a href="<?= $controller->links['cargo_add_page'] ?>" class="btn btn-primary xs-mb"><? __dbt("Додати вантаж") ?></a>
                </div>
                <? } ?>
                <? if ($controller->user->hasRights('transport') || !$controller->authorized) { ?>
                <div class="col-sm-3 col-md-3">
                    <a href="<?= $controller->links['transport_add_page'] ?>" class="btn btn-primary xs-mb"><? __dbt("Додати транспорт") ?></a>
                </div>
                <? } ?>
                <div class="col-sm-3 col-md-3">
                    <? if ($controller->authorized) { ?><a href="javascript:;" class="btn btn-primary xs-mb button-add-template"><i class="fa fa-plus"></i><? __dbt("Додати шаблон") ?></a><? } ?>
                </div>
                <div class="col-sm-3 col-md-3 pull-right">
                    <ul class="nav nav-tabs" role="tablist">
                    <? if ($controller->request->action() == 'cargo') { ?>
                        <li><a href="<?= $controller->links['transport'] . '?' . http_build_query($controller->request->query()) ?>"><? __dbt("Транспорт") ?></a></li>
                        <li class="active"><a href="javascript:;"><? __dbt("Вантажі") ?></a></li>
                    <? } else { ?>
                        <li class="active"><a href="javascript:;"><? __dbt("Транспорт") ?></a></li>
                        <li><a href="<?= $controller->links['cargo'] . '?' . http_build_query($controller->request->query()) ?>"><? __dbt("Вантажі") ?></a></li>
                    <? } ?>
                    </ul>
                </div>
            </div>
            <?= $stickers ?>
            <form id="filter-form" action="" method="get">
                <section class="primary-section">
                    <div class="row in-line">
                        <div class="col-sm-4 col-md-4">
                            <label></label>
                            <input type="text" value="<?= Arr::get($query, 'load', '') ?>" class="form-control searchGeo" name="load" placeholder="<? __dbt('Звідки') ?>">
                            <input type="hidden" value="<?= htmlspecialchars( Arr::get($query, 'load_geo', '') ) ?>" name="load_geo">
                            <a href="javascript:;" id="swag-button-places" class="fa fa-arrows-h" aria-hidden="true" style="position: absolute; left: 98%; top: 19px;"></a>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label></label>
                            <input type="text" value="<?= Arr::get($query, 'unload', '') ?>" class="form-control searchGeo" name="unload" placeholder="<? __dbt('Куди') ?>">
                            <input type="hidden" value="<?= htmlspecialchars( Arr::get($query, 'unload_geo', '') ) ?>" name="unload_geo">
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <label for="car_type"></label>
                            <select name="car_type" id="car_type" class="form-control">
                                <? $car_type = Arr::get($query, 'car_type', ''); ?>
                                <option value="" <?= ($car_type == '') ? 'selected' : '' ?>><? __dbt("Тип транспорту") ?></option>
                                <? foreach ($car_types as $type): ?>
                                    <option value="<?= $type->ID ?>" <?= ($car_type == $type->ID) ? 'selected' : '' ?>><?= __db($type->localeName) ?></option>
                                <? endforeach; ?>
                            </select>
                            <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute;top: 18px;right: 22px;"></i>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <? if ($controller->authorized) { ?><label></label>
                            <a href="javascript:;" id="show-templates" class="btn btn-primary gradient-top fix" data-toggle="modal" data-target="#template-modal"><? __dbt("Шаблони/семафори ") ?><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a><? } ?>
                        </div>
                    </div>
                    <div class="collapse additional-section" id="collapseExample">
                        <div class="well">
                            <div class="row in-line">
                                <div class="col-sm-4 col-md-4">
                                    <label></label>
                                    <div class="input-group">
                                        <input type="number" min="0" name="load_radius" value="<?= Arr::get($query, 'load_radius', '') ?>" class="form-control" id="maxLength" placeholder="<? __dbt('Радіус') ?>">
                                        <div class="input-group-addon"><? __dbt("КМ") ?></div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label></label>
                                    <div class="input-group">
                                        <input type="number" min="0" name="unload_radius" value="<?= Arr::get($query, 'unload_radius', '') ?>" class="form-control" id="maxLength" placeholder="<? __dbt('Радіус') ?>">
                                        <div class="input-group-addon"><? __dbt("КМ") ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-3">
                                            <label><? __dbt("Вага вантажу") ?></label>
                                            <div class="input-group">
                                                <input type="number" min="0" step="0.001" class="form-control" name="weight" value="<?= Arr::get($query, 'weight', '') ?>">
                                                <div class="input-group-addon"><? __dbt("Т") ?></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label><? __dbt("Об'єм вантажу") ?></label>
                                            <div class="input-group">
                                                <input type="number" min="0" step="0.001" class="form-control" value="<?= Arr::get($query, 'volume', '') ?>" name="volume">
                                                <div class="input-group-addon"><? __dbt("М") ?><sup>3</sup></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <label><? __dbt("Габарити т.з.") ?></label>
                                            <div class="input-group">
                                                <input type="number" min="0" step="0.001" class="form-control" id="maxLength" name="size_z" value="<?= Arr::get($query, 'size_z', '') ?>" placeholder="<? __dbt('Довжина') ?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <label></label>
                                            <div class="input-group">
                                                <input type="number" min="0" step="0.001" class="form-control" id="maxWidth" name="size_x" value="<?= Arr::get($query, 'size_x', '') ?>" placeholder="<? __dbt('Ширина') ?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <label></label>
                                            <div class="input-group">
                                                <input type="number" min="0" step="0.001" class="form-control" id="maxHeight" name="size_y" value="<?= Arr::get($query, 'size_y', '') ?>" placeholder="<? __dbt('Висота') ?>">
                                                <div class="input-group-addon"><? __dbt("М") ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h2><? __dbt("Додаткові параметри") ?></h2>
                                    <div class="row">
                                        <div class="checks-box">
                                            <div class="col-sm-3 col-md-3">
                                                <h3><? __dbt("Документи") ?></h3>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="doc_TIR" <?= (Arr::get($query, 'doc_TIR', '') != '') ? 'checked' : '' ?>>TIR
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="doc_CMR" <?= (Arr::get($query, 'doc_CMR', '') != '') ? 'checked' : '' ?>>CMR
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="doc_T1" <?= (Arr::get($query, 'doc_T1', '') != '') ? 'checked' : '' ?>>T1
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="doc_sanpassport" <?= (Arr::get($query, 'doc_sanpassport', '') != '') ? 'checked' : '' ?>><? __dbt("Санпаспорт") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="doc_sanbook" <?= (Arr::get($query, 'doc_sanbook', '') != '') ? 'checked' : '' ?>><? __dbt("Санкнижка") ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checks-box">
                                            <div class="col-sm-3 col-md-3">
                                                <h3><? __dbt("Завантаження") ?></h3>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="load_from_side" <?= (Arr::get($query, 'load_from_side', '') != '') ? 'checked' : '' ?>><? __dbt("Збоку") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="load_from_top" <?= (Arr::get($query, 'load_from_top', '') != '') ? 'checked' : '' ?>><? __dbt("Зверху") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="load_from_behind" <?= (Arr::get($query, 'load_from_behind', '') != '') ? 'checked' : '' ?>><? __dbt("Ззаду") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="load_tent" <?= (Arr::get($query, 'load_tent', '') != '') ? 'checked' : '' ?>><? __dbt("Розтентування") ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checks-box">
                                            <div class="col-sm-3 col-md-3">
                                                <h3><? __dbt("Умови") ?></h3>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_plomb" <?= (Arr::get($query, 'cond_plomb', '') != '') ? 'checked' : '' ?>><? __dbt('Пломба') ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_load" <?= (Arr::get($query, 'cond_load', '') != '') ? 'checked' : '' ?>><? __dbt("Довантаження") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_stripes" <?= (Arr::get($query, 'cond_stripes', '') != '') ? 'checked' : '' ?>><? __dbt("Ремені") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_rem_stands" <?= (Arr::get($query, 'cond_rem_stands', '') != '') ? 'checked' : '' ?>><? __dbt("З'ємні стійки") ?></label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_bort" <?= (Arr::get($query, 'cond_bort', '') != '') ? 'checked' : '' ?>><? __dbt("Жорсткий борт") ?></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-md-3">
                                                <h3><? __dbt("Умови") ?></h3>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_collectable" <?= (Arr::get($query, 'cond_collectable', '') != '') ? 'checked' : '' ?>><? __dbt("Збірний вантаж") ?></label>
                                                </div>
                                                <div class="checkbox checkboxWithInput">
                                                    <label>
                                                        <input type="checkbox" name="cond_t" <?= (Arr::get($query, 'cond_t', '') != '') ? 'checked' : '' ?>>t&#176;
                                                        <input type="number" class="form-control additionalTextInfo" name="cond_t_value" value="<?= Arr::get($query, 'cond_t_value', '') ?>" />
                                                    </label>
                                                </div>
                                                <div class="checkbox checkboxWithInput">
                                                    <label>
                                                        <input type="checkbox" name="cond_pallets" <?= (Arr::get($query, 'cond_pallets', '') != '') ? 'checked' : '' ?>><? __dbt("Палети") ?><input type="number" min="1" class="form-control additionalTextInfo" name="cond_pallets_value" value="<?= Arr::get($query, 'cond_pallets_value', '') ?>" />
                                                    </label>
                                                </div>
                                                <div class="checkbox checkboxWithInput">
                                                    <label>
                                                        <input type="checkbox" name="cond_ADR" <?= (Arr::get($query, 'cond_ADR', '') != '') ? 'checked' : '' ?>>ADR
                                                        <input type="number" min="1" class="form-control additionalTextInfo" name="cond_ADR_value" value="<?= Arr::get($query, 'cond_ADR_value', '') ?>" />
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="cond_only_transport" <?= (Arr::get($query, 'cond_only_transport', '') != '') ? 'checked' : '' ?>><? __dbt("Лише перевізник") ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h2><? __dbt("Вартість перевезення та форма оплати") ?></h2>
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-sm-4 col-md-4">
                                                    <label></label>
                                                    <input type="number" min="0" value="<?= Arr::get($query, 'price_value', '') ?>" name="price_value" class="form-control" placeholder="" />
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <label for="price_currency"></label>
                                                    <select name="price_currency" id="price_currency" class="form-control">
                                                        <option value="1"><? __dbt("грн.") ?></option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3 col-md-3">
                                                    <label></label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="price_before" <?= (Arr::get($query, 'price_before', '') != '') ? 'checked' : '' ?>> <span><? __dbt("Передоплата") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-md-3">
                                                    <label></label>
                                                    <div class="input-group prePaid">
                                                        <input type="number" min="0" class="form-control" value="<?= Arr::get($query, 'price_before_amount', '') ?>" name="price_before_amount">
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-sm-3 col-md-3">
                                                    <label></label>
                                                    <div class="checkbox" style="white-space: nowrap">
                                                        <label>
                                                            <input type="checkbox" name="price_PDV" <?= (Arr::get($query, 'price_PDV', '') != '') ? 'checked' : '' ?>> <span><? __dbt("ПДВ") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4">
                                                    <label></label>
                                                    <div class="checkbox" style="white-space: nowrap">
                                                        <label>
                                                            <input type="checkbox" name="price_on_unload" <?= (Arr::get($query, 'price_on_unload', '') != '') ? 'checked' : '' ?>> <span><? __dbt("При розвантаженні") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4">
                                                    <label></label>
                                                    <div class="checkbox" style="white-space: nowrap">
                                                        <label>
                                                            <input type="checkbox" name="price_on_load" <?= (Arr::get($query, 'price_on_load', '') != '') ? 'checked' : '' ?>> <span><? __dbt("При завантаженні") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 col-md-2">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="pay_form" value="" <?= (Arr::get($query, 'pay_form', '') == '') ? 'checked' : '' ?> />
                                                            <span><? __dbt('(не зазначено)') ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="pay_form" value="b/g" <?= (Arr::get($query, 'pay_form', '') == 'b/g') ? 'checked' : '' ?> />
                                                            <span><? __dbt("б/г") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="pay_form" value="gotivka" <?= (Arr::get($query, 'pay_form', '') == 'gotivka') ? 'checked' : '' ?> />
                                                            <span><? __dbt("готівка") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="pay_form" value="combined" <?= (Arr::get($query, 'pay_form', '') == 'combined') ? 'checked' : '' ?> />
                                                            <span><? __dbt("комбінована") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="pay_form" value="card" <?= (Arr::get($query, 'pay_form', '') == 'card') ? 'checked' : '' ?> />
                                                            <span><? __dbt("на картку") ?></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row in-line">
                        <div class="col-sm-4 col-md-4">
                            <label></label>
                            <input class="form-control datepicker" name="date_from" value="<?= Arr::get($query, 'date_from', '') ?>" placeholder="<? __dbt('Дата від') ?>">
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label></label>
                            <input class="form-control datepicker" name="date_to" value="<?= Arr::get($query, 'date_to', '') ?>" placeholder="<? __dbt('Дата до') ?>">
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <label></label>
                            <a class="additonal-btn crossRotate" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><? __dbt("Розширений пошук ") ?><i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <label class="tr-helper"></label>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i><? __dbt("Знайти") ?></button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>