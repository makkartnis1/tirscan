<div class="content">
    <? $controller->echoBlock('tenders') ?>
</div>
<div class="container center">
    <div class="seo_block_list">
        <?= $controller->seo['content_text']; ?>
    </div>
</div>

<!--<div class="content">-->
<!--<div class="container-fluid transport top-item">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box green">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#" id="q">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport first">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box green">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport second">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box green">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport first">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box yellow">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport second">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box yellow">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport first">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box gray">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span></div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport second">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="thumb-block tender">-->
<!--                    <div class="thumb-inner-box">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="border-bottom-mobile">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-4 col-ms-4 col-sm-4 col-md-4">-->
<!--                                            <a href="/ukr/tender/view/125478" class="tender-link">-->
<!--                                                <div class="inner-box gray">-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-top lh-tender">-</span>-->
<!--                                                    <span class="gray-top">14.12 16:33:34</span>-->
<!--                                                    <span class="gray-bottom">RU-UA</span>-->
<!--                                                    <span class="tender-ico"><i class="fa fa-gavel" aria-hidden="true"></i> 38</span> /-->
<!--                                                    <span class="tender-ico">100$</span>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-8 col-ms-8 col-sm-8 col-md-8">-->
<!--                                            <div class="box-border">-->
<!--                                                <div class="center-inner-block">-->
<!--                                                    <p>Ленінградська обл.<i class="fa fa-angle-right fa-2x in-p"></i></p>-->
<!--                                                    <p>Малопольське</p>-->
<!--                                                </div>-->
<!--                                                <div class="center-inner-block bottom">-->
<!--                                                    <p>Бокситогорськ, <a href="#">...</a></p>-->
<!--                                                    <p>Тарностав-Люб<a href="#">...</a>-->
<!--                                                        <a href="#"></a>-->
<!--                                                    </p>-->
<!--                                                </div>-->
<!--                                                <i class="fa fa-map-marker fa-2x abs"></i>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 col-ms-12 col-sm-6 col-md-6">-->
<!--                                <div class="row thumb-bottom">-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6 white">-->
<!--                                        <div class="right-inner">-->
<!--                                            <p class="yellow-span"></p>-->
<!--                                            <div class="descr-tr">-->
<!--                                                <p class="top-inner">Тент, длинномер TIR, ADR 11<span class="back-img appl"></span></p>-->
<!--                                                <p class="count">Кількість автомобілів: <b>20</b></p>-->
<!--                                                <p class="middle-inner">2.42 х 3 х 13.6</p>-->
<!--                                                <p class="bottom-inner">22 т / 86 м3</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-6 col-ms-6 col-sm-6 col-md-6">-->
<!--                                        <div class="right-inner-last">-->
<!--                                            <a href="#" class="p-title last">ООО "Евротранс Экспресс" (Юридическое лицо)-->
<!--                                                <i class="fa fa-envelope fa-2x pull-right"></i>-->
<!--                                                <span class="radio-online active pull-right"></span>-->
<!--                                                <i class="fa fa-asterisk client-abs"></i>-->
<!--                                            </a>-->
<!---->
<!--                                            <p class="p-top last">Михаил</p>-->
<!--                                            <p class="p-middle last">+7(912)744-02-93</p>-->
<!--                                            <div class="p-rating last">-->
<!--                                                <div class="stars">-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star active"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <i class="fa fa-star"></i>-->
<!--                                                    <span class="ratings">3.38</span>-->
<!--                                                </div>-->
<!--                                                <div class="comments">-->
<!--                                                    <i class="fa fa-comment-o fa-flip-horizontal"></i> 12-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="container-fluid transport bottom">-->
<!--    <div class="container center">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="center-p">-->
<!--                    <nav>-->
<!--                        <ul class="pagination">-->
<!--                            <li>-->
<!--                                <a href="#" aria-label="Previous">-->
<!--                                    <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li><a href="#">1</a></li>-->
<!--                            <li><a href="#">2</a></li>-->
<!--                            <li><a href="#">3</a></li>-->
<!--                            <li><a href="#">4</a></li>-->
<!--                            <li><a href="#">5</a></li>-->
<!--                            <li>-->
<!--                                <a href="#" aria-label="Next">-->
<!--                                    <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </nav>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
</div>