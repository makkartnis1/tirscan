<?php
/**
 * @var Controller_Frontend_Requests $controller
 * @var array $template_updates
 */
?>

<?
if ($controller->request->action() == 'cargo') {
    $url = Route::url('frontend_site_request_cargo_list', $controller->getRequestParams());
}
else {
    $url = Route::url('frontend_site_request_transport_list', $controller->getRequestParams());
}
?>


<div class="template-fixed-box template-semafor-block" <? if (empty($template_updates)) { ?>style="display:none;"<? } ?>>
    <div class="head">
        <div class="row">
            <div class="col-xs-12">
                <h3><a href="javascript:;">Шаблони <!--<i class="fa fa-times"></i></a>--> </h3>
            </div>
        </div>
    </div>
    <ul class="inner-holder template-items">
        <? foreach ($template_updates as $template_update) { ?>
       <li class="template-item active">
            <div class="first-holder text-center">
                <span class="items-numb"><?= $template_update['count'] ?></span>
            </div>
            <div class="center-holder text-left">
                <a href="<?= $url . '?t=' . $template_update['template']->ID ?>">
                    <?= $template_update['template']->name ?>
                </a>
            </div>
        </li>
        <? } ?>
    </ul>
</div>

<script id="template_filterTemplateInModal" type="text/x-handlebars-template">
    {{#each items}}
    <li class="template-item" data-id="{{id}}" data-name="{{name}}" data-query="{{query}}">
        <div class="first-holder text-center">
            {{#if_equal_soft active_semafor 'yes'}}
            <span class="items-numb"> </span>
            {{/if_equal_soft}}
        </div>
        <div class="center-holder text-center">
            <a href="<?= $url ?>?t={{id}}" class="link-apply-template">
                {{name}}
            </a>
            <input type="text" data-id="{{id}}" style="display: none;" class="input-edit-template-name form-control" value="{{name}}">
        </div>
        <div class="last-holder text-right">
            <a href="javascript:;" class="template-edit-name"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a href="javascript:;" class="template-delete"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
            <a href="javascript:;" class="template-update-query"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
            {{#if_equal_soft active_semafor 'no'}}
            <a href="javascript:;" class="template-semafor"><i class="fa fa-bell" aria-hidden="true"></i></a>
            {{/if_equal_soft}}
        </div>
    </li>
    {{else}}
    <p>Не створено жодного шаблону!</p>
    {{/each}}
</script>

<div class="modal fade" id="template-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                <h4 class="modal-title" id="myModalLabel">Шаблони</h4>
            </div>
            <div class="modal-body template">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="template-fixed-box">
                            <ul class="inner-holder">
                            </ul>
                        </div>
                        <div class="template-footer">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <button type="button" class="button-add-template btn btn-primary"><i class="fa fa-plus"></i> Створити шаблон</button>
                                </div>
                                <div class="col-xs-4 col-xs-offset-4 text-right">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрити</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>