<?php
/**
 * LINKeR
 */

/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 03.06.16
 * Time: 17:51
 */
?>
<script  type="text/x-handlebars-template" id="modal-cargo-or-transport-request-additional-info">

<div class="list-group">
    {{#each list}}
    <div class="list-group-item additional-info-list">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <span data-name>{{{name}}}</span>
                </div>
                <div class="pull-right">
                    <span data-value>{{{value}}}</span>
                    <span data-unit>{{{unit}}}</span>
                </div>
            </div>
        </div>
    </div>
    {{/each}}
</div>

</script>