<div class="whiteFluidContainer">
    <div class="container-fluid ">
        <div class="container center">
            <div class="t-block">
                <div class="t-title">
                    <p><?= $code ?>!</p>
                </div>
                <div class="t-content">
                    <p><?= $message ?></p>
                </div>
            </div>
        </div>
    </div>
</div>