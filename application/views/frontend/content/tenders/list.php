<div class="myStatistics">
<?

if ($right == 'cargo' or $right == 'exp')
{
    ?>
    <div class="row">
        <div class="col-xs-3 col-xs-offset-9">
            <a href="/<?=$lang_uri?>/tender/add" class="btn btn-primary"> <?=__db('tenders.page.add.title')?></a>
        </div>
    </div>
<?
}
?>


<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs profile responsive" role="tablist">

            <?
            if ($right == 'exp' or $right == 'cargo')
            {
                ?>
                <li role="presentation" class="active">
                    <a href="#app1" aria-controls="app1" role="tab" data-toggle="tab">
                        <?=__db('profile.tab.my-tenders')?>
                    </a>
                </li>
            <?
            }
            ?>

            <?
            if ($right == 'exp' or $right == 'trans')
            {
                ?>
                <li role="presentation" <?=($right == 'trans')?'class="active"':''?>>
                    <a href="#app2" aria-controls="app2" role="tab" data-toggle="tab">
                        <?=__db('profile.tab.owner_tenders')?>
                    </a>
                </li>
            <?
            }
            ?>
        </ul>
    </div>
</div>

<div class="tab-content responsive">

    <?
    if ($right == 'exp' or $right == 'cargo')
    {
        ?>

        <div role="tabpanel" class="tab-pane active" id="app1">


            <div class="row">
                <div class="col-md-12">
                    <section class="profile tender-table table-responsive">
                        <table class="table table-striped table-condensed table-hover table-tenders-list">
                            <thead>
                            <tr>
                                <th><?=__db('data')?></th>
                                <th><?=__db('from')?></th>
                                <th><?=__db('to')?></th>
                                <th><?=__db('tenders.page.cargo_type')?></th>
                                <th><?=__db('profile.tab.weight_and_volume')?></th>
                                <th><?=__db('cina')?></th>
                                <th><?=__db('profile.tab.bets')?></th>
                                <th><?=__db('profile.tab.status')?></th>
                                <th><?=__db('profile.tab.actions')?></th>
                            </tr>
                            </thead>
                            <tbody id="profile-my-tenders-list">


                            </tbody>
                        </table>
                    </section>
                    <ul class="pagination center" data-tender-my="pagination"></ul>
                </div>
            </div>



        </div>

    <?
    }
    ?>


    <?
    if ($right == 'exp' or $right == 'trans')
    {
        ?>

        <div role="tabpanel" class="tab-pane <?=($right == 'trans')?'active':''?>" id="app2">


            <div class="row">
                <div class="col-md-12">
                    <section class="profile table-responsive">
                        <table class="table table-striped table-condensed table-hover table-tenders-list">
                            <thead>
                            <tr>
                                <th><?=__db('data')?></th>
                                <th><?=__db('from')?></th>
                                <th><?=__db('to')?></th>
                                <th><?=__db('tenders.page.cargo_type')?></th>
                                <th><?=__db('profile.tab.weight_and_volume')?></th>
                                <th><?=__db('cina')?></th>
                                <th><?=__db('profile.tab.position')?>/<?=__db('profile.tab.bets')?></th>
                                <th><?=__db('profile.tab.status')?></th>
                                <th><?=__db('profile.tab.actions')?></th>
                            </tr>
                            </thead>
                            <tbody id="profile-owner-tenders-list">


                            </tbody>
                        </table>
                    </section>
                    <ul class="pagination center" data-tender-owner="pagination"></ul>
                </div>
            </div>


        </div>

    <?
    }
    ?>


</div>
</div>

<div id="for_user_right" right="<?=$right?>"></div>

<style type="text/css">
    .table-tenders-list td
    {
        vertical-align: middle !important;
        cursor: pointer;
    }

    .table-tenders-list
    {
        font-size: 12px;
    }

    .tender-link
    {
        color: black;
    }

    .table-tenders-list .active td
    {
        background-color: #dff0d8 !important;
    }

    .table-tenders-list .inactive td
    {
        background-color: #e9f6ff !important;
    }
    .editing-block-tender
    {
        font-size: 14px;
    }

</style>