<div class="content">
<div class="container-fluid transport bottom">
<div class="container center">
<div class="tender-box">
<div class="modal-content">
<div class="tender-head">
    <div class="row">
        <div class="col-xs-6"><h4 class="title"><span class="ttl"><?=__db('tender_page.tender')?> №<?=$tender['info']['ID']?></span></h4></div>
        <div class="col-xs-6 text-right"><a href="<?=(isset($_SERVER["HTTP_REFERER"]))?$_SERVER["HTTP_REFERER"]:'/'.$controller->locale->uri?>"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><?=__db('tender_page.back')?></a></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="one-tender-block" style="padding: 15px 30px 30px; font-size: 12px">

            <section class="profile table-responsive">
                <table class="table table-striped table-condensed table-hover table-tenders-list">
                    <thead>
                    <tr>
                        <th><? __dbt("Дата") ?></th>
                        <th><? __dbt("Звідки") ?></th>
                        <th><? __dbt("Куди") ?></th>
                        <th><? __dbt("Тип вантажу") ?></th>
                        <th><? __dbt("Вага і об'єм") ?></th>
                        <th><? __dbt("Ціна") ?></th>
                        <th><? __dbt("Ставки") ?></th>
                        <th><? __dbt("Компанія") ?></th>
                    </tr>
                    </thead>
                    <tbody id="profile-my-tenders-list">
                    <tr>
                        <td>
                            <?
                            if ($tender['info']['dateFromLoad'] == $tender['info']['dateToLoad'])
                            {
                                echo date("d.m",strtotime($tender['info']['dateFromLoad']));
                            }
                            else
                            {
                                echo date("d.m",strtotime($tender['info']['dateFromLoad'])).' - '.date("d.m",strtotime($tender['info']['dateToLoad']));
                            }
                            ?>
                        </td>

                        <?

                        $exp_from_place = explode(', ',$tender['place_from'][0]['name']);
                        $from_1 = $exp_from_place[0];
                        $from_2 = (isset($exp_from_place[1]))?$exp_from_place[1]:$from_1;

                        $exp_to_place = explode(', ',$tender['place_to'][0]['name']);
                        $to_1 = $exp_to_place[0];
                        $to_2 = (isset($exp_to_place[1]))?$exp_to_place[1]:$to_1;

                        ?>

                        <td>
                            <?=$from_1?>(<?=$tender['place_from'][0]['country']?>)
                        </td>
                        <td>
                            <?=$to_1?>(<?=$tender['place_to'][0]['country']?>)
                        </td>
                        <td>
                            <?=($tender['info']['cargoType'])?$tender['info']['cargoType']:'-'?>
                        </td>
                        <td>
                            <?
                            $weight_and_volume = [];
                            if ($tender['info']['weight']) $weight_and_volume[] = $tender['info']['weight'].' т';
                            if ($tender['info']['volume']) $weight_and_volume[] = $tender['info']['volume'].' м<sup>3</sup>';
                            ?>
                            <?=implode(' / ',$weight_and_volume)?>
                        </td>
                        <td>
                            <?=($tender['info']['priceHide'] == 'no' or $tender['info']['userID'] == $user_id)?$tender['info']['price'].' '.$tender['info']['code'].'<br>':''?>
                            <?=($tender['info']['priceHide'] == 'yes')?__db('tender_page.hidden'):''?>
                        </td>
                        <td>
                            <?=$tender['count_requests']?>
                        </td>
                        <td>
                            <a href="<?=$tender['company_url']?>" target="_blank" class="tender-link">
                                <?
                                if ($controller->authorized)
                                {
                                    echo $tender['company_name'].'<br>'.$tender['phone'];
                                }
                                else
                                {
                                    echo __db('tender_page.hidden');
                                }
                                ?>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </section>

        </div>
    </div>
</div>
<div class="modal-body tender">
<div class="row">
<div class="col-lg-12 col-md-12">
<div class="row">
    <div class="col-xs-8 container-helper">
        <h3>
            <?
            if (!empty($conditions) and $user_id != $tender['info']['userID'])
            {
                echo __db('tender_page.condition_error');
            }
            else
            {
                echo __db('tender_page.condition');
            }
            ?>
        </h3>
        <ul class="tender-list">
            <?
            if ($tender['info']['filterPartners'] == 'yes')
            {
                ?>
                <li <?=(array_key_exists('partner',$conditions) and $user_id != $tender['info']['userID'])?'class="conditions_refused"':''?>><i class="fa <?=(array_key_exists('partner',$conditions) and $user_id != $tender['info']['userID'])?'fa-times conditions_refused':'fa-check'?>" aria-hidden="true"></i>
                    <?=__db('tenders.page.filter.partners');?>
                </li>
            <?
            }
            else
            {
                ?>
                <li <?=(array_key_exists('company_type',$conditions) and $user_id != $tender['info']['userID'])?'class="conditions_refused"':''?>><i class="fa <?=(array_key_exists('company_type',$conditions) and $user_id != $tender['info']['userID'])?'fa-times conditions_refused':'fa-check'?>" aria-hidden="true"></i>
                    <?=__db('tender_page.companies_type')?>:
                    <b><?=($tender['info']['filterForwarder']=='yes')?__db('tender_page.trans_and_exp'):__db('tender_page.trans_only')?></b>
                </li>
                <li <?=(array_key_exists('company_status',$conditions) and $user_id != $tender['info']['userID'])?'class="conditions_refused"':''?>><i class="fa <?=(array_key_exists('company_status',$conditions) and $user_id != $tender['info']['userID'])?'fa-times conditions_refused':'fa-check'?>" aria-hidden="true"></i>
                    <?=__db('tender_page.company_status')?>:
                    <b><?=($tender['info']['filterCompany']=='yes')?__db('tender_page.company_finance_confirm'):__db('tender_page.all')?></b>
                </li>
                <li <?=(array_key_exists('company_time',$conditions) and $user_id != $tender['info']['userID'])?'class="conditions_refused"':''?>><i class="fa <?=(array_key_exists('company_time',$conditions) and $user_id != $tender['info']['userID'])?'fa-times conditions_refused':'fa-check'?>" aria-hidden="true"></i>
                    <?=__db('tender_page.time_on_market')?>:
                    <b><?=($tender['info']['filterCompanyYears'])?__db('tenders.page.from[:count:]years',array('[:count:]'=>$tender['info']['filterCompanyYears'])):__db('tenders.page.not_specified')?></b>
                </li>
                <li <?=(array_key_exists('user',$conditions) and $user_id != $tender['info']['userID'])?'class="conditions_refused"':''?>><i class="fa <?=(array_key_exists('user',$conditions) and $user_id != $tender['info']['userID'])?'fa-times conditions_refused':'fa-check'?>" aria-hidden="true"></i>
                    <b>
                        <?=__db('tender_page.only_registered')?>
                    </b>
                </li>
            <?
            }
            ?>

        </ul>
    </div>
    <p class="price">
        <?
        if ($tender['info']['priceHide'] == 'no' or $tender['info']['userID'] == $user_id)
        {
            ?>
            <?=__db('tender_page.max_price')?>: <?=$tender['info']['price'].' '.$tender['info']['code']?>
            <?=($tender['info']['userID'] == $user_id)?(($tender['info']['priceHide'] == 'no')?'<br>('.__db('otkrytaya').')':'<br>('.__db('tender_page.hidden').')'):''?>
        <?
        }
        ?>
    </p>
    <div class="col-xs-4  container-helper bg-grey">
        <?

        if (($tender['info']['status'] == 'active' or $tender['info']['status'] == 'completed') and $tender['info']['userID'] != $user_id and $user_id)
        {
            if ($tender['isset_user_rates'])
            {
                ?>
                <h3>
                    <?=__db('tender_page.you_bet_place')?>
                </h3>
                <ul class="tender-list cash">
                    <li>
                        <?
                        if (gettype($tender['user_place_num']) == 'integer')
                        {
                            $num_place = $tender['user_place_num']+1;
                            ?>
                            <span class="<?=($num_place==1)?'num_place_first':'num_place_other'?>">№ <?=$num_place?></span>
                        <?
                        }
                        else
                        {
                            echo $tender['user_place_num'];
                        }
                        ?>
                    </li>
                </ul>
            <?
            }
        }


        if ($tender['info']['userID'] != $user_id and empty($conditions))
        {
            ?>
            <h3><?=__db('tender_page.you_last_bet')?></h3>
            <ul class="tender-list cash">
                <li><?=$tender['user_rate']?></li>
            </ul>
        <?
        }
        ?>

        <h3>
            <?


            if ($tender['info']['status'] == 'active')
            {
                echo __db('tender_page.to_finish');
            }
            elseif($tender['info']['status'] == 'inactive')
            {
                echo __db('tender_page.to_start');
            }elseif($tender['info']['status'] == 'completed')
            {
                echo __db('tenders.model.notification.completed.subject');
            }
            ?>

            <i class="fa fa-clock-o" aria-hidden="true"></i></h3>
        <ul class="tender-list">
            <li>
                <?
                if ($tender['info']['status'] == 'active' or $tender['info']['status'] == 'inactive')
                {
                    ?>
                    <span id="date_end"></span>
                <?
                }
                elseif($tender['info']['status'] == 'stopped')
                {
                    ?>
                    <div class="attention_block">
                        <?=__db('tender_page.stop_for_reason')?>: <br>
                        <?=($tender['info']['stop_reason'])?$tender['info']['stop_reason']:'-'?>
                    </div>
                <?
                }
                else
                {
                    echo __db('tender_page.time_out');
                }
                ?>
            </li>
        </ul>
    </div>
</div>
<div class="row">

    <?
    $docTIR = $tender['info']['docTIR'];
    $docCMR = $tender['info']['docCMR'];
    $docT1 = $tender['info']['docT1'];
    $docSanPassport = $tender['info']['docSanPassport'];
    $docSanBook = $tender['info']['docSanBook'];
    $loadFromSide = $tender['info']['loadFromSide'];
    $loadFromTop = $tender['info']['loadFromTop'];
    $loadFromBehind = $tender['info']['loadFromBehind'];
    $loadTent = $tender['info']['loadTent'];
    $condPlomb = $tender['info']['condPlomb'];
    $condLoad = $tender['info']['condLoad'];
    $condBelts = $tender['info']['condBelts'];
    $condRemovableStands = $tender['info']['condRemovableStands'];
    $condHardSide = $tender['info']['condHardSide'];
    $condCollectableCargo = $tender['info']['condCollectableCargo'];
    $condTemperature = $tender['info']['condTemperature'];
    $condPalets = $tender['info']['condPalets'];
    $condADR = $tender['info']['condADR'];
    $carCount = $tender['info']['carCount'];
    $sizeX = $tender['info']['sizeX'];
    $sizeY = $tender['info']['sizeY'];
    $sizeZ = $tender['info']['sizeZ'];

    $text = '';
    $docs_arr = [];
    $load_arr = [];
    $cond_arr = [];


    if ($docTIR == 'yes')
    {
        $docs_arr[] = 'TIR';
    }

    if ($docCMR == 'yes')
    {
        $docs_arr[] = 'CMR';
    }

    if ($docT1 == 'yes')
    {
        $docs_arr[] = 'T1';
    }

    if ($docSanPassport == 'yes')
    {
        $docs_arr[] = __db('tenders.page.sanitary_passport');
    }

    if ($docSanBook == 'yes')
    {
        $docs_arr[] = __db('tenders.page.sanitary_book');
    }

    if ($loadFromSide == 'yes')
    {
        $load_arr[] = __db('tenders.page.load_type.side');
    }

    if ($loadFromTop == 'yes')
    {
        $load_arr[] = __db('tenders.page.load_type.top');
    }

    if ($loadFromBehind == 'yes')
    {
        $load_arr[] = __db('tenders.page.load_type.behind');
    }

    if ($loadTent == 'yes')
    {
        $load_arr[] = __db('tenders.page.load_type.tent');
    }

    if ($condPlomb == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.plomb');
    }

    if ($condLoad == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.reload');
    }

    if ($condBelts == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.belts');
    }

    if ($condRemovableStands == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.stands');
    }

    if ($condHardSide == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.bort');
    }

    if ($condCollectableCargo == 'yes')
    {
        $cond_arr[] = __db('tenders.page.conditions.collectable');
    }

    if ($condTemperature)
    {
        $cond_arr[] = __db('tender_page.t').$condTemperature."°";
    }

    if ($condPalets)
    {
        $cond_arr[] = __db('tenders.page.conditions.pallets').":".$condPalets;
    }

    if ($condADR)
    {
        $cond_arr[] = "ADR:".$condADR;
    }

    if (!empty($docs_arr))
    {
        $text .= __db('tenders.page.docs').": ".implode(', ',$docs_arr).".<br>";
    }

    if (!empty($load_arr))
    {
        $text .= __db('tenders.page.load_type').": ".implode(', ',$load_arr).".<br>";
    }

    if (!empty($cond_arr))
    {
        $text .= __db('tender_page.conditions').": ".implode(', ',$cond_arr).". ";
    }

    if ($carCount)
    {
        $text .= "<br>".__db('kilkist.mashin').": ".$carCount.". ";
    }

    if ($sizeX or $sizeY or $sizeZ)
    {
        $gabarits_arr = [];

        if ($sizeX)
        {
            $gabarits_arr[] = "довжина ".(float)$sizeX." м";
        }

        if ($sizeY)
        {
            $gabarits_arr[] = "ширина ".(float)$sizeY." м";
        }

        if ($sizeZ)
        {
            $gabarits_arr[] = "висота ".(float)$sizeZ." м";
        }

        $text .= "<br>".__db('gabariti.transportnogo.zasobu').": ".implode(', ',$gabarits_arr);
    }

    ?>
    <div class="col-xs-8 container-helper">
        <h3><?=__db('tenders.page.additional')?></h3>
        <p class="descr">
            <?
            if ($text)
            {
                echo $text;
            }
            else
            {

                echo __db('tender_page.conditions_empty');
            }
            ?>
        </p>
    </div>
    <?

    if ($tender['info']['status'] == 'active' and $tender['info']['userID'] != $user_id and empty($conditions))
    {
        ?>
        <div class="col-xs-4 container-helper bg-grey">
            <h3>
                <?=__db('tender_page.recommended_bet')?>
            </h3>
            <ul class="tender-list">
                <li>
                    <input class="form-control" type="number" name="custom_rate" placeholder="<?=__db('tender_page.suma')?>" value="<?=$tender['next_rate_val']?>"> <span id="currency_code"><?=$tender['currency']?></span>
                    <br><span id="set_auto_bet" data-toggle="modal" data-target="#auto_bet">
                        <?
                        if ($tender['auto_rate'])
                        {
                            ?><? __dbt("Мінімальна автоставка: ") ?><?=$tender['auto_rate'].' '.$tender['currency']?>
                        <?
                        }
                        else
                        {
                            ?><? __dbt("Встановити автоставку") ?><?
                        }
                        ?>
                    </span>
                </li>
            </ul>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-6">
                    <button id="custom_confirm_modal" class="btn btn-primary pull-right" data-toggle="modal" data-target="#bet"><?=__db('tender_page.bet')?></button>
                </div>
            </div>
        </div>
    <?
    }
    ?>
</div>
</div>

<?
if (!empty($tender['requests']) and ($tender['info']['priceHide'] == 'no' or $tender['info']['userID'] == $user_id))
{

    ?>

    <div class="col-xs-12">
        <div class="">
            <h3 class="table-title-company"><?=__db('tender_page.bets_list')?></h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th><?=__db('tender_page.company')?></th>
                    <th><?=__db('data')?></th>
                    <th><?=__db('tender_page.bet')?></th>
                </tr>
                </thead>
                <tbody>
                <?
                $i=0;
                foreach($tender['requests'] as $req)
                {
                    $i++;
                    ?>
                    <tr class="<?

                    if ($req['userID'] == $user_id and $i > 1)
                    {
                        echo 'my_bet_row';
                    }

                    if ($tender['info']['userID'] == $user_id)
                    {
                        if ($i == 1)
                            echo 'my_bet_row';
                    }
                    else
                    {
                        if ($i == 1)
                        {

                            if ($req['userID'] == $user_id)
                            {
                                echo 'best_bet_row_my';
                            }
                            else
                            {
                                echo 'best_bet_row_other';
                            }

                        }
                    }

                    ?>">
                        <td>
                            <?
                            if ($tender['info']['userID'] == $user_id)
                            {
                                ?>
                                <a href="<?=$req['company_url']?>"><?=$req['company_name']?></a>
                            <?
                            }
                            else
                            {
                                if ($req['userID'] == $user_id)
                                {
                                    echo __db('tender_page.you_bet');
                                }
                                else
                                {
                                    ?>
                                    <i>
                                        <?=__db('tender_page.company_hide')?>
                                    </i>
                                <?
                                }

                            }

                            ?>
                        </td>
                        <td>
                            <?=date("d.m.Y H:i:s",strtotime($req['created']))?>
                        </td>
                        <td>
                            <?
                            echo $req['customPrice'].' '.$tender['info']['code'];
                            ?>
                        </td>
                    </tr>
                <?
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

<?
}
?>

</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="modal fade feedbackModal" id="auto_bet" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-left: 15px">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Встановлення автоставки") ?></span></h4>
            </div>
            <div class="modal-body">

                <div class="apps-red">
                    <div class="row transparent">
                        <div class="col-md-12">
                            <form class="apps-red" action="" method="post">
                                <div class="content addTransportContent">
                                    <div class="greyFluidContainer">
                                        <div class="container-fluid">
                                            <div class="center">
                                                <div class="formHolder">
                                                    <div class="row">
                                                        <div class="form-group col-md-12" style="margin-top:5px">
                                                            <label for=""><? __dbt("Автоматична мінімальна ставка: ") ?><input style="width: 110px !important; display: inline-block;" class="form-control" type="number" name="auto_bet" placeholder="не вказано" value="<?=$tender['auto_rate']?>"> <span id="currency_code"><?=$tender['currency']?></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="whiteFluidContainer">
                                        <div class="formHolder cargoType">
                                            <div class="form-group uploadPlace" style="padding: 0 15px">
                                                <label for="off" style="font-size: 11px"><? __dbt("Правила роботи автоставок") ?><ul>
                                                        <li><? __dbt("Автоматична мінімальна ставка - це мінімальна сума до якої будуть автоматично виставлятися ставки без участі користувача") ?></li>
                                                        <li><? __dbt("Автоматичні ставки будуть виставлені у випадку якщо поточна ставка користувача не є найкращою") ?></li>
                                                        <li><? __dbt("Автоматичні ставки будуть проставлятися з кроком, який ставновить 1% від початкової суми торгів") ?></li>
                                                        <li><? __dbt("Після встановлення автоматичної ставки відмінити її уже не можна") ?></li>
                                                    </ul>
                                                    <br>
                                                    <?=__db('tender_page.offers[:link:]',array('[:link:]'=>'/'.Route::get('frontend_static_page')->uri(array('lang'=>$controller->locale->uri,'uri'=>'tenders-offer'))))?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-8">
                                            <button style="margin-bottom: 20px" type="submit" class="btn btn-primary"><? __dbt("Зберегти") ?></button>
                                            <input type="hidden" name="go_auto_bet">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade feedbackModal" id="bet" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-left: 15px">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl">
                        <?=__db('tender_page.bet_confirm')?>
                    </span></h4>
            </div>
            <div class="modal-body">

                <div class="apps-red">
                    <div class="row transparent">
                        <div class="col-md-12">
                            <form class="apps-red" action="" method="post">
                                <div class="content addTransportContent">
                                    <div class="greyFluidContainer">
                                        <div class="container-fluid">
                                            <div class="center">
                                                <div class="formHolder">
                                                    <div class="row">
                                                        <div class="form-group col-md-12" style="margin-top:15px">
                                                            <label for=""><?=__db('tender_page.you_bet_is')?>: <span id="res_custom_rate"><?=$tender['next_rate']?></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="whiteFluidContainer">
                                        <div class="formHolder cargoType">
                                            <div class="form-group uploadPlace" style="padding: 0 15px">
                                                <label for="off">
                                                    <?=__db('tender_page.offers[:link:]',array('[:link:]'=>'/'.Route::get('frontend_static_page')->uri(array('lang'=>$controller->locale->uri,'uri'=>'tenders-offer'))))?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-8">
                                            <button style="margin-bottom: 20px" type="submit" class="btn btn-primary"><?=__db('tender_page.go_bet')?></button>
                                            <input type="hidden" name="go_bet">
                                            <input type="hidden" name="custom_bet">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
    $(function(){

        moment.locale('<?=$controller->locale->googleLang?>');

        countdown.resetLabels();

        countdown.setLabels(
            ' <?=__db('tender_page.msec_1')?>| <?=__db('tender_page.sec_1')?>| <?=__db('tender_page.min_1')?>| <?=__db('tender_page.hour_1')?>| <?=__db('tender_page.day_1')?>| <?=__db('tender_page.week_1')?>| <?=__db('tender_page.year_1')?>',
            ' <?=__db('tender_page.msec_2')?>| <?=__db('tender_page.sec_2')?>| <?=__db('tender_page.min_2')?>| <?=__db('tender_page.hour_2')?>| <?=__db('tender_page.day_2')?>| <?=__db('tender_page.week_2')?>| <?=__db('tender_page.year_2')?>',
            ' і ',
            ', ',
            '0');

        function updateTime()
        {
            var time = moment("<?=($tender['info']['status'] == 'active')?$tender['info']['datetimeToTender']:$tender['info']['datetimeFromTender']?>").countdown().toString();
            var date_end_full = moment("<?=($tender['info']['status'] == 'active')?$tender['info']['datetimeToTender']:$tender['info']['datetimeFromTender']?>").format('llll');

            if (time == '0')
            {
                window.location.reload();
            }
            else
            {
                $("#date_end").html(time+'<br>('+date_end_full+')');
            }

        }


        <?
            if ($tender['info']['status'] == 'active' or $tender['info']['status'] == 'inactive')
            {
                ?>
        updateTime();
        setInterval(function(){
            updateTime();
        },1000);
        <?
    }
?>


        $(document).on( "click", "#custom_confirm_modal", function(){
            var custom_rate = $("[name=custom_rate]").val();
            $("[name=custom_bet]").val(custom_rate);
            var currency = $("#currency_code").html();
            $("#res_custom_rate").html(custom_rate+' '+currency);
        });


        setInterval(function(){
            checkChangeTender('<?=$tender['info']['ID']?>','<?=$tender['info']['status']?>','<?=count($tender['requests'])?>');
        },5000);

    });

    function checkChangeTender(id,status,count_requests)
    {
        $.post("/api/front/tender/changes",{id:id,status:status,count_requests:count_requests},callbackCheckChangeTender,"json");
    }

    function callbackCheckChangeTender(data)
    {
        if (data.data.result)
            window.location = '<?='/'.Route::get('frontend_site_tender')->uri(array('lang'=>$controller->locale->uri,'id'=>$tender['info']['ID']))?>';
    }

</script>