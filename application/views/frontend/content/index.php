<div class="index-content-holder">
<?= $filter_form ?>
<div class="container-fluid transparent top">
    <div class="container center">
        <div style="position: relative; width: 960px; margin: auto;z-index: 10" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 22px" src="http://placehold.it/160x200" />-->
<!--            <img style="position: absolute; right: -175px; top: 22px" src="http://placehold.it/160x200" />-->
        </div>
        <div style="position: relative; width: 960px; margin: auto;z-index: 10" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 370px" src="http://placehold.it/160x150" />-->
<!--            <img style="position: absolute; right: -175px; top: 370px" src="http://placehold.it/160x150" />-->
        </div>
        <div class="row">
            <div class="col-md-12">
                    <br>
                    <div class="text-center visible-lg visible-sm">
<!--                        <img src="http://placehold.it/970x90" />-->
                    </div>
                <div class="customNavigation">
                    <a class="btn prev" id="prev"><i class="fa fa-chevron-left fa-5x"></i></a>
                    <a class="btn next" id="next"><i class="fa fa-chevron-right fa-5x"></i></a>
                </div>
                <div id="owl-demo">
                    <div class="item">
                        <div class="carousel-title">
                            <div class="span-yellow"></div>
                            <span class="upper"><? __dbt('ОНОВЛЕННЯ ЗАявок по доступних вантажах') ?></span>
                        </div>
                        <section class="table-section-holder">
                            <table class="table table-striped table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th><? __dbt('Дата') ?></th>
                                    <th><? __dbt('Звідки') ?></th>
                                    <th><? __dbt('Куди') ?></th>
                                    <th class="col-additional-info"><? __dbt('Дод. інформація') ?></th>
                                    <th><? __dbt('Вага') ?></th>
                                    <th><? __dbt('Кузов') ?></th>
                                    <th><? __dbt('Ціна') ?></th>
                                    <th><? __dbt('Контакти') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($last_cargo_requests as $request): ?>
                                    <tr data-id="<?= $request['ID'] ?>"
                                        data-click="showTransportModal"
                                        data-from-placeid="<?= $request['from_placeID'] ?>"
                                        data-to-placeid="<?= $request['to_placeID'] ?>"
                                        data-request-id="<?= $request['ID'] ?>"
                                        data-request-type="cargo"
                                        class="cargo-row"
                                    >
                                        <td>
                                            <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
                                            <? $json = [] ?>
                                            <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

                                            <?

                                            if( !empty($request['_json_advancedPayment']) ){
                                                $unit = trim($request['_json_advancedPayment']);
                                            }else{
                                                $unit = $request['_json_currency_code'];
                                            }

                                            $json['price'] = $request['_json_price'].'|'.$unit;

                                            ?>

                                            <script type="application/json"><?= json_encode($json) ?></script>
                                        </td>
                                        <td class="from-country">
                                            <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
                                            <? foreach ($request['from_otherGeo'] as $geo): ?>
                                                <br><?= $geo['from_name'] ?> (<?= $geo['from_country'] ?>)
                                            <? endforeach; ?>
                                        </td>
                                        <td class="to-country">
                                            <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
                                            <? foreach ($request['to_otherGeo'] as $geo): ?>
                                                <br><?= $geo['to_name'] ?> (<?= $geo['to_country'] ?>)
                                            <? endforeach; ?>
                                        </td>
                                        <td class="col-additional-info">
                                            <?= Frontend_Helper_String::none_if_null($request['info'], $request['info'])  ?>
                                        </td>
                                        <td>
                                            <?= Frontend_Helper_String::none_if_null($request['weight'], (float)$request['weight'] . ' т') ?>
                                        </td>
                                        <td>
                                            <?= __db($request['car_type_localeName']) ?>
                                        </td>
                                        <td>
                                            <?= Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']) ?>
                                        </td>
                                        <td data-user-id="<?= $request['userID'] ?>" data-id="<?= $request['ID'] ?>">
                                            <? if ($controller->authorized) { ?>
                                            <? if ($request['companyTypeID'] == 1) { ?>
                                                <b class="expeditor-mark" style="color: red;">Е</b>
                                            <? } ?>
                                            <a href="<?= Route::url('frontend_site_company_page', $controller->getRequestParams(['company_url_name' => $request['companyID'] . '-company' ])) ?>">
                                            <?= htmlspecialchars(Frontend_Helper_String::substr_if_more($request['company_name'], 23)) ?>
                                                <br>
                                                <?= $request['phoneCode'] . $request['phone'] ?>
                                            <? }
                                            else { ?>
                                                <? __dbt('Дані про компанію<br>приховано') ?>
                                            <? } ?>
                                            </a>
                                            <i class="fa fa-envelope table-ico contact" aria-hidden="true"></i>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                    </div>
                    <div class="item">
                        <div class="carousel-title">
                            <div class="span-yellow"></div>
                            <span class="upper"><? __dbt('ОНОВЛЕННЯ Заявок по доступних транспортах') ?></span>
                        </div>
                        <section class="table-section-holder">

                            <table class="table table-striped table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th><? __dbt('Дата') ?></th>
                                    <th><? __dbt('Звідки') ?></th>
                                    <th><? __dbt('Куди') ?></th>
                                    <th class="col-additional-info"><? __dbt('Дод. інформація') ?></th>
                                    <th><? __dbt('Вага') ?></th>
                                    <th><? __dbt('Кузов') ?></th>
                                    <th><? __dbt('Ціна') ?></th>
                                    <th><? __dbt('Контакти') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($last_transport_requests as $request): ?>
                                    <tr data-id="<?= $request['ID'] ?>"
                                        data-click="showTransportModal"
                                        data-from-placeid="<?= $request['from_placeID'] ?>"
                                        data-to-placeid="<?= $request['to_placeID'] ?>"
                                        data-request-id="<?= $request['ID'] ?>"
                                        data-request-type="transport"
                                        class="cargo-row"
                                    >
                                        <td>
                                            <?= $request['dateFrom'] . (($request['dateTo'] != null) ? ' - ' . $request['dateTo'] : '') ?>
                                            <? $json = [] ?>
                                            <? foreach ($request as $key => $value){ if(strpos($key, 'json_') ===0){ $json[ substr($key, strlen('json_')) ] = $value; } } ?>

                                            <?

                                            if( !empty($request['_json_advancedPayment']) ){
                                                $unit = trim($request['_json_advancedPayment']);
                                            }else{
                                                $unit = $request['_json_currency_code'];
                                            }

                                            $json['price'] = $request['_json_price'].'|'.$unit;

                                            ?>

                                            <script type="application/json"><?= json_encode($json) ?></script>
                                        </td>
                                        <td class="from-country">
                                            <?= $request['from_name'] ?> (<?= $request['from_country'] ?>)
                                            <? foreach ($request['from_otherGeo'] as $geo): ?>
                                                <br><?= $geo['from_name'] ?> (<?= $geo['from_country'] ?>)
                                            <? endforeach; ?>
                                        </td>
                                        <td class="to-country">
                                            <?= $request['to_name'] ?> (<?= $request['to_country'] ?>)
                                            <? foreach ($request['to_otherGeo'] as $geo): ?>
                                                <br><?= $geo['to_name'] ?> (<?= $geo['to_country'] ?>)
                                            <? endforeach; ?>
                                        </td>
                                        <td class="col-additional-info">
                                            <?= Frontend_Helper_String::none_if_null($request['info'], $request['info'])  ?>
                                        </td>
                                        <td>
                                            <?= Frontend_Helper_String::none_if_null($request['car_liftingCapacity'], (float)$request['car_liftingCapacity'] . ' т') ?>
                                        </td>
                                        <td>
                                            <?= __db($request['car_type_localeName']) ?>
                                        </td>
                                        <td>
                                            <?= Frontend_Helper_String::none_if_null($request['value'], $request['value'] . ' ' . $request['valueCode']) ?>
                                        </td>
                                        <td data-user-id="<?= $request['userID'] ?>" data-id="<?= $request['ID'] ?>">
                                            <? if ($controller->authorized) { ?>
                                            <? if ($request['companyTypeID'] == 1) { ?>
                                                <b class="expeditor-mark" style="color: red;">Е</b>
                                            <? } ?>
                                            <a href="<?= Route::url('frontend_site_company_page', $controller->getRequestParams(['company_url_name' => $request['companyID'] . '-company' ])) ?>">
                                            <?= htmlspecialchars(Frontend_Helper_String::substr_if_more($request['company_name'], 23)) ?>
                                                <br>
                                                <?= $request['phoneCode'] . $request['phone'] ?>
                                            <? }
                                            else { ?>
                                                <? __dbt('Дані про компанію<br>приховано') ?>
                                            <? } ?>
                                            </a>
                                            <i class="fa fa-envelope table-ico contact" aria-hidden="true"></i>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-fluid possibilities">



    <div class="row full">

        <div style="position: relative; width: 960px; margin: auto;z-index: 10" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 30px" src="http://placehold.it/160x355" />-->
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="possibilities-block">
                <div class="row">
                    <div class="col-md-12 left-block">
                        <div class="span-yellow"></div>
                        <div class="title-up pos"><? __dbt('Можливості нашого порталу') ?></div>
                        <p class="pos-p">
                            <?=__db('main_page.our_opportunities')?>
                        </p>
                        <ul class="pos-ul">
                            <li><i class="fa fa-dropbox fa-2x"></i><span><? __dbt('СТРАХУВАННЯ вантажу') ?></span></li>
                            <li><i class="fa fa-truck fa-2x"></i><span><? __dbt('надійніСТЬ перевізникІВ') ?></span></li>
                            <li><i class="fa fa-globe fa-2x"></i><span><? __dbt('Перевезення по всьому світу') ?></span></li>
                            <li><i class="fa fa-users fa-2x"></i><span><? __dbt('Клієнтська підтримка') ?></span></li>
                            <li>
                                <i class="fa fa-file-o fa-2x fa-flip-horizontal"></i><i class="fa fa-file-o fa-2x abs fa-flip-horizontal"></i><span><? __dbt('робота з біржою') ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="img-back">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid transparent news-servises">
    <div class="container center">
        <div class="row">
            <div style="position: relative; width: 960px; margin: auto;z-index: 10" class="visible-lg visible-sm">
<!--                <img style="position: absolute; left: -175px; top: 27px" src="http://placehold.it/160x365" />-->
<!--                <img style="position: absolute; right: -175px; top: 27px" src="http://placehold.it/160x365" />-->
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="news">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="span-yellow"></div>
                            <div class="title-up new"><? __dbt('Новини порталу') ?><a href="<?= Route::url('frontend_site_news_list', $controller->getRequestParams()) ?>" class="pull-right n"><? __dbt('Всі новини') ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($last_news as $value) { ?>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="news-box">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="img-news">
                                                <a href="news/<?= $value['ID'] ?>-<?= $value['url'] ?>"><img src="<?= $value['image'] ?>"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="n-top">
                                                <i class="fa fa-calendar"></i><?= date('d.m.y G:i:s', strtotime($value['createDate'])); ?>
                                            </p>
                                            <p class="n-middle"><?= $value['title']; ?></p>
                                            <p class="n-bottom">
                                                <a href="news/<?= $value['ID'] ?>-<?= $value['url'] ?>"><? __dbt('Детальніше') ?><i class="fa fa-caret-right"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="servises">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="span-yellow"></div>
                            <div class="title-up new"><? __dbt('НАШ СЕРВІС У СВІТІ') ?></div>
                            <div class="stat-img"></div>
                            <p class="gray s">
                                <?=__db('main_page.our_service_anons')?>
                            </p>
                            <p class="n-bottom"><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'worldwide'])) ?>"><? __dbt('Детальніше ') ?><i class="fa fa-caret-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid transparent stat">
    <div class="container center padding">
        <div class="stat-block">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="border-right">
                        <div class="stat-top">
                            <i class="fa fa-building-o fa-2x"></i>
                            <span><?= $stats['completed_positions'] ?></span>
                        </div>
                        <div class="stat-bottom"><? __dbt('Успішних контактів') ?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="border-right">
                        <div class="stat-top">
                            <i class="fa fa-users fa-2x"></i>
                            <span><?= $stats['companies'] ?></span>
                        </div>
                        <div class="stat-bottom"><? __dbt('Зареєстрованих компаній') ?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="border-right">
                        <div class="stat-top">
                            <i class="fa fa-globe fa-2x"></i>
                            <span><?= $stats['countries'] ?></span>
                        </div>
                        <div class="stat-bottom"><? __dbt('Географія порталу, країн') ?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="border-right transparent">
                        <div class="stat-top">
                            <i class="fa fa-clock-o fa-2x"></i>
                            <span><?= $stats['active_positions'] ?></span>
                        </div>
                        <div class="stat-bottom"><? __dbt('Активних позицій') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-message" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt('Повідомлення менеджеру заявки') ?></span></h4>
            </div>
            <form method="post" id="form-messages">
                <div class="modal-body">
                    <input type="hidden" id="user_from" value="<?= $controller->user->ID; ?>">
                    <div class="form-title"><? __dbt('Тема повідомлення') ?></div>
                    <div class="form-group">
                        <input type="text"
                            name="theme"
                            class="form-control">
                    </div>

                    <div class="form-title"><? __dbt('Повідомлення') ?></div>
                    <div class="form-group">
                        <input type="hidden"
                            name="user_to">
                        <textarea type="text"
                            name="message"
                            id="message-textarea"
                            class="form-control <?= $controller->classIfError('message', 'complaint') ?>"></textarea>
                    </div>
                    <ul id="error-msg" class="errorText"></ul>

                    <div class="row">
                        <div class="offset-md-7 col-sm-5">
                            <button type="button" id="send-message" class="btn btn-primary"><? __dbt('Відправити') ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script id="template_modalRequestApply" type="text/x-handlebars-template">
    {{#if loaded}}
    <span class="request-text"><? __dbt('Вами вже було відправлено пропозицію по даній заявці') ?></span>
    {{else}}
    {{#if not_mine}}
    <button id="btn-send-application" class="btn btn-primary pull-right" data-request-type="{{type}}" data-request-id="{{request_id}}"><? __dbt('Відправити пропозицію') ?></button>
    {{else}}
    <span class="request-text"><? __dbt('Це заявка розміщена вашою компанією') ?></span>
    {{/if}}
    {{/if}}
</script>

<?= View::factory('frontend/content/requests/modal/request-additional-info'); ?>
