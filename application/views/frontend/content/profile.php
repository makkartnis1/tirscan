<div class="greyFluidContainer">
    <div class="container-fluid">
        <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 160px" src="http://placehold.it/160x600">-->
<!--            <img style="position: absolute; right: -175px; top: 160px" src="http://placehold.it/160x600">-->
        </div>
        <div class="container center">
            <div class="myCabinetHolder">
                <?= $profile_head ?>
            </div>
            <div class="cabinetTabs">
                <div class="navTabs">
                    <ul class="nav">
                        <?= $tab_nav ?>
                    </ul>
                </div>
                <div class="tab-content">
                    <?= $tab_content ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="content">-->
<!--    <div class="container-fluid profile">-->
<!--        <div class="container center profile">-->
<!--            <div class="row profile-head">-->
<!--                --><?//= $profile_head ?>
<!--            </div>-->
<!--            <div class="row profile-bottom">-->
<!--                <div class="col-xs-3 col-ms-4 col-sm-3 col-md-2">-->
<!--                    <ul class="nav nav-tabs tabs-left">-->
<!--                        --><?//= $tab_nav ?>
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-xs-9 col-ms-8 col-sm-9 col-md-10">-->
<!--                    <div class="profile-content tab-content">-->
<!--                        --><?//= $tab_content ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->