<div class="greyFluidContainer">
    <div class="container-fluid">

        <div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--            <img style="position: absolute; left: -175px; top: 160px" src="http://placehold.it/160x600" />-->
<!--            <img style="position: absolute; right: -175px; top: 160px" src="http://placehold.it/160x600" />-->
        </div>

        <div class="container center">
            <div class="t-block block-white">
                <div class="row">
                    <div class="col-md-8">
                        <div class="t-content static-block">
                            <?= $content ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="newsCategoriesMenu col-md-12">
                                <h2><? __dbt('Категорії') ?></h2>
                                <ul>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'our-services'])) ?>"><span>»</span><? __dbt('Наші послуги') ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'about'])) ?>"><span>»</span><? __dbt('Про проект') ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'system-description'])) ?>"><span>»</span><? __dbt('Опис системи') ?></a></li>
                                    <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'public-offer'])) ?>"><span>»</span><? __dbt('Публічна оферта') ?></a></li>
                                </ul>
                            </div>
                            <div class="col-md-12 visible-lg visible-sm">
<!--                                <img style="margin-left: 70px" src="http://placehold.it/160x600" />-->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>