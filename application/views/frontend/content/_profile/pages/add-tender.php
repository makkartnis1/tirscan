<div class="content">
    <div class="container-fluid transport bottom">
        <div class="container center">
            <div class="tender-box">
                <div class="modal-content">
                    <div class="tender-head">
                        <div class="row">
                            <div class="col-xs-6">
                                <h4 class="title"><span class="ttl">Створення тендеру</span></h4>
                            </div>
                            <div class="col-xs-6 text-right"><a href="/ukr/profile#tenders"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>повернутися назад</a></div>
                        </div>
                    </div>
                    <div class="modal-body tender add">
                        <form class="apps-red">
                            <div class="row transparent">
                                <div class="col-md-12">
                                    <div class="content addTransportContent cargoContent">
                                        <div class="greyFluidContainer">
                                            <div class="container-fluid ">
                                                <div class=" center ">
                                                    <div class="formHolder">
                                                        <div class="row">
                                                            <div class="form-group uploadPlace col-lg-6 col-md-6">
                                                                <label for="startPointCargo">Місце завантаження:</label>
                                                                <input type="text" class="form-control" id="startPointCargo" placeholder="Пункт (країна) завантаження">
                                                                <p><a href="#">Додати місце завантаження</a></p>
                                                            </div>
                                                            <div class="form-group uploadPlace col-lg-6 col-md-6">
                                                                <label for="endPointCargo">Місце розвантаження:</label>
                                                                <input type="text" class="form-control" id="endPointCargo" placeholder="Пункт (країна) розвантаження">
                                                                <p><a href="#">Додати місце розвантаження</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="whiteFluidContainer">
                                            <div class="container-fluid ">
                                                <div class=" center ">
                                                    <div class="formHolder cargoType">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <label for="cargoType">Тип вантажу</label>
                                                                    <input type="text" class="form-control" id="cargoType">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="maxWeightCargo">Вага вантажу</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="maxWeightCargo">
                                                                        <div class="input-group-addon">Т</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="maxContentCargo">Об'єм вантажу</label>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="maxContentCargo">
                                                                        <div class="input-group-addon">М<sup>3</sup></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <label for="cargoTruckType">Тип транспорту</label>
                                                                    <select name="" id="cargoTruckType">
                                                                        <option value="" selected="selected">- не обрано - </option>
                                                                        <option value="">ізотерм</option>
                                                                        <option value="">реф</option>
                                                                        <option value="">тент</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="truckQnt">Кількість машин</label>
                                                                    <input type="number" id="truckQnt" class="form-control" />
                                                                </div>
                                                                <div class="cargoMeasures">
                                                                    <label>Габарити транспортного засобу</label>
                                                                    <div class="measuresInput">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" id="maxLength" placeholder="Довжина">
                                                                            <div class="input-group-addon">М</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="measuresInput">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" id="maxWidth" placeholder="Ширина">
                                                                            <div class="input-group-addon">М</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="measuresInput">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" id="maxHeight" placeholder="Висота">
                                                                            <div class="input-group-addon">М</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="line clearfix">
                                                                <div class="col-md-12">
                                                                    <p class="additionalInfoHeading">Додаткова інформація: <span class="additionalInfoResult"></span> <a href="#" data-toggle="modal" data-target="#additionalInfoModal">вказати</a></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row transparent">
                                <div class="col-md-12">
                                    <div class="content addTransportContent">
                                        <div class="greyFluidContainer">
                                            <div class="container-fluid">
                                                <div class="center">
                                                    <div class="formHolder">
                                                        <label for="uploadCargoBeginDate">Дата:</label>
                                                        <div class="form-group row">
                                                            <div class="col-xs-4">
                                                                <input type="text" class="form-control" id="datepicker1" placeholder="Дата з">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <input type="text" class="form-control" id="datepicker2" placeholder="Дата по">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="startPointCargo">Рекомендована ціна</label>
                                                                <select>
                                                                    <option>не вказаний</option>
                                                                    <option>100$</option>
                                                                    <option>200$</option>
                                                                    <option>300$</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group check-block">
                                                                    <input type="checkbox" class="" id="price">
                                                                    <label for="price">Приховати рекомендовану ціну </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label>Tермін діяльності компанії на ринку</label>
                                                                <select>
                                                                    <option>до 3 років</option>
                                                                    <option>3-7 років </option>
                                                                    <option>понад 7 років</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Мінімально допустимий рейтинг компаній</label>
                                                                <select>
                                                                    <option>не вказаний</option>
                                                                    <option>більше 2</option>
                                                                    <option>більше 3</option>
                                                                    <option>більше 4</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-lg-12">
                                                                <input type="checkbox" class="" id="filter-company">
                                                                <label for="filter-company">Фільтрувати компанії лише з підтвердженими платіжними реквізитами</label>
                                                            </div>
                                                            <div class="form-group col-lg-12">
                                                                <input type="checkbox" class="" id="exp">
                                                                <label for="exp">Дозволяти приймати участь експедиторам</label>
                                                            </div>
                                                            <div class="form-group col-lg-12">
                                                                <input type="checkbox" class="" id="company">
                                                                <label for="company">Дозволяти приймати участь компаніям, що присутня у чорних списках
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="whiteFluidContainer">
                                            <div class="container-fluid ">
                                                <div class=" center ">
                                                    <div class="formHolder">
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="form-group rulesAccept">
                                                                    <div class="checkbox">
                                                                        <input type="checkbox" checked>
                                                                        <label>
                                                                            Я ознайомився і згідний з <a href="#">правилами</a> подачі тендерів на сайті
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div type="button" class="btn btn-primary"><b>РОЗМІСТИТИ</b></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade additionalInfoModal" id="additionalInfoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl">Додаткова інформація</span></h4>
            </div>
            <div class="modal-body">
                <h2>Відмітьте необхідні поля</h2>
                <form>
                    <div class="additionalInfoItem">
                        <h3>Документи</h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">TIR
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">CMR
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">T1
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Санпаспорт
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Санкнижка
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="additionalInfoItem">
                        <h3>Завантаження</h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Збоку
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Зверху
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Ззаду
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Розтентування
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="additionalInfoItem">
                        <h3>Умови</h3>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Пломба
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Довантаження
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">Ремені
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxOnly">
                                <label>
                                    <input type="checkbox">З'ємні стійки
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                                <label>
                                    <input type="checkbox">Жорсткий борт
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-6 checkboxOnly">
                                <label>
                                    <input type="checkbox">Збірний вантаж
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input type="checkbox">t&#176;
                                    <input type="text" class="form-control additionalTextInfo" />
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input type="checkbox">Палети
                                    <input type="text" class="form-control additionalTextInfo" />
                                </label>
                            </div>
                            <div class="checkbox col-md-2 col-sm-3 checkboxWithInput">
                                <label>
                                    <input type="checkbox">ADR
                                    <input type="text" class="form-control additionalTextInfo" />
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="additionalInfoItem">
                        <div class="row">
                            <div class="checkbox col-sm-6 checkboxOnly">
                                <label>
                                    <input type="checkbox">Лише перевізник
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Примітка</h3>
                            <textarea class="form-control" placeholder="" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-lg-offset-9">
                            <button class="btn btn-primary pull-right additionalInfoButton" type="button">Зберегти</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>