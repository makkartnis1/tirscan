<?
/**
 * @var $controller Controller_Frontend_Profile
 */
?>

<div class="row profile-head">
    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-2">
        <div class="profile-img-box">
            <div class="img-block">
                <div class="img-box">
                    <a href="#" class="fadeInDown">
                        <i class="fa fa-arrow-circle-down fa-2x"></i>
                    </a>
                    <span class="new-mess"><?php echo($count_messages)?></span>
                </div>
            </div>
            <div class="img-block-middle">
                <span class="green">Online</span>
            </div>
            <div class="img-block-bottom">
                <div class="social-block">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-vk"></i></a>
                    <a href="#"><i class="fa fa-linkedin-square"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-ms-12 col-sm-12 col-md-10">
        <div class="top-profile">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-6">
                    <p class="profile-name">
                        <?= $controller->user_locale->name ?>
                        <span class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </span>
                        <a href="javascript:;">
                            <i class="fa fa-credit-card"></i>0 грн
                        </a>
                    </p>
                    <p class="sub">
                        <a href="#infoblock" data-toggle="tab">Редагувати ПІБ або назву компанії</a>
                    </p>
                </div>
                <? if ($controller->user->hasRights('transport')): ?>
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <p class="padding first">
                        <a href="<?= $controller->links['transport_add_page'] ?>" type="button" class="btn btn-primary"><b>Додати транспорт</b></a>
                    </p>
                </div>
                <? endif; ?>
                <? if ($controller->user->hasRights('cargo')): ?>
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <p class="padding">
                        <a href="<?= $controller->links['cargo_add_page'] ?>" type="button" class="btn btn-primary"><b>Додати вантаж</b></a>
                    </p>
                </div>
                <? endif; ?>
            </div>
        </div>
        <div class="top-bottom">
            <div class="row">
                <div class="col-md-7">
                    <!--                    <p>У вас <a href="#application" data-toggle="tab">0 нових заявок</a> на вантаж та <a href="#messages" data-toggle="tab">0 повідомлень</a></p>-->
                </div>
                <div class="col-md-5">
                    <p class="pull-right">
                        Останній вхід в систему: <span class="lighter"> <?= date("d.m.Y G:i"); ?></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>