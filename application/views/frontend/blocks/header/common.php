<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<div class="container-fluid top">
    <div class="container center">
        <nav class="navbar navbar-default top">
            <div class="container-fluid">
                <div class="navbar-header top">
                </div>
                <div class="navbar-collapse collapse in" id="bs-example-navbar-collapse-1" aria-expanded="true">
                    <ul class="nav navbar-nav left">
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'system-description'])) ?>"><? __dbt('Опис системи') ?></a></li>
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'public-offer'])) ?>"><? __dbt('Публічна оферта ') ?></a></li>
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'private-policy'])) ?>"><? __dbt('Політика приватності') ?></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                                <span class="box-flag <?= $controller->locale->uri ?>"></span><?= $controller->locale->title ?> <i class="fa fa-angle-down fa-lg"></i>

                            </a>
                            <ul class="dropdown-menu">
                            <? foreach($controller->locales as $locale): ?>
                                <li><a href="<?= $controller->getCurrentUrl($locale->uri) ?>"><span class="box-flag <?= $locale->uri ?>"></span><?= $locale->title ?></a></li>
                            <? endforeach; ?>
                            </ul>
                        </li>

<!--                        <li class="dropdown">-->
<!--                            <a href="#" class="dropdown-toggle divider" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">-->
<!--                                <span class="box-flag ukr"></span>UAH Гривня <i class="fa fa-angle-down fa-lg"></i>-->
<!--                            </a>-->
<!--                            <ul class="dropdown-menu currency">-->
<!--                            </ul>-->
<!--                        </li>-->

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle divider" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="box-flag ua"></span><? __dbt('UAH Гривня') ?> <i class="fa fa-angle-down fa-lg"></i>
                            </a>
                            <ul class="dropdown-menu currency">
                                <li><a href="javascript:;"><span class="box-flag ua"></span> <? __dbt('UAH Гривня') ?></a></li>
                            </ul>
                        </li>

                        <? $controller->echoBlock('header/userbar') ?>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <?if(false){ /*        <TEST>      */ ?>
<!--        приклад роботи із konjax-->
    <div class="container center">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    <div class="row">
                        <div class="col-md-12">freeze whole body!</div>
                        <div class="col-md-12" data-freeze="all">
                            <button type="button" name="ajax.freeze" value="success">success</button>
                            <button type="button" name="ajax.freeze" value="warning">warning</button>
                            <button type="button" name="ajax.freeze" value="error">warning</button>
                            <button type="button" name="ajax.freeze" value="server_error">server_error</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-info">
                    <div class="row">
                        <div class="col-md-12">freeze this blocks!</div>
                        <div class="col-md-12" data-freeze="blocks">
                            <button type="button" name="ajax.freeze" value="success">success</button>
                            <button type="button" name="ajax.freeze" value="warning">warning</button>
                            <button type="button" name="ajax.freeze" value="error">warning</button>
                            <button type="button" name="ajax.freeze" value="server_error">server_error</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="alert alert-success">
                        <div id="userblock" style="display: block; height: 100px;">
                            userblock
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="alert alert-warning">
                        <div id="otherblock" style="display: block; height: 100px;">
                            otherblock
                        </div>
                    </div>
                </div>
                <div id="anotherblock" class="col-md-4">
                    <div class="alert alert-danger">
                        <div style="display: block; height: 100px;">
                            anotherblock
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <? /*        </TEST>      */ } ?>
    <div class="container center">
        <nav class="navbar navbar-default bottom">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?= $controller->links['to_main_page'] ?>">
                        <div class="logo-top"></div>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= $controller->links['cargo'] ?>"<?= ($controller->selectedMenuItem == 'cargo') ? ' class="selected-menu"' : '' ?>><?= __db('site.menu.header.cargo') ?></a></li>
                        <li><a href="<?= $controller->links['transport'] ?>"<?= ($controller->selectedMenuItem == 'transport') ? ' class="selected-menu"' : '' ?>><?= __db('site.menu.header.transport') ?></a></li>
                        <li><a href="<?= $controller->links['tenders'] ?>"<?= ($controller->selectedMenuItem == 'tenders') ? ' class="selected-menu"' : '' ?>><?= __db('site.menu.header.tenders') ?></a></li>
                        <li><a href="<?= $controller->links['companies_page'] ?>"<?= ($controller->selectedMenuItem == 'companies') ? ' class="selected-menu"' : '' ?>><?= __db('site.menu.header.companies') ?></a></li>
                        <li><a href="<?= $controller->links['news'] ?>"<?= ($controller->selectedMenuItem == 'news') ? ' class="selected-menu"' : '' ?>><?= __db('site.menu.header.press') ?></a></li>
                        <li><a href="<?= $controller->links['contacts'] ?>"<?= ($controller->selectedMenuItem == 'contacts') ? ' class="selected-menu"' : '' ?>><?= __db('kontakti') ?></a></li>
<!--                        <li>-->
<!--                            <a href="javascript:;">-->
<!--                                <form role="search" method="get" id="searchform" action="">-->
<!--                                    <label for="s">-->
<!--                                        <i class="fa fa-search"></i>-->
<!--                                    </label>-->
<!--                                    <input type="text" placeholder="Пошук" id="s" />-->
<!--                                </form>-->
<!--                            </a>-->
<!--                        </li>-->
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</div>