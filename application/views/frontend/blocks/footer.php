<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<div class="footer">
    <div class="container-fluid footer">
        <div class="container center">
            <div class="row black-bottom">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="logo-f">
                        <a href="/<?=$controller->locale->uri?>/">
                            <div class="logo-footer"></div>
                        </a>
                        <p>
                            <?=__db('footer.under_logo_text')?>
                        </p>
                        <div class="social-block">
                            <a href="#">
                                <i class="fa fa-facebook fa-lg"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-twitter fa-lg"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-vk fa-lg"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-2">
                    <div class="nav-title"><? __dbt('НАВІГАЦІЯ') ?></div>
                    <ul class="f-ul">
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'our-services'])) ?>"><? __dbt('Наші послуги') ?></a></li>
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'about'])) ?>"><? __dbt('Про проект') ?></a></li>
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'system-description'])) ?>"><? __dbt('Опис системи') ?></a></li>
                        <li><a href="<?= Route::url('frontend_static_page', $controller->getRequestParams(['uri' => 'public-offer'])) ?>"><? __dbt('Публічна оферта') ?></a></li>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#feedback"><? __dbt('Зворотній зв\'язок') ?></a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-2">
                    <div class="nav-title"></div>
                    <ul class="f-ul">
                        <li><a href="<?= $controller->links['transport'] ?>"><? __dbt('Транспорт') ?></a></li>
                        <li><a href="<?= $controller->links['cargo'] ?>"><? __dbt('Вантажі') ?></a></li>
                        <li><a href="<?= $controller->links['news'] ?>"><? __dbt('Прес-центр') ?></a></li>
                    </ul>
                </div>
                <div class="col-xs-4 visible-lg visible-sm">
<!--                    <img style="margin-top:40px; float: right;" src="http://placehold.it/200x200" />-->
                </div>
                <? if (! $controller->authorized): ?>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="nav-title"><? __dbt('РЕЄСТРАЦІЯ НА ПОРТАЛІ') ?></div>
                        <p class="white"><?     /*BLOCK_FIX_OF_CURRENT_LOCALE_START*/     $tmp_locale_var_original = 'Відчуйте переваги реєстрації на нашому порталі і станьте зареєстрованим учасником вже сьогодні
                        ';     $tmp_locale_var_const = Transliterate::factory($tmp_locale_var_original)->trim()->lowercase()->replaceSpaces("."); echo( __db($tmp_locale_var_const));          /*BLOCK_FIX_OF_CURRENT_LOCALE_END*/ ?></p>
                        <a href="<?= $controller->links['registration_page'] ?>"><div type="button" class="btn btn-primary registration-btn"><b><? __dbt('РЕЄСТРАЦІЯ НА ПОРТАЛІ') ?></b></div></a>
                    </div>
                <? endif; ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copy">
                        <div class="copyright-top-border"></div>
                        <div class="copyright">
                            <p><? __dbt('Copyright') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? $controller->echoBlock('footer/login-modal'); ?>

<? $controller->echoBlock('footer/feedback-modal'); ?>

<div class="modal fade" id="forgot-pass" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt('Відновлення паролю') ?></span></h4>
            </div>
            <form method="post" action="<?= $controller->links['forgot_pass'] ?>">
                <div class="modal-body">
                    <div class="form-title"><? __dbt('Ваш e-mail') ?></div>
                    <div class="form-group">
                        <input type="email" name="email-forgot" class="form-control" value="" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-primary"><? __dbt('Відправити') ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>