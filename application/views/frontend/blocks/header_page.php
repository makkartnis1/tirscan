<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--    <img style="position: absolute; left: -175px; top: 37px" src="http://placehold.it/160x280" />-->
<!--    <img style="position: absolute; right: -175px; top: 37px" src="http://placehold.it/160x280" />-->
</div>

<div class="header">
    <? $controller->echoBlock('header/common') ?>
    <div class="container-fluid bottom breadcrumbs">
        <div class="container center">
            <div class="row hero">
                <div class="col-md-4 hero" style="padding-left: 0; padding-right: 0;">
                    <div class="hero-text-block">
                        <span class="border-yellow"></span>
                        <p class="hero-text">
                            <? if (isset($controller->seo['title_for_header'])) { ?>
                            <?= $controller->seo['title_for_header'] ?>
                            <? } else { ?>
                            <?= $controller->l10n['title'] ?>
                            <? } ?>
                        </p>
                        <? $controller->echoBlock('breadcrumbs'); ?>
                    </div>
                </div>
                <div class="col-md-8 hero">
<!--                    <img src="http://placehold.it/625x60" />-->
                </div>
            </div>
        </div>
    </div>
</div>