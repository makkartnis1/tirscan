<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<ol class="breadcrumb page">
    <? foreach($controller->breadcrumbs->getLinks() as $link): ?>
        <li>
            <a href="<?= $link->getHref() ?>"<?= ($link->isActive() ? ' class="active"' : '') ?>><?= $link->getTitle() ?></a>
        </li>
    <? endforeach; ?>
</ol>