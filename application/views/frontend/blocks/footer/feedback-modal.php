<div class="modal fade" id="feedback" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"></span><? __dbt('Зворотній зв\'язок') ?></h4>
            </div>
            <form method="post" id="feedback-form">
                <div class="modal-body">
                    <div class="form-title"><? __dbt('Ваше ім\'я') ?></div>
                    <div class="form-group">
                        <input type="text" id="feedback-name" name="name" class="form-control" value="" placeholder="">
                    </div>
                    <div class="form-title"><? __dbt('Ваш e-mail') ?></div>
                    <div class="form-group">
                        <input type="email" id="feedback-email" name="email" class="form-control" value="<?= $email;?>" placeholder="">
                    </div>
                    <div class="form-title"><? __dbt('Повідомлення') ?></div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" id="feedback-message" placeholder=""></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <button id="feedback-send" type="submit" class="btn btn-primary"><? __dbt('Відправити') ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>