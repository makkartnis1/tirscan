<?
/**
 * @var $controller Controller_Frontend_Base
 * @var $email_value string
 */
?>

<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #27324b;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt('Авторизація') ?></span></h4>
            </div>
            <form method="post" action="<?= $controller->links['login'] ?>">
                <div class="modal-body">
                    <div class="form-title"><? __dbt('Ваш e-mail') ?></div>
                    <div class="form-group">
                        <input type="email" name="login-email" class="form-control" value="<?= $email_value ?>" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-title"><? __dbt('Ваш пароль') ?></div>
                    <div class="form-group">
                        <input type="password" name="login-password" class="form-control" id="exampleInputPassword1" placeholder="">
                    </div>
                    <div class="form-title box clearfix">
                        <label class="label--checkbox pull-left">
                            <input type="checkbox" name="remember-me" class="checkbox" checked>
                            <span class="gr black"><? __dbt('Запам\'ятати мене') ?></span>
                        </label>
                        <a class="modal-link pull-right" href="javascript:;" data-toggle="modal" data-dismiss="modal" data-target="#forgot-pass"><? __dbt('Забули пароль?') ?></a>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <button type="submit" name="action-login" class="btn btn-primary"><? __dbt('Увійти') ?></button>
                        </div>
                        <div class="col-sm-7">
                            <a href="<?= Route::url('frontend_site_company_registration', $controller->getRequestParams()) ?>"><button class="btn btn-primary" type="button"><? __dbt('Реєстрація') ?></button></a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="title-center" style="margin-bottom: 15px; font-size: 18px; font-weight: 700;"><? __dbt('Увійти за допомогою соцмережі') ?></div>
                    <div class="social-block">
                        <a href="javascript:;" class="social-link" data-target=".ulogin-button-facebook"><i class="fa fa-facebook fa-lg"></i></a>
                        <a href="javascript:;" class="social-link" data-target=".ulogin-button-twitter"><i class="fa fa-twitter fa-lg"></i></a>
                        <a href="javascript:;" class="social-link" data-target=".ulogin-button-vkontakte"><i class="fa fa-vk fa-lg"></i></a>
                        <a href="javascript:;" class="social-link" data-target=".ulogin-button-linkedin"><i class="fa fa-linkedin fa-lg"></i></a>
                        <div id="uLogin_edceb956" data-uloginid="edceb956" style="display: none;"></div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>