<?
/**
 * @var $pagination array
 */
?>

<? if (count($pagination['pages']) > 1): ?>
<nav class="text-center">
    <ul class="pagination">
        <? if ($pagination['arrows']['previous']['active']): ?>
        <li>
            <a href="<?= $pagination['arrows']['previous']['url'] ?>" aria-label="Previous">
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
            </a>
        </li>
        <? endif; ?>

        <? foreach($pagination['pages'] as $link): ?>
            <? if ($link['type'] == 'page') { ?>
                <li<?= ($link['active']) ? ' class="active"' : "" ?>>
                    <a href="<?= $link['url'] ?>"><?= $link['number'] ?></a>
                </li>
            <? } else if ($link['type'] == 'etc') { ?>
                <li>
                    <a><?= $link['title'] ?></a>
                </li>
            <? } ?>
        <? endforeach; ?>

        <? if ($pagination['arrows']['next']['active']): ?>
        <li>
            <a href="<?= $pagination['arrows']['next']['url'] ?>" aria-label="Next">
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </li>
        <? endif; ?>
    </ul>
</nav>
<? endif; ?>