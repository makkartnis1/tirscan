<?php
/**
 * @var array $stickers
 */
?>

<? if (count($stickers) > 0): ?>
<div class="row one-line">
    <div class="col-xs-12">
    <? foreach ($stickers as $sticker): ?>
        <a href="javascript:;" clear-target="<?= htmlspecialchars($sticker['clear']) ?>" class="sticker btn btn-info">
            <?= $sticker['text'] ?>
            <i class="sticker-close fa fa-times" aria-hidden="true"></i>
        </a>
    <? endforeach; ?>
    </div>
</div>
<? endif; ?>