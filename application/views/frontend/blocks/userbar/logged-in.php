<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<li class="dropdown logo-user" class="none">
    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= $controller->user->email ?>
    </a>
    <ul class="dropdown-menu user">
        <li id="my-messages"><a href="javascript:;"><? __dbt('МОЇ ПОВІДОМЛЕННЯ') ?><span count="message"><?php echo($count_messages)?></span><i class="fa fa-bars fa-lg"></i></a></li>
<!--        <li><a href="--><?//= $controller->links['profile_requests'] ?><!--"><? __dbt('МОЇ ЗАЯВКИ') ?><span>0</span><i class="fa fa-pie-chart fa-lg fa-flip-horizontal"></i></a></li>-->
        <li><a href="<?= $controller->links['profile_index'] ?>"><? __dbt('МІЙ ПРОФІЛЬ') ?><i class="fa fa-cog fa-lg"></i></a></li>
        <li><a href="<?= $controller->links['logout'] ?>"><? __dbt('ВИЙТИ З ПОРТАЛУ') ?><i class="fa fa-sign-out fa-lg"></i></a></li>
    </ul>
</li>
<li>
    <a href="#">
        <div class="numb"><sup count="message"><?php echo($count_messages)?></sup></div>
    </a>
</li>
<li><a href="<?= $controller->links['logout'] ?>"><i class="fa fa-sign-out fa-lg"></i><? __dbt('Вихід') ?></a></li>