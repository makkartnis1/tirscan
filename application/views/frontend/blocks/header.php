<?
/**
 * @var $controller Controller_Frontend_Site
 * @var $car_types Model_Frontend_User_Car_Type
 */
?>

<div style="position: relative; width: 960px; margin: auto;" class="visible-lg visible-sm">
<!--    <img style="position: absolute; left: -175px; top: 37px" src="http://placehold.it/160x210" />-->
<!--    <img style="position: absolute; right: -175px; top: 37px" src="http://placehold.it/160x210" />-->
</div>

<div class="header">
    <? $controller->echoBlock('header/common') ?>
    <div class="container-fluid middle">
        <div class="container center">
        </div>
    </div>
    <div class="container-fluid bottom">
        <div class="container center">
            <div class="row hero">
                <div class="col-md-12 hero">
                    <div class="hero-text-block">
                        <? if (! $controller->authorized){ ?>

                            <span class="border-yellow"></span>
                            <p class="hero-text"><? __dbt('НОВИЙ ТРАНСПОРТНИЙ АГРЕГАТОР') ?></p>

                            <a class="hero-text-regis" href="<?= $controller->links['registration_page'] ?>"><? __dbt('РЕЄСТРАЦІЯ') ?></a>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>