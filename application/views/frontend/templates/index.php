<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<!doctype html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="Description" content="<?= $controller->seo['description'] ?>">

    <title><?= $controller->seo['title'] ?></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900,300,100'>
    <link rel="stylesheet" type='text/css' href="/assets/css/bootstrap.css">
    <link rel="stylesheet" type='text/css' href="/assets/css/bootstrap_ms.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.3/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.css">
    <link rel="stylesheet" type='text/css' href="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css" />

    <? foreach ($controller->styles as $style): ?>
        <link rel="stylesheet" type='text/css' href="<?= $style ?>"/>
    <? endforeach; ?>

    <link rel="stylesheet" type='text/css' href="/assets/css/style.css" />
    <link rel="stylesheet" type='text/css' href="/assets/css/style_main.css" />
    <link rel="stylesheet" type='text/css' href="/assets/css/additionalStyle.css" />
    <!--[if lt IE 9]>
    <script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>
    <![endif]-->

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-82640553-2', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
</head>

<body>

<? $controller->echoBlock('header'); ?>

<? $controller->echoBlock('content'); ?>

<? $controller->echoBlock('footer'); ?>

<div class="modal fade transportOrderModal" id="transportOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a
                            href="#"></a></span></button>
                <h4 class="modal-title"><span class="ttl"><? __dbt("Заявка") ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 table-section-holder" data-table="here">
                        <?/* Динамічно грзутиься рядок із таблиці */?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><? __dbt("Додаткова інформація про заявку") ?></h3>
                            </div>
                            <div class="col-md-12" data-info >

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="orderModalMapHolder">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d162757.72582793544!2d30.392609105722805!3d50.402170238254264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4ee15a4505%3A0x764931d2170146fe!2z0JrQuNGX0LIsIDAyMDAw!5e0!3m2!1suk!2sua!4v1453978363646"
                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="modal-apply-area">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.css3finalize.min.js"></script>
<script type="text/javascript" src="/assets/js/retina.js"></script>
<script type="text/javascript" src="/assets/js/jquery.krid-select.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
<script src="/assets/js/jquery.easy-overlay.js" type="text/css"></script>

<script>
    $('#q').each(function () { // Notice the .each() loop, discussed below
        $(this).qtip({
            content: {
                text: $('#hidden') // Use the "div" element next to this for the content
            },
            show: {
                event: 'mouseover'
            },
            hide: {
                target: $('.close-q'),
                event: 'click'
            },
            position: {
                my: 'top left', // Position my top left...
                at: 'bottom center' // at the bottom right of...
            }
        });
    });
</script>
<script>
    $('.selectpicker').selectpicker({});
</script>
<script>
    $('.dropdown-toggle').dropdown();
</script>
<script type="text/javascript" src="/assets/js/owl.carousel.js"></script>
<script>
    $(document).ready(function () {

        var owl = $("#owl-demo");

        owl.owlCarousel({
            items: 1, //10 items above 1000px browser width
            itemsDesktop: [1000, 1], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 1], // betweem 900px and 601px
            itemsTablet: [600, 1], //2 items between 600 and 0
            itemsMobile: [480, 1] // itemsMobile disabled - inherit from itemsTablet option
        });

        // Custom Navigation Events
        $(".next").click(function () {
            owl.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        })
        $(".play").click(function () {
            owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
        })
        $(".stop").click(function () {
            owl.trigger('owl.stop');
        })

    });
</script>

<? $controller->echoBlock('js/init'); ?>

<? foreach ($controller->scripts as $script): ?>
    <? list($type, $template_id) = (substr(basename($script), 0, 3) == 'hb-') ? ['text/x-handlebars-template', substr_replace(basename($script) , '', strrpos(basename($script) , '.'))] : ['text/javascript',''] ?>
<script type="<?= $type ?>" id="<?= $template_id ?>" src="<?= $script ?>"></script>
<? endforeach; ?>

<script type="text/javascript" src="/assets/js/frontend/main/init.js"></script>

<script>
    $(function () {
        <? foreach ($controller->notifications as $note): ?>
        <?
            if ($note['locale_key'])
                $note['message'] = __db($note['message']);
        ?>
        toastr.<?= $note['type'] ?>("<?= str_replace('"', '\'', $note['message']) ?>");
        <? endforeach; ?>
    });
    window.tscan = {
        google : {
            key : '<?= Kohana::$config->load('frontend/site.google_API.key') ?>'
        }
    }
</script>

</body>

</html>