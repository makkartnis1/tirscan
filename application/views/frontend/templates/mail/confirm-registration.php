<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900,300,100'>
    <title>Tirscan</title>
    <style type="text/css">
        @font-face {
            font-family: 'Roboto';
        }

        body {
            font-family: 'Roboto';
        }

        * {
            font-family: 'Roboto';
        }

        a {
            color: #4A72AF;
        }

        body,
        #header h1,
        #header h2,
        p {
            margin: 0;
            padding: 0;
        }

        #main {
            border: 0px solid transparent;
            -webkit-box-shadow: 6px 6px 6px 1px rgba(0, 0, 0, .175);
            box-shadow: 1px 1px 15px 2px rgba(0, 0, 0, .175);
        }

        img {
            display: inline-block;
        }

        #top-message p,
        #bottom-message p {
            color: #3f4042;
            font-size: 12px;
            font-family: 'Roboto';
        }

        #header h1 {
            color: #ffffff !important;
            font-family: 'Roboto';
            font-size: 24px;
            margin-bottom: 0!important;
            padding-bottom: 0;
        }

        #header h2 {
            color: #ffffff !important;
            font-family: 'Roboto';
            font-size: 24px;
            margin-bottom: 0 !important;
            padding-bottom: 0;
        }

        #header p {
            color: #ffffff !important;
            font-family: 'Roboto';
            font-size: 13px;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin: 0 0 0.8em 0;
        }

        h3 {
            font-size: 28px;
            color: #444444 !important;
            font-family: 'Roboto';
        }

        h4 {
            padding-left: 30px;
            padding-right: 30px;
            margin-top: 30px;
            margin-bottom: 20px;
            font-size: 16px;
            font-weight: 400;
            color: #444444 !important;
            font-family: 'Roboto';
        }

        h5 {
            font-size: 18px;
            color: #444444 !important;
            font-family: 'Roboto';
        }

        p {
            padding-left: 30px;
            padding-right: 30px;
            font-size: 12px;
            color: #444444 !important;
            font-family: 'Roboto';
            line-height: 1.2;
            font-weight: 300;
        }

        span {
            font-size: 14px;
            display: inline-block;
            font-family: 'Roboto';
            position: relative;
        }

        #footer-t {
            background-color: #27324b;
            font-size: 12px;
        }

        #footer-t tr {
            background-color: #27324b;
            font-size: 12px;
        }

        #footer-b {
            border-top: 1px solid #dfdfdf;
            background-color: #27324b;
        }

        #content-1 {}

        b {
            font-weight: 500;
        }
        a{
            line-height: 1.2;
            font-weight: 300;
        }
    </style>

</head>

<body>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#fff">
    <tr>
        <td>
            <table id="top-message" cellpadding="20" cellspacing="0" width="560" align="center">

                <tr>

                    <td align="center">


                    </td>

                </tr>

            </table>
            <table id="main" width="560" align="center" cellpadding="0" cellspacing="0" bgcolor="ffffff">
                <tr>
                    <td>
                        <table id="header" height="5" cellpadding="0" cellspacing="0" align="center" bgcolor="#27324b">
                            <tr>
                            </tr>
                        </table>
                        <table id="header" height="100" cellpadding="0" cellspacing="0" align="center" bgcolor="#27324b">
                            <tr>
                                <td widht="247" height="100" bgcolor="#27324b">
                                    <p>
                                        <a href="#"><img src="http://demo14.biznes.com.ua/tirscan2/Tirscan/img/logof@2x.png" height="45" style="margin-top:-5px;"></a>
                                    </p>
                                </td>
                                <td width="400" height="100" align="left" bgcolor="#27324b">
                                    <p style="margin-top:3px;"><? __dbt("Новий транспортний") ?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-1" cellpadding="0" cellspacing="0" align="left" width="100%">
                            <tr>
                                <td>
                                    <h4><? __dbt("Доброго дня, ") ?><?= $username ?>!</h4>
                                </td>
                            </tr>
                        </table>
                        <table id="content-2" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:14px; margin-bottom: 20px;"><? __dbt("Ви отримали цей лист, тому що Ваша адреса електронної пошти була вказана при реєстрації на сайті транспортний агрегатор ") ?><a href="http://tirscan.com" style="text-decoration:none; font-weight:400;">TIRSCAN</a></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-3" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:14px; margin-bottom:20px;"><? __dbt("Щоб підтвердити реєстрацію, перейдіть за цим посиланням. Воно буде дійсне протягом двох годин з моменту отримання Вами листа.") ?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-4" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:20px; margin-bottom: 5px; width:540px; word-break: break-all;"><a href="<?= $link ?>" style="text-decoration:none;font-weight:400;"><?= $link ?></a></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-5" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="color: #707070!important; font-size:13px; margin-bottom: 20px; width:540px; word-break: keep-all"><? __dbt("Якщо посилання не відкривається, скопіюйте його і вставте у адресний рядок браузера") ?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-6" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:14px; margin-bottom: 15px;"><? __dbt("Підтвердження необхідно для виключення несанкціонованого використання Вашого e-mail. Для підтвердження достатньо перейти за посиланням, додаткових дій не потрібно.") ?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-7" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:14px; margin-bottom: 20px; width:500px; padding:20px;border:1px solid #f0f0f0; text-align:center;"><? __dbt("Для доступу до сайту використовуйте:") ?><br>
                                        <b>E-mail:</b> <a href="#" style="text-decoration:none;"><?= $email ?></a><? __dbt("та пароль, вказаний при реєстрації") ?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="content-8" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td>
                                    <p style="font-size:14px; margin-bottom: 35px;"><? __dbt("Якщо Ви не реєструвалися на ") ?><a href="http://tirscan.com" style="text-decoration:none">TIRSCAN</a>, то просто проігноруйте це повідомлення.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="footer-t" style="padding-top: 30px; padding-bottom:20px;">
                            <tr>
                                <td style="vertical-align:top">
                                    <table align="center">
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 30px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Додати заявку") ?></a></p>
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 30px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Транспорт") ?></a></p>
                                    </table>
                                </td>
                                <td style="vertical-align:top">
                                    <table align="center">
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Вантаж") ?></a></p>
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Оголошення") ?></a></p>
                                    </table>
                                </td>
                                <td style="vertical-align:top">
                                    <table align="center">
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Наші послуги") ?></a></p>
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Про проект") ?></a></p>
                                    </table>
                                </td>
                                <td style="vertical-align:top">
                                    <table align="center">
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Опис системи") ?></a></p>
                                        <p align="left" style="margin-bottom: 10px;padding-right:20px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Публічна оферта") ?></a></p>
                                    </table>
                                </td>
                                <td style="vertical-align:top">
                                    <table align="center">
                                        <p align="left" style="margin-bottom: 10px;padding-right:21px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Політика приватності") ?></a></p>
                                        <p align="left" style="margin-bottom: 10px;padding-right:21px; padding-left: 20px; font-size:10px;"><a href="#" style="color:fff;"><? __dbt("Новини компанії") ?></a></p>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="footer-b">
                            <tr>
                                <td widht="247" height="60" bgcolor="#27324b">
                                    <p style="position:relative; top:-2px;">
                                        <a href="#"><img src="http://demo14.biznes.com.ua/tirscan2/Tirscan/img/logof@2x.png" height="30"></a>
                                    </p>
                                </td>
                                <td width="400" height="60" align="right" bgcolor="#27324b">
                                    <p style="position:relative; top:-3px;">
                                        <a href="#" style="color:fff; text-decoration:none; font-size:11px;"></a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center">
                <tr>
                    <td align="center">
                        <p>© 2016, TIRSCAN - новий транспортний агрегатор</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>