<?
/**
 * @var $controller Controller_Frontend_Base
 */
?>

<!doctype html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="Description" content="<?= $controller->seo['description'] ?>">

    <title><?= $controller->seo['title'] ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900,300,100'>
    <link rel="stylesheet" type='text/css' href="/assets/css/bootstrap.css">
    <link rel="stylesheet" type='text/css' href="/assets/css/bootstrap_ms.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.3/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.css">
    <link rel="stylesheet" type='text/css' href="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css" />

    <? foreach ($controller->styles as $style): ?>
        <link rel="stylesheet" type='text/css' href="<?= $style ?>"/>
    <? endforeach; ?>

    <link rel="stylesheet" type='text/css' href="/assets/css/style.css" />
    <link rel="stylesheet" type='text/css' href="/assets/css/style_main.css" />
    <link rel="stylesheet" type='text/css' href="/assets/css/additionalStyle.css" />
    <link rel="stylesheet" type='text/css' href="/assets/css/partStyle.css" />
    <!--[if lt IE 9]>
    <script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>
    <![endif]-->

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-82640553-2', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
</head>

<body>

<?
    $controller->echoBlock('header');
    $controller->echoBlock('content');
    $controller->echoBlock('footer');
?>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.css3finalize.min.js"></script>
<script type="text/javascript" src="/assets/js/retina.js"></script>
<script type="text/javascript" src="/assets/js/jquery.krid-select.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
<script>
    $('.selectpicker').selectpicker({});
</script>
<script>
    $('.q').qtip({ // Grab some elements to apply the tooltip to
        content: {
            text: 'My common piece of text here'
        }
    })
</script>
<script>
    $('.dropdown-toggle').dropdown()
</script>
<script type="text/javascript" src="/assets/js/owl.carousel.js"></script>
<script>
    $(document).ready(function () {

        var owl = $("#owl-demo");

        owl.owlCarousel({
            items: 1, //10 items above 1000px browser width
            itemsDesktop: [1000, 1], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 1], // betweem 900px and 601px
            itemsTablet: [600, 1], //2 items between 600 and 0
            itemsMobile: [480, 1] // itemsMobile disabled - inherit from itemsTablet option
        });

        // Custom Navigation Events
        $(".next").click(function () {
            owl.trigger('owl.next');
        });
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        });
        $(".play").click(function () {
            owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
        });
        $(".stop").click(function () {
            owl.trigger('owl.stop');
        })

    });
</script>

<? $controller->echoBlock('js/init'); ?>

<? foreach ($controller->scripts as $script): ?>
    <? list($type, $template_id) = (substr(basename($script), 0, 3) == 'hb-') ? ['text/x-handlebars-template', substr_replace(basename($script) , '', strrpos(basename($script) , '.'))] : ['text/javascript',''] ?>
    <script type="<?= $type ?>" id="<?= $template_id ?>" src="<?= $script ?>"></script>
<? endforeach; ?>

<script type="text/javascript" src="/assets/js/frontend/main/init.js"></script>

<script>
    $(function () {
        <? foreach ($controller->notifications as $note): ?>
            toastr.<?= $note['type'] ?>("<?= str_replace('"', '\'', $note['message']) ?>");
        <? endforeach; ?>
    });
    window.tscan = {
        google : {
            key : '<?= Kohana::$config->load('frontend/site.google_API.key') ?>'
        }
    }
</script>

</body>

</html>