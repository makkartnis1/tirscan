<?
/**
 * @var $controller Controller_Frontend_Authorized
 */
?>

<script>

    Array.prototype.in_array = function(obj) {
        var i = this.length;
        while (i--) {
            if (this[i] == obj) {
                return true;
            }
        }
        return false;
    }

    window['l10n'] = <?= json_encode($controller->l10n) ?>;

    window.ssi = { //System Site Info
//        user: {
//            id: <?//= $controller->user->ID ?>
//        },
        locale: {
            uri: '<?= $controller->locale->uri ?>',
            id: <?= $controller->locale->ID ?>
        },

        authorized: <?= ($controller->authorized) ? '1' : '0' ?>,
        profile: {
            pass:{
                length: [<?= Controller_Frontend_Company::PASS_MIN_LENGTH ?>, <?= Controller_Frontend_Company::PASS_MAX_LENGTH ?>],
                regexp: <?= Controller_Frontend_Company::PASS_REGEX ?>//''
            }
        },
        requests: {
            transport: '<?= Route::url('frontend_site_request_transport_list', $controller->getRequestParams()) ?>',
            cargo: '<?= Route::url('frontend_site_request_cargo_list', $controller->getRequestParams()) ?>'
        },
        dialog: {
            confirmation: function (dialogID, message, value) {

                var content = $('<div>' +
                    message +
                    '</br>' +
                    '   <div id="'+dialogID+'">' +
                    '       <button class="btn btn-danger pull-left"   name="onConfirm" value="' + value + '" type="button">'      + i18n('btn.yes')   +    '</button>' +
                    '       <button class="btn btn-default pull-right" name="onDecline" value="' + value + '" type="button">'      + i18n('btn.no')    +    '</button>' +
                    '   </div>' +
                    '</div>');

                toastr.info(content.html());


            }
        },
        api:{
            messaging: {
                get_messages: '<?= Route::url('linker/api',['controller'=>'messaging', 'method'=>'get_messages']) ?>',
                send_message: '<?= Route::url('linker/api',['controller'=>'messaging', 'method'=>'send_message']) ?>',
                get_all_dialogs: '<?= Route::url('linker/api',['controller'=>'messaging', 'method'=>'get_all_dialogs']) ?>',
            },
            applications: {
                send: '<?= Route::url('linker/api',['controller'=>'applications', 'method'=>'send']) ?>',
                get: '<?= Route::url('linker/api',['controller'=>'applications', 'method'=>'get']) ?>'
            },
            
            profile:{
                changePWD: '<?= Route::url('linker/api',['controller'=>'pass',      'method'=>'change']) ?>',
                getInfo:   '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'info']) ?>',
                saveInfo:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'save']) ?>',
                addNews:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'add_news']) ?>',
                uploadImgNews:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'upload_image']) ?>',
                listNews:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'list_news']) ?>',
                oneNews:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'one_news']) ?>',
                generateManagerLink:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'generate_managers']) ?>'
            },
            tickets:{
                request_add_view: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'request_add_view']) ?>',

                get_transports: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_transport_requests']) ?>',
                get_transport_ticket_companies: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_transport_ticket_companies']) ?>',
                get_transport_propositions: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_transport_propositions']) ?>',
                set_transport_proposition: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'set_transport_proposition']) ?>',
                delete_transport_proposition: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'delete_transport_proposition']) ?>',
                close_transport_ticket: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'close_transport_ticket']) ?>',
                delete_transport_request: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'delete_transport_request']) ?>',

                get_cargo: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_cargo_requests']) ?>',
                get_cargo_ticket_companies: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_cargo_ticket_companies']) ?>',
                get_cargo_propositions: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_cargo_propositions']) ?>',
                set_cargo_proposition: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'set_cargo_proposition']) ?>',
                delete_cargo_proposition: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'delete_cargo_proposition']) ?>',
                close_cargo_ticket: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'close_cargo_ticket']) ?>',
                delete_cargo_request: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'delete_cargo_request']) ?>',

                request_order: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'request_order']) ?>',
                
                get_subscriptions: '<?= Route::url('linker/api',['controller'=>'requests', 'method'=>'get_subscriptions']) ?>',
            },
            filter:{
                templates: {
                    get: '<?= Route::url('krid/filter/api', ['controller'=>'templates', 'method'=>'get']) ?>',
                    update: '<?= Route::url('krid/filter/api', ['controller'=>'templates', 'method'=>'update']) ?>',
                    remove: '<?= Route::url('krid/filter/api', ['controller'=>'templates', 'method'=>'remove']) ?>',
                    activate_semafor: '<?= Route::url('krid/filter/api', ['controller'=>'templates', 'method'=>'activate_semafor']) ?>',
                    check_new: '<?= Route::url('krid/filter/api', ['controller'=>'templates', 'method'=>'check_new']) ?>'
                }
            },

            company:{
                saveInfo:  '<?= Route::url('linker/api',['controller'=>'profile',   'method'=>'save_my_company']) ?>',
                managers:   {
                    read:   '<?= Route::url('linker/api',['controller'=>'manager',  'method'=>'read']) ?>',
                    delete: '<?= Route::url('linker/api',['controller'=>'manager',  'method'=>'delete']) ?>'
                }
            },

            transport: {
                listMyTransport: '<?= Route::url('linker/api', ['controller' => 'transport', 'method' => 'show_mine']) ?>',
                transportEdit: '<?= Route::url('linker/api', ['controller' => 'transport', 'method' => 'details_mine']) ?>',
                deleteTransport: '<?= Route::url('linker/api', ['controller' => 'transport', 'method' => 'delete_mine']) ?>',
                updateTransport: '<?= Route::url('linker/api', ['controller' => 'transport', 'method' => 'update_mine']) ?>',
                setMainImage: '<?= Route::url('linker/api', ['controller' => 'transport', 'method' => 'update_car_main_image']) ?>'
            },

            wallet: {
                get_liqpay_form: '<?= Route::url('linker/api',['controller'=>'wallet', 'method'=>'get_liqpay_form']) ?>',
                get_history: '<?= Route::url('linker/api',['controller'=>'wallet', 'method'=>'get_history']) ?>'
            },

            messenger: {

                getDialog:   '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'get_dialog']) ?>',
                getDialogs:  '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'get_dialogs']) ?>',
                sendMessage: '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'send_message']) ?>',
                delMessage:  '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'del_message']) ?>',
                delDialog:   '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'del_dialog']) ?>',
                changeStatus:'<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'change_status']) ?>',
                getNewData:  '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'get_new_data']) ?>',
                createNewDialogSendMessage: '<?= Route::url('linker/api',['controller'=>'messenger',  'method'=>'new_dialog_and_message']) ?>'
            },

            feedback:{

                sendFeedback:  '<?= Route::url('linker/api',['controller'=>'feedback',  'method'=>'send_feedback']) ?>'
            },

            subscribe:{

                subscribeToNews:  '<?= Route::url('linker/api',['controller'=>'subscription',  'method'=>'send_subscribe']) ?>',
                getCompany: '<?= Route::url('linker/api',['controller'=>'subscription',  'method'=>'get_sub_company']) ?>',
                unSubscribe: '<?= Route::url('linker/api',['controller'=>'subscription',  'method'=>'un_subscribe']) ?>'
            },

            complaints: {

                sendComplaint:  '<?= Route::url('linker/api',['controller'=>'complaints',  'method'=>'send_complaint']) ?>',
                addToBlackList:  '<?= Route::url('linker/api',['controller'=>'complaints',  'method'=>'add_to_black_list']) ?>',
                delWithBlackList:  '<?= Route::url('linker/api',['controller'=>'complaints',  'method'=>'del_with_black_list']) ?>',
                getBlackList:  '<?= Route::url('linker/api',['controller'=>'complaints',  'method'=>'get_black_list']) ?>'
            },

            requestFeedback:{
            	
                get:            '<?= Route::url('linker/api',['controller'=>'request',  'method'=>'get_feedback']) ?>'
            },

            tender: {
                get:      '<?= Route::url('linker/api',['controller'=>'tender',  'method'=>'get']) ?>',
                getMy:      '<?= Route::url('linker/api',['controller'=>'tender',  'method'=>'getMy']) ?>',
                delete:      '<?= Route::url('linker/api',['controller'=>'tender',  'method'=>'delete']) ?>',
                stop:      '<?= Route::url('linker/api',['controller'=>'tender',  'method'=>'stop']) ?>',
                changes:      '<?= Route::url('linker/api',['controller'=>'tender',  'method'=>'changes']) ?>'
            },

            partner: {
                list:               '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'load_list']) ?>',
                decline:            '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'decline']) ?>',
                news_list:          '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'get_news']) ?>',
                news_update:        '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'update_news']) ?>',
                get_one_news:       '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'get_one_news']) ?>',
                accept:             '<?= Route::url('linker/api',['controller'=>'partner',  'method'=>'confirm']) ?>'
            },
            statistic: {
                feedback:               '<?= Route::url('linker/api',['controller'=>'statistic',  'method'=>'feedback']) ?>',
                request:{
                    transport:          '<?= Route::url('linker/api',['controller'=>'statistic',  'method'=>'transport']) ?>',
                    cargo:              '<?= Route::url('linker/api',['controller'=>'statistic',  'method'=>'cargo']) ?>'
                }
            },
            i18n:{
                variables: [],
                url:'<?= Route::url('linker/api',['controller'=>'i18n',  'method'=>'create']) ?>'
            }
        },

        debug: true,
        log: function(){
            if( this.debug === true )
                console.log.apply(console, arguments);
        }, 

        error: function(){
            if( this.debug === true )
                console.error.apply(console, arguments);

        },


        file_uploader_profiles: <?= json_encode( Kohana::$config->load('uploader/settings')->as_array() ) ?>

    };

    function i18n(key){

        var i18n = <?= json_encode( (I18n::$db_18in === false) ? [] : I18n::$db_18in ) ?>;

        var result = key;

        if(typeof i18n[key] != 'undefined'){

            result = i18n[key]['translation'];

        }else{

            if( !window.ssi.api.i18n.variables.in_array(key) ){

                window.ssi.api.i18n.variables.push(key);

            }

        }

        if(typeof arguments[1] == typeof {}){

            $.each(arguments[1], function(search, replace){

                result = result.split(search).join(replace);

            });

        }
        
        return result;

    }

    <? if (false) { ?>
    window.onbeforeunload =  function(e){

        var message = "Все ок! нових мовних змінних не знайдено! Підтверджуй перезавантаження сторінки!";

        if( window.ssi.api.i18n.variables.length > 0 ){

            var vars = window.ssi.api.i18n.variables;

                $.post(window.ssi.api.i18n.url, {
                    data: { i18n: window.ssi.api.i18n.variables, loc_id: window.ssi.locale.id }
                },'json').complete(function(){

                    alert('Вродь збережено, звалюй із сторінки куди хотів');

                    window.ssi.api.i18n.variables = [];

                });



            var message = "ПІЖДИ 2 СЕК, поки збережуться мовні змінні!: \r\n \r\n" + vars.join("\r\n");

        }

        if (typeof e == "undefined") {
            e = window.event;
        }

        if (e) {
            e.returnValue = message;
        }

        return message;


    };
    <? } ?>
    
    function i18n_validation(row) {

        if(row[0] == 'regex'){

            row[1][0] = i18n( row[1][0] );

        }

        var prefix = (typeof  arguments[1] == typeof '') ? arguments[1] + '.' : '';
        prefix = '';

        //"phone":  ROW === ["not_empty",[""]],
        //"hash":   ROW === ["regex",["123123","\/^[a-zA-Z][a-zA-Z0-9!@#$%_]+$\/"]]

        var replacement = {};

        $.each(row[1], function (i, value) {
            replacement[':'+i] = value;
        });

        return i18n( 'form.error.' + prefix  +row[0], replacement);
    }




</script>

<?= /**
 *  LINKeR:>> Блок нижче відповідає за динамічну загрузку файлів на сервер. Як ним корисутватись -
 * @see Frontend_Uploader
 * @see Controller_Frontend_File
 *    
 */
    View::factory('linker/uploader/init');
?>
<?= /**
 * LINKeR:>>  Блок генерації handlebars пагінації для JS використання лише
 */
 View::factory('frontend/content/profile/tab/transport/handlebars/pagination'); ?>

