<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
    $(function(){

        set_value("dateFromLoad","<?= Arr::get($post, 'dateFromLoad', '') ?>");
        set_value("dateToLoad","<?= Arr::get($post, 'dateToLoad', '') ?>");

        var loads = <?= json_encode(Arr::get($post, 'loads', '')) ?>;
        if (loads) set_load("start","load",loads);

        var unloads = <?= json_encode(Arr::get($post, 'unloads', '')) ?>;
        if (unloads) set_load("end","unload",unloads);

        set_value("typeCargo","<?= Arr::get($post, 'typeCargo', '') ?>");
        set_value("weightCargo","<?= Arr::get($post, 'weightCargo', '') ?>");
        set_value("volumeCargo","<?= Arr::get($post, 'volumeCargo', '') ?>");
        set_value("carType","<?= Arr::get($post, 'carType', '') ?>");
        set_value("truckQnt","<?= Arr::get($post, 'truckQnt', '') ?>");
        set_value("carLength","<?= Arr::get($post, 'carLength', '') ?>");
        set_value("carWidth","<?= Arr::get($post, 'carWidth', '') ?>");
        set_value("carHeight","<?= Arr::get($post, 'carHeight', '') ?>");
        set_value("dateFromTender","<?= Arr::get($post, 'dateFromTender', '') ?>");
        set_value("dateToTender","<?= Arr::get($post, 'dateToTender', '') ?>");
        set_value("timeFromTender","<?= Arr::get($post, 'timeFromTender', '') ?>");
        set_value("timeToTender","<?= Arr::get($post, 'timeToTender', '') ?>");
        set_value("startPrice","<?= Arr::get($post, 'startPrice', '') ?>");
        set_value("currency","<?= Arr::get($post, 'currency', '') ?>");

        set_checked("priceHide","<?= Arr::get($post, 'priceHide', '') ?>");

        set_value("companyYears","<?= Arr::get($post, 'companyYears', '') ?>");
        set_checked("filterCompanyYears","<?= Arr::get($post, 'filterCompanyYears', '') ?>");
        set_checked("filterForwarder","<?= Arr::get($post, 'filterForwarder', '') ?>");
        set_checked("filterPartners","<?= Arr::get($post, 'filterPartners', '') ?>");
        set_checked("active","<?= Arr::get($post, 'active', '') ?>");
        set_checked("rules","<?= Arr::get($post, 'rules', '') ?>");

        set_checked("doc_TIR","<?= Arr::get($post, 'doc_TIR', '') ?>");
        set_checked("doc_CMR","<?= Arr::get($post, 'doc_CMR', '') ?>");
        set_checked("doc_T1","<?= Arr::get($post, 'doc_T1', '') ?>");
        set_checked("doc_sanpassport","<?= Arr::get($post, 'doc_sanpassport', '') ?>");
        set_checked("doc_sanbook","<?= Arr::get($post, 'doc_sanbook', '') ?>");
        set_checked("load_side","<?= Arr::get($post, 'load_side', '') ?>");
        set_checked("load_top","<?= Arr::get($post, 'load_top', '') ?>");
        set_checked("load_behind","<?= Arr::get($post, 'load_behind', '') ?>");
        set_checked("load_tent","<?= Arr::get($post, 'load_tent', '') ?>");
        set_checked("cond_plomb","<?= Arr::get($post, 'cond_plomb', '') ?>");
        set_checked("cond_reload","<?= Arr::get($post, 'cond_reload', '') ?>");
        set_checked("cond_belts","<?= Arr::get($post, 'cond_belts', '') ?>");
        set_checked("cond_removable_stands","<?= Arr::get($post, 'cond_removable_stands', '') ?>");
        set_checked("cond_bort","<?= Arr::get($post, 'cond_bort', '') ?>");
        set_checked("cond_collectable","<?= Arr::get($post, 'cond_collectable', '') ?>");
        set_checked("t","<?= Arr::get($post, 't', '') ?>");
        set_checked("pallets","<?= Arr::get($post, 'pallets', '') ?>");
        set_checked("ADR","<?= Arr::get($post, 'ADR', '') ?>");

        set_value("info","<?= Arr::get($post, 'info', '') ?>");

        show_hidden_fields("t_amount","<?= (Arr::get($post, 't_amount', '')) ? Arr::get($post, 't_amount', '') : '' ?>");
        show_hidden_fields("pallets_amount","<?= (Arr::get($post, 'pallets_amount', '')) ? Arr::get($post, 'pallets_amount', '') : '' ?>");
        show_hidden_fields("ADR_amount","<?= (Arr::get($post, 'ADR_amount', '')) ? Arr::get($post, 'ADR_amount', '') : '' ?>");

        set_additional_text();

        var errors = <?= json_encode($errors) ?>;
        set_errors(errors);

    });
</script>