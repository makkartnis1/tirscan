<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tirscan</title>

    <style>
        .error-block {
            position: absolute;
            width: 413px;
            height: 150px;
            top: 50%;
            left: 50%;
            margin: -80px 0 0 -206px;

            background-color: #F2F2F2;
        }

        .error-logo {
            margin: 20px 100px;
        }

        .error-message {
            text-align: center;
            font-family: Tahoma, Geneva, sans-serif;
        }
    </style>
    <!--[if lt IE 9]>
    <script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>
    <![endif]-->
</head>

<body>
    <div class="error-block">
        <div class="error-logo">
            <img src="/assets/img/logo.png">
        </div>
        <div class="error-message">
            <p>500 - Помилка сервера!<br><a href="#"><? __dbt("на головну") ?></a></p>
        </div>
    </div>
</body>

</html>