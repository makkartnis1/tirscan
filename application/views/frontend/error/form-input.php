<?php
/**
 * @var $errors array
 */
?>

<ul class="errorText">
    <li><?= __db('form.error.' . $errors[0], $errors[1]) ?></li>
</ul>