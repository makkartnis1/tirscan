<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>


<div class="panel-body">
    <div class="adv-table">

        <div class="main_search">
            <button class="btn btn-primary main_search_btn" id="search_transport_widget_btn">Шукати</button>
            <input type="text" class="form-control main_search_input" name="search_transport_widget" placeholder="Пошук">
        </div>

        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport_w">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Номер</th>
                <th>Характеристики</th>
            </tr>
            </thead>
        </table>

    </div>
</div>


<script type="text/javascript">

    $(function(){

        window.user_id_tr = '';

        window.table_data = {
            ajax: {
                url: "/component_cms/<?=$lang?>/transport/db_widget/",
                type: "POST",
                data : {
                    get: {user_id:window.user_id_tr}
                }
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        var number = row.number;
                        var volume = row.volume;
                        if (!volume) volume = '-';
                        var liftingCapacity = row.liftingCapacity;
                        if (!liftingCapacity) liftingCapacity = '-';
                        var sizeX = row.sizeX;
                        if (!sizeX) sizeX = '-';
                        var sizeY = row.sizeY;
                        if (!sizeY) sizeY = '-';
                        var sizeZ = row.sizeZ;
                        if (!sizeZ) sizeZ = '-';
                        var car_type = row.car_type;
                        var type = 'вантажівка';
                        if (row.type == 'napiv prychip') type = 'напівпричіп';
                        if (row.type == 'z prychipom') type = 'з причепом';
                        var text = car_type+',&nbsp;'+type+', '+sizeX+'x'+sizeY+'x'+sizeZ+', '+liftingCapacity+'т&nbsp;/&nbsp;'+volume+'м3'+',<br>номер машини: '+number;

                        return '<input type="radio" name="select_one_tr_w" class="select_one_tr_w" data-id="'+row.ID+'" data-name="'+text+'">';
                    }
                },
                {
                    className: "center",data:"ID",
                    render: function (val, type, row) {
                        return '<div class="user-name">'+row.ID+'</div>';
                    }
                },
                {
                    className: "center",data:"number"
                },
                {
                    className: "center",data:"characters",
                    render: function (val, type, row) {
                        var volume = row.volume;
                        if (!volume) volume = '-';
                        var liftingCapacity = row.liftingCapacity;
                        if (!liftingCapacity) liftingCapacity = '-';
                        var sizeX = row.sizeX;
                        if (!sizeX) sizeX = '-';
                        var sizeY = row.sizeY;
                        if (!sizeY) sizeY = '-';
                        var sizeZ = row.sizeZ;
                        if (!sizeZ) sizeZ = '-';
                        var car_type = row.car_type;
                        var type = 'вантажівка';
                        if (row.type == 'napiv prychip') type = 'напівпричіп';
                        if (row.type == 'z prychipom') type = 'з причепом';
                        var text = car_type+',&nbsp;'+type+'<br>'+sizeX+'x'+sizeY+'x'+sizeZ+'<br>'+liftingCapacity+'т&nbsp;/&nbsp;'+volume+'м3';
                        return text;
                    }
                }
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        };

    });

    $(document).on( "click", "#search_transport_widget_btn", function(e){
        e.preventDefault();
        go_search_transport_widget();
    });

    $('body').keyup(function(e){
        if(event.keyCode==13)
        {
            e.preventDefault();
            var focus = $("[name=search_transport_widget]").is( ":focus" );
            if (focus)
            {
                go_search_transport_widget();
            }

        }
    });

    function go_search_transport_widget()
    {
        var search = $("[name=search_transport_widget]").val();
        window.table_tr_w.search(search).draw(false);
    }

    function callback_action_tr_w()
    {
        if (window.table_tr_w) window.table_tr_w.destroy();
        window.table_data.ajax.data.get.user_id = window.user_id_tr;

        window.table_tr_w = $("#table_transport_w").DataTable(window.table_data);
    }



    $(document).on( "click", ".select_one_tr_w", function(){
        var c_count = 0;
        $(".select_one_tr_w").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });
    });

</script>


<script type="text/javascript">

    function set_all_ids_tr_w()
    {
        var ids = [];
        var names = [];
        $(".select_one_tr_w").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
                names.push($(this).data('name'));
            }
        });

        window.ids_tr_w = ids;
        window.names_tr_w = names;
    }

    function callback_itemAll_user()
    {
        $(".select_one_tr_w").prop('checked',false);
        callback_action_tr_w();
    }

    <?=$callback_function_view?>
    <?=$callback_function_btn?>

</script>
