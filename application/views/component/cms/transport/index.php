<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    #row_for_search_transport th
    {
        border-bottom: none;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                    <a href="<?=$add_url?>"><span class="btn btn-default"><i class="fa fa-plus"></i> Додати транспорт</span></a>
                    <div style="float: right; position: relative; z-index: 100;" class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Дії <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu" style="left:-105px;">
                            <li><a href="#" id="delete_all">Видалити</a></li>
                        </ul>
                    </div>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport">
                            <thead>
                            <tr id="row_for_search_transport">
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>ID</th>
                                <th>Користувач</th>
                                <th>Компанія</th>
                                <th style="min-width: 55px">Номер</th>
                                <th>Об'єм</th>
                                <th style="min-width: 140px">Вантажопідйомність</th>
                                <th style="min-width: 80px">Довжина</th>
                                <th style="min-width: 70px">Ширина</th>
                                <th style="min-width: 60px">Висота</th>
                                <th>Тип транспорта</th>
                                <th>Кузов</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function(){


    $('#row_for_search_transport th').each( function () {

        var ind = $(this).index();

        if (ind == 1 || ind == 2 || ind == 3 || ind == 4 || ind == 5 || ind == 6 || ind == 7 || ind == 8 || ind == 9 || ind == 12)
        {

            var pl = '';
            var width = '';
            if (ind == 1 || ind == 5 || ind == 6 || ind == 7 || ind == 8 || ind == 9)
            {
                pl = 'від-до';
                width = 'min-width:50px';
            }

            $(this).append( '<input index="'+ind+'" type="text" style="width:95%;'+width+'" placeholder="'+pl+'" />' );

        }else if(ind == 10)
        {
            $(this).append( '<select index="'+ind+'" style="width:95%;" class="search_select"><option value="">---</option><?
                    foreach($types as $t)
                    {
                        ?><option value="<?=$t['ID']?>"><?=$t['localeName']?></option><?
                    }
                ?></select>' );

        }else if(ind == 11 )
        {
            $(this).append( '<select index="'+ind+'" style="width:95%;" class="search_select"><option value="">---</option>' +
                '<option value="vantazhivka">Вантажівка</option>' +
                '<option value="napiv prychip">Напівпричіп</option>' +
                '<option value="z prychipom">З причепом</option>' +
                '</select>' );
        }

    } );

    window.table = $('#table_transport').DataTable( {
        ajax: {
            url: "/component_cms/<?=$lang?>/transport/db/",
            type: "POST",
            data : {
                get: <?=(!empty($get))?json_encode($get):'1'?>
            }
        },
        drawCallback: function() {
            $(".select_all").prop('checked',false);
        },
        deferRender : true,
        serverSide: true,
        processing: true,
        columns: [
            {
                "orderable": false,
                render: function (val, type, row) {
                    return '<input type="checkbox" class="select_one" data-id="'+row.ID+'" data-name="'+row.number+'">';
                }
            },
            {
                className: "center",data:"ID",
                render: function (val, type, row) {
                    return '<div class="user-name">'+row.ID+'</div>';
                }
            },
            {
                className: "center",data:"userID","orderable": false,
                render: function (val, type, row) {
                    return '<a target="_blank" class="link_name" href="/cms/uk/user/edit/'+row.userID+'/">'+row.user_name+'</a>';
                }
            },
            {
                className: "center",data:"company","orderable": false,
                render: function (val, type, row) {
                    return '<a target="_blank" class="link_name" href="/cms/uk/company/edit/'+row.companyID+'/">'+row.company+'</a>';
                }
            },
            {
                className: "center",data:"number"
            },
            {
                className: "center",data:"volume"
            },
            {
                className: "center",data:"liftingCapacity"
            },
            {
                className: "center",data:"sizeX"
            },
            {
                className: "center",data:"sizeY"
            },
            {
                className: "center",data:"sizeZ"
            },
            {
                className: "center",data:"carTypeID",
                render: function (val, type, row) {
                    return row.car_type;
                }
            },
            {
                className: "center",data:"type",
                render: function (val, type, row) {
                    var type = 'Вантажівка';
                    if (row.type == 'napiv prychip') type = 'Напівпричіп';
                    if (row.type == 'z prychipom') type = 'З причепом';
                    return type;
                }
            },
            {
                className: "center",
                "orderable": false,
                render: function (val, type, row) {
                    var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$edit_url?>'+row.ID+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                    res += '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-name="'+row.number+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                    return res+'</div>';
                }
            },
        ],
        "order": [[1, 'desc']],
        language: {
            processing:     "Завантажую..",
            search:         "Пошук&nbsp;:",
            lengthMenu:     "Записів на сторінці: _MENU_ ",
            info:           "Показано записів з _START_ по _END_ із _TOTAL_",
            infoEmpty:      "Записів немає",
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Даних немає",
            emptyTable:     "Даних немає",
            paginate: {
                first:      "Перша",
                previous:   "Назад",
                next:       "Вперед",
                last:       "Остання"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
            }
        }

    } );


    <?
        for($k=1;$k<=15;$k++)
        {
            ?>
            $("#row_for_search_transport th input[index=<?=$k?>], #row_for_search_transport th select[index=<?=$k?>]").on( 'keyup change', function () {
                window.table
                    .column(<?=$k?>)
                    .search(this.value)
                    .draw();
            } );
            <?
        }
    ?>


});


function callback_action()
{
    window.table.draw(false);
}


$(document).on( "click", ".select_one", function(){
    var c_count = 0;
    $(".select_one").each(function(){
        if ($(this).prop('checked'))
        {
            c_count++;
        }
    });

});

$(document).on( "click", ".select_all", function(){
    if ($(this).prop('checked'))
    {
        $(".select_one").prop('checked',true);
        $(".select_all").prop('checked',true);
    }
    else
    {
        $(".select_one").prop('checked',false);
        $(".select_all").prop('checked',false);
    }
});

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення транспорта</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення транспорта з номером "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("#name_del").html(name);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/transport/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення вибраного транспорту</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраного транспорту?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/transport/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>
