<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Транспорт успішно оновлено
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_userID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано користувача
    </div>
<?
}

if (isset($error_carTypeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано тип транспорта
    </div>
    <?
}

if (isset($error_type))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано тип кузова
    </div>
    <?
}

if (isset($error_number))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано номер транспорта
    </div>
    <?
}

if (isset($error_volume))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказаний об'єм
    </div>
    <?
}

if (isset($error_liftingCapacity))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано вантажопідйомність
    </div>
<?
}


if (isset($error_sizeX))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано довжину
    </div>
    <?
}


if (isset($error_sizeY))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано ширину
    </div>
    <?
}

if (isset($error_sizeZ))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано висоту
    </div>
    <?
}

if (isset($error_number_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Номер транспорта повинен бути числом більшим за нуль
    </div>
    <?
}


if (isset($error_volume_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Об'єм повинен бути числом більшим за нуль
    </div>
    <?
}


if (isset($error_liftingCapacity_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Вантажопідйомність повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeX_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Довжина повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeY_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Ширина повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeZ_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Висота повинна бути числом більшим за нуль
    </div>
    <?
}


?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


<div class="form-group">
    <label class="col-sm-2 control-label">Номер&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="number" type="text" class="form-control m-bot15" maxlength="45" placeholder="до 45 символів" required="" value="<?=$info['info']['number']?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Тип&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-4">
        <select name="carTypeID" class="form-control m-bot15" required="">
            <option value="">-Виберіть-</option>
            <?
            foreach($types as $t)
            {
                ?>
                <option <?=($t['ID']==$info['info']['carTypeID'])?'selected':''?> value="<?=$t['ID']?>"><?=$t['localeName']?></option>
                <?
            }
            ?>
        </select>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Тип кузова&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-4">
        <select name="type" class="form-control m-bot15" required="">
            <option value="">-Виберіть-</option>
            <option <?=('vantazhivka'==$info['info']['type'])?'selected':''?> value="vantazhivka">Вантажівка</option>
            <option <?=('napiv prychip'==$info['info']['type'])?'selected':''?> value="napiv prychip">Напівпричіп</option>
            <option <?=('z prychipom'==$info['info']['type'])?'selected':''?> value="z prychipom">З причіпом</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Вантажопідй. (Т)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="liftingCapacity" type="text" class="form-control m-bot15" maxlength="10" required="" value="<?=$info['info']['liftingCapacity']?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Об'єм (М<sup>3</sup>)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="volume" type="text" class="form-control m-bot15" maxlength="10" required="" value="<?=$info['info']['volume']?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Довжина (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeX" type="text" class="form-control m-bot15" maxlength="10" required="" value="<?=$info['info']['sizeX']?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Ширина (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeY" type="text" class="form-control m-bot15" maxlength="10" required="" value="<?=$info['info']['sizeY']?>">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Висота (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeZ" type="text" class="form-control m-bot15" maxlength="10" required="" value="<?=$info['info']['sizeZ']?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Власник</label>
    <div class="col-sm-10">
        <div class="btn btn-default btn_select_widget" id="select_user">Вибрати користувача</div>
        Закріплений користувач: <span class="widget_label" id="user_name"><?=(isset($info['info']['userName']))?$info['info']['userName']:'не вказано'?></span><span class="close_span" id="close_span_user"><?=(isset($info['info']['userName']))?'x':''?></span>
        <input type="hidden" name="userID" value="<?=(isset($info['info']['userID']))?$info['info']['userID']:''?>">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Додатково</label>
    <div class="col-sm-7">
        <textarea class="form-control" name="info" style="height: 150px"><?=$info['info']['info']?></textarea>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-sm-2">Фото</label>
    <div class="controls col-sm-2">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="gallery[]" class="default" multiple>
                                                </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
    <?
    if ($info['files']['gallery'])
    {
        ?>
        <div class="controls col-sm-8">
            Закріплені фото:
            <?
            $k=0;
            foreach($info['files']['gallery'] as $f)
            {
                $k++;
                ?>
                <span doc_id="sv_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/public/car/gallery/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                        </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="sv_<?=$k?>">x</span>
                <input doc_id="sv_<?=$k?>" type="hidden" name="isset_gallery[]" value="<?=$f?>" multiple="">
                <?
            }
            ?>
        </div>
    <?
    }
    ?>

</div>


<div class="form-group">
    <label class="control-label col-sm-2">Документи</label>
    <div class="controls col-sm-2">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="docs[]" class="default" multiple>
                                                </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
    <?
    if ($info['files']['docs'])
    {
        ?>
        <div class="controls col-sm-8">
            Закріплені документи:
            <?
            $k=0;
            foreach($info['files']['docs'] as $f)
            {
                $k++;
                ?>
                <span doc_id="s_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/private/car/docs/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                    </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="s_<?=$k?>">x</span>
                <input doc_id="s_<?=$k?>" type="hidden" name="isset_docs[]" value="<?=$f?>" multiple="">
            <?
            }
            ?>
        </div>
    <?
    }
    ?>

</div>


<div class="form-group">
    <label class="control-label col-sm-2">Фото номерів</label>
    <div class="controls col-sm-2">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="numbers[]" class="default" multiple>
                                                </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
    <?
    if ($info['files']['numbers'])
    {
        ?>
        <div class="controls col-sm-8">
            Закріплені фото номерів:
            <?
            $k=0;
            foreach($info['files']['numbers'] as $f)
            {
                $k++;
                ?>
                <span doc_id="n_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/private/car/numbers/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                    </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="n_<?=$k?>">x</span>
                <input doc_id="n_<?=$k?>" type="hidden" name="isset_numbers[]" value="<?=$f?>" multiple="">
            <?
            }
            ?>
        </div>
    <?
    }
    ?>

</div>


<button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_car", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#close_span_user").html("x");
                                }
                        });',
                        'user_type'=>'transport'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_car" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('не вказано');
    });

    $(document).on( "click", ".close_current_file", function(){
        var doc_id = $(this).attr('doc_id');
        $("span[doc_id="+doc_id+"]").remove();
        $("input[doc_id="+doc_id+"]").remove();
    });

</script>






