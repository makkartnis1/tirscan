<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Транспорт успішно додано
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_userID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано користувача
    </div>
<?
}

if (isset($error_carTypeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано тип транспорта
    </div>
    <?
}

if (isset($error_type))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано тип кузова
    </div>
    <?
}

if (isset($error_number))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано номер транспорта
    </div>
    <?
}

if (isset($error_volume))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказаний об'єм
    </div>
    <?
}

if (isset($error_liftingCapacity))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано вантажопідйомність
    </div>
<?
}


if (isset($error_sizeX))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано довжину
    </div>
    <?
}


if (isset($error_sizeY))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано ширину
    </div>
    <?
}

if (isset($error_sizeZ))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано висоту
    </div>
    <?
}

if (isset($error_number_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Номер транспорта повинен бути числом більшим за нуль
    </div>
    <?
}


if (isset($error_volume_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Об'єм повинен бути числом більшим за нуль
    </div>
    <?
}


if (isset($error_liftingCapacity_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Вантажопідйомність повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeX_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Довжина повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeY_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Ширина повинна бути числом більшим за нуль
    </div>
    <?
}

if (isset($error_sizeZ_val))
{
    ?>
    <div class="alert alert-danger fade in">
        Висота повинна бути числом більшим за нуль
    </div>
    <?
}


?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


<div class="form-group">
    <label class="col-sm-2 control-label">Номер&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="number" type="text" class="form-control m-bot15" maxlength="45" placeholder="до 45 символів" required="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Тип&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-4">
        <select name="carTypeID" class="form-control m-bot15" required="">
            <option value="">-Виберіть-</option>
            <?
            foreach($types as $t)
            {
                ?>
                <option value="<?=$t['ID']?>"><?=$t['localeName']?></option>
                <?
            }
            ?>
        </select>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Тип кузова&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-4">
        <select name="type" class="form-control m-bot15" required="">
            <option value="">-Виберіть-</option>
            <option value="vantazhivka">Вантажівка</option>
            <option value="napiv prychip">Напівпричіп</option>
            <option value="z prychipom">З причіпом</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Вантажопідй. (Т)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="liftingCapacity" type="text" class="form-control m-bot15" maxlength="10" required="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Об'єм (М<sup>3</sup>)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="volume" type="text" class="form-control m-bot15" maxlength="10" required="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Довжина (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeX" type="text" class="form-control m-bot15" maxlength="10" required="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Ширина (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeY" type="text" class="form-control m-bot15" maxlength="10" required="">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Висота (М)&nbsp;<span class="required_span">*</span></label>
    <div class="col-sm-7">
        <input name="sizeZ" type="text" class="form-control m-bot15" maxlength="10" required="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Власник</label>
    <div class="col-sm-10">
        <div class="btn btn-default btn_select_widget" id="select_user">Вибрати користувача</div>
        Закріплений користувач: <span class="widget_label" id="user_name">не вказано</span><span class="close_span" id="close_span_user"></span>
        <input type="hidden" name="userID" value="">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">Додатково</label>
    <div class="col-sm-7">
        <textarea class="form-control" name="info" style="height: 150px"></textarea>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-sm-2">Фото</label>
    <div class="controls col-sm-10">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="gallery[]" class="default" multiple>
                                                </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-sm-2">Документи</label>
    <div class="controls col-sm-10">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="docs[]" class="default" multiple>
                                                </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
</div>


<div class="form-group">
    <label class="control-label col-sm-2">Фото номерів</label>
    <div class="controls col-sm-10">
        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-default btn-file">
                                            <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                            <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                            <input type="file" name="numbers[]" class="default" multiple>
                                            </span>
            <span class="fileupload-preview" style="margin-left:5px;"></span>
            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
        </div>
    </div>
</div>


<button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_car", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#close_span_user").html("x");
                                }
                        });',
                        'user_type'=>'transport'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_car" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('не вказано');
    });

</script>






