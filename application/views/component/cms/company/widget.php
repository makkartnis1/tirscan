<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="panel-body">
    <div class="adv-table">

        <div class="main_search">
            <button class="btn btn-primary main_search_btn" id="search_company_widget_btn">Искать</button>
            <input type="text" class="form-control main_search_input" name="search_company_widget" placeholder="Поиск">
        </div>

        <table style="width:100% !important;" class="display table table-bordered table-striped" id="table_company">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Ім'я</th>
                <th>IPN</th>
                <th>Телефон(и)</th>
            </tr>
            </thead>
        </table>

    </div>
</div>


<script type="text/javascript">

    $(function(){

        window.table_company = $('#table_company').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/company/db/",
                type: "POST",
                data : {
                    filter: <?=(isset($filter))?json_encode($filter):'1'?>
                }
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                    return '<input type="radio" name="select_one_company" class="select_one_company" data-id="'+row.ID+'" data-name="'+row.name+'">';
                    }
                },
                {
                    className: "center",data:"ID",
                    render: function (val, type, row) {
                        return '<div class="user-name">'+row.ID+'</div>';
                    }
                },
                {
                    className: "center",data:"name","orderable": false
                },
                {
                    className: "center",data:"ipn","orderable": false
                },
                {
                    className: "center",data:"contacts","orderable": false,
                    render: function (val, type, row) {
                        var phone = (row.phone)?row.phone:'';
                        var phone2 = (row.phone2)?',<br>'+row.phone2:'';
                        var phones = phone+''+phone2;
                        if (phones) phones = phones+'<br>';
                        return phones;
                    }
                }
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Загружаю..",
                search:         "Поиск&nbsp;:",
                lengthMenu:     "Записей на странице: _MENU_ ",
                info:           "Показано записей с _START_ по _END_ из _TOTAL_",
                infoEmpty:      "Записей нет",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Таблица пустая",
                emptyTable:     "Таблица пустая",
                paginate: {
                    first:      "Первая",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Последняя"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


        $(document).on( "click", "#search_company_widget_btn", function(e){
            e.preventDefault();
            go_search();
        });

        $('body').keyup(function(e){
            if(event.keyCode==13)
            {
                e.preventDefault();
                var focus = $("[name=search_company_widget]").is( ":focus" );
                if (focus)
                {
                    go_search();
                }

            }
        });

    });

    function go_search()
    {
        var search = $("[name=search_company_widget]").val();
        window.table_company.search(search).draw(false);
    }

    function callback_action_company()
    {
        window.table_company.draw(false);
    }


    $(document).on( "click", ".select_one_company", function(){
        var c_count = 0;
        $(".select_one_company").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });


</script>


<script type="text/javascript">

    function set_all_ids_company()
    {
        var ids = [];
        var names = [];
        $(".select_one_company").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
                names.push($(this).data('name'));
            }
        });

        window.ids_company = ids;
        window.names_company = names;
    }

    function callback_itemAll_company()
    {
        $(".select_all_company").prop('checked',false);
        callback_action_company();
    }

    <?=$callback_function_view?>
    <?=$callback_function_btn?>

</script>
