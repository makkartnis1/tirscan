<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>
<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Профіль компанії успішно оновлено
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}

if (isset($error_country_places))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано країну / місто
    </div>
    <?
}

if (isset($error_userID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано користувача
    </div>
    <?
}

if (isset($error_typeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано тип компанії
    </div>
    <?
}


if (isset($error_ownershipTypeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано форму власності компанії
    </div>
    <?
}


if (isset($error_phoneCodeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано телефонний код
    </div>
    <?
}

if (isset($error_empty_ipn))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано номер компанії
    </div>
    <?
}


if (isset($error_empty_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Основну назву компанії не вказано
    </div>
    <?
}

if (isset($error_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Назва компанії може містити букви, пробіл і символ "-"
    </div>
    <?
}


if (isset($error_empty_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон не вказано
    </div>
    <?
}


if (isset($error_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон може містити цифри, пробіл і символи "(" та ")"
    </div>
    <?
}

if (isset($error_not_isset_address))
{
    ?>
    <div class="alert alert-danger fade in">
        Адресу компанії не вказано
    </div>
    <?
}

?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label class="col-sm-2 control-label">Країна/Місто&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-7">
            <input name="country" id="country" type="text" class="form-control m-bot15" required="" value="<?=$info['geo_name']?>">
            <input name="country_places" id="country_places" type="hidden" value="<?=$info['geo_ids']?>">
        </div>
    </div>

    <?
    foreach($langs as $l)
    {
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label">Назва компанії - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-7">
                <input name="name[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['name']:''?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Юридична адреса - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-7">
                <input name="address[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['address']:''?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Фактична адреса - <?=$l['title']?></label>
            <div class="col-sm-7">
                <input name="realAddress[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['realAddress']:''?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Додатково - <?=$l['title']?></label>
            <div class="col-sm-7">
                <textarea name="info[<?=$l['ID']?>]" class="form-control m-bot15"><?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['info']:''?></textarea>
            </div>
        </div>


    <?
    }
    ?>



    <div class="form-group">
        <label class="col-sm-2 control-label">IPN&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-7">
            <input name="ipn" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" required="" value="<?=$info['info']['ipn']?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 abton-form-checkbox-title">Підтверджено адміністрацією</label>
        <div class="col-sm-1 abton-form-checkbox">
            <div class="flat-grey single-row icheck form_checkbox">
                <input type="checkbox" name="approvedByAdmin" <?=($info['info']['approvedByAdmin']=='yes')?'checked':''?>>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2 abton-form-checkbox-title">Довірена компанія</label>
        <div class="col-sm-1 abton-form-checkbox">
            <div class="flat-grey single-row icheck form_checkbox">
                <input type="checkbox" name="trustableMark" <?=($info['info']['trustableMark']=='yes')?'checked':''?>>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Власник компанії</label>
        <div class="col-sm-10">
            <div class="btn btn-default btn_select_widget" id="select_user">Вибрати користувача</div>
            Закріплений користувач: <span class="widget_label" id="user_name"><?=(isset($info['info']['userName']))?$info['info']['userName']:'не вказано'?></span><span class="close_span" id="close_span_user"><?=(isset($info['info']['userName']))?'x':''?></span>
            <input type="hidden" name="userID" value="<?=(isset($info['info']['userID']))?$info['info']['userID']:''?>">
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Тип компанії&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-4">
            <select name="typeID" class="form-control m-bot15" required="">
                <option value="">-Виберіть-</option>
                <?
                foreach($types as $t)
                {
                    ?>
                    <option <?=($info['info']['typeID'] == $t['ID'])?'selected':''?> value="<?=$t['ID']?>"><?=$t['name']?></option>
                <?
                }
                ?>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Форма власності&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-4">
            <select name="ownershipTypeID" class="form-control m-bot15" required="">
                <option value="">-Виберіть-</option>
                <?
                foreach($owner_types as $o)
                {
                    ?>
                    <option <?=($info['info']['ownershipTypeID'] == $o['ID'])?'selected':''?> value="<?=$o['ID']?>"><?=$o['name']?></option>
                <?
                }
                ?>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Телефон&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-2">
            <select name="phoneCodeID" class="form-control m-bot15" required="">
                <option value="">-Код-</option>
                <?
                foreach($phone_codes as $p_c)
                {
                    ?>
                    <option <?=($info['info']['phoneCodeID'] == $p_c['ID'])?'selected':''?> value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                <?
                }
                ?>
            </select>
        </div>
        <div class="col-sm-5">
            <input name="phone" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" required="" value="<?=$info['info']['phone']?>">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Стаціонарний телефон</label>
        <div class="col-sm-2">
            <select name="phoneStationaryCodeID" class="form-control m-bot15">
                <option value="">-Код-</option>
                <?
                foreach($phone_codes as $p_c)
                {
                    ?>
                    <option <?=($info['info']['phoneStationaryCodeID'] == $p_c['ID'])?'selected':''?> value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                <?
                }
                ?>
            </select>
        </div>
        <div class="col-sm-5">
            <input name="phoneStationary" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" value="<?=$info['info']['phoneStationary']?>">
        </div>
    </div>


    <div class="form-group">

        <label class="control-label col-sm-2">Лого компанії</label>

        <div class="col-sm-3">
            <div class="fileupload fileupload-new" data-provides="fileupload">

                <?
                $file=$_SERVER['DOCUMENT_ROOT'].'/uploads/images/company/'.$info['info']['ID'].'.jpg';
                if (file_exists($file))
                {
                    $img = '/uploads/images/company/'.$info['info']['ID'].'.jpg';
                }
                ?>

                <div id="no_image" class="fileupload-new thumbnail" style="width: 200px; height: 150px; <?=(isset($img))?'display:none;':''?>">
                    <img src="/public/cms/img/no_image_small.png">
                </div>
                <div id="image_ex" class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px; <?=(isset($img))?'display:block;':''?>">
                    <?
                    if (isset($img))
                    {
                        ?>
                        <img src="<?=$img?>" style="max-height: 150px;">
                    <?
                    }
                    ?>
                </div>
                <div style="width: 203px;">
                                                   <span id="reload_file" class="btn btn-default btn-file">
                                                   <span id="load_file" style="display:none;" class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати</span>
                                                   <span style="display:inline-block;" class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img" onchange="setStatus()">
                                                   </span>

                    <a id="del_file" style="display:inline-block;" class="btn btn-danger fileupload-exists"><i class="fa fa-trash"></i> Видалити</a>
                </div>
            </div>
        </div>

    </div>
    <?
    $is_file = (file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads/images/company/'.$info['info']['ID'].'.jpg'))?1:0;
    ?>
    <input type="hidden" name="file_exist" value="<?=$is_file?>">



    <div class="form-group">
        <label class="control-label col-sm-2">Свідоцтво про реєстрацію, як суб’єкта підприємницької діяльності</label>
        <div class="controls col-sm-2">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="sertifikaty[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
        <?
        if ($info['files']['sertifikaty'])
        {
            ?>
            <div class="controls col-sm-8">
                Закріплені документи:
                <?
                $k=0;
                foreach($info['files']['sertifikaty'] as $f)
                {
                    $k++;
                    ?>
                    <span doc_id="sv_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/private/company/sertifikaty/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                        </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="sv_<?=$k?>">x</span>
                    <input doc_id="sv_<?=$k?>" type="hidden" name="isset_sertifikaty[]" value="<?=$f?>" multiple="">
                <?
                }
                ?>
            </div>
        <?
        }
        ?>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Податкові документи</label>
        <div class="controls col-sm-2">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="podatkovi[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
        <?
            if ($info['files']['podatkovi'])
            {
                ?>
                <div class="controls col-sm-8">
                        Закріплені документи:
                        <?
                        $k=0;
                        foreach($info['files']['podatkovi'] as $f)
                        {
                            $k++;
                            ?>
                            <span doc_id="p_<?=$k?>" class="widget_label">
                                <a href="/uploads/files/private/company/podatkovi/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                            </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="p_<?=$k?>">x</span>
                            <input doc_id="p_<?=$k?>" type="hidden" name="isset_podatkovi[]" value="<?=$f?>" multiple="">
                            <?
                        }
                        ?>
                </div>
                <?
            }
        ?>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Ліцензія на транспортні або експедиторські послуги</label>
        <div class="controls col-sm-2">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="license-transport-and-expedition[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
        <?
        if ($info['files']['license-transport-and-expedition'])
        {
            ?>
            <div class="controls col-sm-8">
                Закріплені документи:
                <?
                $k=0;
                foreach($info['files']['license-transport-and-expedition'] as $f)
                {
                    $k++;
                    ?>
                    <span doc_id="s_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/private/company/license-transport-and-expedition/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                    </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="s_<?=$k?>">x</span>
                    <input doc_id="s_<?=$k?>" type="hidden" name="isset_license-transport-and-expedition[]" value="<?=$f?>" multiple="">
                <?
                }
                ?>
            </div>
        <?
        }
        ?>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Свідоцтво про державну реєстрацію</label>
        <div class="controls col-sm-2">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="cert-state-reg[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
        <?
        if ($info['files']['cert-state-reg'])
        {
            ?>
            <div class="controls col-sm-8">
                Закріплені документи:
                <?
                $k=0;
                foreach($info['files']['cert-state-reg'] as $f)
                {
                    $k++;
                    ?>
                    <span doc_id="ser_<?=$k?>" class="widget_label">
                        <a href="/uploads/files/private/company/cert-state-reg/<?=$info['info']['ID']?>/<?=$f?>"><?=$f?></a>
                    </span><span style="margin-right: 10px" class="close_span close_current_file" doc_id="ser_<?=$k?>">x</span>
                    <input doc_id="ser_<?=$k?>" type="hidden" name="isset_cert-state-reg[]" value="<?=$f?>" multiple="">
                <?
                }
                ?>
            </div>
        <?
        }
        ?>
    </div>


    <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#del_file", function(){
        $("#no_image").show();
        $("#image_ex").hide();
        $("[name=file_exist]").val('0');

    });

    $(document).on( "click", "#reload_file", function(){
        $("#image_ex").show();
    });
</script>
<script type="text/javascript">
    function setStatus()
    {
        $("[name=file_exist]").val('1');
    }
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_company", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#close_span_user").html("x");
                                }
                        });',
                        'company_id'=>$info['info']['ID']
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_company" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('не вказано');
    });

    $(document).on( "click", ".close_current_file", function(){
        var doc_id = $(this).attr('doc_id');
        $("span[doc_id="+doc_id+"]").remove();
        $("input[doc_id="+doc_id+"]").remove();
    });

</script>

<script type="text/javascript">
    $(function(){
        google_place_init('country','uk');
    });
</script>


