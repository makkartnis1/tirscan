<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>
<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Компанія успішно створена
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_country_places))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано країну / місто
    </div>
    <?
}

if (isset($error_userID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано користувача
    </div>
    <?
}

if (isset($error_typeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано тип компанії
    </div>
    <?
}


if (isset($error_ownershipTypeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано форму власності компанії
    </div>
    <?
}


if (isset($error_phoneCodeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано телефонний код
    </div>
    <?
}

if (isset($error_empty_ipn))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано номер компанії
    </div>
    <?
}


if (isset($error_empty_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Основну назву компанії не вказано
    </div>
    <?
}

if (isset($error_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Назва компанії може містити букви, пробіл і символ "-"
    </div>
    <?
}


if (isset($error_empty_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон не вказано
    </div>
    <?
}


if (isset($error_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон може містити цифри, пробіл і символи "(" та ")"
    </div>
    <?
}

if (isset($error_not_isset_address))
{
    ?>
    <div class="alert alert-danger fade in">
        Адресу компанії не вказано
    </div>
    <?
}


if (isset($errors['new_user']))
{
    extract($errors['new_user'],EXTR_PREFIX_ALL,"user_error");
}

if (isset($user_error_pass))
{
    ?>
    <div class="alert alert-danger fade in">
        Довжина пароля користувача повинна бути не меншою ніж 6 символів. Пароль може містити цифри, букви латинського алфавіту і наступні символи "_-.,;"
    </div>
<?
}

if (isset($user_error_conf_pass))
{
    ?>
    <div class="alert alert-danger fade in">
        Пароль і підтвердження пароля користувача повинні співпадати
    </div>
<?
}

if (isset($user_error_email_isset))
{
    ?>
    <div class="alert alert-danger fade in">
        Вказана електронна пошта користувача уже зареєстрована в системі
    </div>
<?
}


if (isset($user_error_not_isset_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Ім'я користувача не може бути пустим
    </div>
<?
}

if (isset($user_error_name))
{
    ?>
    <div class="alert alert-danger fade in">
        Ім'я користувача може містити букви, пробіл і символ "-"
    </div>
<?
}

if (isset($user_error_phoneCodeID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано телефонний код користувача
    </div>
    <?
}

if (isset($user_error_empty_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон користувача не вказано
    </div>
    <?
}


if (isset($user_error_phone))
{
    ?>
    <div class="alert alert-danger fade in">
        Телефон користувача може містити цифри, пробіл і символи "(" та ")"
    </div>
    <?
}

?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label class="col-sm-2 control-label">Країна/Місто&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-7">
            <input name="country" id="country" type="text" class="form-control m-bot15" required="">
            <input name="country_places" id="country_places" type="hidden">
        </div>
    </div>

    <?
    foreach($langs as $l)
    {
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label">Назва компанії - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-7">
                <input name="name[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Юридична адреса - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-7">
                <input name="address[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Фактична адреса - <?=$l['title']?></label>
            <div class="col-sm-7">
                <input name="realAddress[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Додатково - <?=$l['title']?></label>
            <div class="col-sm-7">
                <textarea name="info[<?=$l['ID']?>]" class="form-control m-bot15"></textarea>
            </div>
        </div>

        <?
    }
    ?>



    <div class="form-group">
        <label class="col-sm-2 control-label">IPN&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-7">
            <input name="ipn" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 abton-form-checkbox-title">Підтверджено адміністрацією</label>
        <div class="col-sm-1 abton-form-checkbox">
            <div class="flat-grey single-row icheck form_checkbox">
                <input type="checkbox" name="approvedByAdmin">
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2 abton-form-checkbox-title">Довірена компанія</label>
        <div class="col-sm-1 abton-form-checkbox">
            <div class="flat-grey single-row icheck form_checkbox">
                <input type="checkbox" name="trustableMark">
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Власник компанії</label>
        <div class="col-sm-10">
            <div id="add_user_btn" class="btn btn-primary btn-submit-form">+ Створити нового</div>
            Закріплений користувач: <span class="widget_label" id="user_name">не вказано</span><span class="close_span" id="close_span_user"></span>
            <input type="hidden" name="userID">
        </div>
    </div>




    <script type="text/javascript">
        $(document).on( "click", "#add_user_btn", function(){
            $("#new_user_modal").modal();
        });
    </script>


    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="new_user_modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Профіль нового користувача</h4>
                    </div>
                    <div class="modal-body">
                        <?
                        foreach($langs as $l)
                        {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Ім'я - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
                                <div class="col-sm-9">
                                    <input name="new_user_info[name][<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів">
                                </div>
                            </div>
                        <?
                        }
                        ?>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-9">
                                <input name="new_user_info[email]" type="text" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-3">Email підтверджено</label>
                            <div class="col-sm-1 abton-form-checkbox">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" name="new_user_info[emailApproved]">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Телефон&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-2">
                                <select name="new_user_info[phoneCodeID]" class="form-control m-bot15">
                                    <option value="">-Код-</option>
                                    <?
                                    foreach($phone_codes as $p_c)
                                    {
                                        ?>
                                        <option value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input name="new_user_info[phone]" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Стаціонарний телефон</label>
                            <div class="col-sm-2">
                                <select name="new_user_info[phoneStationaryCodeID]" class="form-control m-bot15">
                                    <option value="">-Код-</option>
                                    <?
                                    foreach($phone_codes as $p_c)
                                    {
                                        ?>
                                        <option value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input name="new_user_info[phoneStationary]" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">ICQ</label>
                            <div class="col-sm-9">
                                <input name="new_user_info[icq]" type="text" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Skype</label>
                            <div class="col-sm-9">
                                <input name="new_user_info[skype]" type="text" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Пароль&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="new_user_info[pass]" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Повтор пароля&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="new_user_info[conf_pass]" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>



                        <div class="form-group">

                            <label class="control-label col-sm-3">Фото</label>

                            <div class="col-sm-3">

                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="/public/cms/img/no_image_small.png" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div style="width: 203px;">
                                                   <span class="btn btn-default btn-file" style="margin-left: 0px;">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Выбрати</span>
                                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="avatar">
                                                   </span>

                                        <a href="#" class="btn btn-danger fileupload-exists" style="margin-left: 0px;" data-dismiss="fileupload"><i class="fa fa-trash"></i> Видалити</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                        <button id="save_user" type="button" data-dismiss="modal" class="btn btn-success">Зберегти і повернутися</button>
                    </div>
                </div>
            </div>
        </div>


    <script type="text/javascript">
        $(document).on( "click", "#save_user", function(){
            $("#user_name").html('Буде створено новий');
        });
    </script>


    <div class="form-group">
        <label class="col-sm-2 control-label">Тип компанії&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-4">
            <select name="typeID" class="form-control m-bot15" required="">
                <option value="">-Виберіть-</option>
                <?
                foreach($types as $t)
                {
                    ?>
                    <option value="<?=$t['ID']?>"><?=$t['name']?></option>
                <?
                }
                ?>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Форма власності&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-4">
            <select name="ownershipTypeID" class="form-control m-bot15" required="">
                <option value="">-Виберіть-</option>
                <?
                foreach($owner_types as $o)
                {
                    ?>
                    <option value="<?=$o['ID']?>"><?=$o['name']?></option>
                <?
                }
                ?>
            </select>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">Телефон&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-2">
            <select name="phoneCodeID" class="form-control m-bot15" required="">
                <option value="">-Код-</option>
                <?
                foreach($phone_codes as $p_c)
                {
                    ?>
                    <option value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                <?
                }
                ?>
            </select>
        </div>
        <div class="col-sm-5">
            <input name="phone" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Стаціонарний телефон</label>
        <div class="col-sm-2">
            <select name="phoneStationaryCodeID" class="form-control m-bot15">
                <option value="">-Код-</option>
                <?
                foreach($phone_codes as $p_c)
                {
                    ?>
                    <option value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                <?
                }
                ?>
            </select>
        </div>
        <div class="col-sm-5">
            <input name="phoneStationary" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
        </div>
    </div>


    <div class="form-group">

        <label class="control-label col-sm-2">Лого компанії</label>

        <div class="col-sm-3">

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="/public/cms/img/no_image_small.png" alt="" />
                </div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                <div style="width: 203px;">
                                                   <span class="btn btn-default btn-file" style="margin-left: 0px;">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Выбрати</span>
                                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img">
                                                   </span>

                    <a href="#" class="btn btn-danger fileupload-exists" style="margin-left: 0px;" data-dismiss="fileupload"><i class="fa fa-trash"></i> Видалити</a>
                </div>
            </div>
        </div>

    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Свідоцтво про реєстрацію, як суб’єкта підприємницької діяльності</label>
        <div class="controls col-sm-10">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="sertifikaty[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Податкові документи</label>
        <div class="controls col-sm-10">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="podatkovi[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Ліцензія на транспортні або експедиторські послуги</label>
        <div class="controls col-sm-10">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="license-transport-and-expedition[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-sm-2">Свідоцтво про державну реєстрацію</label>
        <div class="controls col-sm-10">
            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-default btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати файли</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                <input type="file" name="cert-state-reg[]" class="default" multiple>
                                                </span>
                <span class="fileupload-preview" style="margin-left:5px;"></span>
                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
            </div>
        </div>
    </div>


    <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>

<script type="text/javascript">
    $(function(){
        google_place_init('country','uk');
    });
</script>




