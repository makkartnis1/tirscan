<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading" style="background: #eff0f4; padding: 0px 15px; border: none;">
                    <?=$base_title?>
                </header>
                <div class="panel-body" style="background: #eff0f4;">
                    <?
                    if (isset($change_ok))
                    {
                        ?>
                        <div class="alert alert-success fade in">
                            Шаблон сповіщення успішно змінено
                        </div>
                    <?
                    }
                    ?>
                    <form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data" style="background: white">
                        <section class="panel" style="margin-bottom: 30px;">
                            <header class="panel-heading custom-tab dark-tab">
                                <ul class="nav nav-tabs">
                                    <?
                                    $i=0;
                                    foreach($langs as $l)
                                    {
                                        $i++;
                                        ?>
                                        <li class="<?=($i==1)?'active':''?>">
                                            <a href="#lang_<?=$l['uri']?>" data-toggle="tab"><?=$l['title']?></a>
                                        </li>
                                    <?
                                    }
                                    ?>
                                </ul>
                            </header>
                            <div class="panel-body">
                                <div class="tab-content">

                                    <?
                                    $i=0;
                                    foreach($langs as $l)
                                    {
                                        $i++;
                                        ?>
                                        <div class="tab-pane <?=($i==1?'active':'')?>" id="lang_<?=$l['uri']?>">

                                            <div class="form-group">
                                                <label class="col-sm-1 control-label">Заголовок <?=strtoupper($l['uri'])?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="title_<?=$l['uri']?>" id="title_<?=$l['uri']?>" value="<?=$info['locales'][$l['ID']]['title']?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-1 control-label">Шаблон <?=strtoupper($l['uri'])?></label>
                                                <div class="col-sm-10">
                                                    <textarea rows="5" class="form-control" name="text_<?=$l['uri']?>" id="text_<?=$l['uri']?>"><?=$info['locales'][$l['ID']]['text']?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    <?
                                    }
                                    ?>


                                    <div class="form-group" style="margin-top:10px">
                                        <label class="col-sm-1 control-label">Ключ</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control" value="<?=$info['info']['name']?>">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>


                        <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary">Зберегти</button>
                    </form>

                </div>
            </section>
        </div>
    </div>
</div>
