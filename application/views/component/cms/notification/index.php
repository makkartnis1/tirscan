<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                    <a href="<?=$add_url?>"><span class="btn btn-default"><i class="fa fa-plus"></i> Додати шаблон</span></a>

                </header>

                <div class="panel-body">
                    <div class="adv-table">


                        <button class="btn btn-default btn-actions" id="delete_all">Пакетне видалення</button>


                        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>№</th>
                                <th>Ключ</th>
                                <th>Заголовок</th>
                                <th>Шаблон</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/notification/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one" data-id="'+row.id+'" data-name="'+row.name+'">';
                    }
                },
                {
                    className: "center",data:"id",orderable: false
                },
                {
                    className: "center",data:"name",orderable: false
                },
                {
                    className: "center",data:"title",orderable: false
                },
                {
                    className: "center",data:"text",orderable: false
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$edit_url?>'+row.id+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.id+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Загружаю..",
                search:         "Поиск&nbsp;:",
                lengthMenu:     "Записей на странице: _MENU_ ",
                info:           "Показано записей с _START_ по _END_ из _TOTAL_",
                infoEmpty:      "Записей нет",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Таблица пустая",
                emptyTable:     "Таблица пустая",
                paginate: {
                    first:      "Первая",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Последняя"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one", function(){
        var c_count = 0;
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one").prop('checked',true);
            $(".select_all").prop('checked',true);
        }
        else
        {
            $(".select_one").prop('checked',false);
            $(".select_all").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення шаблона сповіщень</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення шаблона сповіщень "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        $("#name_del").html(id);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/notification/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення групи сповіщень</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних сповіщень?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/notification/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

