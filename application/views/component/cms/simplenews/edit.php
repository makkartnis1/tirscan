<link rel="stylesheet" href="/public/cms/vendor/redactor/redactor.css" />
<script src="/public/cms/vendor/redactor/redactor.js"></script>
<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Новина успішно додана
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_title))
{
    ?>
    <div class="alert alert-danger fade in">
        Заголовок не може бути пустим
    </div>
    <?
}

if (isset($error_content))
{
    ?>
    <div class="alert alert-danger fade in">
        Текст не може бути пустим
    </div>
<?
}

?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


    <div class="form-group">
    <?
    foreach($langs as $l)
    {
    ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Заголовок - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-9">
                <input name="title[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['title']:''?>">
            </div>
        </div>
    <?
    }
    ?>

    <?
    foreach($langs as $l)
    {
    ?>

        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Анонс <?=$l['title']?> <span class="required_optional_span">*</span></label>
            <div class="col-sm-9" style="margin-top: 10px;">
                <script type="text/javascript">
                    $(function()
                    {
                        $('#preview_<?=$l['ID']?>').redactor({
                            minHeight: 100,
                            lang: 'ru',
                            imageUpload: '/redactor'
                        });
                    });
                </script>
                <textarea rows="5" class="form-control" id="preview_<?=$l['ID']?>" name="preview[<?=$l['ID']?>]"><?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['preview']:''?></textarea>
            </div>
        </div>

    <?
    }
    ?>

    <?
    foreach($langs as $l)
    {
    ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Текст <?=$l['title']?> <span class="required_optional_span">*</span></label>
            <div class="col-sm-9" style="margin-top: 10px;">
                <script type="text/javascript">
                    $(function()
                    {
                        $('#content_<?=$l['ID']?>').redactor({
                            minHeight: 100,
                            lang: 'ru',
                            imageUpload: '/redactor'
                        });
                    });
                </script>
                <textarea rows="5" class="form-control" id="content_<?=$l['ID']?>" name="content[<?=$l['ID']?>]"><?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['content']:''?></textarea>
            </div>
        </div>
    <?
    }
    ?>

    <div class="col-sm-12" style="margin-top: 10px">
        <label class="col-sm-2 control-label">URL</label>
        <div class="col-sm-9">
            <input name="url" type="text" class="form-control m-bot15" value="<?=$info['info']['url']?>">
        </div>
    </div>

    </div>


    <div class="col-sm-12">

        <label class="control-label col-sm-2" style="font-weight: normal">Фото</label>

        <div class="col-sm-9" style="padding-left: 5px">

            <div class="fileupload fileupload-new" data-provides="fileupload">

                <?
                $file=$_SERVER['DOCUMENT_ROOT'].$info['info']['image'];
                if (file_exists($file) and ($info['info']['image']))
                {
                    $img = $info['info']['image'];
                }
                ?>

                <div id="no_image" class="fileupload-new thumbnail" style="width: 200px; height: 150px; <?=(isset($img))?'display:none;':''?>">
                    <img src="/public/cms/img/no_image_small.png">
                </div>
                <div id="image_ex" class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px; <?=(isset($img))?'display:block;':''?>">
                    <?
                    if (isset($img))
                    {
                        ?>
                        <img src="<?=$img?>" style="max-height: 150px;">
                        <?
                    }
                    ?>
                </div>
                <div style="width: 203px;">
                                                   <span id="reload_file" class="btn btn-default btn-file">
                                                   <span id="load_file" style="display:none;" class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати</span>
                                                   <span style="display:inline-block;" class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img" onchange="setStatus()">
                                                   </span>

                    <a id="del_file" style="display:inline-block;" class="btn btn-danger fileupload-exists"><i class="fa fa-trash"></i> Видалити</a>
                </div>
            </div>

            <?
            $is_file = (file_exists($_SERVER['DOCUMENT_ROOT'].$info['info']['image']))?1:0;
            ?>
            <input type="hidden" name="file_exist" value="<?=$is_file?>">

        </div>

    </div>

    <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#del_file", function(){
        $("#no_image").show();
        $("#image_ex").hide();
        $("[name=file_exist]").val('0');

    });

    $(document).on( "click", "#reload_file", function(){
        $("#image_ex").show();
    });

    function setStatus()
    {
        $("[name=file_exist]").val('1');
    }
</script>
