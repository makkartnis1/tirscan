<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                    <a href="<?=$add_url?>"><span class="btn btn-default"><i class="fa fa-plus"></i> Создать групу</span></a>
                    <div style="float: right; position: relative; top:60px; z-index: 100;" class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Действия <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu" style="left:-55px;">
                            <li><a href="#" id="delete_all">Удалить</a></li>
                        </ul>
                    </div>
                    <i style="color: green;">Модуль работает</i>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>#</th>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th><input type="checkbox" class="select_all"></th>
                                <th>#</th>
                                <th>Название</th>
                                <th>Действия</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/UserGroup/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one" data-id="'+row.id+'" data-name="'+row.name+'">';
                    }
                },
                {
                    className: "center",data:"id"
                },
                {
                    className: "center",data:"title",
                    render: function (val, type, row) {
                        return '<i>'+row.title+'</i>';
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$edit_url?>'+row.id+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.id+'" data-name="'+row.title+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Загружаю..",
                search:         "Поиск&nbsp;:",
                lengthMenu:     "Записей на странице: _MENU_ ",
                info:           "Показано записей с _START_ по _END_ из _TOTAL_",
                infoEmpty:      "Записей нет",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Таблица пустая",
                emptyTable:     "Таблица пустая",
                paginate: {
                    first:      "Первая",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Последняя"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one", function(){
        var c_count = 0;
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one").prop('checked',true);
            $(".select_all").prop('checked',true);
        }
        else
        {
            $(".select_one").prop('checked',false);
            $(".select_all").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Удаление группы пользователей</h4>
            </div>
            <div class="modal-body">
                Вы подтверждаете удаление группы пользователей "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Удалить</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("#name_del").html(name);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/UserGroup/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Удаление выбранных груп пользователей</h4>
            </div>
            <div class="modal-body">
                Вы подтверждаете удаление выбранных груп пользователей?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Удалить</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/UserGroup/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

