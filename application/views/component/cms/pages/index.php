<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    .table_data tr:first-child th
    {
        border-bottom:1px solid #DDDDDD !important;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                    <a href="<?=$add_url?>"><span class="btn btn-default"><i class="fa fa-plus"></i> Додати сторінку</span></a>
                    <div style="float: right; position: relative; z-index: 100;" class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">Дії <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu" style="left:-105px;">
                            <li><a href="#" id="delete_all">Видалити</a></li>
                        </ul>
                    </div>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_news">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="select_all_req"></th>
                                <th>Заголовок</th>
                                <th>URL</th>
                                <th>Дата створення</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){


        window.table = $('#table_news').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/pages/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all_req").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one_req" data-id="'+row.ID+'">';
                    }
                },
                {
                    className: "center",data:"title"
                },
                {
                    className: "center",data:"uri"
                },
                {
                    className: "center",data:"created"
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$edit_url?>'+row.uri+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.uri+'" data-title="'+row.title+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one_req", function(){
        var c_count = 0;
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all_req", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one_req").prop('checked',true);
            $(".select_all_req").prop('checked',true);
        }
        else
        {
            $(".select_one_req").prop('checked',false);
            $(".select_all_req").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення сторінки</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення сторінки "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        $("#name_del").html($(this).data('title'));
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/pages/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення вибраних сторінок</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних сторінок?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/pages/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all_req").prop('checked',false);
        callback_action();
    }
</script>