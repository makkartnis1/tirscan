<link rel="stylesheet" href="/public/cms/vendor/redactor/redactor.css" />
<script src="/public/cms/vendor/redactor/redactor.js"></script>
<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Сторінка успішно оновлена
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_empty_title))
{
    ?>
    <div class="alert alert-danger fade in">
        Заголовок не може бути пустим
    </div>
    <?
}

if (isset($error_empty_content))
{
    ?>
    <div class="alert alert-danger fade in">
        Текст не може бути пустим
    </div>
<?
}

?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


    <div class="form-group">
    <?
    foreach($langs as $l)
    {
    ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Заголовок <br> <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
            <div class="col-sm-9">
                <input name="title[<?=$l['ID']?>]" type="text" class="form-control m-bot15" value="<?=$info[$l['ID']]['title']?>">
            </div>
        </div>
    <?
    }
    ?>

    <?
    foreach($langs as $l)
    {
    ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Текст <br> <?=$l['title']?> <span class="required_optional_span">*</span></label>
            <div class="col-sm-9" style="margin-top: 10px;">
                <script type="text/javascript">
                    $(function()
                    {
                        $('#content_<?=$l['ID']?>').redactor({
                            minHeight: 100,
                            lang: 'ru',
                            imageUpload: '/redactor'
                        });
                    });
                </script>
                <textarea rows="5" class="form-control" id="content_<?=$l['ID']?>" name="content[<?=$l['ID']?>]"><?=$info[$l['ID']]['content']?></textarea>
            </div>
        </div>
    <?
    }
    ?>

    <?
    foreach($langs as $l)
    {
        ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">Ключові слова <br> <?=$l['title']?></label>
            <div class="col-sm-9" style="margin-top: 10px;">
                <textarea rows="5" class="form-control" id="keywords_<?=$l['ID']?>" name="keywords[<?=$l['ID']?>]"><?=$info[$l['ID']]['keywords']?></textarea>
            </div>
        </div>
        <?
    }
    ?>

    <?
    foreach($langs as $l)
    {
        ?>
        <div class="col-sm-12" style="margin-top: 10px">
            <label class="col-sm-2 control-label">SEO-опис <br> <?=$l['title']?></label>
            <div class="col-sm-9" style="margin-top: 10px;">
                <textarea rows="5" class="form-control" id="description_<?=$l['ID']?>" name="description[<?=$l['ID']?>]"><?=$info[$l['ID']]['description']?></textarea>
            </div>
        </div>
        <?
    }
    ?>


    <div class="col-sm-12" style="margin-top: 10px">
        <label class="col-sm-2 control-label">URL</label>
        <div class="col-sm-9">
            <?
                $uri = '';
                foreach($langs as $l)
                {
                    $uri = $info[$l['ID']]['uri'];
                }
                echo $uri;
            ?>
        </div>
    </div>

    </div>



    <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>
