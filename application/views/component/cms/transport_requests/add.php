<style type="text/css">
    @media (max-width: 870px)
    {
        .col-sm-8
        {
            width: 100%;
        }
        .col-sm-3 {
            width: 100%;
            text-align: left;
        }

        .col-sm-2 {
            width: 100%;
            text-align: left;
        }

        .form-horizontal .control-label
        {
            text-align: left;
        }
    }

</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>
<div class="wrapper">
<div class="row">
<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>
<div class="panel-body">
<div class="module_desc_div">
    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
</div>
<?
if (isset($change_ok))
{
    ?>
    <div class="alert alert-success fade in">
        Заявка на транспорт успішно додана
    </div>
<?
}

if (isset($errors))
{
    extract($errors,EXTR_PREFIX_ALL,"error");
}


if (isset($error_dateFrom))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано дату завантаження
    </div>
<?
}


if (isset($error_userID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано користувача
    </div>
<?
}

if (isset($error_userCarID))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано машину користувача
    </div>
<?
}

if (isset($error_places_from))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано місце завантаження
    </div>
    <?
}

if (isset($error_places_to))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вибрано місце розвантаження
    </div>
    <?
}

if (isset($error_typeCurrency))
{
    ?>
    <div class="alert alert-danger fade in">
        Не вказано валюту
    </div>
    <?
}


?>

<form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


<div class="form-group">
    <div class="col-sm-6">
        <label class="col-sm-5 control-label" style="text-align: left;width: 180px;">Датa&nbsp;завантаження&nbsp;від&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-7" style="text-align: left;">
            <input style="max-width: 138px" name="dateFrom" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($data))?$data['dateFrom']:date("Y-m-d")?>">
        </div>
    </div>
    <div class="col-sm-6">
        <label class="col-sm-5 control-label" style="text-align: left;width: 170px">Датa&nbsp;завантаження&nbsp;до&nbsp;</label>
        <div class="col-sm-7" style="text-align: left;">
            <input style="max-width: 138px" name="dateTo" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($data))?$data['dateTo']:$config['date_to']?>">
        </div>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-6">
        <label class="col-sm-5 control-label label_left" style="width: 177px; padding-right: 0">Вартість перевезення:</label>
        <div class="col-sm-7" style="padding: 0">
            <label class="widget_label control-label" id="price_details" style="position: relative;padding: 0;top: 4px;text-align: left">
                <?
                if (isset($data))
                {
                    if ($data['specifiedPrice'] == 'yes')
                {
                    ?>
                    <script type="text/javascript">
                        $(function(){
                            $("[name=is_price]").prop("checked",true);
                        });
                    </script>
                    <?
                    $price_val = $data['value'];
                    $price_priceCurrencyID = $data['priceCurrencyID'];
                    $price_paymentType = $data['paymentType'];
                    $price_pdv = $data['PDV'];
                    $price_onLoad = $data['onLoad'];
                    $price_onUnload = $data['onUnload'];
                    $price_onPrepay = $data['onPrepay'];
                    $price_advancedPayment = $data['advancedPayment'];
                    $price_customPriceType = $data['customPriceType'];


                    $text = '';
                    $text_arr = [];
                    if ($price_val)
                    {
                        if ($price_priceCurrencyID)
                        {
                            $text_arr[] = $price_val.' '.$data['cur_name'];
                        }
                        else
                        {
                            $text_arr[] = $price_val.' '.$price_customPriceType;
                        }
                    }


                    if ($price_paymentType == 'b/g')
                    {
                        $text_arr[] = 'б/г';
                    }

                    if ($price_paymentType == 'gotivka')
                    {
                        $text_arr[] = 'готівка';
                    }

                    if ($price_paymentType == 'combined')
                    {
                        $text_arr[] = 'комбінована';
                    }

                    if ($price_paymentType == 'card')
                    {
                        $text_arr[] = 'на картку';
                    }

                    if ($price_pdv == 'yes')
                    {
                        $text_arr[] = 'ПДВ';
                    }

                    if ($price_onLoad == 'yes')
                    {
                        $text_arr[] = 'при завантаженні';
                    }

                    if ($price_onUnload == 'yes')
                    {
                        $text_arr[] = 'при розвантаженні';
                    }

                    if ($price_onPrepay == 'yes')
                    {
                        if ($price_advancedPayment)
                        {
                            $text_arr[] = 'передоплата '.$price_advancedPayment.'%';

                        }
                    }

                    if (!empty($text_arr))
                    {
                        $text = implode(', ',$text_arr);
                    }

                    echo $text;
                    }
                }
                ?>
            </label>
            <div id="add_price_btn" class="btn btn-link control-label" style="text-decoration: underline; padding-top: 6px; padding-right: 0;padding-bottom: 0">Вказати</div>
        </div>
    </div>

    <div class="col-sm-6">
        <label class="col-sm-5 control-label label_left" style="width: 177px; padding-right: 0">Додаткова інформація:</label>
        <div class="col-sm-7" style="padding: 0">
            <label class="widget_label control-label" id="info_details" style="position: relative;padding: 0;top: 4px;text-align: left">
                <?

                if (isset($data))
                {

                    $docTIR = $data['docTIR'];
                    $docCMR = $data['docCMR'];
                    $docT1 = $data['docT1'];
                    $docSanPassport = $data['docSanPassport'];
                    $docSanBook = $data['docSanBook'];
                    $loadFromSide = $data['loadFromSide'];
                    $loadFromTop = $data['loadFromTop'];
                    $loadFromBehind = $data['loadFromBehind'];
                    $loadTent = $data['loadTent'];
                    $condPlomb = $data['condPlomb'];
                    $condLoad = $data['condLoad'];
                    $condBelts = $data['condBelts'];
                    $condRemovableStands = $data['condRemovableStands'];
                    $condHardSide = $data['condHardSide'];
                    $condCollectableCargo = $data['condCollectableCargo'];
                    $condTemperature = $data['condTemperature'];
                    $condPalets = $data['condPalets'];
                    $condADR = $data['condADR'];

                    $text = '';
                    $docs_arr = [];
                    $load_arr = [];
                    $cond_arr = [];


                    if ($docTIR == 'yes')
                    {
                        $docs_arr[] = 'TIR';
                    }

                    if ($docCMR == 'yes')
                    {
                        $docs_arr[] = 'CMR';
                    }

                    if ($docT1 == 'yes')
                    {
                        $docs_arr[] = 'T1';
                    }

                    if ($docSanPassport == 'yes')
                    {
                        $docs_arr[] = 'Санпаспорт';
                    }

                    if ($docSanBook == 'yes')
                    {
                        $docs_arr[] = 'Санкнижка';
                    }

                    if ($loadFromSide == 'yes')
                    {
                        $load_arr[] = 'Збоку';
                    }

                    if ($loadFromTop == 'yes')
                    {
                        $load_arr[] = 'Зверху';
                    }

                    if ($loadFromBehind == 'yes')
                    {
                        $load_arr[] = 'Ззаду';
                    }

                    if ($loadTent == 'yes')
                    {
                        $load_arr[] = 'Розтентування';
                    }

                    if ($condPlomb == 'yes')
                    {
                        $cond_arr[] = 'Пломба';
                    }

                    if ($condLoad == 'yes')
                    {
                        $cond_arr[] = 'Довантаження';
                    }

                    if ($condBelts == 'yes')
                    {
                        $cond_arr[] = 'Ремені';
                    }

                    if ($condRemovableStands == 'yes')
                    {
                        $cond_arr[] = "З'ємні стійки";
                    }

                    if ($condHardSide == 'yes')
                    {
                        $cond_arr[] = "Жорсткий борт";
                    }

                    if ($condHardSide == 'yes')
                    {
                        $cond_arr[] = "Жорсткий борт";
                    }

                    if ($condCollectableCargo == 'yes')
                    {
                        $cond_arr[] = "Жорсткий борт";
                    }

                    if ($condTemperature)
                    {
                        $cond_arr[] = "Температура:".$condTemperature."°";
                    }

                    if ($condPalets)
                    {
                        $cond_arr[] = "Палети:".$condPalets;
                    }

                    if ($condADR)
                    {
                        $cond_arr[] = "ADR:".$condADR;
                    }

                    if (!empty($docs_arr))
                    {
                        $text .= "Документи: ".implode(', ',$docs_arr).". ";
                    }

                    if (!empty($load_arr))
                    {
                        $text .= "Завантаження: ".implode(', ',$load_arr).". ";
                    }

                    if (!empty($cond_arr))
                    {
                        $text .= "Умови: ".implode(', ',$cond_arr).". ";
                    }

                    if ($text)
                    {
                        echo $text;
                    }

                }

                ?>
            </label>
            <div id="add_info_btn" class="btn btn-link control-label" style="text-decoration: underline; padding-top: 6px; padding-right: 0;padding-bottom: 0">Вказати</div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-6">
        <label class="col-sm-3 control-label" style="width: 105px">Користувач&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-9">
            <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="user_name"><?=(isset($data))?$data['user_name']:''?></span>
            <div class="btn btn-link btn_select_widget" id="select_user">Вказати</div>
            <input type="hidden" name="userID" value="<?=(isset($data))?$data['userID']:''?>">
        </div>
        <label style="margin-left: 15px; font-size: 11px">
            власник/менеджер транспортної або експедиторської компанії у якого наявний власний транспорт
        </label>
    </div>
    <div class="col-sm-6">
        <label class="col-sm-3 control-label" style="width: 105px">Транспорт&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-9" style="width: 160px">
            <?
            if (isset($data))
            {
                $type = 'вантажівка';
                if ($data['type'] == 'napiv prychip') $type = 'напівпричіп';
                if ($data['type'] == 'z prychipom') $type = 'з причепом';
                $text_car = $data['car_type'].',&nbsp;'.$type.', '.$data['sizeX'].'x'.$data['sizeY'].'x'.$data['sizeZ'].', '.$data['liftingCapacity'].'т&nbsp;/&nbsp;'.$data['volume'].'м3'.',<br>номер машини: '.$data['number'];
            }
            ?>
            <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="transport_name"><?=(isset($data))?$text_car:''?></span>
            <div class="btn btn-link btn_select_widget" id="select_transport">Вказати</div>
            <input type="hidden" name="userCarID" value="<?=(isset($data))?$data['userCarID']:''?>">
        </div>

    </div>
</div>






<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="price_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вартість перевезення</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <div class="col-sm-4 is_price_block" style="padding-right: 0; position: relative; top: 2px;">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="checkbox" name="is_price" <?=(isset($data))?(($data['specifiedPrice'] == 'yes')?'checked':''):''?>> Вказати вартість
                        </div>
                    </div>
                    <div class="col-sm-8" style="padding-left: 0">
                        <label class="col-sm-1 control-label" style="padding-right: 0;padding-left: 0;">Ціна</label>
                        <div class="col-sm-3" style="padding-right: 0">
                            <input name="price_value" type="text" class="form-control m-bot15" maxlength="10" value="<?=(isset($data))?$data['value']:''?>">
                        </div>

                        <div class="col-sm-3" style="padding-right: 0">
                            <select name="price_priceCurrencyID" class="form-control m-bot15">
                                <option value="">валюта</option>
                                <?
                                foreach($price_cur as $p_c)
                                {
                                    ?>
                                    <option <?=(isset($data))?(($p_c['ID']==$data['priceCurrencyID'])?'selected':''):''?> value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-1" style="padding-right: 0;padding-left: 10px">
                            <label class="control-label">або</label>
                        </div>

                        <div class="col-sm-4" style="padding-right: 0">
                            <input style="padding: 8px" name="price_customPriceType" type="text" class="form-control m-bot15" placeholder="вказати свою" value="<?=(isset($data))?$data['customPriceType']:''?>">
                        </div>

                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-2">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="radio" name="price_paymentType" value="b/g" <?=(isset($data))?(($data['paymentType']=='b/g')?'checked':''):'checked'?>> б/г
                        </div>
                    </div>

                    <div class="col-sm-2" style="width: 20%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="radio" name="price_paymentType" value="gotivka" <?=(isset($data))?(($data['paymentType']=='gotivka')?'checked':''):''?>> готівка
                        </div>
                    </div>

                    <div class="col-sm-4" style="width: 27%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="radio" name="price_paymentType" value="combined" <?=(isset($data))?(($data['paymentType']=='b/g')?'checked':''):''?>> комбінована
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="radio" name="price_paymentType" value="card" <?=(isset($data))?(($data['paymentType']=='card')?'checked':''):''?>> на картку
                        </div>
                    </div>


                </div>



                <div class="form-group">
                    <div class="col-sm-3" style="width: 16.8%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="checkbox" name="price_pdv" <?=(isset($data))?(($data['PDV']=='yes')?'checked':''):''?>> ПДВ
                        </div>
                    </div>
                    <div class="col-sm-3" style="width: 33%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="checkbox" name="price_onLoad" <?=(isset($data))?(($data['onLoad']=='yes')?'checked':''):''?>> при завантаженні
                        </div>
                    </div>

                    <div class="col-sm-3" style="width: 35%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="checkbox" name="price_onUnload" <?=(isset($data))?(($data['onUnload']=='yes')?'checked':''):''?>> при розвантаженні
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-3" style="width: 27%">
                        <div class="flat-grey single-row icheck form_checkbox">
                            <input type="checkbox" name="price_onPrepay" <?=(isset($data))?(($data['onPrepay']=='yes')?'checked':''):''?>> передоплата
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input name="price_advancedPayment" type="text" class="form-control m-bot15" maxlength="3" placeholder="%" value="<?=(isset($data))?(($data['onPrepay']=='yes')?$data['advancedPayment']:''):''?>">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="save_price">ОК</button>
            </div>
        </div>
    </div>
</div>


    <script type="text/javascript">
        $(document).on( "click", "#add_price_btn", function(){
            $("#price_modal").modal();
        });
    </script>


    <script type="text/javascript">
        $(document).on( "click", "#save_price", function(){
            var price_val = $("[name=price_value]").val();
            var price_priceCurrencyID = $("[name=price_priceCurrencyID]").children("option:checked").text();
            var price_paymentType = $("[name=price_paymentType]:checked").val();
            var price_pdv = $("[name=price_pdv]").prop("checked");
            var price_onLoad = $("[name=price_onLoad]").prop("checked");
            var price_onUnload = $("[name=price_onUnload]").prop("checked");
            var price_onPrepay = $("[name=price_onPrepay]").prop("checked");
            var price_advancedPayment = $("[name=price_advancedPayment]").val();
            var is_price = $("[name=is_price]").prop("checked");
            var price_customPriceType = $("[name=price_customPriceType]").val();

            var text = '';
            var text_arr = [];
            if (price_val)
            {
                if (price_priceCurrencyID)
                {
                    text_arr.push(price_val+' '+price_priceCurrencyID);
                }
                if (price_customPriceType)
                {
                    text_arr.push(price_val+' '+price_customPriceType);
                }
            }
            if (price_paymentType)
            {
                if (price_paymentType == 'b/g')
                {
                    text_arr.push('б/г');
                }

                if (price_paymentType == 'gotivka')
                {
                    text_arr.push('готівка');
                }

                if (price_paymentType == 'combined')
                {
                    text_arr.push('комбінована');
                }

                if (price_paymentType == 'card')
                {
                    text_arr.push('на картку');
                }
            }

            if (price_pdv)
            {
                text_arr.push('ПДВ');
            }

            if (price_onLoad)
            {
                text_arr.push('при завантаженні');
            }

            if (price_onUnload)
            {
                text_arr.push('при розвантаженні');
            }

            if (price_onPrepay)
            {
                if (price_advancedPayment)
                {
                    text_arr.push('передоплата '+price_advancedPayment+'%');
                }
            }

            if (text_arr.length > 0)
            {
                text = text_arr.join(', ');
            }

            if (is_price)
            {
                if (text)
                {
                    $("#price_details").html(text);
                }
            }
            else
            {
                $("#price_details").html('');
            }




        });
    </script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="info_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Додаткова інформація</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <div class="col-sm-12">
                        Документи
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="docTIR" <?=(isset($data))?(($data['docTIR']=='yes')?'checked':''):''?>> TIR
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="docCMR" <?=(isset($data))?(($data['docCMR']=='yes')?'checked':''):''?>> CMR
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="docSanPassport" <?=(isset($data))?(($data['docSanPassport']=='yes')?'checked':''):''?>> Санпаспорт
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="docSanBook" <?=(isset($data))?(($data['docSanBook']=='yes')?'checked':''):''?>> Санкнижка
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="docT1" <?=(isset($data))?(($data['docT1']=='yes')?'checked':''):''?>> T1
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-12">
                        Завантаження
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="loadFromSide" <?=(isset($data))?(($data['loadFromSide']=='yes')?'checked':''):''?>> Збоку
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="loadFromTop" <?=(isset($data))?(($data['loadFromTop']=='yes')?'checked':''):''?>> Зверху
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="loadFromBehind" <?=(isset($data))?(($data['loadFromBehind']=='yes')?'checked':''):''?>> Ззаду
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div style="width: 132px" class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="loadTent" <?=(isset($data))?(($data['loadTent']=='yes')?'checked':''):''?>> Розтентування
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-12">
                        Умови
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="condPlomb" <?=(isset($data))?(($data['condPlomb']=='yes')?'checked':''):''?>> Пломба
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox" style="width: 131px">
                                <input type="checkbox" name="condLoad" <?=(isset($data))?(($data['condLoad']=='yes')?'checked':''):''?>> Довантаження
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="condBelts" <?=(isset($data))?(($data['condBelts']=='yes')?'checked':''):''?>> Ремені
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox">
                                <input type="checkbox" name="condRemovableStands" <?=(isset($data))?(($data['condRemovableStands']=='yes')?'checked':''):''?>> З'ємні стійки
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox" style="width: 133px">
                                <input type="checkbox" name="condHardSide" <?=(isset($data))?(($data['condHardSide']=='yes')?'checked':''):''?>> Жорсткий борт
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="flat-grey single-row icheck form_checkbox" style="width: 139px">
                                <input type="checkbox" name="condCollectableCargo" <?=(isset($data))?(($data['condCollectableCargo']=='yes')?'checked':''):''?>> Збірний вантаж
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <div class="col-sm-6" style="width: 52px;padding: 0;margin: 0">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" id="is_temperature" <?=(isset($data))?(($data['condTemperature'])?'checked':''):''?>> t°
                                </div>
                            </div>
                            <div class="col-sm-6" style="width: 75px;padding: 0;margin: 0">
                                <input name="condTemperature" type="text" class="form-control m-bot15" maxlength="3" style="<?=(isset($data))?(($data['condTemperature'])?'':'display: none;'):'display: none;'?>margin:0;height: 28px;margin-left: 5px" value="<?=(isset($data))?(($data['condTemperature'])?$data['condTemperature']:''):''?>">
                            </div>

                        </div>
                        <script type="text/javascript">
                            $('#is_temperature').on('ifChecked', function(event){
                                $('[name=condTemperature]').show();
                            });
                            $('#is_temperature').on('ifUnchecked', function(event){
                                $('[name=condTemperature]').hide();
                            });
                        </script>

                        <div class="col-sm-12">
                            <div class="col-sm-6" style="width: 90px;padding: 0;margin: 0">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" id="is_palets" <?=(isset($data))?(($data['condPalets'])?'checked':''):''?>> Палети
                                </div>
                            </div>
                            <div class="col-sm-6" style="width: 40px;padding: 0;margin: 0">
                                <input name="condPalets" type="text" class="form-control m-bot15" style="<?=(isset($data))?(($data['condPalets'])?'':'display: none;'):'display: none;'?>margin:0;height: 28px;margin-left: 5px;padding: 8px" value="<?=(isset($data))?(($data['condPalets'])?$data['condPalets']:''):''?>">
                            </div>
                        </div>
                        <script type="text/javascript">
                            $('#is_palets').on('ifChecked', function(event){
                                $('[name=condPalets]').show();
                            });
                            $('#is_palets').on('ifUnchecked', function(event){
                                $('[name=condPalets]').hide();
                            });
                        </script>

                        <div class="col-sm-12">
                            <div class="col-sm-6" style="width: 70px;padding: 0;margin: 0">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" id="is_adr" <?=(isset($data))?(($data['condADR'])?'checked':''):''?>> ADR
                                </div>
                            </div>
                            <div class="col-sm-6" style="width: 60px;padding: 0;margin: 0">
                                <input name="condADR" type="text" class="form-control m-bot15" style="<?=(isset($data))?(($data['condADR'])?'':'display: none;'):'display: none;'?>margin:0;height: 28px;margin-left: 5px" value="<?=(isset($data))?(($data['condADR'])?$data['condADR']:''):''?>">
                            </div>
                        </div>

                        <script type="text/javascript">
                            $('#is_adr').on('ifChecked', function(event){
                                $('[name=condADR]').show();
                            });
                            $('#is_adr').on('ifUnchecked', function(event){
                                $('[name=condADR]').hide();
                            });
                        </script>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="save_info">ОК</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on( "click", "#add_info_btn", function(){
        $("#info_modal").modal();
    });
</script>

<script type="text/javascript">
    $(document).on( "click", "#save_info", function(){

        var docTIR = $("[name=docTIR]").prop("checked");
        var docCMR = $("[name=docCMR]").prop("checked");
        var docT1 = $("[name=docT1]").prop("checked");
        var docSanPassport = $("[name=docSanPassport]").prop("checked");
        var docSanBook = $("[name=docSanBook]").prop("checked");
        var loadFromSide = $("[name=loadFromSide]").prop("checked");
        var loadFromTop = $("[name=loadFromTop]").prop("checked");
        var loadFromBehind = $("[name=loadFromBehind]").prop("checked");
        var loadTent = $("[name=loadTent]").prop("checked");
        var condPlomb = $("[name=condPlomb]").prop("checked");
        var condLoad = $("[name=condLoad]").prop("checked");
        var condBelts = $("[name=condBelts]").prop("checked");
        var condRemovableStands = $("[name=condRemovableStands]").prop("checked");
        var condHardSide = $("[name=condHardSide]").prop("checked");
        var condCollectableCargo = $("[name=condCollectableCargo]").prop("checked");
        var condTemperature = $("[name=condTemperature]").val();
        var condPalets = $("[name=condPalets]").val();
        var condADR = $("[name=condADR]").val();
        var is_temperature = $("#is_temperature").prop("checked");
        var is_palets = $("#is_palets").prop("checked");
        var is_adr = $("#is_adr").prop("checked");


        var text = '';
        var docs_arr = [];
        var load_arr = [];
        var cond_arr = [];


        if (docTIR) docs_arr.push('TIR');
        if (docCMR) docs_arr.push('CMR');
        if (docT1) docs_arr.push('T1');
        if (docSanPassport) docs_arr.push('Санпаспорт');
        if (docSanBook) docs_arr.push('Санкнижка');

        if (loadFromSide) load_arr.push('Збоку');
        if (loadFromTop) load_arr.push('Зверху');
        if (loadFromBehind) load_arr.push('Ззаду');
        if (loadTent) load_arr.push('Розтентування');

        if (condPlomb) cond_arr.push('Пломба');
        if (condLoad) cond_arr.push('Довантаження');
        if (condBelts) cond_arr.push('Ремені');
        if (condRemovableStands) cond_arr.push("З'ємні стійки");
        if (condHardSide) cond_arr.push("Жорсткий борт");
        if (condCollectableCargo) cond_arr.push("Збірний вантаж");
        if (is_temperature)
        {
            if (condTemperature) cond_arr.push("Температура("+condTemperature+"°)");
        }
        if (is_palets)
        {
            if (is_palets) cond_arr.push("Палети("+condPalets+")");
        }
        if (is_adr)
        {
            if (is_adr) cond_arr.push("ADR("+condADR+")");
        }


        if (docs_arr.length > 0)
        {
            text += "<b>Документи:</b> "+docs_arr.join(', ')+". ";
        }

        if (load_arr.length > 0)
        {
            text += "<br><b>Завантаження:</b> "+load_arr.join(', ')+". ";
        }

        if (cond_arr.length > 0)
        {
            text += "<br><b>Умови:</b> "+cond_arr.join(', ')+". ";
        }

        if (text)
        {
            $("#info_details").html(text);
        }
        else
        {
            $("#info_details").html('');
        }


    });
</script>

<div class="form-group">

    <div class="col-sm-6">
        <label class="col-sm-12 control-label label_left">Місце завантаження&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-12">
            <div id="places_from_block">
                <?
                    $i_from_group = 0;
                    if (isset($data))
                    {
                        foreach($data['geo_groups'] as $geo_group)
                        {
                            if ($geo_group['type'] == 'to') continue;
                            $i_from_group++;
                            if ($i_from_group == 1)
                            {
                                ?>
                                <input type="text" id="place_from_<?=$i_from_group?>" class="form-control" value="<?=$geo_group['name']?>">
                                <input name="places_from[]" id="place_from_<?=$i_from_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                <?
                            }
                            else
                            {
                                ?>
                                <div class="one_place_block">
                                    <input type="text" id="place_from_<?=$i_from_group?>" class="form-control dynamic_place_inputs" value="<?=$geo_group['name']?>">
                                    <input name="places_from[]" id="place_from_<?=$i_from_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                    <button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>
                                </div>
                                <?
                            }
                        }

                    }

                    if ($i_from_group == 0)
                    {
                        ?>
                        <input type="text" id="place_from_<?=$i_from_group?>" class="form-control">
                        <input name="places_from[]" id="place_from_<?=$i_from_group?>_places" type="hidden">
                        <?
                    }

                ?>

            </div>
            <div id="add_place_from" style="padding-left: 0" class="btn btn-link">Додати місце</div>
        </div>
    </div>

    <div class="col-sm-6">
        <label class="col-sm-12 control-label label_left">Місце розвантаження&nbsp;<span class="required_span">*</span></label>
        <div class="col-sm-12">
            <div id="places_to_block">

                <?
                $i_to_group = 0;
                if (isset($data))
                {
                    foreach($data['geo_groups'] as $geo_group)
                    {
                        if ($geo_group['type'] == 'from') continue;
                        $i_to_group++;
                        if ($i_to_group == 1)
                        {
                            ?>
                            <input type="text" id="place_to_<?=$i_to_group?>" class="form-control" value="<?=$geo_group['name']?>">
                            <input name="places_to[]" id="place_to_<?=$i_to_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                        <?
                        }
                        else
                        {
                            ?>
                            <div class="one_place_block">
                                <input type="text" id="place_to_<?=$i_to_group?>" class="form-control dynamic_place_inputs" value="<?=$geo_group['name']?>">
                                <input name="places_to[]" id="place_to_<?=$i_to_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                <button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>
                            </div>
                        <?
                        }
                    }

                }

                if ($i_to_group == 0)
                {
                    ?>
                    <input type="text" id="place_to_<?=$i_to_group?>" class="form-control">
                    <input name="places_to[]" id="place_to_<?=$i_to_group?>_places" type="hidden">
                    <?
                }
                ?>

            </div>
            <div id="add_place_to" style="padding-left: 0" class="btn btn-link">Додати місце</div>
        </div>
    </div>

</div>

<script type="text/javascript">

            $(function(){
                <?
                    $num_place_from = 0;
                    $num_place_to = 0;

                    if (isset($data))
                    {
                        foreach($data['geo_groups'] as $geo_group)
                        {
                            if ($geo_group['type'] == 'from')
                            {
                                $num_place_from++;
                                ?>
                                google_place_init('place_from_<?=$num_place_from?>','uk');
                                <?
                            }
                            else
                            {
                                $num_place_to++;
                                ?>
                                google_place_init('place_to_<?=$num_place_to?>','uk');
                                <?
                            }
                        }
                    }


                ?>

                window.num_place_from = '<?=$num_place_from?>';
                window.num_place_to = '<?=$num_place_to?>';
            });

            $(document).on( "click", "#add_place_from", function(){
                window.num_place_from++;
                var text = '<div class="one_place_block">' +
                    '<input type="text" id="place_from_'+window.num_place_from+'" class="form-control dynamic_place_inputs"><input name="places_from[]" id="place_from_'+window.num_place_from+'_places" type="hidden">' +
                    '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                    '</div>';
                $("#places_from_block").append(text);
                google_place_init('place_from_'+window.num_place_from,'uk');
            });

            $(document).on( "click", "#add_place_to", function(){
                window.num_place_to++;
                var text = '<div class="one_place_block">' +
                    '<input type="text" id="place_to_'+window.num_place_to+'" class="form-control dynamic_place_inputs"><input name="places_to[]" id="place_to_'+window.num_place_to+'_places" type="hidden">' +
                    '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                    '</div>';
                $("#places_to_block").append(text);
                google_place_init('place_to_'+window.num_place_to,'uk');
            });

            $(document).on( "click", ".delete_place", function(){
                $(this).parent(".one_place_block").remove();
            });

        </script>

<div class="form-group" style="padding:0 10px 10px;">
    <label class="col-sm-12 control-label label_left">Коментар</label>
    <div class="col-sm-12">
        <textarea class="form-control" name="info" style="height: 120px" placeholder="Інша додаткова інформація"><?=(isset($data))?$data['info']:''?></textarea>
    </div>
</div>

    <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
</form>


</div>
</section>
</div>
</div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_car", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    window.user_id_tr = window.ids_user[i];
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#close_span_user").html("x");
                                    $("[name=userCarID]").val("");
                                    $("#transport_name").html("");
                                    $("#close_span_transport").html("");
                                }
                        });',
                        'isset_car'=>'1',
                        'user_type'=>'transport'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_car" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="transport_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір транспорта</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('transport','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_transport", function () {
                            if (window.user_id_tr)
                            {
                                $("#transport_modal").modal();
                                callback_action_tr_w();
                            }
                            else
                            {
                               $("#error_modal_text").html("Спочатку виберіть користувача");
                               $("#error_modal").modal();
                            }

                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_transport_to_req", function () {
                                set_all_ids_tr_w();
                                for (var i = 0; i < window.ids_tr_w.length; i++) {
                                    $("[name=userCarID]").val(window.ids_tr_w[i]);
                                    $("#transport_name").html(window.names_tr_w[i]);
                                    $("#close_span_transport").html("x");
                                }
                        });'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_transport_to_req" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('');
    });

    $(document).on( "click", "#close_span_transport", function(){
        $("[name=userCarID]").val('');
        $(this).html('');
        $('#user_transport').html('');
    });

</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="error_modal" class="modal fade">
    <div class="modal-dialog" style="max-width: 400px">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Помилка</h4>
            </div>
            <div class="modal-body" id="error_modal_text"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ОК</button>
            </div>
        </div>
    </div>
</div>


