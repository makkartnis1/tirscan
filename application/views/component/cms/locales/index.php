<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    .t_edit
    {
        margin-left: 5px;
        position: relative;
        bottom: 1px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="example">
                            <thead>
                            <tr id="row_for_search_user">
                                <th></th>
                                <?
                                $i=1;
                                foreach($langs as $l)
                                {
                                    $i++;
                                    ?>
                                    <th></th>
                                <?
                                }
                                ?>
                            </tr>
                            <tr>
                                <th>Ключ</th>
                                <?
                                $i=1;
                                foreach($langs as $l)
                                {
                                    $i++;
                                    ?>
                                    <th style="min-width: 80px">Переклад <?=$l['title']?></th>
                                    <?
                                }
                                ?>
                            </tr>
                            </thead>


                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function(){

    var lang_index = <?=$i?>;

    $('#row_for_search_user th').each( function () {

        var ind = $(this).index();

        if(ind <= lang_index)
        {
            $(this).append( '<input index="'+ind+'" type="text" style="width:95%;"/>' );
        }

    } );

    window.table = $('#example').DataTable( {
        ajax: {
            url: "/component_cms/<?=$lang?>/locales/db/",
            type: "POST"
        },
        deferRender : true,
        serverSide: true,
        processing: true,
        columns: [
            {
                className: "center",data:"const"
            },
            <?
                foreach($langs as $l)
                {
                    ?>
                    {
                        className: "center",data:"translate_<?=$l['uri']?>",
                        render: function (val, type, row) {
                        var name = (row['translate_<?=$l['uri']?>']!=null)?row['translate_<?=$l['uri']?>']:'-';
                        return name+'<button style="color: #428bca !important;" t_id="'+row.id+'" t_lng="<?=$l['ID']?>" t_translate="'+row['translate_<?=$l['uri']?>']+'" class="btn btn-link t_edit"><i class="fa fa-pencil"></i></button>';
                        }
                    },
                <?
                }
                ?>
        ],
        "order": [[1, 'desc']],
        language: {
            processing:     "Завантажую..",
            search:         "Пошук&nbsp;:",
            lengthMenu:     "Записів на сторінці: _MENU_ ",
            info:           "Показано записів з _START_ по _END_ із _TOTAL_",
            infoEmpty:      "Записів немає",
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Даних немає",
            emptyTable:     "Даних немає",
            paginate: {
                first:      "Перша",
                previous:   "Назад",
                next:       "Вперед",
                last:       "Остання"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
            }
        }

    } );


    <?
        for($k=0;$k<=10;$k++)
        {
            ?>
    $("#row_for_search_user th input[index=<?=$k?>], #row_for_search_user th select[index=<?=$k?>]").on( 'keyup change', function () {
        window.table
            .column(<?=$k?>)
            .search(this.value)
            .draw();
    } );
    <?
}
?>


});


function callback_action()
{
    window.table.draw(false);
}


</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Редагування локалізації</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="edit_content" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="save_translate" data-dismiss="modal" class="btn btn-success">Зберегти</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", ".t_edit", function(){
        var id = $(this).attr('t_id');
        var lng_id = $(this).attr('t_lng');
        var translate = $(this).attr('t_translate');

        $("#save_translate").attr('data_id',id);
        $("#save_translate").attr('lng_id',lng_id);
        $("#edit_content").val(translate);

        $("#editModal").modal();

    });

    $(document).on( "click", "#save_translate", function(){

        var id = $(this).attr('data_id');
        var lng_id = $(this).attr('lng_id');
        var translate = $("#edit_content").val();

        $.post("/component_cms/<?=$lang?>/locales/ajax/",{id:id,lng_id:lng_id,translate:translate,action:'edit'},callback_action,"json");
    });

</script>

