<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>


<header class="panel-heading"></header>

<div class="panel-body">
    <div class="adv-table">

        <table style="width:100% !important;" class="display table table-bordered table-striped" id="example_admin">
            <thead>
            <tr>
                <th></th>
                <th>Имя / Логин</th>
                <th>Группа</th>
                <th>Эл. почта</th>
                <th>Дата</th>
                <th>Активность</th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th></th>
                <th>Имя / Логин</th>
                <th>Группа</th>
                <th>Эл. почта</th>
                <th>Дата</th>
                <th>Активность</th>
            </tr>
            </tfoot>
        </table>

    </div>
</div>


<script type="text/javascript">


    $(function(){

        window.table_admin = $('#example_admin').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/admin/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all_admin").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        return '<input type="checkbox" class="select_one_admin" data-id="'+row.id+'" data-name="'+row.name+'">';
                    }
                },
                {
                    className: "center",data:"name",
                    render: function (val, type, row) {
                        var name = (row.name)?row.name:'-';
                        var login = (row.login)?row.login:'-';
                        return name+' / '+login;
                    }
                },
                {
                    className: "center",data:"group_id",
                    render: function (val, type, row) {
                        return '<span class="label label-primary">'+row.group_title+'</span>';
                    }
                },
                {
                    className: "center",data:"email",
                    render: function (val, type, row) {
                        return (row.email)?row.email:'-';
                    }
                },
                {
                    className: "center",data:"register_date"
                },
                {
                    className: "center",data:"active",
                    render: function (val, type, row) {
                        var active = '<span style="margin: 4px 0;" class="label label-danger">Заблокированный</span>';
                        if (row.active=='1') active = '<span style=" margin: 4px 0;" class="label label-success">Активный</span>';
                        return active;
                    }
                }
            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Загружаю..",
                search:         "Поиск&nbsp;:",
                lengthMenu:     "Записей на странице: _MENU_ ",
                info:           "Показано записей с _START_ по _END_ из _TOTAL_",
                infoEmpty:      "Записей нет",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Таблица пустая",
                emptyTable:     "Таблица пустая",
                paginate: {
                    first:      "Первая",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Последняя"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action_admin()
    {
        window.table_admin.draw(false);
    }


    $(document).on( "click", ".select_one_admin", function(){
        var c_count = 0;
        $(".select_one_admin").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });


</script>


<script type="text/javascript">

    function set_all_ids_admin()
    {
        var ids = [];
        var names = [];
        $(".select_one_admin").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
                names.push($(this).data('name'));
            }
        });

        window.ids_admin = ids;
        window.names_admin = names;
    }

    function callback_itemAll_admin()
    {
        $(".select_all_admin").prop('checked',false);
        callback_action_admin();
    }

    <?=$callback_function_view?>
    <?=$callback_function_btn?>

</script>
