<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">

    <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                </header>

                <div class="panel-body">
                    <div class="adv-table">

                        <div class="filter-block">
                            <form class="form-horizontal adminex-form" enctype="multipart/form-data">

                                <div class="form-group">

                                    <div class="col-sm-4">

                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label" style=" text-align: left; padding-left: 0;">Підтвердження&nbsp;Email</label>
                                            <select name="emailApproved" class="form-control">
                                                <option value="">---</option>
                                                <option <?=(isset($filter['emailApproved']))?(($filter['emailApproved']=='approved')?'selected':''):''?> value="approved">Підтверджено</option>
                                                <option <?=(isset($filter['emailApproved']))?(($filter['emailApproved']=='pending')?'selected':''):''?> value="pending">Не підтверджено</option>

                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label" style=" text-align: left;padding-left: 0;">Тип користувача</label>
                                            <select name="group" class="form-control">
                                                <option value="">---</option>
                                                <option <?=(isset($filter['group']))?(($filter['group']=='manager')?'selected':''):''?> value="manager">Менеджер</option>
                                                <option <?=(isset($filter['group']))?(($filter['group']=='owner')?'selected':''):''?> value="owner">Власник</option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-sm-4">

                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label" style=" text-align: left; padding-left: 0;">Тип&nbsp;компанії</label>
                                            <select name="companyType" class="form-control">
                                                <option value="">---</option>
                                                <?
                                                foreach($company_types as $c_t)
                                                {
                                                    ?>
                                                    <option <?=(isset($filter['companyType']))?(($filter['companyType']==$c_t['ID'])?'selected':''):''?> value="<?=$c_t['ID']?>"><?=$c_t['name']?></option>
                                                <?
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label" style=" text-align: left;padding-left: 0;">На&nbsp;сайті</label>
                                            <select name="registered" class="form-control">
                                                <option value="">---</option>
                                                <?
                                                foreach($register_times as $k => $v)
                                                {
                                                    ?>
                                                    <option <?=(isset($filter['registered']))?(($filter['registered']==$k)?'selected':''):''?> value="<?=$k?>"><?=$v?></option>
                                                <?
                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-sm-2">
                                        <label class="col-sm-4 control-label" style="text-align: left;width: 85px;">Датa&nbsp;реєстрації</label>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="dateCreate1" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate1']))?$filter['dateCreate1']:''?>" placeholder="від">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px" name="dateCreate2" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate2']))?$filter['dateCreate2']:''?>" placeholder="до">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <div class="btn btn-link btn_select_widget" id="clearDateCreate">Очистити</div>
                                            <script type="text/javascript">
                                                $(document).on( "click", "#clearDateCreate", function(){
                                                    $("[name=dateCreate1]").val('');
                                                    $("[name=dateCreate2]").val('');
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;ID</label>
                                        <div class="col-sm-12" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="ID" type="text" class="form-control" value="<?=(isset($filter['ID']))?$filter['ID']:''?>" placeholder="введіть ID">
                                        </div>
                                        <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;імені</label>
                                        <div class="col-sm-12" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="name" type="text" class="form-control" value="<?=(isset($filter['name']))?$filter['name']:''?>" placeholder="введіть ім'я">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">

                                        <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;Email</label>
                                        <div class="col-sm-12" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="email" type="text" class="form-control" value="<?=(isset($filter['email']))?$filter['email']:''?>" placeholder="введіть Email">
                                        </div>

                                        <label class="col-sm-12 control-label" style="text-align: left;">Пошук&nbsp;по&nbsp;телефону</label>
                                        <div class="col-sm-12" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="phone" type="text" class="form-control" value="<?=(isset($filter['phone']))?$filter['phone']:''?>" placeholder="введіть телефон">
                                        </div>

                                    </div>

                                    <div class="col-sm-4">
                                        <div class="col-sm-12" style="margin-top: 26px">
                                            <button style="margin:0 !important;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Застосувати</button>
                                        </div>
                                        <div class="col-sm-12" style="margin-top: 10px">
                                            <a style="margin:0 !important;" href="/cms/<?=$lang?>/user" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Скинути фільтри</a>
                                        </div>
                                    </div>

                                </div>



                            </form>

                        </div>

                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="example">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ім'я<br>--<br>Тип</th>
                                <th>Компанія</th>
                                <th>Транспорт/Вантажі/Тендери</th>
                                <th>Відгуки(позитивні/нейтральні/негативні)</th>
                                <th>Власний транспорт</th>
                                <th>Новини</th>
                                <th>Куплено валюти</th>
                                <th>Баланс</th>
                                <th>На сайті</th>
                                <th>Контакти</th>
                                <th>Дії</th>
                            </tr>
                            </thead>


                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){

        window.table = $('#example').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/user/db/",
                type: "POST",
                data : {
                    filter: <?=(!empty($filter))?json_encode($filter):'1'?>
                }
            },
            drawCallback: function() {
                $(".select_all").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"ID",
                    render: function (val, type, row) {
                        return row.ID;
                    }
                },
                {
                    className: "center",data:"name","orderable": false,
                    render: function (val, type, row) {
                        return '<a target="_blank" href="/cms/uk/user/edit/'+row.ID+'" class="link_name">'+row.name+'</a><br>--<br>'+row.type;
                    }
                },
                {
                    className: "center",data:"company","orderable": false,
                    render: function (val, type, row) {
                        return row.company+'<br>--<br>'+row.company_type;
                    }
                },
                {
                    className: "center",data:"requests","orderable": false,
                    render: function (val, type, row) {
                        return row.transport_req+row.cargo_req+row.tenders;
                    }
                },
                {
                    className: "center",data:"comments","orderable": false,
                    render: function (val, type, row) {
                        return row.positive+row.neutral+row.negative;
                    }
                },
                {
                    className: "center",data:"transports","orderable": false,
                    render: function (val, type, row) {
                        return row.transports;
                    }
                },
                {
                    className: "center",data:"news","orderable": false,
                    render: function (val, type, row) {
                        return row.news;
                    }
                },
                {
                    className: "center",data:"valuta","orderable": false,
                    render: function (val, type, row) {
                        return row.valuta;
                    }
                },
                {
                    className: "center",data:"walletBalance","orderable": false,
                    render: function (val, type, row) {
                        return row.walletBalance;
                    }
                },
                {
                    className: "center",data:"register","orderable": false,
                    render: function (val, type, row) {
                        return row.register_date;
                    }
                },
                {
                    className: "center",data:"contacts","orderable": false,
                    render: function (val, type, row) {

                        var apr = row.emailApproved;
                        var apr_text = (apr=='approved')?'<span class="label label-success">Підтверджений</span>':'<span class="label label-warning">Не підтверджено</span>';
                        var phone = (row.phone)?row.phone:'';
                        var phone2 = (row.phone2)?',<br>'+row.phone2:'';
                        var phones = phone+''+phone2;
                        if (phones) phones = 'Телефон(и):<br> '+phones+'<br>';
                        var skype = (row.skype)?'Skype: '+row.skype+'<br>':'';
                        var icq = (row.icq)?'ICQ: '+row.icq+'<br>':'';
                        return row.email+'<br>'+apr_text+'<br>'+phones+skype+icq;
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {

                        var res = '<div style="min-width: 80px;">';
                        res += '<a style="margin-right:2px;" href="<?=$edit_url?>'+row.ID+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-name="'+row.name+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one", function(){
        var c_count = 0;
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one").prop('checked',true);
            $(".select_all").prop('checked',true);
        }
        else
        {
            $(".select_one").prop('checked',false);
            $(".select_all").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення користувача</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення користувача з ел. адресою "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        $("#name_del").html(name);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/user/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення групи користувачів</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних користувачів?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/user/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all").prop('checked',false);
        callback_action();
    }
</script>

