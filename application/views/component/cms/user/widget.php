<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>


<div class="panel-body">
    <div class="adv-table">

        <div class="main_search">
            <button class="btn btn-primary main_search_btn" id="search_user_widget_btn">Шукати</button>
            <input type="text" class="form-control main_search_input" name="search_user_widget" placeholder="Пошук">
        </div>

        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_user">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <?
                $i=1;
                foreach($langs as $l)
                {
                    $i++;
                    ?>
                    <th>Ім'я<br><?=$l['title']?></th>
                <?
                }
                ?>
                <th>Email</th>
            </tr>
            </thead>

        </table>

    </div>
</div>


<script type="text/javascript">


    $(function(){


        $('#row_for_search_user th').each( function () {

            var ind = $(this).index();

            if(ind > 0)
            {
                $(this).append( '<input index="'+ind+'" type="text" style="width:95%" />' );
            }

        } );

        window.table_user = $('#table_user').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/user/db_widget/",
                type: "POST",
                data:{
                    company_id:'<?=(isset($company_id))?$company_id:''?>',
                    isset_car:'<?=(isset($isset_car))?$isset_car:''?>',
                    user_type:'<?=(isset($user_type))?$user_type:''?>'
                }
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    "orderable": false,
                    render: function (val, type, row) {
                        var name = '';
                        <?
                            foreach($langs as $l)
                            {
                                ?>
                                    if (row['name_<?=$l['uri']?>']!=null && !name) name = row['name_<?=$l['uri']?>'];
                                <?
                            }
                        ?>
                        return '<input type="radio" name="select_one_user" class="select_one_user" data-id="'+row.ID+'" data-name="'+name+'">';
                    }
                },
                {
                    className: "center",data:"ID","orderable": false,
                    render: function (val, type, row) {
                        var avatar = '<img class="avatar-img" src="'+row.avatar+'"><br>';
                        return avatar+'<div class="user-name">'+row.ID+'</div>';
                    }
                },
                <?
                    $count_langs = count($langs);
                    $kl = 0;
                    foreach($langs as $l)
                    {
                        $kl++;
                        ?>
                          {
                            className: "center",data:"name_<?=$l['uri']?>","orderable": false,
                                    render: function (val, type, row) {
                                    var name = (row['name_<?=$l['uri']?>']!=null)?row['name_<?=$l['uri']?>']:'-';
                                    return '<div class="user-name">'+name+'</div>';
                                    }
                          },
                        <?
                    }
                ?>
                {
                    className: "center",data:"email","orderable": false
                }

            ],
            "order": [[1, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );

        $(document).on( "click", "#search_user_widget_btn", function(e){
            e.preventDefault();
            go_search_user_widget();
        });

        $('body').keyup(function(e){
            if(event.keyCode==13)
            {
                e.preventDefault();
                var focus = $("[name=search_user_widget]").is( ":focus" );
                if (focus)
                {
                    go_search_user_widget();
                }

            }
        });

    });

    function go_search_user_widget()
    {
        var search = $("[name=search_user_widget]").val();
        window.table_user.search(search).draw(false);
    }

    function callback_action_user()
    {
        window.table_user.draw(false);
    }


    $(document).on( "click", ".select_one_user", function(){
        var c_count = 0;
        $(".select_one_user").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });


</script>


<script type="text/javascript">

    function set_all_ids_user()
    {
        var ids = [];
        var names = [];
        $(".select_one_user").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
                names.push($(this).data('name'));
            }
        });

        window.ids_user = ids;
        window.names_user = names;
    }

    function callback_itemAll_user()
    {
        $(".select_all_user").prop('checked',false);
        callback_action_user();
    }

    <?=$callback_function_view?>
    <?=$callback_function_btn?>

</script>
