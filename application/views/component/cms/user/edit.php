<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                </header>
                <div class="panel-body">
                <div class="module_desc_div">
                    <span class="required_optional_span">*</span> - обов'язкове до заповнення одне із полів<br>
                    <span class="required_span">*</span> - обов'язкові до заповнення поля<br>
                </div>
                    <?
                    if (isset($change_ok))
                    {
                        ?>
                        <div class="alert alert-success fade in">
                            Профіль користувача успішно оновлено
                        </div>
                    <?
                    }

                    if (isset($errors))
                    {
                        extract($errors,EXTR_PREFIX_ALL,"error");
                    }


                    if (isset($error_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Довжина пароля повинна бути не меншою ніж 6 символів. Пароль може містити цифри, букви латинського алфавіту і наступні символи "_-.,;"
                        </div>
                    <?
                    }

                    if (isset($error_conf_pass))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Пароль і підтвердження пароля повинні співпадати
                        </div>
                    <?
                    }

                    if (isset($error_email_isset))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Вказана електронна пошта уже зареєстрований в системі
                        </div>
                    <?
                    }

                    if (isset($error_companyID))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Не вибрана компанія
                        </div>
                    <?
                    }

                    if (isset($error_not_isset_name))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Ім'я користувача не може бути пустим
                        </div>
                    <?
                    }

                    if (isset($error_name))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Ім'я може містити букви, пробіл і символ "-"
                        </div>
                    <?
                    }

                    if (isset($error_phoneCodeID))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Не вибрано телефонний код користувача
                        </div>
                    <?
                    }

                    if (isset($error_empty_phone))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Телефон користувача не вказано
                        </div>
                    <?
                    }


                    if (isset($error_phone))
                    {
                        ?>
                        <div class="alert alert-danger fade in">
                            Телефон користувача може містити цифри, пробіл і символи "(" та ")"
                        </div>
                    <?
                    }

                    ?>

                    <form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data">


                        <?
                        foreach($langs as $l)
                        {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Ім'я - <?=$l['title']?>&nbsp;<span class="required_optional_span">*</span></label>
                                <div class="col-sm-7">
                                    <input name="name[<?=$l['ID']?>]" type="text" class="form-control m-bot15" maxlength="100" placeholder="до 100 символів" value="<?=(isset($info['locales'][$l['ID']]))?$info['locales'][$l['ID']]['name']:''?>">
                                </div>
                            </div>
                        <?
                        }
                        ?>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-7">
                                <input name="email" type="email" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів" value="<?=$info['info']['email']?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-2 abton-form-checkbox-title">Email підтверджено</label>
                            <div class="col-sm-1 abton-form-checkbox">
                                <div class="flat-grey single-row icheck form_checkbox">
                                    <input type="checkbox" name="emailApproved" <?=($info['info']['emailApproved']=='approved')?'checked':''?>>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">Компанія&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-10">
                                <div class="btn btn-default btn_select_widget" id="select_company">Вибрати компанію</div>
                                Поточна компанія: <span class="widget_label" id="company_name"><?=$info['info']['company_name']?></span><span class="close_span" id="close_span_company" style="color:red">x</span>
                                <input type="hidden" name="companyID" value="<?=$info['info']['companyID']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Телефон&nbsp;<span class="required_span">*</span></label>
                            <div class="col-sm-2">
                                <select name="phoneCodeID" class="form-control m-bot15">
                                    <option value="">-Код-</option>
                                    <?
                                    foreach($phone_codes as $p_c)
                                    {
                                        ?>
                                        <option <?=($info['info']['phoneCodeID'] == $p_c['ID'])?'selected':''?> value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input name="phone" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" value="<?=$info['info']['phone']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Стаціонарний телефон</label>
                            <div class="col-sm-2">
                                <select name="phoneStationaryCodeID" class="form-control m-bot15">
                                    <option value="">-Код-</option>
                                    <?
                                    foreach($phone_codes as $p_c)
                                    {
                                        ?>
                                        <option <?=($info['info']['phoneStationaryCodeID'] == $p_c['ID'])?'selected':''?> value="<?=$p_c['ID']?>"><?=$p_c['code']?></option>
                                    <?
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input name="phoneStationary" type="tel" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" value="<?=$info['info']['phoneStationary']?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">ICQ</label>
                            <div class="col-sm-7">
                                <input name="icq" type="text" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів" value="<?=$info['info']['icq']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Skype</label>
                            <div class="col-sm-7">
                                <input name="skype" type="text" class="form-control m-bot15" maxlength="50" placeholder="до 50 символів" value="<?=$info['info']['skype']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label abton-form-checkbox-title">Змінити пароль</label>
                            <div class="col-sm-1 abton-form-checkbox">
                                <div class="flat-grey single-row icheck form_checkbox">
                                        <input type="checkbox" name="change_pass">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Пароль</label>
                            <div class="col-sm-7">
                                <input name="pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Повтор пароля</label>
                            <div class="col-sm-7">
                                <input name="conf_pass" type="password" class="form-control m-bot15" maxlength="20" placeholder="до 20 символів">
                            </div>
                        </div>



                        <div class="form-group">

                            <label class="control-label col-sm-2">Фото</label>

                                <div class="col-sm-3">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">

                                        <?
                                        $dir=$_SERVER['DOCUMENT_ROOT'].'/uploads/files/public/user/avatar/'.$info['info']['ID'].'/';

                                        if (file_exists($dir))
                                        {
                                            if ($handle = opendir($dir)) {
                                                while (false !== ($entry = readdir($handle))) {
                                                    if ($entry != "." && $entry != "..") {
                                                        $img = $entry;
                                                    }
                                                }
                                                closedir($handle);
                                            }
                                        }

                                        if (isset($img))
                                        {
                                            $img = '/uploads/files/public/user/avatar/'.$info['info']['ID'].'/'.$img;
                                        }
                                        ?>

                                        <div id="no_image" class="fileupload-new thumbnail" style="width: 200px; height: 150px; <?=(isset($img))?'display:none;':''?>">
                                            <img src="/public/cms/img/no_image_small.png">
                                        </div>
                                        <div id="image_ex" class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px; <?=(isset($img))?'display:block;':''?>">
                                            <?
                                            if (isset($img))
                                            {
                                                ?>
                                                <img src="<?=$img?>" style="max-height: 150px;">
                                            <?
                                            }
                                            ?>
                                        </div>
                                        <div style="width: 203px;">
                                                   <span id="reload_file" class="btn btn-default btn-file">
                                                   <span id="load_file" style="display:none;" class="fileupload-new"><i class="fa fa-paper-clip"></i> Вибрати</span>
                                                   <span style="display:inline-block;" class="fileupload-exists"><i class="fa fa-undo"></i> Змінити</span>
                                                   <input type="file" class="default" name="img" onchange="setStatus()">
                                                   </span>

                                            <a id="del_file" style="display:inline-block;" class="btn btn-danger fileupload-exists"><i class="fa fa-trash"></i> Видалити</a>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <?
                        $is_file = (isset($img))?1:0;
                        ?>
                        <input type="hidden" name="file_exist" value="<?=$is_file?>">

                        <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary btn-submit-form">Зберегти</button>
                    </form>


                </div>
            </section>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on( "click", "#del_file", function(){
        $("#no_image").show();
        $("#image_ex").hide();
        $("[name=file_exist]").val('0');

    });

    $(document).on( "click", "#reload_file", function(){
        $("#image_ex").show();
    });
</script>
<script type="text/javascript">
    function setStatus()
    {
        $("[name=file_exist]").val('1');
    }
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="company_user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір компанії</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('company','widget','ru',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_company", function () {
                            $("#company_user_modal").modal();
                            callback_action_company();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_company_to_user", function () {
                                set_all_ids_company();
                                for (var i = 0; i < window.ids_company.length; i++) {
                                    $("[name=companyID]").val(window.ids_company[i]);
                                    $("#company_name").html(window.names_company[i]);
                                    $("#close_span_company").html("x");
                                }
                        });'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_company_to_user" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on( "click", "#close_span_company", function(){
        $("[name=companyID]").val('');
        $(this).html('');
        $('#company_name').html('не вказано');
    });

</script>



