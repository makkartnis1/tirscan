<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    .table_data tr:first-child th
    {
        border-bottom:1px solid #DDDDDD !important;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>

<div class="wrapper">
<div class="row">

<div class="col-sm-12">
<section class="panel">
<header class="panel-heading">
    <?=$base_title?>
</header>

<div class="panel-body" style="padding: 0 15px">
<div class="adv-table">


<div class="filter-block">
<form class="form-horizontal adminex-form" enctype="multipart/form-data">


<div class="form-group">


    <div class="col-sm-2">
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Користувач</label>
            <div class="col-sm-9">
                            <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="user_name">
                                <?
                                if (isset($userID))
                                {
                                    ?>
                                    <span id="close_span_user"><?=$userName?>&nbsp;<span style="color:red">x</span></span>
                                <?
                                }
                                ?>
                            </span>
                <div class="btn btn-link btn_select_widget" id="select_user">Вказати</div>
                <input type="hidden" name="userID" value="">
            </div>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-12 control-label">Компанія</label>
            <div class="col-sm-9" style="width: 160px">
                            <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="company_name">
                                <?
                                if (isset($companyID))
                                {
                                    ?>
                                    <span id="close_span_company"><?=$companyName?>&nbsp;<span style="color:red">x</span></span>
                                    <?
                                }
                                ?>
                            </span>
                <div class="btn btn-link btn_select_widget" id="select_company">Вказати</div>
                <input type="hidden" name="companyID" value="">
            </div>
        </div>
    </div>

    <div class="col-sm-2">
            <label class="col-sm-12 control-label" style="text-align: left;width: 85px;">Пошук&nbsp;по&nbsp;тексту</label>
            <div class="col-sm-12" style="text-align: left;">
                <input style="min-width: 140px;margin-bottom:3px" name="text" type="text" class="form-control" value="<?=(isset($text))?$text:''?>" placeholder="введіть текст">
            </div>
    </div>


    <div class="col-sm-2">
        <div class="col-sm-12" style="margin-top: 30px">
            <button style="margin:0 15px;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Застосувати</button>
        </div>
        <div class="col-sm-12" style="margin-top: 10px">
            <a href="/cms/<?=$lang?>/complaints" style="margin:0 15px;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Скинути фільтри</a>
        </div>
    </div>

</div>

</form>

</div>


<table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport_req">
    <thead>
    <tr>
        <th style="min-width: 20px">№</th>
        <th>Текст</th>
        <th>Користувач</th>
        <th>Компанія</th>
        <th style="min-width: 110px">Дата створення</th>
        <th>Статус</th>
        <th>Дії</th>
    </tr>
    </thead>

</table>

</div>
</div>

</section>
</div>
</div>
</div>

<script type="text/javascript">

    $(function(){


        window.table = $('#table_transport_req').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/complaints/db/",
                type: "POST",
                data : {
                    get: <?=(!empty($filter))?json_encode($filter):'1'?>
                }
            },
            drawCallback: function() {
                $(".select_all_req").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"ID"
                },
                {
                    className: "center",data:"message","orderable": false,
                    render: function (val, type, row) {

                        var text = row.short_message;

                        if (row.message != row.short_message)
                        {
                            text+=' <button class="btn view_full_text" data-full="'+row.message+'" data-toggle="button"><i class="fa fa-eye"></i></button>';
                        }

                        return text;
                    }
                },
                {
                    className: "center",data:"userID","orderable": false,
                    render: function (val, type, row) {
                        var user = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/user/edit/'+row.userID+'">'+row.userName+'</a>';
                        return user;
                    }
                },
                {
                    className: "center",data:"complainCompanyID","orderable": false,
                    render: function (val, type, row) {
                        var company = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/company/edit/'+row.complainCompanyID+'">'+row.companyName+'</a>';
                        return company;
                    }
                },
                {
                    className: "center",data:"created","orderable": false
                },
                {
                    className: "center",data:"status","orderable": false,
                    render: function (val, type, row) {

                        var status = '<span class="label label-default">Прочитано</span>';

                        if (row.status == 'unread') status = '<span class="label label-danger">Непрочитано</span>';

                        return status;
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one_req", function(){
        var c_count = 0;
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all_req", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one_req").prop('checked',true);
            $(".select_all_req").prop('checked',true);
        }
        else
        {
            $(".select_one_req").prop('checked',false);
            $(".select_all_req").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення скарги</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення скарги "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        $("#name_del").html(id);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/complaints/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>



<script type="text/javascript">

    function callback_itemAll()
    {
        $(".select_all_req").prop('checked',false);
        callback_action();
    }
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_transport", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    window.user_id_tr = window.ids_user[i];
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#user_name").html("<span id=\"close_span_user\">"+window.names_user[i]+"&nbsp;<span style=\"color:red\">x</span></span>");

                                }
                        });'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_transport" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="company_transport_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір компанії</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('company','widget','ru',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_company", function () {
                            $("#company_transport_modal").modal();
                            callback_action_company();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_company_to_transport", function () {
                                set_all_ids_company();
                                for (var i = 0; i < window.ids_company.length; i++) {
                                    $("[name=companyID]").val(window.ids_company[i]);
                                    $("#company_name").html("<span id=\"close_span_company\">"+window.names_company[i]+"&nbsp;<span style=\"color:red\">x</span></span>");
                                }
                        });'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_company_to_transport" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#close_span_company", function(){
        $("[name=companyID]").val('');
        $(this).html('');
        $('#company_name').html('');
    });
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="allTextModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Повний текст скарги</h4>
            </div>
            <div class="modal-body">
                <p id="text_comment_all"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".view_full_text", function(){
        var text = $(this).data('full');
        $("#text_comment_all").html(text);
        $("#allTextModal").modal('show');
    });
</script>