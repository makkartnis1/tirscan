<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    .table_data tr:first-child th
    {
        border-bottom:1px solid #DDDDDD !important;
    }
    #close_span_company
    {
        cursor: pointer;
    }
    #close_span_user
    {
        cursor: pointer;
    }
    .form-group
    {
        margin-bottom: 0;
        margin-top: 0;
        padding-bottom: 0;
        padding-top: 0;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<div class="wrapper">
    <div class="row">

        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                </header>

                <div class="panel-body" style="padding: 0 15px">
                    <div class="adv-table">


                        <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport_req">
                            <thead>
                            <tr>
                                <th style="width: 25px">№</th>
                                <th>Між</th>
                                <th>Тема</th>
                                <th>Останнє повідомлення</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){


        window.table = $('#table_transport_req').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/userdialogs/db/",
                type: "POST"
            },
            drawCallback: function() {
                $(".select_all_req").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"id"
                },
                {
                    className: "center",data:"from_to","orderable": false,
                    render: function (val, type, row) {

                        var fromRole = 'пользователь';
                        var toRole = 'пользователь';

                        var fromID = row.fromUserID;
                        var toID = row.toUserID;

                        var from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/user/edit/'+fromID+'">'+row.fromUserName+'</a>';
                        var to = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/user/edit/'+toID+'">'+row.toUserName+'</a>';

                        var fromAdmin = row.fromAdminName;
                        var toAdmin = row.toAdminName;

                        if (fromAdmin)
                        {
                            fromID = row.fromAdminID;
                            fromRole = 'администратор';
                            from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/admin/edit/'+fromID+'">'+fromAdmin+'</a>';
                        }

                        if (toAdmin)
                        {
                            toID = row.toAdminID;
                            toRole = 'администратор';
                            to = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/admin/edit/'+toID+'">'+toAdmin+'</a>';
                        }



                        return from+'<br><br>-<br>'+to+'<br>';
                    }
                },
                {
                    className: "center",data:"theme","orderable": false
                },
                {
                    className: "left",data:"message","orderable": false,
                    render: function (val, type, row) {


                        var from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/user/edit/'+row.userMessageID+'">'+row.userName+'</a>';

                        if (row.adminMessageID)
                        {
                            from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/admin/edit/'+row.adminMessageID+'">'+row.adminName+'</a>';
                        }

                        var text = row.short_message;

                        if (row.countUnrMess > 0)
                        {
                            text += '&nbsp;<count class="badge badge-important">'+row.countUnrMess+'</count>';
                        }

                        return '<i>'+from+'<br>'+row.created+'</i><br><a class="link-standart text-mess" href="<?=$show_url?>'+row.id+'">'+text+'</a>';
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$show_url?>'+row.id+'" class="btn btn-info"><i class="fa fa-eye"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.id+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one_req", function(){
        var c_count = 0;
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all_req", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one_req").prop('checked',true);
            $(".select_all_req").prop('checked',true);
        }
        else
        {
            $(".select_one_req").prop('checked',false);
            $(".select_all_req").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення діалогу</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення діалогу з номером "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        $("#name_del").html(id);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/userDialogs/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення вибраних діалогів</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних діалогів?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/userDialogs/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all_req").prop('checked',false);
        callback_action();
    }
</script>
