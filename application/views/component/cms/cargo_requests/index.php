<style type="text/css">
    input[type=search]
    {
        display: none;
    }
    .dataTables_length
    {
        margin-bottom: 10px;
    }
    a
    {
        color:white;
    }
    .table_data tr:first-child th
    {
        border-bottom:1px solid #DDDDDD !important;
    }
</style>
<!-- DataTables, TableTools and Editor CSS -->
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

<!-- jQuery, DataTables, TableTools and Editor Javascript -->
<script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&language=uk&key=<?=$config['browser_key']?>"></script>
<script type="text/javascript" src="/public/cms/js/location.js"></script>

<div class="wrapper">
    <div class="row">

    <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    <?=$base_title?>
                </header>

                <div class="panel-body" style="padding: 0 15px">
                    <div class="adv-table">

                    <div class="filter-block">

                    <form class="form-horizontal adminex-form" enctype="multipart/form-data">

                        <?
                            if (isset($req_info))
                            {
                                ?>
                                    <div class="reg_related_info well">
                                        <h5>Зявка для якої знайдено підходящий транспорт</h5>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12"><b>Автор</b></div>
                                        <div class="col-sm-12">
                                            <i>Ім'я:</i> <?=$req_info['info']['user_name']?>
                                        </div>
                                        <div class="col-sm-12">
                                            <i>Телефон(и):</i>
                                            <?=($req_info['info']['phone'])?$req_info['info']['code'].$req_info['info']['phone'].' ':''?>
                                            <?=($req_info['info']['phoneStationary'])?$req_info['info']['code_stat'].$req_info['info']['phoneStationary'].' ':''?>
                                        </div>
                                        <div class="col-sm-12">
                                            <i>Компанія:</i> <?=$req_info['info']['company_name']?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12"><b>Завантаження</b></div>
                                        <div class="col-sm-12"><i>Місця:</i>
                                            <?
                                                foreach($req_info['geo_groups'] as $group)
                                                {
                                                    if ($group['type'] == 'to') continue;
                                                    ?>
                                                    <div>
                                                        <?=$group['name']?>
                                                    </div>
                                                    <?
                                                }
                                            ?>
                                        </div>
                                        <div class="col-sm-12"><i>Дата:</i> <?=$req_info['info']['dateFrom']?></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12"><b>Розвантаження</b></div>
                                        <div class="col-sm-12"><i>Місця:</i>
                                            <?
                                            foreach($req_info['geo_groups'] as $group)
                                            {
                                                if ($group['type'] == 'from') continue;
                                                ?>
                                                <div>
                                                    <?=$group['name']?>
                                                </div>
                                            <?
                                            }
                                            ?>
                                        </div>
                                        <div class="col-sm-12"><i>Дата:</i> <?=$req_info['info']['dateTo']?></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12"><b>Транспорт</b></div>
                                        <div class="col-sm-12">
                                            <?=$req_info['transport']?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="col-sm-12"><b>Вартість</b></div>
                                        <div class="col-sm-12">
                                            <?=$req_info['price']?>
                                        </div>
                                    </div>
                                </div>
                            <?
                            }
                            else
                            {
                                ?>
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label" style="text-align: left;">Датa&nbsp;створення</label>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="dateCreate1" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate1']))?$filter['dateCreate1']:''?>" placeholder="від">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px" name="dateCreate2" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateCreate2']))?$filter['dateCreate2']:''?>" placeholder="до">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <div class="btn btn-link btn_select_widget" id="clearDateCreate">Очистити</div>
                                            <script type="text/javascript">
                                                $(document).on( "click", "#clearDateCreate", function(){
                                                    $("[name=dateCreate1]").val('');
                                                    $("[name=dateCreate2]").val('');
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="col-sm-12 control-label label_left">Завантаження</label>
                                        <div class="col-sm-12">
                                            <div id="places_from_block">


                                                <?
                                                if (isset($filter['places_from']))
                                                {
                                                    $i_from_group = 0;
                                                    foreach($filter['places_from_groups'] as $geo_group)
                                                    {
                                                        $i_from_group++;
                                                        ?>
                                                        <div class="one_place_block">
                                                            <input type="text" id="place_from_<?=$i_from_group?>" class="form-control dynamic_place_inputs" value="<?=$geo_group['name']?>">
                                                            <input name="places_from[]" id="place_from_<?=$i_from_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                                            <button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    <?
                                                    }
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="one_place_block">
                                                        <input type="text" id="place_from_1" class="form-control dynamic_place_inputs">
                                                        <input name="places_from[]" id="place_from_1_places" type="hidden">
                                                    </div>
                                                <?
                                                }
                                                ?>

                                            </div>
                                            <div id="add_place_from" style="padding-left: 0" class="btn btn-link">Додати місце</div>
                                        </div>

                                        <label class="col-sm-1 control-label" style="text-align: left; position: relative; bottom: 3px;min-width: 35px">Радіус</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="number" name="radius_from" placeholder="км" min="1" value="<?=(isset($filter['radius_from']))?(($filter['radius_from'] > 0)?$filter['radius_from']:''):''?>">
                                        </div>

                                    </div>

                                    <div class="col-sm-4">
                                        <label class="col-sm-12 control-label label_left">Розвантаження</label>
                                        <div class="col-sm-12">
                                            <div id="places_to_block">

                                                <?
                                                if (isset($filter['places_to']))
                                                {
                                                    $i_to_group = 0;
                                                    foreach($filter['places_to_groups'] as $geo_group)
                                                    {
                                                        $i_to_group++;
                                                        ?>
                                                        <div class="one_place_block">
                                                            <input type="text" id="place_to_<?=$i_to_group?>" class="form-control dynamic_place_inputs" value="<?=$geo_group['name']?>">
                                                            <input name="places_to[]" id="place_to_<?=$i_to_group?>_places" type="hidden" value="<?=implode(',',$geo_group['ids'])?>">
                                                            <button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    <?
                                                    }
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="one_place_block">
                                                        <input type="text" id="place_to_1" class="form-control dynamic_place_inputs">
                                                        <input name="places_to[]" id="place_to_1_places" type="hidden">
                                                    </div>
                                                <?
                                                }
                                                ?>
                                            </div>
                                            <div id="add_place_to" style="padding-left: 0" class="btn btn-link">Додати місце</div>
                                        </div>

                                        <label class="col-sm-1 control-label" style="text-align: left; position: relative; bottom: 3px;min-width: 35px">Радіус</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="number" name="radius_to" placeholder="км" min="1" value="<?=(isset($filter['radius_to']))?(($filter['radius_to'] > 0)?$filter['radius_to']:''):''?>">
                                        </div>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label" style="text-align: left;">Датa&nbsp;завантаження&nbsp;від</label>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="dateFrom1" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateFrom1']))?$filter['dateFrom1']:''?>" placeholder="від">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px" name="dateFrom2" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateFrom2']))?$filter['dateFrom2']:''?>" placeholder="до">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <div class="btn btn-link btn_select_widget" id="clearDateFrom">Очистити</div>
                                            <script type="text/javascript">
                                                $(document).on( "click", "#clearDateFrom", function(){
                                                    $("[name=dateFrom1]").val('');
                                                    $("[name=dateFrom2]").val('');
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label" style="text-align: left;">Датa&nbsp;завантаження&nbsp;до</label>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px;margin-bottom:3px" name="dateTo1" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateTo1']))?$filter['dateTo1']:''?>" placeholder="від">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <input style="min-width: 140px" name="dateTo2" type="text" class="form-control default-date-picker" size="16" value="<?=(isset($filter['dateTo2']))?$filter['dateTo2']:''?>" placeholder="до">
                                        </div>
                                        <div class="col-sm-7" style="text-align: left;">
                                            <div class="btn btn-link btn_select_widget" id="clearDateTo">Очистити</div>
                                            <script type="text/javascript">
                                                $(document).on( "click", "#clearDateTo", function(){
                                                    $("[name=dateTo1]").val('');
                                                    $("[name=dateTo2]").val('');
                                                });
                                            </script>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label">Користувач</label>
                                            <div class="col-sm-9">
                                <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="user_name">
                                    <?
                                    if (isset($filter['userName']))
                                    {
                                        ?>
                                        <span id="close_span_user"><?=$filter['userName']?>&nbsp;<span style="color:red">x</span></span>
                                    <?
                                    }
                                    ?>
                                </span>
                                                <div class="btn btn-link btn_select_widget" id="select_user">Вказати</div>
                                                <input type="hidden" name="userID" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label">Компанія</label>
                                            <div class="col-sm-9" style="width: 160px">
                                <span style="position: relative;padding: 0;top: 1px;text-align: left" class="widget_label control-label" id="company_name">
                                    <?
                                    if (isset($filter['companyName']))
                                    {
                                        ?>
                                        <span id="close_span_company"><?=$filter['companyName']?>&nbsp;<span style="color:red">x</span></span>
                                    <?
                                    }
                                    ?>
                                </span>
                                                <div class="btn btn-link btn_select_widget" id="select_company">Вказати</div>
                                                <input type="hidden" name="userCompanyID" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="col-sm-12">
                                            <label class="col-sm-12 control-label" style="text-align: left;">Статус</label>
                                            <div class="col-sm-7">
                                                <select name="status" class="form-control" style="min-width: 140px">
                                                    <option value="">---</option>
                                                        <option <?=(isset($filter['status']))?(($filter['status']=='active.have-no-proposition')?'selected':''):''?> value="active.have-no-proposition">Без пропозицій</option>
                                                        <option <?=(isset($filter['status']))?(($filter['status']=='active.have-confirmed')?'selected':''):''?> value="active.have-confirmed">В роботі</option>
                                                        <option <?=(isset($filter['status']))?(($filter['status']=='active.have-not-confirmed')?'selected':''):''?> value="active.have-not-confirmed">Не підтверджені</option>
                                                        <option <?=(isset($filter['status']))?(($filter['status']=='completed')?'selected':''):''?> value="completed">Завершені</option>
                                                        <option <?=(isset($filter['status']))?(($filter['status']=='outdated')?'selected':''):''?> value="outdated">Прострочені</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="col-sm-3 control-label" style="width: 150px; text-align: left;">Тип транспорту</label>
                                            <div class="col-sm-9" style="width: 160px">
                                                <select name="carTypeID" class="form-control" style="min-width: 140px">
                                                    <option value="">---</option>
                                                    <?
                                                    foreach($type_transports as $t)
                                                    {
                                                        ?>
                                                        <option <?=(isset($filter['carTypeID']))?(($filter['carTypeID']==$t['ID'])?'selected':''):''?> value="<?=$t['ID']?>"><?=$t['name']?></option>
                                                    <?
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-2">
                                        <div class="col-sm-12" style="margin-top: 30px">
                                            <button style="margin:0 15px;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Застосувати</button>
                                        </div>
                                        <div class="col-sm-12" style="margin-top: 10px">
                                            <a href="/cms/<?=$lang?>/cargoRequests" style="margin:0 15px;" type="submit" name="goFilter" class="btn btn-primary btn-submit-form">Скинути фільтри</a>
                                        </div>
                                    </div>

                                </div>

                                <script type="text/javascript">

                                    $(function(){

                                        <?
                                        $num_place_from = 0;
                                        $num_place_to = 0;

                                        if (isset($filter['places_from']))
                                        {
                                            foreach($filter['places_from_groups'] as $geo_group)
                                            {
                                                $num_place_from++;
                                                ?>
                                        google_place_init('place_from_<?=$num_place_from?>','uk');
                                        <?
                                    }
                                }

                                if (isset($filter['places_to']))
                                {
                                    foreach($filter['places_to_groups'] as $geo_group)
                                    {
                                        $num_place_to++;
                                        ?>
                                        google_place_init('place_to_<?=$num_place_to?>','uk');
                                        <?
                                    }
                                }

                                ?>

                                        window.num_place_from = '<?=$num_place_from?>';
                                        window.num_place_to = '<?=$num_place_to?>';

                                        if (window.num_place_from == 0)
                                        {
                                            window.num_place_from = 1;
                                            google_place_init('place_from_1','uk');
                                        }
                                        if (window.num_place_to == 0)
                                        {
                                            window.num_place_to = 1;
                                            google_place_init('place_to_1','uk');
                                        }


                                    });

                                    $(document).on( "click", "#add_place_from", function(){
                                        window.num_place_from++;
                                        var text = '<div class="one_place_block">' +
                                            '<input type="text" id="place_from_'+window.num_place_from+'" class="form-control dynamic_place_inputs"><input name="places_from[]" id="place_from_'+window.num_place_from+'_places" type="hidden">' +
                                            '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                                            '</div>';
                                        $("#places_from_block").append(text);
                                        google_place_init('place_from_'+window.num_place_from,'uk');
                                    });

                                    $(document).on( "click", "#add_place_to", function(){
                                        window.num_place_to++;
                                        var text = '<div class="one_place_block">' +
                                            '<input type="text" id="place_to_'+window.num_place_to+'" class="form-control dynamic_place_inputs"><input name="places_to[]" id="place_to_'+window.num_place_to+'_places" type="hidden">' +
                                            '<button class="btn btn-danger delete_place"><i class="fa fa-times"></i></button>' +
                                            '</div>';
                                        $("#places_to_block").append(text);
                                        google_place_init('place_to_'+window.num_place_to,'uk');
                                    });

                                    $(document).on( "click", ".delete_place", function(){
                                        $(this).parent(".one_place_block").remove();
                                    });

                                </script>
                                <?
                            }
                        ?>


                    </form>

                    </div>


                    <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport_req">
                            <thead>
                            <tr>
                                <th style="min-width: 60px">№<br>додано</th>
                                <th>Підходить транспорт</th>
                                <th>Завантаження</th>
                                <th>Розвантаження</th>
                                <th>Вантаж</th>
                                <th style="min-width: 100px">Транспорт</th>
                                <th>Автор</th>
                                <th>Статус і ціна</th>
                                <th>Дії</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>

            </section>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(function(){


        window.table = $('#table_transport_req').DataTable( {
            ajax: {
                url: "/component_cms/<?=$lang?>/cargoRequests/db/",
                type: "POST",
                data : {
                    filter: <?=(!empty($filter))?json_encode($filter):'1'?>
                }
            },
            drawCallback: function() {
                $(".select_all_req").prop('checked',false);
            },
            deferRender : true,
            serverSide: true,
            processing: true,
            columns: [
                {
                    className: "center",data:"ID",
                    render: function (val, type, row) {
                        return '№'+row.ID+'<br>'+row.created;
                    }
                },
                {
                    className: "center",data:"related","orderable": false
                },
                {
                    className: "center",data:"from_places","orderable": false,
                    render: function (val, type, row) {
                        return row.from_places+'<br>'+row.load_date;
                    }
                },
                {
                    className: "center",data:"to_places","orderable": false,
                    render: function (val, type, row) {
                        return row.to_places;
                    }
                },
                {
                    className: "center",data:"cargo","orderable": false,
                    render: function (val, type, row) {
                        return row.cargo;
                    }
                },
                {
                    className: "center",data:"transport","orderable": false,
                    render: function (val, type, row) {
                        return row.transport;
                    }
                },
                {
                    className: "center",data:"user","orderable": false,
                    render: function (val, type, row) {
                        return row.user;
                    }
                },
                {
                    className: "center",data:"requests","orderable": false,
                    render: function (val, type, row) {
                        var status = '<span class="label label-default">Без пропозицій</span>';
                        if (row.status == 'active.have-confirmed') status = '<span class="label label-primary">В роботі</span>';
                        if (row.status == 'active.have-not-confirmed') status = '<span class="label label-danger">Не підтверджена</span>';
                        if (row.status == 'completed') status = '<span class="label label-success">Завершена</span>';
                        if (row.status == 'outdated') status = '<span class="label label-warning">Прострочена</span>';
                        if (row.requests == '') return 'Заявки відстутні<br>'+status+"<br>Ціна: "+row.price;
                        return row.status_work+"<button class='btn btn-info see_requests' data-requests='"+row.requests+"'>Заявки</button><br>"+status+"<br>Ціна: "+row.price;
                    }
                },
                {
                    className: "center",
                    "orderable": false,
                    render: function (val, type, row) {
                        var res = '<div style="min-width: 80px;"><a style="margin-right:2px;" href="<?=$edit_url?>'+row.ID+'" class="btn btn-info"><i class="fa fa-pencil"></i></a>';
                        res += '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                        return res+'</div>';
                    }
                },
            ],
            "order": [[0, 'desc']],
            language: {
                processing:     "Завантажую..",
                search:         "Пошук&nbsp;:",
                lengthMenu:     "Записів на сторінці: _MENU_ ",
                info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                infoEmpty:      "Записів немає",
                infoFiltered:   "",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Даних немає",
                emptyTable:     "Даних немає",
                paginate: {
                    first:      "Перша",
                    previous:   "Назад",
                    next:       "Вперед",
                    last:       "Остання"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                }
            }

        } );


    });


    function callback_action()
    {
        window.table.draw(false);
    }


    $(document).on( "click", ".select_one_req", function(){
        var c_count = 0;
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                c_count++;
            }
        });

    });

    $(document).on( "click", ".select_all_req", function(){
        if ($(this).prop('checked'))
        {
            $(".select_one_req").prop('checked',true);
            $(".select_all_req").prop('checked',true);
        }
        else
        {
            $(".select_one_req").prop('checked',false);
            $(".select_all_req").prop('checked',false);
        }
    });

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення заявки</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення заявки з номером "<span id="name_del"></span>"?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", ".delete", function(){
        var id = $(this).data('id');
        $("#name_del").html(id);
        $("#delItem").attr('id_item',id);
        $("#delModal").modal();
    });

    $(document).on( "click", "#delItem", function(){
        var id = $(this).attr('id_item');
        $.post("/component_cms/<?=$lang?>/cargoRequests/ajax/",{id:id,action:'delete'},callback_action,"json");
        $("#delModal").modal('hide');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Видалення вибраних заявок</h4>
            </div>
            <div class="modal-body">
                Ви підтверджуєте видалення вибраних заявок?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $(document).on( "click", "#delete_all", function(){
        set_all_ids();
        if (window.ids.length > 0) $("#delModalAll").modal();
    });

    $(document).on( "click", "#delItemAll", function(){
        $.post("/component_cms/<?=$lang?>/cargoRequests/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
    });

    function set_all_ids()
    {
        var ids = [];
        $(".select_one_req").each(function(){
            if ($(this).prop('checked'))
            {
                ids.push($(this).data('id'));
            }
        });

        window.ids = ids;
    }

    function callback_itemAll()
    {
        $(".select_all_req").prop('checked',false);
        callback_action();
    }
</script>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="allReqModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Список заявок на вантаж</h4>
            </div>
            <div class="modal-body" id="allReqText">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ОК</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on( "click", ".see_requests", function(){
        var req = $(this).data("requests");
        $("#allReqText").html(req);
        $("#allReqModal").modal();
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="user_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір користувача</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('user','widget','ua',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_user", function () {
                            $("#user_modal").modal();
                            callback_action_user();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_user_to_transport", function () {
                                set_all_ids_user();
                                for (var i = 0; i < window.ids_user.length; i++) {
                                    window.user_id_tr = window.ids_user[i];
                                    $("[name=userID]").val(window.ids_user[i]);
                                    $("#user_name").html(window.names_user[i]);
                                    $("#user_name").html("<span id=\"close_span_user\">"+window.names_user[i]+"&nbsp;<span style=\"color:red\">x</span></span>");

                                }
                        });',
                        'user_type'=>'cargo'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_user_to_transport" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on( "click", "#close_span_user", function(){
        $("[name=userID]").val('');
        $(this).html('');
        $('#user_name').html('');
    });
</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="company_transport_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Вибір компанії</h4>
            </div>
            <div class="modal-body">
                <?php Component::cms('company','widget','ru',
                    array(
                        'callback_function_view'=>
                        '$(document).on("click", "#select_company", function () {
                            $("#company_transport_modal").modal();
                            callback_action_company();
                        });',
                        'callback_function_btn'=>
                        '$(document).on("click", "#select_company_to_transport", function () {
                                set_all_ids_company();
                                for (var i = 0; i < window.ids_company.length; i++) {
                                    $("[name=userCompanyID]").val(window.ids_company[i]);
                                    $("#company_name").html("<span id=\"close_span_company\">"+window.names_company[i]+"&nbsp;<span style=\"color:red\">x</span></span>");
                                }
                        });',
                        'company_type'=>'cargo'
                    )
                );?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                <button type="button" id="select_company_to_transport" data-dismiss="modal" class="btn btn-success">Вибрати</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on( "click", "#close_span_company", function(){
        $("[name=userCompanyID]").val('');
        $(this).html('');
        $('#company_name').html('');
    });
</script>

