<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading" style="background: #eff0f4; padding: 0px 15px; border: none;">
                    <?=$base_title?>
                    <i style="color: #00c400;">Модуль работает</i>
                </header>
                <div class="panel-body" style="background: #eff0f4;">
                    <?
                    if (isset($change_ok))
                    {
                        ?>
                        <div class="alert alert-success fade in">
                            Група администраторов успешно добавлена
                        </div>
                    <?
                    }
                    ?>
                    <form class="form-horizontal adminex-form" method="post" enctype="multipart/form-data" style="background: white">



                        <section class="panel" style="margin-bottom: 30px;">
                            <header class="panel-heading custom-tab dark-tab">
                                <ul class="nav nav-tabs">
                                    <?
                                    foreach($langs as $l)
                                    {
                                        ?>
                                        <li class="<?=($l['def'])?'active':''?>">
                                            <a href="#lang_<?=$l['pref']?>" data-toggle="tab"><?=$l['title']?></a>
                                        </li>
                                    <?
                                    }
                                    ?>
                                </ul>
                            </header>
                            <div class="panel-body">
                                <div class="tab-content">

                                    <?
                                    foreach($langs as $l)
                                    {
                                        ?>
                                        <div class="tab-pane <?=($l['def']?'active':'')?>" id="lang_<?=$l['pref']?>">

                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-1 control-label">Название <?=strtoupper($l['pref'])?></label>
                                                <div class="col-sm-10">
                                                    <input name="title_<?=$l['pref']?>" type="text" class="form-control m-bot15" maxlength="150" placeholder="не более 150 символов">
                                                </div>
                                            </div>

                                        </div>
                                    <?
                                    }
                                    ?>


                                </div>
                            </div>
                        </section>


                        <button style="margin: 15px;" type="submit" name="go" class="btn btn-primary">Сохранить</button>

                    </form>

                </div>


        </div>

        </section>
    </div>
</div>
</div>




