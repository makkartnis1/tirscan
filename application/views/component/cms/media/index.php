<div class="mediafiles">
    <?
    if ($is_files == 1)
    {
        ?>
        <div class="btn btn-primary" id="setting_file_types_"><i class="fa fa-wrench"></i>   Установить типы файлов</div>
        <?
    }
    ?>
    <section class="panel" id="files_panel" style="border: 1px solid #424F63;border-top:none;">
        <header class="panel-heading custom-tab dark-tab">
            <ul class="nav nav-tabs file_tab_header">
                <?
                if ($is_files == 1)
                {
                    ?>
                    <div style="position: relative; top: 7px; height: 43px; border-radius: 0;" class="btn btn-primary" id="setting_file_types"><i style="margin-top: 8px;" class="fa fa-wrench"></i></div>
                    <?
                }
                ?>
            </ul>
        </header>
        <div class="panel-body">
            <div class="tab-content file_tab_content">

            </div>
        </div>
    </section>



</div>



<div aria-hidden="true" role="dialog" tabindex="-1" id="file_type_settings_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Типы медиафайлов для текущего раздела</h4>
            </div>
            <div class="modal-body">
                <?
                if ($is_files == 1)
                {
                    ?>
                    <div class="file_types">
                        <?
                        if (!empty($all_file_types))
                        {
                            foreach($all_file_types as $file_type)
                            {
                                ?>
                                <div class="one_file_type">
                                    <input type="checkbox" class="file_types_checkbox" value="<?=$file_type['id']?>"><span class="file_type_label"><?=$file_type['title']?></span>
                                </div>
                            <?
                            }
                        }
                        ?>
                    </div>
                <?
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="set_file_types_btn" data-dismiss="modal" class="btn btn-success">Применить</button>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" role="dialog" tabindex="-1" id="edit_file_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Редактирование информации о файле</h4>
            </div>
            <div class="modal-body">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2" id="edit_file_content"></td>
                    </tr>
                    <tr class="control_file_create">
                        <td>URL</td>
                        <td>
                            <input type="text" class="form-control" name="edit_file_url" maxlength="500" placeholder="внешняя ссылка на файл">
                        </td>
                    </tr>
                    <tr class="control_file_create">
                        <td>Код</td>
                        <td>
                            <textarea class="form-control" name="edit_file_code" placeholder="код видео/флеш обьект"></textarea>
                        </td>
                    </tr>
                    <?
                    foreach($langs as $l)
                    {
                        ?>
                        <tr>
                            <td>Название <?=$l['pref']?></td>
                            <td>
                                <input type="text" class="form-control edit_file_title" lang_id="<?=$l['id']?>" maxlength="500">
                            </td>
                        </tr>
                    <?
                    }
                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary control_btns"><a id="download_file" href="#">Скачать файл</a></button>
                <button type="button" class="btn btn-primary control_btns"><a id="open_file" target="_blank" href="#">Открыть файл</a></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="edit_file_final" data-dismiss="modal" class="btn btn-success">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="create_file_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Создание файла</h4>
            </div>
            <div class="modal-body">
                    <table style="width: 100%">
                        <tr>
                            <td>Тип файла</td>
                            <td>
                                <div class="type_file"><input type="radio" name="type_create" value="1" checked> Изображение</div>
                                <div class="type_file"><input type="radio" name="type_create" value="2"> Видео</div>
                                <div class="type_file"><input type="radio" name="type_create" value="3"> Документ</div>
                            </td>
                        </tr>
                        <tr>
                            <td>URL</td>
                            <td>
                                <input type="text" class="form-control" name="url" maxlength="500" placeholder="внешняя ссылка на файл">
                            </td>
                        </tr>
                        <tr>
                            <td>Код</td>
                            <td>
                                <textarea class="form-control" name="code" placeholder="код видео/флеш обьект"></textarea>
                            </td>
                        </tr>
                        <?
                            foreach($langs as $l)
                            {
                                ?>
                                <tr>
                                    <td>Название <?=$l['pref']?></td>
                                    <td>
                                        <input type="text" class="form-control add_file_title" lang_id="<?=$l['id']?>" maxlength="500">
                                    </td>
                                </tr>
                                <?
                            }
                        ?>
                    </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="create_file_final" data-dismiss="modal" class="btn btn-success">Создать файл</button>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" role="dialog" tabindex="-1" id="edit_album_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Редактирование альбома</h4>
            </div>
            <div class="modal-body">
                <table style="width: 100%">
                    <?
                    foreach($langs as $l)
                    {
                        ?>
                        <tr>
                            <td>Название <?=$l['pref']?></td>
                            <td>
                                <input type="text" class="form-control edit_album_title" lang_id="<?=$l['id']?>" maxlength="500">
                            </td>
                        </tr>
                    <?
                    }
                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="edit_album_final" data-dismiss="modal" class="btn btn-success">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="create_album_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Создание альбома</h4>
            </div>
            <div class="modal-body">
                <table style="width: 100%">
                    <?
                    foreach($langs as $l)
                    {
                        ?>
                        <tr>
                            <td>Название <?=$l['pref']?></td>
                            <td>
                                <input type="text" class="form-control add_album_title" lang_id="<?=$l['id']?>" maxlength="500">
                            </td>
                        </tr>
                    <?
                    }
                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="create_album_final" data-dismiss="modal" class="btn btn-success">Создать альбом</button>
            </div>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="add_file_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Добавление файлов</h4>
            </div>
            <div class="modal-body">
                <form id="form_files" action="" method="post" enctype="multipart/form-data">
                    <div style="margin-bottom: 10px">
                        <span style="margin-right: 10px">Типы файлов</span>
                        <div class="type_file"><input type="radio" name="type" value="1" checked> Изображения</div>
                        <div class="type_file"><input type="radio" name="type" value="2"> Видео</div>
                        <div class="type_file"><input type="radio" name="type" value="3"> Документы</div>
                    </div>
                    <a href="javascript:;" class="btn btn-info" style="margin-top: 0; position:relative; cursor: pointer;"><span id="label_status_files">Выбрать файлы</span>
                        <input type="file" name="files[]" id="files" multiple="" style='cursor:pointer !important;position:absolute;z-index:2;top:0;left:0;width:100%;height:32px;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' onchange="change_file_input()">
                        <input type="hidden" name="file_type_id">
                        <input type="hidden" name="album_id">
                        <input type="hidden" name="table_name" value="<?=$table_name?>">
                        <input type="hidden" name="col_name" value="<?=$col_name?>">
                        <input type="hidden" name="<?=($id)?'id':'session_id'?>" value="<?=($id)?$id:$session_id?>">
                    </a>
                    <div class="btn btn-danger" id="cancel_load_file" style="display: none; position: relative;">Отмена</div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="load_files" data-dismiss="modal" class="btn btn-success">Загрузить файлы</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){

        <?
        if ($session_id)
        {
            ?>
                $("[name=session_id]").val('<?=$session_id?>');
            <?
        }
        ?>

        window.media_file_types = [];
        window.media_album_types = [];
        <?
            if ($is_files == 1)
            {
                ?>
                  render_file_types();
                <?
            }
            if ($is_albums == 1)
            {
                ?>
                    set_alboms_block();
                <?
            }
        ?>
        $(".file_types_checkbox").iCheck({
            checkboxClass: 'icheckbox_flat-grey',
            radioClass: 'iradio_flat-grey'
        });
        $(".type_file").iCheck({
            checkboxClass: 'icheckbox_flat-grey',
            radioClass: 'iradio_flat-grey'
        });
    });

    $(document).on( "click", ".delete_album", function(){
        var album_id = $(this).attr('album_id');
        $.post("/component_cms/ru/media/delete_album/",{album_id:album_id},callback_delete_album,"json");
    });

    function callback_delete_album()
    {
        render_albums();
    }

    $(document).on( "click", ".edit_album", function(){
        var album_id = $(this).attr('album_id');
        $("#edit_album_final").attr('album_id',album_id);
        $.post("/component_cms/ru/media/get_album_full_info/",{album_id:album_id},callback_album_full_info,"json");
    });

    function callback_album_full_info(data)
    {
        if (data)
        {
            for(var i in data.locales)
            {
                $(".edit_album_title[lang_id="+i+"]").val(data.locales[i].title);
            }

            $("#edit_album_modal").modal();
        }
    }

    function set_full_info_start_render()
    {

    }

    function set_full_info_end_render()
    {

    }

    $(document).on( "click", ".view_full_info", function(){
        var file_id = $(this).attr('file_id');
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        $("#edit_file_final").attr('file_id',file_id);
        $("#edit_file_final").attr('file_type_id',file_type_id);
        $("#edit_file_final").attr('album_id',album_id);
        $("[name=edit_file_url]").val('');
        $("[name=edit_file_code]").val('');
        set_full_info_start_render();
        $.post("/component_cms/ru/media/get_file_full_info/",{file_id:file_id},callback_file_full_info,"json");
    });

    function callback_file_full_info(data)
    {
        if (data)
        {
            if (data.isset_file == '1')
            {
                $(".control_file_create").hide();


                if (data.type == '1')
                {
                    var rejum = '';
                    if (data.size_types)
                    {
                        var size_types = data.size_types;
                        var sp = size_types.toString().split('.');
                        if (in_array('l',sp)) rejum = '_l';
                        if (in_array('m',sp)) rejum = '_m';
                    }

                    var src = '/uploads/files/'+data.year+'/'+data.month+'/'+data.day+'/'+data.id+'/'+data.id+rejum+'.'+data.ext;
                    $("#edit_file_content").html('<img style="width: 100%;" src="'+src+'">');
                }
                var original_src = '/uploads/files/'+data.year+'/'+data.month+'/'+data.day+'/'+data.id+'/'+data.id+'.'+data.ext;
                $("#download_file").attr('href','/component_cms/ru/media/send_file/?file=<?=$_SERVER['DOCUMENT_ROOT']?>'+original_src);
                $("#open_file").attr('href',original_src);
            }
            else
            {
                $("#download_file").hide();
                $("#open_file").hide();
                $(".control_file_create").show();
                $("[name=edit_file_url]").val(data.url);
                $("[name=edit_file_code]").val(data.code);
                if (data.url)
                {
                    if (data.type == '1')
                    {
                        $("#edit_file_content").html('<img style="width: 100%;" src="'+data.url+'">');
                    }
                }
                else
                {
                    $("#edit_file_content").html(data.code);
                }
            }


            for(var i in data.locales)
            {
               $(".edit_file_title[lang_id="+i+"]").val(data.locales[i].title);
            }

            $("#edit_file_modal").modal();

            set_full_info_end_render();

        }
    }


    function set_edit_file_start_render()
    {

    }

    function set_edit_file_end_render()
    {

    }

    $(document).on( "click", "#edit_album_final", function(){
        var album_id = $(this).attr('album_id');
        var titles = {};
        $(".edit_album_title").each(function(){
            var lang_id = $(this).attr('lang_id');
            var val = $(this).val();
            titles[lang_id] = val;
        });

        var data = {
            titles:titles,
            album_id:album_id
        };
        $.post("/component_cms/ru/media/edit_album/",data,callback_edit_album,"json");
    });

    function callback_edit_album()
    {
        render_albums();
    }

    $(document).on( "click", "#edit_file_final", function(){
        var file_id = $(this).attr('file_id');
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var url = $("[name=edit_file_url]").val();
        var code = $("[name=edit_file_code]").val();
        var titles = {};
        $(".edit_file_title").each(function(){
            var lang_id = $(this).attr('lang_id');
            var val = $(this).val();
            titles[lang_id] = val;
        });

        var data = {
            file_id:file_id,
            titles:titles,
            url:url,
            code:code,
            file_type_id:file_type_id,
            album_id:album_id
        };
        set_edit_file_start_render();
        $.post("/component_cms/ru/media/edit_file/",data,callback_edit_file,"json");
    });


    function callback_edit_file(data)
    {
        if (data)
        {
            set_edit_file_end_render();
            if (data.file_type_id != '') render_files(data.file_type_id);
            if (data.album_id != '') render_album(data.album_id);
        }
    }


    function set_create_album_start_render()
    {

    }

    function set_create_album_end_render()
    {

    }

    $(document).on( "click", "#create_album_final", function(){
        var titles = {};
        $(".add_album_title").each(function(){
            var lang_id = $(this).attr('lang_id');
            var val = $(this).val();
            titles[lang_id] = val;
        });

        var data = {
            titles:titles,
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>"
        };

        <?
        if ($id)
        {
            ?>
              data.id = "<?=$id?>";
            <?
        }
        else
        {
            ?>
            data.session_id = "<?=$session_id?>";
            <?
        }
        ?>

        set_create_album_start_render();
        $.post("/component_cms/ru/media/create_album/",data,callback_create_album,"json");
    });

    function callback_create_album()
    {
        render_albums();
    }

    function set_create_file_start_render()
    {

    }

    function set_create_file_end_render()
    {

    }

    $(document).on( "click", "#create_file_final", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var type = $("[name=type_create]:checked").val();
        var url = $("[name=url]").val();
        var code = $("[name=code]").val();
        var titles = {};
        $(".add_file_title").each(function(){
            var lang_id = $(this).attr('lang_id');
            var val = $(this).val();
            titles[lang_id] = val;
        });


        var data = {
            file_type_id:file_type_id,
            album_id:album_id,
            type:type,
            url:url,
            code:code,
            titles:titles,
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>"
        };

        <?
        if ($id)
        {
            ?>
                data.id = "<?=$id?>";
            <?
        }
        else
        {
            ?>
                data.session_id = "<?=$session_id?>";
            <?
        }
        ?>

        set_create_file_start_render();
        $.post("/component_cms/ru/media/add_files/",data,callback_create_file,"json");
        $("[name=url]").val('');
        $("[name=code]").val('');
        $(".add_file_title").val('');

    });

    function callback_create_file(data)
    {
        if (data)
        {
            if (data.file_type_id != '') render_files(data.file_type_id);
            if (data.album_id != '') render_album(data.album_id);
        }
    }

    $(document).on( "click", ".create_file", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        $("#create_file_final").attr('file_type_id',file_type_id);
        $("#create_file_final").attr('album_id',album_id);
        $("#create_file_modal").modal();
    });

    $(document).on( "click", "#create_album", function(){
        $("#create_album_modal").modal();
    });

    $(document).on( "click", "#setting_file_types", function(){
        $("#file_type_settings_modal").modal();
    });

    $(document).on( "click", "#setting_file_types_", function(){
        $("#file_type_settings_modal").modal();
    });

    $(document).on( "click", "#set_file_types_btn", function(){
        set_file_types();
    });

    $(document).on( "click", ".add_files", function(){
        var form_name = $(this).attr('form_name');
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        $("#form_files").attr('name',form_name);
        $("[name=file_type_id]").val(file_type_id);
        $("[name=album_id]").val(album_id);
        $("#add_file_modal").modal();
    });

    $(document).on( "click", "#load_files", function(){
        var form_name = $("#form_files").attr('name');
        var album_id = $("[name=album_id]").val();
        var file_type_id = $("[name=file_type_id]").val();
        send_files(form_name,album_id,file_type_id);
    });

    function change_file_input()
    {
        $("#label_status_files").html('Файлы выбраны');
        $("#cancel_load_file").show();
    }

    $(document).on( "click", "#cancel_load_file", function(){
        $("#files").val('');
        $("#label_status_files").html('Выбрать файлы');
        $(this).hide();
    });

    $(document).on( "click", ".one_file_delete", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var file_id = $(this).attr('file_id');
        var file_ids = [];
        file_ids.push(file_id);
        if (file_type_id) files_start_render(file_type_id);
        if (album_id) album_start_render(album_id);
        $.post("/component_cms/ru/media/delete_files/",{file_ids:file_ids,file_type_id:file_type_id,album_id:album_id},callback_delete_files,"json");
    });

    function callback_delete_files(data)
    {
        if (data.file_type_id != '') render_files(data.file_type_id);
        if (data.album_id != '') render_album(data.album_id);
    }

    $(document).on( "click", ".one_file_set_main", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var file_id = $(this).attr('file_id');
        if (file_type_id) files_start_render(file_type_id);
        if (album_id) album_start_render(album_id);
        var data = {
            file_id:file_id,
            file_type_id:file_type_id,
            album_id:album_id,
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>"
        };

        <?
        if ($id)
        {
            ?>
            data.id = "<?=$id?>";
            <?
        }
        else
        {
            ?>
            data.session_id = "<?=$session_id?>";
            <?
        }
        ?>

        $.post("/component_cms/ru/media/add_main_file/",data,callback_set_main_file,"json");
    });

    function callback_set_main_file(data)
    {
        if (data.file_type_id != '') render_files(data.file_type_id);
        if (data.album_id != '') render_album(data.album_id);
    }

    $(document).on( "click", ".one_file_up", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var file_id = $(this).attr('file_id');
        if (file_type_id) files_start_render(file_type_id);
        if (album_id) album_start_render(album_id);
        $.post("/component_cms/ru/media/up_srt_file/",{file_id:file_id,file_type_id:file_type_id,album_id:album_id},callback_srt_file,"json");
    });

    $(document).on( "click", ".one_file_down", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var file_id = $(this).attr('file_id');
        if (file_type_id) files_start_render(file_type_id);
        if (album_id) album_start_render(album_id);
        $.post("/component_cms/ru/media/down_srt_file/",{file_id:file_id,file_type_id:file_type_id,album_id:album_id},callback_srt_file,"json");
    });


    $(document).on( "click", ".up_album", function(){
        var album_id = $(this).attr('album_id');
        $.post("/component_cms/ru/media/up_srt_album/",{album_id:album_id},callback_srt_album,"json");
    });

    $(document).on( "click", ".down_album", function(){
        var album_id = $(this).attr('album_id');
        $.post("/component_cms/ru/media/down_srt_album/",{album_id:album_id},callback_srt_album,"json");
    });

    function callback_srt_album()
    {
        render_albums();
    }

    function callback_srt_file(data)
    {
        if (data.file_type_id != '') render_files(data.file_type_id);
        if (data.album_id != '') render_album(data.album_id);
    }

    $(document).on( "click", ".check_all", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var selector = (file_type_id)?$(".one_file_checkbox[file_type_id="+file_type_id+"]"):$(".one_file_checkbox[album_id="+album_id+"]");
        selector.prop('checked',true);
    });

    $(document).on( "click", ".uncheck_all", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var selector = (file_type_id)?$(".one_file_checkbox[file_type_id="+file_type_id+"]"):$(".one_file_checkbox[album_id="+album_id+"]");
        selector.prop('checked',false);
    });

    $(document).on( "click", ".multi_delete", function(){
        var file_type_id = $(this).attr('file_type_id');
        var album_id = $(this).attr('album_id');
        var selector = (file_type_id)?$(".one_file_checkbox[file_type_id="+file_type_id+"]"):$(".one_file_checkbox[album_id="+album_id+"]");
        var file_ids = [];
        selector.each(function(){
            if ($(this).prop('checked'))
            {
                file_ids.push($(this).attr('file_id'));
            }
        });


        if (file_ids.length > 0)
        {
            if (file_type_id) files_start_render(file_type_id);
            if (album_id) album_start_render(album_id);
            $.post("/component_cms/ru/media/delete_files/",{file_ids:file_ids,file_type_id:file_type_id,album_id:album_id},callback_delete_files,"json");

        }
    });

    function set_file_types_start_render()
    {

    }

    function set_file_types_end_render()
    {

    }

    function set_file_types()
    {
        var file_type_ids = {};
        $(".file_types_checkbox").each(function(){
            if ($(this).prop("checked"))
            {
                file_type_ids[$(this).val()] = '1';
            }
            else
            {
                file_type_ids[$(this).val()] = '0';
            }
        });


        set_file_types_start_render();
        $.post("/component_cms/ru/media/set_file_types/",{file_types:file_type_ids,plugin_id:'<?=$plugin_id?>'},callback_set_file_types,"json");
    }

    function callback_set_file_types()
    {
        render_file_types();
    }


    function render_file_types()
    {
        set_file_types_start_render();
        var data = {
            plugin_id:"<?=$plugin_id?>",
            default_site_lng_id:"<?=$default_site_lng_id?>"
        };
        $.post("/component_cms/ru/media/get_file_types/",data,callback_render_file_types,"json");
    }

    function callback_render_file_types(data)
    {
        var files_header = $(".file_tab_header");
        var files_content = $(".file_tab_content");
        window.media_file_types = [];
        if (data)
        {
            files_header.children(".file_header").remove();
            files_content.children(".file_content").remove();
            $(".file_types_checkbox").iCheck('uncheck');
            for(var i in data)
            {
               file_type_header = '<li class="file_header"><a href="#file_type_'+data[i].id+'" data-toggle="tab">'+data[i].title+'</a></li>';
                $(".file_tab_header").append(file_type_header);
               file_type_content = '<div class="tab-pane file_content" id="file_type_'+data[i].id+'">';
               file_type_content += build_buttons(data[i].id,'');
               file_type_content += '</div>';
               $(".file_types_checkbox[value="+data[i].id+"]").iCheck('check');
               $(".file_tab_content").append(file_type_content);
               window.media_file_types.push('1');
               render_files(data[i].id);
            }
            files_header.children(".file_header:first").addClass("active");
            files_content.children(".file_content:first").addClass("active");
        }
        else
        {
            files_header.children(".file_header").remove();
            files_content.children(".file_content").remove();
        }
        if (window.media_file_types.length == 0)
        {
            $("#files_panel").hide();
            $("#setting_file_types_").show();
        }
        else
        {
            $("#files_panel").show();
            $("#setting_file_types_").hide();
        }

        set_file_types_end_render();

    }


    function set_alboms_block()
    {
        var albums_block_header = '<li class="albums_block_header"><a href="#albums_block_content" data-toggle="tab">Альбомы</a></li>';
        $(".file_tab_header").append(albums_block_header);
        var albums_block_content = '<div class="tab-pane" id="albums_block_content"><div class="panel-group albums_block_content" id="accordion"></div></div>';
        $(".file_tab_content").append(albums_block_content);
        render_albums();
    }

    function albums_start_render()
    {

    }

    function albums_end_render()
    {

    }

    function render_albums()
    {
        var data = {
            default_site_lng_id: "<?=$default_site_lng_id?>",
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>"
        };

        <?
        if ($id)
        {
            ?>
                data.id = "<?=$id?>";
            <?
        }
        else
        {
            ?>
                data.session_id = "<?=$session_id?>";
            <?
        }
        ?>

        $.post("/component_cms/ru/media/get_albums/",data,callback_render_albums,"json");
        albums_start_render();
    }

    function callback_render_albums(data)
    {
        var albums_block_content = $(".albums_block_content");

        if (data)
        {
            albums_block_content.html('');
            for(var i in data)
            {
                var album = '<div class="panel">'+
                                '<div class="panel-heading panel-heading-album dark">'+
                                    '<h4 class="panel-title">'+
                                        '<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse'+data[i].id+'">'+data[i].title +
                                        '</a>'+
                                        '<div class="btn btn-danger delete_album" album_id="'+data[i].id+'"><i class="fa fa-times"></i></div>' +
                                        '<div class="btn btn-info down_album" album_id="'+data[i].id+'"><i class="fa fa-arrow-down"></i></div>' +
                                        '<div class="btn btn-info up_album" album_id="'+data[i].id+'"><i class="fa fa-arrow-up"></i></div>' +
                                        '<div class="btn btn-default edit_album" album_id="'+data[i].id+'"><i class="fa fa-pencil"></i></div>' +
                                    '</h4>'+
                                '</div>'+
                                '<div id="collapse'+data[i].id+'" class="panel-collapse collapse">'+
                                    '<div class="panel-body one_album" album_id="'+data[i].id+'"></div>'+
                                '</div>'+
                           '</div>';
                albums_block_content.append(album);
                render_album(data[i].id);
            }
        }
        var add_album_btn = '<div class="btn btn-link" id="create_album">Создать альбом</div>';
        albums_block_content.append(add_album_btn);
        albums_end_render();

    }


    function album_start_render(album_id)
    {

    }

    function album_end_render(album_id)
    {

    }



    function render_album(album_id)
    {
        var data = {
            default_site_lng_id: "<?=$default_site_lng_id?>",
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>",
            album_id: album_id
        };

        <?
        if ($id)
        {
            ?>
        data.id = "<?=$id?>";
        <?
    }
    else
    {
        ?>
        data.session_id = "<?=$session_id?>";
        <?
    }
    ?>

        $.post("/component_cms/ru/media/get_files_by_album_id/",data,callback_render_album,"json");
        album_start_render(album_id);
    }

    function callback_render_album(data)
    {
        var files = $(".one_album[album_id="+data.album_id+"]");
        files.html('');

        if (data.files)
        {
            if (data.files)
            {
                for(var i in data.files)
                {
                    var one_file = data.files[i];
                    var file = build_file(one_file.id,one_file.title,one_file.url,one_file.year,one_file.month,one_file.day,one_file.ext,one_file.is_main,one_file.type,one_file.size_types,one_file.isset_file,'',data.album_id);
                    files.append(file);
                }
            }

            $(".one_file_checkbox").iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });

            var album_buttons = '<div class="btn btn-danger multi_delete" file_type_id="" album_id="'+data.album_id+'"><i class="fa fa-times"></i></div><input type="checkbox" class="select_all_files" file_type_id="" album_id="'+data.album_id+'">';
            files.append(album_buttons);
        }

        var buttons = build_buttons('',data.album_id);
        files.append(buttons);

        $(".select_all_files").iCheck({
            checkboxClass: 'icheckbox_flat-grey icheck_all_files'
        });
        $('.select_all_files').on('ifChecked', function(event){
            var file_type_id = $(this).attr('file_type_id');
            var album_id = $(this).attr('album_id');
            if (file_type_id) $('.one_file_checkbox[file_type_id='+file_type_id+']').iCheck('check');
            if (album_id) $('.one_file_checkbox[album_id='+album_id+']').iCheck('check');
        });
        $('.select_all_files').on('ifUnchecked', function(event){
            var file_type_id = $(this).attr('file_type_id');
            var album_id = $(this).attr('album_id');
            if (file_type_id) $('.one_file_checkbox[file_type_id='+file_type_id+']').iCheck('uncheck');
            if (album_id) $('.one_file_checkbox[album_id='+album_id+']').iCheck('uncheck');
        });

        album_end_render(data.album_id);
    }


    function files_start_render(file_type_id)
    {

    }

    function files_end_render(file_type_id)
    {

    }

    function render_files(file_type_id)
    {

        var data = {
            default_site_lng_id: "<?=$default_site_lng_id?>",
            table_name: "<?=$table_name?>",
            col_name: "<?=$col_name?>",
            file_type_id: file_type_id
        };

        <?
        if ($id)
        {
            ?>
            data.id = "<?=$id?>";
            <?
        }
        else
        {
            ?>
            data.session_id = "<?=$session_id?>";
            <?
        }
        ?>

        $.post("/component_cms/ru/media/get_files/",data,callback_render_files,"json");
        files_start_render(file_type_id);
    }

    function callback_render_files(data)
    {
        var files = $("#file_type_"+data.file_type_id);
        files.html('');
            var file = '';
            if (data.files)
            {
                for(var i in data.files)
                {
                    var one_file = data.files[i];
                    file = build_file(one_file.id,one_file.title,one_file.url,one_file.year,one_file.month,one_file.day,one_file.ext,one_file.is_main,one_file.type,one_file.size_types,one_file.isset_file,data.file_type_id,'');
                    files.append(file);
                }

                var multi_buttons = '<div class="btn btn-danger multi_delete" file_type_id="'+data.file_type_id+'" album_id=""><i class="fa fa-times"></i></div><input type="checkbox" class="select_all_files" file_type_id="'+data.file_type_id+'">';
                files.append(multi_buttons);

                $(".select_all_files").iCheck({
                    checkboxClass: 'icheckbox_flat-grey icheck_all_files'
                });
                $('.select_all_files').on('ifChecked', function(event){
                    var file_type_id = $(this).attr('file_type_id');
                    $('.one_file_checkbox[file_type_id='+file_type_id+']').iCheck('check');
                });
                $('.select_all_files').on('ifUnchecked', function(event){
                    var file_type_id = $(this).attr('file_type_id');
                    $('.one_file_checkbox[file_type_id='+file_type_id+']').iCheck('uncheck');
                });
            }

            var buttons = build_buttons(data.file_type_id,'');
            files.append(buttons);
        $(".one_file_checkbox").iCheck({
            checkboxClass: 'icheckbox_flat-grey',
            radioClass: 'iradio_flat-grey'
        });
        files_end_render(data.file_type_id);

    }



    function send_files(form_name,album_id,file_type_id)
    {

        if (file_type_id) files_start_render(file_type_id);
        if (album_id) album_start_render(album_id);
        var formData = new FormData(document.forms[form_name]);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/component_cms/ru/media/add_files/");
        xhr.send(formData);

        xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if(this.status == 200) {

                    if (file_type_id) render_files(file_type_id);
                    if (album_id) render_album(album_id);
                    $("#files").val('');
                    $("#label_status_files").html('Выбрать файлы');
                    $("#cancel_load_file").hide();

                }
            }
        };
    }


    function build_file(file_id,file_title,file_url,file_year,file_month,file_day,file_ext,file_is_main,file_type,size_types,isset_file,file_type_id,album_id)
    {
        var file = '<div class="one_file" file_id="'+file_id+'">' +
            '<div class="file_checkbox"><input class="one_file_checkbox" type="checkbox" file_type_id="'+file_type_id+'" album_id="'+album_id+'" file_id="'+file_id+'"></div>' +
            '<div class="btn btn-danger one_file_delete" file_type_id="'+file_type_id+'" album_id="'+album_id+'" file_id="'+file_id+'"><i class="fa fa-times"></i></div>';
        if (file_is_main == '1')
        {
            file += '<div class="label label-primary one_file_main">главное</div>';
        }
        else
        {
            file += '<div class="btn btn-primary btn-xs one_file_set_main" file_type_id="'+file_type_id+'" album_id="'+album_id+'" file_id="'+file_id+'">сделать главным</div>';
        }

        file += '<div class="block_img">';
        if (file_type == '1')
        {
           var rejum = '';
           if (isset_file == '1' && size_types)
           {
               var sp = size_types.toString().split('.');
               if (in_array('l',sp)) rejum = '_l';
               if (in_array('m',sp)) rejum = '_m';
               if (in_array('s',sp)) rejum = '_s';
           }

           var file_src = '/uploads/files/'+file_year+'/'+file_month+'/'+file_day+'/'+file_id+'/'+file_id+rejum+'.'+file_ext;

           var src = (file_url)?file_url:file_src;
           file += '<img class="one_file_img" file_id="'+file_id+'" src="'+src+'">';
        }
        if (file_type == '2')
        {
            file += '<i class="fa fa-play-circle files_icons"></i>';
        }
        if (file_type == '3')
        {
            file += '<i class="fa fa-file-text files_icons"></i>';
        }

        file += '<div class="btn btn-info btn-xs view_full_info" file_id="'+file_id+'" file_type_id="'+file_type_id+'" album_id="'+album_id+'"><i class="fa fa-eye"></i></div>';

        file += '</div>';

        var title = (file_title)?file_title:file_id;

        var title = (file_ext)?title+'.'+file_ext:title;

        file += '<div class="one_file_title"><span>'+title+'</span></div>' +
            '<div class="btn btn-info one_file_up" file_type_id="'+file_type_id+'" album_id="'+album_id+'" file_id="'+file_id+'"><i class="fa fa-arrow-up"></i></div>' +
            '<div class="btn btn-info one_file_down" file_type_id="'+file_type_id+'" album_id="'+album_id+'" file_id="'+file_id+'"><i class="fa fa-arrow-down"></i></div>' +
            '</div>';

        return file;
    }

    function build_buttons(file_type_id,album_id)
    {
        var form_name = 'form_'+(file_type_id)?'files_'+file_type_id:'album_'+album_id;
        var buttons = '<div class="media_btn_block"><div class="create_file media_btn" file_type_id="'+file_type_id+'" album_id="'+album_id+'"><i class="fa fa-file"></i><br>Создать</div>' +
                    '<div class="add_files media_btn" file_type_id="'+file_type_id+'" album_id="'+album_id+'" form_name="'+form_name+'"><i class="fa fa-download"></i><br>Добавить</div></div>';

        return buttons;
    }


    function in_array(value, array)
    {
        for(var i = 0; i < array.length; i++)
        {
            if(array[i] == value) return true;
        }
        return false;
    }





</script>
<style type="text/css">
    .one_file
    {
        width: 170px;
        height: 170px;
        position: relative;
        margin: 5px;
        float: left;
    }

    .block_img
    {
        width: 100%;
        height: 100%;
        overflow: hidden;
        text-align: center;
    }
    .one_file_img
    {
        height: 100%;
    }
    .one_file_delete
    {
        position: absolute;
        right: 0;
        padding: 1px 5px;
        opacity: .65;

    }
    .file_checkbox
    {
        position: absolute;
        top: 2px;
        left: 2px;
        margin: 0;
    }

    .one_file_set_main
    {
        /*display: none;*/
    }
    .one_file_set_main, .one_file_main
    {
        position: absolute;
        top: 27px;
        left: 2px;
        opacity: .65;
        z-index: 1;
    }

    .one_file_up
    {
        position: absolute;
        top: 0;
        right: 85px;
        padding: 1px 5px;
        /*display: none;*/
        opacity: .65;
    }

    .one_file_down
    {
        position: absolute;
        top: 0;
        right: 59px;
        padding: 1px 5px;
        opacity: .65;
        /*display: none;*/

    }
    .files_icons
    {
        position: relative;
        left: -52px;
        top: 59px;
        font-size: 52px;
    }
    .view_full_info
    {
        position: absolute;
        top: 61px;
        font-size: 30px;
        left: 61px;
        padding: 0 8px;
        border-radius: 25px;
        opacity: .65;
        /*display: none;*/
    }

    .one_file_title
    {
        position: absolute;
        bottom: 0;
        background-color: black;
        color: #fff;
        width: 100%;
        text-align: center;
        opacity: .45;
        font-size: 11px;
    }
    .one_file_title span
    {
        opacity: 1;
    }
    .icheckbox_flat-grey, .iradio_flat-grey
    {
        float: left;
    }
    .iradio_flat-grey
    {
        margin-right: 5px;
    }
    .one_file_type
    {
        margin: 10px 0;
    }
    .file_type_label
    {
        margin-left: 10px;
    }
    #setting_file_types
    {
        float: right;
    }
    #setting_file_types_
    {
        display: none;
    }
    #files_panel
    {
        display: none;
    }
    .type_file
    {
        margin-right: 10px;
        display: inline-block;
    }
    .icheck_all_files
    {
        position: absolute;
        right: 29px;
        top: -2px;
    }
    .multi_delete
    {
        padding: 0px 5px;
        float: right;
        position: absolute;
        top: -3px;
        right: 0;
    }
    #create_file_modal table, #edit_file_modal table
    {
        width: 100%;
    }
    #create_file_modal table td, #edit_file_modal table td, #create_album_modal table td, #edit_album_modal table td
    {
        padding: 5px;
    }
    #create_file_modal table tr td:first-child, #edit_file_modal table tr td:first-child
    {
        width: 100px;
        text-align: right;
    }
    .media_btn_block
    {
        float: left;
    }

    .media_btn
    {
        border: 1px dashed;
        padding: 10px;
        border-radius: 13px;
        margin: 5px;
        text-align: center;
        cursor: pointer;
        float: left;
        width: 90px;
    }
    .media_btn .fa
    {
        font-size: 40px;
    }
    .mediafiles
    {
        width: 100%;
    }
    .file_tab_header .active a
    {
        padding: 12px !important;
        position: relative;
        top: 8px;

    }
    .panel-heading-album
    {
        padding: 10px 15px !important;
    }
    .up_album,.down_album,.delete_album
    {
        float: right;
        margin: 0 2px;
    }
    .edit_album
    {
        margin-left: 3px;
    }
    .file_content
    {
        padding-top: 20px;
        position: relative;
    }
    .one_album
    {
        position: relative;
        border: none !important;
        padding-top: 20px;
    }
    .mediafiles .panel-collapse
    {
        padding-top: 10px;
    }
    #download_file, #open_file
    {
        color: white;
        text-decoration: none;
    }
    .control_btns
    {
        float:left;
    }
</style>