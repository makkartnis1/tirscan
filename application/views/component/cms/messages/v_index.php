<?
    if ($checkDialog)
    {
        ?>
        <style type="text/css">
            input[type=search]
            {
                display: none;
            }
            .dataTables_length
            {
                margin-bottom: 10px;
            }
            a
            {
                color:white;
            }
            .table_data tr:first-child th
            {
                border-bottom:1px solid #DDDDDD !important;
            }
            #close_span_company
            {
                cursor: pointer;
            }
            #close_span_user
            {
                cursor: pointer;
            }
            .form-group
            {
                margin-bottom: 0;
                margin-top: 0;
                padding-bottom: 0;
                padding-top: 0;
            }
        </style>
        <!-- DataTables, TableTools and Editor CSS -->
        <link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/media/css/jquery.dataTables.css"/>
        <link rel="stylesheet" type="text/css" href="/public/cms/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.css"/>

        <!-- jQuery, DataTables, TableTools and Editor Javascript -->
        <script type="text/javascript" src="/public/cms/vendor/datatables/media/js/jquery.dataTables.js"></script>

        <div class="wrapper">
            <div class="row">

                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <?=$base_title?><?=($theme)?' - '.$theme:' не вказана'?>
                        </header>

                        <div class="panel-body" style="padding: 0 15px">
                            <div class="adv-table">


                                <table style="width:100% !important;" class="display table table-bordered table-striped table_data" id="table_transport_req">
                                    <thead>
                                    <tr>
                                        <th style="width: 25px">№</th>
                                        <th>Повідомлення</th>
                                        <th>Дії</th>
                                    </tr>
                                    </thead>

                                </table>

                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $(function(){


                window.table = $('#table_transport_req').DataTable( {
                    ajax: {
                        url: "/component_cms/<?=$lang?>/messages/db/",
                        type: "POST",
                        data : {
                            id: '<?=$id?>'
                        }
                    },
                    drawCallback: function() {
                        $(".select_all_req").prop('checked',false);
                    },
                    deferRender : true,
                    serverSide: true,
                    processing: true,
                    columns: [
                        {
                            className: "center",data:"ID"
                        },
                        {
                            className: "center",data:"message","orderable": false,
                            render: function (val, type, row) {


                                var from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/user/edit/'+row.userMessageID+'">'+row.userName+'</a>';

                                if (row.adminMessageID)
                                {
                                    from = '<a class="link-standart" target="_blank" href="/cms/<?=$lang?>/admin/edit/'+row.adminMessageID+'">'+row.adminName+'</a>';
                                }

                                var text = row.short_message;

                                if (row.message != row.short_message)
                                {
                                    text+=' <button class="btn view_full_text" data-full="'+row.message+'" data-toggle="button"><i class="fa fa-eye"></i></button>';
                                }


                                if (row.deleted == 'true')
                                {
                                    text += '<span class="label label-default">Видалене</span>';
                                }

                                return '<i>'+from+'<br>'+row.created+'</i><br><a class="link-standart text-mess">'+text+'</a>';
                            }
                        },
                        {
                            className: "center",
                            "orderable": false,
                            render: function (val, type, row) {
                               var res = '<button class="btn btn-danger delete" data-id="'+row.ID+'" data-toggle="button"><i class="fa fa-times"></i></button>';
                                return res+'</div>';
                            }
                        },
                    ],
                    "order": [[0, 'desc']],
                    language: {
                        processing:     "Завантажую..",
                        search:         "Пошук&nbsp;:",
                        lengthMenu:     "Записів на сторінці: _MENU_ ",
                        info:           "Показано записів з _START_ по _END_ із _TOTAL_",
                        infoEmpty:      "Записів немає",
                        infoFiltered:   "",
                        infoPostFix:    "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords:    "Даних немає",
                        emptyTable:     "Даних немає",
                        paginate: {
                            first:      "Перша",
                            previous:   "Назад",
                            next:       "Вперед",
                            last:       "Остання"
                        },
                        aria: {
                            sortAscending:  ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre dÃ©croissant"
                        }
                    }

                } );


            });


            function callback_action()
            {
                window.table.draw(false);
            }


            $(document).on( "click", ".select_one_req", function(){
                var c_count = 0;
                $(".select_one_req").each(function(){
                    if ($(this).prop('checked'))
                    {
                        c_count++;
                    }
                });

            });

            $(document).on( "click", ".select_all_req", function(){
                if ($(this).prop('checked'))
                {
                    $(".select_one_req").prop('checked',true);
                    $(".select_all_req").prop('checked',true);
                }
                else
                {
                    $(".select_one_req").prop('checked',false);
                    $(".select_all_req").prop('checked',false);
                }
            });

        </script>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Видалення повідомлення</h4>
                    </div>
                    <div class="modal-body">
                        Ви підтверджуєте видалення повідомлення з номером "<span id="name_del"></span>"?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                        <button type="button" id="delItem" data-dismiss="modal" class="btn btn-success">Видалити</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).on( "click", ".delete", function(){
                var id = $(this).data('id');
                $("#name_del").html(id);
                $("#delItem").attr('id_item',id);
                $("#delModal").modal();
            });

            $(document).on( "click", "#delItem", function(){
                var id = $(this).attr('id_item');
                $.post("/component_cms/<?=$lang?>/messages/ajax/",{id:id,action:'delete'},callback_action,"json");
                $("#delModal").modal('hide');
            });
        </script>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="delModalAll" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Видалення вибраних повідомлень</h4>
                    </div>
                    <div class="modal-body">
                        Ви підтверджуєте видалення вибраних повідомлень?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Відміна</button>
                        <button type="button" id="delItemAll" data-dismiss="modal" class="btn btn-success">Видалити</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">


            $(document).on( "click", "#delete_all", function(){
                set_all_ids();
                if (window.ids.length > 0) $("#delModalAll").modal();
            });

            $(document).on( "click", "#delItemAll", function(){
                $.post("/component_cms/<?=$lang?>/messages/ajax/",{ids:window.ids,action:'multi_delete'},callback_itemAll,"json");
            });

            function set_all_ids()
            {
                var ids = [];
                $(".select_one_req").each(function(){
                    if ($(this).prop('checked'))
                    {
                        ids.push($(this).data('id'));
                    }
                });

                window.ids = ids;
            }

            function callback_itemAll()
            {
                $(".select_all_req").prop('checked',false);
                callback_action();
            }
        </script>

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="allTextModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Повний текст повідомлення</h4>
                    </div>
                    <div class="modal-body">
                        <p id="text_comment_all"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).on( "click", ".view_full_text", function(){
                var text = $(this).data('full');
                $("#text_comment_all").html(text);
                $("#allTextModal").modal('show');
            });
        </script>

        <?
    }
    else
    {
        ?>
        <div class="alert alert-danger fade in">
            Вказаного діалога не існує
        </div>
        <?
    }
?>

