<p>
    <a class="remove-doc"
       data-action="delete"
       data-location="<?=           $location               ?>"
       data-id="<?=                 $id                     ?>"
       data-dir="<?=                $rootDir                ?>"
       data-file="<?=               $fileName               ?>"
       data-token="<?=              $fileTokenDelete        ?>" href="#">
        <i class="fa fa-times fa-lg"></i>
        <?= $text ?>
    </a>
</p>