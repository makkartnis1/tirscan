<p>
    <a href="<?= $download_url ?>">
        <span class="blue">
            <i class="fa fa-download fa-lg" style="color: inherit"></i>
            <?= $text ?>
        </span>

    </a>
</p>