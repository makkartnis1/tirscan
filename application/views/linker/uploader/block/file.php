<div class="col-xs-6">

    <div class="row companyDocs">
        <div class="col-xs-4 col-sm-3 place-in-center">

            <?= $block['file/icon'] ?>

        </div>
        
        <div class="col-xs-8 col-sm-9 docName transport">
            
            <?= $block['file/preview-url-block']    ?>

            <?= $block['file/delete-block']         ?>
            
            <?= $block['file/download-block']       ?>
            
        </div>
    </div>
</div>
