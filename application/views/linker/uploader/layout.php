<div class="modal-body" data-widget="files" data-widget-type="manage" data-init="<?= htmlspecialchars($params) ?>">
    <div class="row">
        <div class="col-xs-12 col-ms-12 col-sm-12 col-md-12">

            <div class="companyDocsHolder tr" data-widget="files">

                <p class="title">
                    <?= $title ?>:

                    <?= $block['upload'] ?>

                </p>

                <div class="row">

                    <?= $block['files'] ?>

                </div>
            </div>

        </div>
    </div>
</div>