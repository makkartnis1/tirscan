<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 11.05.16
 * Time: 18:19
 */?>
<div style="display: none/**/" data-this-is="uploader">
    <!--    <UPLOAD ACTION> -->
    <form
        data-this-is="upload"
        action="<?= Route::url('linker/uploader/upload')
        ?>" enctype="multipart/form-data" method="post" target="linker-js-iframe-file-upload">

        <input type="file" name="file">
        <input type="text" name="token">
        <input type="text" name="id">
        <input type="text" name="location">
        <input type="text" name="dir">
        <input type="text" name="config">
        <input type="text" name="config-token">
        <input type="text" name="init">
        <button type="submit"></button>
        <input type="text" name="callback" value="linker_uploader_assign_after_upload">
    </form>
    <iframe style="width: 100%" name="linker-js-iframe-file-upload" src="<?= Route::url('linker/uploader/upload') ?>"></iframe>
    <!--    </UPLOAD ACTION> -->
    <!--    <UPLOAD ACTION> -->
    <form
        data-this-is="delete"
        action="<?= Route::url('linker/uploader/delete')
        ?>" method="post" target="linker-js-iframe-file-delete">

        <input type="text" name="token">
        <input type="text" name="file">
        <input type="text" name="location">
        <input type="text" name="id">
        <input type="text" name="dir">
        <input type="text" name="token/upload">
        <input type="text" name="init">
        <button type="submit"></button>
        <input type="text" name="callback" value="linker_uploader_assign_after_delete">
    </form>
    <iframe style="width: 100%" name="linker-js-iframe-file-delete" src="<?= Route::url('linker/uploader/delete') ?>"></iframe>
    <!--    </UPLOAD ACTION> -->

</div>