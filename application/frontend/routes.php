<?php

/**
 * Правила роутингу з мовами
 *
 * 1) якщо lang по замовчуванню null (або не вказаний), то контролер переадресує користувача на сторінку із мовою
 * по замовчуванню
 * 2) якщо lang по замовчуванню false, то система не буде переадресовувати на мову по замовчуванню якщо lang не буде
 * вказаний при запиті
 */

Route::set('frontend_request_doc', 'doc/<type>/<id>')
     ->defaults([
         'controller' => 'Doc'
     ]);

Route::set('frontend_request_doc2', 'doc2/<type>/<id>')
    ->defaults([
        'controller' => 'Doc',
        'action'     => 'doc2',
    ]);

Route::set('frontend_callback_liqpay', 'liqpay/<action>')
     ->defaults([
         'controller' => 'Liqpay'
     ]);

Route::set('frontend_hmvc_error', '(<lang>/)error(/<code>)')
    ->defaults([
        'directory' => 'Frontend',
        'controller' => 'Error'
    ]);

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -  <доступ до файлів    -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
Route::set('linker/uploader/preview',                   'files/uploads/<rootDir>/<token><location>/<id>/preview/<file>',
    [ 'location' => '.*', 'file' => '.*', 'token'=> '.{32}', 'id' => '\d+'])
    ->defaults([
        'directory'     => 'Frontend',
        'controller'    => 'File',
        'action'        => 'preview',
    ]);
Route::set('linker/uploader/download',                  'files/uploads/<rootDir>/<token><location>/<id>/download/<file>',
    [ 'location' => '.*', 'file' => '.*', 'token'=> '.{32}', 'id' => '\d+' ])
    ->defaults([
        'directory'     => 'Frontend',
        'controller'    => 'File',
        'action'        => 'download',
    ]);
Route::set('linker/uploader/delete',                    'files/uploads/delete'//<rootDir>/<token><location>/<id>/delete/<file>
    //[ 'location' => '.*', 'file' => '.*', 'token'=> '.{32}', 'id' => '\d+']
 )
    ->defaults([
        'directory'     => 'Frontend',
        'controller'    => 'File',
        'action'        => 'delete',
    ]);
Route::set('linker/uploader/upload',                    'files/uploads/upload')
    ->defaults([
        'directory'     => 'Frontend',
        'controller'    => 'File',
        'action'        => 'upload',
    ]);
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  доступ до файлів>    -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -



Route::set('frontend_site_ajax_profile', 'ajax/profile/<action>')
    ->defaults([
        'directory'  => 'frontend/profile',
        'controller' => 'ajax'
    ]);

Route::set('frontend_site_test', 'test')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'test',
        'action'     => 'index'
    ]);

// окремі сторінки профілю
Route::set('frontend_site_profile_page_actions', '(<lang>/)profile/action/<action>', ['action' => 'add_tender'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'profile',
        'action'     => 'add_tender'
    ]);

// профіль користувача
Route::set('frontend_site_profile', '(<lang>/)profile')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'profile',
        'action' => 'index',
    ]);

Route::set('no_car_gallery', 'uploads/files/public/car/<action>/<id>/<file>',[
    'id'    => '\d+',
    'file'    => '.*',
])
    ->defaults([
        'directory'  => 'Frontend/Fix/File',
        'controller' => 'car',
        'action' => 'index',
    ]);
//                                   /uploads/files/public/user/avatar/24188/no-avatar.jpg
//                                   /uploads/files/public/user/avatar/3067/no-avatar.jpg
Route::set('no_user_avatar_gallery', 'uploads/files/public/user/<action>/<id>/<file>',[
    'id'    => '\d+',
    'file'    => '.*',
])
    ->defaults([
        'directory'  => 'Frontend/Fix/File',
        'controller' => 'user',
        'action' => 'avatar',
    ]);

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -  <hmvc блоки профайла -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
Route::set('hmvc/profile',                  '@hmvc-linker/-.-/profile/page/<localeID>/<controller>/<action>')
    ->defaults([
        'directory'  => 'Frontend/HMVC/Profile',
        'action' => 'index',
    ]);
Route::set('hmvc/profile/tab/infoblock',    '@hmvc-linker/-.-/profile/tab/shell/tab/infoblock/<localeID>/<controller>/<action>')
    ->defaults([
        'directory'  => 'Frontend/HMVC/Profile/Tab/Info',
        'action' => 'index',
    ]);

Route::set('hmvc/profile/tab/my-profile',    '@hmvc-linker/-.-/profile/tab/shell/tab/my-profile/<localeID>/<controller>/<action>')
    ->defaults([
        'directory'  => 'Frontend/HMVC/Profile/Tab/Profile',
        'action' => 'index',
    ]);
Route::set('hmvc/profile/tab',              '@hmvc-linker/-.-/profile/tab/shell/<localeID>/<controller>/<action>')
    ->defaults([
        'directory'  => 'Frontend/HMVC/Profile/Tab',
        'action' => 'index',
    ]);
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  hmvc блоки профайла> -   -   -   -
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -

Route::set('@widget','@front/@site/@widget/<controller>/<action>(/<param>)',['param','.*'])
    ->defaults([
        'directory'     => 'Frontend/Widget',
    ]);

Route::set('linker/api',                  'api/front/<controller>/<method>')
    ->defaults([
        'directory'  => 'Ajax/Frontend',
        'action' => 'index',
    ]);

Route::set('krid/filter/api',                  'api/front/filter/<controller>/<method>')
     ->defaults([
         'directory'  => 'Ajax/Frontend/Filter',
         'action' => 'index',
     ]);

// Список новин компанії
Route::set('frontend_site_company_list_news', '(<lang>/)company/news(/<company_id>)(/<page>)', ['company_id' => '[0-9]+', 'page' => '[1-9]\d*'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'company',
         'action'     => 'list_news'
     ]);

// Сторінка новини компанії
Route::set('frontend_site_company_page_news', '(<lang>/)company/news(/<company_id>/)<id>-<url>', ['company_id' => '[0-9]+', 'id' => '[0-9]+', 'url' => '.+'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'company',
         'action'     => 'page_news'
     ]);

// Список новин сайту
Route::set('frontend_site_news_list', '(<lang>/)news(/<page>)', ['page' => '[1-9]\d*'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'NewsSite',
         'action'     => 'list',
     ]);

// Сторінка новини сайту
Route::set('frontend_site_news_page', '(<lang>/)news/<id>-<url>', ['id' => '[0-9]+', 'url' => '.+'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'NewsSite',
         'action'     => 'page',
     ]);

// список компаній
Route::set('frontend_site_company_list_first_page', '(<lang>/)company')
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'company',
         'action' => 'list',
         'page' => 1
     ]);
Route::set('frontend_site_company_list', '(<lang>/)company(/page-<page>)', ['page' => '[1-9]\d*'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'company',
        'action' => 'list'
    ]);

// сторінка компанії
Route::set('frontend_site_company_page', '(<lang>/)company/<company_url_name>', ['company_url_name' => '[1-9]\d*[-].+'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'company',
        'action' => 'page'
    ]);



// додавання тендерної заявки
//Route::set('frontend_site_request_add_tender', '(<lang>/)tender/add')
  //  ->defaults([
    //    'directory'  => 'frontend',
      //  'controller' => 'tenders',
        //'action' => 'add'
   // ]);

// додавання тендерної заявки
Route::set('frontend_site_request_add_tender', '(<lang>/)tender/add')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'tenders',
        'action' => 'add'
    ]);


// редагування однієї тендерної заявки
Route::set('frontend_site_tender_edit', '(<lang>/)tender/edit/<id>', ['id' => '[1-9]\d*'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'tenders',
        'action' => 'edit'
    ]);


// вивід однієї тендерної заявки
Route::set('frontend_site_tender', '(<lang>/)tender/<id>', ['id' => '[1-9]\d*'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'tenders',
         'action' => 'one'
     ]);

// TODO: wtf is this shit?
//Route::set('frontend_site_request_tender_page', '(<lang>/)tender/view(/<id>)')
//    ->defaults([
//        'directory'  => 'frontend',
//        'controller' => 'requests',
//        'action' => 'tender_page'
//    ]);

Route::set('frontend_site_request_tender_list_first_page', '(<lang>/)tender')
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'requests',
         'action' => 'tender'
     ]);
Route::set('frontend_site_request_tender_list', '(<lang>/)tender(/page-<page>)', ['page' => '[1-9]\d*'])
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'requests',
         'action' => 'tender',
         'page' => 1
     ]);

Route::set('frontend_site_request_transport_list_first_page', '(<lang>/)transport')
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'requests',
         'action' => 'transport',
         'page' => 1
     ]);
Route::set('frontend_site_request_transport_list', '(<lang>/)transport(/page-<page>)', ['page' => '[1-9]\d*'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'requests',
        'action' => 'transport'
    ]);

Route::set('frontend_site_request_cargo_list_first_page', '(<lang>/)cargo')
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'requests',
         'action' => 'cargo',
         'page' => 1
     ]);
Route::set('frontend_site_request_cargo_list', '(<lang>/)cargo(/page-<page>)', ['page' => '[1-9]\d*'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'requests',
        'action' => 'cargo'
    ]);


// додавання транспорту
Route::set('frontend_site_request_add_transport', '(<lang>/)transport/add')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'requests',
        'action' => 'addTransport'
    ]);

// додавання вантажу
Route::set('frontend_site_request_add_cargo', '(<lang>/)cargo/add')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'requests',
        'action' => 'addCargo'
    ]);

// реєстрація на сайті
Route::set('frontend_site_company_registration', '(<lang>/)company/registration')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'company',
        'action'     => 'registration',
    ]);

Route::set('frontend_site_company_registration_confirmation', '(<lang>/)company/registration/confirm/<code>')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'company',
        'action'     => 'confirm_registration',
    ]);

Route::set('frontend_site_company_password_confirmation', '(<lang>/)company/password/confirm/<code>')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'company',
        'action'     => 'confirm_forgot_password',
    ]);

// службові сторінки (авторизація і вихід з системи)
Route::set('frontend_site_auth', 'auth/<action>', ['action' => 'login|logout|social|forgot_pass'])
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'authorized',
        'action'     => 'login',
        'lang'       => false
    ]);

// статична сторінка
Route::set('frontend_static_page', '(<lang>/)page/<uri>')
     ->defaults([
         'directory'  => 'frontend',
         'controller' => 'static',
         'action'     => 'index'
     ]);


// головна сторінка
Route::set('frontend_index', '(<lang>)')
    ->defaults([
        'directory'  => 'frontend',
        'controller' => 'site',
        'action'     => 'index',
        'lang'       => 'ru'
    ]);
