<?php defined('SYSPATH') OR die('No direct access allowed.');

$conf_profiles          = Kohana::list_files('/config/uploader/settings');
$conf_profiles_result   = [];

foreach ($conf_profiles as $shortPath => $fullPath){
    /*

    ["/config/uploader/settings/default.php"]=>

    string(109) "/Users/LINKeR/PhpstormProjects/bitbucket.tirscan.com/tirscan/application/config/uploader/settings/default.php"


     */

    $conf_tmp_1 = explode('.',$shortPath);

    $conf_tmp_ext   = array_pop($conf_tmp_1);

    if(strtolower($conf_tmp_ext) !== 'php')
        continue;

    $conf_tmp_name  = implode('.',$conf_tmp_1);

    $conf_profiles_result[basename($conf_tmp_name)] = Kohana::$config->load(substr($conf_tmp_name,8))->as_array();

    # callbacks, вони в JSON_ENCODE попадати не повинни, бо це серверна обробка
    unset($conf_profiles_result[basename($conf_tmp_name)]['after']);

}

# повертає масив всіх профілів для загрузки файлів для використання під час валідацї на JS
return $conf_profiles_result;