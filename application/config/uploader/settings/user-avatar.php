<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    # якщо не перевіряти то не задавати або задати 0, або INT значення більше нуля
    'size'      => 1000000, // 1000000 === 1 MB
    
    # якщо не перевіряти то не задавати, або довільне значення, або массив із переліокм значень
    'mime'          => ['image/jpeg', 'image/png'],
    
    'after'         => [
        'save'      => function($fullFilePathOfJustUploadedAndValidFile){

            try{


                // Путь до оригинального файла
                $filename = $fullFilePathOfJustUploadedAndValidFile;

                $img = Image::factory($filename);
                $w = $img->width;
                $h = $img->height;

                $XY = ($w < $h) ? $w : $h;

                $img->crop($XY, $XY);

                if( $XY > 400 )
                    $img->resize( 400, 400, Image::AUTO);

                $img->save($filename, 80);

//                // Пересжимаем и сохраняем изображение
//                Image::factory($filename)
//                    ->resize(200, 200, Image::AUTO)
//                    ->save($filename, 80);

                $files = glob(dirname($filename).'/*'); // get all file names

                foreach($files as $file){ //

                    if(is_file($file) AND $file !== $filename)
                        unlink($file); // ви даляємо всі інші старі аватарки

                }

            }catch (Exception $e){

                return 'avatar.save.failed';

            }


        },
    ],

);