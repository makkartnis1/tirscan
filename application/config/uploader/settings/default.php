<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    # якщо не перевіряти то не задавати або задати 0, або INT значення більше нуля
    'size'      => 1000*60*2*5,
    
    # якщо не перевіряти то не задавати, або довільне значення, або массив із переліокм значень
    'mime'          => ['image/jpeg', 'image/png'],

);