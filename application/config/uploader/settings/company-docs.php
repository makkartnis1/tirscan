<?php
/**
 * LINKeR
 */

defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    # якщо не перевіряти то не задавати або задати 0, або INT значення більше нуля
    'size'      => 5000000, // 1000000 === 1 MB
    
    # якщо не перевіряти то не задавати, або довільне значення, або массив із переліокм значень
    'mime'          => [
        'image/jpeg',
        'image/png',
        'application/msword',//        .doc, .dot
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',//.docx
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template',//.dotx
        'application/vnd.ms-word.document.macroEnabled.12',//.docm
        'application/vnd.ms-word.template.macroEnabled.12',//.dotm
        'application/pdf', // pdf
    ],
    
    'after'         => [
        'save'      => function(){},// do nothing
    ],

);