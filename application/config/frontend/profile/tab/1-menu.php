<?php defined('SYSPATH') OR die('No direct access allowed.');

return [

    // перша основна менюшка (загальна "шкура" для всіх вложених таб)
    
    'profile'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-user',
            'caption'   => 'profile.tab.my-profile',    // Ваш профіль
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'info',
                ],
            ]
        ]

    ],


    'messenger'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-bars',                   // Мої повідомлення
            'caption'   => 'profile.tab.my-messages',
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'messenger',
                ],
            ]
        ]

    ],


    'transport'  => [

        'enabled'   => 'transport',
//        $this->user->hasRights('transport');
//        $this->user->hasRights('cargo');

        'tab'       => [
            'icon'      => 'fa-truck',
            'caption'   => 'profile.tab.my-transport',  // Мій транспорт
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'transport',
                ],
            ]
        ]

    ],


    'ticket-cargo'  => [

        'enabled'   => 'cargo',

        'tab'       => [
            'icon'      => 'fa-suitcase',
            'caption'   => 'profile.tab.my-cargo-tickets',    // Мої заявки на вантаж
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller'    => 'ticket',
                    'action'        => 'cargo'
                ],
            ]
        ]

    ],


    'ticket-transport'  => [

        'enabled'   => 'transport',

        'tab'       => [
            'icon'      => 'fa-car',
            'caption'   => 'profile.tab.my-transport-tickets',    // Мої заявки на транспорт
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller'    => 'ticket',
                    'action'        => 'transport'
                ],
            ]
        ]

    ],



    'tender'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-clock-o',
            'caption'   => 'profile.tab.my-tenders',    // Тендери
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'tender',
                ],
            ]
        ]

    ],


    'wallet'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-credit-card',
            'caption'   => 'profile.tab.my-wallet',    // Гаманець
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'wallet',
                ],
            ]
        ]

    ],


    'infoblock'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-list-alt',
            'caption'   => 'profile.tab.my-infoblock',    // Інформаційний блок
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'profile',
                ],
            ]
        ]

    ],


    'statistic'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-bar-chart',
            'caption'   => 'profile.tab.my-statistic',    // Статистика
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'statistic',
                ],
            ]
        ]

    ],

    'my-company'  => [

        'enabled'   => 'owner',
        

        'tab'       => [
            'icon'      => 'fa-building',
            'caption'   => 'profile.tab.my-company',    // Управління компанією для власнка компанії (управління менеджерами і т.д.)
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'company',
                ],
            ]
        ]

    ],

    'subscription'  => [

        'enabled'   => '',

        'tab'       => [
            'icon'      => 'fa-envelope',
            'caption'   => 'profile.tab.my-subscription',    // Підписка на новини
        ],

        'content-hmvc'       => [
            'route' =>  [
                'name'          => 'hmvc/profile/tab',
                'parameters'    => [
                    'controller' => 'subscription',
                ],
            ]
        ]

    ],

];