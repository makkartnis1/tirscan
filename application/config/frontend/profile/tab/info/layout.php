<?php defined('SYSPATH') OR die('No direct access allowed.');

return [

    // вміст контейнера "інфоблок"
    
    'tab-infoblock-myinfo'  => [
        'caption'       => __db('profile.tab.infoblock.myinfo'),
        'action'        => 'index',
        'controller'    => 'myinfo',
    ],
    'tab-infoblock-docs'  => [
        'caption'       => __db('profile.tab.infoblock.docs'),
        'action'        => 'index',
        'controller'    => 'docs',
    ],
    'tab-infoblock-mypass'  => [
        'caption'       => __db('profile.tab.infoblock.mypass'),
        'action'        => 'index',
        'controller'    => 'mypass',
    ],
    'tab-infoblock-companyinfo'  => [
        'caption'       => __db('profile.tab.infoblock.companyinfo'),
        'action'        => 'index',
        'controller'    => 'companyinfo',
    ],
    'tab-infoblock-news'  => [
        'caption'       => __db('profile.tab.infoblock.newslist'),
        'action'        => 'index',
        'controller'    => 'newslist',
    ],
    'tab-infoblock-newsadd'  => [
        'caption'       => __db('profile.tab.infoblock.newsadd'),
        'action'        => 'index',
        'controller'    => 'newsadd',
    ],

];