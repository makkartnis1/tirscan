<?php defined('SYSPATH') OR die('No direct access allowed.');

return [

    'pagination' => [
        'group_length' => 2
    ],

    'company' => [
        'list' => [
            'page_size' => 8
        ],

        'paths' => [
            'documents' => [
                'podatkovi' => '/uploads/files/company/podatkovi/%d/*.*',
                'sertifikaty' => '/uploads/files/company/sertifikaty/%d/*.*',
                'svidoctva' => '/uploads/files/company/svidoctva/%d/*.*',
            ],
            'images' => '/uploads/images/company/%d.jpg'
        ],

        'images' => [
            'no_logo' => '/uploads/files/no_image.jpg'
        ]
    ],

    'user' => [
        'registration' => [
            'temp_link_length' => 32,
            'temp_link_ttl' => '2 hours'
        ],
        'cars' => [
            'paths' => [
                'documents' => '/uploads/files/cars/docs/%d/',
                'images' => '/uploads/files/cars/fotos/%d/',
            ]
        ]
    ],

    'google_API' => [
        'key' => 'AIzaSyDaVrehTIv6GFZeKlWOLB_EOjkp2CuXZfg'
    ],

    'uLogin' => [
        'request_url' => 'http://ulogin.ru/token.php?token=%s&host=%s'
    ]

];