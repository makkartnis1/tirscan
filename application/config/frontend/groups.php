<?php defined('SYSPATH') OR die('No direct access allowed.');

return [

    Model_Frontend_Company_Type::EXPEDITOR => [
        'transport',
        'cargo'
    ],

    Model_Frontend_Company_Type::TRANSPORT => [
        'transport'
    ],

    Model_Frontend_Company_Type::CARGO => [
        'cargo'
    ]

];