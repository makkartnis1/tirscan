<?php defined('SYSPATH') OR die('No direct access allowed.');

// матриця ваг
$weights = [
    'json_docTIR' => 1,
    'json_docCMR' => 3,
    'json_docT1' => 2,
    'json_docSanPassport' => 13,
    'json_docSanBook' => 12,

    'json_loadFromSide' => 11,
    'json_loadFromTop' => 10,
    'json_loadFromBehind' => 9,
    'json_loadTent' => 8,

    'json_condPlomb' => 7,
    'json_condLoad' => 14,
    'json_condBelts' => 6,
    'json_condRemovableStands' => 15,
    'json_condHardSide' => 16,
    'json_condCollectableCargo' => 17,
    'json_condPalets' => 18,
    'json_condADR' => 19,
    'json_condTemperature' => 20,

    'json_onlyTransporter' => 50,

    'json_cargoType' => 75

//    'json_PDV' => 1,
//    'json_onLoad' => 1,
//    'json_onUnload' => 1,
//    'json_onPrepay' => 1,
//    'json_advancedPayment' => 1,
];

// матриця спільних параметрів
$common = [
    'db_locales' => [
        'json_docTIR' => 'request.info.docTIR(:value:)',
        'json_docCMR' => 'request.info.docCMR(:value:)',
        'json_docT1' => 'request.info.docT1(:value:)',
        'json_docSanPassport' => 'request.info.docSanPassport(:value:)',
        'json_docSanBook' => 'request.info.docSanBook(:value:)',
        
        'json_loadFromSide' => 'request.info.loadFromSide(:value:)',
        'json_loadFromTop' => 'request.info.loadFromTop(:value:)',
        'json_loadFromBehind' => 'request.info.loadFromBehind(:value:)',
        'json_loadTent' => 'request.info.loadTent(:value:)',

        'json_condPlomb' => 'request.info.condPlomb(:value:)',
        'json_condLoad' => 'request.info.condLoad(:value:)',
        'json_condBelts' => 'request.info.condBelts(:value:)',
        'json_condRemovableStands' => 'request.info.condRemovableStands(:value:)',
        'json_condHardSide' => 'request.info.condHardSide(:value:)',
        'json_condCollectableCargo' => 'request.info.condCollectableCargo(:value:)',
        'json_condPalets' => 'request.info.condPalets(:value:)',
        'json_condADR' => 'request.info.condADR(:value:)',
        'json_condTemperature' => 'request.info.condTemperature(:value:)',

//        'json_PDV' => 'request.info.PDV',
//        'json_onLoad' => 'request.info.onLoad',
//        'json_onUnload' => 'request.info.onUnload',
//        'json_onPrepay' => 'request.info.onPrepay',
//        'json_advancedPayment' => 'request.info.advancedPayment',
    ]
];
$cargo = ($transport = $common);

// специфічні зміни для вантажних заявок
$cargo['db_locales']['json_onlyTransporter'] = 'request.info.onlyTransporter(:value:)';
$cargo['db_locales']['json_cargoType'] = 'request.info.cargoType(:value:)';

// специфічні зміни для транспортних заявок
//$transport = [];

return compact('weights', 'cargo', 'transport');