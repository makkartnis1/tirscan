<?php defined('SYSPATH') OR die('No direct access allowed.');

$transport = [
    'date_from' => [
        'i18n'  => 'request.form.date_from',
        'value' => ':date_from:',
        'clear' => '[name="date_from"]'
    ],
    'date_to' => [
        'i18n'  => 'request.form.date_to',
        'value' => ':date_to:',
        'clear' => '[name="date_to"]'
    ],
    'car_type' => [
        'i18n'  => 'request.form.car_type',
        'value' => ':car_type:',
        'clear' => '[name="car_type"]'
    ],
    'weight' => [
        'i18n'  => 'request.form.weight',
        'value' => '>= :weight:',
        'clear' => '[name="weight"]'
    ],
    'volume' => [
        'i18n'  => 'request.form.volume',
        'value' => '>= :volume:',
        'clear' => '[name="volume"]'
    ],
    'size_x' => [
        'i18n'  => 'request.form.size_x',
        'value' => '>= :size_x:',
        'clear' => '[name="size_x"]'
    ],
    'size_y' => [
        'i18n'  => 'request.form.size_y',
        'value' => '>= :size_y:',
        'clear' => '[name="size_y"]'
    ],
    'size_z' => [
        'i18n'  => 'request.form.size_z',
        'value' => '>= :size_z:',
        'clear' => '[name="size_z"]'
    ],
    'doc_TIR' => [
        'i18n'  => 'request.form.doc_TIR',
        'value' => null,
        'clear' => '[name="doc_TIR"]'
    ],
    'doc_CMR' => [
        'i18n'  => 'request.form.doc_CMR',
        'value' => null,
        'clear' => '[name="doc_CMR"]'
    ],
    'doc_T1' => [
        'i18n'  => 'request.form.doc_T1',
        'value' => null,
        'clear' => '[name="doc_T1"]'
    ],
    'doc_sanpassport' => [
        'i18n'  => 'request.form.doc_sanpassport',
        'value' => null,
        'clear' => '[name="doc_sanpassport"]'
    ],
    'doc_sanbook' => [
        'i18n'  => 'request.form.doc_sanbook',
        'value' => null,
        'clear' => '[name="doc_sanbook"]'
    ],
    'load_from_side' => [
        'i18n'  => 'request.form.load_from_side',
        'value' => null,
        'clear' => '[name="load_from_side"]'
    ],
    'load_from_top' => [
        'i18n'  => 'request.form.load_from_top',
        'value' => null,
        'clear' => '[name="load_from_top"]'
    ],
    'load_from_behind' => [
        'i18n'  => 'request.form.load_from_behind',
        'value' => null,
        'clear' => '[name="load_from_behind"]'
    ],
    'load_tent' => [
        'i18n'  => 'request.form.load_tent',
        'value' => null,
        'clear' => '[name="load_tent"]'
    ],
    'cond_plomb' => [
        'i18n'  => 'request.form.cond_plomb',
        'value' => null,
        'clear' => '[name="cond_plomb"]'
    ],
    'cond_load' => [
        'i18n'  => 'request.form.cond_load',
        'value' => null,
        'clear' => '[name="cond_load"]'
    ],
    'cond_stripes' => [
        'i18n'  => 'request.form.cond_stripes',
        'value' => null,
        'clear' => '[name="cond_stripes"]'
    ],
    'cond_rem_stands' => [
        'i18n'  => 'request.form.cond_rem_stands',
        'value' => null,
        'clear' => '[name="cond_rem_stands"]'
    ],
    'cond_bort' => [
        'i18n'  => 'request.form.cond_bort',
        'value' => null,
        'clear' => '[name="cond_bort"]'
    ],
    'cond_collectable' => [
        'i18n'  => 'request.form.cond_collectable',
        'value' => null,
        'clear' => '[name="cond_collectable"]'
    ],
    'cond_t' => [
        'i18n'  => 'request.form.cond_t',
        'value' => ':cond_t_value:',
        'clear' => '[name="cond_t"],[name="cond_t_value"]'
    ],
    'cond_pallets' => [
        'i18n'  => 'request.form.cond_pallets',
        'value' => '>= :cond_pallets_value:',
        'clear' => '[name="cond_pallets"],[name="cond_pallets_value"]'
    ],
    'cond_ADR' => [
        'i18n'  => 'request.form.cond_ADR',
        'value' => ':cond_ADR_value:',
        'clear' => '[name="cond_ADR"],[name="cond_ADR_value"]'
    ],
    'cond_only_transport' => [
        'i18n'  => 'request.form.cond_only_transport',
        'value' => null,
        'clear' => '[name="cond_only_transport"]'
    ],
    'price_value' => [
        'i18n'  => 'request.form.price_value',
        'value' => '<= :price_value:',
        'clear' => '[name="price_value"]'
    ],
    'price_on_load' => [
        'i18n'  => 'request.form.price_on_load',
        'value' => null,
        'clear' => '[name="price_on_load"]'
    ],
    'price_PDV' => [
        'i18n'  => 'request.form.price_PDV',
        'value' => null,
        'clear' => '[name="price_PDV"]'
    ],
    'price_before' => [
        'i18n'  => 'request.form.price_before',
        'value' => null,
        'clear' => '[name="price_before"]'
    ],
    'price_before_amount' => [
        'i18n'  => 'request.form.price_before_amount',
        'value' => '<= :price_before_amount:%',
        'clear' => '[name="price_before_amount"]'
    ],
    'price_on_unload' => [
        'i18n'  => 'request.form.price_on_unload',
        'value' => null,
        'clear' => '[name="price_on_unload"]'
    ],
    'pay_form' => [
        'i18n'  => 'request.form.pay_form',
        'value' => ':!pay_form:',
        'clear' => '[name="pay_form"]'
    ],
    'load_geo' => [
        'i18n'  => 'request.form.geo_from',
        'value' => ':load:',
        'clear' => '[name="load"],[name="load_geo"]'
    ],
    'unload_geo' => [
        'i18n'  => 'request.form.geo_to',
        'value' => ':unload:',
        'clear' => '[name="unload"],[name="unload_geo"]'
    ],
    'load_radius' => [
        'i18n'  => 'request.form.geo_from_radius',
        'value' => ':load_radius:',
        'clear' => '[name="load_radius"]'
    ],
    'unload_radius' => [
        'i18n'  => 'request.form.geo_to_radius',
        'value' => ':unload_radius:',
        'clear' => '[name="unload_radius"]'
    ]
];

$cargo = array_replace_recursive($transport, [
    'weight' => [
        'value' => '<= :weight:'
    ],
    'volume' => [
        'value' => '<= :volume:'
    ],
    'size_x' => [
        'value' => '<= :size_x:'
    ],
    'size_y' => [
        'value' => '<= :size_y:'
    ],
    'size_z' => [
        'value' => '<= :size_z:'
    ],
    'cond_pallets' => [
        'value' => '<= :cond_pallets_value:'
    ],
    'cond_t' => [
        'value' => '>= :cond_t_value:'
    ],
    'price_value' => [
        'value' => '>= :price_value:'
    ],
    'price_before_amount' => [
        'value' => '>= :price_before_amount:%',
    ]
]);

return compact('transport', 'cargo');