$(function(){
    var height_menu = $(".sticky-left-side").css("height");
    $(".main-content").css("height",height_menu);

    $('.default-date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });

});


$(document).on( "click", "#view_mode", function(){
    var collapse = $(this).attr('collapse');
    if (collapse==0)
    {

        $(this).attr('collapse','1');
        $("#collapse_i").removeClass('fa-caret-square-o-left');
        $("#collapse_i").addClass('fa-caret-square-o-right');
        $(".main-content").css('width','96%');
        $("body").addClass('left-side-collapsed');
    }
    else
    {
        $(this).attr('collapse','0');
        $("#collapse_i").removeClass('fa-caret-square-o-right');
        $("#collapse_i").addClass('fa-caret-square-o-left');
        $(".main-content").css('width','82%');
        $("body").removeClass('left-side-collapsed');
    }
    $.post("/cms/collapse",{collapse:collapse},"post");

    var height_menu = $(".sticky-left-side").css("height");
    $(".main-content").css("height",height_menu);

});

function number_format( number, decimals, dec_point, thousands_sep ) {

    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    return km + kw + kd;
}
