

        var autocomplete = {};


        function initialize(selector) {
            var options = {
                types : ['(regions)']
            };
            var input = document.getElementById(selector);
            autocomplete[selector] = new google.maps.places.Autocomplete(input , options, googleMapsLoaded);


        }



        function googleMapsLoaded(selector){

            var place = autocomplete[selector].getPlace();

            var post = new Object();
            post.place_id = place.place_id;
            post.latitude = place.geometry.location.lat;
            post.longitude = place.geometry.location.lng;
            post.name = place.name;
            post.full_name = place.formatted_address;
            post.places = [];
            post.country = '';

            $.each(place.address_components, function(key, value){
                if (key > 0)
                {
                    post.places.push(value.long_name);
                }
                post.country = value.short_name;
            });


            $.ajax({
                method: "POST",
                url: "/cms/geo/"+window.lang+"/add/",
                data: post
            })
                .done(function( response ) {
                    $('#'+selector+'_places').val(response);
                });
        }



        function google_place_init(selector,lang)
        {
            window.lang = lang;

            initialize(selector);

            var map = google.maps.event.addDomListener(window, 'load', initialize(selector));


            $(document).on('change', '#'+selector, function () {
                window.setTimeout(function () {
                    googleMapsLoaded(selector);
                }, 300);
            });
        }





