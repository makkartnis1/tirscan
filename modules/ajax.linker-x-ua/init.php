<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Created by LINKeR on 21.04.16.
 */

Route::set('linker/ajax/admin', 'ajax/admin/<controller>(/<method>)')
	->defaults(array(
		'directory'     => 'Ajax_Admin',
		'action'        => 'index'
	));
Route::set('linker/ajax/front', 'ajax/front/<controller>(/<method>)')
	->defaults(array(
		'directory'     => 'Ajax_Frontend',
		'action'        => 'index'
	));