<?php

/**
 * Class Ajax Екземпляр від якого має походити кожен контроллер типу аякс
 * Created by LINKeR on 21.04.16.
 */ 
abstract class Ajax extends Controller{

    public static $ajax_pligin_js               = '/assets/js/frontend/ajax.sys/controllerAjax.0.85.js';
    public static $ajax_pligin_init             = '/assets/js/frontend/ajax.sys/init.js';
    public static $dependent_from_toastr_js     = '/assets/js/frontend/ajax.toastr/toastr.js';
    public static $dependent_from_toastr_css    = '/assets/js/frontend/ajax.toastr/toastr.css';

    protected function db_exception($msg, $type, $key = 'PRIMARY'){

        switch(strtolower($type)){
            case('pk-duplicate'):
                $rgxp = '/duplicate.+for key.+'.$key.'.*/i';
                preg_match($rgxp, $msg, $matches);
                return (count($matches) !== 0);

        }

    }

    private $_data = array(
        'msg'   => array(
            'error'     => array(),
            'success'   => array(),
        ),
        'data'  =>array(),
    );

    protected $ignored_methods = array(
        'before', 'after', 'action_index'       // перелік заборонених методів
    );

    public $data;
    public $post;

    public $debug = false;   // true === будеш бачити стандартні помилки кохани. TODO :: ПЕРЕРОБИТИ ПІД ~KOHANA::ENVIRONMENT
    protected $run = true;  //  дозвіл на виконнання методу


    public function before(){

        $this->data = &$this->_data['data'];
        $this->post = $this->request->post('data');

    }

    public function action_index(){

        if($this->run){

            ($this->debug) ? $this->dirtyMode() : $this->clearMode();

        }

    }

    public function after(){
        $this->response->headers('Content-Type', 'application/json');
        $this->response->body(json_encode($this->_data));
    }

    protected function _init_paging($limit_items, $pagination_step){

        $page = (int) Arr::get($this->post,'page',1);
        $limit = $limit_items;
        $offset = $offset = ($page - 1) * $limit;

        $this->data['pages'] = ceil( $this->data['count'] / $limit );
        $this->data['current'] = $page;
        $this->data['step'] = $pagination_step;

        if($this->data['pages'] <= $page){

            $page = $this->data['current'] = $this->data['pages'];
            $offset = $offset = ($page - 1) * $limit;

        }

        return [$limit, $offset];

    }

    public static function initScript($routeName,$controller){

        $view = View::factory('admin/script/konjax_init', array('url'    =>  Route::url('konjax_', array('controller' => $controller) )));

        return $view;

    }
    
    public static function adminURL($controller, $method){ return Route::url('linker/ajax/admin', array('controller'=>(string) $controller, 'method'=> (string) $method)); }
    public static function frontURL($controller, $method){ return Route::url('linker/ajax/front', array('controller'=>(string) $controller, 'method'=> (string) $method)); }

    //  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
    //  <  getters, checkers

    protected function getAction(){

        return $this->request->param('method');

    }

    protected function checkMethod(){

        if(method_exists($this, $this->getAction())){

            $reflection = new ReflectionMethod($this, $this->getAction());

            //                      PUBLIC                      NOT STATIC                          ALLOWED

            return ( $reflection->isPublic() AND !$reflection->isStatic()  AND (!in_array( strtolower($this->getAction()), $this->ignored_methods )) );

        }else{
            return false;
        }
    }

    protected function dirtyMode(){

        $this->{$this->getAction()}();

    }

    protected function clearMode(){
        if ( ($this->getAction() !== null) AND (strlen($this->getAction()) > 0) AND $this->checkMethod() ){

            try{

                $this->dirtyMode();

            }catch (Exception $e){

                $this->add_error('CATCHED ERROR DURING METHOD EXECUTION: "'. $this->getAction() .'"');
                $this->response->status(500); //INTERNAL SERVER ERROR

            }

        } else {

            $this->add_error('METHOD NOT ALLOWED');
            $this->response->status(405); //METHOD NOT ALLOWED

        }
    }

    protected function checkData(){

        $args = func_get_args();

        foreach($args as $arg){

            $data = $this->post;

            if(!array_key_exists($arg, $data)){

                $this->add_error('INCORRECT DATA RECEIVED');
                $this->response->status(409);

                return false;
            }

        }

        return true;
    }

    //  getters, checkers    >
    //  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -

    protected function add_success($msg){

        $this->_data['msg']['success'][] = $msg;

    }
    protected function add_error($msg){

        $this->_data['msg']['error'][] = $msg;

    }

    protected function has_errors(){

        return ( count( $this->_data['msg']['error'] ) > 0 );

    }
    
    

}