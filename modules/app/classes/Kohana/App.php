<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_App extends App {

    public function getCurrentEnvironment() {
        return Kohana::$environment;
    }

    public function loadConfiguration($name) {
        return (array) Kohana::$config->load('app/' . $name);
    }

}