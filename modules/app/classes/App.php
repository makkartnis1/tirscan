<?php defined('SYSPATH') or die('No direct script access.');

abstract class App {

    public static $_application_class = null;

    /** #SINGLETON */

    private static $_instance = null;

    /**
     * @return App
     * @throws Exception
     */
    public static function get() {
        if (self::$_application_class === null) {
            throw new Exception("Вкажіть App::\$_application_class перед використанням App::get() у app/init.php");
        }

        if (self::$_instance === null) {
            self::$_instance = new self::$_application_class();
        }

        return self::$_instance;
    }

    public static function component($name) {
        return self::get()->getComponent($name);
    }

    /** /SINGLETON */

    protected $_components   = [];
    protected $_environments = [];

    protected $_current_specification;
    protected $_specification = [];

    abstract public function getCurrentEnvironment();

    abstract public function loadConfiguration($name);

    public function getComponent($name) {
        if (!array_key_exists($name, $this->_components)) {
            throw new Exception("Компонент '{$name}' не задано! Перевірте конфігураційний файл поточної специфікації 'app/specifications/{$this->_current_specification}.php'");
        }

        return $this->_components[$name];
    }

    public function loadSpecification($environments) {
        $current_environment = $this->getCurrentEnvironment();
        $specification       = (isset($environments[$current_environment]))
            ? $environments[$current_environment]
            : $environments['default'];

        $this->_specification         = $this->loadConfiguration('specifications/' . $specification);
        $this->_current_specification = $specification;
    }

    protected function createComponent($specification, $parent_component_name) {
        try {
            $class = new ReflectionClass($specification['class']);
            $args  = (array_key_exists('__construct', $specification))
                ? $specification['__construct']
                : [];

            $new_component = $class->newInstanceArgs($args);
            unset($specification['class']);
            unset($specification['__construct']);

            foreach ($specification as $key => $value) {
                if (is_array($value) && array_key_exists('class', $value)) {
                    $new_component->$key = $this->createComponent($value, $parent_component_name);
                }
                else {
                    $new_component->$key = $value;
                }
            }
        }
        catch (Exception $e) {
            throw new Exception("Не вдалось створити компонент '{$parent_component_name}'! Перевірте конфігураційний файл поточної специфікації 'app/specifications/{$this->_current_specification}.php'");
        }

        return $new_component;
    }

    protected function assembleComponents() {
        foreach ($this->_specification['components'] as $component => $specification) {
            $this->_components[$component] = $this->createComponent($specification, $component);
        }
    }

    public function init() {
        $this->_environments = $this->loadConfiguration('environments');
        $this->loadSpecification($this->_environments);

        $this->assembleComponents();
    }

    final private function __construct() {
        $this->init();
    }

    final private function __clone() {
    }

}