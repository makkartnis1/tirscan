-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Час створення: Чрв 22 2016 р., 14:06
-- Версія сервера: 5.5.44-MariaDB-1~wheezy
-- Версія PHP: 5.4.41-0+deb7u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `tirscan_site`
--

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Black_List`
--

CREATE TABLE IF NOT EXISTS `Frontend_Black_List` (
  `id` int(11) NOT NULL,
  `companyID` int(11) NOT NULL,
  `fromUserID` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Cargo`
--

CREATE TABLE IF NOT EXISTS `Frontend_Cargo` (
  `ID` int(10) unsigned NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `cargoType` varchar(256) NOT NULL,
  `transportTypeID` int(10) unsigned DEFAULT NULL,
  `volume` decimal(12,3) DEFAULT NULL,
  `weight` decimal(12,3) DEFAULT NULL,
  `carCount` int(10) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) DEFAULT NULL,
  `sizeY` decimal(12,3) DEFAULT NULL,
  `sizeZ` decimal(12,3) DEFAULT NULL,
  `priceID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `onlyTransporter` enum('yes','no') NOT NULL,
  `status` enum('active.have-no-proposition','active.have-confirmed','active.have-not-confirmed','completed','outdated') NOT NULL DEFAULT 'active.have-no-proposition',
  `parser` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28981 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Cargo_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Cargo_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Comments`
--

CREATE TABLE IF NOT EXISTS `Frontend_Comments` (
  `ID` int(10) unsigned NOT NULL,
  `text` varchar(1000) NOT NULL,
  `companyID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cargoID` int(10) unsigned DEFAULT NULL,
  `transportID` int(10) unsigned DEFAULT NULL,
  `rating` enum('positive','negative') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Companies`
--

CREATE TABLE IF NOT EXISTS `Frontend_Companies` (
  `ID` int(10) unsigned NOT NULL,
  `approvedByAdmin` enum('no','yes') NOT NULL DEFAULT 'no',
  `trustableMark` enum('no','yes') NOT NULL,
  `registered` datetime NOT NULL,
  `typeID` int(10) unsigned NOT NULL,
  `ipn` varchar(100) NOT NULL,
  `ownershipTypeID` int(11) NOT NULL,
  `primaryLocaleID` int(10) unsigned NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phoneStationary` varchar(45) DEFAULT NULL,
  `phoneCodeID` int(11) NOT NULL,
  `phoneStationaryCodeID` int(11) DEFAULT NULL,
  `foundationDate` date DEFAULT NULL,
  `parser` varchar(30) DEFAULT NULL,
  `url` varchar(200) NOT NULL,
  `rating` decimal(3,2) unsigned NOT NULL DEFAULT '1.00'
) ENGINE=InnoDB AUTO_INCREMENT=6212 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Locales` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `realAddress` text,
  `companyID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Ownership_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Ownership_Types` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_Ownership_Types`
--

INSERT INTO `Frontend_Company_Ownership_Types` (`ID`, `name`, `localeName`) VALUES
(1, 'ТзОВ', 'tzov'),
(2, 'ПП', 'pp'),
(3, 'ФОП', 'fop'),
(4, 'СПД', 'spd'),
(5, 'ФЛ-П', ''),
(6, 'ЧП', ''),
(7, 'ФЛП', ''),
(8, 'ООО', ''),
(9, 'ТОВ', ''),
(10, 'ФО-П', ''),
(11, 'T.E.C.', ''),
(12, 'МЧП', ''),
(13, 'ДТЕП', ''),
(14, 'ЧП"ТА', ''),
(15, 'ПТЕП', ''),
(16, 'Transport', ''),
(17, 'ОАО', ''),
(18, 'МП', ''),
(19, 'ЗГО', ''),
(20, 'ПрАТ', ''),
(21, 'Леонідова', ''),
(22, 'ЧПКФ', ''),
(23, 'ЧАО', ''),
(24, 'ДП', ''),
(25, 'ПВКП', ''),
(26, 'ЧПФ', ''),
(27, 'ЧФ', ''),
(28, 'АСОЦІАЦІЯ', ''),
(29, 'ПКФ', '');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Partners`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Partners` (
  `ID` int(10) unsigned NOT NULL,
  `company1ID` int(10) unsigned NOT NULL,
  `company2ID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Company_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_Company_Types` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Company_Types`
--

INSERT INTO `Frontend_Company_Types` (`ID`, `name`, `localeName`) VALUES
(1, 'Експедитор', 'company.type.expeditor'),
(2, 'Транспорт', 'company.type.transport'),
(3, 'Вантажотримач', 'company.type.cargo');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Complaints`
--

CREATE TABLE IF NOT EXISTS `Frontend_Complaints` (
  `ID` int(10) unsigned NOT NULL,
  `message` varchar(1000) NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complainCompanyID` int(10) unsigned NOT NULL,
  `status` enum('readed','unread') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблиця скарг';

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Currencies`
--

CREATE TABLE IF NOT EXISTS `Frontend_Currencies` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `code` char(3) NOT NULL,
  `status` enum('active','unactive') NOT NULL,
  `UAHRate` decimal(12,2) unsigned NOT NULL,
  `localeName` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Currencies`
--

INSERT INTO `Frontend_Currencies` (`ID`, `name`, `code`, `status`, `UAHRate`, `localeName`) VALUES
(1, 'Гривня', 'грн', 'active', '1.00', 'UAN'),
(2, 'Долар', '$', 'active', '0.00', 'USD'),
(3, 'Рубль', 'руб', 'active', '0.00', 'RUB'),
(4, 'Євро', '€', 'active', '0.00', 'EUR');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Dialogs`
--

CREATE TABLE IF NOT EXISTS `Frontend_Dialogs` (
  `id` int(10) unsigned NOT NULL,
  `transportID` int(10) unsigned DEFAULT NULL,
  `cargoID` int(10) unsigned DEFAULT NULL,
  `tenderID` int(10) unsigned DEFAULT NULL,
  `theme` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fromAdminID` int(10) unsigned DEFAULT NULL,
  `toAdminID` int(10) unsigned DEFAULT NULL,
  `toUserID` int(11) unsigned DEFAULT NULL,
  `fromUserID` int(11) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL,
  `deleted` enum('true','false') CHARACTER SET utf8 NOT NULL DEFAULT 'false'
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Dialogs_Unread`
--

CREATE TABLE IF NOT EXISTS `Frontend_Dialogs_Unread` (
  `ID` int(11) NOT NULL,
  `dialogID` int(11) NOT NULL,
  `toUserID` int(11) unsigned DEFAULT NULL,
  `toAdminID` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Feedback`
--

CREATE TABLE IF NOT EXISTS `Frontend_Feedback` (
  `ID` int(11) NOT NULL,
  `message` text NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('unread','read') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Filter_Templates`
--

CREATE TABLE IF NOT EXISTS `Frontend_Filter_Templates` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(300) NOT NULL,
  `query` varchar(10000) NOT NULL,
  `type` enum('transport','cargo') NOT NULL,
  `lastUpdateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastShowTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bufferCount` int(11) unsigned NOT NULL DEFAULT '0',
  `userID` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Geo`
--

CREATE TABLE IF NOT EXISTS `Frontend_Geo` (
  `ID` int(10) unsigned NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `placeID` varchar(45) NOT NULL,
  `parentID` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL,
  `type` enum('country','region','city') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4356 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Geo`
--

INSERT INTO `Frontend_Geo` (`ID`, `lat`, `lng`, `placeID`, `parentID`, `level`, `type`) VALUES
(3377, 50.619900, 26.251617, 'ChIJ14l8LapsL0cRhOTE26uBdAI', 3378, 3, 'city'),
(3378, 50.619900, 26.251617, 'ChIJERyljUNML0cRIED2iIQGAQE', 3379, 2, 'region'),
(3379, 48.379433, 31.165581, 'ChIJjw5wVMHZ0UAREED2iIQGAQA', NULL, 1, 'country'),
(3380, 50.747234, 25.325382, 'ChIJXZaFoeuZJUcRbLiNIqJ0UtI', 3381, 3, 'city'),
(3381, 50.747234, 25.325382, 'ChIJBYI5hgM0JEcRQED2iIQGAQE', 3379, 2, 'region'),
(3382, 49.993500, 36.230385, 'ChIJiw-rY5-gJ0ERCr6kGmgYTC0', 3383, 3, 'city'),
(3383, 49.993500, 36.230385, 'ChIJAfZeLutYJ0ERoEH2iIQGAQE', 3379, 2, 'region'),
(3384, 51.498199, 31.289351, 'ChIJlzXucYlI1UYRtThg59NIIyo', 3385, 3, 'city'),
(3385, 51.498199, 31.289351, 'ChIJT7SGL2DAKkERMEH2iIQGAQE', 3379, 2, 'region'),
(3386, 50.338223, 30.893927, 'ChIJ0cV-Zvro1EAR2nDYSV0sCmk', 3387, 3, 'city'),
(3387, 50.052952, 30.766712, 'ChIJ94pF_F3O1EARB10ge68K4KY', 3379, 2, 'region'),
(3388, 50.254650, 28.658667, 'ChIJXTX6K6NkLEcRKeK52aPSSvE', 3389, 3, 'city'),
(3389, 50.254650, 28.658667, 'ChIJC9KIyxTxK0cRoED2iIQGAQE', 3379, 2, 'region'),
(3390, 46.635418, 32.616867, 'ChIJZ_mbXaIaxEAR_iExjoxHpfM', 3391, 3, 'city'),
(3391, 46.635418, 32.616867, 'ChIJh1TvTkORw0ARQEH2iIQGAQE', 3379, 2, 'region'),
(3392, 49.824787, 30.405336, 'ChIJ8-SmUxdl00ARTFLygq9ZIOw', 3387, 3, 'city'),
(3393, 49.444431, 32.059769, 'ChIJf5dkYIZL0UAR2LXLqSPn3Pg', 3394, 3, 'city'),
(3394, 49.444431, 32.059769, 'ChIJg26lOnZL0UARiu-AtokbRyg', 3379, 2, 'region'),
(3395, 50.907700, 34.798100, 'ChIJYaRz_CACKUERKGyDazb2dNs', 3396, 3, 'city'),
(3396, 50.907700, 34.798100, 'ChIJYaRz_CACKUER0KUajIxyN1A', 3379, 2, 'region'),
(3397, 49.422981, 26.987133, 'ChIJixe7REMGMkcRZMnZJDsL89k', 3398, 3, 'city'),
(3398, 49.422981, 26.987133, 'ChIJtbpfwCz5LUcRkED2iIQGAQE', 3379, 2, 'region'),
(3399, 50.450100, 30.523399, 'ChIJBUVa4U7P1EAR_kYBF9IxSXY', 3379, 3, 'city'),
(3400, 48.620800, 22.287884, 'ChIJ2eO2RLkZOUcRPCoEMAGupg0', 3401, 3, 'city'),
(3401, 48.620800, 22.287884, 'ChIJDQKKqwsTOUcRLtC2-ZaZ5Fk', 3379, 2, 'region'),
(3402, 49.588268, 34.551418, 'ChIJa7rmJeQl2EARgsrkud9rPs8', 3403, 3, 'city'),
(3403, 49.588268, 34.551418, 'ChIJ5cdqFjh110ARcEH2iIQGAQE', 3379, 2, 'region'),
(3404, 50.367836, 33.979671, 'ChIJ96NVLLvVKUERGtDRtB-DHwg', 3403, 3, 'city'),
(3405, 46.975033, 31.994583, 'ChIJ1RNy-4nLxUARgFbgufo5Dpc', 3406, 3, 'city'),
(3406, 46.975033, 31.994583, 'ChIJydRVsbqaxUARLq1R8Q3RgpM', 3379, 2, 'region'),
(3407, 47.910484, 33.391785, 'ChIJe6tUMeDf2kARbhhrfRc6-rA', 3408, 3, 'city'),
(3408, 48.464718, 35.046185, 'ChIJoevoYl_c20ARlxgBvjfDzlk', 3379, 2, 'region'),
(3409, 47.642426, 35.296234, 'ChIJo-_0Omv53EARShPnBnmm8vI', 3410, 3, 'city'),
(3410, 47.838799, 35.139568, 'ChIJ0U8jLBVn3EARjGp5k7-Oh-E', 3379, 2, 'region'),
(3411, 47.472679, 36.262550, 'ChIJG5irSSab3UARmNVFco-DQQ0', 3410, 3, 'city'),
(3412, 50.317223, 29.054144, 'ChIJeTaysimCLEcRcbTGl-C8QdA', 3389, 3, 'city'),
(3413, 50.227722, 30.654125, 'ChIJM-_twYKV1EARpH-Jii1IqXo', 3387, 3, 'city'),
(3414, 44.086510, 46.488667, 'ChIJzQ9i-N9EUkARhrXwBPUCkHQ', 3415, 3, 'city'),
(3415, 42.143188, 47.094978, 'ChIJJ7wfNCRiTkARlWe8YPJG1dE', 3416, 2, 'region'),
(3416, 61.524010, 105.318756, 'ChIJ-yRniZpWPEURE_YRZvj9CRQ', NULL, 1, 'country'),
(3417, 46.295223, 30.648087, 'ChIJ4zSCoPzLx0ARkVtoNc1oMjg', 3418, 3, 'city'),
(3418, 46.484585, 30.732599, 'ChIJaRSOmoExxkARxdd-tWjwax8', 3379, 2, 'region'),
(3419, 48.045124, 30.888432, 'ChIJoegIajlIzkARd6n2TbDlHcs', 3406, 3, 'city'),
(3420, 50.517410, 30.132114, 'ChIJeVdJDCg3K0cRoBQTN5eaxY0', 3387, 3, 'city'),
(3421, 48.507935, 32.262318, 'ChIJZxtECCdD0EAR2OigSUHFKOg', 3422, 3, 'city'),
(3422, 48.507935, 32.262318, 'ChIJ_Y7vgK9C0EARlBkNFDXFp9g', 3379, 2, 'region'),
(3423, 49.839684, 24.029716, 'ChIJV5oQCXzdOkcR4ngjARfFI0I', 3424, 3, 'city'),
(3424, 49.839684, 24.029716, 'ChIJb5xjPmndOkcRYj26h4iMDWM', 3379, 2, 'region'),
(3425, 48.000614, 23.696632, 'ChIJodiycW2XN0cRaMhTjWSl1WE', 3401, 3, 'city'),
(3426, 49.553516, 25.594767, 'ChIJdc6CS602MEcR6FSx7UekhMQ', 3427, 3, 'city'),
(3427, 49.553516, 25.594767, 'ChIJZdKe1ltLMEcRUED2iIQGAQE', 3379, 2, 'region'),
(3428, 46.114925, 32.910221, 'ChIJkWlqLjJqwUARYJhcjZp2YLM', 3391, 3, 'city'),
(3429, 46.482525, 30.723309, 'ChIJQ0yGC4oxxkARbBfyjOKPnxI', 3418, 3, 'city'),
(3430, 54.010902, 38.296307, 'ChIJNYvM_a-dNkERvvCIzpRLqic', 3431, 3, 'city'),
(3431, 54.163769, 37.564953, 'ChIJc7c_RiwJNEERSxxmDujG1aA', 3416, 2, 'region'),
(3432, 47.838799, 35.139568, 'ChIJA7uF-j1n3EARSj9NB9lcZ34', 3410, 3, 'city'),
(3433, 48.173462, 23.297249, 'ChIJS-oU1kIpOEcRojgYcgYxiao', 3401, 3, 'city'),
(3434, 46.619930, 32.707268, 'ChIJkxeVKvUFxEARr0KCri5OWIQ', 3391, 3, 'city'),
(3435, 48.146351, 23.030212, 'ChIJfWm-Il46OEcRW-fiEqgMNfU', 3401, 3, 'city'),
(3436, 50.063766, 29.904968, 'ChIJtWSR9h9V00AR0Prd_F6OZmM', 3387, 3, 'city'),
(3437, 50.457325, 29.812654, 'ChIJy-3lNtlAK0cRqdrprDayZAQ', 3387, 3, 'city'),
(3438, 49.881248, 24.076715, 'ChIJhSxxX5TcOkcRtBkHRQBJseY', 3424, 3, 'city'),
(3439, 55.461861, 29.987444, 'ChIJqc9SC0uvxUYR2hjPwC2rlZk', 3440, 3, 'city'),
(3440, 55.295982, 28.758364, 'ChIJ59aqCSbiz0YRjtiFes6qwec', 3441, 2, 'region'),
(3441, 53.709808, 27.953388, 'ChIJgUit4oQl2kYREIzsgdGhAAA', NULL, 1, 'country'),
(3442, 48.017151, 23.572468, 'ChIJ433JsFmQN0cRiqu-oNsmnDw', 3401, 3, 'city'),
(3443, 49.605328, 34.342060, 'ChIJi6DvAHCH10ARodJLPIFWqfg', 3403, 3, 'city'),
(3444, 49.948441, 23.530294, 'ChIJMfEMl5Q9O0cRZgLJ1OsTMAw', 3424, 3, 'city'),
(3445, 48.696716, 26.582537, 'ChIJd7V6-HO4M0cRN1H35EI7rII', 3398, 3, 'city'),
(3446, 48.738968, 37.584351, 'ChIJm5vqwKSX30AR7HgmWcHe_Ww', 3447, 3, 'city'),
(3447, 48.015884, 37.802849, 'ChIJe9U9ohib30ARJEyiBAomT7o', 3379, 2, 'region'),
(3448, 48.769928, 30.215441, 'ChIJ17G7ibsN0kARUZkv1IAsMUg', 3394, 3, 'city'),
(3449, 48.362709, 25.775379, 'ChIJU_dCt1oANEcRYY9b159JzVk', 3450, 3, 'city'),
(3450, 48.291683, 25.935217, 'ChIJH2F-TOwINEcR3I8UhwBnFyw', 3379, 2, 'region'),
(3451, 48.719200, 32.677097, 'ChIJKUAabc-N0EARCF0i7ttfnKg', 3422, 3, 'city'),
(3452, 48.523117, 34.613682, 'ChIJ7d-kDjba20AR-IUoSyi549A', 3408, 3, 'city'),
(3453, 49.782379, 32.683113, 'ChIJm2kMLT3t1kARU7TAC-Lf0aU', 3403, 3, 'city'),
(3454, 50.588470, 32.387585, 'ChIJY6zcHkiT1UAR6qvpkGodeKs', 3385, 3, 'city'),
(3455, 47.097134, 37.543365, 'ChIJK1jnvqfm5kARzrV1CjAY0aU', 3447, 3, 'city'),
(3456, 47.382477, 34.993843, 'ChIJ3TaCUjrc3EARwJRLcmwV_aI', 3410, 3, 'city'),
(3457, 48.887592, 30.704412, 'ChIJEyWPU-0j0kARrHnNSJHFYVQ', 3394, 3, 'city'),
(3458, 49.520714, 23.206551, 'ChIJCUjL4AilO0cRoK42ft7wqfw', 3424, 3, 'city'),
(3459, 49.910660, 28.590031, 'ChIJH02eRYf5LEcRmWnGpwzX1P4', 3389, 3, 'city'),
(3460, 49.714935, 28.836365, 'ChIJl6_mq-PfLEcROqeeWQZJKac', 3461, 3, 'city'),
(3461, 49.233082, 28.468218, 'ChIJiy6tDP5UzUARsED2iIQGAQE', 3379, 2, 'region'),
(3462, 48.292080, 25.935837, 'ChIJBc324n8INEcRem15WfaWs8U', 3450, 3, 'city'),
(3463, 48.480122, 34.021225, 'ChIJu6i0BnFK2kARcLSKbQimPmo', 3408, 3, 'city'),
(3464, 46.811722, 33.490196, 'ChIJJ_pDErycw0ARSaq2pvi38p0', 3391, 3, 'city'),
(3465, 50.177090, 30.319637, 'ChIJdxka4Uql1EARLwfu5DopyVQ', 3387, 3, 'city'),
(3466, 49.661793, 32.047710, 'ChIJAxfqk_6l1kARdLR0DL1lDwI', 3394, 3, 'city'),
(3467, 48.252159, 25.197380, 'ChIJiXAYVZq-NkcRtgxenkcNL1k', 3450, 3, 'city'),
(3468, 48.113255, 32.765869, 'ChIJg6VSDvO82kARqi1SXswtkzk', 3422, 3, 'city'),
(3469, 50.692020, 28.637878, 'ChIJb6WDp6vtK0cRPoAlXB76aYQ', 3389, 3, 'city'),
(3470, 47.833958, 32.833923, 'ChIJyzd3DJTM2kAR28ra-2YwBY4', 3406, 3, 'city'),
(3471, 46.661827, 34.992500, 'ChIJ4URTZSGXwkARXY119VfXBTQ', 3410, 3, 'city'),
(3472, 55.688301, 37.399578, 'ChIJ1TsyRQ1OtUYR2rJlYQIjbNo', 3473, 3, 'city'),
(3473, 55.340397, 38.291763, 'ChIJ2cXDsDGySkERQvLeO8CzDJE', 3416, 2, 'region'),
(3474, 52.870686, 48.605972, 'ChIJLbc9Lz4FaUERwhmoT_MuImw', 3475, 3, 'city'),
(3475, 53.418385, 50.472553, 'ChIJu61Uz18ZZkERmE1NRiHorHI', 3416, 2, 'region'),
(3476, 49.373051, 35.461353, 'ChIJRdukUMX12EAROzvtEWtX4gI', 3383, 3, 'city'),
(3477, 49.115162, 33.280155, 'ChIJyYHSSepM10ARHmOND1k2-DI', 3422, 3, 'city'),
(3478, 49.276031, 23.510557, 'ChIJRbeALq1JOkcR1HJifj-sRfc', 3424, 3, 'city'),
(3479, 49.064831, 25.381716, 'ChIJ4dL4Me0DMUcRFYzoeFQfKgY', 3427, 3, 'city'),
(3480, 49.962727, 33.605331, 'ChIJbTmCWQmz10ARtDentwQZsZo', 3403, 3, 'city'),
(3481, 48.369476, 34.457333, 'ChIJoXfORTDH20ARyju_8me68iI', 3408, 3, 'city'),
(3482, 48.994236, 24.186729, 'ChIJM_KIzhmnMEcRST9tWmbtX1s', 3483, 3, 'city'),
(3483, 48.922634, 24.711117, 'ChIJHTiwNGzBMEcRn1rIHA4suJc', 3379, 2, 'region'),
(3484, 51.209019, 24.698025, 'ChIJo69Juf86JEcRuZAymwpYVxk', 3381, 3, 'city'),
(3485, 48.853199, 37.605301, 'ChIJzfwgVc2a30ARYnfShIcrj98', 3447, 3, 'city'),
(3486, 49.796799, 30.131084, 'ChIJnXsiNz5C00AR6MYsWPgR44Q', 3387, 3, 'city'),
(3487, 48.420040, 37.052292, 'ChIJ2RgO9-JN3kARAG3r1XmvnBM', 3447, 3, 'city'),
(3488, 50.511082, 30.790899, 'ChIJ7xzVfdHb1EARiZaO_Nyy6_Y', 3387, 3, 'city'),
(3489, 48.598667, 37.998035, 'ChIJp8Uk9xvm30ARwu6_7FXimKM', 3447, 3, 'city'),
(3490, 48.989758, 37.804996, 'ChIJWUEaZmJ4IEERDsHHNzvWY4E', 3447, 3, 'city'),
(3491, 50.339001, 30.422234, 'ChIJuQeULC_I1EARDoSPCra6XpE', 3387, 3, 'city'),
(3492, 51.240944, 33.205051, 'ChIJnxaWSPNJKkERsunE02dKCT4', 3396, 3, 'city'),
(3493, 45.101009, -0.411627, 'ChIJg1fmwk0kAEgRvq8zQ0hlOEQ', 3494, 3, 'city'),
(3494, 45.708717, 0.626891, 'ChIJ3-hEGv1T_kcR9ODuTtX6now', 3495, 2, 'region'),
(3495, 46.227638, 2.213749, 'ChIJMVd4MymgVA0R99lHx5Y__Ws', NULL, 1, 'country'),
(3496, 48.922634, 24.711117, 'ChIJHTiwNGzBMEcRwkGe5ZQj09Y', 3483, 3, 'city'),
(3497, 49.233082, 28.468218, 'ChIJiWRaGWVbLUcR_nTd7lnh1Ms', 3461, 3, 'city'),
(3498, 49.385029, 27.624226, 'ChIJD0HQpGuGLUcRdjbKV_yPNsg', 3398, 3, 'city'),
(3499, 48.523945, 37.707581, 'ChIJPVQ1OCPE30ARhvd60ZfFBP0', 3447, 3, 'city'),
(3500, 56.244099, 43.435181, 'ChIJ63XVIT4lTkERqBlUJDFmrAU', 3501, 3, 'city'),
(3501, 55.799515, 44.029678, 'ChIJ7wrHyEc5UEERonVMLoqQ9Vk', 3416, 2, 'region'),
(3502, 48.244122, 28.276522, 'ChIJnaft3RTDzEARpJjEK7_81Fc', 3461, 3, 'city'),
(3503, 49.049683, 33.228470, 'ChIJzUU6Ilyy0EARcolcKrTJDew', 3422, 3, 'city'),
(3504, 47.072235, 29.395159, 'ChIJd2k3ESQVyUARnDxxDt8mdO0', 3505, 2, 'city'),
(3505, 47.411633, 28.369884, 'ChIJoWm3KDZ8yUARy6xT36wZgSU', NULL, 1, 'country'),
(3506, 47.535709, 29.456230, 'ChIJpU7x9TCpzkARJtasIYkJiBo', 3418, 3, 'city'),
(3507, 49.547985, 36.418945, 'ChIJCeRsTfheJ0ERGOTN8AcEzkA', 3383, 3, 'city'),
(3508, 46.855022, 35.358700, 'ChIJI-XtJOKxwkARSpGi82_h4aE', 3410, 3, 'city'),
(3509, 49.265610, 23.843134, 'ChIJ1zCrzR1qOkcRg12o7NBll9s', 3424, 3, 'city'),
(3510, 48.447693, 22.700714, 'ChIJYy1TaJmrOUcRPHStt13itwo', 3401, 3, 'city'),
(3511, 51.345654, 26.601982, 'ChIJwfownoiiKEcRiTQY82yUYoo', 3378, 3, 'city'),
(3512, 49.367424, 23.936481, 'ChIJ8abtRH9nOkcRirznRUB-ZD0', 3424, 3, 'city'),
(3513, 47.441471, 35.286728, 'ChIJXaHxAeQd3UARPnxU2SGgFAs', 3410, 3, 'city'),
(3514, 50.737175, 24.165880, 'ChIJxRUpYcHzJEcRHEV8navik_0', 3381, 3, 'city'),
(3515, 46.675568, 30.999443, 'ChIJQYHhZ_4WxkARnHDYHzGstAw', 3418, 3, 'city'),
(3516, 48.010445, 33.574341, 'ChIJh32MY38e20AR_Ov1WbxUskc', 3408, 3, 'city'),
(3517, 46.854626, 30.074944, 'ChIJpzRDpTvpyEARwLYE90ajlqg', 3418, 3, 'city'),
(3518, 46.485874, 32.972912, 'ChIJOVEPigr7w0ARPI224yxTujI', 3391, 3, 'city'),
(3519, 47.758579, 29.533915, 'ChIJh_DM1h4gzEARE_id-25M9ik', 3418, 3, 'city'),
(3520, 49.047882, 31.557125, 'ChIJ6Zouey5y0UAREe00WUHsJdk', 3394, 3, 'city'),
(3521, 48.598534, 35.314148, 'ChIJj1Ot8eRP2UARHqDnhM0f9LE', 3408, 3, 'city'),
(3522, 51.326832, 28.802896, 'ChIJ4ap0ufiHKUcRCsP53TK-M8E', 3389, 3, 'city'),
(3523, 46.521095, 32.520126, 'ChIJjY0nwpACxEARSMABHoSLEao', 3391, 3, 'city'),
(3524, 49.155712, 23.867456, 'ChIJPbKAEqcUOkcR6n6dUkE5yfU', 3424, 3, 'city'),
(3525, 51.250000, 32.599998, 'ChIJr7jINFVfKkERybmaHjeHl40', 3385, 3, 'city'),
(3526, 43.407909, 40.017239, 'ChIJvcws5yzr9UARscPR_fqcjeU', 3527, 2, 'city'),
(3527, 43.001553, 41.023407, 'ChIJ_0KkE08vX0ARb5qYJeZSSJM', NULL, 1, 'region'),
(3528, 49.806026, 36.050346, 'ChIJdY-kBux3J0ERJI7PfBf-KCE', 3383, 3, 'city'),
(3529, 48.663311, 29.750780, 'ChIJ5b399qKIzUARmxd3Vb31VJk', 3461, 3, 'city'),
(3530, 50.593334, 28.447271, 'ChIJ_eLVY6X2K0cRVLktKx6ZVfE', 3389, 3, 'city'),
(3531, 49.551025, 27.938271, 'ChIJ67DXbESfLUcRJ_HZ4XghrIU', 3461, 3, 'city'),
(3532, 48.533478, 28.741636, 'ChIJlyuOjDIWzUARzZDs-sADB5Q', 3461, 3, 'city'),
(3533, 48.464718, 35.046185, 'ChIJj0YI_QPj20ARuhrB8tXzHAo', 3408, 3, 'city'),
(3534, 46.843319, 33.427364, 'ChIJ7b1m3wyDw0ARM8ahxUsM_t0', 3391, 3, 'city'),
(3535, 46.255947, 33.298965, 'ChIJ4QgZ26NiwUARm5Z7uc8h-no', 3391, 3, 'city'),
(3536, 47.560501, 31.336117, 'ChIJkz2E3pCdz0ART1f7vYt7Q-Y', 3406, 3, 'city'),
(3537, 48.638557, 29.910273, 'ChIJcR2GHLCLzUAR3hg1-9lUQY8', 3461, 3, 'city'),
(3538, 51.687176, 26.563822, 'ChIJU9uCy4UsJkcRzdhn5hvRvZ0', 3378, 3, 'city'),
(3539, 45.839111, 29.614576, 'ChIJP1Gyj8D3t0ARFXyBqa7GiYU', 3418, 3, 'city'),
(3540, 45.683842, 28.614311, 'ChIJ29uYYFyot0ARYV8fZIGbQEk', 3418, 3, 'city'),
(3541, 50.327930, 26.521109, 'ChIJdTEXEDsAL0cRwlrc6OpDnQw', 3378, 3, 'city'),
(3542, 48.890236, 36.308002, 'ChIJvx3Q4j4v30ARUjXa69F6Nac', 3383, 3, 'city'),
(3543, 49.705559, 37.602798, 'ChIJ18YfcFzXIEERBoZtlz9q1dI', 3383, 3, 'city'),
(3544, 49.476498, 27.018631, 'ChIJPdw0f5z4LUcR1vPTGFDquzM', 3398, 3, 'city'),
(3545, 50.307098, 31.464331, 'ChIJO3Hqs9xV1EARTgZPq0DoT9k', 3387, 3, 'city'),
(3546, 49.305233, 25.686226, 'ChIJT5WadZqxMUcRkfmiKh0dMQs', 3427, 3, 'city'),
(3547, 49.018650, 26.339781, 'ChIJT3QL56eAMUcRPNVd0f4-5EY', 3398, 3, 'city'),
(3548, 48.530491, 25.041204, 'ChIJ38v0fpXSNkcRAL-HQ7ip4sE', 3483, 3, 'city'),
(3549, 48.800610, 26.048895, 'ChIJI9sLT3NjMUcRBbhd-ML6owg', 3427, 3, 'city'),
(3550, 51.524010, 32.216843, 'ChIJ41_5CrDdKkERKqKUxShMGGs', 3385, 3, 'city'),
(3551, 48.980236, 31.002113, 'ChIJZ051f9wp0kAR0p9bkLFozsg', 3394, 3, 'city'),
(3552, 45.465073, 29.248032, 'ChIJc0pALFjVuUARruTbka5fR8U', 3418, 3, 'city'),
(3553, 47.409874, 32.441227, 'ChIJ57TNvcoaxUARDONQvW0Ybbw', 3406, 3, 'city'),
(3554, 49.788803, 33.730793, 'ChIJc_iOnC2k10AROCDHwCkjjHY', 3403, 3, 'city'),
(3555, 49.399651, 37.064285, 'ChIJwWTjiqSvIEERmyNFOuH73jo', 3383, 3, 'city'),
(3556, 47.892601, 35.372238, 'ChIJRRLEVJ9D3EARXseaCLgZFlo', 3410, 3, 'city'),
(3557, 48.282192, 37.184780, 'ChIJ_Yuq7_033kARfNVpEZSK2gc', 3447, 3, 'city'),
(3558, 49.016121, 38.375141, 'ChIJyVe4TJ4TIEERQ31WTKOBKQk', 3559, 3, 'city'),
(3559, 48.574039, 39.307816, 'ChIJ8zUQQaAY4EARuDtkU2fs3IQ', 3379, 2, 'region'),
(3560, 49.426113, 31.261852, 'ChIJBXbL4jrm00ARu8j5qpOIUI0', 3394, 3, 'city'),
(3561, 49.212147, 37.266502, 'ChIJYyZXLkieIEERdMzURKF21jc', 3383, 3, 'city'),
(3562, 49.065784, 33.410034, 'ChIJ3xgCm6tT10ARHFucngBOIEk', 3403, 3, 'city'),
(3563, 48.529449, 35.903255, 'ChIJ_0hLcDK43kARPadNhDFxWpY', 3408, 3, 'city'),
(3564, 46.361816, 33.532703, 'ChIJ3WPElszPw0ARBWzOMugEy-c', 3391, 3, 'city'),
(3565, 49.041428, 24.395039, 'ChIJJeHPK2-iMEcRDCILLBRzOsU', 3483, 3, 'city'),
(3566, 48.268131, 34.720253, 'ChIJBchJeOXs20ARDizXvzsan8Q', 3408, 3, 'city'),
(3567, 47.695309, 31.852333, 'ChIJQdTN0XKKz0AR9ptu4TG3dL0', 3406, 3, 'city'),
(3568, 49.952785, 32.138470, 'ChIJoUzX1FIm1EARZmS5c0__pRY', 3394, 3, 'city'),
(3569, 48.648212, 34.336891, 'ChIJN9K6868s2kARBr4Ujh2Xq4c', 3408, 3, 'city'),
(3570, 40.746941, 48.033257, 'ChIJ2xNVv_mvOUARMUmD1SBKclo', 3571, 2, 'city'),
(3571, 40.143105, 47.576927, 'ChIJ3x2iGtl8MEARCM2DPmtSyeY', NULL, 1, 'country'),
(3572, 51.083675, 28.400478, 'ChIJN1Y64LB8KUcRE46plyHIMVU', 3389, 3, 'city'),
(3573, 50.834137, 28.951231, 'ChIJe5AZwym_K0cRDZJ0g3qLu9o', 3389, 3, 'city'),
(3574, 46.189186, 29.146496, 'ChIJtZaI1sopyEARYBUVkxXPiIQ', 3418, 3, 'city'),
(3575, 50.769680, 29.260489, 'ChIJ339fBhqkK0cRt7-J6-pwYBQ', 3389, 3, 'city'),
(3576, 51.290649, 25.561283, 'ChIJq1qGhfR2JkcRZmjGllqt7Mc', 3381, 3, 'city'),
(3577, 47.947178, 30.340662, 'ChIJRbjCq8VgzkARviFexeZ7JFo', 3406, 3, 'city'),
(3578, 50.304501, 28.987272, 'ChIJW95N7z6BLEcRbogonNhTarE', 3389, 3, 'city'),
(3579, 49.969360, 24.609461, 'ChIJHQ93xeWzOkcR9qAPnnw01gY', 3424, 3, 'city'),
(3580, 47.835960, 36.844845, 'ChIJDYCa8Uj93UARuRaDeKw1Obk', 3447, 3, 'city'),
(3581, 51.670883, 33.907894, 'ChIJN49XgGLtK0ER4edkdLgr8_Y', 3396, 3, 'city'),
(3582, 49.715485, 23.905561, 'ChIJu7Ej2g_lOkcR1VEzs4IEpYw', 3424, 3, 'city'),
(3583, 49.274410, 25.131330, 'ChIJiYRrnFVYMEcRuj7vP84jEGQ', 3427, 3, 'city'),
(3584, 49.208801, 39.589699, 'ChIJu0cz1dd6H0ERZOUblqld6fE', 3559, 3, 'city'),
(3585, 49.464394, 36.858479, 'ChIJkzoJkCdTJ0ERRqTmLr_MECg', 3383, 3, 'city'),
(3586, 49.931553, 23.571066, 'ChIJo0TMP2Y9O0cRAhtKPEYZjE0', 3424, 3, 'city'),
(3587, 48.885120, 22.453997, 'ChIJR4m3Id1pOUcRBG6k1R9db2I', 3401, 3, 'city'),
(3588, 49.895645, 23.768724, 'ChIJtZ-2jc8hO0cReIlxJpOfeUo', 3424, 3, 'city'),
(3589, 48.483643, 25.162485, 'ChIJFwBU8YXLNkcRTNdmDuLCvKA', 3483, 3, 'city'),
(3590, 50.387177, 24.219946, 'ChIJ-6mlotXeJEcRhsBaeORTd-4', 3424, 3, 'city'),
(3591, 49.390911, 30.198820, 'ChIJt0U6-iMB00AR21tBqrAzcoo', 3387, 3, 'city'),
(3592, 48.672398, 25.496256, 'ChIJa3sRgXFHMUcRt6vi9MG_SbE', 3483, 3, 'city'),
(3593, 49.409939, 24.610601, 'ChIJL7Rcy79-MEcRyfiY12qqx1E', 3483, 3, 'city'),
(3594, 49.010883, 25.802650, 'ChIJpSfB8CwLMUcR8gtzlbHFTEQ', 3427, 3, 'city'),
(3595, 50.284233, 36.932941, 'ChIJpb9Bo8LwJkERzGq6vJhE0xY', 3383, 3, 'city'),
(3596, 50.071583, 31.450153, 'ChIJtRyQHN5C1EARkMzkPfFQJAk', 3387, 3, 'city'),
(3597, 46.773769, 36.803478, 'ChIJNwxfI9DK50AR_FAdI9nVZmo', 3410, 3, 'city'),
(3598, 46.332817, 30.092333, 'ChIJA1dEK7R2yEAR7o4TcSQr5RU', 3418, 3, 'city'),
(3599, 50.741508, 33.480953, 'ChIJz2xUtxjxKUERgcmBY86Nobg', 3396, 3, 'city'),
(3600, 49.587280, 25.665806, 'ChIJ8_xxZCsyMEcR7CgzIDCTGPU', 3427, 3, 'city'),
(3601, 53.881989, 24.137009, 'ChIJUWM2yNgo3kYR-wpXekkRA-U', 3602, 3, 'city'),
(3602, 53.659996, 25.344856, 'ChIJu5YcsjrW30YRtAIPeKwCpLc', 3441, 2, 'region'),
(3603, 51.120552, 35.035725, 'ChIJ2WgojNUhKUERGjnEjAoEZuM', 3396, 3, 'city'),
(3604, 49.448551, 28.522655, 'ChIJdxv26F0VLUcR6p4nufqu4UY', 3461, 3, 'city'),
(3605, 47.488789, 33.531712, 'ChIJD-NH4ii0xEARljbJasWan0Y', 3391, 3, 'city'),
(3606, 49.709381, 24.099731, 'ChIJQ-j5fKfuOkcRMrm5Srje0Do', 3424, 3, 'city'),
(3607, 50.953213, 28.645754, 'ChIJ0fDwWQ7bK0cRODv52PKUrlk', 3389, 3, 'city'),
(3608, 49.013008, 28.504219, 'ChIJI2NoGV-pMkcRBOMfTZDBMdg', 3461, 3, 'city'),
(3609, 50.183334, 28.549999, 'ChIJA3ltmvxmLEcR8hB5y0ZZ3gQ', 3389, 3, 'city'),
(3610, 48.220936, 30.015650, 'ChIJnYcxWB7ZzUARcJr-qaWOz_U', 3422, 3, 'city'),
(3611, 47.083332, 30.033333, 'ChIJA-9Vuc3ZyEAR2DbjRwE_Rsk', 3418, 3, 'city'),
(3612, 48.446423, 25.569910, 'ChIJb5QvL8dSMUcRqQMhuHkHeFs', 3483, 3, 'city'),
(3613, 31.200092, 29.918739, 'ChIJ0w9xJpHE9RQRuWvuKabN4LQ', 3614, 3, 'city'),
(3614, 26.820553, 30.802498, 'ChIJ6TZcw3aJNhQRRMTEJQmgRSw', NULL, 1, 'country'),
(3615, 46.751526, 33.367821, 'ChIJW-VT-fqPw0AR-Fld4_pJ3ec', 3391, 3, 'city'),
(3616, 48.616016, 35.398479, 'ChIJS9kEXvtO2UARBrQoKW_OE8E', 3408, 3, 'city'),
(3617, 49.665031, 30.989470, 'ChIJ8xGAAK6P00ARivwmk8Qd5qU', 3387, 3, 'city'),
(3618, 48.729492, 34.632404, 'ChIJM_vF6al-2UARgmMORUoFGoE', 3408, 3, 'city'),
(3619, 50.391899, 30.368267, 'ChIJM8wmjybK1EAR5CLzKWJ9lF8', 3387, 3, 'city'),
(3620, 46.178432, 34.806816, 'ChIJt_QjDNje6UARdAacI1WyXzw', 3391, 3, 'city'),
(3621, 51.548943, 33.386051, 'ChIJm5NCfmm7K0ER0BNHyzuHHCA', 3396, 3, 'city'),
(3622, 47.595116, 37.483143, 'ChIJbWygskU150AR8tnntN_duO0', 3447, 3, 'city'),
(3623, 47.246410, 35.709835, 'ChIJ47Ggj75r3UARY48o8fv5OUo', 3410, 3, 'city'),
(3624, 48.296211, 37.270004, 'ChIJy_KRsH423kAR_YZ4fO-SWOc', 3447, 3, 'city'),
(3625, 49.754684, 27.212158, 'ChIJ9_C0zZvgLUcRftM9C2PlJ38', 3398, 3, 'city'),
(3626, 50.109940, 30.629885, 'ChIJJZuNXGiX1EAR75RQAG5TlzE', 3387, 3, 'city'),
(3627, 49.173008, 27.035286, 'ChIJQzzlvPQUMkcRCU-AJwyI-uc', 3398, 3, 'city'),
(3628, 46.624020, 31.100065, 'ChIJS1JECHEVxkARGKj_YlbY_Jk', 3418, 3, 'city'),
(3629, 46.185452, 30.341492, 'ChIJUcAJETLdx0AReCaG53Cm9Go', 3418, 3, 'city'),
(3630, 50.848194, 24.325657, 'ChIJR7A8LtRYJEcRzCFFRTxsMvs', 3381, 3, 'city'),
(3631, 50.622765, 27.161446, 'ChIJebQ349zELkcRee-1YmakbH4', 3378, 3, 'city'),
(3632, 51.220757, 27.644220, 'ChIJeVzEG54bKUcRqnSCuUzwEjU', 3389, 3, 'city'),
(3633, 48.948177, 38.487877, 'ChIJF_xkgvIQIEERwhKRyz5fLNM', 3559, 3, 'city'),
(3634, 47.487091, 34.416737, 'ChIJYZCAz8C63EAR-6vR45JMpDY', 3410, 3, 'city'),
(3635, 50.232128, 28.647369, 'ChIJpfbodXBkLEcRDrSUQ2gLEp0', 3389, 3, 'city'),
(3636, 48.974953, 28.847408, 'ChIJLV4ajcdXzUAR4KrZ5Cft1sk', 3461, 3, 'city'),
(3637, 48.906616, 38.443359, 'ChIJxdBMqPEPIEERqg-3pVDQyk0', 3559, 3, 'city'),
(3638, 50.810581, 27.316694, 'ChIJpRoBm1MtKUcRagvAtLe6Jkc', 3389, 3, 'city'),
(3639, 49.694405, 36.359547, 'ChIJsfUISehlJ0EREOTjZOMdTdQ', 3383, 3, 'city'),
(3640, 49.382309, 34.726551, 'ChIJ_6kg5Xw12EARZgxnyUyh7hQ', 3403, 3, 'city'),
(3641, 51.875706, 33.496616, 'ChIJJcqUQ_iCK0ERQmlapehTqzs', 3396, 3, 'city'),
(3642, 50.078808, 25.147894, 'ChIJ1Ql_DQ74L0cRyk9uvd7iWuU', 3424, 3, 'city'),
(3643, 48.819138, 26.534954, 'ChIJx-eE_jvMM0cR2KRNoNoaoL4', 3398, 3, 'city'),
(3644, 48.621201, 24.575968, 'ChIJTT1BkYgyN0cR6N8ejb0d72U', 3483, 3, 'city'),
(3645, 46.484085, 30.592272, 'ChIJI7K4oEcsxkARcLO5UjKoBes', 3418, 3, 'city'),
(3646, 48.484142, 34.252659, 'ChIJLbNzupTL20ARaPzkM0p1EH0', 3408, 3, 'city'),
(3647, 50.175751, 27.055992, 'ChIJdep_6UKKLkcRRPz80h0U1tw', 3398, 3, 'city'),
(3648, 50.222076, 24.845963, 'ChIJ8dURlilEJUcRYOray-R93Us', 3424, 3, 'city'),
(3649, 47.657280, 34.078590, 'ChIJ41eDlXdb20AR8DPP9ttNIfA', 3408, 3, 'city'),
(3650, 49.227718, 31.852234, 'ChIJAURMbGFA0UARLL814Syguoc', 3394, 3, 'city'),
(3651, 49.076206, 30.969990, 'ChIJ1b8J6KMs0kARP4gH3t8puy4', 3394, 3, 'city'),
(3652, 50.518749, 30.239782, 'ChIJ29VN6RYyK0cRNI9p50Rp41w', 3387, 3, 'city'),
(3653, 49.118462, 26.336142, 'ChIJdSF0bR2GMUcRpGIcOgE95LU', 3398, 3, 'city'),
(3654, 50.644505, 29.914631, 'ChIJmUh8VFo9K0cRnMd4e4cP0gE', 3387, 3, 'city'),
(3655, 50.287575, 29.509436, 'ChIJSyLvwjdeK0cRTxpBVPXtEPE', 3389, 3, 'city'),
(3656, 47.567459, 34.394814, 'ChIJx07vBnCj3EARxGnU1ZHPOzY', 3408, 3, 'city'),
(3657, 49.395000, 36.215450, 'ChIJ-atwykaj2EARosPv_lZFnlA', 3383, 3, 'city'),
(3658, 47.666870, 38.076283, 'ChIJKXBFqq7e4EAR3mYpqirZNfc', 3447, 3, 'city'),
(3659, 49.415482, 38.156891, 'ChIJOUb8ZSb4IEERhBSAq2EPBZw', 3559, 3, 'city'),
(3660, 50.060005, 37.382294, 'ChIJuQhKnevaJkERZqDCOWar_Js', 3383, 3, 'city'),
(3661, 49.838722, 35.620617, 'ChIJE0hpSRGJJ0ERYfJN86sRqew', 3383, 3, 'city'),
(3662, 49.836315, 36.681313, 'ChIJ9ZGpXLEVJ0ERwrX0fR7ufrQ', 3383, 3, 'city'),
(3663, 49.721848, 35.867413, 'ChIJI8RZsSWAJ0ERJqwPdSKSDB0', 3383, 3, 'city'),
(3664, 50.163624, 35.540245, 'ChIJlZQTo3LoJ0ERnH0x-1bTr0c', 3383, 3, 'city'),
(3665, 50.077942, 36.789791, 'ChIJrYJcgNICJ0ERMrrfk3--Zeg', 3383, 3, 'city'),
(3666, 48.171230, 33.845615, 'ChIJ2ao6DrwA20ARkqWP-wrz47U', 3408, 3, 'city'),
(3667, 50.175819, 30.101763, 'ChIJD4esjBas1EARDAkHqe_E0RU', 3387, 3, 'city'),
(3668, 50.874474, 27.804295, 'ChIJ6ZdtmDxGKUcRQkoB3u_wtYQ', 3389, 3, 'city'),
(3669, 48.221107, 26.271299, 'ChIJ3Sz_QPN2NEcR1PsZRr9eZr0', 3450, 3, 'city'),
(3670, 51.576473, 26.570644, 'ChIJkTuzEEkoJkcRiL-M3uJ9YWc', 3378, 3, 'city'),
(3671, 51.564674, 31.784777, 'ChIJmZ61t5DNKkER5cELi3iK_BU', 3385, 3, 'city'),
(3672, 50.299068, 28.888241, 'ChIJoyvJjft9LEcRLhA_0ii610M', 3389, 3, 'city'),
(3673, 48.735992, 24.856077, 'ChIJr0apOR_XMEcRn6iKvfKx_b0', 3483, 3, 'city'),
(3674, 48.683804, 29.247627, 'ChIJO7iKWetwzUARhDgGVCJRefw', 3461, 3, 'city'),
(3675, 48.720116, 30.339870, 'ChIJn3AIaLcP0kARyPLF2Pf6clI', 3394, 3, 'city'),
(3676, 50.879070, 26.442322, 'ChIJkQjHBFBIL0cRieuVTYLwV1U', 3378, 3, 'city'),
(3677, 50.283691, 24.634508, 'ChIJx9_Po1o5JUcRQJ4vyVvio8Q', 3424, 3, 'city'),
(3678, 48.344120, 33.524479, 'ChIJtdan6jlZ2kAR-7lGBt4t1q4', 3408, 3, 'city'),
(3679, 48.045956, 37.966690, 'ChIJqbkNSpiO4EARHGTg4v0u5eQ', 3447, 3, 'city'),
(3680, 48.015884, 37.802849, 'ChIJLZqRAJWQ4EARhG-Fxf1eMzY', 3447, 3, 'city'),
(3681, 49.015667, 33.645046, 'ChIJgb7AXM3_2UARkiS-BidlEXo', 3403, 3, 'city'),
(3682, 48.458439, 27.792929, 'ChIJE-czwXEvM0cRZ_UVp-a1x34', 3461, 3, 'city'),
(3683, 48.813946, 29.381205, 'ChIJ1ak7uqN9zUARrm2wFBzE-cc', 3461, 3, 'city'),
(3684, 52.183743, 34.041206, 'ChIJJVM7xTNnLEERo8Qrkm8y6p0', 3396, 3, 'city'),
(3685, 50.146053, 31.086611, 'ChIJYzOfNDSL1EARNoUGCRcTZgw', 3387, 3, 'city'),
(3686, 50.421505, 33.985069, 'ChIJK26zm7TWKUERbBjPy4kWhsc', 3403, 3, 'city'),
(3687, 5.463816, 10.800005, 'ChIJPWy600KZ9RAR9lifxK0sABs', 3688, 2, 'region'),
(3688, 7.369722, 12.354722, 'ChIJIQ8-cFM3YRARU5uCmUXEAys', NULL, 1, 'country'),
(3689, 47.986584, 37.290695, 'ChIJmbf2MWAe3kARc6Dil3qhuGU', 3447, 3, 'city'),
(3690, 49.324020, 34.407307, 'ChIJucVrIfnO2UARXkqfJ_FNLXU', 3403, 3, 'city'),
(3691, 50.481064, 34.965435, 'ChIJgwwuwCNqKEERDdIszWBRKxM', 3396, 3, 'city'),
(3692, 48.322903, 35.521397, 'ChIJp6DKDrcC3EAR3CUQhzKCjwc', 3408, 3, 'city'),
(3693, 48.995789, 29.439714, 'ChIJITesfHWa0kARBygCZ8zSSeo', 3461, 3, 'city'),
(3694, 50.226093, 23.617216, 'ChIJ5zhdQmw1O0cRAs-MRv9kd7c', 3424, 3, 'city'),
(3695, 48.574039, 39.307816, 'ChIJhWJMhGTFH0ERZHGeh34u4Pg', 3559, 3, 'city'),
(3696, 48.467453, 22.763594, 'ChIJVdoKQqmuOUcRtt7y4dJKwX8', 3401, 3, 'city'),
(3697, 46.425613, 30.613375, 'ChIJ_ac2JKgyxkARmoEXHB8bpjM', 3418, 3, 'city'),
(3698, 49.039051, 28.108595, 'ChIJnZqC6p6iMkcRICzP0AxJqM8', 3461, 3, 'city'),
(3699, 50.446800, 30.075621, 'ChIJFwv0tjE2K0cRVKc236CsxwM', 3387, 3, 'city'),
(3700, 49.666977, 25.772308, 'ChIJwfcdY2gsMEcRvTaQSHSjvUc', 3427, 3, 'city'),
(3701, 49.732876, 29.664289, 'ChIJAQMmVcs200ARkZs1pFzEUKo', 3387, 3, 'city'),
(3702, 48.344704, 34.989876, 'ChIJGbmfJ4_620ARxpuAqpyUz8I', 3408, 3, 'city'),
(3703, 44.963562, 33.609200, 'ChIJOfjZhYK36kARALo1zqIZ0sg', 3704, 2, 'city'),
(3704, 44.952118, 34.102417, 'ChIJkfrH7elR6kARkpRoogcKsfY', NULL, 1, 'region'),
(3705, 49.366394, 29.658381, 'ChIJASqYvE7n0kARQPG0firc3G8', 3387, 3, 'city'),
(3706, 50.128502, 25.253031, 'ChIJ20PP1fD2L0cRYCQ0uUsgm5Y', 3378, 3, 'city'),
(3707, 48.542561, 37.041786, 'ChIJkf3LD56s30ARSvDKgw730o0', 3447, 3, 'city'),
(3708, 49.809410, 24.901371, 'ChIJ_b4d3_wHMEcRFmgAIU5y12g', 3424, 3, 'city'),
(3709, 46.774940, 31.952608, 'ChIJYXVIGL0sxEARSi7LfM4tnDo', 3406, 3, 'city'),
(3710, 49.154057, 23.028006, 'ChIJOQjs2ZXRO0cR02Zq7zq3b_8', 3424, 3, 'city'),
(3711, 48.807835, 24.540951, 'ChIJKYfAB0nIMEcRYRfr6XTHesk', 3483, 3, 'city'),
(3712, 47.010452, 28.863810, 'ChIJoWm3KDZ8yUARPN1JVzDW0Tc', 3505, 3, 'city'),
(3713, 49.040176, 23.512917, 'ChIJT7lAw3kjOkcRgentYW25PWI', 3424, 3, 'city'),
(3714, 48.735920, 22.472324, 'ChIJDQKKqwsTOUcRfG7GBA548kw', 3401, 3, 'city'),
(3715, 47.648926, 34.610519, 'ChIJAXtBLriQ3EARkXC1F2GYsng', 3408, 3, 'city'),
(3716, 52.834061, 79.869591, 'ChIJmRnSiacl5UIR13kr6iVVYO0', 3717, 3, 'city'),
(3717, 51.793629, 82.675858, 'ChIJ_0EiPofU50IR1TtsjXACf-4', 3416, 2, 'region'),
(3718, 50.304760, 30.516098, 'ChIJmfCvSH641EAROOnZPsoZDIo', 3387, 3, 'city'),
(3719, 49.722054, 23.456598, 'ChIJuf2SkpcFO0cRguYaRVc2Lf8', 3424, 3, 'city'),
(3720, 54.497780, 48.283890, 'ChIJFQ-EoBEkXUERzrhtOoCW36k', 3721, 3, 'city'),
(3721, 53.979336, 47.776241, 'ChIJuQJWTTOcQkERoJjxg6WjAgE', 3416, 2, 'region'),
(3722, 48.560406, 38.648720, 'ChIJFXAy4Lcc4EARViKlUzOwx4Q', 3559, 3, 'city'),
(3723, 47.696857, 32.503853, 'ChIJo--wMcc4xUARzaDvlUU2jzQ', 3406, 3, 'city'),
(3724, 50.358871, 33.259033, 'ChIJlUZBL8sQ1kARClIiyWJb9tQ', 3403, 3, 'city'),
(3725, 48.058174, 32.161079, 'ChIJ6VdifogB0EARXiCrRyqqTCA', 3422, 3, 'city'),
(3726, 46.251999, 30.437288, 'ChIJhQp-4ufEx0AR4GHXzWmOB_A', 3418, 3, 'city'),
(3727, 48.584908, 34.812923, 'ChIJgUP5bd5h2UARxgWs6LfY3Zc', 3408, 3, 'city'),
(3728, 49.649200, 26.975721, 'ChIJS2q15t0HLkcRDuzgSZMX0GM', 3398, 3, 'city'),
(3729, 48.673977, 28.856379, 'ChIJFxA-mdZBzUARN529Laqkh9Y', 3461, 3, 'city'),
(3730, 50.256821, 31.781765, 'ChIJd5Y_-x9N1EAR9xj4J6R7_pI', 3387, 3, 'city'),
(3731, 50.064152, 30.171228, 'ChIJLx-OTOen1EARiMczxGRJcRQ', 3387, 3, 'city'),
(3732, 50.312519, 30.860180, 'ChIJEwRGimHp1EAROPQfgXSwmLI', 3387, 3, 'city'),
(3733, 50.140999, 27.522400, 'ChIJ-5FaKfwpLEcRdJA3DRpmZFw', 3398, 3, 'city'),
(3734, 48.384949, 28.867336, 'ChIJp57hpbwEzUARWhwsroEKCOM', 3461, 3, 'city'),
(3735, 50.590477, 30.483198, 'ChIJXWQiRVjT1EARpJ5SK-iO4A8', 3387, 3, 'city'),
(3736, 29.971888, -97.869759, 'ChIJGWGB4ypUW4YRpg7UQrvonpk', 3737, 3, 'city'),
(3737, 31.968599, -99.901810, 'ChIJSTKCCzZwQIYRPN4IGI8c6xY', 3738, 2, 'region'),
(3738, 37.090240, -95.712891, 'ChIJCzYy5IS16lQRQrfeQ5K5Oxw', NULL, 1, 'country'),
(3739, 49.687775, 37.177856, 'ChIJ8cL8EXAzJ0ERjjyvF-HBPdo', 3383, 3, 'city'),
(3740, 51.104103, 28.019665, 'ChIJ9XqZFfNsKUcRCO7KSlSCqH4', 3389, 3, 'city'),
(3741, 48.154324, 24.819695, 'ChIJc536hWLuNkcRVqTnRG1XzD0', 3483, 3, 'city'),
(3742, 49.358013, 23.512320, 'ChIJNWmpEqNOOkcR5Y6yCjU3cJo', 3424, 3, 'city'),
(3743, 50.064793, 43.228428, 'ChIJPfFfz85DF0ERyk6E6vlrvtc', 3744, 3, 'city'),
(3744, 49.760452, 45.000000, 'ChIJmwd7FuN-EEERaMmemJeB6WY', 3416, 2, 'region'),
(3745, 47.075371, 32.806175, 'ChIJ9xJsy6xfxEARWzCooRDFThs', 3406, 3, 'city'),
(3746, 48.321667, 30.235092, 'ChIJ2ZH7D_zhzUAR6kyx4oUHwiU', 3422, 3, 'city'),
(3747, 53.858219, 49.799461, 'ChIJ2WMFtNi4Z0ERCR_YLX50g5M', 3475, 3, 'city'),
(3748, 49.960808, 29.450191, 'ChIJSYdlvxC3LEcR3gcDoVKvn8U', 3389, 3, 'city'),
(3749, 48.336716, 29.868635, 'ChIJP7TvfGLCzUAR75-CQQOROq0', 3422, 3, 'city'),
(3750, 49.076847, 27.682611, 'ChIJ-Uc7fy6JMkcRproaSi50jd0', 3461, 3, 'city'),
(3751, 50.371140, 33.748608, 'ChIJmQDPbdQn1kARyqHjlG721wk', 3403, 3, 'city'),
(3752, 50.999622, 26.750679, 'ChIJ_28up5zLKEcRZSO-JOiTVls', 3378, 3, 'city'),
(3753, 46.873657, 36.602474, 'ChIJS0N6LYi450ARDuw2gTsPnfw', 3410, 3, 'city'),
(3754, 47.817265, 34.754871, 'ChIJS4OnrGCG3EAR8jk1ywd6mnI', 3408, 3, 'city'),
(3755, 49.225513, 29.048887, 'ChIJ7-2sJoS20kARJxjMDUO7pwk', 3461, 3, 'city'),
(3756, 50.139420, 36.279488, 'ChIJPW_TWRCvJ0ERzoa_DZxJiMk', 3383, 3, 'city'),
(3757, 49.289589, 35.762680, 'ChIJOUuxLQvs2EARaDQZkwmRNFs', 3383, 3, 'city'),
(3758, 48.772446, 23.097401, 'ChIJU45aE12aOUcRCiyo4biVycU', 3401, 3, 'city'),
(3759, 51.716946, 26.270605, 'ChIJkTpZTyE0JkcRX7PIrVx9vhA', 3378, 3, 'city'),
(3760, 51.307556, 31.038963, 'ChIJZ5HVk3lX1UYRWiQVfIkEmcU', 3385, 3, 'city'),
(3761, 48.790722, 31.658262, 'ChIJ34wsDLil0UARXBfHzBKi4ic', 3422, 3, 'city'),
(3762, 49.038605, 32.188889, 'ChIJKULFS9MY0UARvnDNQGH7kaQ', 3394, 3, 'city'),
(3763, 49.291779, 31.446533, 'ChIJWeKBZ2Vg0UARFNepI6ad6Lo', 3394, 3, 'city'),
(3764, 47.851658, 34.957142, 'ChIJ_73ZclVi3EARjMs7v7vjO6w', 3410, 3, 'city'),
(3765, 48.322647, 31.518763, 'ChIJ7w-F6Gcs0EARY_Oc1E_osy4', 3422, 3, 'city'),
(3766, 50.449375, 30.186222, 'ChIJ6_FXL6s1K0cRxmEIGClsJkU', 3387, 3, 'city'),
(3767, 49.154041, 37.629433, 'ChIJk6QtRaeGIEERUGc1Uo02g9A', 3447, 3, 'city'),
(3768, 49.529083, 26.141699, 'ChIJyQEZNkTbMUcRoLKvH5Qxh_0', 3427, 3, 'city'),
(3769, 49.292404, 25.539906, 'ChIJZ1n7WUGtMUcR7pIFTfFVRsk', 3427, 3, 'city'),
(3770, 51.343655, 25.849087, 'ChIJDYtdvHsNJkcRAOHxDuPbKPs', 3378, 3, 'city'),
(3771, 50.940041, 34.632370, 'ChIJn0CZZ2EGKUER2K_oRPZEdJM', 3396, 3, 'city'),
(3772, 50.562447, 30.313215, 'ChIJs_lIoY0tK0cRtjv3apyr31w', 3387, 3, 'city'),
(3773, 45.357315, 36.468292, 'ChIJKfVf_1G77kAR9l-ykVOBc20', 3704, 2, 'city'),
(3774, 44.746090, 33.861565, 'ChIJOcVoCofU6kARxs8cvso-k44', 3704, 2, 'city'),
(3775, 44.616650, 33.525368, 'ChIJ9USRZe8llUARzDRN_6-nLb0', 3775, 2, 'city'),
(3776, 50.376041, 30.799030, 'ChIJaatNdO_C1EARjvSwg7vOU1U', 3387, 3, 'city'),
(3777, 48.542858, 22.994892, 'ChIJa74kWge7OUcRVsakX_rYtFY', 3401, 3, 'city'),
(3778, 46.827995, 36.889996, 'ChIJlxOf372050ARpbp6q7fh10k', 3410, 3, 'city'),
(3779, 49.994972, 36.048676, 'ChIJFR0ybWKYJ0ERVPYWuzmuYuE', 3383, 3, 'city'),
(3780, 49.388706, 40.146751, 'ChIJxwFSJ0JPH0ER-FEHvtzdofs', 3781, 3, 'city'),
(3781, 47.685326, 41.825893, 'ChIJIffqXTC6HUER4Jfxg6WjAgE', 3416, 2, 'region'),
(3782, 48.368954, 29.533165, 'ChIJyQdIIFO5zUARy_6GZnQZp_w', 3461, 3, 'city'),
(3783, 49.534515, 26.209307, 'ChIJYyZW9VfcMUcRVcsrsCyLESM', 3398, 3, 'city'),
(3784, 49.428505, 25.971289, 'ChIJ-bOP01PGMUcRY6OhU-HdCao', 3427, 3, 'city'),
(3785, 48.811722, 29.973894, 'ChIJZYGToVBw0kARpEzIBr4YGdo', 3394, 3, 'city'),
(3786, 51.037796, 31.888100, 'ChIJ_6ggA8idKkER8PU9Xrhe3Z4', 3385, 3, 'city'),
(3787, 51.166660, 31.846619, 'ChIJY6-JIWWYKkERCpMHbgOH6gM', 3385, 3, 'city'),
(3788, 50.432964, 30.212856, 'ChIJAWtqtwY1K0cRC5HP5sZZ10s', 3387, 3, 'city'),
(3789, 50.579140, 30.215075, 'ChIJuQvBlRsuK0cRSo-Icn4FVgY', 3387, 3, 'city'),
(3790, 49.104000, 29.209274, 'ChIJeQOKQIG80kARbDUSrypA66c', 3461, 3, 'city'),
(3791, 50.182590, 26.560299, 'ChIJxSE3fj9RLkcReh_Hio-Zb4Y', 3398, 3, 'city'),
(3792, 45.497124, 28.957695, 'ChIJZ7dMW113t0ARTLX0MBttli0', 3418, 3, 'city'),
(3793, 48.860477, 38.196064, 'ChIJieZfazEKIEERNhwmRdruYLA', 3447, 3, 'city'),
(3794, 48.535488, 35.070423, 'ChIJF2J8Y-FY2UARzD5uaENq8Z4', 3408, 3, 'city'),
(3795, 48.135117, 30.077047, 'ChIJW1-q4HF3zkARdGMBb_W-LZA', 3418, 3, 'city'),
(3796, 45.922276, 41.311588, 'ChIJb8pMQ-OO-0ARPM_qzpRdwtw', 3797, 3, 'city'),
(3797, 44.668098, 43.520214, 'ChIJQ6Vyx8On-EAR6kQG4kRWElY', 3416, 2, 'region'),
(3798, 49.462204, 24.068769, 'ChIJwYi3hy9gOkcRVvidaz9hX0I', 3424, 3, 'city'),
(3799, 47.565880, 35.787674, 'ChIJs9GMwUCm3UARmZRQXlgcSIk', 3410, 3, 'city'),
(3800, 49.385338, 24.142183, 'ChIJ1ZoHX3RjOkcRxLtn_7HOgiQ', 3424, 3, 'city'),
(3801, 46.752270, 32.786598, 'ChIJDd2KRFgNxEARVFMx92nZJ2Q', 3391, 3, 'city'),
(3802, 50.214558, 24.966002, 'ChIJF9HMWqVEJUcRRIUUWrYPOwQ', 3424, 3, 'city'),
(3803, 49.920238, 23.724085, 'ChIJfQF-XyQiO0cRkGktAjmlSUU', 3424, 3, 'city'),
(3804, 49.659668, 28.458021, 'ChIJtWSLJCMFLUcRPpVColiU0xs', 3461, 3, 'city'),
(3805, 50.677731, 28.202562, 'ChIJkSS_cyH8K0cRrkw4FcmgNj4', 3389, 3, 'city'),
(3806, 48.676517, 38.099079, 'ChIJpfIxLlXl30ARd-egGvUb5ok', 3447, 3, 'city'),
(3807, 52.548859, 39.506504, 'ChIJj92GzKVrOkERDCD5femuleE', 3808, 3, 'city'),
(3808, 52.526470, 39.203228, 'ChIJJZMSbOcKOkERb2g1eBuJov8', 3416, 2, 'region'),
(3809, 47.202862, 37.319710, 'ChIJ2Ue8Hucb50ARKqmiqClm3m0', 3447, 3, 'city'),
(3810, 51.159191, 27.976694, 'ChIJm6fLULgSKUcRAjqQckyqZxY', 3389, 3, 'city'),
(3811, 50.740211, 24.648676, 'ChIJkyGJvgwAJUcRiAkXYisA_M4', 3381, 3, 'city'),
(3812, 50.109436, 24.276068, 'ChIJPWWhaibTOkcRPV678CZU9Qs', 3424, 3, 'city'),
(3813, 47.940323, 35.432812, 'ChIJWdyEZRFB3EARGloZATh__to', 3410, 3, 'city'),
(3814, 47.453484, 39.052509, 'ChIJUxjiOZ5m4UARqlrJDHFBpLc', 3781, 3, 'city'),
(3815, 50.773617, 30.303556, 'ChIJhRTqjgfZKkcRqCjM1_cpE0Q', 3387, 3, 'city'),
(3816, 53.186260, 44.012306, 'ChIJd8HwYvDAQEER9fy9_-CUQT0', 3817, 3, 'city'),
(3817, 53.141209, 44.094006, 'ChIJh_6zX0LlQEERsJfxg6WjAgE', 3416, 2, 'region'),
(3818, 50.586021, 27.636377, 'ChIJa6TzhfiwLkcRDqrn279K29Q', 3389, 3, 'city'),
(3819, 50.031273, 29.021637, 'ChIJ20mf-meTLEcRriHatjYGu3o', 3389, 3, 'city'),
(3820, 50.590862, 32.449574, 'ChIJh_exAeTs1UARNtn9WWhLyko', 3385, 3, 'city'),
(3821, 47.309181, 31.788393, 'ChIJ6fimg6WbxUARa7KQ3QAhodw', 3406, 3, 'city'),
(3822, 60.044884, 30.705435, 'ChIJJ6rUSrDTl0YRJa_7Pl4Sum4', 3823, 3, 'city'),
(3823, 60.079323, 31.892664, 'ChIJgxy2QfxivUYRAJdzzhbvAAE', 3416, 2, 'region'),
(3824, 50.068825, 34.764534, 'ChIJ6wz3ewr910ARtqxEdrFtInU', 3403, 3, 'city'),
(3825, 50.239487, 32.507092, 'ChIJkdgdTHbX1UARFkCxXTh2Vpc', 3403, 3, 'city'),
(3826, 48.824326, 37.631943, 'ChIJFW8_2Jia30ARZeGZoVLWJrY', 3447, 3, 'city'),
(3827, 50.661633, 32.920818, 'ChIJk0BaNoz41UAReiicQXM5ch8', 3385, 3, 'city'),
(3828, 50.315098, 34.900562, 'ChIJPeZ-W2gRKEERnOXLZZtybcs', 3396, 3, 'city'),
(3829, 48.588001, 26.138454, 'ChIJO6eqHIvmM0cRkGCHvV7EPA4', 3450, 3, 'city'),
(3830, 46.185204, 29.862635, 'ChIJlf70j9wOyEARBw7bpmLJ-s8', 3418, 3, 'city'),
(3831, 49.134682, 29.406992, 'ChIJAZswUYOV0kARpLhrVra1q6U', 3461, 3, 'city'),
(3832, 50.540344, 14.540243, 'ChIJaWcLVxpnCUcRIAEVZg-vAAQ', 3833, 3, 'city'),
(3833, 50.659424, 14.763242, 'ChIJw21vd603CUcRgKgUZg-vAAE', 3834, 2, 'region'),
(3834, 49.817493, 15.472962, 'ChIJQ4Ld14-UC0cRb1jb03UcZvg', NULL, 1, 'country'),
(3835, 48.679848, 39.467251, 'ChIJy2ThvpTHH0ERiv_yGI9MTpQ', 3559, 3, 'city'),
(3836, 48.221581, 34.781551, 'ChIJDYXOAzeN20ARzCJjolpDnwc', 3408, 3, 'city'),
(3837, 45.978844, 29.423555, 'ChIJh2mKKgTkt0ARXCqwdgdBnGI', 3418, 3, 'city'),
(3838, 48.194016, 23.892797, 'ChIJ_8OiHuJ0N0cRKpKC2iVcRF4', 3401, 3, 'city'),
(3839, 49.862064, 30.819241, 'ChIJS7MaPuR_00ARkNsz8uw24Pc', 3387, 3, 'city'),
(3840, 50.058876, 23.973646, 'ChIJuyXKcs0pO0cRMPaHmvAcE10', 3424, 3, 'city'),
(3841, 48.204010, 34.873238, 'ChIJBbgHcqmM20ARQFie4VmkfOA', 3408, 3, 'city'),
(3842, 49.443993, 23.003752, 'ChIJSY1emIK-O0cRW6jagTbY9Xw', 3424, 3, 'city'),
(3843, 49.751034, 31.469700, 'ChIJPbPr8XUM1EARqFlIC3t6n5s', 3394, 3, 'city'),
(3844, 49.722645, 36.061794, 'ChIJf0CTJDR8J0ERIDU8VWmpmFA', 3383, 3, 'city'),
(3845, 48.892246, 26.860388, 'ChIJ3QO5ei1KMkcR7dPYj5akpTQ', 3398, 3, 'city'),
(3846, 49.802673, 23.146923, 'ChIJxef0RAJzO0cRFHvemX6sSYc', 3424, 3, 'city'),
(3847, 51.805176, 31.094088, 'ChIJdzLm23Ua1UYRRxTRYWC9idg', 3385, 3, 'city'),
(3848, 55.264462, 31.517750, 'ChIJga6T1h5rz0YR8aW9hPqDPFo', 3849, 3, 'city'),
(3849, 54.988300, 32.667740, 'ChIJwYqGKV4nzEYRQJjxg6WjAgE', 3416, 2, 'region'),
(3850, 48.154469, 38.359070, 'ChIJK_vFgQld4EARQPNMFmbYVLc', 3447, 3, 'city'),
(3851, 47.930363, 28.867706, 'ChIJG3zL54tnzEARIb3kOpFvxbw', 3852, 3, 'city'),
(3852, 47.215298, 29.463804, 'ChIJz7BLtahRyUARRs7QbF29tHc', 3505, 2, 'region'),
(3853, 44.952118, 34.102417, 'ChIJxRyZ397d6kARQAt4vC9CKZw', 3704, 2, 'city'),
(3854, 48.910477, 34.911598, 'ChIJo3RIMawM2UARjoCQCeEj2dM', 3408, 3, 'city'),
(3855, 51.996922, 33.264378, 'ChIJ1SxKUrt8K0ERC1uS5uQy2M0', 3385, 3, 'city'),
(3856, 49.087727, 28.348202, 'ChIJgSx3zj2nMkcRuNMswzuK1Ww', 3461, 3, 'city'),
(3857, 50.800137, 28.079664, 'ChIJeWr3U6hQKUcRFQplflAAlIY', 3389, 3, 'city'),
(3858, 48.197186, 22.637094, 'ChIJbc2BeNRaOEcRt2EBkKfEE6w', 3401, 3, 'city'),
(3859, 48.498852, 38.670601, 'ChIJl6AtIioa4EAR5mAqN-Y0IRs', 3559, 3, 'city'),
(3860, 49.331818, 26.744614, 'ChIJY73cxOQcMkcRZCgZJb-6xRQ', 3398, 3, 'city'),
(3861, 50.000561, 25.507252, 'ChIJ9X6PaQPmL0cRwpBRzAAXvrU', 3427, 3, 'city'),
(3862, 49.533360, 31.727613, 'ChIJv_sBD8tV0UAR8KD6HrcqiR8', 3394, 3, 'city'),
(3863, 50.017319, 32.990845, 'ChIJN5rnOdtd1kARPJ9RdJ00lYw', 3403, 3, 'city'),
(3864, 50.402466, 33.384750, 'ChIJezz8oAYZ1kARVtjDjMRUrgY', 3403, 3, 'city'),
(3865, 49.660465, 32.344879, 'ChIJjfSQhQi81kARpu4ruD--uKM', 3394, 3, 'city'),
(3866, 50.019810, 33.941673, 'ChIJ0y4VVpvI10ARYrChMbwWh6A', 3403, 3, 'city'),
(3867, 50.569412, 33.793419, 'ChIJmVe6v-bcKUER0pG_D2Nrbrw', 3396, 3, 'city'),
(3868, 50.118668, 32.452202, 'ChIJgzw6rIp_1kARf5OIeQQSdMc', 3403, 3, 'city'),
(3869, 50.204632, 34.363853, 'ChIJf9bOFKjY10AR7sSyNNXwgmI', 3403, 3, 'city'),
(3870, 47.660526, 33.720539, 'ChIJFVZiXzVI20ARvJj4FtZdguw', 3408, 3, 'city'),
(3871, 48.622272, 22.962488, 'ChIJO_MHChajOUcRvhGm83qd6BQ', 3401, 3, 'city'),
(3872, 46.640099, 14.448949, 'ChIJS2wlEC8McEcRm7psrglK1GU', 3873, 3, 'city'),
(3873, 46.722202, 14.180588, 'ChIJF_WJpph0cEcRP8merT25Wtk', 3874, 2, 'region'),
(3874, 47.516232, 14.550072, 'ChIJfyqdJZsHbUcRr8Hk3XvUEhA', NULL, 1, 'country'),
(3875, 50.576927, 30.545078, 'ChIJbYfeM3_U1EARzCYsx_Khfpo', 3387, 3, 'city'),
(3876, 49.292377, 33.070362, 'ChIJy2yxBQQx10AR9riVSilu2AM', 3403, 3, 'city'),
(3877, 46.066254, 30.452837, 'ChIJS0fnBmrrx0ARbdlUOpkZf3I', 3418, 3, 'city'),
(3878, 48.962788, 32.224735, 'ChIJf6QioRLi0EARdFUyFoCqWzc', 3422, 3, 'city'),
(3879, 49.392021, 33.259998, 'ChIJz1rDkLg-10AR0xYg2d53zpw', 3403, 3, 'city'),
(3880, 51.149403, 34.317524, 'ChIJBdYWPo1xKUERKFQRKCf7hR4', 3396, 3, 'city'),
(3881, 49.660694, 25.143467, 'ChIJ36YrjFkRMEcRH4QP54gN3dc', 3427, 3, 'city'),
(3882, 48.384357, 30.462620, 'ChIJt3-SJdkFzkARJWATawsP7fY', 3422, 3, 'city'),
(3883, 47.404579, 38.905613, 'ChIJD8WenFtd4UARNSl7mXnfpXI', 3781, 3, 'city'),
(3884, 48.649063, 24.369993, 'ChIJ142LSm9KN0cRvoxFTQbZNH4', 3483, 3, 'city'),
(3885, 50.241421, 25.762260, 'ChIJRfe1qN2WL0cRrvzvJRPXfo8', 3378, 3, 'city'),
(3886, 49.443890, 24.936140, 'ChIJ76rOffhlMEcRGziF8OJ5g28', 3427, 3, 'city'),
(3887, 48.140636, 38.936737, 'ChIJ34vHUu5K4EARUgJRuZDdPkA', 3559, 3, 'city'),
(3888, 46.721992, 32.293201, 'ChIJ9fFkvp0kxEARIAXgx-9ecIo', 3391, 3, 'city'),
(3889, 48.307098, 38.029633, 'ChIJNVb0aBd34EARLG93MCufLPM', 3447, 3, 'city'),
(3890, 48.355434, 32.651756, 'ChIJXzFTaq530EARVU19nELh8Gg', 3422, 3, 'city'),
(3891, 48.311783, 23.041895, 'ChIJr63R8Uc1OEcRXrPmZTMybhg', 3401, 3, 'city'),
(3892, 50.264606, 29.883574, 'ChIJO_jvQqFRK0cRsGzQ2y7u178', 3387, 3, 'city'),
(3893, 48.434044, 22.210104, 'ChIJhRX59cLjOEcR96W_yASXNhE', 3401, 3, 'city'),
(3894, 48.616192, 37.527218, 'ChIJZ7fHeMi-30ARfw3gXbkZ3Cc', 3447, 3, 'city'),
(3895, 49.342400, 34.216576, 'ChIJJVYzYqTQ2UARguTXHhHrpfw', 3403, 3, 'city'),
(3896, 47.161022, 30.594168, 'ChIJU5Dur5Y1z0ARPryYEvd0bcI', 3418, 3, 'city'),
(3897, 48.338963, 34.941101, 'ChIJsxqw8BXw20AR7oTLL_jkuvs', 3408, 3, 'city'),
(3898, 47.309830, 32.848770, 'ChIJHQPVzXLlxEARKk2x1c7ruaI', 3406, 3, 'city'),
(3899, 47.321156, 33.303268, 'ChIJRVv8OjW-xEARLkk7F7cTQ7w', 3391, 3, 'city'),
(3900, 50.455360, 30.771639, 'ChIJocRkwIHb1EARuMt3kKn-fq4', 3387, 3, 'city'),
(3901, 46.342983, 30.566778, 'ChIJ4XJ9hdfNx0ARnhNnzfnSTSQ', 3418, 3, 'city'),
(3902, 49.042229, 38.217655, 'ChIJzZN0YUpsIEERLmrsqxksPl0', 3559, 3, 'city'),
(3903, 42.145821, 24.762207, 'ChIJmz3FfQrRrBQRtD5qZRIqNLU', 3904, 3, 'city'),
(3904, 42.135406, 24.745291, 'ChIJPXZIogjRrBQRoDgTb_rRcGQ', 3905, 2, 'region'),
(3905, 42.733883, 25.485830, 'ChIJifBbyMH-qEAREEy_aRKgAAA', NULL, 1, 'country'),
(3906, 50.583763, 34.490463, 'ChIJ7c6aJFlKKEERG_QyFweUXXA', 3396, 3, 'city'),
(3907, 49.952778, 28.857859, 'ChIJ9yWeoMXtLEcRBEnwV9lOHmk', 3389, 3, 'city'),
(3908, 49.431938, 29.672588, 'ChIJsTrxqrff0kARYALvUha4KCo', 3387, 3, 'city'),
(3909, 51.232986, 24.036440, 'ChIJ1Ra7LksLJEcRkTzdLOuB3po', 3381, 3, 'city'),
(3910, 49.240055, 24.876467, 'ChIJo7T_MiqLMEcR6DP_is061X0', 3483, 3, 'city'),
(3911, 49.251980, 26.273161, 'ChIJ1bqlLFuSMUcRwH--9f8tWg4', 3398, 3, 'city'),
(3912, 48.854095, 27.799667, 'ChIJwc92I7zrMkcR8yi_wt3Y5dY', 3461, 3, 'city'),
(3913, 50.508911, 26.256643, 'ChIJrbw2MpMNL0cRsx_bcE6TLn8', 3378, 3, 'city'),
(3914, 49.691181, 30.478016, 'ChIJ5XQ4zVNx00ARNlbrNxZ-DZ4', 3387, 3, 'city'),
(3915, 53.138367, 18.481342, 'ChIJBR7VDXIwA0cR1a-HKeXsqEw', 3916, 3, 'city'),
(3916, 53.164837, 18.483421, 'ChIJE4dDGsQ2A0cRMMAOrTIOAgE', 3917, 2, 'region'),
(3917, 51.919437, 19.145136, 'ChIJuwtkpGSZAEcR6lXMScpzdQk', NULL, 1, 'country'),
(3918, 51.252483, 32.422783, 'ChIJ25VlBYL0KkERAtnMf_yAqfI', 3385, 3, 'city'),
(3919, 50.350704, 31.318892, 'ChIJEau0uLv-1EARL4otsp7142Q', 3387, 3, 'city'),
(3920, 50.062035, 35.158222, 'ChIJ7QhaHmv8J0ERBshzbP6pMVs', 3383, 3, 'city'),
(3921, 50.721416, 33.785042, 'ChIJy1V1g7zrKUERPVDX4ltbs-g', 3396, 3, 'city'),
(3922, 46.821014, 30.945789, 'ChIJNwstElAbxkARRbXQ1zh6peg', 3418, 3, 'city'),
(3923, 48.234554, 30.887239, 'ChIJkx-KCa8kzkARnoj70WsyI1c', 3422, 3, 'city'),
(3924, 49.486305, 29.268579, 'ChIJjwHwynvU0kARVg93UrLttKs', 3461, 3, 'city'),
(3925, 50.451359, 30.226521, 'ChIJY2Oad2A0K0cRluBqaWn9dt4', 3387, 3, 'city'),
(3926, 50.509296, 31.016056, 'ChIJTRaDZfzf1EAR0CXCD4foJV4', 3387, 3, 'city'),
(3927, 48.101700, 33.758137, 'ChIJ6X-PlpMD20ARejycvL7qhbo', 3408, 3, 'city'),
(3928, 48.204288, 35.173027, 'ChIJ9T_ibEAJ3EARHPzHEoeKDlM', 3408, 3, 'city'),
(3929, 48.634476, 35.463722, 'ChIJf7mV_zxJ2UARGk5ow7phSxI', 3408, 3, 'city'),
(3930, 49.652222, 23.486530, 'ChIJF1rElssGO0cRhCBrJwonH68', 3424, 3, 'city'),
(3931, 50.767109, 35.267666, 'ChIJi9qZMnPzKEERKBMSVZJgzfs', 3396, 3, 'city'),
(3932, 50.428375, 25.336191, 'ChIJPxn55q18JUcRBeXPMNKC8hM', 3378, 3, 'city'),
(3933, 51.083637, 30.875269, 'ChIJw2EoNpGxKkcRd_rY6rLud9s', 3385, 3, 'city'),
(3934, 50.152035, 29.762014, 'ChIJWeoL8eapLEcRpiSUaV7Olko', 3387, 3, 'city'),
(3935, 48.076481, 39.670712, 'ChIJmRA0_Jnn4UARLgj1RrbDWNA', 3559, 3, 'city'),
(3936, 48.313503, 25.082230, 'ChIJDUD9E0fHNkcRb22zHWP7u6Q', 3483, 3, 'city'),
(3937, 48.996941, 31.402243, 'ChIJJXz10rx30UARJNSGAbBWGsU', 3394, 3, 'city'),
(3938, 49.790527, 23.369946, 'ChIJWw6bFiAOO0cRKeougTwgaUw', 3424, 3, 'city'),
(3939, 48.616879, 34.653812, 'ChIJFWeSNPXX20ARetMJwSMOUO4', 3408, 3, 'city'),
(3940, 51.891113, 31.595854, 'ChIJdfbIJy_Z1EYR5UT34CJGUs8', 3385, 3, 'city'),
(3941, 51.250000, 32.599998, 'ChIJr7jINFVfKkERKMa-w0Algdg', 3385, 3, 'city'),
(3942, 50.497444, 31.783318, 'ChIJK5PW4BGm1UAR0Lfgk82rdLw', 3387, 3, 'city'),
(3943, 50.532314, 30.850269, 'ChIJe5QGxt_Y1EARUFE1zXCkcxs', 3387, 3, 'city'),
(3944, 48.835258, 27.267593, 'ChIJoSOBxTVWMkcRRV9gYIxuSiY', 3398, 3, 'city'),
(3945, 48.154793, 25.710072, 'ChIJJfmOyDYaNEcRWl7y-kOD-nw', 3450, 3, 'city'),
(3946, 49.676647, 33.787025, 'ChIJQ5VzghOg10ARNo0mwAcsEdU', 3403, 3, 'city'),
(3947, 49.669212, 24.559317, 'ChIJsQ64E-aYOkcRjlUf1p51Xek', 3424, 3, 'city'),
(3948, 48.748150, 24.426947, 'ChIJ9ztjQ521MEcR8p_pxqXVNWA', 3483, 3, 'city'),
(3949, 48.926842, 23.917402, 'ChIJH0PTYDsEOkcRmpNlfKGDrrg', 3483, 3, 'city'),
(3950, 48.564175, 32.326550, 'ChIJoayrIbld0EARXn6AK3Mvfec', 3422, 3, 'city'),
(3951, 48.961449, 30.337511, 'ChIJ4yICjVdA0kARmJoEwxapJy0', 3394, 3, 'city'),
(3952, 47.822762, 31.184082, 'ChIJ-Ruuw-i6z0ARbnXgFDkpfrg', 3406, 3, 'city'),
(3953, 47.545940, 33.922867, 'ChIJu9imb2xR20ARxq7zeZMuJgU', 3408, 3, 'city'),
(3954, 49.954197, 36.096607, 'ChIJIyn1lmefJ0ERPaB8PEaR5PE', 3383, 3, 'city'),
(3955, 46.162197, 32.505276, 'ChIJBbpRuzs2wUARpEc06Yx-_0Y', 3391, 3, 'city'),
(3956, 47.501507, 34.634186, 'ChIJz-HRAAS_3EARB9mmEE55kLE', 3410, 3, 'city'),
(3957, 48.737247, 39.231197, 'ChIJ9b5lRWDqH0ER8peWGJ5mIGM', 3559, 3, 'city'),
(3958, 46.408951, 32.636242, 'ChIJ3U921q6sxkARhc3iQrX5dcE', 3391, 3, 'city'),
(3959, 50.099472, 24.345448, 'ChIJrzUf6rUyJUcRstEMHa26ckg', 3424, 3, 'city'),
(3960, 50.119110, 26.828114, 'ChIJVSAotDNcLkcRb72hYGks3QE', 3398, 3, 'city'),
(3961, 54.945591, 20.445229, 'ChIJp7bVILRF40YRKoApQoNVmMQ', 3962, 3, 'city'),
(3962, 54.823528, 21.481615, 'ChIJ4wsuREWc40YRewI60MRYJR4', 3416, 2, 'region'),
(3963, 48.872578, 24.609180, 'ChIJObkXrqbHMEcRfrzpT9M4HD0', 3483, 3, 'city'),
(3964, 49.407776, 24.307978, 'ChIJ4xkNX4N9OkcROkdrcA1c8vs', 3424, 3, 'city'),
(3965, 55.981567, 37.134846, 'ChIJXQ7q_8YUtUYRrN7q7WixXoc', 3473, 3, 'city'),
(3966, 50.581100, 25.540426, 'ChIJKymdoQCBJUcRtO4qNNkXSYM', 3378, 3, 'city'),
(3967, 46.421459, 33.146263, 'ChIJ3Xz8HMLmw0AR3ouh_mEPr9U', 3391, 3, 'city'),
(3968, 50.820114, 25.317963, 'ChIJ1RtWFEeWJUcRzrNm6L5bxkE', 3381, 3, 'city'),
(3969, 49.922569, 27.733112, 'ChIJPZrcC_PLLUcRlLZ26DT6FiY', 3389, 3, 'city'),
(3970, 46.369247, 35.341633, 'ChIJYyfZNbcx6EARosCnBlK1-40', 3410, 3, 'city'),
(3971, 50.447281, 30.945173, 'ChIJbTjLE1Dn1EARyuA4vvsYUEQ', 3387, 3, 'city'),
(3972, 52.832424, 25.480679, 'ChIJ90x0k8Ks2EYRcbJubK7wc2c', 3973, 3, 'city'),
(3973, 52.529663, 25.460648, 'ChIJLYlCZvkLIUcR93BLf6j985E', 3441, 2, 'region'),
(3974, 49.625122, 29.052305, 'ChIJl77g3g0oLUcRssll6mH_kEg', 3389, 3, 'city'),
(3975, 50.374931, 27.986448, 'ChIJB2S-IS8aLEcRc2hX2_eOLaE', 3389, 3, 'city'),
(3976, 48.996536, 29.799932, 'ChIJq0Hbivph0kARlmcFcYdW7JE', 3394, 3, 'city'),
(3977, 49.771542, 35.077667, 'ChIJx9sOf-UR2EARpvMPRLlTFoI', 3403, 3, 'city'),
(3978, 50.055695, 28.117701, 'ChIJp3OkbJRILEcRXRCE5izm-pQ', 3389, 3, 'city'),
(3979, 47.627892, 30.989851, 'ChIJZUixTaEJz0AR6FN56CIy3rE', 3406, 3, 'city'),
(3980, 50.496529, 29.233738, 'ChIJzwZB3kB3K0cRi0CkIydem84', 3389, 3, 'city'),
(3981, 48.699181, 35.385811, 'ChIJ6cUNI7VH2UARvKNmC3hyyaY', 3408, 3, 'city'),
(3982, 48.372890, 35.177639, 'ChIJ46DuQs7-20ARWl2_xQ5Y01U', 3408, 3, 'city'),
(3983, 49.341511, 34.313042, 'ChIJEfHKciXQ2UARyq457B4Sgnc', 3403, 3, 'city'),
(3984, 49.146481, 35.870964, 'ChIJvYeUxELD2EARAhs7o_6sZxI', 3383, 3, 'city'),
(3985, 46.294064, 33.092014, 'ChIJsUPUHntgwUARbO_mRjS3khE', 3391, 3, 'city'),
(3986, 50.748989, 28.720650, 'ChIJtUIRZw_rK0cRohxwL2p10f4', 3389, 3, 'city'),
(3987, 49.094536, 30.405060, 'ChIJHWG3EaBI0kARDUGjQc14OEw', 3394, 3, 'city'),
(3988, 48.582157, 34.870316, 'ChIJGU9tUDhg2UARTIa8_mtgrYM', 3408, 3, 'city'),
(3989, 51.524570, 32.500492, 'ChIJ7R1Lc0_iKkERCUyxqfn2LtE', 3385, 3, 'city'),
(3990, 48.028278, 38.765614, 'ChIJR6wm439S4EARzhRkPEWkhqE', 3447, 3, 'city'),
(3991, 49.125870, 28.364653, 'ChIJq3MwsaVYLUcRzPeKj_qllmI', 3461, 3, 'city'),
(3992, 49.428909, 23.747295, 'ChIJWYr3ojhaOkcRYYwz1t-gZXA', 3424, 3, 'city'),
(3993, 44.970310, 34.058193, 'ChIJ2cFOh2Td6kARuKk48Qv6H3I', 3704, 2, 'city'),
(3994, 49.769947, 24.193560, 'ChIJscJQwWvqOkcR6Ht74q1Sl24', 3424, 3, 'city'),
(3995, 51.195389, 31.991419, 'ChIJg1DwwxuXKkER2HKEb7lQ9is', 3385, 3, 'city'),
(3996, 50.948616, 30.878677, 'ChIJu6oovUW1KkcR4Mf-EmyleyI', 3385, 3, 'city'),
(3997, 46.351509, 34.335510, 'ChIJsZK4ohpCwkARPgBIoixRXXY', 3391, 3, 'city'),
(3998, 45.350193, 28.850191, 'ChIJH1ZB9-Rlt0ARfWJy7C3Ymr0', 3418, 3, 'city'),
(3999, 49.830029, 36.994877, 'ChIJZZuHMTw7J0ERV4dqH6pC32E', 3383, 3, 'city'),
(4000, 50.913166, 31.116846, 'ChIJyTDKVfdI1UARQqy2IW3TKNs', 3385, 3, 'city'),
(4001, 48.924065, 24.665354, 'ChIJ0WFxZ8TAMEcRIreYhXJa-w0', 3483, 3, 'city'),
(4002, 51.655891, 25.103025, 'ChIJo1_GbaSTJkcRsqXYPsZSTYI', 3381, 3, 'city'),
(4003, 42.425777, 25.634464, 'ChIJNTnoMAtpqEARsZ-HYhTtunQ', 3905, 3, 'city'),
(4004, 49.262688, 30.830725, 'ChIJmZR50wjJ00ARTsf8jJOeepo', 3394, 3, 'city'),
(4005, 49.014004, 35.364437, 'ChIJuyPEJOMb2UARrQN5CexEuyo', 3408, 3, 'city'),
(4006, 50.074860, 31.201685, 'ChIJuWTyEx5h1EARvF8uh7KAyqQ', 3387, 3, 'city'),
(4007, 49.303009, 28.262114, 'ChIJ8-WSxBRmLUcRp2W5uCj-Tg4', 3461, 3, 'city'),
(4008, 48.344425, 33.837353, 'ChIJ3cvd8mhS2kARQ-7wDQPaBmM', 3408, 3, 'city'),
(4009, 46.502808, 30.328140, 'ChIJC4R0TpmAyEARqhGJ9Gre3RI', 3418, 3, 'city'),
(4010, 49.073055, 32.661781, 'ChIJESydks3a0EARf-RVQKeKzRs', 3394, 3, 'city'),
(4011, 49.561996, 30.497391, 'ChIJB8paU2yg00ARvZqmU82MkdE', 3387, 3, 'city'),
(4012, 49.129494, 30.780470, 'ChIJoUyf8T8z0kAR8tm1J4PV_qw', 3394, 3, 'city'),
(4013, 50.366901, 30.369781, 'ChIJWYAiyvbJ1EARCkbKW-zK0sY', 3387, 3, 'city'),
(4014, 48.447174, 25.476704, 'ChIJPyATesSyNkcR1AJuqeRRqc0', 3483, 3, 'city'),
(4015, 48.572479, 35.086910, 'ChIJTx22Xe9b2UAR156-ZPRFoMk', 3408, 3, 'city'),
(4016, 49.835457, 33.912834, 'ChIJEbs7y-y810ARIvzlZ5p0o5Y', 3403, 3, 'city'),
(4017, 46.128220, 30.384611, 'ChIJl886LOnpx0ARGptUXeE9KDc', 3418, 3, 'city'),
(4018, 50.106232, 34.280792, 'ChIJf52BjV3c10ARf0viNIx3Wq4', 3403, 3, 'city'),
(4019, 49.893272, 36.011986, 'ChIJPY3xg0GcJ0ER6ClKZZj6lmM', 3383, 3, 'city'),
(4020, 50.306812, 30.410727, 'ChIJ7Qjy27S31EARCnGwUcDhj5M', 3387, 3, 'city'),
(4021, 49.241245, 30.098993, 'ChIJlQSDxANW0kAR0qxwOIW2tPY', 3394, 3, 'city'),
(4022, 49.381470, 32.147423, 'ChIJhVGjTc410UARLzPRGNyr1Ok', 3394, 3, 'city'),
(4023, 48.293739, 38.883354, 'ChIJJz6wC3Q64EARXb8MDsWNQYs', 3559, 3, 'city'),
(4024, 50.748226, 27.807896, 'ChIJRfQOR7dNKUcRqmgs3BfrxlU', 3389, 3, 'city'),
(4025, 48.457218, 36.434731, 'ChIJZ93dBvXx3kAROoR8HXefKjs', 3408, 3, 'city'),
(4026, 50.400913, 25.759068, 'ChIJbz3zMlybL0cRprQZ48bblQg', 3378, 3, 'city'),
(4027, 48.814430, 35.530640, 'ChIJ8ZnOnDE62UARfEE9t90vqFQ', 3408, 3, 'city'),
(4028, 50.296906, 26.860065, 'ChIJO9W3z9T2LkcRhAM_-m28x-w', 3398, 3, 'city'),
(4029, 51.287426, 30.998919, 'ChIJt0ziEvZX1UYRjmty-zYUJS8', 3385, 3, 'city'),
(4030, 52.181461, 32.583996, 'ChIJ06aTPcBTK0ERx7Kp4cm3VzI', 3385, 3, 'city');
INSERT INTO `Frontend_Geo` (`ID`, `lat`, `lng`, `placeID`, `parentID`, `level`, `type`) VALUES
(4031, 48.472340, 25.279245, 'ChIJb3jSbGO1NkcRuhPu3bQgouQ', 3483, 3, 'city'),
(4032, 48.936134, 24.159409, 'ChIJa2mlLiqmMEcRkIYHd7Q2OYs', 3483, 3, 'city'),
(4033, 50.415859, 30.129450, 'ChIJ1xLPbQhKK0cR2tHblX0hhgs', 3387, 3, 'city'),
(4034, 50.696060, 29.598734, 'ChIJtwnu1bYPK0cRFq-Fo0q16CM', 3387, 3, 'city'),
(4035, 48.858803, 24.996813, 'ChIJETyyR_beMEcRWOBJ0CtRqWs', 3483, 3, 'city'),
(4036, 45.828133, 29.378397, 'ChIJV_u8Nwzyt0ARpooJOk3EmH0', 3418, 3, 'city'),
(4037, 48.253345, 26.584150, 'ChIJCdSftz-BM0cRic0_9RxQIew', 3450, 3, 'city'),
(4038, 50.741264, 25.456736, 'ChIJXxJkuRKQJUcRVF3oR6nyRlE', 3381, 3, 'city'),
(4039, 48.136597, 37.749134, 'ChIJZzqXL_eD4EARLbIph12qt2w', 3447, 3, 'city'),
(4040, 48.405525, 37.187080, 'ChIJA6yxqIhL3kARCHfqNdjM1WM', 3447, 3, 'city'),
(4041, 48.204540, 37.336823, 'ChIJK9JMCKQ63kARlEEUErIfG_Q', 3447, 3, 'city'),
(4042, 47.818047, 35.463448, 'ChIJczCzrVBF3EARDB7g_qsEXnk', 3410, 3, 'city'),
(4043, 49.014977, 31.053904, 'ChIJy-i0xIKA0UARXt7yq26-e_I', 3394, 3, 'city'),
(4044, 46.019936, 29.673437, 'ChIJIaAy-nYCyEARQC3z-DF3gZ0', 3418, 3, 'city'),
(4045, 50.707615, 28.595407, 'ChIJl5sUhy7sK0cR5qv80iH59Cc', 3389, 3, 'city'),
(4046, 49.785557, 23.869827, 'ChIJJ4881ELhOkcRrI-4Gifa29g', 3424, 3, 'city'),
(4047, 45.888054, 30.145805, 'ChIJC21YoQoKuEARQpWp3SmSvLo', 3418, 3, 'city'),
(4048, 52.292412, 30.484392, 'ChIJ8ZuRX_A21EYRihn5lPE-KkU', 4049, 3, 'city'),
(4049, 52.164875, 29.133326, 'ChIJswX2bnVp1EYRXm4VP35AKWk', 3441, 2, 'region'),
(4050, 50.553009, 30.632477, 'ChIJvVbKONLW1EAR_s2Fprs7n3Y', 3387, 3, 'city'),
(4051, 49.823895, 34.526096, 'ChIJ_zTUW4j010ARAJjl4uiRXCA', 3403, 3, 'city'),
(4052, 47.937088, 29.628839, 'ChIJVetXFyYvzEARFNS-9iFEmx0', 3418, 3, 'city'),
(4053, 49.323097, 28.082315, 'ChIJveOY2t96LUcR6wWCD0yvXOM', 3461, 3, 'city'),
(4054, 50.586067, 30.910822, 'ChIJX43JuiIn1UARhsfNfHwpjuc', 3387, 3, 'city'),
(4055, 49.534138, 39.120419, 'ChIJw1JKdg2_IUERvTQi73giy4U', 3559, 3, 'city'),
(4056, 49.422680, 24.810165, 'ChIJRYtVGtRkMEcRRDk3n3v5KiU', 3427, 3, 'city'),
(4057, 49.783047, 24.005083, 'ChIJOQW5jUbmOkcR2tAB0branOo', 3424, 3, 'city'),
(4058, 46.703140, 33.420059, 'ChIJeQ9BteeQw0ARfD4LNkzN7kw', 3391, 3, 'city'),
(4059, 48.613018, 34.780891, 'ChIJUTU7DT5i2UARLAWCpiz-ogw', 3408, 3, 'city'),
(4060, 38.167004, 23.338949, 'ChIJwXMwuQpRoBQRgH654iy9AAQ', 4061, 3, 'city'),
(4061, 38.045757, 23.858475, 'ChIJUwsYu2cPnxQRkIC54iy9AAE', 4062, 2, 'region'),
(4062, 39.074207, 21.824312, 'ChIJY2xxEcdKWxMRHS2a3HUXOjY', NULL, 1, 'country'),
(4063, 48.411633, 33.694160, 'ChIJdYnTzata2kARauO5belwGzs', 3408, 3, 'city'),
(4064, 49.767689, 24.426069, 'ChIJKV53vH2-OkcRoxKSCQNCUEo', 3424, 3, 'city'),
(4065, 48.178295, 25.342663, 'ChIJW5AiHnSiNkcRfia8fa99l-c', 3450, 3, 'city'),
(4066, 49.107834, 28.682026, 'ChIJBd-og31ULUcRK1R1CvPHAIc', 3461, 3, 'city'),
(4067, 49.230251, 33.126431, 'ChIJ_z1wlDE010ARYv18C_E9hXk', 3403, 3, 'city'),
(4068, 48.594547, 34.419796, 'ChIJO1EEE9zT20AR-FLyfpykoGE', 3408, 3, 'city'),
(4069, 48.762886, 28.069046, 'ChIJBRaECkPPMkcRuR65dDMqtRY', 3461, 3, 'city'),
(4070, 48.373405, 22.911482, 'ChIJUeZvhVyzOUcRhAq-6cmYWSw', 3401, 3, 'city'),
(4071, 49.344830, 28.722860, 'ChIJ2bUa-HxILUcR1hD-SSVbjyE', 3461, 3, 'city'),
(4072, 50.401077, 29.375027, 'ChIJjbe0Y_xkK0cRghzYgjrMGg4', 3389, 3, 'city'),
(4073, 49.977039, 24.092272, 'ChIJxzuhqAzaOkcRemKHJn9kKms', 3424, 3, 'city'),
(4074, 49.089024, 25.162865, 'ChIJJzZ3n4D8MEcRNVuBTTa6_VQ', 3427, 3, 'city'),
(4075, 48.029129, 23.432285, 'ChIJiyx4gzOPN0cRtFxcBEJry08', 3401, 3, 'city'),
(4076, 48.759937, 26.626673, 'ChIJb17ouL_JM0cRgDdf47ymy8I', 3398, 3, 'city'),
(4077, 53.465832, 64.036377, 'ChIJUzTPnOmGy0MRb_9ySZciBhs', 4078, 3, 'city'),
(4078, 51.507710, 64.047905, 'ChIJi9flewxqMkIRGpyhYRgn0TQ', 4079, 2, 'region'),
(4079, 48.019573, 66.923683, 'ChIJR6n87AcQqTgRGw6z5C-Ee18', NULL, 1, 'country'),
(4080, 60.359955, 29.738077, 'ChIJ0bCRP20Hl0YRmErmv3TT8sQ', 3823, 3, 'city'),
(4081, 50.713409, 28.023298, 'ChIJ-Q-cYR9UKUcRgnXwpDl8jIw', 3389, 3, 'city'),
(4082, 48.258064, 36.725586, 'ChIJgwrrwY1n3kARLKE-TUFoGIQ', 3408, 3, 'city'),
(4083, 49.754642, 29.295462, 'ChIJ6WoexyPOLEcRguYx1oc9zNI', 3389, 3, 'city'),
(4084, 47.284885, 29.129229, 'ChIJDfQhbSJZyUAR2DFkl9Zzhy8', 3505, 2, 'city'),
(4085, 50.333973, 26.650084, 'ChIJ8Tm3pof-LkcReVircFLYItE', 3398, 3, 'city'),
(4086, 50.744476, 26.017193, 'ChIJERiPVZZnL0cRZ5PKyPYscMY', 3378, 3, 'city'),
(4087, 57.159012, 65.551666, 'ChIJoU-pyF7hu0MR_gy9DYXQt14', 4088, 3, 'city'),
(4088, 56.963440, 66.948280, 'ChIJN9hfSngypEMRkJhFUq_GAwE', 3416, 2, 'region'),
(4089, 50.317116, 30.298166, 'ChIJ05GAS4a21EARYeLRXmjSoWQ', 3387, 3, 'city'),
(4090, 50.743889, 31.385942, 'ChIJjzLEH-UU1UARzKlRRGb1EqA', 3385, 3, 'city'),
(4091, 49.915733, 24.209749, 'ChIJsyNdawTFOkcRZBAJQ7mDNkU', 3424, 3, 'city'),
(4092, 50.454044, 28.673552, 'ChIJYcA_G9iJK0cRz2CwdGUHncg', 3389, 3, 'city'),
(4093, 51.085304, 25.010931, 'ChIJm-NCCHPLJUcR2KfsmDL2dXg', 3381, 3, 'city'),
(4094, 51.628811, 24.968847, 'ChIJH4-kH_OWJkcR0C6VdPiGIcY', 3381, 3, 'city'),
(4095, 48.588482, 37.835423, 'ChIJpyAEd2XC30ARtc3trcqxum4', 3447, 3, 'city'),
(4096, 48.047764, 33.878120, 'ChIJj_GumnAP20ARQJZp9KUge-o', 3408, 3, 'city'),
(4097, 51.201458, 32.773777, 'ChIJR-CbWIBbKkERLrgdWvdTiMw', 3385, 3, 'city'),
(4098, 47.712990, 35.533340, 'ChIJ3Tpcj-pT3EARq3xMNG6V-4M', 3410, 3, 'city'),
(4099, 48.332771, 23.141386, 'ChIJb15TjJ7MOUcRVvFaMRCSKes', 3401, 3, 'city'),
(4100, 59.345303, 29.956291, 'ChIJN7dR1Sv-lUYRhPm-8JaNoNc', 3823, 3, 'city'),
(4101, 47.785454, 34.935688, 'ChIJ1QWRcIqJ3EARWuqERKrlQyE', 3410, 3, 'city'),
(4102, 46.818768, 32.595837, 'ChIJT-RxdEcVxEARZn722LVmezs', 3391, 3, 'city'),
(4103, 49.042156, 25.455181, 'ChIJAfrKXMgFMUcRMmtVFDHDagE', 3427, 3, 'city'),
(4104, 51.195473, 33.828812, 'ChIJvcEToqPVK0ERwKK5VROM2gY', 3396, 3, 'city'),
(4105, 46.596390, 33.229832, 'ChIJ4cNCGhTtw0ARYkTwHJsQK-E', 3391, 3, 'city'),
(4106, 48.259090, 23.375677, 'ChIJFb1CMrbUOUcRZhW2TPua_vY', 3401, 3, 'city'),
(4107, 50.393242, 30.153999, 'ChIJPeR-xWJKK0cR5mnwCYnaYRY', 3387, 3, 'city'),
(4108, 49.722088, 29.211250, 'ChIJlSRyjP7QLEcRVp91qOfudjg', 3389, 3, 'city'),
(4109, 47.973969, 24.008625, 'ChIJv2Q3ZGalN0cRQq48pLoXnhE', 3401, 3, 'city'),
(4110, 49.627495, 36.146915, 'ChIJZwLlQORiJ0ERDosiZODyrO8', 3383, 3, 'city'),
(4111, 48.633106, 35.337757, 'ChIJG4aAI2BP2UARyBmcQ-xQWyA', 3408, 3, 'city'),
(4112, 51.787483, 69.159164, 'ChIJYU4rXDQMSUIRHuwsFaUfSl8', 4113, 3, 'city'),
(4113, 51.916531, 69.411049, 'ChIJEeO49_o5SUIRLq2115NPy_E', 4079, 2, 'region'),
(4114, 52.970867, 36.050671, 'ChIJpzBYSJYQMkERQ20TD2mxvKk', 4115, 3, 'city'),
(4115, 52.745018, 36.484962, 'ChIJDU8FOJPwMUER22dHyN39iUM', 3416, 2, 'region'),
(4116, 49.709671, 35.163689, 'ChIJaWDueeoS2EARZqTHvbxb-RA', 3403, 3, 'city'),
(4117, 48.480183, 24.867065, 'ChIJDQDBtNfWNkcRmlTGvFcOta8', 3483, 3, 'city'),
(4118, 52.665668, 36.369316, 'ChIJW9JUZEnjMUEReZHuZizMK1s', 4115, 3, 'city'),
(4119, 51.522190, 30.745712, 'ChIJN9V16_Rl1UYR6ikj7bkFB8U', 3387, 3, 'city'),
(4120, 49.906139, 25.744455, 'ChIJRXIzWVLaL0cRhC6xn_s7Q28', 3427, 3, 'city'),
(4121, 46.414444, 30.269108, 'ChIJ58n1HsR4yEARYj0gTh-OzqI', 3418, 3, 'city'),
(4122, 47.087566, 30.597776, 'ChIJzT84_h-1yEARith-MkHQ4Uw', 3418, 3, 'city'),
(4123, 50.024658, 32.728149, 'ChIJEQkpRGBi1kARMM3xUjkb8GY', 3403, 3, 'city'),
(4124, 48.980637, 24.199755, 'ChIJR6FEceOmMEcRgHjOV3JJRM8', 3483, 3, 'city'),
(4125, 48.665844, 30.817434, 'ChIJDVbXkCPx0UARumYKQxAWPXk', 3422, 3, 'city'),
(4126, 49.999832, 26.416040, 'ChIJ8wLAe65JLkcRGgTpAUoElto', 3398, 3, 'city'),
(4127, 50.119133, 30.763445, 'ChIJ1_t4A5mR1EARDVLn8mXgCMs', 3387, 3, 'city'),
(4128, 48.478725, 31.669174, 'ChIJ3cobzyE10EARfChMKaB6Xmk', 3422, 3, 'city'),
(4129, 49.526535, 29.919724, 'ChIJ1R8R1QYZ00ARnAH_HookgFs', 3387, 3, 'city'),
(4130, 50.238510, 28.836983, 'ChIJCXobyVl9LEcRXXpkXpfYZuk', 3389, 3, 'city'),
(4131, 49.540573, 30.877205, 'ChIJWd9yvuuW00ARnLLkHsDIFdw', 3387, 3, 'city'),
(4132, 54.031479, 41.713387, 'ChIJO443DuzkR0ERcoYDJia9iRc', 4133, 3, 'city'),
(4133, 54.387596, 41.259567, 'ChIJI_OrHRUvSEERzelNFHeHaD0', 3416, 2, 'region'),
(4134, 49.958706, 23.156982, 'ChIJf-8DxAJpO0cRinX6pqQuwt0', 3424, 3, 'city'),
(4135, 51.590935, 31.296026, 'ChIJDcAuawA51UYRFwgispJ3T08', 3385, 3, 'city'),
(4136, 49.833855, 36.324013, 'ChIJlSWMAMlzJ0ER9g6xR8uvOg4', 3383, 3, 'city'),
(4137, 49.123772, 24.731316, 'ChIJTT7YpPySMEcRe_VX4-wc9f4', 3483, 3, 'city'),
(4138, 49.521004, 39.562065, 'ChIJOafgLSvkIUERToinsXTm-Co', 3559, 3, 'city'),
(4139, 49.796139, 28.648218, 'ChIJ74NKwtfiLEcR8AEmAuS64VE', 3461, 3, 'city'),
(4140, 48.543434, 32.062840, 'ChIJeejOJKBF0EARU6m_4CZEQNs', 3422, 3, 'city'),
(4141, 47.779793, 37.249023, 'ChIJdZOfyQ-s4EARyiAbKdp2wUA', 3447, 3, 'city'),
(4142, 49.031219, 30.249699, 'ChIJfaORYIdD0kARKBCOsPBW0Zc', 3394, 3, 'city'),
(4143, 49.906582, 32.681904, 'ChIJ8SL4uBaM1kARkIGgjL1kDek', 3403, 3, 'city'),
(4144, 48.743095, 35.293476, 'ChIJ283ZWMNA2UARcrP-epuzSCs', 3408, 3, 'city'),
(4145, 46.858410, 31.384966, 'ChIJt0BataIKxkARiP1bpuYkpFM', 3406, 3, 'city'),
(4146, 51.076275, 25.074865, 'ChIJKdpeAuPLJUcRIAIooScyJZQ', 3381, 3, 'city'),
(4147, 48.923347, 23.878593, 'ChIJB1bK-28EOkcRqrBzIk16vs0', 3483, 3, 'city'),
(4148, 49.556896, 32.093201, 'ChIJDW_x2O2x1kARUhfNqMjPOiY', 3394, 3, 'city'),
(4149, 50.262318, 30.289282, 'ChIJgz3-him31EAR_ayvafT3_w8', 3387, 3, 'city'),
(4150, 49.521393, 28.544950, 'ChIJlbZiG9YVLUcRDXMj0_CeIrk', 3461, 3, 'city'),
(4151, 46.618767, 31.538794, 'ChIJka5MdZpkxkARhC8bOnENnUM', 3406, 3, 'city'),
(4152, 51.661427, 24.523352, 'ChIJ-fTn4ieuJkcR5ErKJMRWeLY', 3381, 3, 'city'),
(4153, 50.028667, 27.029379, 'ChIJWy1CX6hlLkcRCry5yJxhUWs', 3398, 3, 'city'),
(4154, 48.099976, 29.124264, 'ChIJDwyQaEZOzEARL1zCvUB0Hgs', 3418, 3, 'city'),
(4155, 48.144016, 29.423409, 'ChIJIU5jYKvKzUAR02rFfqIlhJQ', 3461, 3, 'city'),
(4156, 50.486355, 24.280155, 'ChIJ1c9eA5jgJEcRPF00GBOpKDY', 3424, 3, 'city'),
(4157, 46.123631, 32.291271, 'ChIJ9xkM8DkzwUAR3I0N0nGowIU', 3391, 3, 'city'),
(4158, 49.565933, 34.066521, 'ChIJ8Qu8q3GC10ARXfP0fIGqkM4', 3403, 3, 'city'),
(4159, 46.785206, 38.993294, 'ChIJ5Ym1nMxt5EARfKO7P_Nd1Fc', 3781, 3, 'city'),
(4160, 46.077164, 32.529938, 'ChIJtZophQM6wUARTmNoSZkzkyE', 3391, 3, 'city'),
(4161, 50.810074, 35.147644, 'ChIJib8YEyDxKEERSsHmtW-Wx3w', 3396, 3, 'city'),
(4162, 50.874538, 29.220093, 'ChIJpcr96BiwK0cRQACEiDQTPlg', 3389, 3, 'city'),
(4163, 48.246559, 31.406048, 'ChIJZasKrnnVz0AReC_nEQV1kcI', 3422, 3, 'city'),
(4164, 49.163383, 25.707209, 'ChIJ-RhD3WOmMUcRMApbLqYJyPY', 3427, 3, 'city'),
(4165, 48.598888, 31.291933, 'ChIJD4MPfh7B0UARjx08Hcc4b44', 3422, 3, 'city'),
(4166, 48.212906, 29.342163, 'ChIJlUwdS5OzzUARUi13biTXHAU', 3461, 3, 'city'),
(4167, 49.919418, 27.766684, 'ChIJpQFvKb7LLUcRtOC9CNWOxrs', 3389, 3, 'city'),
(4168, 50.521496, 30.906630, 'ChIJC7cv7ePe1EARU5miwywiTcQ', 3387, 3, 'city'),
(4169, 49.976189, 27.226429, 'ChIJy7_1QJZ5LkcRCDuTlMxnR1I', 3398, 3, 'city'),
(4170, 49.705200, 31.295179, 'ChIJu8mxzVb100ARPH2mPdmE7tY', 3394, 3, 'city'),
(4171, 50.255836, 30.232817, 'ChIJncJLW3-x1EARCBw8I0XCjEk', 3387, 3, 'city'),
(4172, 50.422134, 46.813412, 'ChIJc4L-pORZEkERE_UL8-MPe4k', 3744, 3, 'city'),
(4173, 50.462234, 32.189331, 'ChIJD0E2yDi_1UARC2aMo5hDKXU', 3385, 3, 'city'),
(4174, 53.469112, 43.937305, 'ChIJP2MBEufSQEERjPCFDUq0AVE', 3817, 3, 'city'),
(4175, 48.628242, 38.372715, 'ChIJgXPdiFgH4EARC7EvqQVdos8', 3559, 3, 'city'),
(4176, 46.314381, 34.094086, 'ChIJjxIGDPo4wkARs4jqItm0pH4', 3391, 3, 'city'),
(4177, 49.839954, 26.417099, 'ChIJj9x4t6s7LkcRMmFbCamrdEQ', 3398, 3, 'city'),
(4178, 52.192692, 25.123554, 'ChIJTR2Tu34vJ0cRUWrH7JSm588', 3973, 3, 'city'),
(4179, 49.273685, 38.925549, 'ChIJYT56QxUzIEERl6nHi5zkV4Q', 3559, 3, 'city'),
(4180, 48.147514, 37.297134, 'ChIJkd9XEqQk3kARe1AsF0ymR4w', 3447, 3, 'city'),
(4181, 50.107815, 25.727667, 'ChIJ7d6i9yjAL0cR5CIOxfUuITY', 3427, 3, 'city'),
(4182, 43.237679, 76.962860, 'ChIJd_wkbf9ugzgR5-q0ZGPnqzk', 4079, 2, 'city'),
(4183, 48.228569, 26.533255, 'ChIJrWd9y-GGM0cRHCjpoSgkU4Q', 3450, 3, 'city'),
(4184, 48.408413, 27.993605, 'ChIJFXl7TDHTzEARRou6Jr9TrIU', 3461, 3, 'city'),
(4185, 49.749607, 24.009287, 'ChIJiXbllanoOkcR3q0nfeALZVM', 3424, 3, 'city'),
(4186, 51.400127, 25.608795, 'ChIJzd-0KCVwJkcR7XwZ3pbOjsY', 3381, 3, 'city'),
(4187, 50.348774, 26.925083, 'ChIJLYecvLXxLkcRkGuYi1vDI3I', 3398, 3, 'city'),
(4188, 46.227840, 30.528391, 'ChIJUa5O-obGx0ARnhhLO-Vt89A', 3418, 3, 'city'),
(4189, 49.329464, 32.214626, 'ChIJO93WTbA20UARVo6IwT7Xilw', 3394, 3, 'city'),
(4190, 50.205162, 37.105965, 'ChIJL9pKe0HvJkERvFByY0uy7Bk', 3383, 3, 'city'),
(4191, 46.626705, 32.441372, 'ChIJT4LLP00ZxEARDk-9EBu4TOA', 3391, 3, 'city'),
(4192, 47.660713, 36.273239, 'ChIJt1o26Z6V3UARfXROxX-VoAw', 3410, 3, 'city'),
(4193, 47.479275, 34.504585, 'ChIJF9f9yYW53EAR4JOIOTUX3yA', 3410, 3, 'city'),
(4194, 47.081512, 29.850531, 'ChIJyXO_ssImyUARmKJKovqxcrM', 3418, 3, 'city'),
(4195, 49.840870, 35.314579, 'ChIJHZ5jav4K2EARzs-z2dfTS5M', 3383, 3, 'city'),
(4196, 50.274288, 32.935619, 'ChIJc3HcPZBy1kAR825-yjVbKFY', 3403, 3, 'city'),
(4197, 48.558224, 32.900146, 'ChIJB04xuJiB0EAR8alhFoUHPJs', 3422, 3, 'city'),
(4198, 48.132446, 34.647209, 'ChIJS-zQHriP20AR-BBkPF-9ZoU', 3408, 3, 'city'),
(4199, 49.873192, 36.934441, 'ChIJPQe7eMo8J0ERGHD7uvsYB-0', 3383, 3, 'city'),
(4200, 48.068497, 28.722345, 'ChIJ3Xib1nD2zEARN7XiDQbiSaw', 3852, 3, 'city'),
(4201, 55.652733, 37.590649, 'ChIJR3_k3CizSkERm0cXWhRfkFU', 4202, 3, 'city'),
(4202, 55.755825, 37.617298, 'ChIJybDUc_xKtUYRTM9XV8zWRD0', 3416, 2, 'region'),
(4203, 47.865898, 30.590549, 'ChIJkaZjAkRXzkARzvi6TxOmtYs', 3406, 3, 'city'),
(4204, 48.723427, 30.697296, 'ChIJ9c4FOP0d0kARYpeuzCepqG0', 3422, 3, 'city'),
(4205, 48.084850, 30.782219, 'ChIJ64Fx7IdHzkAR2p6dU-vYZ6Q', 3406, 3, 'city'),
(4206, 47.534222, 35.899124, 'ChIJATIGdSmh3UARLhIgqV66Whs', 3410, 3, 'city'),
(4207, 46.912552, 37.104591, 'ChIJUYL9SUBT5kAR2JS_GEfoDtU', 3447, 3, 'city'),
(4208, 44.334846, 132.556519, 'ChIJN3aKHClGrV8Rw31ryccgLU8', 4209, 3, 'city'),
(4209, 45.052563, 135.000000, 'ChIJN2Xp3lfbql8R_qp_YAZ9e34', 3416, 2, 'region'),
(4210, 51.046642, 30.185087, 'ChIJD2ZnVyDsKkcR6lvSd_-SC1k', 3387, 3, 'city'),
(4211, 49.793751, 25.373985, 'ChIJCbSsqyQfMEcR3vgPMHEFyYA', 3427, 3, 'city'),
(4212, 48.006931, 23.769648, 'ChIJoao8Lx2YN0cRGHXawxUTjT0', 3401, 3, 'city'),
(4213, 51.650200, 27.237011, 'ChIJpV5GlypnKEcR3oyUQ8ZP-Ek', 3378, 3, 'city'),
(4214, 47.375381, 35.307911, 'ChIJA7YfYNIe3UARox4bNZT91UQ', 3410, 3, 'city'),
(4215, 42.871189, 131.378448, 'ChIJcaP_C6RptF8RJaquIgaW3FU', 4209, 3, 'city'),
(4216, 50.440163, 30.287947, 'ChIJe6LIuCzL1EARfjCTUNwnfQs', 3387, 3, 'city'),
(4217, 48.783886, 28.481144, 'ChIJs4dnfwdLzUAR3hTzS6iPKM0', 3461, 3, 'city'),
(4218, 48.481556, 24.581917, 'ChIJ_U6UhDokN0cR8u8DKfBlVBw', 3483, 3, 'city'),
(4219, 49.096512, 33.572121, 'ChIJV9c335tW10AR3MvOFm2SD4Q', 3403, 3, 'city'),
(4220, 50.138123, 30.737352, 'ChIJpeev0VCR1EARsoB4pUCiBOA', 3387, 3, 'city'),
(4221, 47.709530, 35.215260, 'ChIJRcYmRy_23EARuEWL8YkwGlA', 3410, 3, 'city'),
(4222, 49.070251, 23.854315, 'ChIJXSCxop0ROkcRhfMFF0hwPLU', 3483, 3, 'city'),
(4223, 50.507763, 25.611076, 'ChIJ184h5SaAL0cRelDXILfYvvY', 3378, 3, 'city'),
(4224, 51.284103, 24.348373, 'ChIJlSKCLqgRJEcR7F-ITlWGqiI', 3381, 3, 'city'),
(4225, 50.160461, 37.337547, 'ChIJsVnXfE3CJkERXBL7IwtYiNg', 3383, 3, 'city'),
(4226, 49.883255, 34.004040, 'ChIJVfqFeGW-10ARYPaeDAtIcKg', 3403, 3, 'city'),
(4227, 50.647404, 30.414696, 'ChIJXXJdh8srK0cRokLSFOh7EmU', 3387, 3, 'city'),
(4228, 46.723080, 36.354122, 'ChIJCxpfgBrb50ARCjIW2MPHFaU', 3410, 3, 'city'),
(4229, 50.355415, 26.567327, 'ChIJq-TLdOIBL0cRtJ8BCSoniI8', 3378, 3, 'city'),
(4230, 49.870029, 29.943142, 'ChIJSwcILmdP00ARaED1q5uKygc', 3387, 3, 'city'),
(4231, 50.455677, 30.174110, 'ChIJHeqNeDM0K0cRZCKHfOpSsBg', 3387, 3, 'city'),
(4232, 47.693306, 33.268261, 'ChIJ94Z_n-os20AR5GXUE4STmjU', 3408, 3, 'city'),
(4233, 47.203827, 30.916348, 'ChIJMc83lGtFz0AR498B4x5FTNk', 3418, 3, 'city'),
(4234, 50.412300, 30.360193, 'ChIJ6ZuohbvL1EARBi69rgUtfCE', 3387, 3, 'city'),
(4235, 51.564960, 32.954960, 'ChIJ7c7_baMHK0ERrbN3vTzGf5o', 3385, 3, 'city'),
(4236, 50.748455, 26.820438, 'ChIJb3TeVggmL0cRnJiYi1pXWR0', 3378, 3, 'city'),
(4237, 48.259563, 24.345074, 'ChIJnXBN88AbN0cRpm1SBL0dotA', 3401, 3, 'city'),
(4238, 46.152821, 30.331921, 'ChIJj3H-oDzox0ARDo92RM9SVl0', 3418, 3, 'city'),
(4239, 57.971951, 102.619591, 'ChIJfxlTHX7i71wR---5yzOfVLg', 4240, 3, 'city'),
(4240, 56.132141, 103.948624, 'ChIJn7wx-uZ5WFwRcJZFUq_GAwE', 3416, 2, 'region'),
(4241, 50.335060, 31.356848, 'ChIJC_5VSiv_1EARAtlK10WcUxw', 3387, 3, 'city'),
(4242, 49.073147, 26.183172, 'ChIJ24URGYWEMUcRomP2LYt8gbg', 3427, 3, 'city'),
(4243, 48.984806, 24.704681, 'ChIJe--kD57qMEcRQMnaLgDlDmI', 3483, 3, 'city'),
(4244, 51.630451, 26.183022, 'ChIJ_3UO9N45JkcREktgfH8i8ck', 3378, 3, 'city'),
(4245, 49.474503, 24.129059, 'ChIJz_XlyxOKOkcR6mErQ-xAiq8', 3424, 3, 'city'),
(4246, 48.620461, 29.327724, 'ChIJu9LRR0R2zUARiRgYe4j7ylk', 3461, 3, 'city'),
(4247, 49.260567, 24.626881, 'ChIJoTx5hwWGMEcRf43PNfhC1Js', 3483, 3, 'city'),
(4248, 55.851604, 48.537151, 'ChIJ41eXpwbjW0ERgQ_24RoZLzs', 4249, 3, 'city'),
(4249, 55.180237, 50.726395, 'ChIJaf-Qi0dEXkERaIvmnXcqnhI', 3416, 2, 'region'),
(4250, 49.603928, 6.593718, 'ChIJ4fk0bIFxlUcR4BbbENXUIgQ', 4251, 3, 'city'),
(4251, 50.118347, 7.308953, 'ChIJY4Hac2nMl0cR_M8OuHuSeBc', 4252, 2, 'region'),
(4252, 51.165691, 10.451526, 'ChIJa76xwh5ymkcRW-WRjmtd6HU', NULL, 1, 'country'),
(4253, 51.836926, 46.753941, 'ChIJpYvAqJVWE0ERLadaTEr-qus', 3416, 2, 'region'),
(4254, 49.457626, 35.120007, 'ChIJ-clDsbpE2EARKnxOi2ib1a0', 3403, 3, 'city'),
(4255, 53.620785, 56.391006, 'ChIJOW_-UtQj2EMR-gez54d9p8M', 4256, 3, 'city'),
(4256, 54.231216, 56.164528, 'ChIJ_UeUVQW02UMRNFQ0VFqKPHA', 3416, 2, 'region'),
(4257, 46.848564, 34.380424, 'ChIJ6dUIAukdw0ARSikkvXlKvm4', 3391, 3, 'city'),
(4258, 49.982941, 36.090435, 'ChIJVx6D0FSfJ0ERMqnPIsqAQT0', 3383, 3, 'city'),
(4259, 50.109722, 36.115501, 'ChIJye4bwzq7J0ERKMx0RYCzMA0', 3383, 3, 'city'),
(4260, 49.291248, 23.427185, 'ChIJlX3n1RVKOkcRa-aIuZpYHHM', 3424, 3, 'city'),
(4261, 50.834953, 25.882986, 'ChIJ_8X7H4xfL0cRcgIDi_TtB4I', 3381, 3, 'city'),
(4262, 48.245693, 23.489792, 'ChIJ9fDQH_B_N0cRQgAC681JBuQ', 3401, 3, 'city'),
(4263, 47.631145, 29.779116, 'ChIJIb5cA--8zkARKkmG3DetIko', 3418, 3, 'city'),
(4264, 49.938087, 23.388521, 'ChIJNUcwYhMVO0cRUC9CZvow4LE', 3424, 3, 'city'),
(4265, 46.700844, 31.374996, 'ChIJffC4KflxxkARZxpLwgn-e68', 3406, 3, 'city'),
(4266, 50.717205, 33.116531, 'ChIJKTUXEdkDKkERtWiAgM74kC8', 3385, 3, 'city'),
(4267, 54.783241, 20.854141, 'ChIJe0qfPJxy40YRzD4_KNRYjDk', 3962, 3, 'city'),
(4268, 56.619270, 84.881683, 'ChIJ7cBxVAKXJkMRvCb_gfl5H3Q', 4269, 3, 'city'),
(4269, 58.896988, 82.676552, 'ChIJ_V9fmlJGPUMRGMMELSwHeZ4', 3416, 2, 'region'),
(4270, 50.275196, 30.421911, 'ChIJT8Ofptq51EAR3viJG54bt4U', 3387, 3, 'city'),
(4271, 47.013252, 29.669777, 'ChIJqSQjDoYYyUARwreVr14S1gY', 3418, 3, 'city'),
(4272, 47.825893, 33.497272, 'ChIJw9sPM_gk20AROsm7H4630Gg', 3408, 3, 'city'),
(4273, 49.628834, 25.726177, 'ChIJxa_ywsEtMEcRpkSD1s10S1c', 3427, 3, 'city'),
(4274, 50.238010, 26.259768, 'ChIJ8adS7eyuL0cR_D-0oMAMfuU', 3378, 3, 'city'),
(4275, 49.553028, 34.619892, 'ChIJ7YVtn-Uv2EARNlvB5EArDsw', 3403, 3, 'city'),
(4276, 48.362339, 24.405499, 'ChIJf6jzMpoYN0cR-Mk2m2T3d5M', 3483, 3, 'city'),
(4277, 48.474689, 32.203011, 'ChIJRfLrWkBC0EARVDZlyGgc2IU', 3422, 3, 'city'),
(4278, 46.225052, 34.640850, 'ChIJXbJ6RvZzwkARWcrRsWqaWtE', 3391, 3, 'city'),
(4279, 48.054588, 24.212894, 'ChIJE3zWf_WpN0cRgDDZDpurPSw', 3401, 3, 'city'),
(4280, 46.627426, 31.194740, 'ChIJT58spllrxkARRCvz79QfpRY', 3406, 3, 'city'),
(4281, 48.122143, 37.876595, 'ChIJN_6-V2GG4EARNF12tbu6Bg4', 3447, 3, 'city'),
(4282, 48.793419, 34.015133, 'ChIJ4wq_NA4h2kARnPQfTPaY-9M', 3408, 3, 'city'),
(4283, 50.021404, 35.438824, 'ChIJC8xWB97xJ0ERtmoWiOdXmM0', 3383, 3, 'city'),
(4284, 49.796570, 36.341541, 'ChIJI9vxkf9xJ0ERwjkeFAATfp8', 3383, 3, 'city'),
(4285, 38.077744, 46.296558, 'ChIJ6VhimSUFGkARxuhRcLGD5Gk', 4286, 3, 'city'),
(4286, 37.903572, 46.268211, 'ChIJL3Q-mSMFGkARuogCZ79opXA', 4287, 2, 'region'),
(4287, 32.427910, 53.688046, 'ChIJ8R1rwS7s9z4RzvpRntOVsEA', NULL, 1, 'country'),
(4288, 50.450100, 30.523399, 'ChIJBUVa4U7P1EARBBiQlxMdJV8', 3379, 2, 'region'),
(4289, 47.955708, 23.877579, 'ChIJlZcPamG7N0cRKE2K4obq_-k', 3401, 3, 'city'),
(4290, 48.571529, 25.487165, 'ChIJY-f87jtJMUcR2PnloMVdBx4', 3483, 3, 'city'),
(4291, 48.471340, 38.813442, 'ChIJ3T3k4MgY4EARvJhngg37cqw', 3559, 3, 'city'),
(4292, 48.697746, 38.976646, 'ChIJB1den_TlH0ERnFQ9VDeVxl8', 3559, 3, 'city'),
(4293, 51.334576, 33.864681, 'ChIJf2meNj3aK0ERAmrJ2aY-xt0', 3396, 3, 'city'),
(4294, 49.390411, 28.717798, 'ChIJ_RxEI4g4LUcR-hMOI4dR6Fc', 3461, 3, 'city'),
(4295, 50.381008, 29.932468, 'ChIJZRsaxk1PK0cRFrnfUkO7SMc', 3387, 3, 'city'),
(4296, 47.142139, 39.775810, 'ChIJLZUOM8C840ARWUeqfDsYWlA', 3781, 3, 'city'),
(4297, 50.597404, 31.671957, 'ChIJYeJJXHgJ1UARJsBvC_R_AMI', 3385, 3, 'city'),
(4298, 50.829758, 29.217518, 'ChIJTxQdGjqlK0cRoJPMXWte-CU', 3389, 3, 'city'),
(4299, 48.072739, 26.066732, 'ChIJ47rIdXBsNEcR8kEQt6phS1Q', 3450, 3, 'city'),
(4300, 50.252029, 30.443163, 'ChIJHwVU-I651EARqt8GPOh8gxg', 3387, 3, 'city'),
(4301, 48.266869, 23.098989, 'ChIJYaQlp4czOEcRuPGiTyCNfYk', 3401, 3, 'city'),
(4302, 47.434185, 34.344070, 'ChIJ17Umva2x3EAR2FiqxllR4PA', 3410, 3, 'city'),
(4303, 52.887932, 55.323338, 'ChIJhaKJm6gmfUERqMBWwsGqr94', 4256, 3, 'city'),
(4304, 50.092087, 29.543669, 'ChIJ1YKsw3alLEcRasxhec2GuQA', 3389, 3, 'city'),
(4305, 45.206020, 41.325397, 'ChIJkz2FxbOE-UARw9yr7qAo12g', 4306, 3, 'city'),
(4306, 45.641529, 39.705597, 'ChIJO4mGd7lC8EARgJXxg6WjAgE', 3416, 2, 'region'),
(4307, 48.459324, 38.273182, 'ChIJGxzIoZwL4EARYuPW35dm7Q4', 3447, 3, 'city'),
(4308, 46.360146, 32.352261, 'ChIJ88Uud7GxxkARL_PEJDkazxU', 3391, 3, 'city'),
(4309, 49.920898, 28.185562, 'ChIJL2jc0X1SLEcRvEKWDrbFSfM', 3389, 3, 'city'),
(4310, 49.884647, 36.118031, 'ChIJYbN40AieJ0ER0LXjghraFVQ', 3383, 3, 'city'),
(4311, 50.915665, 25.265238, 'ChIJt_F5kRm_JUcR0AxTwwEur1A', 3381, 3, 'city'),
(4312, 50.557804, 26.266979, 'ChIJvZH9mosSL0cRX2swUnvHnQs', 3378, 3, 'city'),
(4313, 51.452812, 31.073149, 'ChIJx_HaZJVE1UYRNATIL1rsaio', 3385, 3, 'city'),
(4314, 50.219391, 28.720774, 'ChIJg7QyeqljLEcRirOSNHViHqE', 3389, 3, 'city'),
(4315, 48.360279, 24.392221, 'ChIJDes4Yw0YN0cRtMhbgLFEIQo', 3483, 3, 'city'),
(4316, 49.675426, 30.386936, 'ChIJgb6CC1Ny00ARFmyVW7OuE6E', 3387, 3, 'city'),
(4317, 47.457123, 37.864227, 'ChIJLeLfjrcq50ARbM_mVcFEDYQ', 3447, 3, 'city'),
(4318, 47.398758, 29.390661, 'ChIJMxQprFNSyUAR2C0o3zgDsdg', 3418, 3, 'city'),
(4319, 48.804489, 29.718307, 'ChIJh1XTvs9-0kARmpyt-C9Fgtc', 3461, 3, 'city'),
(4320, 50.230358, 30.375738, 'ChIJlwJio3K61EARJExf8l2CYtE', 3387, 3, 'city'),
(4321, 49.074963, 25.974583, 'ChIJl6ZFi4eeMUcR1GArocEZRRg', 3427, 3, 'city'),
(4322, 46.738930, 35.638317, 'ChIJoW1uyZ4D6EARVsRL8LxpGi4', 3410, 3, 'city'),
(4323, 45.400299, 29.596174, 'ChIJBb-UAJQ0uEARtuqGrrj3ZFQ', 3418, 3, 'city'),
(4324, 46.640347, 30.648853, 'ChIJT2Ai6CEnxkAR_uofwYLlSdg', 3418, 3, 'city'),
(4325, 46.628887, 30.999725, 'ChIJd8L7tOI9xkARrGzBlD0__oE', 3418, 3, 'city'),
(4326, 49.836746, 36.551098, 'ChIJrVdbZl0TJ0ERhJJLw9qNllg', 3383, 3, 'city'),
(4327, 44.266705, -76.764168, 'ChIJz_mEl9bZ14kRaoJs2Wo4ngs', 4328, 3, 'city'),
(4328, 34.063343, -117.650887, 'ChIJe2Ld6ts0w4ARkDFY-VrjAwc', 4329, 2, 'region'),
(4329, 56.130367, -106.346771, 'ChIJ2WrMN9MDDUsRpY9Doiq3aJk', NULL, 1, 'country'),
(4330, 52.264248, 33.598686, 'ChIJG9tDx_mRLEERABEfQ6pIvIo', 3396, 3, 'city'),
(4331, 49.407116, 25.007713, 'ChIJaaJ3JrhnMEcRSJuzrKZp-H8', 3427, 3, 'city'),
(4332, 45.131054, 33.594109, 'ChIJ0Ymvbqei6kARr6aNIwtyEbE', 3704, 2, 'city'),
(4333, 50.348488, 30.186489, 'ChIJ782DHC1LK0cR9UGlvi1TaI4', 3387, 3, 'city'),
(4334, 46.142494, 32.952908, 'ChIJESbbsKBpwUARvNghvSRuR8I', 3391, 3, 'city'),
(4335, 50.085655, 32.657867, 'ChIJTRqOwfV81kARXlBlIW97-vY', 3403, 3, 'city'),
(4336, 48.884388, 29.077612, 'ChIJTe85OS1ezUARtehGqbuJ7T0', 3461, 3, 'city'),
(4337, 52.939514, 48.217987, 'ChIJe-RODdGgaUERrw8zF3vlu5g', 3721, 3, 'city'),
(4338, 50.764359, 25.390741, 'ChIJU2BYeJOQJUcRukrVCsbtk3M', 3381, 3, 'city'),
(4339, 50.644459, 13.835284, 'ChIJOX8yMYmOCUcRUPwVZg-vAAQ', 4340, 3, 'city'),
(4340, 50.611904, 13.787009, 'ChIJyauIT6CJCUcRcKgUZg-vAAE', 3834, 2, 'region'),
(4341, 46.356709, 30.627529, 'ChIJ10cfbETMx0ARlLl0iLCtRVY', 3418, 3, 'city'),
(4342, 45.369675, 38.214378, 'ChIJvws9aWPF70ARNB7fmaJXRPM', 4306, 3, 'city'),
(4343, 48.270596, 41.759796, 'ChIJ790vPeJPHEERupEL-846eMI', 3781, 3, 'city'),
(4344, 50.830566, 33.881527, 'ChIJWV85_PuTKUEREqq3HpHKqp4', 3396, 3, 'city'),
(4345, 49.713364, 36.926468, 'ChIJg7mJEic4J0ERFiKIiwcOT7U', 3383, 3, 'city'),
(4346, 49.850780, 35.917091, 'ChIJzWq6bkODJ0ERKEi-IJ_pXuM', 3383, 3, 'city'),
(4347, 47.375492, 30.195835, 'ChIJzwKqBGbXzkARmJAawDlJEac', 3418, 3, 'city'),
(4348, 46.903927, 37.345917, 'ChIJCSlBt6BX5kARGpx-qnj9Xak', 3447, 3, 'city'),
(4349, 46.806393, 32.256088, 'ChIJ4003GoY6xEARWFHYX2iEJZc', 3391, 3, 'city'),
(4350, 50.732880, 27.883490, 'ChIJjeko4D9SKUcRNvFm7RTB2NA', 3389, 3, 'city'),
(4351, 49.485874, 28.341759, 'ChIJtUFlk9oSLUcR1CXI1nA1rMQ', 3461, 3, 'city'),
(4352, 43.641098, 51.198509, 'ChIJ13W8MhcytEERaXcKY-bhFHY', 4353, 3, 'city'),
(4353, 44.590801, 53.849953, 'ChIJPfRWtOSXsUERTH4ltHbJu7Y', 4079, 2, 'region'),
(4354, 48.646992, 25.738312, 'ChIJqbPYtVBbMUcRksyJ2FadQ44', 3427, 3, 'city'),
(4355, 48.809597, 35.259087, 'ChIJ003nfeI_2UARRX3BU3yeBas', 3408, 3, 'city');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Geo_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Geo_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `fullAddress` varchar(1000) NOT NULL,
  `country` char(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=983 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Geo_Locales`
--

INSERT INTO `Frontend_Geo_Locales` (`ID`, `localeID`, `geoID`, `name`, `fullAddress`, `country`) VALUES
(1, 1, 3377, 'Рівне', 'Рівне, Рівненська область, Україна', 'UA'),
(2, 1, 3378, 'Рівненська область', 'Рівненська область, Україна', 'UA'),
(3, 1, 3379, 'Україна', 'Україна', 'UA'),
(4, 2, 3377, 'Ровно', 'Ровно, Ровенская область, Украина', 'UA'),
(5, 2, 3378, 'Ровенская область', 'Ровенская область, Украина', 'UA'),
(6, 2, 3379, 'Украина', 'Украина', 'UA'),
(7, 1, 3380, 'Луцьк', 'Луцьк, Волинська область, Україна', 'UA'),
(8, 1, 3381, 'Волинська область', 'Волинська область, Україна', 'UA'),
(9, 1, 3382, 'Харків', 'Харків, Харківська область, Україна', 'UA'),
(10, 1, 3383, 'Харківська область', 'Харківська область, Україна', 'UA'),
(11, 1, 3384, 'Чернігів', 'Чернігів, Чернігівська область, Україна', 'UA'),
(12, 1, 3385, 'Чернігівська область', 'Чернігівська область, Україна', 'UA'),
(13, 1, 3386, 'Бориспіль', 'Міжнародний аеропорт Бориспіль (KBP), Бориспіль, Київська область, Україна', 'UA'),
(14, 1, 3387, 'Київська область', 'Київська область, Україна', 'UA'),
(15, 1, 3388, 'Житомир', 'Житомир, Житомирська область, Україна', 'UA'),
(16, 1, 3389, 'Житомирська область', 'Житомирська область, Україна', 'UA'),
(17, 1, 3390, 'Херсон', 'Херсон, Херсонська область, Україна', 'UA'),
(18, 1, 3391, 'Херсонська область', 'Херсонська область, Україна', 'UA'),
(19, 1, 3392, 'Узин', 'Узин, Київська область, Україна', 'UA'),
(20, 1, 3393, 'Черкаси', 'Черкаси, Черкаська область, Україна', 'UA'),
(21, 1, 3394, 'Черкаська область', 'Черкаська область, Україна', 'UA'),
(22, 1, 3395, 'Суми', 'Суми, Сумська область, Україна', 'UA'),
(23, 1, 3396, 'Сумська область', 'Сумська область, Україна', 'UA'),
(24, 1, 3397, 'Хмельницький', 'Хмельницький, Хмельницька область, Україна', 'UA'),
(25, 1, 3398, 'Хмельницька область', 'Хмельницька область, Україна', 'UA'),
(26, 1, 3399, 'Київ', 'Київ, Україна, 02000', 'UA'),
(27, 1, 3400, 'Ужгород', 'Ужгород, Закарпатська область, Україна', 'UA'),
(28, 1, 3401, 'Закарпатська область', 'Закарпатська область, Україна', 'UA'),
(29, 1, 3402, 'Полтава', 'Полтава, Полтавська область, Україна', 'UA'),
(30, 1, 3403, 'Полтавська область', 'Полтавська область, Україна', 'UA'),
(31, 1, 3404, 'Гадяч', 'Гадяч, Полтавська область, Україна', 'UA'),
(32, 1, 3405, 'Миколаїв', 'Миколаїв, Миколаївська область, Україна', 'UA'),
(33, 1, 3406, 'Миколаївська область', 'Миколаївська область, Україна', 'UA'),
(34, 1, 3407, 'Кривий Ріг', 'Кривий Ріг, Дніпропетровська область, Україна', 'UA'),
(35, 1, 3408, 'Дніпропетровська область', 'Дніпропетровська область, Україна', 'UA'),
(36, 1, 3409, 'Приморське', 'Приморське, Запорізька область, Україна', 'UA'),
(37, 1, 3410, 'Запорізька область', 'Запорізька область, Україна', 'UA'),
(38, 1, 3411, 'Пологи', 'Пологи, Запорізька область, Україна', 'UA'),
(39, 1, 3412, 'Коростишів', 'Коростишів, Житомирська область, Україна', 'UA'),
(40, 1, 3413, 'Козин', 'Козин, Київська область, Україна', 'UA'),
(41, 1, 3414, 'Новодмитриевка', 'Новодмитриевка, Респ. Дагестан, Росія, 368876', 'RU'),
(42, 1, 3415, 'Республика Дагестан', 'Дагестан, Росія', 'RU'),
(43, 1, 3416, 'Росія', 'Росія', 'RU'),
(44, 1, 3417, 'Чорноморськ', 'Чорноморськ, Одеська область, Україна', 'UA'),
(45, 1, 3418, 'Одеська область', 'Одеська область, Україна', 'UA'),
(46, 1, 3419, 'Первомайськ', 'Первомайськ, Миколаївська область, Україна', 'UA'),
(47, 1, 3420, 'Михайлівка-Рубежівка', 'Михайлівка-Рубежівка, Київська область, Україна, 08110', 'UA'),
(48, 1, 3421, 'Кіровоград', 'Кіровоград, Кіровоградська область, Україна', 'UA'),
(49, 1, 3422, 'Кіровоградська область', 'Кіровоградська область, Україна', 'UA'),
(50, 1, 3423, 'Львів', 'Львів, Львівська область, Україна', 'UA'),
(51, 1, 3424, 'Львівська область', 'Львівська область, Україна', 'UA'),
(52, 1, 3425, 'Тересва', 'Тересва, Закарпатська область, Україна, 90564', 'UA'),
(53, 1, 3426, 'Тернопіль', 'Тернопіль, Тернопільська область, Україна', 'UA'),
(54, 1, 3427, 'Тернопільська область', 'Тернопільська область, Україна', 'UA'),
(55, 1, 3428, 'Скадовськ', 'Скадовськ, Херсонська область, Україна', 'UA'),
(56, 1, 3429, 'Одеса', 'Одеса, Одеська область, Україна', 'UA'),
(57, 1, 3430, 'Новомосковськ', 'Новомосковськ, Тульська область, Росія', 'RU'),
(58, 1, 3431, 'Тульська область', 'Тульська область, Росія', 'RU'),
(59, 1, 3432, 'Запоріжжя', 'Запоріжжя, Запорізька область, Україна', 'UA'),
(60, 1, 3433, 'Хуст', 'Хуст, Закарпатська область, Україна', 'UA'),
(61, 1, 3434, 'Цюрупинськ', 'Цюрупинськ, Херсонська область, Україна', 'UA'),
(62, 1, 3435, 'Виноградів', 'Виноградів, Закарпатська область, Україна', 'UA'),
(63, 1, 3436, 'Фастів', 'Фастів, Київська область, Україна', 'UA'),
(64, 1, 3437, 'Макарів', 'Макарів, Київська область, Україна', 'UA'),
(65, 1, 3438, 'Малехів', 'Малехів, Львівська область, Україна, 80383', 'UA'),
(66, 1, 3439, 'Городок', 'Городок, Білорусь', 'BY'),
(67, 1, 3440, 'Вітебська область', 'Вітебська область, Білорусь', 'BY'),
(68, 1, 3441, 'Білорусь', 'Білорусь', 'BY'),
(69, 1, 3442, 'Тячів', 'Тячів, Закарпатська область, Україна', 'UA'),
(70, 1, 3443, 'Абазівка', 'Абазівка, Полтавська область, Україна, 38715', 'UA'),
(71, 1, 3444, 'Шкло', 'Шкло, Львівська область, Україна', 'UA'),
(72, 1, 3445, 'Кам''янець-Подільський', 'Кам''янець-Подільський, Хмельницька область, Україна', 'UA'),
(73, 1, 3446, 'Краматорськ', 'Краматорськ, Донецька область, Україна', 'UA'),
(74, 1, 3447, 'Донецька область', 'Донецька область, Україна', 'UA'),
(75, 1, 3448, 'Умань', 'Умань, Черкаська область, Україна', 'UA'),
(76, 1, 3449, 'Лужани', 'Лужани, Чернівецька область, Україна, 59342', 'UA'),
(77, 1, 3450, 'Чернівецька область', 'Чернівецька область, Україна', 'UA'),
(78, 1, 3451, 'Знам''янка', 'Знам''янка, Кіровоградська область, Україна', 'UA'),
(79, 1, 3452, 'Дніпродзержинськ', 'Дніпродзержинськ, Дніпропетровська область, Україна', 'UA'),
(80, 1, 3453, 'Оржиця', 'Оржиця, Полтавська область, Україна', 'UA'),
(81, 1, 3454, 'Прилуки', 'Прилуки, Чернігівська область, Україна', 'UA'),
(82, 1, 3455, 'Маріуполь', 'Маріуполь, Донецька область, Україна', 'UA'),
(83, 1, 3456, 'Дніпрорудне', 'Дніпрорудне, Запорізька область, Україна', 'UA'),
(84, 1, 3457, 'Тальне', 'Тальне, Черкаська область, Україна', 'UA'),
(85, 1, 3458, 'Самбір', 'Самбір, Львівська область, Україна', 'UA'),
(86, 1, 3459, 'Бердичів', 'Бердичів, Житомирська область, Україна', 'UA'),
(87, 1, 3460, 'Козятин', 'Козятин, Вінницька область, Україна, 22100', 'UA'),
(88, 1, 3461, 'Вінницька область', 'Вінницька область, Україна', 'UA'),
(89, 1, 3462, 'Чернівці', 'Чернівці, Чернівецька область, Україна', 'UA'),
(90, 1, 3463, 'Вільногірськ', 'Вільногірськ, Дніпропетровська область, Україна', 'UA'),
(91, 1, 3464, 'Каховка', 'Каховка, Херсонська область, Україна', 'UA'),
(92, 1, 3465, 'Васильків', 'Васильків, Київська область, Україна', 'UA'),
(93, 1, 3466, 'Золотоноша', 'Золотоноша, Черкаська область, Україна', 'UA'),
(94, 1, 3467, 'Вижниця', 'Вижниця, Чернівецька область, Україна', 'UA'),
(95, 1, 3468, 'Долинська', 'Долинська, Кіровоградська область, Україна', 'UA'),
(96, 1, 3469, 'Нова Борова', 'Нова Борова, Житомирська область, Україна, 12114', 'UA'),
(97, 1, 3470, 'Казанка', 'Казанка, Миколаївська область, Україна', 'UA'),
(98, 1, 3471, 'Новоданилівка', 'Новоданилівка, Запорізька область, Україна, 72520', 'UA'),
(99, 1, 3472, 'Заречье', 'Заречье, Московская обл., Росія, 143085', 'RU'),
(100, 1, 3473, 'Московская область', 'Московська обл., Росія', 'RU'),
(101, 1, 3474, 'Спасское', 'Спасское, Самарская обл., Росія', 'RU'),
(102, 1, 3475, 'Самарская область', 'Самарська область, Росія', 'RU'),
(103, 1, 3476, 'Красноград', 'Красноград, Харківська область, Україна', 'UA'),
(104, 1, 3477, 'Власівка', 'Власівка, Кіровоградська область, Україна, 27552', 'UA'),
(105, 1, 3478, 'Трускавець', 'Трускавець, Львівська область, Україна, 82200', 'UA'),
(106, 1, 3479, 'Бучач', 'Бучач, Тернопільська область, Україна', 'UA'),
(107, 1, 3480, 'Миргород', 'Миргород, Полтавська область, Україна', 'UA'),
(108, 1, 3481, 'Кринички', 'Кринички, Дніпропетровська область, Україна', 'UA'),
(109, 1, 3482, 'Брошнів-Осада', 'Брошнів-Осада, Івано-Франківська область, Україна', 'UA'),
(110, 1, 3483, 'Івано-Франківська область', 'Івано-Франківська область, Україна', 'UA'),
(111, 1, 3484, 'Ковель', 'Ковель, Волинська область, Україна', 'UA'),
(112, 1, 3485, 'Слов''янськ', 'Слов''янськ, Донецька область, Україна, 84122', 'UA'),
(113, 1, 3486, 'Біла Церква', 'Біла Церква, Київська область, Україна', 'UA'),
(114, 1, 3487, 'Добропілля', 'Добропілля, Донецька область, Україна', 'UA'),
(115, 1, 3488, 'Бровари', 'Бровари, Київська область, Україна', 'UA'),
(116, 1, 3489, 'Бахмут', 'Бахмут, Донецька область, Україна', 'UA'),
(117, 1, 3490, 'Лиман', 'Лиман, Донецька область, Україна', 'UA'),
(118, 1, 3491, 'Чабани', 'Чабани, Київська область, Україна', 'UA'),
(119, 1, 3492, 'Конотоп', 'Конотоп, Сумська область, Україна', 'UA'),
(120, 1, 3493, 'Cézac', 'Lubat, 33620 Cézac, Франція', 'FR'),
(121, 1, 3494, 'Aquitaine Limousin Poitou-Charentes', 'Aquitaine-Limousin-Poitou-Charentes, Франція', 'FR'),
(122, 1, 3495, 'Франція', 'Франція', 'FR'),
(123, 1, 3496, 'Івано-Франківськ', 'Івано-Франківськ, Івано-Франківська область, Україна', 'UA'),
(124, 1, 3497, 'Вінниця', 'Вінниця, Вінницька область, Україна', 'UA'),
(125, 1, 3498, 'Летичів', 'Летичів, Хмельницька область, Україна', 'UA'),
(126, 1, 3499, 'Костянтинівка', 'Костянтинівка, Донецька область, Україна', 'UA'),
(127, 1, 3500, 'Дзержинськ', 'Дзержинськ, Нижньогородська область, Росія', 'RU'),
(128, 1, 3501, 'Нижньогородська область', 'Нижньогородська область, Росія', 'RU'),
(129, 1, 3502, 'Ямпіль', 'Ямпіль, Вінницька область, Україна', 'UA'),
(130, 1, 3503, 'Світловодськ', 'Світловодськ, Кіровоградська область, Україна', 'UA'),
(131, 1, 3504, 'Ташлик', 'Ташлик, Молдова', 'MD'),
(132, 1, 3505, 'Молдова', 'Молдова', 'MD'),
(133, 1, 3506, 'Красні Окни', 'Красні Окни, Одеська область, Україна', 'UA'),
(134, 1, 3507, 'Черкаський Бишкин', 'Черкаський Бишкин, Харківська область, Україна, 63453', 'UA'),
(135, 1, 3508, 'Мелітополь', 'Мелітополь, Запорізька область, Україна', 'UA'),
(136, 1, 3509, 'Стрий', 'Стрий, Львівська область, Україна', 'UA'),
(137, 1, 3510, 'Мукачево', 'вулиця Ужгородська, Мукачево, Закарпатська область, Україна', 'UA'),
(138, 1, 3511, 'Сарни', 'Сарни, Рівненська область, Україна', 'UA'),
(139, 1, 3512, 'П''ятничани', 'П''ятничани, Львівська область, Україна, 82423', 'UA'),
(140, 1, 3513, 'Василівка', 'Василівка, Запорізька область, Україна', 'UA'),
(141, 1, 3514, 'Нововолинськ', 'Нововолинськ, Волинська область, Україна', 'UA'),
(142, 1, 3515, 'Визирка', 'Визирка, Одеська область, Україна, 67543', 'UA'),
(143, 1, 3516, 'Надеждівка', 'Надеждівка, Дніпропетровська область, Україна, 53042', 'UA'),
(144, 1, 3517, 'Роздільна', 'Роздільна, Одеська область, Україна', 'UA'),
(145, 1, 3518, 'Великі Копані', 'Великі Копані, Херсонська область, Україна', 'UA'),
(146, 1, 3519, 'Котовськ', 'Котовськ, Одеська область, Україна', 'UA'),
(147, 1, 3520, 'Матусів', 'Матусів, Черкаська область, Україна', 'UA'),
(148, 1, 3521, 'Піщанка', 'Піщанка, Дніпропетровська область, Україна, 51283', 'UA'),
(149, 1, 3522, 'Овруч', 'Овруч, Житомирська область, Україна', 'UA'),
(150, 1, 3523, 'Гола Пристань', 'Гола Пристань, Херсонська область, Україна', 'UA'),
(151, 1, 3524, 'Моршин', 'Моршин, Львівська область, Україна', 'UA'),
(152, 1, 3525, 'Ічня', 'Ічня, Чернігівська область, Україна', 'UA'),
(153, 1, 3526, 'Веселое', 'Веселое', ''),
(154, 1, 3527, 'Аҧсны Аҳәынҭқарра', 'Аҧсны Аҳәынҭқарра', ''),
(155, 1, 3528, 'Мерефа', 'Мерефа, Харківська область, Україна', 'UA'),
(156, 1, 3529, 'Теплик', 'Теплик, Вінницька область, Україна', 'UA'),
(157, 1, 3530, 'Хорошів', 'Хорошів, Житомирська область, Україна', 'UA'),
(158, 1, 3531, 'Хмільник', 'Хмільник, Вінницька область, Україна', 'UA'),
(159, 1, 3532, 'Вапнярка', 'Вапнярка, Вінницька область, Україна', 'UA'),
(160, 1, 3533, 'Дніпропетровськ', 'Дніпропетровськ, Дніпропетровська область, Україна', 'UA'),
(161, 1, 3534, 'Берислав', 'Берислав, Херсонська область, Україна', 'UA'),
(162, 1, 3535, 'Каланчак', 'Каланчак, Херсонська область, Україна', 'UA'),
(163, 1, 3536, 'Вознесенськ', 'Вознесенськ, Миколаївська область, Україна', 'UA'),
(164, 1, 3537, 'Удич', 'Удич, Вінницька область, Україна, 23853', 'UA'),
(165, 1, 3538, 'Людинь', 'Людинь, Рівненська область, Україна, 34123', 'UA'),
(166, 1, 3539, 'Татарбунари', 'Татарбунари, Одеська область, Україна', 'UA'),
(167, 1, 3540, 'Болград', 'Болград, Одеська область, Україна', 'UA'),
(168, 1, 3541, 'Острог', 'Острог, Рівненська область, Україна', 'UA'),
(169, 1, 3542, 'Лозова', 'Лозова, Харківська область, Україна', 'UA'),
(170, 1, 3543, 'Куп''янськ', 'Куп''янськ, Харківська область, Україна', 'UA'),
(171, 1, 3544, 'Лісові Гринівці', 'Лісові Гринівці, Хмельницька область, Україна, 31340', 'UA'),
(172, 1, 3545, 'Березань', 'Березань, Київська область, Україна', 'UA'),
(173, 1, 3546, 'Теребовля', 'Тернопільська вулиця, Теребовля, Тернопільська область, Україна, 48100', 'UA'),
(174, 1, 3547, 'Чемерівці', 'Чемерівці, Хмельницька область, Україна', 'UA'),
(175, 1, 3548, 'Коломия', 'Коломия, Івано-Франківська область, Україна', 'UA'),
(176, 1, 3549, 'Борщів', 'Борщів, Тернопільська область, Україна', 'UA'),
(177, 1, 3550, 'Мена', 'Мена, Чернігівська область, Україна', 'UA'),
(178, 1, 3551, 'Єрки', 'Єрки, Черкаська область, Україна, 20505', 'UA'),
(179, 1, 3552, 'Кілія', 'Кілія, Одеська область, Україна', 'UA'),
(180, 1, 3553, 'Баштанка', 'Баштанка, Миколаївська область, Україна', 'UA'),
(181, 1, 3554, 'Велика Багачка', 'Велика Багачка, Полтавська область, Україна', 'UA'),
(182, 1, 3555, 'Савинці', 'Савинці, Харківська область, Україна, 64270', 'UA'),
(183, 1, 3556, 'Дружелюбівка', 'Дружелюбівка, Запорізька область, Україна, 70053', 'UA'),
(184, 1, 3557, 'Красноармійськ', 'Красноармійськ, Донецька область, Україна', 'UA'),
(185, 1, 3558, 'Рубіжне', 'Рубіжне, Луганська область, Україна', 'UA'),
(186, 1, 3559, 'Луганська область', 'Луганська область, Україна', 'UA'),
(187, 1, 3560, 'Корсунь-Шевченківський', 'Корсунь-Шевченківський, Черкаська область, Україна', 'UA'),
(188, 1, 3561, 'Ізюм', 'Ізюм, Харківська область, Україна', 'UA'),
(189, 1, 3562, 'Кременчук', 'Кременчук, Полтавська область, Україна', 'UA'),
(190, 1, 3563, 'Павлоград', 'Павлоград, Дніпропетровська область, Україна', 'UA'),
(191, 1, 3564, 'Чаплинка', 'Чаплинка, Херсонська область, Україна, 75200', 'UA'),
(192, 1, 3565, 'Калуш', 'Калуш, Івано-Франківська область, Україна', 'UA'),
(193, 1, 3566, 'Сурсько-Михайлівка', 'Сурсько-Михайлівка, Дніпропетровська область, Україна', 'UA'),
(194, 1, 3567, 'Єланець', 'Єланець, Миколаївська область, Україна', 'UA'),
(195, 1, 3568, 'Драбів', 'Драбів, Черкаська область, Україна', 'UA'),
(196, 1, 3569, 'Верхньодніпровськ', 'Верхньодніпровськ, Дніпропетровська область, Україна', 'UA'),
(197, 1, 3570, 'Ивановка', 'Ивановка, Азербайджан', 'AZ'),
(198, 1, 3571, 'Азербайджан', 'Азербайджан', 'AZ'),
(199, 1, 3572, 'Лугини', 'Лугини, Житомирська область, Україна', 'UA'),
(200, 1, 3573, 'Чоповичі', 'Чоповичі, Житомирська область, Україна', 'UA'),
(201, 1, 3574, 'Тарутине', 'Тарутине, Одеська область, Україна', 'UA'),
(202, 1, 3575, 'Малин', 'Малин, Житомирська область, Україна', 'UA'),
(203, 1, 3576, 'Маневичі', 'Маневичі, Волинська область, Україна', 'UA'),
(204, 1, 3577, 'Криве Озеро', 'Криве Озеро, Миколаївська область, Україна', 'UA'),
(205, 1, 3578, 'Стрижівка', 'Стрижівка, Житомирська область, Україна, 12525', 'UA'),
(206, 1, 3579, 'Буськ', 'Буськ, Львівська область, Україна', 'UA'),
(207, 1, 3580, 'Велика Новосілка', 'Велика Новосілка, Донецька область, Україна', 'UA'),
(208, 1, 3581, 'Глухів', 'Глухів, Сумська область, Україна', 'UA'),
(209, 1, 3582, 'Пустомити', 'Пустомити, Львівська область, Україна', 'UA'),
(210, 1, 3583, 'Підгайці', 'Підгайці, Тернопільська область, Україна', 'UA'),
(211, 1, 3584, 'Біловодськ', 'Біловодськ, Луганська область, Україна', 'UA'),
(212, 1, 3585, 'Балаклія', 'Балаклія, Харківська область, Україна', 'UA'),
(213, 1, 3586, 'Новояворівськ', 'Новояворівськ, Львівська область, Україна, 81053/81054', 'UA'),
(214, 1, 3587, 'Великий Березний', 'Великий Березний, Закарпатська область, Україна', 'UA'),
(215, 1, 3588, 'Страдч', 'Страдч, Львівська область, Україна, 81076', 'UA'),
(216, 1, 3589, 'Залуччя', 'Залуччя, Івано-Франківська область, Україна, 78284', 'UA'),
(217, 1, 3590, 'Червоноград', 'Червоноград, Львівська область, Україна', 'UA'),
(218, 1, 3591, 'Ставище', 'Ставище, Київська область, Україна', 'UA'),
(219, 1, 3592, 'Городенка', 'Городенка, Івано-Франківська область, Україна', 'UA'),
(220, 1, 3593, 'Рогатин', 'Рогатин, Івано-Франківська область, Україна', 'UA'),
(221, 1, 3594, 'Чортків', 'Чортків, Тернопільська область, Україна', 'UA'),
(222, 1, 3595, 'Вовчанськ', 'Вовчанськ, Харківська область, Україна', 'UA'),
(223, 1, 3596, 'Переяслав-Хмельницький', 'Переяслав-Хмельницький, Київська область, Україна', 'UA'),
(224, 1, 3597, 'Бердянськ', 'Бердянськ, Запорізька область, Україна', 'UA'),
(225, 1, 3598, 'Красна Коса', 'Красна Коса, Одеська область, Україна, 67734', 'UA'),
(226, 1, 3599, 'Ромни', 'Ромни, Сумська область, Україна', 'UA'),
(227, 1, 3600, 'Шляхтинці', 'Шляхтинці, Тернопільська область, Україна, 47710', 'UA'),
(228, 1, 3601, 'Поречье', 'Поречье, Білорусь', 'BY'),
(229, 1, 3602, 'Гродненская область', 'Гродненська область, Білорусь', 'BY'),
(230, 1, 3603, 'Юнаківка', 'Юнаківка, Сумська область, Україна, 42317', 'UA'),
(231, 1, 3604, 'Калинівка', 'Калинівка, Вінницька область, Україна', 'UA'),
(232, 1, 3605, 'Високопілля', 'Високопілля, Херсонська область, Україна', 'UA'),
(233, 1, 3606, 'Милятичі', 'Милятичі, Львівська область, Україна, 81139', 'UA'),
(234, 1, 3607, 'Коростень', 'Коростень, Житомирська область, Україна', 'UA'),
(235, 1, 3608, 'Тиврів', 'Тиврів, Вінницька область, Україна', 'UA'),
(236, 1, 3609, 'Шумськ', 'Шумськ, Житомирська область, Україна', 'UA'),
(237, 1, 3610, 'Завалля', 'Завалля, Кіровоградська область, Україна, 26334', 'UA'),
(238, 1, 3611, 'Любашівка', 'Любашівка, Одеська область, Україна', 'UA'),
(239, 1, 3612, 'Снятин', 'Снятин, Івано-Франківська область, Україна', 'UA'),
(240, 1, 3613, 'Александрія', 'Александрія, Олександрія, Єгипет', 'EG'),
(241, 1, 3614, 'Єгипет', 'Єгипет', 'EG'),
(242, 1, 3615, 'Нова Каховка', 'Нова Каховка, Херсонська область, Україна', 'UA'),
(243, 1, 3616, 'Меліоративне', 'Меліоративне, Дніпропетровська область, Україна, 51217', 'UA'),
(244, 1, 3617, 'Миронівка', 'Миронівка, Київська область, Україна, 08800', 'UA'),
(245, 1, 3618, 'Петриківка', 'Петриківка, Дніпропетровська область, Україна', 'UA'),
(246, 1, 3619, 'Вишневе', 'Вишневе, Київська область, Україна, 08132', 'UA'),
(247, 1, 3620, 'Генічеськ', 'Генічеськ, Херсонська область, Україна', 'UA'),
(248, 1, 3621, 'Кролевець', 'Кролевець, Сумська область, Україна', 'UA'),
(249, 1, 3622, 'Волноваха', 'Волноваха, Донецька область, Україна', 'UA'),
(250, 1, 3623, 'Токмак', 'Токмак, Запорізька область, Україна', 'UA'),
(251, 1, 3624, 'Димитров', 'Димитров, Донецька область, Україна', 'UA'),
(252, 1, 3625, 'Старокостянтинів', 'Старокостянтинів, Хмельницька область, Україна', 'UA'),
(253, 1, 3626, 'Обухів', 'Обухів, Київська область, Україна', 'UA'),
(254, 1, 3627, 'Вербка-Мурована', 'Вербка-Мурована, Хмельницька область, Україна, 32142', 'UA'),
(255, 1, 3628, 'Южне', 'Южне, Одеська область, Україна', 'UA'),
(256, 1, 3629, 'Білгород-Дністровський', 'Білгород-Дністровський, Одеська область, Україна', 'UA'),
(257, 1, 3630, 'Володимир-Волинський', 'Володимир-Волинський, Волинська область, Україна', 'UA'),
(258, 1, 3631, 'Корець', 'Корець, Рівненська область, Україна', 'UA'),
(259, 1, 3632, 'Олевськ', 'Олевськ, Житомирська область, Україна', 'UA'),
(260, 1, 3633, 'Сєвєродонецьк', 'Сєвєродонецьк, Луганська область, Україна', 'UA'),
(261, 1, 3634, 'Кам''янка-Дніпровська', 'Кам''янка-Дніпровська, Запорізька область, Україна', 'UA'),
(262, 1, 3635, 'Зарічани', 'Зарічани, Житомирська область, Україна, 12440', 'UA'),
(263, 1, 3636, 'Немирів', 'Немирів, Вінницька область, Україна', 'UA'),
(264, 1, 3637, 'Лисичанськ', 'Лисичанськ, Луганська область, Україна, 93100', 'UA'),
(265, 1, 3638, 'Городниця', 'Городниця, Житомирська область, Україна, 11714', 'UA'),
(266, 1, 3639, 'Зміїв', 'Зміїв, Харківська область, Україна', 'UA'),
(267, 1, 3640, 'Кустолово-Суходілка', 'Кустолово-Суходілка, Полтавська область, Україна, 39433', 'UA'),
(268, 1, 3641, 'Шостка', 'Шостка, Сумська область, Україна', 'UA'),
(269, 1, 3642, 'Броди', 'Броди, Львівська область, Україна', 'UA'),
(270, 1, 3643, 'Думанів', 'Думанів, Хмельницька область, Україна, 32321', 'UA'),
(271, 1, 3644, 'Надвірна', 'Надвірна, Івано-Франківська область, Україна', 'UA'),
(272, 1, 3645, 'Хлібодарське', 'Хлібодарське, Одеська область, Україна, 67667', 'UA'),
(273, 1, 3646, 'Верхівцеве', 'Верхівцеве, Дніпропетровська область, Україна', 'UA'),
(274, 1, 3647, 'Шепетівка', 'Шепетівка, Хмельницька область, Україна', 'UA'),
(275, 1, 3648, 'Лопатин', 'Лопатин, Львівська область, Україна', 'UA'),
(276, 1, 3649, 'Покров', 'Покров, Дніпропетровська область, Україна, 53300', 'UA'),
(277, 1, 3650, 'Сміла', 'Сміла, Черкаська область, Україна', 'UA'),
(278, 1, 3651, 'Звенигородка', 'Звенигородка, Черкаська область, Україна', 'UA'),
(279, 1, 3652, 'Ірпінь', 'Ірпінь, Київська область, Україна', 'UA'),
(280, 1, 3653, 'Закупне', 'Закупне, Хмельницька область, Україна, 31614', 'UA'),
(281, 1, 3654, 'Бородянка', 'Бородянка, Київська область, Україна', 'UA'),
(282, 1, 3655, 'Брусилів', 'Брусилів, Житомирська область, Україна, 12600', 'UA'),
(283, 1, 3656, 'Нікополь', 'Нікополь, Дніпропетровська область, Україна', 'UA'),
(284, 1, 3657, 'Первомайський', 'Первомайський, Харківська область, Україна', 'UA'),
(285, 1, 3658, 'Комсомольське', 'Комсомольське, Донецька область, Україна', 'UA'),
(286, 1, 3659, 'Сватове', 'Сватове, Луганська область, Україна', 'UA'),
(287, 1, 3660, 'Великий Бурлук', 'Великий Бурлук, Харківська область, Україна', 'UA'),
(288, 1, 3661, 'Валки', 'Валки, Харківська область, Україна', 'UA'),
(289, 1, 3662, 'Чугуїв', 'Чугуїв, Харківська область, Україна', 'UA'),
(290, 1, 3663, 'Нова Водолага', 'Нова Водолага, Харківська область, Україна', 'UA'),
(291, 1, 3664, 'Богодухів', 'Богодухів, Харківська область, Україна', 'UA'),
(292, 1, 3665, 'Старий Салтів', 'Старий Салтів, Харківська область, Україна, 62560', 'UA'),
(293, 1, 3666, 'Любе', 'Любе, Дніпропетровська область, Україна, 53120', 'UA'),
(294, 1, 3667, 'Борова', 'Борова, Київська область, Україна', 'UA'),
(295, 1, 3668, 'Ємільчине', 'Ємільчине, Житомирська область, Україна', 'UA'),
(296, 1, 3669, 'Новоселиця', 'Новоселиця, Чернівецька область, Україна', 'UA'),
(297, 1, 3670, 'Дубровиця', 'Дубровиця, Рівненська область, Україна', 'UA'),
(298, 1, 3671, 'Березна', 'Березна, Чернігівська область, Україна', 'UA'),
(299, 1, 3672, 'Кмитів', 'Кмитів, Житомирська область, Україна, 12526', 'UA'),
(300, 1, 3673, 'Отинія', 'Отинія, Івано-Франківська область, Україна', 'UA'),
(301, 1, 3674, 'Ладижин', 'Ладижин, Вінницька область, Україна', 'UA'),
(302, 1, 3675, 'Гродзеве', 'Гродзеве, Черкаська область, Україна, 20350', 'UA'),
(303, 1, 3676, 'Костопіль', 'Костопіль, Рівненська область, Україна, 35000', 'UA'),
(304, 1, 3677, 'Радехів', 'Радехів, Львівська область, Україна, 80200', 'UA'),
(305, 1, 3678, 'Жовті Води', 'Жовті Води, Дніпропетровська область, Україна', 'UA'),
(306, 1, 3679, 'Макіївка', 'Макіївка, Донецька область, Україна', 'UA'),
(307, 1, 3680, 'Донецьк', 'Донецьк, Донецька область, Україна', 'UA'),
(308, 1, 3681, 'Горішні плавні', 'Горішні плавні, Полтавська область, Україна, 39800', 'UA'),
(309, 1, 3682, 'Могилів-Подільський', 'Могилів-Подільський, Вінницька область, Україна', 'UA'),
(310, 1, 3683, 'Гайсин', 'Гайсин, Вінницька область, Україна', 'UA'),
(311, 1, 3684, 'Середина-Буда', 'Середина-Буда, Сумська область, Україна', 'UA'),
(312, 1, 3685, 'Старе', 'Старе, Київська область, Україна, 08362', 'UA'),
(313, 1, 3686, 'Красна Лука', 'Красна Лука, Полтавська область, Україна, 37315', 'UA'),
(314, 1, 3687, 'Захід', 'Захід, Камерун', 'CM'),
(315, 1, 3688, 'Камерун', 'Камерун', 'CM'),
(316, 1, 3689, 'Курахове', 'Курахове, Донецька область, Україна', 'UA'),
(317, 1, 3690, 'Руденківка', 'Руденківка, Полтавська область, Україна, 39323', 'UA'),
(318, 1, 3691, 'Тростянець', 'Тростянець, Сумська область, Україна', 'UA'),
(319, 1, 3692, 'Синельникове', 'Синельникове, Дніпропетровська область, Україна', 'UA'),
(320, 1, 3693, 'Дашів', 'Дашів, Вінницька область, Україна, 22740', 'UA'),
(321, 1, 3694, 'Рава-Руська', 'Рава-Руська, Львівська область, Україна', 'UA'),
(322, 1, 3695, 'Луганськ', 'Луганськ, Луганська область, Україна', 'UA'),
(323, 1, 3696, 'Кольчино', 'Кольчино, Закарпатська область, Україна, 89636', 'UA'),
(324, 1, 3697, 'Прилиманське', 'Прилиманське, Одеська область, Україна, 67820', 'UA'),
(325, 1, 3698, 'Жмеринка', 'Жмеринка, Вінницька область, Україна, 23100', 'UA'),
(326, 1, 3699, 'Буча', 'Буча, Київська область, Україна, 08105', 'UA'),
(327, 1, 3700, 'Збараж', 'Збараж, Тернопільська область, Україна', 'UA'),
(328, 1, 3701, 'Сквира', 'Сквира, Київська область, Україна', 'UA'),
(329, 1, 3702, 'Новоолександрівка', 'Новоолександрівка, Дніпропетровська область, Україна, 52070', 'UA'),
(330, 1, 3703, 'Миколаївка', 'Миколаївка 97546', ''),
(331, 1, 3704, 'Крим', 'Крим', ''),
(332, 1, 3705, 'Тетіїв', 'Тетіїв, Київська область, Україна', 'UA'),
(333, 1, 3706, 'Радивилів', 'Радивилів, Рівненська область, Україна', 'UA'),
(334, 1, 3707, 'Копані', 'Копані, Донецька область, Україна, 85012', 'UA'),
(335, 1, 3708, 'Золочів', 'Золочів, Львівська область, Україна', 'UA'),
(336, 1, 3709, 'Галицинове', 'Галицинове, Миколаївська область, Україна, 57286', 'UA'),
(337, 1, 3710, 'Турка', 'Турка, Львівська область, Україна', 'UA'),
(338, 1, 3711, 'Богородчани', 'Богородчани, Івано-Франківська область, Україна', 'UA'),
(339, 1, 3712, 'Кишинів', 'Кишинів, Молдова', 'MD'),
(340, 1, 3713, 'Сколе', 'Сколе, Львівська область, Україна', 'UA'),
(341, 1, 3714, 'Перечин', 'Перечин, Закарпатська область, Україна', 'UA'),
(342, 1, 3715, 'Марганець', 'Марганець, Дніпропетровська область, Україна', 'UA'),
(343, 1, 3716, 'Благовещенка', 'Благовещенка, Алтайский край, Росія', 'RU'),
(344, 1, 3717, 'Алтайский край', 'Алтайський край, Росія', 'RU'),
(345, 1, 3718, 'Лісники', 'Лісники, Київська область, Україна, 08172', 'UA'),
(346, 1, 3719, 'Мильчиці', 'Мильчиці, Львівська область, Україна, 81531', 'UA'),
(347, 1, 3720, 'Максимовка', 'Максимовка, Ульяновская обл., Росія, 433319', 'RU'),
(348, 1, 3721, 'Ульяновская область', 'Ульяновська область, Росія', 'RU'),
(349, 1, 3722, 'Стаханов', 'Стаханов, Луганська область, Україна', 'UA'),
(350, 1, 3723, 'Новий Буг', 'Новий Буг, Миколаївська область, Україна', 'UA'),
(351, 1, 3724, 'Лохвиця', 'Лохвиця, Полтавська область, Україна', 'UA'),
(352, 1, 3725, 'Бобринець', 'Бобринець, Кіровоградська область, Україна', 'UA'),
(353, 1, 3726, 'Овідіополь', 'Овідіополь, Одеська область, Україна', 'UA'),
(354, 1, 3727, 'Партизанське', 'Партизанське, Дніпропетровська область, Україна, 52012', 'UA'),
(355, 1, 3728, 'Красилів', 'Красилів, Хмельницька область, Україна, 31000', 'UA'),
(356, 1, 3729, 'Тульчин', 'Тульчин, Вінницька область, Україна', 'UA'),
(357, 1, 3730, 'Яготин', 'Яготин, Київська область, Україна', 'UA'),
(358, 1, 3731, 'Мар''янівка', 'Мар''янівка, Київська область, Україна, 08650', 'UA'),
(359, 1, 3732, 'Мартусівка', 'Мартусівка, Київська область, Україна, 08343', 'UA'),
(360, 1, 3733, 'Полонне', 'Понинка, Полонне, Хмельницька область, Україна', 'UA'),
(361, 1, 3734, 'Крижопіль', 'Крижопіль, Вінницька область, Україна', 'UA'),
(362, 1, 3735, 'Вишгород', 'Вишгород, Київська область, Україна', 'UA'),
(363, 1, 3736, 'Kyle', 'Zarya, Kyle, TX 78640, Сполучені Штати Америки', 'US'),
(364, 1, 3737, 'Texas', 'Техас, Сполучені Штати Америки', 'US'),
(365, 1, 3738, 'Сполучені Штати Америки', 'Сполучені Штати Америки', 'US'),
(366, 1, 3739, 'Шевченкове', 'Шевченкове, Харківська область, Україна', 'UA'),
(367, 1, 3740, 'Білокоровичі', 'Білокоровичі, Житомирська область, Україна, 11055', 'UA'),
(368, 1, 3741, 'Верховина', 'Верховина, Івано-Франківська область, Україна', 'UA'),
(369, 1, 3742, 'Дрогобич', 'Дрогобич, Львівська область, Україна', 'UA'),
(370, 1, 3743, 'Михайловка', 'Михайловка, Волгоградская обл., Росія', 'RU'),
(371, 1, 3744, 'Волгоградская область', 'Волгоградська область, Росія', 'RU'),
(372, 1, 3745, 'Снігурівка', 'Снігурівка, Миколаївська область, Україна', 'UA'),
(373, 1, 3746, 'Ульяновка', 'Ульяновка, Кіровоградська область, Україна', 'UA'),
(374, 1, 3747, 'Кирилловка', 'Кирилловка, Самарская обл., Росія, 445151', 'RU'),
(375, 1, 3748, 'Попільня', 'Попільня, Житомирська область, Україна', 'UA'),
(376, 1, 3749, 'Гайворон', 'Гайворон, Кіровоградська область, Україна', 'UA'),
(377, 1, 3750, 'Бар', 'Бар, Вінницька область, Україна', 'UA'),
(378, 1, 3751, 'Петрівка-Роменська', 'Петрівка-Роменська, Полтавська область, Україна, 37333', 'UA'),
(379, 1, 3752, 'Березне', 'Березне, Рівненська область, Україна', 'UA'),
(380, 1, 3753, 'Трояни', 'Трояни, Запорізька область, Україна, 71151', 'UA'),
(381, 1, 3754, 'Томаківка', 'Томаківка, Дніпропетровська область, Україна', 'UA'),
(382, 1, 3755, 'Липовець', 'Липовець, Вінницька область, Україна', 'UA'),
(383, 1, 3756, 'Руська Лозова', 'Руська Лозова, Харківська область, Україна, 62332', 'UA'),
(384, 1, 3757, 'Кегичівка', 'Кегичівка, Харківська область, Україна', 'UA'),
(385, 1, 3758, 'Нижні Ворота', 'Нижні Ворота, Закарпатська область, Україна, 89130', 'UA'),
(386, 1, 3759, 'Сварицевичі', 'Сварицевичі, Рівненська область, Україна, 34120', 'UA'),
(387, 1, 3760, 'Козероги', 'Козероги, Чернігівська область, Україна, 15556', 'UA'),
(388, 1, 3761, 'Новомиргород', 'Новомиргород, Кіровоградська область, Україна', 'UA'),
(389, 1, 3762, 'Косарі', 'Косарі, Черкаська область, Україна, 20813', 'UA'),
(390, 1, 3763, 'Городище', 'Городище, Черкаська область, Україна', 'UA'),
(391, 1, 3764, 'Високогірне', 'Високогірне, Запорізька область, Україна, 70421', 'UA'),
(392, 1, 3765, 'Новоукраїнка', 'Новоукраїнка, Кіровоградська область, Україна', 'UA'),
(393, 1, 3766, 'Капітанівка', 'Капітанівка, Київська область, Україна, 08112', 'UA'),
(394, 1, 3767, 'Коровій Яр', 'Коровій Яр, Донецька область, Україна, 84413', 'UA'),
(395, 1, 3768, 'Підволочиськ', 'Підволочиськ, Тернопільська область, Україна', 'UA'),
(396, 1, 3769, 'Дарахів', 'Дарахів, Тернопільська область, Україна, 48146', 'UA'),
(397, 1, 3770, 'Кузнецовськ', 'Кузнецовськ, Рівненська область, Україна', 'UA'),
(398, 1, 3771, 'Степанівка', 'Степанівка, Сумська область, Україна', 'UA'),
(399, 1, 3772, 'Горенка', 'Горенка, Київська область, Україна, 08105', 'UA'),
(400, 1, 3773, 'Керч', 'Керч', ''),
(401, 1, 3774, 'Бахчисарай', 'Бахчисарай', ''),
(402, 1, 3775, 'Севастополь', 'Севастополь', ''),
(403, 1, 3776, 'Щасливе', 'Щасливе, Київська область, Україна, 08325', 'UA'),
(404, 1, 3777, 'Свалява', 'Свалява, Закарпатська область, Україна', 'UA'),
(405, 1, 3778, 'Новопетрівка', 'Новопетрівка, Запорізька область, Україна, 71162', 'UA'),
(406, 1, 3779, 'Солоницівка', 'Солоницівка, Харківська область, Україна', 'UA'),
(407, 1, 3780, 'Чертково', 'Чертково, Ростовська область, Росія', 'RU'),
(408, 1, 3781, 'Ростовська область', 'Ростовська область, Росія', 'RU'),
(409, 1, 3782, 'Бершадь', 'Бершадь, Вінницька область, Україна', 'UA'),
(410, 1, 3783, 'Волочиськ', 'Волочиськ, Хмельницька область, Україна', 'UA'),
(411, 1, 3784, 'Скалат', 'Скалат, Тернопільська область, Україна, 47851', 'UA'),
(412, 1, 3785, 'Христинівка', 'Христинівка, Черкаська область, Україна', 'UA'),
(413, 1, 3786, 'Ніжин', 'Ніжин, Чернігівська область, Україна, 16600', 'UA'),
(414, 1, 3787, 'Вертіївка', 'Вертіївка, Чернігівська область, Україна', 'UA'),
(415, 1, 3788, 'Гореничі', 'вулиця Окружна, Гореничі, Київська область, Україна', 'UA'),
(416, 1, 3789, 'Гостомель', 'Гостомель, Київська область, Україна', 'UA'),
(417, 1, 3790, 'Іллінці', 'Іллінці, Вінницька область, Україна', 'UA'),
(418, 1, 3791, 'Плужне', 'Плужне, Хмельницька область, Україна, 30320', 'UA'),
(419, 1, 3792, 'Утконосівка', 'Утконосівка, Одеська область, Україна, 68645', 'UA'),
(420, 1, 3793, 'Верхньокам''янське', 'Верхньокам''янське, Донецька область, Україна, 84525', 'UA'),
(421, 1, 3794, 'Ювілейне', 'Ювілейне, Дніпропетровська область, Україна, 52005', 'UA'),
(422, 1, 3795, 'Саврань', 'Саврань, Одеська область, Україна', 'UA'),
(423, 1, 3796, 'Привольное', 'Привольное, Ставропольский край, Росія', 'RU'),
(424, 1, 3797, 'Ставропольский край', 'Ставропольський край, Росія', 'RU'),
(425, 1, 3798, 'Розділ', 'Розділ, Львівська область, Україна', 'UA'),
(426, 1, 3799, 'Оріхів', 'Оріхів, Запорізька область, Україна', 'UA'),
(427, 1, 3800, 'Жидачів', 'Жидачів, Львівська область, Україна', 'UA'),
(428, 1, 3801, 'Дар''ївка', 'Дар''ївка, Херсонська область, Україна, 75032', 'UA'),
(429, 1, 3802, 'Грицеволя', 'Грицеволя, Львівська область, Україна, 80260', 'UA'),
(430, 1, 3803, 'Івано-Франкове', 'Івано-Франкове, Львівська область, Україна', 'UA'),
(431, 1, 3804, 'Лемешівка', 'Лемешівка, Вінницька область, Україна, 22410', 'UA'),
(432, 1, 3805, 'Усолуси', 'Усолуси, Житомирська область, Україна, 11261', 'UA'),
(433, 1, 3806, 'Соледар', 'Соледар, Донецька область, Україна', 'UA'),
(434, 1, 3807, 'Подгорное', 'Подгорное, Липецкая обл., Росія', 'RU'),
(435, 1, 3808, 'Липецкая область', 'Ліпецька область, Росія', 'RU'),
(436, 1, 3809, 'Нікольське', 'Нікольське, Донецька область, Україна', 'UA'),
(437, 1, 3810, 'Діброва', 'Діброва, Житомирська область, Україна, 11023', 'UA'),
(438, 1, 3811, 'Локачі', 'Локачі, Волинська область, Україна', 'UA'),
(439, 1, 3812, 'Батятичі', 'Батятичі, Львівська область, Україна, 80423', 'UA'),
(440, 1, 3813, 'Вільнянськ', 'Вільнянськ, Запорізька область, Україна', 'UA'),
(441, 1, 3814, 'Копани', 'Копани, Ростовская обл., Росія, 346976', 'RU'),
(442, 1, 3815, 'Димер', 'Димер, Київська область, Україна', 'UA'),
(443, 1, 3816, 'Каменка', 'Каменка, Пензенская обл., Росія', 'RU'),
(444, 1, 3817, 'Пензенская область', 'Пензенська обл., Росія', 'RU'),
(445, 1, 3818, 'Новоград-Волинський', 'Новоград-Волинський, Житомирська область, Україна', 'UA'),
(446, 1, 3819, 'Андрушівка', 'Андрушівка, Житомирська область, Україна', 'UA'),
(447, 1, 3820, 'Єгорівка', 'Єгорівка, Чернігівська область, Україна, 17508', 'UA'),
(448, 1, 3821, 'Нова Одеса', 'Нова Одеса, Миколаївська область, Україна', 'UA'),
(449, 1, 3822, 'Романовка', 'Романовка, Ленинградская обл., Росія, 188670', 'RU'),
(450, 1, 3823, 'Ленинградская область', 'Ленінградська область, Росія', 'RU'),
(451, 1, 3824, 'Котельва', 'Котельва, Полтавська область, Україна', 'UA'),
(452, 1, 3825, 'Пирятин', 'Пирятин, Полтавська область, Україна', 'UA'),
(453, 1, 3826, 'Слов''янськ', 'Харьковский пер., Слов''янськ, Донецька область, Україна', 'UA'),
(454, 1, 3827, 'Срібне', 'Срібне, Чернігівська область, Україна', 'UA'),
(455, 1, 3828, 'Охтирка', 'Охтирка, Сумська область, Україна', 'UA'),
(456, 1, 3829, 'Перебиківці', 'Перебиківці, Чернівецька область, Україна, 60010', 'UA'),
(457, 1, 3830, 'Успенівка', 'Успенівка, Одеська область, Україна, 68242', 'UA'),
(458, 1, 3831, 'Яблуновиця', 'Яблуновиця, Вінницька область, Україна, 22642', 'UA'),
(459, 1, 3832, 'Дуба', 'Дуба, Чеська Республіка', 'CZ'),
(460, 1, 3833, 'Ліберецький край', 'Ліберецький край, Чеська Республіка', 'CZ'),
(461, 1, 3834, 'Чеська Республіка', 'Чеська Республіка', 'CZ'),
(462, 1, 3835, 'Станиця Луганська', 'Станиця Луганська, Луганська область, Україна, 93600', 'UA'),
(463, 1, 3836, 'Аполлонівка', 'Аполлонівка, Дніпропетровська область, Україна, 52406', 'UA'),
(464, 1, 3837, 'Арциз', 'Арциз, Одеська область, Україна', 'UA'),
(465, 1, 3838, 'Дубове', 'Дубове, Закарпатська область, Україна, 90531', 'UA'),
(466, 1, 3839, 'Кагарлик', 'Кагарлик, Київська область, Україна', 'UA'),
(467, 1, 3840, 'Жовква', 'Жовква, Львівська область, Україна', 'UA'),
(468, 1, 3841, 'Солоне', 'Солоне, Дніпропетровська область, Україна', 'UA'),
(469, 1, 3842, 'Старий Самбір', 'Старий Самбір, Львівська область, Україна', 'UA'),
(470, 1, 3843, 'Канів', 'Канів, Черкаська область, Україна', 'UA'),
(471, 1, 3844, 'Бірки', 'Бірки, Харківська область, Україна, 63421', 'UA'),
(472, 1, 3845, 'Дунаївці', 'Дунаївці, Хмельницька область, Україна', 'UA'),
(473, 1, 3846, 'Мостиська', 'Мостиська, Львівська область, Україна', 'UA'),
(474, 1, 3847, 'Ріпки', 'Ріпки, Чернігівська область, Україна', 'UA'),
(475, 1, 3848, 'Демидов', 'Демидов, Смоленська обл., Росія, 216240', 'RU'),
(476, 1, 3849, 'Смоленська обл.', 'Смоленська обл., Росія', 'RU'),
(477, 1, 3850, 'Кіровське', 'Кіровське, Донецька область, Україна', 'UA'),
(478, 1, 3851, 'Катеринівка', 'Катеринівка, Молдова', 'MD'),
(479, 1, 3852, 'Придністров''я', 'Придністров''я, Молдова', 'MD'),
(480, 1, 3853, 'Сімферополь', 'Сімферополь', ''),
(481, 1, 3854, 'Магдалинівка', 'Магдалинівка, Дніпропетровська область, Україна', 'UA'),
(482, 1, 3855, 'Новгород-Сіверський', 'Новгород-Сіверський, Чернігівська область, Україна', 'UA'),
(483, 1, 3856, 'Гнівань', 'Гнівань, Вінницька область, Україна, 23310', 'UA'),
(484, 1, 3857, 'Яблунець', 'Яблунець, Житомирська область, Україна, 11250', 'UA'),
(485, 1, 3858, 'Берегове', 'Берегове, Закарпатська область, Україна', 'UA'),
(486, 1, 3859, 'Брянка', 'Брянка, Луганська область, Україна', 'UA'),
(487, 1, 3860, 'Райківці', 'Райківці, Хмельницька область, Україна, 31356', 'UA'),
(488, 1, 3861, 'Почаїв', 'Почаїв, Тернопільська область, Україна', 'UA'),
(489, 1, 3862, 'Мошни', 'Мошни, Черкаська область, Україна, 19615', 'UA'),
(490, 1, 3863, 'Лубни', 'Лубни, Полтавська область, Україна', 'UA'),
(491, 1, 3864, 'Заводське', 'Заводське, Полтавська область, Україна', 'UA'),
(492, 1, 3865, 'Чорнобай', 'Чорнобай, Черкаська область, Україна', 'UA'),
(493, 1, 3866, 'Великі Сорочинці', 'Великі Сорочинці, Полтавська область, Україна, 37645', 'UA'),
(494, 1, 3867, 'Липова Долина', 'Липова Долина, Сумська область, Україна', 'UA'),
(495, 1, 3868, 'Гребінка', 'Гребінка, Полтавська область, Україна', 'UA'),
(496, 1, 3869, 'Зіньків', 'Зіньків, Полтавська область, Україна', 'UA'),
(497, 1, 3870, 'Апостолове', 'Апостолове, Дніпропетровська область, Україна', 'UA'),
(498, 1, 3871, 'Поляна', 'Поляна, Закарпатська область, Україна', 'UA'),
(499, 1, 3872, 'Dolina', 'Dolina, 9131 Dolina, Австрія', 'AT'),
(500, 1, 3873, 'Kärnten', 'Каринтія, Австрія', 'AT'),
(501, 1, 3874, 'Австрія', 'Австрія', 'AT'),
(502, 1, 3875, 'Осещина', 'Осещина, Київська область, Україна, 07363', 'UA'),
(503, 1, 3876, 'Кагамлик', 'Кагамлик, Полтавська область, Україна, 39043', 'UA'),
(504, 1, 3877, 'Затока', 'Затока, Одеська область, Україна', 'UA'),
(505, 1, 3878, 'Олександрівка', 'Олександрівка, Кіровоградська область, Україна', 'UA'),
(506, 1, 3879, 'Глобине', 'Глобине, Полтавська область, Україна', 'UA'),
(507, 1, 3880, 'Білопілля', 'Білопілля, Сумська область, Україна', 'UA'),
(508, 1, 3881, 'Зборів', 'Зборів, Тернопільська область, Україна', 'UA'),
(509, 1, 3882, 'Голованівськ', 'Голованівськ, Кіровоградська область, Україна', 'UA'),
(510, 1, 3883, 'Покровське', 'Покровське, Ростовська область, Росія', 'RU'),
(511, 1, 3884, 'Манява', 'Манява, Івано-Франківська область, Україна, 77772', 'UA'),
(512, 1, 3885, 'Смига', 'Смига, Рівненська область, Україна, 35680', 'UA'),
(513, 1, 3886, 'Бережани', 'Бережани, Тернопільська область, Україна', 'UA'),
(514, 1, 3887, 'Красний Луч', 'Красний Луч, Луганська область, Україна', 'UA'),
(515, 1, 3888, 'Миролюбівка', 'Миролюбівка, Херсонська область, Україна, 75014', 'UA'),
(516, 1, 3889, 'Горлівка', 'Горлівка, Донецька область, Україна, 84600', 'UA'),
(517, 1, 3890, 'Новгородка', 'Новгородка, Кіровоградська область, Україна', 'UA'),
(518, 1, 3891, 'Іршава', 'Іршава, Закарпатська область, Україна', 'UA'),
(519, 1, 3892, 'Бишів', 'Бишів, Київська область, Україна, 08072', 'UA'),
(520, 1, 3893, 'Чоп', 'Чоп, Закарпатська область, Україна', 'UA'),
(521, 1, 3894, 'Дружківка', 'Дружківка, Донецька область, Україна, 84200', 'UA'),
(522, 1, 3895, 'Малий Кобелячок', 'Малий Кобелячок, Полтавська область, Україна, 39333', 'UA'),
(523, 1, 3896, 'Котовське', 'Котовське, Одеська область, Україна, 67311', 'UA'),
(524, 1, 3897, 'Сурсько-Литовське', 'Сурсько-Литовське, Дніпропетровська область, Україна', 'UA'),
(525, 1, 3898, 'Березнегувате', 'Березнегувате, Миколаївська область, Україна', 'UA'),
(526, 1, 3899, 'Велика Олександрівка', 'Велика Олександрівка, Херсонська область, Україна', 'UA'),
(527, 1, 3900, 'Княжичі', 'Княжичі, Київська область, Україна, 07455', 'UA'),
(528, 1, 3901, 'Великодолинське', 'Великодолинське, Одеська область, Україна', 'UA'),
(529, 1, 3902, 'Кремінна', 'Кремінна, Луганська область, Україна', 'UA'),
(530, 1, 3903, 'Пловдив', 'ул. „Киев“ 3, 4000 Пловдив, Болгарія', 'BG'),
(531, 1, 3904, 'Пловдив', 'Пловдив, Болгарія', 'BG'),
(532, 1, 3905, 'Болгарія', 'Болгарія', 'BG'),
(533, 1, 3906, 'Лебедин', 'Лебедин, Сумська область, Україна', 'UA'),
(534, 1, 3907, 'Червоне', 'Червоне, Житомирська область, Україна, 13434', 'UA'),
(535, 1, 3908, 'Кашперівка', 'Кашперівка, Київська область, Україна, 09812', 'UA'),
(536, 1, 3909, 'Любомль', 'Любомль, Волинська область, Україна', 'UA'),
(537, 1, 3910, 'Дитятин', 'Дитятин, Івано-Франківська область, Україна, 77140', 'UA'),
(538, 1, 3911, 'Сатанів', 'Сатанів, Хмельницька область, Україна, 32034', 'UA'),
(539, 1, 3912, 'Копайгород', 'Копайгород, Вінницька область, Україна, 23053', 'UA'),
(540, 1, 3913, 'Здолбунів', 'Здолбунів, Рівненська область, Україна', 'UA'),
(541, 1, 3914, 'Рокитне', 'Рокитне, Київська область, Україна', 'UA'),
(542, 1, 3915, 'Лубянка', 'Лубянка, Польща', 'PL'),
(543, 1, 3916, 'Куявсько-Поморське', 'Куявсько-Поморське, Польща', 'PL'),
(544, 1, 3917, 'Польща', 'Польща', 'PL'),
(545, 1, 3918, 'Борзна', 'Борзна, Чернігівська область, Україна', 'UA'),
(546, 1, 3919, 'Баришівка', 'Баришівка, Київська область, Україна', 'UA'),
(547, 1, 3920, 'Краснокутськ', 'Краснокутськ, Харківська область, Україна', 'UA'),
(548, 1, 3921, 'Рубанка', 'Рубанка, Сумська область, Україна, 42144', 'UA'),
(549, 1, 3922, 'Комінтернівське', 'Комінтернівське, Одеська область, Україна', 'UA'),
(550, 1, 3923, 'Вільшанка', 'Вільшанка, Кіровоградська область, Україна', 'UA'),
(551, 1, 3924, 'Погребище', 'Погребище, Вінницька область, Україна', 'UA'),
(552, 1, 3925, 'Стоянка', 'Стоянка, Київська область, Україна, 08114', 'UA'),
(553, 1, 3926, 'Гоголів', 'Гоголів, Київська область, Україна, 07452', 'UA'),
(554, 1, 3927, 'Девладове', 'Девладове, Дніпропетровська область, Україна, 53132', 'UA'),
(555, 1, 3928, 'Микільське-на-Дніпрі', 'Микільське-на-Дніпрі, Дніпропетровська область, Україна, 52470', 'UA'),
(556, 1, 3929, 'Знаменівка', 'Знаменівка, Дніпропетровська область, Україна', 'UA'),
(557, 1, 3930, 'Рудки', 'Рудки, Львівська область, Україна', 'UA'),
(558, 1, 3931, 'Краснопілля', 'Краснопілля, Сумська область, Україна', 'UA'),
(559, 1, 3932, 'Демидівка', 'Демидівка, Рівненська область, Україна', 'UA'),
(560, 1, 3933, 'Морівськ', 'Морівськ, Чернігівська область, Україна, 17021', 'UA'),
(561, 1, 3934, 'Дідівщина', 'Дідівщина, Київська область, Україна, 08514', 'UA'),
(562, 1, 3935, 'Свердловськ', 'Свердловськ, Луганська область, Україна', 'UA'),
(563, 1, 3936, 'Косів', 'Косів, Івано-Франківська область, Україна', 'UA'),
(564, 1, 3937, 'Шпола', 'Шпола, Черкаська область, Україна', 'UA'),
(565, 1, 3938, 'Судова Вишня', 'Судова Вишня, Львівська область, Україна', 'UA'),
(566, 1, 3939, 'Єлизаветівка', 'Єлизаветівка, Дніпропетровська область, Україна', 'UA'),
(567, 1, 3940, 'Городня', 'Городня, Чернігівська область, Україна', 'UA'),
(568, 1, 3941, 'Корюківка', 'Корюківка, Чернігівська область, Україна', 'UA'),
(569, 1, 3942, 'Згурівка', 'Згурівка, Київська область, Україна', 'UA'),
(570, 1, 3943, 'Димитрове', 'Димитрове, Київська область, Україна, 07402', 'UA'),
(571, 1, 3944, 'Нова Ушиця', 'Нова Ушиця, Хмельницька область, Україна', 'UA'),
(572, 1, 3945, 'Сторожинець', 'Сторожинець, Чернівецька область, Україна', 'UA'),
(573, 1, 3946, 'Білоцерківка', 'Білоцерківка, Полтавська область, Україна, 38340', 'UA'),
(574, 1, 3947, 'Перемишляни', 'Перемишляни, Львівська область, Україна', 'UA'),
(575, 1, 3948, 'Дзвиняч', 'Дзвиняч, Івано-Франківська область, Україна, 77750', 'UA'),
(576, 1, 3949, 'Вигода', 'Вигода, Івано-Франківська область, Україна, 77552', 'UA'),
(577, 1, 3950, 'Созонівка', 'Созонівка, Кіровоградська область, Україна, 27602', 'UA'),
(578, 1, 3951, 'Маньківка', 'Маньківка, Черкаська область, Україна', 'UA'),
(579, 1, 3952, 'Южноукраїнськ', 'Южноукраїнськ, Миколаївська область, Україна', 'UA'),
(580, 1, 3953, 'Мар''янське', 'Мар''янське, Дніпропетровська область, Україна', 'UA'),
(581, 1, 3954, 'Пісочин', 'Пісочин, Харківська область, Україна', 'UA'),
(582, 1, 3955, 'Новософіївка', 'Новософіївка, Херсонська область, Україна, 75664', 'UA'),
(583, 1, 3956, 'Енергодар', 'Енергодар, Запорізька область, Україна', 'UA'),
(584, 1, 3957, 'Щастя', 'Щастя, Луганська область, Україна', 'UA'),
(585, 1, 3958, 'Гладківка', 'Гладківка, Херсонська область, Україна, 75633', 'UA'),
(586, 1, 3959, 'Кам''янка-Бузька', 'Кам''янка-Бузька, Львівська область, Україна, 80400', 'UA'),
(587, 1, 3960, 'Ізяслав', 'Ізяслав, Хмельницька область, Україна', 'UA'),
(588, 1, 3961, 'Малиновка', 'Малиновка, Калининградская обл., Росія, 238530', 'RU'),
(589, 1, 3962, 'Калининградская область', 'Калінінградська область, Росія', 'RU'),
(590, 1, 3963, 'Лисець', 'Лисець, Івано-Франківська область, Україна', 'UA'),
(591, 1, 3964, 'Ходорів', 'Ходорів, Львівська область, Україна', 'UA'),
(592, 1, 3965, 'Андреевка', 'Андреевка, Московская обл., Росія', 'RU'),
(593, 1, 3966, 'Брищі', 'Брищі, Рівненська область, Україна, 35108', 'UA'),
(594, 1, 3967, 'Брилівка', 'Брилівка, Херсонська область, Україна, 75143', 'UA'),
(595, 1, 3968, 'Кульчин', 'Кульчин, Волинська область, Україна, 45241', 'UA'),
(596, 1, 3969, 'Старий Любар', 'Старий Любар, Житомирська область, Україна, 13133', 'UA'),
(597, 1, 3970, 'Кирилівка', 'Кирилівка, Запорізька область, Україна, 72563', 'UA'),
(598, 1, 3971, 'Дударків', 'Дударків, Київська область, Україна, 08330', 'UA'),
(599, 1, 3972, 'Доманово', 'Доманово, Білорусь', 'BY'),
(600, 1, 3973, 'Брестская область', 'Берестейська область, Білорусь', 'BY'),
(601, 1, 3974, 'Немиринці', 'Немиринці, Житомирська область, Україна, 13643', 'UA'),
(602, 1, 3975, 'Довбиш', 'Довбиш, Житомирська область, Україна, 12724', 'UA'),
(603, 1, 3976, 'Монастирище', 'Монастирище, Черкаська область, Україна', 'UA'),
(604, 1, 3977, 'Скороходове', 'Скороходове, Полтавська область, Україна, 38813', 'UA'),
(605, 1, 3978, 'Чуднів', 'Чуднів, Житомирська область, Україна', 'UA'),
(606, 1, 3979, 'Доманівка', 'Доманівка, Миколаївська область, Україна', 'UA'),
(607, 1, 3980, 'Радомишль', 'Радомишль, Житомирська область, Україна', 'UA'),
(608, 1, 3981, 'Черкаське', 'Черкаське, Дніпропетровська область, Україна, 51272', 'UA'),
(609, 1, 3982, 'Любимівка', 'Любимівка, Дніпропетровська область, Україна, 52042', 'UA'),
(610, 1, 3983, 'Нові Санжари', 'Нові Санжари, Полтавська область, Україна', 'UA'),
(611, 1, 3984, 'Сахновщина', 'Сахновщина, Харківська область, Україна', 'UA'),
(612, 1, 3985, 'Птахівка', 'Птахівка, Херсонська область, Україна, 75740', 'UA'),
(613, 1, 3986, 'Іршанськ', 'Іршанськ, Житомирська область, Україна, 12110', 'UA'),
(614, 1, 3987, 'Буки', 'Буки, Черкаська область, Україна, 20114', 'UA'),
(615, 1, 3988, 'Горянівське', 'Горянівське, Дніпропетровська область, Україна, 52035', 'UA'),
(616, 1, 3989, 'Сосниця', 'Сосниця, Чернігівська область, Україна', 'UA'),
(617, 1, 3990, 'Сніжне', 'Сніжне, Донецька область, Україна', 'UA'),
(618, 1, 3991, 'Селище', 'Селище, Вінницька область, Україна, 23316', 'UA'),
(619, 1, 3992, 'Меденичі', 'Меденичі, Львівська область, Україна', 'UA'),
(620, 1, 3993, 'Мирне', 'Мирне', ''),
(621, 1, 3994, 'Дмитровичі', 'Дмитровичі, Львівська область, Україна, 81150', 'UA'),
(622, 1, 3995, 'Заньки', 'Заньки, Чернігівська область, Україна, 16623', 'UA'),
(623, 1, 3996, 'Остер', 'Остер, Чернігівська область, Україна, 17044', 'UA'),
(624, 1, 3997, 'Новотроїцьке', 'Новотроїцьке, Херсонська область, Україна', 'UA'),
(625, 1, 3998, 'Ізмаїл', 'Ізмаїл, Одеська область, Україна', 'UA'),
(626, 1, 3999, 'Базаліївка', 'Базаліївка, Харківська область, Україна, 63531', 'UA'),
(627, 1, 4000, 'Козелець', 'Козелець, Чернігівська область, Україна', 'UA'),
(628, 1, 4001, 'Загвіздя', 'Загвіздя, Івано-Франківська область, Україна, 77450', 'UA'),
(629, 1, 4002, 'Воєгоща', 'Воєгоща, Волинська область, Україна, 44534', 'UA'),
(630, 1, 4003, 'Стара-Заґора', 'Стара-Заґора, Болгарія', 'BG'),
(631, 1, 4004, 'Лисянка', 'Лисянка, Черкаська область, Україна', 'UA'),
(632, 1, 4005, 'Перещепине', 'Перещепине, Дніпропетровська область, Україна', 'UA'),
(633, 1, 4006, 'Ковалин', 'Ковалин, Київська область, Україна, 08435', 'UA'),
(634, 1, 4007, 'Дашківці', 'Дашківці, Вінницька область, Україна, 22363', 'UA'),
(635, 1, 4008, 'Саксагань', 'Саксагань, Дніпропетровська область, Україна, 52173', 'UA'),
(636, 1, 4009, 'Теплодар', 'Теплодар, Одеська область, Україна', 'UA'),
(637, 1, 4010, 'Чигирин', 'Чигирин, Черкаська область, Україна', 'UA'),
(638, 1, 4011, 'Тараща', 'Тараща, Київська область, Україна', 'UA'),
(639, 1, 4012, 'Водяники', 'Водяники, Черкаська область, Україна, 20232', 'UA'),
(640, 1, 4013, 'Крюківщина', 'Крюківщина, Київська область, Україна, 08136', 'UA'),
(641, 1, 4014, 'Прутівка', 'Прутівка, Івано-Франківська область, Україна, 78348', 'UA'),
(642, 1, 4015, 'Підгородне', 'Підгородне, Дніпропетровська область, Україна', 'UA'),
(643, 1, 4016, 'Яреськи', 'Яреськи, Полтавська область, Україна, 38030', 'UA'),
(644, 1, 4017, 'Шабо', 'Шабо, Одеська область, Україна, 67770', 'UA'),
(645, 1, 4018, 'Дейкалівка', 'вулиця Гора, Дейкалівка, Полтавська область, Україна', 'UA'),
(646, 1, 4019, 'Буди', 'Буди, Харківська область, Україна, 62456', 'UA'),
(647, 1, 4020, 'Віта-Поштова', 'Віта-Поштова, Київська область, Україна, 08170', 'UA'),
(648, 1, 4021, 'Жашків', 'Жашків, Черкаська область, Україна', 'UA'),
(649, 1, 4022, 'Червона Слобода', 'Червона Слобода, Черкаська область, Україна, 19604', 'UA'),
(650, 1, 4023, 'Петровське', 'Петровське, Луганська область, Україна', 'UA'),
(651, 1, 4024, 'Сербо-Слобідка', 'Сербо-Слобідка, Житомирська область, Україна, 11244', 'UA'),
(652, 1, 4025, 'Петропавлівка', 'Петропавлівка, Дніпропетровська область, Україна', 'UA'),
(653, 1, 4026, 'Дубно', 'Дубно, Рівненська область, Україна', 'UA'),
(654, 1, 4027, 'Попасне', 'Попасне, Дніпропетровська область, Україна, 51242', 'UA'),
(655, 1, 4028, 'Славута', 'Славута, Хмельницька область, Україна, 30000', 'UA'),
(656, 1, 4029, 'Смолин', 'Смолин, Чернігівська область, Україна, 15557', 'UA'),
(657, 1, 4030, 'Семенівка', 'Семенівка, Чернігівська область, Україна', 'UA'),
(658, 1, 4031, 'Заболотів', 'Заболотів, Івано-Франківська область, Україна', 'UA'),
(659, 1, 4032, 'Рожнятів', 'Рожнятів, Івано-Франківська область, Україна', 'UA'),
(660, 1, 4033, 'Шпитьки', 'Шпитьки, Київська область, Україна, 08122', 'UA'),
(661, 1, 4034, 'Пісківка', 'Пісківка, Київська область, Україна', 'UA'),
(662, 1, 4035, 'Тлумач', 'Тлумач, Івано-Франківська область, Україна', 'UA');
INSERT INTO `Frontend_Geo_Locales` (`ID`, `localeID`, `geoID`, `name`, `fullAddress`, `country`) VALUES
(663, 1, 4036, 'Виноградівка', 'Виноградівка, Одеська область, Україна, 68452', 'UA'),
(664, 1, 4037, 'Мамалига', 'Мамалига, Чернівецька область, Україна, 60364', 'UA'),
(665, 1, 4038, 'Лище', 'вулиця Гайка, Лище, Волинська область, Україна', 'UA'),
(666, 1, 4039, 'Авдіївка', 'Авдіївка, Донецька область, Україна', 'UA'),
(667, 1, 4040, 'Білицьке', 'Білицьке, Донецька область, Україна', 'UA'),
(668, 1, 4041, 'Новогродівка', 'Новогродівка, Донецька область, Україна', 'UA'),
(669, 1, 4042, 'Новотавричеське', 'Новотавричеське, Запорізька область, Україна, 70510', 'UA'),
(670, 1, 4043, 'Ватутіне', 'Ватутіне, Черкаська область, Україна', 'UA'),
(671, 1, 4044, 'Сарата', 'Сарата, Одеська область, Україна', 'UA'),
(672, 1, 4045, 'Ягодинка', 'Ягодинка, Житомирська область, Україна, 12113', 'UA'),
(673, 1, 4046, 'Оброшине', 'Оброшине, Львівська область, Україна, 81115', 'UA'),
(674, 1, 4047, 'Базар''янка', 'Базар''янка, Одеська область, Україна, 68162', 'UA'),
(675, 1, 4048, 'Горошков', 'Горошков, Білорусь', 'BY'),
(676, 1, 4049, 'Гомельская область', 'Гомельська область, Білорусь', 'BY'),
(677, 1, 4050, 'Погреби', 'Погреби, Київська область, Україна, 07416', 'UA'),
(678, 1, 4051, 'Диканька', 'Диканька, Полтавська область, Україна', 'UA'),
(679, 1, 4052, 'Балта', 'Балта, Одеська область, Україна, 66100', 'UA'),
(680, 1, 4053, 'Літин', 'Літин, Вінницька область, Україна', 'UA'),
(681, 1, 4054, 'Велика Димерка', 'Велика Димерка, Київська область, Україна, 07442', 'UA'),
(682, 1, 4055, 'Новопсков', 'Новопсков, Луганська область, Україна', 'UA'),
(683, 1, 4056, 'Куряни', 'Куряни, Тернопільська область, Україна, 47522', 'UA'),
(684, 1, 4057, 'Сокільники', 'Сокільники, Львівська область, Україна, 81130', 'UA'),
(685, 1, 4058, 'Цукури', 'Цукури, Херсонська область, Україна, 74832', 'UA'),
(686, 1, 4059, 'Балівка', 'Балівка, Дніпропетровська область, Україна, 52010', 'UA'),
(687, 1, 4060, 'Вилия', 'Вилия 190 12, Греція', 'GR'),
(688, 1, 4061, 'Аттіка', 'Аттика, Греція', 'GR'),
(689, 1, 4062, 'Греція', 'Греція', 'GR'),
(690, 1, 4063, 'П''ятихатки', 'П''ятихатки, Дніпропетровська область, Україна', 'UA'),
(691, 1, 4064, 'Куровичі', 'Куровичі, Львівська область, Україна, 80725', 'UA'),
(692, 1, 4065, 'Берегомет', 'Берегомет, Чернівецька область, Україна, 59233', 'UA'),
(693, 1, 4066, 'Вороновиця', 'Вороновиця, Вінницька область, Україна', 'UA'),
(694, 1, 4067, 'Градизьк', 'Градизьк, Полтавська область, Україна', 'UA'),
(695, 1, 4068, 'Дніпровське', 'Дніпровське, Дніпропетровська область, Україна, 51650', 'UA'),
(696, 1, 4069, 'Шаргород', 'Шаргород, Вінницька область, Україна', 'UA'),
(697, 1, 4070, 'Негрово', 'Негрово, Закарпатська область, Україна, 90114', 'UA'),
(698, 1, 4071, 'Турбів', 'Турбів, Вінницька область, Україна, 22513', 'UA'),
(699, 1, 4072, 'Забілоччя', 'Забілоччя, Житомирська область, Україна, 12262', 'UA'),
(700, 1, 4073, 'Надичі', 'Надичі, Львівська область, Україна, 80371', 'UA'),
(701, 1, 4074, 'Монастириська', 'Монастириська, Тернопільська область, Україна, 48300', 'UA'),
(702, 1, 4075, 'Вишково', 'Вишково, Закарпатська область, Україна, 90454', 'UA'),
(703, 1, 4076, 'Гуменці', 'Гуменці, Хмельницька область, Україна, 32325', 'UA'),
(704, 1, 4077, 'Владимировка', 'Владимировка 110000, Казахстан', 'KZ'),
(705, 1, 4078, 'Костанайская область', 'Кустанай, Казахстан', 'KZ'),
(706, 1, 4079, 'Казахстан', 'Казахстан', 'KZ'),
(707, 1, 4080, 'Первомайське сільське поселення', 'Первомайське сільське поселення, Ленінградська область, Росія, 188855', 'RU'),
(708, 1, 4081, 'Бараші', 'Бараші, Житомирська область, Україна, 11255', 'UA'),
(709, 1, 4082, 'Межова', 'Межова, Дніпропетровська область, Україна', 'UA'),
(710, 1, 4083, 'Ягнятин', 'Ягнятин, Житомирська область, Україна, 13631', 'UA'),
(711, 1, 4084, 'Дубоссари', 'Коржова, Дубоссари, Молдова', 'MD'),
(712, 1, 4085, 'Нетішин', 'Нетішин, Хмельницька область, Україна', 'UA'),
(713, 1, 4086, 'Клевань', 'Клевань, Рівненська область, Україна', 'UA'),
(714, 1, 4087, 'Тюмень', 'ул. Осипенко, Тюмень, Тюменская обл., Росія', 'RU'),
(715, 1, 4088, 'Тюменская область', 'Тюменська область, Росія', 'RU'),
(716, 1, 4089, 'Боярка', 'Боярка, Київська область, Україна', 'UA'),
(717, 1, 4090, 'Бобровиця', 'Бобровиця, Чернігівська область, Україна', 'UA'),
(718, 1, 4091, 'Запитів', 'Запитів, Львівська область, Україна, 80461', 'UA'),
(719, 1, 4092, 'Черняхів', 'Черняхів, Житомирська область, Україна', 'UA'),
(720, 1, 4093, 'Голоби', 'Голоби, Волинська область, Україна', 'UA'),
(721, 1, 4094, 'Камінь-Каширський', 'Камінь-Каширський, Волинська область, Україна', 'UA'),
(722, 1, 4095, 'Часів Яр', 'Часів Яр, Донецька область, Україна', 'UA'),
(723, 1, 4096, 'Софіївка', 'Софіївка, Дніпропетровська область, Україна', 'UA'),
(724, 1, 4097, 'Бахмач', 'Бахмач, Чернігівська область, Україна', 'UA'),
(725, 1, 4098, 'Комишуваха', 'Комишуваха, Запорізька область, Україна', 'UA'),
(726, 1, 4099, 'Білки', 'Білки, Закарпатська область, Україна, 90132', 'UA'),
(727, 1, 4100, 'Выра', 'Выра, Ленинградская обл., Росія, 188356', 'RU'),
(728, 1, 4101, 'Долинське', 'Долинське, Запорізька область, Україна, 70420', 'UA'),
(729, 1, 4102, 'Східне', 'Східне, Херсонська область, Україна, 75020', 'UA'),
(730, 1, 4103, 'Трибухівці', 'Трибухівці, Тернопільська область, Україна, 48431', 'UA'),
(731, 1, 4104, 'Буринь', 'Буринь, Сумська область, Україна', 'UA'),
(732, 1, 4105, 'Нова Маячка', 'Нова Маячка, Херсонська область, Україна, 75120', 'UA'),
(733, 1, 4106, 'Липча', 'Липча, Закарпатська область, Україна, 90415', 'UA'),
(734, 1, 4107, 'Горбовичі', 'Горбовичі, Київська область, Україна, 08162', 'UA'),
(735, 1, 4108, 'Ружин', 'Ружин, Житомирська область, Україна', 'UA'),
(736, 1, 4109, 'Великий Бичків', 'Великий Бичків, Закарпатська область, Україна, 90615', 'UA'),
(737, 1, 4110, 'Таранівка', 'Таранівка, Харківська область, Україна', 'UA'),
(738, 1, 4111, 'Орлівщина', 'Орлівщина, Дніпропетровська область, Україна', 'UA'),
(739, 1, 4112, 'Мариновка', 'Мариновка 020000, Казахстан', 'KZ'),
(740, 1, 4113, 'Акмолинская область', 'Акмольська обл., Казахстан', 'KZ'),
(741, 1, 4114, 'Орёл', 'УМВД России по Орловской области, ул. Новикова, 5, Орёл, Орловская обл., Росія, 302028', 'RU'),
(742, 1, 4115, 'Орловская область', 'Орловська область, Росія', 'RU'),
(743, 1, 4116, 'Чутове', 'Чутове, Полтавська область, Україна', 'UA'),
(744, 1, 4117, 'Рунгури', 'Рунгури, Івано-Франківська область, Україна, 78272', 'UA'),
(745, 1, 4118, 'Змиевка', 'Змиевка, Орловская обл., Росія, 303320', 'RU'),
(746, 1, 4119, 'Славутич', 'Славутич, Київська область, Україна', 'UA'),
(747, 1, 4120, 'Вишнівець', 'Вишнівець, Тернопільська область, Україна', 'UA'),
(748, 1, 4121, 'Маяки', 'Маяки, Одеська область, Україна, 67654', 'UA'),
(749, 1, 4122, 'Джугастрове', 'Джугастрове, Одеська область, Україна, 67222', 'UA'),
(750, 1, 4123, 'Новооржицьке', 'Новооржицьке, Полтавська область, Україна, 37714', 'UA'),
(751, 1, 4124, 'Брошнів', 'Брошнів, Івано-Франківська область, Україна, 77614', 'UA'),
(752, 1, 4125, 'Новоархангельськ', 'Новоархангельськ, Кіровоградська область, Україна', 'UA'),
(753, 1, 4126, 'Білогір''я', 'Білогір''я, Хмельницька область, Україна', 'UA'),
(754, 1, 4127, 'Трипілля', 'Трипілля, Київська область, Україна, 08722', 'UA'),
(755, 1, 4128, 'Плетений Ташлик', 'Плетений Ташлик, Кіровоградська область, Україна, 26245', 'UA'),
(756, 1, 4129, 'Володарка', 'Володарка, Київська область, Україна', 'UA'),
(757, 1, 4130, 'Левків', 'Левків, Житомирська область, Україна, 12405', 'UA'),
(758, 1, 4131, 'Богуслав', 'Богуслав, Київська область, Україна', 'UA'),
(759, 1, 4132, 'Шацьк', 'Шацьк, Рязанська область, Росія', 'RU'),
(760, 1, 4133, 'Рязанська область', 'Рязанська область, Росія', 'RU'),
(761, 1, 4134, 'Краковець', 'Краковець, Львівська область, Україна', 'UA'),
(762, 1, 4135, 'Халявин', 'Халявин, Чернігівська область, Україна, 15524', 'UA'),
(763, 1, 4136, 'Васищеве', 'Васищеве, Харківська область, Україна', 'UA'),
(764, 1, 4137, 'Галич', 'Галич, Івано-Франківська область, Україна', 'UA'),
(765, 1, 4138, 'Марківка', 'Марківка, Луганська область, Україна', 'UA'),
(766, 1, 4139, 'Жежелів', 'Жежелів, Вінницька область, Україна, 22116', 'UA'),
(767, 1, 4140, 'Грузьке', 'Грузьке, Кіровоградська область, Україна, 27632', 'UA'),
(768, 1, 4141, 'Вугледар', 'Вугледар, Донецька область, Україна', 'UA'),
(769, 1, 4142, 'Кищенці', 'Кищенці, Черкаська область, Україна, 20121', 'UA'),
(770, 1, 4143, 'Тарасенкове', 'Тарасенкове, Полтавська область, Україна, 37734', 'UA'),
(771, 1, 4144, 'Вільне', 'Вільне, Дніпропетровська область, Україна, 51260', 'UA'),
(772, 1, 4145, 'Березанка', 'Березанка, Миколаївська область, Україна', 'UA'),
(773, 1, 4146, 'Поповичі', 'Поповичі, Волинська область, Україна, 45090', 'UA'),
(774, 1, 4147, 'Старий Мізунь', 'Старий Мізунь, Івано-Франківська область, Україна, 77543', 'UA'),
(775, 1, 4148, 'Чапаєвка', 'Чапаєвка, Черкаська область, Україна, 19774', 'UA'),
(776, 1, 4149, 'Глеваха', 'Глеваха, Київська область, Україна', 'UA'),
(777, 1, 4150, 'Корделівка', 'Корделівка, Вінницька область, Україна, 22445', 'UA'),
(778, 1, 4151, 'Очаків', 'Очаків, Миколаївська область, Україна', 'UA'),
(779, 1, 4152, 'Ратне', 'Ратне, Волинська область, Україна', 'UA'),
(780, 1, 4153, 'Чотирбоки', 'Чотирбоки, Хмельницька область, Україна, 30442', 'UA'),
(781, 1, 4154, 'Кодима', 'Кодима, Одеська область, Україна', 'UA'),
(782, 1, 4155, 'Стратіївка', 'Стратіївка, Вінницька область, Україна, 24831', 'UA'),
(783, 1, 4156, 'Сокаль', 'Сокаль, Львівська область, Україна', 'UA'),
(784, 1, 4157, 'Залізний Порт', 'Залізний Порт, Херсонська область, Україна, 75653', 'UA'),
(785, 1, 4158, 'Решетилівка', 'Решетилівка, Полтавська область, Україна', 'UA'),
(786, 1, 4159, 'Александровка', 'Александровка, Ростовская обл., Росія, 346765', 'RU'),
(787, 1, 4160, 'Лазурне', 'Лазурне, Херсонська область, Україна', 'UA'),
(788, 1, 4161, 'Самотоївка', 'Самотоївка, Сумська область, Україна, 42440', 'UA'),
(789, 1, 4162, 'Баранівка', 'Баранівка, Житомирська область, Україна, 11600', 'UA'),
(790, 1, 4163, 'Помічна', 'Помічна, Кіровоградська область, Україна', 'UA'),
(791, 1, 4164, 'Буданів', 'Буданів, Тернопільська область, Україна, 48154', 'UA'),
(792, 1, 4165, 'Смоліне', 'Смоліне, Кіровоградська область, Україна', 'UA'),
(793, 1, 4166, 'Чечельник', 'Чечельник, Вінницька область, Україна, 24800', 'UA'),
(794, 1, 4167, 'Любар', 'Любар, Житомирська область, Україна', 'UA'),
(795, 1, 4168, 'Красилівка', 'Красилівка, Київська область, Україна, 07451', 'UA'),
(796, 1, 4169, 'Гриців', 'Гриців, Хмельницька область, Україна, 30455', 'UA'),
(797, 1, 4170, 'Степанці', 'Степанці, Черкаська область, Україна, 19031', 'UA'),
(798, 1, 4171, 'Кожухівка', 'Кожухівка, Київська область, Україна, 08618', 'UA'),
(799, 1, 4172, 'Харьковка', 'Харьковка, Волгоградская обл., Росія, 404204', 'RU'),
(800, 1, 4173, 'Яблунівка', 'Яблунівка, Чернігівська область, Україна, 17591', 'UA'),
(801, 1, 4174, 'Атмис', 'ул. Змеевка, Атмис, Пензенская обл., Росія, 442146', 'RU'),
(802, 1, 4175, 'Попасна', 'Попасна, Луганська область, Україна', 'UA'),
(803, 1, 4176, 'Громівка', 'Громівка, Херсонська область, Україна, 75352', 'UA'),
(804, 1, 4177, 'Теофіполь', 'Теофіполь, Хмельницька область, Україна', 'UA'),
(805, 1, 4178, 'Дрогичин', 'Днепро-Бугская ул., Дрогичин, Білорусь', 'BY'),
(806, 1, 4179, 'Старобільськ', 'Старобільськ, Луганська область, Україна', 'UA'),
(807, 1, 4180, 'Селидове', 'Селидове, Донецька область, Україна', 'UA'),
(808, 1, 4181, 'Кременець', 'Кременець, Тернопільська область, Україна', 'UA'),
(809, 1, 4182, 'Алматы', 'Ужгородская, Алматы, Казахстан', 'KZ'),
(810, 1, 4183, 'Драниця', 'Драниця, Чернівецька область, Україна, 60356', 'UA'),
(811, 1, 4184, 'Бандишівка', 'Бандишівка, Вінницька область, Україна, 24054', 'UA'),
(812, 1, 4185, 'Солонка', 'Солонка, Львівська область, Україна, 81131', 'UA'),
(813, 1, 4186, 'Галузія', 'Галузія, Волинська область, Україна, 44613', 'UA'),
(814, 1, 4187, 'Перемишель', 'Перемишель, Хмельницька область, Україна, 30073', 'UA'),
(815, 1, 4188, 'Дальник', 'Дальник, Одеська область, Україна, 67842', 'UA'),
(816, 1, 4189, 'Леськи', 'Леськи, Черкаська область, Україна, 19640', 'UA'),
(817, 1, 4190, 'Білий Колодязь', 'Білий Колодязь, Харківська область, Україна, 62540', 'UA'),
(818, 1, 4191, 'Білозерка', 'Білозерка, Херсонська область, Україна', 'UA'),
(819, 1, 4192, 'Гуляйполе', 'Гуляйполе, Запорізька область, Україна', 'UA'),
(820, 1, 4193, 'Водяне', 'Водяне, Запорізька область, Україна', 'UA'),
(821, 1, 4194, 'Велика Михайлівка', 'Велика Михайлівка, Одеська область, Україна', 'UA'),
(822, 1, 4195, 'Коломак', 'Коломак, Харківська область, Україна', 'UA'),
(823, 1, 4196, 'Чорнухи', 'Чорнухи, Полтавська область, Україна', 'UA'),
(824, 1, 4197, 'Нова Прага', 'Нова Прага, Кіровоградська область, Україна, 28042', 'UA'),
(825, 1, 4198, 'Безбородькове', 'Безбородькове, Дніпропетровська область, Україна, 52440', 'UA'),
(826, 1, 4199, 'Печеніги', 'Печеніги, Харківська область, Україна', 'UA'),
(827, 1, 4200, 'Северинівка', 'Северинівка, Молдова', 'MD'),
(828, 1, 4201, 'Москва', 'ул. Каховка, 2, Москва, Росія, 117303', 'RU'),
(829, 1, 4202, 'Москва', 'Москва, Росія', 'RU'),
(830, 1, 4203, 'Врадіївка', 'Врадіївка, Миколаївська область, Україна', 'UA'),
(831, 1, 4204, 'Кам''янече', 'Кам''янече, Кіровоградська область, Україна, 26110', 'UA'),
(832, 1, 4205, 'Болеславчик', 'Болеславчик, Миколаївська область, Україна, 55220', 'UA'),
(833, 1, 4206, 'Мала Токмачка', 'Мала Токмачка, Запорізька область, Україна, 70550', 'UA'),
(834, 1, 4207, 'Урзуф', 'Урзуф, Донецька область, Україна, 87455', 'UA'),
(835, 1, 4208, 'Чернігівка', 'Чернігівка, Приморський край, Росія', 'RU'),
(836, 1, 4209, 'Приморський край', 'Приморський край, Росія', 'RU'),
(837, 1, 4210, 'Фрузинівка', 'Фрузинівка, Київська область, Україна', 'UA'),
(838, 1, 4211, 'Залізці', 'Залізці, Тернопільська область, Україна, 47234', 'UA'),
(839, 1, 4212, 'Грушово', 'Грушово, Закарпатська область, Україна, 90570', 'UA'),
(840, 1, 4213, 'Дроздинь', 'Дроздинь, Рівненська область, Україна, 34211', 'UA'),
(841, 1, 4214, 'Бурчак', 'Бурчак, Запорізька область, Україна, 72010', 'UA'),
(842, 1, 4215, 'Славянка', 'Славянка, Приморский край, Росія', 'RU'),
(843, 1, 4216, 'Чайки', 'Чайки, Київська область, Україна, 08130', 'UA'),
(844, 1, 4217, 'Рахни-Лісові', 'Рахни-Лісові, Вінницька область, Україна, 23536', 'UA'),
(845, 1, 4218, 'Яремче', 'Яремче, Івано-Франківська область, Україна, 78500', 'UA'),
(846, 1, 4219, 'Потоки', 'Потоки, Полтавська область, Україна, 39741', 'UA'),
(847, 1, 4220, 'Українка', 'Українка, Київська область, Україна', 'UA'),
(848, 1, 4221, 'Кушугум', 'Кушугум, Запорізька область, Україна', 'UA'),
(849, 1, 4222, 'Болехів', 'Болехів, Івано-Франківська область, Україна', 'UA'),
(850, 1, 4223, 'Млинів', 'Млинів, Рівненська область, Україна', 'UA'),
(851, 1, 4224, 'Зачернеччя', 'Зачернеччя, Волинська область, Україна, 44340', 'UA'),
(852, 1, 4225, 'Приколотне', 'Приколотне, Харківська область, Україна', 'UA'),
(853, 1, 4226, 'Шишаки', 'Шишаки, Полтавська область, Україна', 'UA'),
(854, 1, 4227, 'Старі Петрівці', 'Старі Петрівці, Київська область, Україна, 07353', 'UA'),
(855, 1, 4228, 'Приморськ', 'Приморськ, Запорізька область, Україна', 'UA'),
(856, 1, 4229, 'Вільбівне', 'Вільбівне, Рівненська область, Україна, 35809', 'UA'),
(857, 1, 4230, 'Великополовецьке', 'Великополовецьке, Київська область, Україна, 09030', 'UA'),
(858, 1, 4231, 'Дмитрівка', 'Дмитрівка, Київська область, Україна, 08112', 'UA'),
(859, 1, 4232, 'Широке', 'Широке, Дніпропетровська область, Україна', 'UA'),
(860, 1, 4233, 'Березівка', 'Березівка, Одеська область, Україна', 'UA'),
(861, 1, 4234, 'Софіївська Борщагівка', 'Софіївська Борщагівка, Київська область, Україна, 08131', 'UA'),
(862, 1, 4235, 'Короп', 'Короп, Чернігівська область, Україна', 'UA'),
(863, 1, 4236, 'Жорнівка', 'Жорнівка, Рівненська область, Україна, 34721', 'UA'),
(864, 1, 4237, 'Ясіня', 'Ясіня, Закарпатська область, Україна, 90630', 'UA'),
(865, 1, 4238, 'Салгани', 'Салгани, Одеська область, Україна, 67725', 'UA'),
(866, 1, 4239, 'Усть-Илимск', 'Братское ш., Усть-Илимск, Иркутская обл., Росія', 'RU'),
(867, 1, 4240, 'Иркутская область', 'Іркутська область, Росія', 'RU'),
(868, 1, 4241, 'Коржі', 'Коржі, Київська область, Україна, 07544', 'UA'),
(869, 1, 4242, 'Гусятин', 'Гусятин, Тернопільська область, Україна', 'UA'),
(870, 1, 4243, 'Ямниця', 'Ямниця, Івано-Франківська область, Україна, 77422', 'UA'),
(871, 1, 4244, 'Степангород', 'Степангород, Рівненська область, Україна, 34314', 'UA'),
(872, 1, 4245, 'Новий Розділ', 'Новий Розділ, Львівська область, Україна', 'UA'),
(873, 1, 4246, 'Губник', 'Губник, Вінницька область, Україна, 23746', 'UA'),
(874, 1, 4247, 'Бурштин', 'Бурштин, Івано-Франківська область, Україна', 'UA'),
(875, 1, 4248, 'Зеленодольськ', 'Зеленодольськ, Татарстан, Росія', 'RU'),
(876, 1, 4249, 'Татарстан', 'Татарстан, Росія', 'RU'),
(877, 1, 4250, 'Irsch', 'Irsch, Німеччина', 'DE'),
(878, 1, 4251, 'Rhineland-Palatinate', 'Рейнланд-Пфальц, Німеччина', 'DE'),
(879, 1, 4252, 'Німеччина', 'Німеччина', 'DE'),
(880, 1, 4253, 'Саратовська область', 'Саратовська область, Росія', 'RU'),
(881, 1, 4254, 'Карлівка', 'Карлівка, Полтавська область, Україна', 'UA'),
(882, 1, 4255, 'Петровское', 'Петровское, Респ. Башкортостан, Росія, 453230', 'RU'),
(883, 1, 4256, 'Республика Башкортостан', 'Башкортостан, Росія', 'RU'),
(884, 1, 4257, 'Нижні Сірогози', 'Нижні Сірогози, Херсонська область, Україна', 'UA'),
(885, 1, 4258, 'Подвірки', 'Подвірки, Харківська область, Україна, 62371', 'UA'),
(886, 1, 4259, 'Дергачі', 'Дергачі, Харківська область, Україна', 'UA'),
(887, 1, 4260, 'Борислав', 'Борислав, Львівська область, Україна', 'UA'),
(888, 1, 4261, 'Цумань', 'Цумань, Волинська область, Україна, 45233', 'UA'),
(889, 1, 4262, 'Хустець', 'Хустець, Закарпатська область, Україна, 90434', 'UA'),
(890, 1, 4263, 'Качурівка', 'Качурівка, Одеська область, Україна, 66370', 'UA'),
(891, 1, 4264, 'Яворів', 'Яворів, Львівська область, Україна', 'UA'),
(892, 1, 4265, 'Тузли', 'Тузли, Миколаївська область, Україна, 57436', 'UA'),
(893, 1, 4266, 'Харкове', 'Харкове, Чернігівська область, Україна, 17270', 'UA'),
(894, 1, 4267, 'Разино', 'Разино, Калининградская обл., Росія, 238323', 'RU'),
(895, 1, 4268, 'Сєвєрськ', 'Сєвєрськ, Томська область, Росія', 'RU'),
(896, 1, 4269, 'Томська область', 'Томська область, Росія', 'RU'),
(897, 1, 4270, 'Іванковичі', 'Іванковичі, Київська область, Україна, 08632', 'UA'),
(898, 1, 4271, 'Великоплоске', 'Великоплоске, Одеська область, Україна, 67140', 'UA'),
(899, 1, 4272, 'Радушне', 'Радушне, Дніпропетровська область, Україна', 'UA'),
(900, 1, 4273, 'Чернихівці', 'Чернихівці, Тернопільська область, Україна, 47370', 'UA'),
(901, 1, 4274, 'Новородчиці', 'Новородчиці, Рівненська область, Україна, 35851', 'UA'),
(902, 1, 4275, 'Терешки', 'Терешки, Полтавська область, Україна, 38762', 'UA'),
(903, 1, 4276, 'Поляниця', 'Поляниця, Івано-Франківська область, Україна, 78593', 'UA'),
(904, 1, 4277, 'Соколівське', 'Соколівське, Кіровоградська область, Україна, 27641', 'UA'),
(905, 1, 4278, 'Новоолексіївка', 'Новоолексіївка, Херсонська область, Україна', 'UA'),
(906, 1, 4279, 'Рахів', 'Рахів, Закарпатська область, Україна, 90600', 'UA'),
(907, 1, 4280, 'Коблеве', 'Коблеве, Миколаївська область, Україна', 'UA'),
(908, 1, 4281, 'Ясинувата', 'Ясинувата, Донецька область, Україна', 'UA'),
(909, 1, 4282, 'Дніпровокам''янка', 'Дніпровокам''янка, Дніпропетровська область, Україна, 51612', 'UA'),
(910, 1, 4283, 'Шарівка', 'Шарівка, Харківська область, Україна', 'UA'),
(911, 1, 4284, 'Темнівка', 'Темнівка, Харківська область, Україна, 62493', 'UA'),
(912, 1, 4285, 'Tabriz', 'East Azerbaijan, Tabriz, Bazar, Іран', 'IR'),
(913, 1, 4286, 'East Azerbaijan', 'Східний Азербайджан, Іран', 'IR'),
(914, 1, 4287, 'Іран', 'Іран', 'IR'),
(915, 1, 4288, 'місто Київ', 'місто Київ, Україна', 'UA'),
(916, 1, 4289, 'Солотвино', 'Солотвино, Закарпатська область, Україна', 'UA'),
(917, 1, 4290, 'Топорівці', 'Топорівці, Івано-Франківська область, Україна, 78162', 'UA'),
(918, 1, 4291, 'Алчевськ', 'Алчевськ, Луганська область, Україна', 'UA'),
(919, 1, 4292, 'Слов''яносербськ', 'Слов''яносербськ, Луганська область, Україна', 'UA'),
(920, 1, 4293, 'Путивль', 'Путивль, Сумська область, Україна', 'UA'),
(921, 1, 4294, 'Стара Прилука', 'Стара Прилука, Вінницька область, Україна, 22511', 'UA'),
(922, 1, 4295, 'Мотижин', 'Мотижин, Київська область, Україна, 08060', 'UA'),
(923, 1, 4296, 'Батайск', 'Восточное ш., Батайск, Ростовская обл., Росія', 'RU'),
(924, 1, 4297, 'Новий Биків', 'Новий Биків, Чернігівська область, Україна', 'UA'),
(925, 1, 4298, 'Пиріжки', 'Пиріжки, Житомирська область, Україна, 11641', 'UA'),
(926, 1, 4299, 'Станівці', 'Станівці, Чернівецька область, Україна, 60432', 'UA'),
(927, 1, 4300, 'Рославичі', 'Рославичі, Київська область, Україна, 08681', 'UA'),
(928, 1, 4301, 'Нижнє Болотне', 'Нижнє Болотне, Закарпатська область, Україна, 90142', 'UA'),
(929, 1, 4302, 'Велика Знам''янка', 'Велика Знам''янка, Запорізька область, Україна', 'UA'),
(930, 1, 4303, 'Марьевка', 'Марьевка, Респ. Башкортостан, Росія, 453288', 'RU'),
(931, 1, 4304, 'Корнин', 'Корнин, Житомирська область, Україна', 'UA'),
(932, 1, 4305, 'Камышеваха', 'Камышеваха, Краснодарский край, Росія, 352205', 'RU'),
(933, 1, 4306, 'Краснодарский край', 'Краснодарський край, Росія', 'RU'),
(934, 1, 4307, 'Миронівський', 'Миронівський, Донецька область, Україна, 84791', 'UA'),
(935, 1, 4308, 'Чулаківка', 'Чулаківка, Херсонська область, Україна, 75635', 'UA'),
(936, 1, 4309, 'Галіївка', 'Галіївка, Житомирська область, Україна, 13251', 'UA'),
(937, 1, 4310, 'Високий', 'Високий, Харківська область, Україна', 'UA'),
(938, 1, 4311, 'Рожище', 'Рожище, Волинська область, Україна', 'UA'),
(939, 1, 4312, 'Квасилів', 'Квасилів, Рівненська область, Україна, 35350', 'UA'),
(940, 1, 4313, 'Михайло-Коцюбинське', 'Михайло-Коцюбинське, Чернігівська область, Україна, 15552', 'UA'),
(941, 1, 4314, 'Станишівка', 'Станишівка, Житомирська область, Україна, 12430', 'UA'),
(942, 1, 4315, 'Буковель', 'Буковель, Івано-Франківська область, Україна', 'UA'),
(943, 1, 4316, 'Острів', 'Острів, Київська область, Україна, 09631', 'UA'),
(944, 1, 4317, 'Гранітне', 'Гранітне, Донецька область, Україна, 87123', 'UA'),
(945, 1, 4318, 'Гулянка', 'Гулянка, Одеська область, Україна, 67950', 'UA'),
(946, 1, 4319, 'Краснопілка', 'Краснопілка, Вінницька область, Україна, 23733', 'UA'),
(947, 1, 4320, 'Крушинка', 'Крушинка, Київська область, Україна, 08635', 'UA'),
(948, 1, 4321, 'Коцюбинці', 'Коцюбинці, Тернопільська область, Україна, 48271', 'UA'),
(949, 1, 4322, 'Приазовське', 'Приазовське, Запорізька область, Україна', 'UA'),
(950, 1, 4323, 'Вилкове', 'Вилкове, Одеська область, Україна, 68355', 'UA'),
(951, 1, 4324, 'Августівка', 'Августівка, Одеська область, Україна, 67632', 'UA'),
(952, 1, 4325, 'Нові Білярі', 'Нові Білярі, Одеська область, Україна', 'UA'),
(953, 1, 4326, 'Новопокровка', 'Новопокровка, Харківська область, Україна, 63523', 'UA'),
(954, 1, 4327, 'Odessa', '2, Odessa, ON, Канада', 'CA'),
(955, 1, 4328, 'Ontario', 'Онтаріо, Каліфорнія, Сполучені Штати Америки', 'CA'),
(956, 1, 4329, 'Канада', 'Канада', 'CA'),
(957, 1, 4330, 'Зноб-Новгородське', 'Зноб-Новгородське, Сумська область, Україна, 41022', 'UA'),
(958, 1, 4331, 'Жовнівка', 'Жовнівка, Тернопільська область, Україна, 47524', 'UA'),
(959, 1, 4332, 'Саки', 'Саки', ''),
(960, 1, 4333, 'Бобриця', 'Бобриця, Київська область, Україна, 08142', 'UA'),
(961, 1, 4334, 'Антонівка', 'Антонівка, Херсонська область, Україна, 75705', 'UA'),
(962, 1, 4335, 'Лазірки', 'Лазірки, Полтавська область, Україна, 37710', 'UA'),
(963, 1, 4336, 'Райгород', 'Райгород, Вінницька область, Україна, 22880', 'UA'),
(964, 1, 4337, 'Ореховка', 'Ореховка, Ульяновская обл., Росія, 433904', 'RU'),
(965, 1, 4338, 'Липини', 'Липини, Волинська область, Україна, 45601', 'UA'),
(966, 1, 4339, 'Тепліце', 'Тепліце, Чеська Республіка', 'CZ'),
(967, 1, 4340, 'Устецький край', 'Устецький край, Чеська Республіка', 'CZ'),
(968, 1, 4341, 'Малодолинське', 'Малодолинське, Одеська область, Україна, 68093', 'UA'),
(969, 1, 4342, 'Полтавська', 'Полтавська, Краснодарський край, Росія', 'RU'),
(970, 1, 4343, 'Безымянка', 'Безымянка, Ростовская обл., Росія, 347202', 'RU'),
(971, 1, 4344, 'Недригайлів', 'Недригайлів, Сумська область, Україна', 'UA'),
(972, 1, 4345, 'Чкаловське', 'Чкаловське, Харківська область, Україна, 63544', 'UA'),
(973, 1, 4346, 'Мокра Рокитна', 'Мокра Рокитна, Харківська область, Україна, 63211', 'UA'),
(974, 1, 4347, 'Ширяєве', 'Ширяєве, Одеська область, Україна', 'UA'),
(975, 1, 4348, 'Білосарайська Коса', 'Білосарайська Коса, Донецька область, Україна, 87443', 'UA'),
(976, 1, 4349, 'Посад-Покровське', 'Посад-Покровське, Херсонська область, Україна, 75010', 'UA'),
(977, 1, 4350, 'Андрієвичі', 'Андрієвичі, Житомирська область, Україна, 11254', 'UA'),
(978, 1, 4351, 'Іванів', 'Іванів, Вінницька область, Україна, 22432', 'UA'),
(979, 1, 4352, 'Актау', 'Актау, Казахстан', 'KZ'),
(980, 1, 4353, 'Манґистау', 'Манґистау, Казахстан', 'KZ'),
(981, 1, 4354, 'Заліщики', 'Заліщики, Тернопільська область, Україна', 'UA'),
(982, 1, 4355, 'Губиниха', 'Губиниха, Дніпропетровська область, Україна', 'UA');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `title` varchar(45) NOT NULL,
  `uri` varchar(3) NOT NULL,
  `status` enum('active','unactive') NOT NULL,
  `googleLang` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Locales`
--

INSERT INTO `Frontend_Locales` (`ID`, `title`, `uri`, `status`, `googleLang`) VALUES
(1, 'Українська', 'ukr', 'active', 'uk'),
(2, 'Русский', 'rus', 'unactive', 'ru');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Locale_Vars`
--

CREATE TABLE IF NOT EXISTS `Frontend_Locale_Vars` (
  `ID` int(10) unsigned NOT NULL,
  `key` varchar(45) NOT NULL,
  `value` varchar(2000) NOT NULL,
  `localeID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Managers_Links`
--

CREATE TABLE IF NOT EXISTS `Frontend_Managers_Links` (
  `ID` int(11) NOT NULL,
  `companyID` int(11) NOT NULL,
  `hashLink` varchar(32) NOT NULL,
  `availableUntil` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Messages`
--

CREATE TABLE IF NOT EXISTS `Frontend_Messages` (
  `ID` int(10) unsigned NOT NULL,
  `dialogID` int(10) unsigned NOT NULL,
  `adminMessageID` int(10) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `userMessageID` int(11) unsigned DEFAULT NULL,
  `deleted` enum('true','false') CHARACTER SET utf8 NOT NULL DEFAULT 'false'
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Messages_Unread`
--

CREATE TABLE IF NOT EXISTS `Frontend_Messages_Unread` (
  `id` int(11) NOT NULL,
  `toUserID` int(11) unsigned DEFAULT NULL,
  `toAdminID` int(11) unsigned DEFAULT NULL,
  `dialogID` int(11) unsigned NOT NULL,
  `messageID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_News`
--

CREATE TABLE IF NOT EXISTS `Frontend_News` (
  `ID` int(10) unsigned NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_News_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_News_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `newsID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Phonecodes`
--

CREATE TABLE IF NOT EXISTS `Frontend_Phonecodes` (
  `ID` int(11) NOT NULL,
  `code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Phonecodes`
--

INSERT INTO `Frontend_Phonecodes` (`ID`, `code`) VALUES
(20, '+0'),
(30, '+00'),
(34, '+038'),
(24, '+050'),
(39, '+067'),
(41, '+068'),
(28, '+08'),
(40, '+080'),
(32, '+095'),
(33, '+098'),
(38, '+099'),
(18, '+3'),
(27, '+30'),
(19, '+35'),
(14, '+359'),
(16, '+372'),
(22, '+373'),
(25, '+375'),
(17, '+38'),
(4, '+380'),
(35, '+386'),
(23, '+44'),
(7, '+48'),
(26, '+49'),
(2, '+7'),
(42, '+790'),
(36, '+792'),
(44, '+793'),
(37, '+797'),
(31, '+8'),
(43, '+809'),
(29, '+90'),
(21, '+994');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Registration_Links`
--

CREATE TABLE IF NOT EXISTS `Frontend_Registration_Links` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `temp` varchar(32) NOT NULL,
  `availableUntil` datetime NOT NULL,
  `type` enum('confirm_email','confirm_password') NOT NULL,
  `casual_password` varchar(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Request_Prices`
--

CREATE TABLE IF NOT EXISTS `Frontend_Request_Prices` (
  `ID` int(10) unsigned NOT NULL,
  `value` decimal(12,2) DEFAULT NULL,
  `paymentType` enum('b/g','gotivka','combined','card') DEFAULT NULL,
  `PDV` enum('yes','no') DEFAULT NULL,
  `onLoad` enum('yes','no') DEFAULT NULL,
  `onUnload` enum('yes','no') DEFAULT NULL,
  `onPrepay` enum('yes','no') DEFAULT NULL,
  `advancedPayment` int(11) DEFAULT NULL,
  `currencyID` int(10) unsigned DEFAULT NULL,
  `specifiedPrice` enum('yes','no') NOT NULL,
  `customPriceType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38021 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Static_Pages`
--

CREATE TABLE IF NOT EXISTS `Frontend_Static_Pages` (
  `ID` int(10) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `keywords` varchar(400) NOT NULL,
  `description` varchar(400) NOT NULL,
  `content` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uri` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_Static_Pages`
--

INSERT INTO `Frontend_Static_Pages` (`ID`, `localeID`, `title`, `keywords`, `description`, `content`, `created`, `uri`) VALUES
(1, 1, 'Прес-центр', 'прес-центр', 'прес-центр', 'Текст сторінки прес-центр', '2016-06-09 12:42:05', 'press'),
(2, 1, 'Контакти', 'сторінка контактів', 'сторінка контактів', 'Вміст сторінки контактів, підтримка <b>HTML-розмітки</b> і т.п.', '2016-06-09 12:54:08', 'contacts'),
(3, 1, 'Регламент тендерного режиму', 'тендерний режим, тендери, тендер, тендер на транспорт', 'Регламент тендерного режиму', 'Публічна оферта на користування тендерним режимом порталу', '2016-06-14 13:37:08', 'tenders-offer');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Subscription`
--

CREATE TABLE IF NOT EXISTS `Frontend_Subscription` (
  `id` int(11) NOT NULL,
  `companyID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Tenders`
--

CREATE TABLE IF NOT EXISTS `Frontend_Tenders` (
  `ID` int(10) unsigned NOT NULL,
  `dateFromLoad` date NOT NULL,
  `dateToLoad` date NOT NULL,
  `datetimeFromTender` datetime NOT NULL,
  `datetimeToTender` datetime NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text,
  `cargoType` varchar(256) DEFAULT NULL,
  `transportTypeID` int(10) unsigned DEFAULT NULL,
  `volume` decimal(12,0) DEFAULT NULL,
  `weight` decimal(12,0) DEFAULT NULL,
  `carCount` int(10) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) DEFAULT NULL,
  `sizeY` decimal(12,3) DEFAULT NULL,
  `sizeZ` decimal(12,3) DEFAULT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `status` enum('active','inactive','completed','deleted','stopped') NOT NULL DEFAULT 'inactive',
  `price` decimal(12,2) unsigned NOT NULL,
  `currencyID` int(11) unsigned NOT NULL,
  `priceHide` enum('yes','no') NOT NULL DEFAULT 'no',
  `filterCompanyYears` int(10) unsigned DEFAULT NULL,
  `filterCompany` enum('yes','no') NOT NULL,
  `filterForwarder` enum('yes','no') NOT NULL DEFAULT 'no',
  `filterPartners` enum('yes','no') NOT NULL DEFAULT 'no',
  `stop_reason` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Tenders_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Tenders_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Transports`
--

CREATE TABLE IF NOT EXISTS `Frontend_Transports` (
  `ID` int(10) unsigned NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `priceID` int(10) unsigned NOT NULL,
  `userCarID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `docTIR` enum('yes','no') NOT NULL,
  `docCMR` enum('yes','no') NOT NULL,
  `docT1` enum('yes','no') NOT NULL,
  `docSanPassport` enum('yes','no') NOT NULL,
  `docSanBook` enum('yes','no') NOT NULL,
  `loadFromSide` enum('yes','no') NOT NULL,
  `loadFromTop` enum('yes','no') NOT NULL,
  `loadFromBehind` enum('yes','no') NOT NULL,
  `loadTent` enum('yes','no') NOT NULL,
  `condPlomb` enum('yes','no') NOT NULL,
  `condLoad` enum('yes','no') NOT NULL,
  `condBelts` enum('yes','no') NOT NULL,
  `condRemovableStands` enum('yes','no') NOT NULL,
  `condHardSide` enum('yes','no') NOT NULL,
  `condCollectableCargo` enum('yes','no') NOT NULL,
  `condTemperature` int(11) DEFAULT NULL,
  `condTemperatureTo` int(11) DEFAULT NULL,
  `condPalets` int(11) DEFAULT NULL,
  `condADR` int(11) DEFAULT NULL,
  `status` enum('active.have-no-proposition','active.have-confirmed','active.have-not-confirmed','completed','outdated') NOT NULL DEFAULT 'active.have-no-proposition',
  `parser` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8761 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Transport_to_Geo_Relation`
--

CREATE TABLE IF NOT EXISTS `Frontend_Transport_to_Geo_Relation` (
  `ID` int(10) unsigned NOT NULL,
  `requestID` int(10) unsigned NOT NULL,
  `geoID` int(10) unsigned NOT NULL,
  `type` enum('from','to') NOT NULL,
  `groupNumber` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_Users`
--

CREATE TABLE IF NOT EXISTS `Frontend_Users` (
  `ID` int(10) unsigned NOT NULL,
  `hash` char(128) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) NOT NULL,
  `emailApproved` enum('approved','pending') NOT NULL,
  `walletBalance` decimal(11,2) NOT NULL,
  `icq` varchar(20) DEFAULT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `currentRating` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL,
  `phone` varchar(45) NOT NULL,
  `phoneStationary` varchar(45) NOT NULL,
  `phoneCodeID` int(11) DEFAULT NULL,
  `phoneStationaryCodeID` int(11) DEFAULT NULL,
  `primaryLocaleID` int(10) unsigned NOT NULL,
  `parser` varchar(30) DEFAULT NULL,
  `parserMail` varchar(255) DEFAULT NULL,
  `deleted` enum('y','n') NOT NULL DEFAULT 'n' COMMENT 'on manager delete, makes it ''Y'''
) ENGINE=InnoDB AUTO_INCREMENT=22709 DEFAULT CHARSET=utf8;

--
-- Тригери `Frontend_Users`
--
DELIMITER $$
CREATE TRIGGER `check_user_company` AFTER UPDATE ON `Frontend_Users`
 FOR EACH ROW BEGIN
	IF NEW.companyID <> OLD.companyID THEN  
		    DELETE FROM Frontend_User_to_Company_Ownership WHERE
            userID = NEW.ID AND companyID = OLD.companyID;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Cars`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Cars` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `number` varchar(45) DEFAULT NULL,
  `info` text,
  `volume` decimal(12,3) unsigned DEFAULT NULL,
  `sizeX` decimal(12,3) unsigned DEFAULT NULL,
  `sizeY` decimal(12,3) unsigned DEFAULT NULL,
  `sizeZ` decimal(12,3) unsigned DEFAULT NULL,
  `liftingCapacity` decimal(12,3) unsigned DEFAULT NULL,
  `carTypeID` int(10) unsigned NOT NULL,
  `type` enum('vantazhivka','napiv prychip','z prychipom') NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active',
  `avatar` varchar(300) DEFAULT NULL COMMENT 'avatar file name from: Frontend_Uploader::DIR_UPL . Frontend_Uploader::DIR_VISIBLE_PUBLIC . Frontend_Uploader::DIR_CAR_GALLERY/$ID/$file"'
) ENGINE=InnoDB AUTO_INCREMENT=2070 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Car_Types`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Car_Types` (
  `ID` int(10) unsigned NOT NULL,
  `localeName` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Frontend_User_Car_Types`
--

INSERT INTO `Frontend_User_Car_Types` (`ID`, `localeName`, `name`) VALUES
(1, 'tent', 'Тент'),
(2, 'izoterm', 'Ізотерм'),
(3, 'refrigerator', 'Рефрижератор'),
(4, 'krytaya', 'Крытая'),
(5, 'konteyner', 'Контейнер'),
(6, 'ref', 'Реф.'),
(7, 'lesovoz', 'Лесовоз'),
(8, 'cel-nomet', 'Цельномет'),
(9, 'izoterm', 'Изотерм'),
(10, 'samosval', 'Самосвал'),
(11, 'konteynerovoz', 'Контейнеровоз'),
(12, 'bortovaya', 'Бортовая'),
(13, 'bus', 'Бус'),
(14, 'zernovoz', 'Зерновоз'),
(15, 'tral-negabarit', 'Трал / Негабарит'),
(16, 'otkrytaya', 'Открытая'),
(17, 'cisterna', 'Цистерна'),
(18, 'platforma', 'Платформа'),
(19, 'evakuator', 'Эвакуатор'),
(20, 'mikroavtobus', 'Микроавтобус'),
(21, 'skotovoz', 'Скотовоз'),
(22, 'avtocisterna', 'Автоцистерна'),
(23, 'special-naya-tehnika', 'Специальная техника'),
(24, 'plitovoz', 'Плитовоз'),
(25, 'avtovoz', 'Автовоз'),
(26, 'avtobus', 'Автобус'),
(27, 'mukovoz', 'Муковоз'),
(28, 'yahtovoz', 'Яхтовоз'),
(29, 'benzovoz', 'Бензовоз'),
(30, 'pogruzchik', 'Погрузчик'),
(31, 'trubovoz', 'Трубовоз'),
(32, 'kran', 'Кран'),
(33, 'steklovoz', 'Стекловоз'),
(34, 'kormovoz', 'Кормовоз');

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Locales`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Locales` (
  `ID` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Socials`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Socials` (
  `ID` int(10) unsigned NOT NULL,
  `network` varchar(45) NOT NULL,
  `identity` varchar(1000) NOT NULL,
  `socialUID` varchar(1000) NOT NULL,
  `email` varchar(100) NOT NULL,
  `userID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Cargo_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Cargo_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `cargoID` int(10) unsigned NOT NULL,
  `status` enum('accepted','rejected','pending') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Company_Ownership`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Company_Ownership` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `companyID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Tender_Auto_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Tender_Auto_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `tenderID` int(10) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Tender_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Tender_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `tenderID` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requestNumber` int(11) unsigned NOT NULL,
  `customPrice` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_to_Transport_Requests`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_to_Transport_Requests` (
  `ID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `transportID` int(10) unsigned NOT NULL,
  `status` enum('accepted','rejected','pending') NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `Frontend_User_Type_Rights`
--

CREATE TABLE IF NOT EXISTS `Frontend_User_Type_Rights` (
  `ID` int(10) unsigned NOT NULL,
  `group` varchar(45) NOT NULL,
  `userTypeID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
  `id` int(5) unsigned NOT NULL,
  `const` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` enum('const','text-block') NOT NULL DEFAULT 'const'
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `i18n`
--

INSERT INTO `i18n` (`id`, `const`, `type`) VALUES
(273, 'company.list.notFound', 'const'),
(276, 'company.type.transport', 'const'),
(278, 'company.list.notFoundBackLink', 'const'),
(279, 'error.404.companyNotFound', 'const'),
(280, 'company.page.title(:company)', 'const'),
(281, 'company.list.title', 'const'),
(285, 'company.page.rating', 'const'),
(286, 'company.page.activity', 'const'),
(287, 'company.page.location', 'const'),
(288, 'company.page.tab.infromation', 'const'),
(289, 'company.page.tab.requests(:count)', 'const'),
(290, 'company.page.tab.documents(:count)', 'const'),
(291, 'company.page.tab.transport(:count)', 'const'),
(301, 'site.title(:page_title)', 'const'),
(302, 'main_page.title', 'const'),
(317, 'company.page.documents.podatkovi', 'const'),
(318, 'company.page.documents.sertifikaty', 'const'),
(319, 'company.page.documents.svidoctva', 'const'),
(323, 'company.type.expeditor', 'const'),
(324, 'company.type.cargo', 'const'),
(327, 'company.registration.title', 'const'),
(328, 'transport.page.add.title', 'const'),
(329, 'cargo.page.add.title', 'const'),
(330, 'profile.title', 'const'),
(334, 'car.type.tent', 'const'),
(335, 'car.type.izoterm', 'const'),
(336, 'car.type.refrigerator', 'const'),
(337, 'currency.hrn', 'const'),
(338, 'car.body.vantazhivka', 'const'),
(339, 'car.body.napiv prychip', 'const'),
(340, 'car.body.z prychipom', 'const'),
(342, 'transport.page.list.title', 'const'),
(343, 'site.menu.header.cargo', 'const'),
(344, 'site.menu.header.transport', 'const'),
(345, 'site.menu.header.companies', 'const'),
(346, 'site.menu.header.press', 'const'),
(347, 'site.menu.header.advertisement', 'const'),
(348, 'site.menu.header.contacts', 'const'),
(349, 'tent', 'const'),
(350, 'izoterm', 'const'),
(351, 'refrigerator', 'const'),
(352, 'transport.list.notFound', 'const'),
(354, 'transport.list.notFoundInfo', 'const'),
(355, 'company.ownership.type.tzov', 'const'),
(356, 'company.ownership.type.pp', 'const'),
(357, 'company.ownership.type.fop', 'const'),
(358, 'company.ownership.type.spd', 'const'),
(359, 'company.ownership.type.fl-p', 'const'),
(360, 'company.ownership.type.chp', 'const'),
(361, 'company.ownership.type.flp', 'const'),
(362, 'company.ownership.type.ooo', 'const'),
(363, 'profile.add_tender.title', 'const'),
(364, 'form.error.email_available', 'const'),
(365, 'form.error.not_empty', 'const'),
(366, 'form.error.email', 'const'),
(367, 'form.error.max_length', 'const'),
(368, 'form.error.min_length', 'const'),
(369, 'form.error.numeric', 'const'),
(370, 'form.error.exact_length', 'const'),
(371, 'email.confirm_registration.title', 'const'),
(372, 'form.error.regex', 'const'),
(373, 'notification.auth.confirm_email', 'const'),
(374, 'tzov', 'const'),
(375, 'pp', 'const'),
(376, 'fop', 'const'),
(377, 'spd', 'const'),
(378, 'tender.page.list.title', 'const'),
(379, 'tender.page.title', 'const'),
(380, 'cargo.page.list.title', 'const'),
(381, 'krytaya', 'const'),
(382, 'konteyner', 'const'),
(383, 'ref', 'const'),
(384, 'lesovoz', 'const'),
(385, 'cel-nomet', 'const'),
(386, 'samosval', 'const'),
(393, 'konteynerovoz', 'const'),
(394, 'bortovaya', 'const'),
(395, 'bus', 'const'),
(396, 'cargo.list.notFound', 'const'),
(397, 'cargo.list.notFoundInfo', 'const'),
(398, 'zernovoz', 'const'),
(399, 'tral-negabarit', 'const'),
(400, 'otkrytaya', 'const'),
(401, 'cisterna', 'const'),
(407, 'platforma', 'const'),
(409, 'tenders.page.add.title', 'const'),
(410, 'evakuator', 'const'),
(411, 'mikroavtobus', 'const'),
(412, 'skotovoz', 'const'),
(413, 'avtocisterna', 'const'),
(414, 'special-naya-tehnika', 'const'),
(415, 'plitovoz', 'const'),
(416, 'avtovoz', 'const'),
(417, 'avtobus', 'const'),
(418, 'mukovoz', 'const'),
(419, 'yahtovoz', 'const'),
(420, 'benzovoz', 'const'),
(421, 'pogruzchik', 'const'),
(422, 'trubovoz', 'const'),
(423, 'kran', 'const'),
(424, 'steklovoz', 'const'),
(425, 'kormovoz', 'const'),
(426, 'site.menu.header.tenders', 'const'),
(427, 'partner.propose.to.be', 'const'),
(428, 'my.partners.list', 'const'),
(429, 'partner.is.not.confirmed', 'const'),
(430, 'request.form.geo_from', 'const'),
(431, 'request.form.geo_to', 'const'),
(432, 'label.attach', 'const'),
(433, 'profile.feedback.history', 'const'),
(434, 'frm.sort', 'const'),
(435, 'feedback.sort.all', 'const'),
(436, 'feedback.sort.positive', 'const'),
(437, 'feedback.sort.negative', 'const'),
(438, 'profile.partners.list', 'const'),
(439, 'partners.show.partners', 'const'),
(440, 'partners.sort.i.must.confirm', 'const'),
(441, 'partners.sort.i.must.wait.confirmation', 'const'),
(442, 'file.car.docs', 'const'),
(443, 'file.block.attach.car.docs', 'const'),
(444, 'file.block.delete.car.docs', 'const'),
(445, 'file.block.download.car.docs', 'const'),
(446, 'file.block.title.car.docs', 'const'),
(447, 'file.car.number.photo', 'const'),
(448, 'file.block.attach.car.number', 'const'),
(449, 'file.block.delete.car.number', 'const'),
(450, 'file.block.download.car.number', 'const'),
(451, 'file.block.title.car.number', 'const'),
(452, 'profile.my-transport.modal-edit.block.car-info', 'const'),
(453, 'profile.my-transport.modal-edit.block.transport-type', 'const'),
(454, 'some.tent', 'const'),
(455, 'some.izoterm', 'const'),
(456, 'some.refrigerator', 'const'),
(457, 'some.krytaya', 'const'),
(458, 'some.konteyner', 'const'),
(459, 'some.ref', 'const'),
(460, 'some.lesovoz', 'const'),
(461, 'some.cel-nomet', 'const'),
(462, 'some.samosval', 'const'),
(463, 'some.konteynerovoz', 'const'),
(464, 'some.bortovaya', 'const'),
(465, 'some.bus', 'const'),
(466, 'some.zernovoz', 'const'),
(467, 'some.tral-negabarit', 'const'),
(468, 'some.otkrytaya', 'const'),
(469, 'some.cisterna', 'const'),
(470, 'some.platforma', 'const'),
(471, 'some.evakuator', 'const'),
(472, 'some.mikroavtobus', 'const'),
(473, 'some.skotovoz', 'const'),
(474, 'some.avtocisterna', 'const'),
(475, 'some.special-naya-tehnika', 'const'),
(476, 'some.plitovoz', 'const'),
(477, 'some.avtovoz', 'const'),
(478, 'some.avtobus', 'const'),
(479, 'some.mukovoz', 'const'),
(480, 'some.yahtovoz', 'const'),
(481, 'some.benzovoz', 'const'),
(482, 'some.pogruzchik', 'const'),
(483, 'some.trubovoz', 'const'),
(484, 'some.kran', 'const'),
(485, 'some.steklovoz', 'const'),
(486, 'some.kormovoz', 'const'),
(487, 'profile.my-transport.modal-edit.block.car-technical-info', 'const'),
(488, 'profile.my-transport.modal-edit.lifting-possibilities', 'const'),
(489, 'units.tone', 'const'),
(490, 'profile.my-transport.modal-edit.block.volume-sizes', 'const'),
(491, 'units.meter', 'const'),
(492, 'profile.my-transport.modal-edit.block.XYZ-sizes', 'const'),
(493, 'unit-label.length', 'const'),
(494, 'unit-label.width', 'const'),
(495, 'unit-label.height', 'const'),
(496, 'profile.my-transport.modal-edit.block.text-info', 'const'),
(497, 'btn.save', 'const'),
(498, 'transport.tab.documents', 'const'),
(499, 'transport.tab.info', 'const'),
(500, 'transport.tab.gallery', 'const'),
(501, 'profile.tab.statistic.feedback', 'const'),
(502, 'profile.tab.statistic.transport.requests', 'const'),
(503, 'profile.tab.statistic.cargo.requests', 'const'),
(504, 'stat.duration.whole.time', 'const'),
(505, 'stat.feedback.proportion[:duration:]', 'const'),
(506, 'stat.feedback.chart', 'const'),
(507, 'stat.transport.proportion[:duration:]', 'const'),
(508, 'stat.transport.chart', 'const'),
(509, 'stat.cargo.proportion[:duration:]', 'const'),
(510, 'stat.cargo.chart', 'const'),
(511, 'stat.show.range.year', 'const'),
(512, 'stat.show.range.month', 'const'),
(513, 'stat.show.chart.range.diff', 'const'),
(514, 'stat.show.chart.range.all', 'const'),
(515, 'request.form.load_tent', 'const'),
(516, 'press-center-title', 'const'),
(517, 'request.form.geo_from_radius', 'const'),
(518, 'request.form.geo_to_radius', 'const'),
(519, 'tenders.bet.empty', 'const'),
(520, 'tenders.bet.not_accepted', 'const'),
(521, 'profile.tab.my-profile', 'const'),
(522, 'profile.tab.my-profile.requests-feedback', 'const'),
(523, 'profile.tab.my-profile.partners', 'const'),
(524, 'profile.tab.my-profile.news_partners', 'const'),
(525, 'profile.tab.my-profile.add_managers', 'const'),
(526, 'profile.tab.my-messages', 'const'),
(527, 'profile.tab.my-transport', 'const'),
(528, 'profile.tab.my-cargo-tickets', 'const'),
(531, 'profile.tab.my-transport-tickets', 'const'),
(532, 'profile.tab.my-tenders', 'const'),
(533, 'profile.tab.my-wallet', 'const'),
(534, 'profile.tab.my-infoblock', 'const'),
(535, 'profile.tab.my-statistic', 'const'),
(536, 'profile.tab.my-company', 'const'),
(537, 'profile.tab.my-managers', 'const'),
(538, 'profile.tab.my-subscription', 'const'),
(539, 'stat.feedback.chart.positive', 'const'),
(540, 'stat.feedback.chart.negative', 'const'),
(541, 'filter.template.added', 'const'),
(543, 'user.not.authorized.permission.denied', 'const'),
(548, 'tenders.bet.error.bet_ahead', 'const'),
(549, 'tenders.bet.higher', 'const'),
(550, 'tenders.bet.incorrect', 'const'),
(551, 'tenders.bet.not_best', 'const'),
(552, 'tenders.bet.success.message', 'const'),
(553, 'tenders.error.access.add_no_transporter', 'const'),
(554, 'tenders.error.access.add_only_authorized', 'const'),
(555, 'tenders.error.access.edit_only_authorized', 'const'),
(556, 'tenders.error.access.not_isset', 'const'),
(557, 'tenders.error.access.not_owner', 'const'),
(558, 'tenders.error.access.no_edit_active', 'const'),
(559, 'tenders.error.publish.fields', 'const'),
(560, 'tenders.error.publish.no_create', 'const'),
(561, 'tenders.model.notification.completed.subject', 'const'),
(562, 'tenders.model.notification.completed.subject.owner', 'const'),
(563, 'tenders.model.notification.completed.text.owner[:link:]', 'const'),
(564, 'tenders.model.notification.completed.text[:link:]', 'const'),
(565, 'tenders.model.notification.stop.subject', 'const'),
(566, 'tenders.model.notification.stop.text[:link:]', 'const'),
(567, 'tenders.page.additional', 'const'),
(568, 'tenders.page.add_place_load', 'const'),
(569, 'tenders.page.add_place_unload', 'const'),
(570, 'tenders.page.back', 'const'),
(571, 'tenders.page.cargo_type', 'const'),
(572, 'tenders.page.check_required_fields', 'const'),
(573, 'tenders.page.conditions', 'const'),
(574, 'tenders.page.conditions.belts', 'const'),
(575, 'tenders.page.conditions.bort', 'const'),
(576, 'tenders.page.conditions.collectable', 'const'),
(577, 'tenders.page.conditions.pallets', 'const'),
(578, 'tenders.page.conditions.plomb', 'const'),
(579, 'tenders.page.conditions.reload', 'const'),
(580, 'tenders.page.conditions.stands', 'const'),
(581, 'tenders.page.count_car', 'const'),
(582, 'tenders.page.create.title', 'const'),
(583, 'tenders.page.currency', 'const'),
(584, 'tenders.page.date_from_tender', 'const'),
(585, 'tenders.page.date_load_from', 'const'),
(586, 'tenders.page.date_load_to', 'const'),
(587, 'tenders.page.date_to_tender', 'const'),
(588, 'tenders.page.dimensions_car', 'const'),
(589, 'tenders.page.docs', 'const'),
(590, 'tenders.page.filter.company', 'const'),
(591, 'tenders.page.filter.forwarder', 'const'),
(592, 'tenders.page.filter.partners', 'const'),
(593, 'tenders.page.from[:count:]year', 'const'),
(594, 'tenders.page.from[:count:]years', 'const'),
(595, 'tenders.page.height', 'const'),
(596, 'tenders.page.hide_price', 'const'),
(597, 'tenders.page.length', 'const'),
(598, 'tenders.page.load_point', 'const'),
(599, 'tenders.page.load_type', 'const'),
(600, 'tenders.page.load_type.behind', 'const'),
(601, 'tenders.page.load_type.side', 'const'),
(602, 'tenders.page.load_type.tent', 'const'),
(603, 'tenders.page.load_type.top', 'const'),
(604, 'tenders.page.mark', 'const'),
(605, 'tenders.page.meters', 'const'),
(606, 'tenders.page.not_specified', 'const'),
(607, 'tenders.page.place_load', 'const'),
(608, 'tenders.page.place_unload', 'const'),
(609, 'tenders.page.publish', 'const'),
(610, 'tenders.page.recommended_price', 'const'),
(611, 'tenders.page.required', 'const'),
(612, 'tenders.page.required_options', 'const'),
(613, 'tenders.page.rules[:link:]', 'const'),
(614, 'tenders.page.sanitary_book', 'const'),
(615, 'tenders.page.sanitary_passport', 'const'),
(616, 'tenders.page.save', 'const'),
(617, 'tenders.page.specify', 'const'),
(618, 'tenders.page.term_company', 'const'),
(619, 'tenders.page.time_from_tender', 'const'),
(620, 'tenders.page.time_to_tender', 'const'),
(621, 'tenders.page.transport_type', 'const'),
(622, 'tenders.page.unload_point', 'const'),
(623, 'tenders.page.volume_cargo', 'const'),
(624, 'tenders.page.weight_cargo', 'const'),
(625, 'tenders.page.width', 'const'),
(626, 'tenders.success.publish.message', 'const'),
(627, 'tenders.validate.date_tender.from.empty', 'const'),
(628, 'tenders.validate.date_tender.from.incorrect', 'const'),
(629, 'tenders.validate.date_tender.to.empty', 'const'),
(630, 'tenders.validate.load_date.from.empty', 'const'),
(631, 'tenders.validate.load_date.from.incorrect', 'const'),
(632, 'tenders.validate.load_date.to.empty', 'const'),
(633, 'tenders.validate.place.from.empty', 'const'),
(634, 'tenders.validate.place.to.empty', 'const'),
(635, 'tenders.validate.price.incorrect', 'const'),
(636, 'tenders.validate.rules', 'const'),
(637, 'tenders.validate.volume_cargo.number', 'const'),
(638, 'tenders.validate.weight_cargo.empty', 'const'),
(639, 'tenders.validate.weight_cargo.number', 'const'),
(649, 'car.body.number.undefined', 'const'),
(650, 'car.body.samosval', 'const'),
(663, '??? ??????', 'const'),
(664, '??? ??????????', 'const'),
(665, 'profile.tab.infoblock.myinfo', 'const'),
(666, 'profile.tab.infoblock.mypass', 'const'),
(667, 'profile.tab.infoblock.companyinfo', 'const'),
(668, 'profile.tab.infoblock.newslist', 'const'),
(669, 'profile.tab.infoblock.newsadd', 'const'),
(670, 'placeholder.profile.icq', 'const'),
(671, 'hint.profile.icq', 'const'),
(672, 'placeholder.profile.skype', 'const'),
(673, 'hint.profile.skype', 'const'),
(674, 'placeholder.profile.email', 'const'),
(675, 'hint.profile.email', 'const'),
(676, 'placeholder.profile.phone', 'const'),
(677, 'hint.profile.phone', 'const'),
(678, 'placeholder.profile.phone.stationary', 'const'),
(679, 'hint.profile.phone.stationary', 'const'),
(680, 'placeholder.profile.name', 'const'),
(681, 'hint.profile.name', 'const'),
(682, 'placeholder.profile.password.current', 'const'),
(683, 'hint.profile.password.current', 'const'),
(684, 'placeholder.profile.password.new', 'const'),
(685, 'hint.profile.password.new', 'const'),
(686, 'placeholder.profile.password.repeat', 'const'),
(687, 'hint.profile.password.repeat', 'const'),
(688, 'placeholder.profile.company.ipn', 'const'),
(689, 'hint.profile.company.ipn', 'const'),
(690, 'placeholder.profile.company.type', 'const'),
(691, 'hint.profile.company.type', 'const'),
(692, 'placeholder.profile.company.ownsership', 'const'),
(693, 'hint.profile.company.ownsership', 'const'),
(694, 'placeholder.profile.company.phone', 'const'),
(695, 'hint.profile.company.phone', 'const'),
(696, 'placeholder.profile.company.phone.stationary', 'const'),
(697, 'hint.profile.company.phone.stationary', 'const'),
(698, 'placeholder.profile.company.name', 'const'),
(699, 'hint.profile.company.name', 'const'),
(700, 'placeholder.profile.company.address', 'const'),
(701, 'hint.profile.company.address', 'const'),
(702, 'placeholder.profile.company.realAddress', 'const'),
(703, 'hint.profile.company.realAddress', 'const'),
(704, 'placeholder.profile.company.info', 'const'),
(705, 'hint.profile.company.info', 'const'),
(706, 'placeholder.profile.news.title', 'const'),
(707, 'hint.profile.news.title', 'const'),
(708, 'placeholder.profile.news.preview', 'const'),
(709, 'hint.profile.news.preview', 'const'),
(710, 'placeholder.profile.news.text', 'const'),
(711, 'hint.profile.news.text', 'const'),
(712, 'btn.reset', 'const'),
(713, '?????????? ?? ????????', 'const'),
(714, '?????????? ?? ???????????? ???????', 'const'),
(715, '?????????? ?? ????????? ???????', 'const'),
(761, 'stat.request.transport', 'const'),
(762, 'stat.request.cargo.outdated', 'const'),
(763, 'stat.request.cargo.completed', 'const'),
(764, 'stat.request.cargo.have.confirmed.proposition', 'const'),
(765, 'stat.request.cargo.have.not.yet.confirmed.proposition', 'const'),
(766, 'stat.request.cargo.have.not.proposition', 'const'),
(767, 'stat.request.transport.outdated', 'const'),
(768, 'stat.request.transport.completed', 'const'),
(769, 'stat.request.transport.have.confirmed.proposition', 'const'),
(770, 'stat.request.transport.have.not.yet.confirmed.proposition', 'const'),
(771, 'stat.request.transport.have.not.proposition', 'const'),
(772, 'january.nominative', 'const'),
(773, 'february.nominative', 'const'),
(774, 'march.nominative', 'const'),
(775, 'april.nominative', 'const'),
(776, 'may.nominative', 'const'),
(777, 'june.nominative', 'const'),
(778, 'july.nominative', 'const'),
(779, 'august.nominative', 'const'),
(780, 'september.nominative', 'const'),
(781, 'october.nominative', 'const'),
(782, 'november.nominative', 'const'),
(783, 'december.nominative', 'const'),
(784, 'january.genitive', 'const'),
(785, 'february.genitive', 'const'),
(786, 'march.genitive', 'const'),
(787, 'april.genitive', 'const'),
(788, 'may.genitive', 'const'),
(789, 'june.genitive', 'const'),
(790, 'july.genitive', 'const'),
(791, 'august.genitive', 'const'),
(792, 'september.genitive', 'const'),
(793, 'october.genitive', 'const'),
(794, 'november.genitive', 'const'),
(795, 'december.genitive', 'const'),
(796, 'comments.rating', 'const'),
(797, 'tender.list.notFound', 'const'),
(798, 'tender.list.notFoundInfo', 'const'),
(799, 'news-company (:company)', 'const'),
(800, 'request.form.car_type', 'const'),
(801, 'tender_page.tender', 'const'),
(802, 'tender_page.back', 'const'),
(803, 'tender_page.condition_error', 'const'),
(804, 'tender_page.condition', 'const'),
(805, 'tender_page.companies_type', 'const'),
(806, 'tender_page.trans_and_exp', 'const'),
(807, 'tender_page.trans_only', 'const'),
(808, 'tender_page.company_status', 'const'),
(809, 'tender_page.company_finance_confirm', 'const'),
(810, 'tender_page.all', 'const'),
(811, 'tender_page.time_on_market', 'const'),
(812, 'tender_page.only_registered', 'const'),
(813, 'tender_page.max_price', 'const'),
(814, 'tender_page.hidden', 'const'),
(815, 'tender_page.you_bet_place', 'const'),
(816, 'tender_page.you_last_bet', 'const'),
(817, 'tender_page.to_finish', 'const'),
(818, 'tender_page.to_start', 'const'),
(819, 'tender_page.stop_for_reason', 'const'),
(820, 'tender_page.time_out', 'const'),
(821, 'tender_page.t', 'const'),
(822, 'tender_page.conditions', 'const'),
(823, 'tender_page.conditions_empty', 'const'),
(824, 'tender_page.recommended_bet', 'const'),
(825, 'tender_page.bet', 'const'),
(826, 'tender_page.suma', 'const'),
(827, 'tender_page.bets_list', 'const'),
(828, 'tender_page.company', 'const'),
(829, 'tender_page.data', 'const'),
(830, 'tender_page.you_bet', 'const'),
(831, 'tender_page.company_hide', 'const'),
(832, 'tender_page.bet_confirm', 'const'),
(833, 'tender_page.you_bet_is', 'const'),
(834, 'tender_page.offers[:link:]', 'const'),
(835, 'tender_page.go_bet', 'const'),
(836, 'tender_page.msec_1', 'const'),
(837, 'tender_page.sec_1', 'const'),
(838, 'tender_page.min_1', 'const'),
(839, 'tender_page.hour_1', 'const'),
(840, 'tender_page.day_1', 'const'),
(841, 'tender_page.week_1', 'const'),
(842, 'tender_page.year_1', 'const'),
(843, 'tender_page.msec_2', 'const'),
(844, 'tender_page.sec_2', 'const'),
(845, 'tender_page.min_2', 'const'),
(846, 'tender_page.hour_2', 'const'),
(847, 'tender_page.day_2', 'const'),
(848, 'tender_page.week_2', 'const'),
(849, 'tender_page.year_2', 'const'),
(850, 'transport.option.loadFromSide', 'const'),
(851, 'transport.option.loadFromTop', 'const'),
(852, 'transport.option.price', 'const'),
(853, 'transport.option.loadFromBehind', 'const'),
(854, 'user.application.successful', 'const'),
(855, 'enter.message', 'const'),
(856, 'message.send.success', 'const'),
(857, 'profile.partners.news.list', 'const'),
(858, 'generate.link', 'const'),
(859, 'profile.tab.my-profile.black_list', 'const'),
(860, 'add.to.black.list.success', 'const'),
(861, 'added.to.black.list.now', 'const'),
(862, 'feedback.error', 'const'),
(863, 'feedback.send.success', 'const'),
(864, 'profile.black.list', 'const'),
(865, 'profile.black.list.num', 'const'),
(866, 'profile.black.list.company', 'const'),
(867, 'profile.black.list.actions', 'const'),
(868, 'profile.black.list.del', 'const'),
(869, 'car.body.tent', 'const'),
(870, 'delete.with.black.list.success', 'const'),
(871, 'company.manager.name', 'const'),
(872, 'manage.company.manager', 'const'),
(873, 'tenders.auto_rate.incorrect', 'const'),
(874, 'sub.company.list', 'const'),
(875, 'sub.company.list.num', 'const'),
(876, 'sub.company.list.company', 'const'),
(877, 'sub.company.list.actions', 'const'),
(878, 'sub.company.list.del', 'const'),
(879, 'profile.tab.from', 'const'),
(880, 'profile.tab.weight_and_volume', 'const'),
(881, 'profile.tab.price', 'const'),
(882, 'profile.tab.bets', 'const'),
(883, 'profile.tab.status', 'const'),
(884, 'profile.tab.position', 'const'),
(885, 'profile.tab.owner_tenders', 'const'),
(886, 'profile.tab.to', 'const'),
(887, 'profile.tab.actions', 'const'),
(888, 'tenders.status.active', 'const'),
(889, 'tenders.status.will_active', 'const'),
(890, 'tenders.status.completed', 'const'),
(891, 'tenders.status.stopped', 'const'),
(892, 'tenders.no_owner_tenders', 'const'),
(893, 'tenders.no_tenders_created', 'const'),
(894, 'tenders.confirm_delete', 'const'),
(895, 'tenders.yes', 'const'),
(896, 'tenders.cancel', 'const'),
(897, 'tenders.confirm_stop', 'const'),
(898, 'tenders.write_reason', 'const'),
(899, 'tenders.delete_active_error', 'const'),
(900, 'tenders.server_error', 'const'),
(901, 'site.error.title', 'const'),
(902, 'site.error.content.500', 'const'),
(903, 'request.form.date_from', 'const'),
(904, 'request.form.date_to', 'const');

-- --------------------------------------------------------

--
-- Структура таблиці `i18n_locale`
--

CREATE TABLE IF NOT EXISTS `i18n_locale` (
  `i18nID` int(5) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `translate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `i18n_locale`
--

INSERT INTO `i18n_locale` (`i18nID`, `localeID`, `translate`) VALUES
(273, 1, 'Компаній по заданим критеріям не знайдено!'),
(273, 2, 'company.list.notFound'),
(276, 1, 'Транспорт'),
(276, 2, 'company.type.transport'),
(278, 1, 'Назад до списку компаній'),
(278, 2, 'company.list.notFoundBackLink'),
(279, 1, 'Компанії не існує'),
(280, 1, 'Компанія «:company»'),
(280, 2, 'company.page.title(:company)'),
(281, 1, 'Список компаній порталу'),
(281, 2, 'company.list.title'),
(285, 1, 'Рейтинг компанії'),
(285, 2, 'company.page.rating'),
(286, 1, 'Діяльність'),
(286, 2, 'company.page.activity'),
(287, 1, 'Розташування'),
(287, 2, 'company.page.location'),
(288, 1, 'Інформація'),
(288, 2, 'company.page.tab.infromation'),
(289, 1, 'Заявки на біржі (:count)'),
(289, 2, 'company.page.tab.requests(:count)'),
(290, 1, 'Документи (:count)'),
(290, 2, 'company.page.tab.documents(:count)'),
(291, 1, 'Наш транспорт (:count)'),
(291, 2, 'company.page.tab.transport(:count)'),
(301, 1, ':page_title - TIRSCAN'),
(301, 2, 'site.title(:page_title)'),
(302, 1, 'Головна сторінка'),
(302, 2, 'main_page.title'),
(317, 1, 'Податкові'),
(317, 2, 'company.page.documents.podatkovi'),
(318, 1, 'Сертифікати'),
(318, 2, 'company.page.documents.sertifikaty'),
(319, 1, 'Свідоцтва'),
(319, 2, 'company.page.documents.svidoctva'),
(323, 1, 'Експедитор'),
(323, 2, 'company.type.expeditor'),
(324, 1, 'Вантажотримач'),
(324, 2, 'company.type.cargo'),
(327, 1, 'Реєстрація на порталі'),
(327, 2, 'company.registration.title'),
(328, 1, 'Додати транспорт'),
(328, 2, 'transport.page.add.title'),
(329, 1, 'Додати вантаж'),
(329, 2, 'cargo.page.add.title'),
(330, 1, 'Мій профіль'),
(334, 1, 'Тент'),
(335, 1, 'Ізотерм'),
(336, 1, 'Рефрижератор'),
(337, 1, 'currency.hrn'),
(338, 1, 'вантажівка'),
(339, 1, 'напівпричіп'),
(340, 1, 'з причіпом'),
(342, 1, 'Транспорт'),
(342, 2, 'transport.page.list.title'),
(343, 1, 'Вантажі'),
(343, 2, 'site.menu.header.cargo'),
(344, 1, 'Транспорт'),
(344, 2, 'site.menu.header.transport'),
(345, 1, 'Компанії'),
(345, 2, 'site.menu.header.companies'),
(346, 1, 'Прес-центр'),
(346, 2, 'site.menu.header.press'),
(347, 1, 'Оголошення'),
(347, 2, 'site.menu.header.advertisement'),
(348, 1, 'Контакти'),
(348, 2, 'site.menu.header.contacts'),
(349, 1, 'тент'),
(349, 2, 'tent'),
(350, 1, 'ізотерм'),
(350, 2, 'izoterm'),
(351, 1, 'рефрижератор'),
(351, 2, 'refrigerator'),
(352, 1, 'Транспортних заявок по заданому фільтру не знайдено!'),
(354, 1, 'Наразі в системі не зареєстровано заявок, що відповідають вказаним фільтрам. Спробуйте пізніше або змініть критерії фільтрування.'),
(355, 1, 'ТзОВ'),
(356, 1, 'ПП'),
(357, 1, 'ФОП'),
(358, 1, 'СПД'),
(359, 1, 'ФЛ-П'),
(360, 1, 'ЧП'),
(361, 1, 'ФЛП'),
(362, 1, 'ООО'),
(363, 1, 'Створити тендер'),
(364, 1, 'Вказаний e-mail вже зайнятий у системі'),
(365, 1, 'Поле обов''язкове для заповнення'),
(366, 1, 'Введіть правильну e-mail адресу'),
(367, 1, 'Кількість символів не повинна перевищувати :1'),
(368, 1, 'Мінімально допустима кількість символів - :1'),
(369, 1, 'Допускаються лише цифри'),
(370, 1, 'Допустима кількість символів - :1'),
(371, 1, 'email.confirm_registration.title'),
(372, 1, 'Допускаються лише літери латинського алфавіту, цифри та спецсимволи !@#$%^_- (пароль повинен починатись з літери)'),
(373, 1, 'Вам необхідно підтвердити ваш e-mail. Вам було відправлено лист для підтвердження реєстрації'),
(374, 1, 'ТЗОВ'),
(374, 2, 'tzov'),
(375, 1, 'ПП'),
(375, 2, 'pp'),
(376, 1, 'ФОП'),
(376, 2, 'fop'),
(377, 1, 'СПД'),
(377, 2, 'spd'),
(378, 1, 'Список тендерів'),
(379, 1, 'Тендер №125478'),
(380, 1, 'Вантажі'),
(380, 2, 'cargo.page.list.title'),
(381, 1, 'крита'),
(381, 2, 'krytaya'),
(382, 1, 'контейнер'),
(382, 2, 'konteyner'),
(383, 1, 'рефрижератор'),
(383, 2, 'ref'),
(384, 1, 'лісовоз'),
(384, 2, 'lesovoz'),
(385, 1, 'цільномет'),
(385, 2, 'cel-nomet'),
(386, 1, 'самосвал'),
(386, 2, 'samosval'),
(393, 1, 'контейнеровоз'),
(393, 2, 'konteynerovoz'),
(394, 1, 'бортова'),
(394, 2, 'bortovaya'),
(395, 1, 'бус'),
(395, 2, 'bus'),
(396, 1, 'cargo.list.notFound'),
(397, 1, 'cargo.list.notFoundInfo'),
(398, 1, 'зерновоз'),
(398, 2, 'zernovoz'),
(399, 1, 'трал-негабаріт'),
(399, 2, 'tral-negabarit'),
(400, 1, 'відкрита'),
(400, 2, 'otkrytaya'),
(401, 1, 'цистерна'),
(401, 2, 'cisterna'),
(407, 1, 'платформа'),
(407, 2, 'platforma'),
(409, 1, 'Створення тендера'),
(410, 1, 'евакуатор'),
(411, 1, 'мікроавтобус'),
(412, 1, 'скотовоз'),
(413, 1, 'автоцистерна'),
(414, 1, 'спец. техніка'),
(415, 1, 'плитовоз'),
(416, 1, 'автовоз'),
(417, 1, 'автобус'),
(418, 1, 'муковоз'),
(419, 1, 'яхтовоз'),
(420, 1, 'бензовоз'),
(421, 1, 'навантажувач'),
(422, 1, 'трубовоз'),
(423, 1, 'кран'),
(424, 1, 'скловоз'),
(425, 1, 'кормовоз'),
(426, 1, 'Тендери'),
(427, 1, 'Запропонувати стати партнером'),
(428, 1, 'Партнери'),
(429, 1, 'Партнер не підтвердив запит'),
(430, 1, 'Місце з'),
(431, 1, 'Місце до'),
(432, 1, 'Прикріпити'),
(433, 1, 'Історія відгуків'),
(434, 1, 'Сортування'),
(435, 1, 'feedback.sort.all'),
(436, 1, 'feedback.sort.positive'),
(437, 1, 'feedback.sort.negative'),
(438, 1, 'Список партнерів'),
(439, 1, 'Показати партнерів'),
(440, 1, 'Очікують мого підтвердження'),
(441, 1, 'Очкують підвердження зі сторони партнера'),
(442, 1, 'документи'),
(443, 1, 'file.block.attach.car.docs'),
(444, 1, 'file.block.delete.car.docs'),
(445, 1, 'file.block.download.car.docs'),
(446, 1, 'file.block.title.car.docs'),
(447, 1, 'фото'),
(448, 1, 'file.block.attach.car.number'),
(449, 1, 'file.block.delete.car.number'),
(450, 1, 'file.block.download.car.number'),
(451, 1, 'file.block.title.car.number'),
(452, 1, 'Інформація про траснпорт'),
(453, 1, 'Тип трансопрту'),
(454, 1, 'тент'),
(455, 1, 'ізотерм'),
(456, 1, 'рефрижератор'),
(457, 1, 'крита'),
(458, 1, 'контейнер'),
(459, 1, 'рефрижератор'),
(460, 1, 'лісовоз'),
(461, 1, 'цільномет'),
(462, 1, 'самоскид'),
(463, 1, 'контейнеровоз'),
(464, 1, 'бортова'),
(465, 1, 'бус'),
(466, 1, 'зерновоз'),
(467, 1, 'трал-негабарит'),
(468, 1, 'відкрита'),
(469, 1, 'цизтерна'),
(470, 1, 'платформа'),
(471, 1, 'евакуатор'),
(472, 1, 'мікроавтобус'),
(473, 1, 'скотовоз'),
(474, 1, 'автоцизтерна'),
(475, 1, 'спец. техніка'),
(476, 1, 'плитовоз'),
(477, 1, 'автовоз'),
(478, 1, 'автобус'),
(479, 1, 'муковоз'),
(480, 1, 'яхтовоз'),
(481, 1, 'бензовоз'),
(482, 1, 'навантажувач'),
(483, 1, 'трубовоз'),
(484, 1, 'кран'),
(485, 1, 'скловоз'),
(486, 1, 'кормовоз'),
(487, 1, 'Технічна інформація'),
(488, 1, 'Вантажопідйомність'),
(489, 1, 'тон'),
(490, 1, 'Об''єм \\ Розміри'),
(491, 1, 'метрів'),
(492, 1, 'Габарити'),
(493, 1, 'довжина'),
(494, 1, 'ширина'),
(495, 1, 'висота'),
(496, 1, 'Опис'),
(497, 1, 'Зберегти'),
(498, 1, 'Документи'),
(499, 1, 'Транспорт'),
(500, 1, 'Галерея'),
(501, 1, 'Статистика по відгукам'),
(502, 1, 'Статистика по транспортним заявкам'),
(503, 1, 'Статистика по вантажним заявкам'),
(504, 1, 'За весь час'),
(505, 1, 'Графік відгуків за період [:duration:]'),
(506, 1, 'Графік статистики'),
(507, 1, 'Графік транспортних заявок за період [:duration:]'),
(508, 1, 'Графік транспортних заявок'),
(509, 1, 'Графік вантижних заявок за період [:duration:]'),
(510, 1, 'Графік вантажних заявок'),
(511, 1, 'Річна статистика'),
(512, 1, 'За місяць'),
(513, 1, 'Лише диаміка змін за обраний період'),
(514, 1, 'Включаючі попередні дані'),
(515, 1, 'request.form.load_tent'),
(516, 1, 'Прес центр'),
(517, 1, 'request.form.geo_from_radius'),
(518, 1, 'request.form.geo_to_radius'),
(519, 1, 'Ставки відсутні'),
(520, 1, 'Ставки більше не приймаються'),
(521, 1, 'Мій профіль'),
(522, 1, 'Відгуки по заявкам'),
(523, 1, 'Мої партнери'),
(524, 1, 'Новини партнерів'),
(525, 1, 'Додати менеджера'),
(526, 1, 'Мої повідомлення'),
(527, 1, 'Мій транспорт'),
(528, 1, 'Мої вантажні заявки'),
(531, 1, 'Мої траспортні заявки'),
(532, 1, 'Мої тендери'),
(533, 1, 'Мій гаманець'),
(534, 1, 'Інфоблок'),
(535, 1, 'Статистика'),
(536, 1, 'Моя компанія'),
(537, 1, 'Мої менеджери'),
(538, 1, 'Мої підписки'),
(539, 1, 'Позитивні'),
(540, 1, 'Негативні'),
(541, 1, 'filter.template.added'),
(543, 1, 'Ви не авторизовані. Доступ заборонений'),
(548, 1, 'Вашу ставку випередили. Зробіть нову ставку'),
(549, 1, 'Ваша ставка є вищою за рекомендовану. Вкажіть ставку із нижчим значенням'),
(550, 1, 'Ви ввели не коректне значення ставки'),
(551, 1, 'Ваша ставка не є найкращою. Вкажіть ставку із нижчим значенням'),
(552, 1, 'Ваша ставка успішно прийнята'),
(553, 1, 'Транспортна компанія немає доступу до створення тендера'),
(554, 1, 'Створити тендер можуть лише авторизовані користувачі'),
(555, 1, 'Для редагування тендера потрібно авторизуватися у системі'),
(556, 1, 'За даним посиланням тендера не існує або він був видалений'),
(557, 1, 'Доступ до редагування має лише власник тендера'),
(558, 1, 'Редагувати можна лише неактивний тендер'),
(559, 1, 'Помилка. Заповніть усі обов''язкові поля і повторіть спробу'),
(560, 1, 'tenders.error.publish.no_create'),
(561, 1, 'Тендер завершено'),
(562, 1, 'Ваш тендер завершено'),
(563, 1, 'Тендер створений вами завершено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(564, 1, 'Тендер в якому ви взяли участь успішно завершено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(565, 1, 'Ваш тендер зупинено'),
(566, 1, 'Ваш тендер зупинено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(567, 1, 'Додаткова інформація'),
(568, 1, 'Додати місце завантаження'),
(569, 1, 'Додати місце розвантаження'),
(570, 1, 'tenders.page.back'),
(571, 1, 'Тип вантажу'),
(572, 1, 'Відмітьте необхідні поля'),
(573, 1, 'tenders.page.conditions'),
(574, 1, 'Ремені'),
(575, 1, 'Жорсткий борт'),
(576, 1, 'Збірний вантаж'),
(577, 1, 'Палети'),
(578, 1, 'Пломба'),
(579, 1, 'Довантаження'),
(580, 1, 'З''ємні стійки'),
(581, 1, 'Кількість машин'),
(582, 1, 'Створення тендера'),
(583, 1, 'Валюта'),
(584, 1, 'Дата початку торгів'),
(585, 1, 'Дата завантаження від'),
(586, 1, 'Дата завантаження до'),
(587, 1, 'Дата закінчення торгів'),
(588, 1, 'Габарити транспортного засобу'),
(589, 1, 'Документи'),
(590, 1, 'Дозволено брати участь компаніям лише з підтвердженими платіжними реквізитами'),
(591, 1, 'Дозволено брати участь експедиторам'),
(592, 1, 'Дозволено брати участь лише партнерам'),
(593, 1, 'tenders.page.from[:count:]year'),
(594, 1, 'від [:count:] років'),
(595, 1, 'Висота'),
(596, 1, 'Приховати рекомендовану ціну'),
(597, 1, 'Довжина'),
(598, 1, 'Пункт завантаження'),
(599, 1, 'Завантаження'),
(600, 1, 'Ззаду'),
(601, 1, 'Збоку'),
(602, 1, 'Розтентування'),
(603, 1, 'Зверху'),
(604, 1, 'Примітка'),
(605, 1, 'tenders.page.meters'),
(606, 1, 'не вказано'),
(607, 1, 'Місце завантаження'),
(608, 1, 'Місце розвантаження'),
(609, 1, 'tenders.page.publish'),
(610, 1, 'Рекомендована ціна'),
(611, 1, 'поля обов''язкові до заповнення'),
(612, 1, 'одне із полів є обов''язковим до заповнення'),
(613, 1, 'Я ознайомився і згідний з <a target=''_blank'' href=''[:link:]''>правилами</a> подачі тендерів на сайті'),
(614, 1, 'Санкнижка'),
(615, 1, 'Санпаспорт'),
(616, 1, 'ЗБЕРЕГТИ'),
(617, 1, 'вказати'),
(618, 1, 'Tермін діяльності компанії на ринку'),
(619, 1, 'Час початку торгів'),
(620, 1, 'Час закінчення торгів'),
(621, 1, 'Тип транспорту'),
(622, 1, 'Пункт розвантаження'),
(623, 1, 'Об''єм вантажу'),
(624, 1, 'Вага вантажу'),
(625, 1, 'Ширина'),
(626, 1, 'Тендер успішно оновлено'),
(627, 1, 'Вкажіть дату початку торгів'),
(628, 1, 'Дата тендера вказана не коректно'),
(629, 1, 'Вкажіть дату закінчення торгів'),
(630, 1, 'Вкажіть дату завантаження ''з'''),
(631, 1, 'Дата завантаження вказана не коректно'),
(632, 1, 'Вкажіть дату завантаження ''до'''),
(633, 1, 'Вкажіть місце завантаження'),
(634, 1, 'Вкажіть місце розвантаження'),
(635, 1, 'Ціна вказана невірно'),
(636, 1, 'Ознайомтеся з правилами подачі тендерів на сайті'),
(637, 1, 'Об''єм вантажу повинен бути додатнім числом'),
(638, 1, 'Вкажіть вагу або об''єм вантажу'),
(639, 1, 'Вага вантажу повинна бути додатнім числом'),
(649, 1, 'Не вказано'),
(650, 1, 'Самосвал'),
(663, 1, 'Мої заявки'),
(664, 1, 'Мої пропозиції'),
(665, 1, 'Моя інформація'),
(666, 1, 'Мій пароль'),
(667, 1, 'Інформація про компанію'),
(668, 1, 'Список новин'),
(669, 1, 'Добавити новину'),
(670, 1, 'Введіть ICQ'),
(671, 1, 'ICQ'),
(672, 1, 'Введіть ваш Skype'),
(673, 1, 'Skype'),
(674, 1, 'Введіть поштову скринку'),
(675, 1, 'Електронна пошта'),
(676, 1, 'Введіть ваш телефон'),
(677, 1, 'Ваш телефон'),
(678, 1, 'Введіть ваш стаціонарний телефон'),
(679, 1, 'Ваш стаціонарний телефон'),
(680, 1, 'Введіть ваше Ім''я'),
(681, 1, 'Ім''я'),
(682, 1, 'Введіть поточний пароль'),
(683, 1, 'Поточний пароль'),
(684, 1, 'Введіть новий пароль'),
(685, 1, 'Новий пароль'),
(686, 1, 'Повторіть новий пароль'),
(687, 1, 'Повторіть новий пароль'),
(688, 1, 'Введіть свій IPN'),
(689, 1, 'ІПН'),
(690, 1, 'Введіть ип компанії'),
(691, 1, 'Тип компаанії'),
(692, 1, 'Введіть тип власності компанії'),
(693, 1, 'Форма власності'),
(694, 1, 'Введіть телефон компанії'),
(695, 1, 'Телефон'),
(696, 1, 'Введіть стаціонарний телефон компанії'),
(697, 1, 'Стаціонарний телефон'),
(698, 1, 'Введіть назву компанії'),
(699, 1, 'Назва'),
(700, 1, 'Введіть адресу компанії'),
(701, 1, 'Адреса компанії'),
(702, 1, 'Введіть фізичну адресу компанії'),
(703, 1, 'Фізична адреса'),
(704, 1, 'Введіть інформацію про компанію'),
(705, 1, 'Інформація про компанію'),
(706, 1, 'Введіть заголовок'),
(707, 1, 'Заголовок новини'),
(708, 1, 'Введіть текст'),
(709, 1, 'Анонс'),
(710, 1, 'placeholder.profile.news.text'),
(711, 1, 'Текст новини'),
(712, 1, 'Зкинути'),
(713, 1, 'Статистика по відгукам'),
(714, 1, 'Статистика по транспортним заявкам'),
(715, 1, 'Статистика по вантажним заявкам'),
(761, 1, 'stat.request.transport'),
(762, 1, 'stat.request.cargo.outdated'),
(763, 1, 'stat.request.cargo.completed'),
(764, 1, 'stat.request.cargo.have.confirmed.proposition'),
(765, 1, 'stat.request.cargo.have.not.yet.confirmed.proposition'),
(766, 1, 'stat.request.cargo.have.not.proposition'),
(767, 1, 'stat.request.transport.outdated'),
(768, 1, 'stat.request.transport.completed'),
(769, 1, 'stat.request.transport.have.confirmed.proposition'),
(770, 1, 'stat.request.transport.have.not.yet.confirmed.proposition'),
(771, 1, 'stat.request.transport.have.not.proposition'),
(772, 1, 'Січень'),
(773, 1, 'Лютий'),
(774, 1, 'Березень'),
(775, 1, 'Квітень'),
(776, 1, 'Травень'),
(777, 1, 'Червень'),
(778, 1, 'Липень'),
(779, 1, 'Серпень'),
(780, 1, 'Вересень'),
(781, 1, 'Жовтень'),
(782, 1, 'Листопад'),
(783, 1, 'Грудень'),
(784, 1, 'Січня'),
(785, 1, 'Лютого'),
(786, 1, 'Березня'),
(787, 1, 'Квітня'),
(788, 1, 'Травня'),
(789, 1, 'Червня'),
(790, 1, 'Липня'),
(791, 1, 'Серпня'),
(792, 1, 'Вересня'),
(793, 1, 'Жовтня'),
(794, 1, 'Листопада'),
(795, 1, 'Грудня'),
(796, 1, 'comments.rating'),
(797, 1, 'tender.list.notFound'),
(798, 1, 'tender.list.notFoundInfo'),
(799, 1, 'news-company (:company)'),
(800, 1, 'request.form.car_type'),
(801, 1, 'Тендер'),
(802, 1, 'повернутися назад'),
(803, 1, 'Не виконані всі умови участі у тендері'),
(804, 1, 'Умови участі в тендері'),
(805, 1, 'Тип компаній'),
(806, 1, 'транспортні і експедиторські компанії'),
(807, 1, 'лише транспортні компанії'),
(808, 1, 'Статус компаній'),
(809, 1, 'лише з підтвердженими платіжними реквізитами'),
(810, 1, 'всі'),
(811, 1, 'Термін діяльності компанії на ринку'),
(812, 1, 'Тільки зареєстровані користувачі'),
(813, 1, 'Максимальна ціна'),
(814, 1, 'прихована'),
(815, 1, 'Місце вашої ставки'),
(816, 1, 'Ваша остання ставка'),
(817, 1, 'До закінчення'),
(818, 1, 'До початку'),
(819, 1, 'Тендер зупинено по причині'),
(820, 1, 'Час вийшов'),
(821, 1, 'Температура'),
(822, 1, 'Умови'),
(823, 1, 'Додаткова інформація не вказана'),
(824, 1, 'Рекомендована ставка'),
(825, 1, 'Ставка'),
(826, 1, 'сума'),
(827, 1, 'Список ставок'),
(828, 1, 'Компанія'),
(829, 1, 'Дата'),
(830, 1, 'Ваша ставка'),
(831, 1, 'Інформацію про компанію приховано'),
(832, 1, 'Підтвердження ставки'),
(833, 1, 'Ваша ставка складатиме'),
(834, 1, 'Зробивши ставку ви підтверджуєте <a target=''_blank'' href=''[:link:]''> публічну оферту </a> на користування тендерним режимом порталу.'),
(835, 1, 'Зробити ставку'),
(836, 1, 'мілісекунда'),
(837, 1, 'секунда'),
(838, 1, 'хвилина'),
(839, 1, 'година'),
(840, 1, 'день'),
(841, 1, 'тиждень'),
(842, 1, 'рік'),
(843, 1, 'мілісекунд'),
(844, 1, 'секунд'),
(845, 1, 'хвилин'),
(846, 1, 'годин'),
(847, 1, 'днів'),
(848, 1, 'тижнів'),
(849, 1, 'років'),
(850, 1, 'Загрузка збоку'),
(851, 1, 'Загрузка зверху'),
(852, 1, 'Ціна'),
(853, 1, 'Загрузка ззаду'),
(854, 1, 'user.application.successful'),
(855, 1, 'Заповніть поле повідомлення'),
(856, 1, 'Повідомлення успішно добавлено'),
(857, 1, 'Новини на які компанія підписана'),
(858, 1, 'Згенерувати ссилку'),
(859, 1, 'Чорний список'),
(860, 1, 'Ви успішно додали компанію до чорного списку'),
(861, 1, 'Компанія вже додана до чорного списку'),
(862, 1, 'Заповніть поле "Повідомлення" або "E-mail"'),
(863, 1, 'Повідомлення відправлене успішно'),
(864, 1, 'Чорний список'),
(865, 1, '№'),
(866, 1, 'Назва компанії'),
(867, 1, 'Дії'),
(868, 1, 'Видалити із списку'),
(869, 1, 'car.body.tent'),
(870, 1, 'Компанію видалено із чорного списку'),
(871, 1, 'company.manager.name'),
(872, 1, 'manage.company.manager'),
(873, 1, 'Автоматична ставка не коректна'),
(874, 1, 'Компанії'),
(875, 1, '№'),
(876, 1, 'Компанії на які ви підписані'),
(877, 1, 'Дії'),
(878, 1, 'Відписатись'),
(879, 1, 'Звідки'),
(880, 1, 'Вага і об''єм'),
(881, 1, 'Ціна'),
(882, 1, 'Ставки'),
(883, 1, 'Статус'),
(884, 1, 'Позиція'),
(885, 1, 'profile.tab.owner_tenders'),
(886, 1, 'Куди'),
(887, 1, 'Дії'),
(888, 1, 'Активний<br>до'),
(889, 1, 'Буде активований<br>в'),
(890, 1, 'Завершений<br>в'),
(891, 1, 'Зупинений'),
(892, 1, 'Ви ще не брали участь в тендерах'),
(893, 1, 'Ви ще не створювали тендерів'),
(894, 1, 'Ви підтверджуєте видалення тендера?'),
(895, 1, 'Так'),
(896, 1, 'Відмінити'),
(897, 1, 'Ви підтверджуєте зупинку тендера?'),
(898, 1, 'Вкажіть причину'),
(899, 1, 'Видалення активованого тендера заборонено'),
(900, 1, 'Помилка сервера. Повторіть дію пізніше'),
(901, 1, 'site.error.title'),
(902, 1, 'site.error.content.500'),
(903, 1, 'request.form.date_from'),
(904, 1, 'request.form.date_to');

-- --------------------------------------------------------

--
-- Структура таблиці `i18n_locale_sasha`
--

CREATE TABLE IF NOT EXISTS `i18n_locale_sasha` (
  `i18nID` int(5) unsigned NOT NULL,
  `localeID` int(10) unsigned NOT NULL,
  `translate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `i18n_locale_sasha`
--

INSERT INTO `i18n_locale_sasha` (`i18nID`, `localeID`, `translate`) VALUES
(448, 1, 'Створення тендера'),
(449, 1, 'tenders.page.back'),
(450, 1, 'поля обов''язкові до заповнення'),
(451, 1, 'одне із полів є обов''язковим до заповнення'),
(452, 1, 'Дата завантаження від'),
(453, 1, 'Дата завантаження до'),
(454, 1, 'Місце завантаження'),
(455, 1, 'Пункт завантаження'),
(456, 1, 'Додати місце завантаження'),
(457, 1, 'Місце розвантаження'),
(458, 1, 'Пункт розвантаження'),
(459, 1, 'Додати місце розвантаження'),
(460, 1, 'Тип вантажу'),
(461, 1, 'Вага вантажу'),
(462, 1, 'Об''єм вантажу'),
(463, 1, 'Тип транспорту'),
(464, 1, 'не вказано'),
(465, 1, 'Кількість машин'),
(466, 1, 'Габарити транспортного засобу'),
(467, 1, 'Довжина'),
(468, 1, 'tenders.page.meters'),
(469, 1, 'Ширина'),
(470, 1, 'Висота'),
(471, 1, 'Додаткова інформація'),
(472, 1, 'вказати'),
(473, 1, 'Дата початку торгів'),
(474, 1, 'Час початку торгів'),
(475, 1, 'Дата закінчення торгів'),
(476, 1, 'Час закінчення торгів'),
(477, 1, 'Рекомендована ціна'),
(478, 1, 'Валюта'),
(479, 1, 'Приховати рекомендовану ціну'),
(480, 1, 'Tермін діяльності компанії на ринку'),
(481, 1, 'tenders.page.from[:count:]year'),
(482, 1, 'від [:count:] років'),
(483, 1, 'Дозволено брати участь компаніям лише з підтвердженими платіжними реквізитами'),
(484, 1, 'Дозволено брати участь експедиторам'),
(485, 1, 'Дозволено брати участь лише партнерам'),
(486, 1, 'Я ознайомився і згідний з <a target=''_blank'' href=''[:link:]''>правилами</a> подачі тендерів на сайті'),
(487, 1, 'tenders.page.publish'),
(488, 1, 'Відмітьте необхідні поля'),
(489, 1, 'Документи'),
(490, 1, 'Санпаспорт'),
(491, 1, 'Санкнижка'),
(492, 1, 'Завантаження'),
(493, 1, 'Збоку'),
(494, 1, 'Зверху'),
(495, 1, 'Ззаду'),
(496, 1, 'Розтентування'),
(497, 1, 'tenders.page.conditions'),
(498, 1, 'Пломба'),
(499, 1, 'Довантаження'),
(500, 1, 'Ремені'),
(501, 1, 'З''ємні стійки'),
(502, 1, 'Жорсткий борт'),
(503, 1, 'Збірний вантаж'),
(504, 1, 'Палети'),
(505, 1, 'Примітка'),
(506, 1, 'ЗБЕРЕГТИ'),
(507, 1, 'Вкажіть дату завантаження ''з'''),
(508, 1, 'Вкажіть дату завантаження ''до'''),
(509, 1, 'Дата завантаження вказана не коректно'),
(510, 1, 'Вкажіть місце завантаження'),
(511, 1, 'Вкажіть місце розвантаження'),
(512, 1, 'Вкажіть вагу або об''єм вантажу'),
(513, 1, 'Вага вантажу повинна бути додатнім числом'),
(514, 1, 'Об''єм вантажу повинен бути додатнім числом'),
(515, 1, 'Вкажіть дату початку торгів'),
(516, 1, 'Вкажіть дату закінчення торгів'),
(517, 1, 'Дата тендера вказана не коректно'),
(518, 1, 'Ціна вказана невірно'),
(519, 1, 'Ознайомтеся з правилами подачі тендерів на сайті'),
(520, 1, 'Ви ввели не коректне значення ставки'),
(521, 1, 'Ваша ставка не є найкращою. Вкажіть ставку із нижчим значенням'),
(522, 1, 'Ваша ставка є вищою за рекомендовану. Вкажіть ставку із нижчим значенням'),
(523, 1, 'Ставки відсутні'),
(524, 1, 'Ставки більше не приймаються'),
(525, 1, 'Ваш тендер зупинено'),
(526, 1, 'Ваш тендер зупинено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(527, 1, 'Ваш тендер завершено'),
(528, 1, 'Тендер створений вами завершено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(529, 1, 'Тендер завершено'),
(530, 1, 'Тендер в якому ви взяли участь успішно завершено. <a href=''[:link:]''>Перейдіть за посиланням для детальної інформації</a>'),
(531, 1, 'tenders.error.publish.no_create'),
(532, 1, 'Для редагування тендера потрібно авторизуватися у системі'),
(533, 1, 'За даним посиланням тендера не існує або він був видалений'),
(534, 1, 'Доступ до редагування має лише власник тендера'),
(535, 1, 'Редагувати можна лише неактивний тендер'),
(536, 1, 'Помилка. Заповніть усі обов''язкові поля і повторіть спробу'),
(537, 1, 'Тендер успішно оновлено'),
(538, 1, 'Створити тендер можуть лише авторизовані користувачі'),
(539, 1, 'Транспортна компанія немає доступу до створення тендера'),
(540, 1, 'Ваша ставка успішно прийнята'),
(541, 1, 'Вашу ставку випередили. Зробіть нову ставку');

-- --------------------------------------------------------

--
-- Структура таблиці `i18n_sasha`
--

CREATE TABLE IF NOT EXISTS `i18n_sasha` (
  `id` int(5) unsigned NOT NULL,
  `const` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `type` enum('const','text-block') NOT NULL DEFAULT 'const'
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `i18n_sasha`
--

INSERT INTO `i18n_sasha` (`id`, `const`, `type`) VALUES
(448, 'tenders.page.create.title', 'const'),
(449, 'tenders.page.back', 'const'),
(450, 'tenders.page.required', 'const'),
(451, 'tenders.page.required_options', 'const'),
(452, 'tenders.page.date_load_from', 'const'),
(453, 'tenders.page.date_load_to', 'const'),
(454, 'tenders.page.place_load', 'const'),
(455, 'tenders.page.load_point', 'const'),
(456, 'tenders.page.add_place_load', 'const'),
(457, 'tenders.page.place_unload', 'const'),
(458, 'tenders.page.unload_point', 'const'),
(459, 'tenders.page.add_place_unload', 'const'),
(460, 'tenders.page.cargo_type', 'const'),
(461, 'tenders.page.weight_cargo', 'const'),
(462, 'tenders.page.volume_cargo', 'const'),
(463, 'tenders.page.transport_type', 'const'),
(464, 'tenders.page.not_specified', 'const'),
(465, 'tenders.page.count_car', 'const'),
(466, 'tenders.page.dimensions_car', 'const'),
(467, 'tenders.page.length', 'const'),
(468, 'tenders.page.meters', 'const'),
(469, 'tenders.page.width', 'const'),
(470, 'tenders.page.height', 'const'),
(471, 'tenders.page.additional', 'const'),
(472, 'tenders.page.specify', 'const'),
(473, 'tenders.page.date_from_tender', 'const'),
(474, 'tenders.page.time_from_tender', 'const'),
(475, 'tenders.page.date_to_tender', 'const'),
(476, 'tenders.page.time_to_tender', 'const'),
(477, 'tenders.page.recommended_price', 'const'),
(478, 'tenders.page.currency', 'const'),
(479, 'tenders.page.hide_price', 'const'),
(480, 'tenders.page.term_company', 'const'),
(481, 'tenders.page.from[:count:]year', 'const'),
(482, 'tenders.page.from[:count:]years', 'const'),
(483, 'tenders.page.filter.company', 'const'),
(484, 'tenders.page.filter.forwarder', 'const'),
(485, 'tenders.page.filter.partners', 'const'),
(486, 'tenders.page.rules[:link:]', 'const'),
(487, 'tenders.page.publish', 'const'),
(488, 'tenders.page.check_required_fields', 'const'),
(489, 'tenders.page.docs', 'const'),
(490, 'tenders.page.sanitary_passport', 'const'),
(491, 'tenders.page.sanitary_book', 'const'),
(492, 'tenders.page.load_type', 'const'),
(493, 'tenders.page.load_type.side', 'const'),
(494, 'tenders.page.load_type.top', 'const'),
(495, 'tenders.page.load_type.behind', 'const'),
(496, 'tenders.page.load_type.tent', 'const'),
(497, 'tenders.page.conditions', 'const'),
(498, 'tenders.page.conditions.plomb', 'const'),
(499, 'tenders.page.conditions.reload', 'const'),
(500, 'tenders.page.conditions.belts', 'const'),
(501, 'tenders.page.conditions.stands', 'const'),
(502, 'tenders.page.conditions.bort', 'const'),
(503, 'tenders.page.conditions.collectable', 'const'),
(504, 'tenders.page.conditions.pallets', 'const'),
(505, 'tenders.page.mark', 'const'),
(506, 'tenders.page.save', 'const'),
(507, 'tenders.validate.load_date.from.empty', 'const'),
(508, 'tenders.validate.load_date.to.empty', 'const'),
(509, 'tenders.validate.load_date.from.incorrect', 'const'),
(510, 'tenders.validate.place.from.empty', 'const'),
(511, 'tenders.validate.place.to.empty', 'const'),
(512, 'tenders.validate.weight_cargo.empty', 'const'),
(513, 'tenders.validate.weight_cargo.number', 'const'),
(514, 'tenders.validate.volume_cargo.number', 'const'),
(515, 'tenders.validate.date_tender.from.empty', 'const'),
(516, 'tenders.validate.date_tender.to.empty', 'const'),
(517, 'tenders.validate.date_tender.from.incorrect', 'const'),
(518, 'tenders.validate.price.incorrect', 'const'),
(519, 'tenders.validate.rules', 'const'),
(520, 'tenders.bet.incorrect', 'const'),
(521, 'tenders.bet.not_best', 'const'),
(522, 'tenders.bet.higher', 'const'),
(523, 'tenders.bet.empty', 'const'),
(524, 'tenders.bet.not_accepted', 'const'),
(525, 'tenders.model.notification.stop.subject', 'const'),
(526, 'tenders.model.notification.stop.text[:link:]', 'const'),
(527, 'tenders.model.notification.completed.subject.owner', 'const'),
(528, 'tenders.model.notification.completed.text.owner[:link:]', 'const'),
(529, 'tenders.model.notification.completed.subject', 'const'),
(530, 'tenders.model.notification.completed.text[:link:]', 'const'),
(531, 'tenders.error.publish.no_create', 'const'),
(532, 'tenders.error.access.edit_only_authorized', 'const'),
(533, 'tenders.error.access.not_isset', 'const'),
(534, 'tenders.error.access.not_owner', 'const'),
(535, 'tenders.error.access.no_edit_active', 'const'),
(536, 'tenders.error.publish.fields', 'const'),
(537, 'tenders.success.publish.message', 'const'),
(538, 'tenders.error.access.add_only_authorized', 'const'),
(539, 'tenders.error.access.add_no_transporter', 'const'),
(540, 'tenders.bet.success.message', 'const'),
(541, 'tenders.bet.error.bet_ahead', 'const');

-- --------------------------------------------------------

--
-- Структура таблиці `Parser_Country`
--

CREATE TABLE IF NOT EXISTS `Parser_Country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `short_name2` varchar(255) NOT NULL,
  `parser_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `Parser_Country`
--

INSERT INTO `Parser_Country` (`id`, `name`, `short_name`, `short_name2`, `parser_id`, `active`) VALUES
(1, 'Украина', 'UA', 'UA', 1, 1),
(2, 'Россия', 'RU', 'RU', 2, 0),
(3, 'Беларусь', 'BY', 'BY', 6, 0),
(4, 'Австрия', 'AT', 'AT', 7, 0),
(5, 'Азербайджан', 'AZ', 'AZ', 8, 0),
(6, 'Албания', 'AL', 'AL', 9, 0),
(7, 'Андорра', 'AD', 'AD', 70, 0),
(8, 'Аргентина', 'ZZ', 'ZZ', 72, 0),
(9, 'Армения', 'AR', 'AM', 10, 0),
(10, 'Афганистан', 'AF', 'BO', 54, 0),
(11, 'Бахрейн', 'BH', 'BH', 76, 0),
(12, 'Белиз', 'BZ', 'BZ', 78, 0),
(13, 'Бельгия', 'BE', 'BE', 11, 0),
(14, 'Болгария', 'BG', 'BG', 12, 0),
(15, 'Боливия', 'ZY', 'ZY', 80, 0),
(16, 'Босния и Герцеговина', 'BA', 'BA', 13, 0),
(17, 'Бразилия', 'BR', 'BR', 82, 0),
(18, 'Великобритания', 'GB', 'GB', 14, 0),
(19, 'Венгрия', 'HU', 'HU', 15, 0),
(20, 'Венесуэла', 'VE', 'VE', 88, 0),
(21, 'Вьетнам', 'VN', 'VN', 90, 0),
(22, 'Германия', 'DE', 'DE', 16, 0),
(23, 'Голландия', 'NL', 'NL', 17, 0),
(24, 'Греция', 'GR', 'GR', 18, 0),
(25, 'Грузия', 'GE', 'GE', 19, 0),
(26, 'Дания', 'DK', 'DK', 20, 0),
(27, 'Европа', 'EU', 'EU', 56, 0),
(28, 'Египет', 'EG', '', 67, 0),
(29, 'Израиль', 'IL', 'IL', 21, 0),
(30, 'Индия', 'IN', 'IN', 60, 0),
(31, 'Иордания', 'JO', 'JO', 107, 0),
(32, 'Ирак', 'IQ', 'IQ', 63, 0),
(33, 'Иран', 'IR', 'IR', 22, 0),
(34, 'Ирландия', 'IE', 'IE', 23, 0),
(35, 'Исландия', 'IS', 'IS', 68, 0),
(36, 'Испания', 'ES', 'ES', 24, 0),
(37, 'Италия', 'IT', 'IT', 25, 0),
(38, 'Казахстан', 'KZ', 'KZ', 26, 0),
(39, 'Канада', 'CA', 'CA', 113, 0),
(40, 'Кипр', 'CY', 'CY', 116, 0),
(41, 'Китай', 'CN', 'CN', 27, 0),
(42, 'Корея Северная', 'KP', 'KP', 122, 0),
(43, 'Корея Южная', 'KR', 'KR', 123, 0),
(44, 'Кыргызстан', 'KG', 'KG', 28, 0),
(45, 'Латвия', 'LV', 'LV', 29, 0),
(46, 'Ливан', 'LB', 'LYV', 61, 0),
(47, 'Ливия', 'LY', 'LY', 131, 0),
(48, 'Литва', 'LT', 'LT', 30, 0),
(49, 'Лихтенштейн', 'LI', 'LI', 132, 0),
(50, 'Люксембург', 'LU', 'LU', 31, 0),
(51, 'Македония', 'MK', 'MK', 32, 0),
(52, 'Мальта', 'MT', 'MT', 140, 0),
(53, 'Марокко', 'MA', 'MA', 33, 0),
(54, 'Мексика', 'MX', 'MX', 142, 0),
(55, 'Молдова', 'MD', 'MD', 34, 0),
(56, 'Монако', 'MC', 'MC', 144, 0),
(57, 'Монголия', 'MN', 'MN', 58, 0),
(58, 'Непал', 'NP', 'NP', 148, 0),
(59, 'Норвегия', 'NO', 'NO', 35, 0),
(60, 'ОАЭ', 'AE', 'AE', 153, 0),
(61, 'Пакистан', 'PK', 'PK', 155, 0),
(62, 'Палестина', 'PS', 'PS', 157, 0),
(63, 'Польша', 'PL', 'PL', 36, 0),
(64, 'Португалия', 'PT', 'PT', 37, 0),
(65, 'Румыния', 'RO', 'RO', 38, 0),
(66, 'Саудовская Аравия', 'SA', 'SA', 168, 0),
(67, 'Сербия', 'RS', 'RS', 57, 0),
(68, 'Сингапур', 'SG', 'SG', 175, 0),
(69, 'Сирия', 'SY', 'SY', 55, 0),
(70, 'Словакия', 'SK', 'SK', 39, 0),
(71, 'Словения', 'SI', 'SI', 40, 0),
(72, 'СНГ', 'СНГ', 'СНГ', 41, 0),
(73, 'Судан', 'SD', 'SD', 178, 0),
(74, 'США', 'US', 'US', 59, 0),
(75, 'Таджикистан', 'TJ', 'TJ', 42, 0),
(76, 'Таиланд', 'TH', 'TH', 181, 0),
(77, 'Тунис', 'TN', '', 62, 0),
(78, 'Туркменистан', 'TM', 'TM', 43, 0),
(79, 'Турция', 'TR', 'TR', 44, 0),
(80, 'Узбекистан', 'UZ', 'UZ', 45, 0),
(81, 'Финляндия', 'FI', 'FI', 46, 0),
(82, 'Франция', 'FR', 'FR', 47, 0),
(83, 'Хорватия', 'HR', 'HR', 48, 0),
(84, 'Черногория', 'ME', 'ME', 64, 0),
(85, 'Чехия', 'CZ', 'CZ', 49, 0),
(86, 'Чили', 'CL', 'CL', 194, 0),
(87, 'Швейцария', 'CH', 'CH', 50, 0),
(88, 'Швеция', 'SE', 'SE', 51, 0),
(89, 'Эквадор', 'EC', 'EC', 196, 0),
(90, 'Эстония', 'EE', 'EE', 52, 0),
(91, 'ЮАР', 'ZA', 'ZA', 200, 0),
(92, 'Япония', 'JP', 'JP', 203, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `x_admins`
--

CREATE TABLE IF NOT EXISTS `x_admins` (
  `id` int(11) unsigned NOT NULL,
  `srt` int(11) NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `email` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `pass` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `temp_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fb_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `vk_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tw_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `go_link` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця адміністраторів CMS';

--
-- Дамп даних таблиці `x_admins`
--

INSERT INTO `x_admins` (`id`, `srt`, `group_id`, `email`, `pass`, `name`, `login`, `active`, `register_date`, `phone`, `temp_key`, `fb_link`, `vk_link`, `tw_link`, `go_link`) VALUES
(1, 6, 1, 'ap@artis.ms', 'e10adc3949ba59abbe56e057f20f883e', 'root', 'root', 1, '2014-11-05 15:19:00', '+380934527685', '', 'https://www.facebook.com/profile.php?id=100001805845331', 'https://vk.com/kindolp', '', ''),
(9, 0, 2, 'admin@admin.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Адміністратор', '', 1, '2016-01-16 12:26:31', '+380934527666, +380934527668', '', '', '', '', ''),
(11, 0, 3, 'moderator@moderator.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Модератор', '', 1, '2016-01-16 12:55:01', '+380934527688', '', '', '', '', ''),
(12, 0, 4, 'menedjer@menedjer.com', 'bce61afbf6ace69db2539066b0dc5eae', 'Менеджер', '', 1, '2016-01-16 12:55:54', '+380934527685', '', '', '', '', ''),
(17, 0, 2, 'rikalsky@gmail.com', '376c43878878ac04e05946ec1dd7a55f', 'Олександр', '', 1, '2016-06-01 08:00:52', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_groups`
--

CREATE TABLE IF NOT EXISTS `x_admin_groups` (
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_groups`
--

INSERT INTO `x_admin_groups` (`id`) VALUES
(1),
(2),
(3),
(4);

--
-- Тригери `x_admin_groups`
--
DELIMITER $$
CREATE TRIGGER `ins_admin_group_locales` AFTER INSERT ON `x_admin_groups`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT id FROM x_plugin_lngs;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_admin_group_locales (plugin_lng_id,admin_group_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_group_locales`
--

CREATE TABLE IF NOT EXISTS `x_admin_group_locales` (
  `id` int(10) unsigned NOT NULL,
  `admin_group_id` int(10) unsigned NOT NULL,
  `plugin_lng_id` smallint(5) unsigned NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_group_locales`
--

INSERT INTO `x_admin_group_locales` (`id`, `admin_group_id`, `plugin_lng_id`, `title`, `preview`) VALUES
(1, 1, 1, 'Суперадміністратор', 'управління адміністраторами та доступ до управління всієї системи'),
(2, 2, 1, 'Адміністратор', 'управління всією системою, максимальні права доступу'),
(3, 3, 1, 'Модератор', 'управління користувачами та інформаційними розділами порталу'),
(4, 4, 1, 'Менеджер', 'управління заявками');

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_group_rights`
--

CREATE TABLE IF NOT EXISTS `x_admin_group_rights` (
  `ID` int(10) unsigned NOT NULL,
  `rightKey` varchar(100) NOT NULL,
  `status` enum('allowed','forbidden') NOT NULL DEFAULT 'allowed',
  `adminGroupID` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `x_admin_locales`
--

CREATE TABLE IF NOT EXISTS `x_admin_locales` (
  `id` int(10) unsigned NOT NULL,
  `admin_id` int(10) unsigned NOT NULL,
  `lng_id` smallint(5) unsigned NOT NULL,
  `role` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `preview` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_admin_locales`
--

INSERT INTO `x_admin_locales` (`id`, `admin_id`, `lng_id`, `role`, `preview`, `name`) VALUES
(1, 1, 1, 'Адміністратор сайту 2W Entertainment', 'Технічний супровід проекту 2W ENTERTAINMENT.<br><span style="font-size: 12px;text-transform: none;">\nЗони контролю: проектування та впровадження функціональних рішень, системне адміністрування, технічна підтримка та розвиток сайту.</span>', 'Олександр Плоднік'),
(2, 1, 7, 'Site administrator of 2W Entertainment', 'Technical support of the project 2W Entertainment.<br><span style="font-size: 12px;text-transform: none;">Areas of control: design and implementation of functional solutions, system administration and site development.</span>', 'Oleksandr Plodnik'),
(3, 1, 9, 'Администратор сайта 2W Entertainment', 'Техническое сопровождение проекта 2W Entertainment.<br><span style="font-size: 12px;text-transform: none;">Зоны контроля: проектирование и внедрение функциональных решений, системное администрирование, техническая поддержка и развитие сайта.</span>', 'Александр Плодник'),
(4, 2, 1, 'Організація концертів', '«Двигун» проекту 2W Entertainment.<br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Місця проведення та інші локації для культурно-масових заходів, комунікація та розвиток звязків з українськими спільнотами за кордоном.                                             Ідеолог проекту «Без меж»</span>', 'Володимир Філяк'),
(5, 2, 7, 'Concert management', 'The «engine» of 2W Entertainment.  <br> <span style="font-size: 12px;text-transform: none;">Areas of control: Venues and locations for cultural events; communication and development of the relationship with the Ukrainian community abroad. Ideologist of the «No boundaries»</span>', 'Volodymyr Filiak'),
(6, 2, 9, 'Организация концертов', '«Двигатель» проекта 2W Entertainment. <br><span style="font-size: 12px;text-transform: none;">Зоны контроля: Места проведения и другие локации для культурно-массовых мероприятий, коммуникация и развитие отношений с украинскими сообществами за границей. Идеолог направления «Без границ»</span>', 'Владимир Филяк'),
(7, 3, 1, 'Організація концертів', 'Стратегічний партнер проекту 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Планування та організація бізнес-процесів проекту, маркетинг та партнерські програми, комунікації з артистами, концертний тур-менеджмент</span>', 'Тетяна Кільтика'),
(8, 3, 7, 'Concert management', 'Strategic partner of 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Areas of control: Planning and Business process management; marketing and partnership programmes; communications with the artists and tour management</span>', 'Tetiana Kiltyka'),
(9, 3, 9, 'Организация концертов', 'Стратегический партнер проекта 2W Entertainment. <br> <span style="font-size: 12px;text-transform: none;">Зоны контроля: Планирование и организация бизнес-процессов проекта, маркетинг и партнерские программы, коммуникации с артистами, концертный тур-менеджмент</span>', 'Татьяна Кильтыка'),
(10, 4, 1, 'Менеджер напрямку Production', 'Лідер напрямку виробництва колекцій української патріотичної продукції. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: менеджмент виробничого процесу на всіх етапах, дистрибуція, логістика, маркетинг та продажі</span>', 'Дмитро Кротов'),
(11, 4, 7, 'Manager of the Production unit', 'Leader of the production of the Unian patriotic collections. <br> <span style="font-size: 12px;text-transform: none;">Areas of control: management of the production process of all stages of distribution, logistic, marketing and sales</span>', 'Dmitry Krotov'),
(12, 4, 9, 'Менеджер направления Production', 'Лидер направления производства коллекций украинской патриотической продукции. <br> <span style="font-size: 12px;text-transform: none;">Зоны контроля: менеджмент производственного процесса на всех этапах, дистрибуция, логистика, маркетинг и продажи</span>', 'Дмитрий Кротов'),
(13, 5, 1, 'Юрист', 'Юридичний консультант. <br> <span style="font-size: 12px;text-transform: none;">Зони контролю: Юридичний супровід усіх бізнес процесів проекту 2W Entertainment</span>', 'Інна Полякова'),
(14, 5, 7, 'Lawyer', 'Legal support of all business processes of 2W Entertainment', 'Inna Poliakova'),
(15, 5, 9, 'Юрист', 'Юридическое сопровождение всех бизнес процессов проектов 2W Entertainment', 'Инна Полякова'),
(16, 6, 1, 'PR & Контент-менеджер', 'Менеджер проекту United Ukraine - об’єднання українців за кордоном.<br> <span style="font-size: 12px;text-transform: none;">\nЗони контролю: контент-менеджмент, зв’язки з громадськістю, адміністрування соціальних мереж, комунікація з артистами та читацькою аудиторією</span>', 'Анастасія Хілевська'),
(17, 6, 7, 'PR & Content manager', 'Manager of the United Ukraine project - Ukrainian community abroad.<br> <span style="font-size: 12px;text-transform: none;">\nAreas of control: content-management, PR, social media marketing,communication with artists and general audience</span>', 'Anastasia Khilevska'),
(18, 6, 9, 'PR & Контент-менеджер', 'Менеджер проекта United Ukraine - объединение украинцев за границей.<br> <span style="font-size: 12px;text-transform: none;">\nЗоны контроля: контент-менеджмент, связи с общественностью, администрирование социальных сетей, коммуникация с артистами и читательской аудиторией</span>', 'Анастасия Хилевська'),
(19, 7, 7, '', '', ''),
(20, 7, 9, '', '', ''),
(21, 7, 1, '', '', ''),
(22, 8, 7, '', '', ''),
(23, 8, 9, '', '', ''),
(24, 8, 1, '', '', ''),
(25, 9, 7, '', '', ''),
(26, 9, 9, '', '', ''),
(27, 9, 1, '', '', ''),
(28, 11, 7, '', '', ''),
(29, 11, 9, '', '', ''),
(30, 11, 1, '', '', ''),
(31, 12, 7, '', '', ''),
(32, 12, 9, '', '', ''),
(33, 12, 1, '', '', ''),
(34, 13, 7, '', '', ''),
(35, 13, 9, '', '', ''),
(36, 13, 1, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `x_geo_log`
--

CREATE TABLE IF NOT EXISTS `x_geo_log` (
  `id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `step` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `object` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `error` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця для логування модуля гео';

-- --------------------------------------------------------

--
-- Структура таблиці `x_notification_templates`
--

CREATE TABLE IF NOT EXISTS `x_notification_templates` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Тригери `x_notification_templates`
--
DELIMITER $$
CREATE TRIGGER `ins_n_t` AFTER INSERT ON `x_notification_templates`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT ID FROM Frontend_Locales;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_notification_template_locales (lng_id,notification_template_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_notification_template_locales`
--

CREATE TABLE IF NOT EXISTS `x_notification_template_locales` (
  `id` int(10) unsigned NOT NULL,
  `lng_id` int(10) unsigned NOT NULL,
  `notification_template_id` int(11) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugins`
--

CREATE TABLE IF NOT EXISTS `x_plugins` (
  `id` int(11) unsigned NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT '1',
  `srt` int(11) NOT NULL,
  `icon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sub` int(10) unsigned DEFAULT NULL,
  `sub2` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця плагінів cms';

--
-- Дамп даних таблиці `x_plugins`
--

INSERT INTO `x_plugins` (`id`, `controller`, `action`, `is_show`, `srt`, `icon`, `sub`, `sub2`) VALUES
(59, '', '', 1, 7, 'fa-eye', NULL, 0),
(60, 'admin', '', 1, 1, '', 59, 0),
(61, 'admin', 'add', 1, 2, '', 59, 0),
(62, 'admin', 'edit', 0, -100, '', 59, 0),
(84, '', '', 1, -200, 'fa-users', NULL, 0),
(85, 'user', '', 1, -200, '', 84, 0),
(86, 'user', 'add', 1, 2, '', 84, 0),
(87, 'user', 'edit', 0, -100, '', 84, 0),
(128, '', '', 1, -270, 'fa-building-o', NULL, 0),
(129, 'company', '', 1, 1, '', 128, 0),
(134, 'admin', 'group', 1, 3, '', 59, 0),
(228, 'company', 'add', 1, 2, '', 128, 0),
(229, 'company', 'edit', 0, -100, '', 128, 0),
(291, 'company', 'owner_types', 1, 3, '', 302, 0),
(293, 'transport', '', 1, 4, '', 128, 0),
(296, '', '', 1, -290, 'fa-truck', NULL, 0),
(297, 'transportRequests', '', 1, 1, '', 296, -100),
(298, 'transportRequests', 'add', 1, 2, '', 296, 0),
(299, '', '', 1, -280, 'fa-th', NULL, 0),
(300, 'cargoRequests', '', 1, 1, '', 299, 0),
(301, 'cargoRequests', 'add', 1, 2, '', 299, 0),
(302, '', '', 1, 400, 'fa-cogs', NULL, 0),
(303, 'settings', 'locales', 1, 1, '', 302, 0),
(304, '', '', 1, -260, 'fa-calendar', NULL, 0),
(305, 'news', '', 1, 1, '', 304, 0),
(306, 'news', 'add', 1, 2, '', 304, 0),
(307, 'user', 'news', 1, 3, '', 84, 0),
(308, 'company', 'comments', 0, 4, '', 128, 0),
(310, '', '', 1, -279, 'fa-gavel', NULL, 0),
(311, 'tenders', '', 1, 0, '', 310, 0),
(312, 'tenders', 'add', 1, 0, '', 310, 0),
(313, 'settings', 'currencies', 1, 0, '', 302, 0),
(314, 'settings', 'car_types', 1, 0, '', 302, 0),
(318, 'feedback', '', 1, 200, '', NULL, 0),
(319, 'messages', '', 1, 300, '', NULL, 0),
(320, 'comments', '', 1, 301, '', NULL, 0),
(321, 'complaints', '', 1, 304, '', NULL, 0),
(323, '', '', 1, 307, '', NULL, 0),
(324, 'pages', '', 1, 1, '', 323, 0),
(325, 'pages', 'add', 1, 2, '', 323, 0);

--
-- Тригери `x_plugins`
--
DELIMITER $$
CREATE TRIGGER `ins_plugin_locale` AFTER INSERT ON `x_plugins`
 FOR EACH ROW BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE lng_id INT;
    DECLARE cur CURSOR FOR SELECT id FROM x_plugin_lngs;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO lng_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            INSERT INTO x_plugin_locales (plugin_lng_id,plugin_id) VALUES (lng_id,NEW.id);
        END LOOP;
    CLOSE cur;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugin_lngs`
--

CREATE TABLE IF NOT EXISTS `x_plugin_lngs` (
  `id` smallint(5) unsigned NOT NULL,
  `pref` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `def` tinyint(1) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='таблиця мов плагінів cms';

--
-- Дамп даних таблиці `x_plugin_lngs`
--

INSERT INTO `x_plugin_lngs` (`id`, `pref`, `title`, `def`, `active`) VALUES
(1, 'uk', 'Українська', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `x_plugin_locales`
--

CREATE TABLE IF NOT EXISTS `x_plugin_locales` (
  `id` int(10) unsigned NOT NULL,
  `plugin_lng_id` smallint(5) unsigned NOT NULL,
  `plugin_id` int(10) unsigned NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `x_plugin_locales`
--

INSERT INTO `x_plugin_locales` (`id`, `plugin_lng_id`, `plugin_id`, `title`, `active`) VALUES
(301, 1, 59, 'Адміністрація', 1),
(302, 1, 60, 'Показати всіх', 1),
(303, 1, 62, 'Профіль адміністратора', 1),
(304, 1, 61, 'Створити', 1),
(305, 1, 84, 'Користувачі', 1),
(306, 1, 85, 'Показати всіх', 1),
(307, 1, 86, 'Створити користувача', 1),
(308, 1, 87, 'Профіль користувача', 1),
(309, 1, 125, 'Групи користувачів', 1),
(310, 1, 128, 'Компанії', 1),
(311, 1, 129, 'Показати всі', 1),
(312, 1, 130, 'Типи компаній', 1),
(313, 1, 134, 'Групи адміністраторів', 1),
(314, 1, 228, 'Створити компанію', 1),
(315, 1, 229, 'Профіль компанії', 1),
(316, 1, 291, 'Форми власності', 1),
(317, 1, 292, 'Транспорт компаній', 1),
(318, 1, 293, 'Транспорт компаній', 1),
(319, 1, 294, 'Додати транспорт', 1),
(320, 1, 295, 'Редагування транспорта', 1),
(321, 1, 296, 'Транспорт', 1),
(322, 1, 297, 'Всі заявки', 1),
(323, 1, 298, 'Створити заявку', 1),
(324, 1, 299, 'Вантажі', 1),
(325, 1, 300, 'Всі заявки', 1),
(326, 1, 301, 'Створити заявку', 1),
(327, 1, 302, 'Налаштування', 1),
(328, 1, 303, 'Локалізація', 1),
(329, 1, 304, 'Прес-центр', 1),
(330, 1, 305, 'Всі новини', 1),
(331, 1, 306, 'Додати', 1),
(332, 1, 307, 'Новини', 1),
(333, 1, 308, 'Коментарі', 1),
(334, 1, 309, 'Аналітика', 1),
(335, 1, 310, 'Тендери', 1),
(336, 1, 311, 'Всі тендери', 1),
(337, 1, 312, 'Створити', 1),
(338, 1, 313, 'Валюти', 1),
(339, 1, 314, 'Типи транспорта', 1),
(340, 1, 315, 'Шаблони сповіщень', 1),
(341, 1, 316, 'Всі сповіщення', 1),
(342, 1, 317, 'Створити', 1),
(343, 1, 318, 'Зворотній зв''язок', 1),
(344, 1, 319, 'Повідомлення', 1),
(345, 1, 320, 'Відгуки', 1),
(346, 1, 321, 'Скарги', 1),
(348, 1, 323, 'Сторінки', 1),
(349, 1, 324, 'Всі сторінки', 1),
(350, 1, 325, 'Додати сторінку', 1);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `Frontend_Black_List`
--
ALTER TABLE `Frontend_Black_List`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_User_Car_Types1_idx` (`transportTypeID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Request_Prices1_idx` (`priceID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `type` (`type`,`geoID`,`requestID`,`groupNumber`),
  ADD KEY `fk_Frontend_Cargo_to_Geo_Frontend_Geo1_idx` (`geoID`),
  ADD KEY `fk_Frontend_Cargo_to_Geo_Frontend_Cargo1_idx` (`requestID`);

--
-- Індекси таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `text` (`text`(255)),
  ADD KEY `companyID` (`companyID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `cargoID` (`cargoID`),
  ADD KEY `transportID` (`transportID`);

--
-- Індекси таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Companies_Frontend_User_Types1_idx` (`typeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Company_Ownership_Types1_idx` (`ownershipTypeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Locales1_idx` (`primaryLocaleID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Phonecodes1_idx` (`phoneCodeID`),
  ADD KEY `fk_Frontend_Companies_Frontend_Phonecodes2_idx` (`phoneStationaryCodeID`);

--
-- Індекси таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Comany_Locales_Frontend_Companies1_idx` (`companyID`),
  ADD KEY `fk_Frontend_Comany_Locales_Frontend_Locales1_idx` (`localeID`);

--
-- Індекси таблиці `Frontend_Company_Ownership_Types`
--
ALTER TABLE `Frontend_Company_Ownership_Types`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Індекси таблиці `Frontend_Company_Partners`
--
ALTER TABLE `Frontend_Company_Partners`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `company1ID_2` (`company1ID`,`company2ID`),
  ADD KEY `company1ID` (`company1ID`),
  ADD KEY `company2ID` (`company2ID`);

--
-- Індекси таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Unique_Index` (`geoID`,`companyID`),
  ADD KEY `fk_Frontend_Company_to_Geo_Relation_Frontend_Geo1_idx` (`geoID`),
  ADD KEY `fk_Frontend_Company_to_Geo_Relation_Frontend_Companies1_idx` (`companyID`);

--
-- Індекси таблиці `Frontend_Company_Types`
--
ALTER TABLE `Frontend_Company_Types`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fromUserID` (`userID`),
  ADD KEY `complainCompanyID` (`complainCompanyID`);

--
-- Індекси таблиці `Frontend_Currencies`
--
ALTER TABLE `Frontend_Currencies`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Dialogs`
--
ALTER TABLE `Frontend_Dialogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `toUserID` (`toUserID`),
  ADD KEY `fromUserID` (`fromUserID`),
  ADD KEY `fromAdminID` (`fromAdminID`),
  ADD KEY `toAdminID` (`toAdminID`),
  ADD KEY `transportID` (`transportID`),
  ADD KEY `cargoID` (`cargoID`),
  ADD KEY `tenderID` (`tenderID`);

--
-- Індекси таблиці `Frontend_Dialogs_Unread`
--
ALTER TABLE `Frontend_Dialogs_Unread`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `toAdminID` (`toAdminID`),
  ADD KEY `toUserID` (`toUserID`);

--
-- Індекси таблиці `Frontend_Feedback`
--
ALTER TABLE `Frontend_Feedback`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`user_name`);

--
-- Індекси таблиці `Frontend_Filter_Templates`
--
ALTER TABLE `Frontend_Filter_Templates`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Filter_Templates_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `parentID` (`parentID`);

--
-- Індекси таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Geo_Locales_Frontend_Locales1_idx` (`localeID`),
  ADD KEY `fk_Frontend_Geo_Locales_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Locales`
--
ALTER TABLE `Frontend_Locales`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Locale_Vars_Frontend_Locales1_idx` (`localeID`);

--
-- Індекси таблиці `Frontend_Managers_Links`
--
ALTER TABLE `Frontend_Managers_Links`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fromAdminID` (`adminMessageID`),
  ADD KEY `dialogID` (`dialogID`),
  ADD KEY `userMessageID` (`userMessageID`);

--
-- Індекси таблиці `Frontend_Messages_Unread`
--
ALTER TABLE `Frontend_Messages_Unread`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `toUserID` (`toUserID`),
  ADD KEY `toAdminID` (`toAdminID`),
  ADD KEY `dialogID` (`dialogID`);

--
-- Індекси таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `companyID` (`companyID`);

--
-- Індекси таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `newsID` (`newsID`),
  ADD KEY `localeID` (`localeID`);

--
-- Індекси таблиці `Frontend_Phonecodes`
--
ALTER TABLE `Frontend_Phonecodes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Індекси таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Registration_Links_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `priceCurrencyID` (`currencyID`);

--
-- Індекси таблиці `Frontend_Static_Pages`
--
ALTER TABLE `Frontend_Static_Pages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Static_Pages_Frontend_Locales1_idx` (`localeID`);

--
-- Індекси таблиці `Frontend_Subscription`
--
ALTER TABLE `Frontend_Subscription`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Індекси таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_User_Car_Types1_idx` (`transportTypeID`),
  ADD KEY `fk_Frontend_Cargo_Frontend_Users1_idx` (`userID`),
  ADD KEY `priceCurrencyID` (`currencyID`);

--
-- Індекси таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `requestID` (`requestID`,`geoID`,`type`,`groupNumber`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Requests1_idx` (`requestID`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Transports_Frontend_Request_Prices1_idx` (`priceID`),
  ADD KEY `fk_Frontend_Transports_Frontend_User_Cars1_idx` (`userCarID`),
  ADD KEY `fk_Frontend_Transports_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `requestID` (`requestID`,`geoID`,`type`,`groupNumber`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Requests1_idx` (`requestID`),
  ADD KEY `fk_Frontend_Request_to_Geo_Frontend_Geo1_idx` (`geoID`);

--
-- Індекси таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Users_Frontend_Companies1_idx` (`companyID`),
  ADD KEY `phoneCodeID` (`phoneCodeID`),
  ADD KEY `phoneStationaryCodeID` (`phoneStationaryCodeID`),
  ADD KEY `primaryLocaleID` (`primaryLocaleID`),
  ADD KEY `deleted` (`deleted`);

--
-- Індекси таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Cars_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_Cars_Frontend_User_Car_Types1_idx` (`carTypeID`);

--
-- Індекси таблиці `Frontend_User_Car_Types`
--
ALTER TABLE `Frontend_User_Car_Types`
  ADD PRIMARY KEY (`ID`);

--
-- Індекси таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Locales_Frontend_Locales1_idx` (`localeID`),
  ADD KEY `fk_Frontend_User_Locales_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_Company_Socials_Frontend_Users1_idx` (`userID`);

--
-- Індекси таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1_idx` (`cargoID`);

--
-- Індекси таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Company_Ownership_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Company_Ownership_Frontend_Companies1_idx` (`companyID`);

--
-- Індекси таблиці `Frontend_User_to_Tender_Auto_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Auto_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `tenderID` (`tenderID`),
  ADD KEY `price` (`price`),
  ADD KEY `created` (`updated`),
  ADD KEY `created_2` (`created`);

--
-- Індекси таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1_idx` (`tenderID`);

--
-- Індекси таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_to_Transport_Requests_Frontend_Users1_idx` (`userID`),
  ADD KEY `fk_Frontend_User_to_Transport_Requests_Frontend_Transports1_idx` (`transportID`);

--
-- Індекси таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Frontend_User_Type_Rights_Frontend_User_Types1_idx` (`userTypeID`);

--
-- Індекси таблиці `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_Const` (`const`),
  ADD KEY `type` (`type`);

--
-- Індекси таблиці `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD UNIQUE KEY `i18nID` (`i18nID`,`localeID`),
  ADD KEY `fk_locale_idx` (`localeID`);

--
-- Індекси таблиці `i18n_locale_sasha`
--
ALTER TABLE `i18n_locale_sasha`
  ADD UNIQUE KEY `i18nID` (`i18nID`,`localeID`),
  ADD KEY `fk_locale_idx` (`localeID`);

--
-- Індекси таблиці `i18n_sasha`
--
ALTER TABLE `i18n_sasha`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_Const` (`const`),
  ADD KEY `type` (`type`);

--
-- Індекси таблиці `Parser_Country`
--
ALTER TABLE `Parser_Country`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `x_admins`
--
ALTER TABLE `x_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `group_id` (`group_id`);

--
-- Індекси таблиці `x_admin_groups`
--
ALTER TABLE `x_admin_groups`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `x_admin_group_locales`
--
ALTER TABLE `x_admin_group_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_group_id` (`admin_group_id`),
  ADD KEY `plugin_lng_id` (`plugin_lng_id`);

--
-- Індекси таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_Backend_Admin_Group_Rights_Backend_Admin_Group1_idx` (`adminGroupID`);

--
-- Індекси таблиці `x_admin_locales`
--
ALTER TABLE `x_admin_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `lng_id` (`lng_id`);

--
-- Індекси таблиці `x_geo_log`
--
ALTER TABLE `x_geo_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `step` (`step`),
  ADD KEY `object` (`object`(255)),
  ADD KEY `error` (`error`(255));

--
-- Індекси таблиці `x_notification_templates`
--
ALTER TABLE `x_notification_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Індекси таблиці `x_notification_template_locales`
--
ALTER TABLE `x_notification_template_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lng_id` (`lng_id`),
  ADD KEY `notification_template_id` (`notification_template_id`);

--
-- Індекси таблиці `x_plugins`
--
ALTER TABLE `x_plugins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub` (`sub`);

--
-- Індекси таблиці `x_plugin_lngs`
--
ALTER TABLE `x_plugin_lngs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pref` (`pref`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Індекси таблиці `x_plugin_locales`
--
ALTER TABLE `x_plugin_locales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lng_id` (`plugin_lng_id`,`plugin_id`),
  ADD KEY `plugin_id` (`plugin_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `Frontend_Black_List`
--
ALTER TABLE `Frontend_Black_List`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28981;
--
-- AUTO_INCREMENT для таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6212;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Ownership_Types`
--
ALTER TABLE `Frontend_Company_Ownership_Types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Partners`
--
ALTER TABLE `Frontend_Company_Partners`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Company_Types`
--
ALTER TABLE `Frontend_Company_Types`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Currencies`
--
ALTER TABLE `Frontend_Currencies`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `Frontend_Dialogs`
--
ALTER TABLE `Frontend_Dialogs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT для таблиці `Frontend_Dialogs_Unread`
--
ALTER TABLE `Frontend_Dialogs_Unread`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Feedback`
--
ALTER TABLE `Frontend_Feedback`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Filter_Templates`
--
ALTER TABLE `Frontend_Filter_Templates`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4356;
--
-- AUTO_INCREMENT для таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=983;
--
-- AUTO_INCREMENT для таблиці `Frontend_Locales`
--
ALTER TABLE `Frontend_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Managers_Links`
--
ALTER TABLE `Frontend_Managers_Links`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=229;
--
-- AUTO_INCREMENT для таблиці `Frontend_Messages_Unread`
--
ALTER TABLE `Frontend_Messages_Unread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT для таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `Frontend_Phonecodes`
--
ALTER TABLE `Frontend_Phonecodes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38021;
--
-- AUTO_INCREMENT для таблиці `Frontend_Static_Pages`
--
ALTER TABLE `Frontend_Static_Pages`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `Frontend_Subscription`
--
ALTER TABLE `Frontend_Subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8761;
--
-- AUTO_INCREMENT для таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22709;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2070;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Car_Types`
--
ALTER TABLE `Frontend_User_Car_Types`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Tender_Auto_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Auto_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=905;
--
-- AUTO_INCREMENT для таблиці `i18n_sasha`
--
ALTER TABLE `i18n_sasha`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=542;
--
-- AUTO_INCREMENT для таблиці `Parser_Country`
--
ALTER TABLE `Parser_Country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT для таблиці `x_admins`
--
ALTER TABLE `x_admins`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблиці `x_admin_groups`
--
ALTER TABLE `x_admin_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `x_admin_group_locales`
--
ALTER TABLE `x_admin_group_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `x_admin_locales`
--
ALTER TABLE `x_admin_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблиці `x_geo_log`
--
ALTER TABLE `x_geo_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `x_notification_templates`
--
ALTER TABLE `x_notification_templates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблиці `x_notification_template_locales`
--
ALTER TABLE `x_notification_template_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблиці `x_plugins`
--
ALTER TABLE `x_plugins`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT для таблиці `x_plugin_lngs`
--
ALTER TABLE `x_plugin_lngs`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `x_plugin_locales`
--
ALTER TABLE `x_plugin_locales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=351;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Cargo`
--
ALTER TABLE `Frontend_Cargo`
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_Request_Prices1` FOREIGN KEY (`priceID`) REFERENCES `Frontend_Request_Prices` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_Frontend_User_Car_Types1` FOREIGN KEY (`transportTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Cargo_to_Geo_Relation`
--
ALTER TABLE `Frontend_Cargo_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Cargo_to_Geo_Frontend_Cargo1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Cargo_to_Geo_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Comments`
--
ALTER TABLE `Frontend_Comments`
  ADD CONSTRAINT `Frontend_Comments_ibfk_1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_3` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Comments_ibfk_4` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Companies`
--
ALTER TABLE `Frontend_Companies`
  ADD CONSTRAINT `fk_Frontend_Companies_Frontend_User_Types1` FOREIGN KEY (`typeID`) REFERENCES `Frontend_Company_Types` (`ID`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_1` FOREIGN KEY (`ownershipTypeID`) REFERENCES `Frontend_Company_Ownership_Types` (`id`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_2` FOREIGN KEY (`primaryLocaleID`) REFERENCES `Frontend_Locales` (`ID`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_3` FOREIGN KEY (`phoneCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Companies_ibfk_4` FOREIGN KEY (`phoneStationaryCodeID`) REFERENCES `Frontend_Phonecodes` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Company_Locales`
--
ALTER TABLE `Frontend_Company_Locales`
  ADD CONSTRAINT `fk_Frontend_Comany_Locales_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `Frontend_Company_Locales_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Company_Partners`
--
ALTER TABLE `Frontend_Company_Partners`
  ADD CONSTRAINT `Frontend_Company_Partners_ibfk_2` FOREIGN KEY (`company2ID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Company_Partners_ibfk_1` FOREIGN KEY (`company1ID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Company_to_Geo_Relation`
--
ALTER TABLE `Frontend_Company_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Company_to_Geo_Relation_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Company_to_Geo_Relation_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Complaints`
--
ALTER TABLE `Frontend_Complaints`
  ADD CONSTRAINT `Frontend_Complaints_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Complaints_ibfk_3` FOREIGN KEY (`complainCompanyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Dialogs`
--
ALTER TABLE `Frontend_Dialogs`
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_1` FOREIGN KEY (`fromAdminID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_2` FOREIGN KEY (`toAdminID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_3` FOREIGN KEY (`toUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_4` FOREIGN KEY (`fromUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_5` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_6` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_ibfk_7` FOREIGN KEY (`tenderID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Dialogs_Unread`
--
ALTER TABLE `Frontend_Dialogs_Unread`
  ADD CONSTRAINT `Frontend_Dialogs_Unread_ibfk_2` FOREIGN KEY (`toAdminID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Dialogs_Unread_ibfk_1` FOREIGN KEY (`toUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Filter_Templates`
--
ALTER TABLE `Frontend_Filter_Templates`
  ADD CONSTRAINT `fk_Frontend_Filter_Templates_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Geo`
--
ALTER TABLE `Frontend_Geo`
  ADD CONSTRAINT `FK_parentID_GEO` FOREIGN KEY (`parentID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Geo_Locales`
--
ALTER TABLE `Frontend_Geo_Locales`
  ADD CONSTRAINT `fk_Frontend_Geo_Locales_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Geo_Locales_Frontend_Locales1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Locale_Vars`
--
ALTER TABLE `Frontend_Locale_Vars`
  ADD CONSTRAINT `Frontend_Locale_Vars_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Messages`
--
ALTER TABLE `Frontend_Messages`
  ADD CONSTRAINT `Frontend_Messages_ibfk_7` FOREIGN KEY (`dialogID`) REFERENCES `Frontend_Dialogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_8` FOREIGN KEY (`adminMessageID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_ibfk_9` FOREIGN KEY (`userMessageID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Messages_Unread`
--
ALTER TABLE `Frontend_Messages_Unread`
  ADD CONSTRAINT `Frontend_Messages_Unread_ibfk_2` FOREIGN KEY (`toUserID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_Unread_ibfk_4` FOREIGN KEY (`toAdminID`) REFERENCES `x_admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Messages_Unread_ibfk_5` FOREIGN KEY (`dialogID`) REFERENCES `Frontend_Dialogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_News`
--
ALTER TABLE `Frontend_News`
  ADD CONSTRAINT `Frontend_News_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_News_ibfk_2` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_News_Locales`
--
ALTER TABLE `Frontend_News_Locales`
  ADD CONSTRAINT `Frontend_News_Locales_ibfk_1` FOREIGN KEY (`newsID`) REFERENCES `Frontend_News` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_News_Locales_ibfk_2` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Registration_Links`
--
ALTER TABLE `Frontend_Registration_Links`
  ADD CONSTRAINT `fk_Frontend_Registration_Links_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Request_Prices`
--
ALTER TABLE `Frontend_Request_Prices`
  ADD CONSTRAINT `Frontend_Request_Prices_ibfk_1` FOREIGN KEY (`currencyID`) REFERENCES `Frontend_Currencies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Static_Pages`
--
ALTER TABLE `Frontend_Static_Pages`
  ADD CONSTRAINT `fk_Frontend_Static_Pages_Frontend_Locales1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Tenders`
--
ALTER TABLE `Frontend_Tenders`
  ADD CONSTRAINT `Frontend_Tenders_ibfk_1` FOREIGN KEY (`transportTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_ibfk_3` FOREIGN KEY (`currencyID`) REFERENCES `Frontend_Currencies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Tenders_to_Geo_Relation`
--
ALTER TABLE `Frontend_Tenders_to_Geo_Relation`
  ADD CONSTRAINT `Frontend_Tenders_to_Geo_Relation_ibfk_1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_Tenders_to_Geo_Relation_ibfk_2` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Transports`
--
ALTER TABLE `Frontend_Transports`
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_Request_Prices1` FOREIGN KEY (`priceID`) REFERENCES `Frontend_Request_Prices` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Transports_Frontend_User_Cars1` FOREIGN KEY (`userCarID`) REFERENCES `Frontend_User_Cars` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Transport_to_Geo_Relation`
--
ALTER TABLE `Frontend_Transport_to_Geo_Relation`
  ADD CONSTRAINT `fk_Frontend_Request_to_Geo_Frontend_Geo1` FOREIGN KEY (`geoID`) REFERENCES `Frontend_Geo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_Request_to_Geo_Frontend_Requests1` FOREIGN KEY (`requestID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_Users`
--
ALTER TABLE `Frontend_Users`
  ADD CONSTRAINT `fk_Frontend_Users_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`),
  ADD CONSTRAINT `Frontend_Users_ibfk_1` FOREIGN KEY (`phoneCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Users_ibfk_2` FOREIGN KEY (`phoneStationaryCodeID`) REFERENCES `Frontend_Phonecodes` (`id`),
  ADD CONSTRAINT `Frontend_Users_ibfk_3` FOREIGN KEY (`primaryLocaleID`) REFERENCES `Frontend_Locales` (`ID`);

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Cars`
--
ALTER TABLE `Frontend_User_Cars`
  ADD CONSTRAINT `fk_Frontend_User_Cars_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_Cars_Frontend_User_Car_Types1` FOREIGN KEY (`carTypeID`) REFERENCES `Frontend_User_Car_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Locales`
--
ALTER TABLE `Frontend_User_Locales`
  ADD CONSTRAINT `fk_Frontend_User_Locales_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `Frontend_User_Locales_ibfk_1` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Socials`
--
ALTER TABLE `Frontend_User_Socials`
  ADD CONSTRAINT `fk_Frontend_Company_Socials_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Cargo_Requests`
--
ALTER TABLE `Frontend_User_to_Cargo_Requests`
  ADD CONSTRAINT `fk_Frontend_User_to_Cargo_Requests_Frontend_Cargo1` FOREIGN KEY (`cargoID`) REFERENCES `Frontend_Cargo` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Cargo_Requests_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Company_Ownership`
--
ALTER TABLE `Frontend_User_to_Company_Ownership`
  ADD CONSTRAINT `fk_Frontend_User_to_Company_Ownership_Frontend_Companies1` FOREIGN KEY (`companyID`) REFERENCES `Frontend_Companies` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Company_Ownership_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Tender_Auto_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Auto_Requests`
  ADD CONSTRAINT `Frontend_User_to_Tender_Auto_Requests_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_User_to_Tender_Auto_Requests_ibfk_2` FOREIGN KEY (`tenderID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Tender_Requests`
--
ALTER TABLE `Frontend_User_to_Tender_Requests`
  ADD CONSTRAINT `Frontend_User_to_Tender_Requests_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Frontend_User_to_Tender_Requests_ibfk_2` FOREIGN KEY (`tenderID`) REFERENCES `Frontend_Tenders` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_to_Transport_Requests`
--
ALTER TABLE `Frontend_User_to_Transport_Requests`
  ADD CONSTRAINT `fk_Frontend_User_to_Transport_Requests_Frontend_Transports1` FOREIGN KEY (`transportID`) REFERENCES `Frontend_Transports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Frontend_User_to_Transport_Requests_Frontend_Users1` FOREIGN KEY (`userID`) REFERENCES `Frontend_Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `Frontend_User_Type_Rights`
--
ALTER TABLE `Frontend_User_Type_Rights`
  ADD CONSTRAINT `fk_Frontend_User_Type_Rights_Frontend_User_Types1` FOREIGN KEY (`userTypeID`) REFERENCES `Frontend_Company_Types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `i18n_locale`
--
ALTER TABLE `i18n_locale`
  ADD CONSTRAINT `fk_i18n_idx` FOREIGN KEY (`i18nID`) REFERENCES `i18n` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_locale_idx` FOREIGN KEY (`localeID`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `x_admins`
--
ALTER TABLE `x_admins`
  ADD CONSTRAINT `x_admins_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `x_admin_groups` (`id`) ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `x_admin_group_rights`
--
ALTER TABLE `x_admin_group_rights`
  ADD CONSTRAINT `fk_Backend_Admin_Group_Rights_Backend_Admin_Group1` FOREIGN KEY (`adminGroupID`) REFERENCES `x_admin_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Обмеження зовнішнього ключа таблиці `x_notification_template_locales`
--
ALTER TABLE `x_notification_template_locales`
  ADD CONSTRAINT `x_notification_template_locales_ibfk_2` FOREIGN KEY (`lng_id`) REFERENCES `Frontend_Locales` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `x_notification_template_locales_ibfk_1` FOREIGN KEY (`notification_template_id`) REFERENCES `x_notification_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
